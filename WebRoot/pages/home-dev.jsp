<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="com.dataeye.ad.assistor.context.SessionContainer"%>
<%@page import="com.dataeye.ad.assistor.context.UserInSession"%>
<%@page import="java.util.Map"%>

<%
  Map<String, Object> map = SessionContainer.getCurrentUserCompatible(request.getSession());
  int status = Integer.parseInt(map.get("status").toString());
  UserInSession currentUser = (UserInSession)map.get("user");
  int accountId = -1;
  String userID = "";
  int companyId = -1;
  String menuPermission = "";
  int accountRole = -1;
  if(currentUser == null){
    accountId = -1;
    userID="";
    companyId = -1;
    menuPermission = "";
    accountRole = -1;
  }else{
    accountId=currentUser.getAccountId();
    userID=currentUser.getAccountName();
    companyId = currentUser.getCompanyId();
    menuPermission = currentUser.getMenuPermission();
    accountRole = currentUser.getAccountRole();
  }
%>


  <!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8"/>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>易投放</title>
  <link rel="shortcut icon" href="https://www.dataeye.com/assets/img/favicon.ico" />

<link href="http://127.0.0.1:8080/assets-dist/css/bundle.css" rel="stylesheet"></head>
<body class="adt-wrap">
  <div id="container"></div>
  <script>
  (function() {
    // 是否使用mock
    // var useMock = location.search.indexOf('mock') > -1
    var useMock = location.hash.indexOf('mock') > -1
    var segments = location.pathname.split('/')
    var contextPath = segments[1] !== 'pages' ? '/' + segments[1] : ''
      window.App = {
      // 生产环境自动排除mock
      useMock: location.pathname.indexOf('-dev') > -1 && useMock,
      // 项目根路径
      CONTEXT_PATH: contextPath,
      accountId: '<%=accountId %>',
      userName: '<%=userID %>',
      companyId: '<%=companyId %>',
      menuPermission: '<%=menuPermission %>',
      accountRole: '<%=accountRole %>',
      status: '<%=status %>'
    }
  })()
  </script>


  <script type="text/javascript" src="http://127.0.0.1:8080/assets-dist/LaunchAuxiliary/common.js"></script><script type="text/javascript" src="http://127.0.0.1:8080/assets-dist/LaunchAuxiliary/app.js"></script></body>
</html>
