<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="com.dataeye.ad.assistor.context.SessionContainer"%>
<%@page import="com.dataeye.ad.assistor.context.UserInSession"%>
<%@page import="java.util.Map"%>

<%
  Map<String, Object> map = SessionContainer.getCurrentUser(request);
  int status = Integer.parseInt(map.get("status").toString());
  boolean showTips = Boolean.parseBoolean(map.get("show_tips").toString());
  UserInSession currentUser = (UserInSession)map.get("user");
  int accountId = -1;
  String userID = "";
  int companyId = -1;
  String menuPermission = "";
  int accountRole = -1;
  if(status == 1){
    response.sendRedirect("https://www.dataeye.com/ptlogin/v2/index.html#/Login");
    return;
  }
  if(currentUser == null){
    accountId = -1;
    userID="";
    companyId = -1;
    menuPermission = "";
    accountRole = -1;
  }else{
    accountId=currentUser.getAccountId();
    userID=currentUser.getAccountName();
    companyId = currentUser.getCompanyId();
    menuPermission = currentUser.getMenuPermission();
    accountRole = currentUser.getAccountRole();
  }
%>


  <!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8"/>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>易投放</title>
  <link rel="shortcut icon" href="https://www.dataeye.com/assets/img/favicon.ico" />

<link href="./../../assets-dist/LaunchAuxiliary/bundle.b92aeda4.css" rel="stylesheet"></head>
<body class="adt-wrap">
  <div id="container"></div>
  <script>
  (function() {
    // 是否使用mock
    // var useMock = location.search.indexOf('mock') > -1
    var useMock = location.hash.indexOf('mock') > -1
    var segments = location.pathname.split('/')
    var contextPath = segments[1] !== 'pages' ? '/' + segments[1] : ''
      window.App = {
      // 生产环境自动排除mock
      useMock: location.pathname.indexOf('-dev') > -1 && useMock,
      // 项目根路径
      CONTEXT_PATH: contextPath,
      accountId: '<%=accountId %>',
      userName: '<%=userID %>',
      companyId: '<%=companyId %>',
      menuPermission: '<%=menuPermission %>',
      accountRole: '<%=accountRole %>',
      status: '<%=status %>',
      showTips: '<%=showTips %>'
    }
  })()
  </script>


  <script type="text/javascript" src="./../../assets-dist/LaunchAuxiliary/common.60d80bab.js"></script><script type="text/javascript" src="./../../assets-dist/LaunchAuxiliary/app.58bee9c0.js"></script></body>
</html>
