<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@page import="com.dataeye.ad.assistor.context.TrackingAuditHandler"%>
<%
  int status = TrackingAuditHandler.checkUser(request);
  if(status == 0){
	response.sendRedirect("http://t2.dataeye.com/pages/index.jsp");
	return;
  }
  if(status == 1){
    response.sendRedirect("https://www.dataeye.com/ptlogin/v2/index.html#/Login");
    return;
  }
%>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8"/>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>易投放</title>
  <link rel="shortcut icon" href="https://www.dataeye.com/assets/img/favicon.ico" />

<link href="./../../assets-dist/LaunchAuxiliary/bundle.b92aeda4.css" rel="stylesheet"></head>
<body class="adt-wrap">
  <div id="container"></div>
  <script>
    (function() {
      // 是否使用mock
      // var useMock = location.search.indexOf('mock') > -1
      var useMock = location.hash.indexOf('mock') > -1
      // console.dir(location)
      // console.dir(useMock)
      var segments = location.pathname.split('/')
      var contextPath = segments[1] !== 'pages' ? '/' + segments[1] : ''

      window.App = {
        // 生产环境自动排除mock
        useMock: location.pathname.indexOf('-dev') > -1 && useMock,
        // 项目根路径
        CONTEXT_PATH: contextPath,
        status: '<%=status %>',
		    product: 2
      }
    })()
  </script>


<script type="text/javascript" src="./../../assets-dist/LaunchAuxiliary/common.60d80bab.js"></script><script type="text/javascript" src="./../../assets-dist/LaunchAuxiliary/app.58bee9c0.js"></script></body>
</html>
