package com.dataeye.ad.assistor.util;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by lenovo on 2017/5/26.
 */
public class TencentCookieUtilsTest {
    @Test
    public void getUid() throws Exception {
        String cookie = "pgv_pvi=1318846464;pgv_si=s2103787520;TDC_token=988344024;confirmuin=0;ptvfsession=f9c90fcdb16d53ee497eaa35bc70cd0fcf5dae7f6fa0f4f21170117ff7b4ad71c9963e7ed70c0977bd79c4db63503ebe5aeb67489dc05c43;ptdrvs=yqC78RNsAwrCiOsy5*KQFYMfYdZinYQjxvMfsNr*q0Q_;ptisp=cnc;uin=o2334713255;skey=@yPZBgC1V1;ETK=;superuin=o2334713255;superkey=GIEs0mzptGJVVHLmC38qqZoh9HwHFpatnEV7ZwIgOU8_;supertoken=2983224905;pt_recent_uins=5d1ab2706d447488d884b3f72c83be2f92e29033d15bfb763bae21fddbc17896ce30d7060d6aad972bca52baa569f2e1e01af32e5ea3b79f;RK=vBlCyQcu6k;ptnick_2334713255=64617461657965;ptcz=598f0a661dab811006c0b0e0404e0bbb8af5fca9ed39bbb6ffce2af2cce66ff3;pt2gguin=o2334713255;p_uin=o2334713255;pt4_token=scVd2gzmz9UU8eyTkilkF5aI78CM865deaHjURPzhto_;gdt_refer=ui.ptlogin2.qq.com;gdt_full_refer=http%3A%2F%2Fui.ptlogin2.qq.com%2Fcgi-bin%2Flogin%3Fstyle%3D9%26pt_ttype%3D1%26appid%3D15000103%26pt_no_auth%3D1%26pt_wxtest%3D1%26daid%3D5%26s_url%3Dhttp%253A%252F%252Fe.qq.com%252Fads;gdt_original_refer=ui.ptlogin2.qq.com;gdt_original_full_refer=http%3A%2F%2Fui.ptlogin2.qq.com%2Fcgi-bin%2Flogin%3Fstyle%3D9%26pt_ttype%3D1%26appid%3D15000103%26pt_no_auth%3D1%26pt_wxtest%3D1%26daid%3D5%26s_url%3Dhttp%253A%252F%252Fe.qq.com%252Fads;tsa_pgv_ssid=tsassid__1495532859252_387351373;tsa_pgv_pvid=tsapvid__1495532859252_539725802;site_type=new;hottag=;hottagtype=;portalversion=new;dm_cookie=version=new&log_type=internal_click&ssid=tsassid__1495532859252_387351373&pvid=tsapvid__1495532859252_539725802&qq=2334713255&loadtime=1083&url=http%3A%2F%2Fe.qq.com%2Fads%3Fsid%3DJ0p8iQv6n%2FcgbbgbtKtz0Tx6Q7nhZaP58b28e5a70201%3D%3D&gdt_refer=ui.ptlogin2.qq.com&gdt_full_refer=http%3A%2F%2Fui.ptlogin2.qq.com%2Fcgi-bin%2Flogin%3Fstyle%3D9%26pt_ttype%3D1%26appid%3D15000103%26pt_no_auth%3D1%26pt_wxtest%3D1%26daid%3D5%26s_url%3Dhttp%253A%252F%252Fe.qq.com%252Fads&gdt_original_refer=ui.ptlogin2.qq.com&gdt_original_full_refer=http%3A%2F%2Fui.ptlogin2.qq.com%2Fcgi-bin%2Flogin%3Fstyle%3D9%26pt_ttype%3D1%26appid%3D15000103%26pt_no_auth%3D1%26pt_wxtest%3D1%26daid%3D5%26s_url%3Dhttp%253A%252F%252Fe.qq.com%252Fads&gdt_from=&uid=4232591";
        String uid = TencentCookieUtils.getUid(cookie);
        System.out.println(uid);
    }

    @Test
    public void getUid2() throws Exception {
        String cookie = "tvfe_boss_uuid=4c45d0e08b7585be; pgv_pvi=8513253376; RK=tb+bmwmu2L; qq_slist_autoplay=on; gdt_original_refer=www.baidu.com; gdt_original_full_refer=https%3A%2F%2Fwww.baidu.com%2Flink%3Furl%3DfVgTWLb00bnTeLT4Ad-jgw7rML_wGcnG8Yd_UV5VHKiZvB-BYrQS0nEngmOaYAoI%26wd%3D%26eqid%3Dc8cbdec100070396000000065912efbf; pac_uid=1_2264093830; pgv_pvid=5750601844; o_cookie=2264093830; _qpsvr_localtk=tk2045; pgv_si=s9263064064; gdt_refer=e.qq.com; gdt_full_refer=http%3A%2F%2Fe.qq.com%2F; tsa_pgv_ssid=tsassid__1495185282104_825611672; pgv_info=ssid=s4159372870; ptui_loginuin=2334713255; pt2gguin=o2334713255; uin=o2334713255; skey=@KCblsgvtB; ptisp=ctc; ptcz=7cc63cceb8962de123fbc36cc0757888cc56189091f651c8bbed1809c85aa57c; atlas_platform=atlas; gdt_from=02_PINPAI_82; portalversion=400; dm_cookie=version=400&log_type=internal_click&ssid=tsassid__1495185282104_825611672&pvid=5750601844&qq=2334713255&loadtime=613&url=http%3A%2F%2Fe.qq.com%2Fads%2F400&gdt_refer=e.qq.com&gdt_full_refer=http%3A%2F%2Fe.qq.com%2F&gdt_original_refer=www.baidu.com&gdt_original_full_refer=https%3A%2F%2Fwww.baidu.com%2Flink%3Furl%3DfVgTWLb00bnTeLT4Ad-jgw7rML_wGcnG8Yd_UV5VHKiZvB-BYrQS0nEngmOaYAoI%26wd%3D%26eqid%3Dc8cbdec100070396000000065912efbf&gdt_from=02_PINPAI_82&uid=4232591&hottag=atlas&hottagtype=header; site_type=400; hottag=atlas; hottagtype=header";
        String uid = TencentCookieUtils.getUid(cookie);
        System.out.println(uid);
    }

}