package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.common.TrendChart;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import com.dataeye.ad.assistor.module.report.model.DeliveryTrendQuery;
import com.dataeye.ad.assistor.module.report.model.Period;
import com.dataeye.ad.assistor.module.report.model.PlanQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class DeliveryContrastiveAnalysisServiceImplTest {
    @Autowired
    private DeliveryContrastiveAnalysisService deliveryContrastiveAnalysisService;

    @Test
    public void costTrendGroupByMediumAccount() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, BigDecimal> trendChart = deliveryContrastiveAnalysisService.costTrendGroupByMediumAccount(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void costMinutelyTrendGroupByMediumAccount() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 6, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 6, 2, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.TwentyMinutes);
        TrendChart<String, BigDecimal> trendChart = deliveryContrastiveAnalysisService.costTrendGroupByMediumAccount(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void ctrTrendGroupByMediumAccount() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, Double> trendChart = deliveryContrastiveAnalysisService.ctrTrendGroupByMediumAccount(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void downloadRateTrendGroupByMediumAccount() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, Double> trendChart = deliveryContrastiveAnalysisService.downloadRateTrendGroupByMediumAccount(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void registrationTrendGroupByMediumAccount() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, Integer> trendChart = deliveryContrastiveAnalysisService.registrationTrendGroupByMediumAccount(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void cpaTrendGroupByMediumAccount() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, BigDecimal> trendChart = deliveryContrastiveAnalysisService.cpaTrendGroupByMediumAccount(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void rechargeTrendGroupByMediumAccount() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, BigDecimal> trendChart = deliveryContrastiveAnalysisService.rechargeTrendGroupByMediumAccount(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void costTrendGroupByPlan() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, BigDecimal> trendChart = deliveryContrastiveAnalysisService.costTrendGroupByPlan(deliveryTrendQuery);
        System.out.println(trendChart);
    }


    @Test
    public void ctrTrendGroupByPlan() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, Double> trendChart = deliveryContrastiveAnalysisService.ctrTrendGroupByPlan(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void downloadRateTrendGroupByPlan() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, Double> trendChart = deliveryContrastiveAnalysisService.downloadRateTrendGroupByPlan(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void registrationTrendGroupByPlan() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, Integer> trendChart = deliveryContrastiveAnalysisService.registrationTrendGroupByPlan(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void cpaTrendGroupByPlan() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, BigDecimal> trendChart = deliveryContrastiveAnalysisService.cpaTrendGroupByPlan(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void rechargeTrendGroupByPlan() throws Exception {
        DeliveryTrendQuery deliveryTrendQuery = new DeliveryTrendQuery();
        deliveryTrendQuery.setCompanyId(2);
        deliveryTrendQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        deliveryTrendQuery.setStartDate(startDate);
        deliveryTrendQuery.setEndDate(endDate);
        deliveryTrendQuery.setProductIds(Arrays.asList(1));
        deliveryTrendQuery.setPeriod(Period.Day);
        TrendChart<String, BigDecimal> trendChart = deliveryContrastiveAnalysisService.rechargeTrendGroupByPlan(deliveryTrendQuery);
        System.out.println(trendChart);
    }

    @Test
    public void listPlans() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 31, 0, 0, 0);
        Date endDate = calendar.getTime();
        PlanQuery planQuery = new PlanQuery();
        planQuery.setStartDate(startDate);
        planQuery.setEndDate(endDate);
        planQuery.setProductIds(Arrays.asList(1));
        planQuery.setMediumIds(Arrays.asList(1, 2, 3, 4));
        planQuery.setMediumAccountIds(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        List<Plan> plans = this.deliveryContrastiveAnalysisService.listPlans(planQuery);
        System.out.println(plans);
    }
}