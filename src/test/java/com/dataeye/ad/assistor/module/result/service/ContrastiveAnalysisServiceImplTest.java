package com.dataeye.ad.assistor.module.result.service;

import com.dataeye.ad.assistor.module.result.model.ContrastiveAnalysisQuery;
import com.dataeye.ad.assistor.module.result.model.TrendChart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by lenovo on 2017/4/10.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class ContrastiveAnalysisServiceImplTest {

    @Autowired
    private ContrastiveAnalysisService contrastiveAnalysisService;

    @Test
    public void dailyCostTrendGroupByOptimizer() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        ContrastiveAnalysisQuery query = new ContrastiveAnalysisQuery();
        query.setCompanyId(2);
        query.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setMediumIds(Arrays.asList(1, 2, 3));
        query.setProductIds(Arrays.asList(1));
        query.setOptimizers(Arrays.asList("zhangqing@lytxgame.com", "zhangkai@youdiancx.com", "zhoulong@lytxgame.com", "4005150@qq.com"));
        TrendChart<BigDecimal> trends = contrastiveAnalysisService.dailyCostTrendGroupByOptimizer(query);
        System.out.println(trends);
    }

    @Test
    public void dailyCtrTrendGroupByOptimizer() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        ContrastiveAnalysisQuery query = new ContrastiveAnalysisQuery();
        query.setCompanyId(2);
        query.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setMediumIds(Arrays.asList(1, 2, 3));
        query.setOptimizers(Arrays.asList("zhangqing@lytxgame.com", "zhangkai@youdiancx.com", "zhoulong@lytxgame.com"));
        TrendChart<BigDecimal> trends = contrastiveAnalysisService.dailyCtrTrendGroupByOptimizer(query);
        System.out.println(trends);
    }


    @Test
    public void dailyDownloadRateTrendGroupByOptimizer() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        ContrastiveAnalysisQuery query = new ContrastiveAnalysisQuery();
        query.setCompanyId(2);
        query.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setMediumIds(Arrays.asList(1, 2, 3));
        query.setOptimizers(Arrays.asList("zhangqing@lytxgame.com", "zhangkai@youdiancx.com", "zhoulong@lytxgame.com"));
        query.setProductIds(Arrays.asList(1));
        TrendChart<BigDecimal> trends = contrastiveAnalysisService.dailyDownloadRateTrendGroupByOptimizer(query);
        System.out.println(trends);
    }

    @Test
    public void dailyCpaTrendGroupByOptimizer() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        ContrastiveAnalysisQuery query = new ContrastiveAnalysisQuery();
        query.setCompanyId(2);
        query.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setMediumIds(Arrays.asList(1, 2, 3));
        query.setOptimizers(Arrays.asList("zhangqing@lytxgame.com", "zhangkai@youdiancx.com", "zhoulong@lytxgame.com"));
        query.setProductIds(Arrays.asList(1));
        TrendChart<BigDecimal> trends = contrastiveAnalysisService.dailyCpaTrendGroupByOptimizer(query);
        System.out.println(trends);
    }


    @Test
    public void dailySevenDayPaybackRateTrendGroupByOptimizer() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        ContrastiveAnalysisQuery query = new ContrastiveAnalysisQuery();
        query.setCompanyId(2);
        query.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setMediumIds(Arrays.asList(1, 2, 3));
        query.setOptimizers(Arrays.asList("zhangqing@lytxgame.com", "zhangkai@youdiancx.com", "zhoulong@lytxgame.com"));
        query.setProductIds(Arrays.asList(1));
        TrendChart<BigDecimal> trends = contrastiveAnalysisService.dailySevenDayPaybackRateTrendGroupByOptimizer(query);
        System.out.println(trends);
    }

}