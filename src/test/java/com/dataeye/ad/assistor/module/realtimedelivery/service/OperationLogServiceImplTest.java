package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.constant.OpCode;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLog;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLogQuery;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLogView;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;

/**
 * Created by huangzehai on 2017/6/28.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class OperationLogServiceImplTest {
    @Autowired
    private OperationLogService operationLogService;

    @Test
    public void addOperationLog() throws Exception {
        OperationLog log = new OperationLog();
        log.setCompanyId(2);
        log.setAccountId(10);
        log.setPlanId(81601);
        log.setOpCode(OpCode.BID);
        log.setPreviousValue("3.5");
        log.setCurrentValue("4");
        log.setResult(0);
        log.setOpTime(new Date());
        log.setMessage("两次出价不能小于5分钱");
        operationLogService.addOperationLog(log);
    }

    @Test
    public void listOperationLogs() {
        PageHelper.startPage(1, 2);
        DataPermissionDomain dataPermissionDomain = new DataPermissionDomain();
        dataPermissionDomain.setCompanyId(2);
        dataPermissionDomain.setOptimizerPermissionMediumAccountIds(new Integer[]{147});
        OperationLogQuery query = new OperationLogQuery();
        query.setCompanyId(dataPermissionDomain.getCompanyId());
        query.setOptimizerPermissionMediumAccountIds(dataPermissionDomain.getOptimizerPermissionMediumAccountIds());
        Page<OperationLogView> logs = operationLogService.listOperationLogs(query);
        System.out.println(logs.size());
    }

}