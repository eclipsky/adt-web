package com.dataeye.ad.assistor.module.payback.service;

import com.dataeye.ad.assistor.module.payback.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;

/**
 * Created by huangzehai on 2017/1/13.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class PaybackReportServiceImplTest {

    @Autowired
    private PaybackReportService paybackReportService;

    @Test
    public void dailyPaybackReport() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        PaybackQuery query = new PaybackQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setMediumIds(Arrays.asList(13));
        query.setAccountIds(Arrays.asList(40));
        query.setSize(10);
//        query.setProductIds(Arrays.asList(1));
        query.setPlanIds(Arrays.asList(80490, 80491, 80507));
        List<DailyPaybackReportView> report = paybackReportService.dailyPaybackReport(query);
        System.out.println(report.size());
    }


    @Test
    public void dailyPaybackReportWithAllMediumAndAllAccount() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 25, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 0, 16, 0, 0, 0);
        Date endDate = calendar.getTime();
        PaybackQuery query = new PaybackQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);

        query.setSize(10);
        List<DailyPaybackReportView> report = paybackReportService.dailyPaybackReport(query);
        System.out.println(report.size());
    }

    @Test
    public void dailyPaybackRateTrend() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 16, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        PaybackTrendQuery query = new PaybackTrendQuery();
        query.setDate(calendar.getTime());
        query.setProductIds(Arrays.asList(1));
        query.setPlanIds(Arrays.asList(80490));
        List<PaybackRate> paybackRates = this.paybackReportService.dailyPaybackRateTrend(query);
        System.out.println(paybackRates.size());
    }

    @Test
    public void monthlyPaybackReport() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 2, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 5, 7, 0, 0, 0);
        Date endDate = calendar.getTime();
        PaybackQuery query = new PaybackQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setSize(10);
//        query.setProductIds(Arrays.asList(1));
//        query.setPlanIds(Arrays.asList(80490));
        List<MonthlyPaybackReportView> report = paybackReportService.monthlyPaybackReport(query);
        System.out.println(report.size());
    }

}