package com.dataeye.ad.assistor.module.result.service;

import com.dataeye.ad.assistor.module.result.model.*;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/6.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class CoreIndicatorServiceImplTest {
    @Autowired
    private CoreIndicatorService coreIndicatorService;

    @Test
    public void getOverallCoreIndicator() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 11, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        OverallIndicator coreIndicator = coreIndicatorService.getOverallCoreIndicator(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getCostGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getCostGroupByMedium(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getRechargeGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getRechargeGroupByMedium(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getPaybackRateGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getPaybackRateGroupByMedium(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getBalanceGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 11, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getBalanceGroupByMedium(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getCpaGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getCpaGroupByMedium(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }


    @Test
    public void getDauGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getDauGroupByMedium(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getCostGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getCostGroupByProduct(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getRechargeGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 1, 12, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 19, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getRechargeGroupByProduct(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getPaybackRateGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getPaybackRateGroupByProduct(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getBalanceGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 11, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getBalanceGroupByProduct(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getCpaGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getCpaGroupByProduct(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }


    @Test
    public void getDauGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 12, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 19, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getDauGroupByProduct(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getCostGroupByOs() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getCostGroupByOs(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getRechargeGroupByOs() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getRechargeGroupByOs(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getPaybackRateGroupByOs() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getPaybackRateGroupByOs(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void getCpaGroupByOs() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getCpaGroupByOs(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }


    @Test
    public void getDauGroupByOs() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        List<Indicator> coreIndicator = coreIndicatorService.getDauGroupByOs(coreIndicatorQuery);
        System.out.println(coreIndicator);
    }

    @Test
    public void dailyCostTrendGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyCostTrendGroupByMedium(coreIndicatorQuery);
        System.out.println(trendChart);
    }

    @Test
    public void dailyRechargeTrendGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyRechargeTrendGroupByMedium(coreIndicatorQuery);
        System.out.println(trendChart);
    }

    @Test
    public void dailyBalanceTrendGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 11, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyBalanceTrendGroupByMedium(coreIndicatorQuery);
        System.out.println(trendChart);
    }

    @Test
    public void dailyCpaTrendGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyCpaTrendGroupByMedium(coreIndicatorQuery);
        System.out.println(trendChart);
    }


    @Test
    public void dailyDauTrendGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyDauTrendGroupByMedium(coreIndicatorQuery);
        System.out.println(trendChart);
    }

    @Test
    public void dailyCostTrendGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyCostTrendGroupByProduct(coreIndicatorQuery);
        System.out.println(trendChart);
    }

    @Test
    public void dailyRechargeTrendGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyRechargeTrendGroupByProduct(coreIndicatorQuery);
        System.out.println(trendChart);
    }

    @Test
    public void dailyBalanceTrendGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 11, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyBalanceTrendGroupByProduct(coreIndicatorQuery);
        System.out.println(trendChart);
    }

    @Test
    public void dailyCpaTrendGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyCpaTrendGroupByProduct(coreIndicatorQuery);
        System.out.println(trendChart);
    }


    @Test
    public void dailyDauTrendGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyDauTrendGroupByProduct(coreIndicatorQuery);
        System.out.println(trendChart);
    }

    @Test
    public void dailyCostTrendGroupByOs() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyCostTrendGroupByOs(coreIndicatorQuery);
        System.out.println(trendChart);
    }

    @Test
    public void dailyRechargeTrendGroupByOs() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyRechargeTrendGroupByOs(coreIndicatorQuery);
        System.out.println(trendChart);
    }


    @Test
    public void dailyCpaTrendGroupByOs() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyCpaTrendGroupByOs(coreIndicatorQuery);
        System.out.println(trendChart);
    }


    @Test
    public void dailyDauTrendGroupByOs() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 2, 31, 0, 0, 0);
        Date endDate = calendar.getTime();

        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(2);
        coreIndicatorQuery.setPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        TrendChart<BigDecimal> trendChart = coreIndicatorService.dailyDauTrendGroupByOs(coreIndicatorQuery);
        System.out.println(trendChart);
    }

}