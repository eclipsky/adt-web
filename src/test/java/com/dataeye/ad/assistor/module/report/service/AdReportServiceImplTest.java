package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.module.report.constant.Defaults;
import com.dataeye.ad.assistor.module.report.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;

/**
 * Created by huangzehai on 2016/12/27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class AdReportServiceImplTest {
    @Autowired
    private AdReportService adReportService;

    @Test
    public void planDailyReportDetail() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 5, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 5, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setAccountIds(accountIds());
        query.setProductIds(null);
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.planDailyReportDetail(query);
        System.out.println(report.size());
    }

    @Test
    public void planDailyReportWithQuery() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        Long accountID = 2L;
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setAccountIds(accountIds());
        query.setQuery("7W");
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.planDailyReportDetail(query);
        System.out.println(report.size());
    }

    @Test
    public void planDailyReportWithoutAccount() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setAccountIds(null);
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.planDailyReportDetail(query);
        System.out.println(report.size());
    }

    @Test
    public void ignoreZeroCostPlan() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();

        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setIgnoreZeroCostPlan(true);
        query.setAccountIds(accountIds());
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.planDailyReportDetail(query);
        System.out.println(report.size());
    }


    @Test
    public void ignoreUnboundPlan() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setAccountIds(accountIds());
        query.setIgnoreUnboundPlan(true);
        query.setPageSize(100);
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.planDailyReportDetail(query);
        System.out.println(report.size());
    }

    @Test
    public void planSummaryReport() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setAccountIds(accountIds());
        query.setIgnoreUnboundPlan(true);
        query.setPageSize(100);
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        PageData report = adReportService.planSummaryReport(query);
        System.out.println(report);
    }

    @Test
    public void mediumSummaryReport() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
//        query.setAccountID(2L);
        query.setIgnoreUnboundPlan(true);
        query.setPageSize(100);
        query.setQuery("头条");
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        PageData report = adReportService.mediumSummaryReport(query);
        System.out.println(report);
    }

    @Test
    public void mediumDailyReport() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setQuery("智");
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        PageData report = adReportService.mediumDailyReport(query);
        System.out.println(report);
    }

    @Test
    public void mediumDailyReportExpandMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 18, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 0, 18, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setQuery("智");
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.mediumDailyReportExpandMedium(query);
        System.out.println(report.size());
    }

    @Test
    public void mediumDailyReportExpandEventGroup() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setMediumIds(mediumIds());
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.mediumDailyReportExpandEventGroup(query);
        System.out.println(report.size());
    }

    @Test
    public void mediumDailyReportExpandAccountAndPlan() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 0, 06, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 0, 06, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setMediumIds(mediumIds());
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        Object report = adReportService.mediumDailyReportExpandAccountAndPlan(query);
        System.out.println(report);
    }

    @Test
    public void planDailyReport() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
//        query.setAccountID(2L);
        query.setIgnoreUnboundPlan(true);
        query.setPageSize(100);
        query.setOrderBy("date");
//        query.setQuery("头条");
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        PageData report = adReportService.planDailyReport(query);
        System.out.println(report);
    }

    @Test
    public void planSummaryReportDetail() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setIgnoreUnboundPlan(true);
//        query.setAccount("2642615624");
        query.setAccountIds(accountIds());
//        query.setPlan("1H-1025");
        query.setPlanId(994);
        query.setOrderBy(Defaults.DATE);
        query.setOrder(Defaults.ASC);
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.planSummaryReportDetail(query);
        System.out.println(report.size());
    }

    @Test
    public void mediumSummaryReportExpandDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setIgnoreUnboundPlan(true);
        query.setMediumIds(mediumIds());
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.mediumSummaryReportExpandDate(query);
        System.out.println(report.size());
    }

    @Test
    public void mediumSummaryReportExpandEventGroup() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setIgnoreUnboundPlan(true);
        query.setMediumIds(mediumIds());
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.mediumSummaryReportExpandEventGroup(query);
        System.out.println(report.size());
    }

    @Test
    public void mediumSummaryReportExpandAccountAndPlan() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setIgnoreUnboundPlan(true);
        query.setMediumIds(mediumIds());
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        Object report = adReportService.mediumSummaryReportExpandAccountAndPlan(query);
        System.out.println(report);
    }

    @Test
    public void mediumSummaryReportExpandEventGroupNotDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setIgnoreUnboundPlan(true);
        query.setMediumIds(mediumIds());
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        List<DeliveryReportView> report = adReportService.mediumSummaryReportExpandEventGroupNotDate(query);
        System.out.println(report.size());
    }

    @Test
    public void mediumSummaryReportExpandAccountAndPlanNotDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setIgnoreUnboundPlan(true);
        query.setMediumIds(mediumIds());
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        Object report = adReportService.mediumSummaryReportExpandAccountAndPlanNotDate(query);
        System.out.println(report);
    }

    @Test
    public void adReport() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setAccountIds(accountIds());
        query.setIgnoreUnboundPlan(true);
        query.setPageSize(100);
        query.setOrderBy(Defaults.TOTAL_COST);
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        PageData report = adReportService.adReport(View.Daily, Dimension.Plan, query);
        System.out.println(report);
    }

    @Test
    public void adReportDetail() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, 11, 16, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2016, 11, 26, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setAccountIds(accountIds());
        query.setIgnoreUnboundPlan(true);
        query.setPageSize(100);
        query.setOrderBy(Defaults.DATE);
        query.setProductIds(Arrays.asList(1));
        query.setAccountId(10);
        Object report = adReportService.adReportDetail(View.Daily, Dimension.Plan, LinkID.Plan, query);
        System.out.println(report);
    }

    /**
     * 生成测试账号ID列表
     *
     * @return
     */
    private List<Integer> accountIds() {
        List<Integer> accountIds = new ArrayList<>();
        accountIds.add(2);
        accountIds.add(3);
        return accountIds;
    }

    private List<Integer> mediumIds() {
        List<Integer> mediumIds = new ArrayList<>();
        //今日头条
        mediumIds.add(2);
        return mediumIds;
    }
}