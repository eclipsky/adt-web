package com.dataeye.ad.assistor.module.mediumaccount.init;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by huangzehai on 2017/1/11.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class TransferMediumAccountHandlerTest {

    @Autowired
    private TransferMediumAccountHandler handler;

    @Test
    public void init() throws Exception {
    	boolean flag = handler.transfer();
    	System.out.println(flag);
    }

}