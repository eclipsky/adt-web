package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceAlertConf;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

/**
 * Created by huangzehai on 2017/4/28.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class BalanceAlertServiceImplTest {

    @Autowired
    private BalanceAlertService balanceAlertService;

    @Test
    public void saveBalanceAlertConf() throws Exception {
        BalanceAlertConf balanceAlertConf = new BalanceAlertConf();
        balanceAlertConf.setAccountId(1);
        balanceAlertConf.setCompanyId(2);
        balanceAlertConf.setEmailAlert(0);
        int result = balanceAlertService.saveBalanceAlertConf(balanceAlertConf);
        System.out.println(result);
    }

    @Test
    public void getBalanceAlertConfByAccountId() throws Exception {
        BalanceAlertConf balanceAlertConf = balanceAlertService.getBalanceAlertConfByAccountId(1);
        System.out.println(balanceAlertConf);
    }

    @Test
    public void sentBalanceWarningEmail() throws Exception {
        balanceAlertService.sentBalanceWarningEmail();
    }

}