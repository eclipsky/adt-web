package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.report.model.IndicatorPreferenceQuery;
import com.dataeye.ad.assistor.module.report.model.UpsAndDownsPreference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

/**
 * Created by huangzehai on 2017/7/6.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class UpsAndDownsPreferenceServiceImplTest {
    @Autowired
    private UpsAndDownsPreferenceService upsAndDownsPreferenceService;

    @Test
    public void getUpsAndDownsPreference() throws Exception {
        IndicatorPreferenceQuery query = new IndicatorPreferenceQuery();
        query.setAccountId(8);
        query.setAccountRole(2);
        query.setCompanyId(2);
        upsAndDownsPreferenceService.getUpsAndDownsPreference(query);
    }

    @Test
    public void updateUpsAndDownsPreference() throws Exception {
        UpsAndDownsPreference upsAndDownsPreference = new UpsAndDownsPreference();
        upsAndDownsPreference.setCompanyId(2);
        upsAndDownsPreference.setAccountId(2);
        upsAndDownsPreference.setMinutes(10);
        upsAndDownsPreference.setIndicatorVisibility("1:1,2:0,3:1");
        upsAndDownsPreferenceService.updateUpsAndDownsPreference(upsAndDownsPreference);
    }

    @Test
    public void hasUpsAndDownsPreference() throws Exception {
        boolean hasPreference = upsAndDownsPreferenceService.hasUpsAndDownsPreference(2);
        System.out.println(hasPreference);
    }

    @Test
    public void insertUpsAndDownsPreference() throws Exception {
        UpsAndDownsPreference upsAndDownsPreference = new UpsAndDownsPreference();
        upsAndDownsPreference.setCompanyId(2);
        upsAndDownsPreference.setAccountId(2);
        upsAndDownsPreference.setMinutes(10);
        upsAndDownsPreference.setIndicatorVisibility("1:1,2:0");
        upsAndDownsPreferenceService.insertUpsAndDownsPreference(upsAndDownsPreference);
    }

    @Test
    public void deleteUpsAndDownsPreference() throws Exception {
        upsAndDownsPreferenceService.deleteUpsAndDownsPreference(2);
    }

}