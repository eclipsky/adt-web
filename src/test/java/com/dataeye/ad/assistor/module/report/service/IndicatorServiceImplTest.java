package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.module.report.model.Indicator;
import com.dataeye.ad.assistor.module.report.model.IndicatorHabit;
import com.dataeye.ad.assistor.module.report.model.IndicatorPreferenceQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * Created by huangzehai on 2017/6/8.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class IndicatorServiceImplTest {
    @Autowired
    private IndicatorService indicatorService;

    @Test
    public void updateIndicatorPreference() throws Exception {
        IndicatorHabit indicatorHabit = new IndicatorHabit();
        indicatorHabit.setAccountId(2);
        indicatorHabit.setCompanyId(2);
        indicatorHabit.setMenuId("4-1");
        indicatorHabit.setIndicatorHabit("1-1:1,1-2:0,1-3:1,1-4:1,1-5:0");
        int result = indicatorService.updateIndicatorPreference(indicatorHabit);
        System.out.println(result);
    }

    @Test
    public void getIndicatorPreference() throws Exception {
        IndicatorPreferenceQuery query = new IndicatorPreferenceQuery();
        query.setAccountId(2);
        query.setMenuId("4-1");
        List<Indicator> indicators = indicatorService.getIndicatorPreference(query);
        System.out.println(indicators);
    }

    @Test
    public void addIndicatorPreference() throws Exception {
        indicatorService.addIndicatorPreference(2, 5, 0, "1-1,1-2,1-3,1-4,1-5,1-6,1-7,1-8,1-10,2-1,2-2,2-3,2-4,3-1,3-2,3-3,3-4,3-5,3-6,4-1,4-2,4-3,4-4,5-1,5-2,5-3,5-4,5-5,5-6,5-7,6-1");
    }

    @Test
    public void updateIndicatorPreferenceForPermissionChanged() throws Exception {
        String indicatorPermission = "1-1,1-4,1-5,1-6,1-7,1-8,1-10,2-1,2-2,2-3,2-4,3-1,3-2,3-3,3-4,3-5,3-6,4-1,4-2,4-3,4-4,5-1,5-2,5-3,5-4,5-5,5-6,6-1";
        indicatorService.updateIndicatorPreference(2, 3, 0, indicatorPermission);
    }

}