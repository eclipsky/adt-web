package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.model.*;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Map;

/**
 * Created by lenovo on 2017/2/27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class RealTimeReportServiceImplTest {
    @Autowired
    private RealTimeReportService realTimeReportService;

    @Test
    public void realTimeReport() throws Exception {
        Map<Period, Map<Integer, RealTimeReport>> reports = realTimeReportService.getLastPeriodIndicators();
        System.out.println(reports);
    }

    @Test
    public void realTimeReportWithPermission() throws Exception {
        int companyId = 2;
        DataPermissionDomain dataPermissionDomain = new DataPermissionDomain();
        dataPermissionDomain.setCompanyId(companyId);
        Map<Period, Map<Integer, RealTimeReport>> reports = realTimeReportService.getLastPeriodIndicators(dataPermissionDomain);
        System.out.println(reports);
    }

    @Test
    public void testTime() {
        int interval = 30;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -interval);
        int minute = calendar.get(Calendar.MINUTE);
        int min = minute / interval * interval;
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        System.out.println(calendar.getTime());
    }

    @Test
    public void getIndicatorAtTime() throws Exception {
        RealTimeReportQuery query = new RealTimeReportQuery();
        query.setCompanyId(2);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(2017, 6, 5, 10, 20, 0);
        query.setStartTime(calendar.getTime());
        Map<Integer, RealTimeReport> indicators = realTimeReportService.getIndicatorAtTime(query);
        System.out.println(indicators.size());
    }

    @Test
    public void test(){
        BigDecimal num = new BigDecimal(-1.22).setScale(1,RoundingMode.HALF_UP);
        System.out.println(num);
    }

}