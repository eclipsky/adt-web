package com.dataeye.ad.assistor.module.report.validtor;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by lenovo on 2017/5/9.
 */
public class SqlValidatorTest {
    @Test
    public void isValidOrderBy() throws Exception {
        Assert.assertTrue(SqlValidator.isValidOrderBy("balance"));
        Assert.assertTrue(SqlValidator.isValidOrderBy("account_name"));
        Assert.assertTrue(SqlValidator.isValidOrderBy("p.account_name"));
        Assert.assertTrue(SqlValidator.isValidOrderBy("p.id,p.account_name"));
        Assert.assertTrue(SqlValidator.isValidOrderBy("p.id, p.account_name"));
        Assert.assertFalse(SqlValidator.isValidOrderBy(";drop table abc"));
    }

    @Test
    public void isValidOrder() throws Exception {
        Assert.assertTrue(SqlValidator.isValidOrder("asc"));
        Assert.assertTrue(SqlValidator.isValidOrder("asc"));
        Assert.assertTrue(SqlValidator.isValidOrder("ASC"));
        Assert.assertTrue(SqlValidator.isValidOrder("DESC"));
        Assert.assertFalse(SqlValidator.isValidOrder("abc"));
        Assert.assertTrue(SqlValidator.isValidOrder(null));
    }

}