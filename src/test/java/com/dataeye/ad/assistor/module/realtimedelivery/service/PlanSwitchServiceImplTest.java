package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.Medium;
import com.dataeye.ad.assistor.module.realtimedelivery.model.DeliveryStatus;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by huangzehai on 2017/5/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class PlanSwitchServiceImplTest {

    @Autowired
    @Qualifier("planSwitchService")
    private PlanSwitchService planSwitchService;

    @Test
    public void updateStatusForTui() throws Exception {
        DeliveryStatus status = new DeliveryStatus();
        status.setMediumId(3);
        status.setMediumAccountId(36);
        status.setMediumPlanId(601214115L);
        status.setStatus(OperationStatus.Disable);
        Result result = planSwitchService.updateStatus(status);
        System.out.println(result);
    }

    @Test
    public void updateStatusForToutiao() throws Exception {
        DeliveryStatus status = new DeliveryStatus();
        status.setMediumId(2);
        status.setMediumAccountId(156);
        status.setMediumPlanId(61132407974L);
        status.setStatus(OperationStatus.Disable);
        status.setPlanId(81900);
        status.setCompanyId(2);
        status.setAccountId(8);
        Result result = planSwitchService.updateStatus(status);
        System.out.println(result);
    }

    @Test
    public void planSwitchForBaidu() throws Exception {
        DeliveryStatus status = new DeliveryStatus();
        status.setMediumId(14);
        status.setMediumAccountId(176);
        status.setMediumPlanId(2462795186L);
        status.setStatus(OperationStatus.Enable);
        Result result = planSwitchService.updateStatus(status);
        System.out.println(result);
    }

    @Test
    public void planSwitchForTencent() throws Exception {
        DeliveryStatus status = new DeliveryStatus();
        status.setMediumId(Medium.Tencent);
        status.setMediumAccountId(216);
        status.setMediumPlanId(29491025L);
        status.setStatus(OperationStatus.Disable);
        Result result = planSwitchService.updateStatus(status);
        System.out.println(result);
    }

}