package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.BidStrategy;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.Medium;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Bid;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;

/**
 * Created by huangzehai on 2017/5/11.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class BidServiceImplTest {
    @Autowired
    @Qualifier("bidService")
    private BidService bidService;

    @Test
    public void toutiaoOCPCBid() throws Exception {
        Bid bid = new Bid();
        bid.setMediumId(2);
        bid.setMediumAccountId(156);
        bid.setMediumPlanId(60413662076L);
        bid.setBid(new BigDecimal(0.36));
        bid.setBidStrategy("OCPC");
        bid.setIsCpaBid(0);
        Result bidResult = bidService.bid(bid);
//        Assert.assertTrue(bidResult.isSuccess());
        System.out.println(bidResult);
    }

    @Test
    public void toutiaoCPCBid() throws Exception {
        Bid bid = new Bid();
        bid.setMediumId(2);
        bid.setMediumAccountId(156);
        bid.setMediumPlanId(60487154510L);
        bid.setBid(new BigDecimal(0.36));
        bid.setBidStrategy("CPC");
        bid.setCompanyId(2);
        bid.setAccountId(8);
        bid.setPlanId(11);
//        bid.setIsCpaBid(0);
        Result bidResult = bidService.bid(bid);
//        Assert.assertTrue(bidResult.isSuccess());
        System.out.println(bidResult);
    }

    @Test
    public void toutiaoCpmBid() throws Exception {
        Bid bid = new Bid();
        bid.setMediumId(2);
        bid.setMediumAccountId(156);
        bid.setMediumPlanId(60496367556L);
        bid.setBid(new BigDecimal(4));
        bid.setBidStrategy("CPM");
//        bid.setIsCpaBid(0);
        Result bidResult = bidService.bid(bid);
//        Assert.assertTrue(bidResult.isSuccess());
        System.out.println(bidResult);
    }


    @Test
    public void toutiaoCpvBid() throws Exception {
        Bid bid = new Bid();
        bid.setMediumId(2);
        bid.setMediumAccountId(156);
        bid.setMediumPlanId(60497089844L);
        bid.setBid(new BigDecimal(0.21));
        bid.setBidStrategy(BidStrategy.CPV);
//        bid.setIsCpaBid(0);
        Result bidResult = bidService.bid(bid);
//        Assert.assertTrue(bidResult.isSuccess());
        System.out.println(bidResult);
    }

    @Test
    public void toutiaoCpaBid() throws Exception {
        Bid bid = new Bid();
        bid.setMediumId(2);
        bid.setMediumAccountId(156);
        bid.setMediumPlanId(61132407974L);
        bid.setBid(new BigDecimal(1.02));
        bid.setBidStrategy(BidStrategy.CPA);
        bid.setPlanId(81900);
        Result bidResult = bidService.bid(bid);
//        Assert.assertTrue(bidResult.isSuccess());
        System.out.println(bidResult);
    }

    /**
     * 测试智汇推出价.
     *
     * @throws Exception
     */
    @Test
    public void tuiBid() throws Exception {
        Bid bid = new Bid();
        bid.setMediumId(3);
        bid.setMediumAccountId(36);
        bid.setMediumPlanId(601214115L);
        bid.setBid(new BigDecimal(0.51));
        Result bidResult = bidService.bid(bid);
        System.out.println(bidResult);
    }

    /**
     * 百度信息流推广出价测试.
     *
     * @throws Exception
     */
    @Test
    public void baiduBid() throws Exception {
        Bid bid = new Bid();
        bid.setMediumId(Medium.BaiduFeedAds);
        bid.setMediumAccountId(176);
        bid.setMediumPlanId(2462795186L);
        bid.setBid(new BigDecimal(1.50));
        bid.setBidStrategy(BidStrategy.CPC);
        Result bidResult = bidService.bid(bid);
        System.out.println(bidResult);
    }

    @Test
    public void tencentCpmBid() throws Exception {
        Bid bid = new Bid();
        bid.setMediumId(Medium.Tencent);
        bid.setMediumAccountId(216);
        bid.setMediumPlanId(29491025L);
        bid.setBid(new BigDecimal(60));
        bid.setBidStrategy(BidStrategy.CPM);
        Result bidResult = bidService.bid(bid);
        System.out.println(bidResult);
    }

}