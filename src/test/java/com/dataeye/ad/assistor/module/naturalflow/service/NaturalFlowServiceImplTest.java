package com.dataeye.ad.assistor.module.naturalflow.service;

import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlowQuery;
import com.dataeye.ad.assistor.module.report.model.DateRangeQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2017/7/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class NaturalFlowServiceImplTest {
    @Autowired
    private NaturalFlowService naturalFlowService;

    @Test
    public void getNaturalFlows() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 6, 17, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 6, 17, 0, 0, 0);
        Date endDate = calendar.getTime();
        NaturalFlowQuery query = new NaturalFlowQuery();
        query.setCompanyId(2);
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProductAndDate(query);
        System.out.println(naturalFlows);
    }

    @Test
    public void getNaturalFlowsGroupByDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 6, 17, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 6, 17, 0, 0, 0);
        Date endDate = calendar.getTime();
        NaturalFlowQuery query = new NaturalFlowQuery();
        query.setCompanyId(2);
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByDate(query);
        System.out.println(naturalFlows);
    }


    @Test
    public void getNaturalFlowRecharge() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 6, 17, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 6, 21, 0, 0, 0);
        Date endDate = calendar.getTime();
        DateRangeQuery query = new DateRangeQuery();
        query.setCompanyId(2);
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        BigDecimal naturalFlowRecharge = naturalFlowService.getNaturalFlowRecharge(query);
        System.out.println(naturalFlowRecharge);
    }

    @Test
    public void getNaturalFlowTotal() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 6, 17, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 6, 24, 0, 0, 0);
        Date endDate = calendar.getTime();
        NaturalFlowQuery query = new NaturalFlowQuery();
        query.setCompanyId(2);
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        NaturalFlow naturalFlow = naturalFlowService.getNaturalFlowTotal(query);
        System.out.println(naturalFlow);
    }

}