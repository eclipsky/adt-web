package com.dataeye.ad.assistor.module.realtimedelivery.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.dataeye.ad.assistor.module.realtimedelivery.model.AlertConf;
import com.dataeye.ad.assistor.module.realtimedelivery.model.AlertQuery;
import com.dataeye.ad.assistor.module.realtimedelivery.model.AlertType;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeDeliveryFormatVo;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeDeliveryVo;

/**
 * Created by luzhuyou on 2017/01/02.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class DeliveryServiceTest {

    @Autowired
    private DeliveryService deliveryService;


    @Test
    public void testQueryDelivery() throws Exception {
        int companyId = 2;
        String medium = "13";
        String account = "177";
        String currentDate = "2017-05-16";
        Integer[] permissionMediumAccountIds = new Integer[]{40, 177, 178};
        List<Integer> productIds = null;
        List<Integer> planIds = null;
        List<RealTimeDeliveryVo> realTimeDeliveryList = deliveryService.queryDelivery(companyId, medium, account, currentDate, permissionMediumAccountIds, productIds,false,planIds);
        List<RealTimeDeliveryFormatVo> formatResultList = new ArrayList<>();
        for (RealTimeDeliveryVo vo : realTimeDeliveryList) {
            formatResultList.add(new RealTimeDeliveryFormatVo(vo));
        }
        System.out.println("testQueryDelivery result====" + formatResultList);
    }

    @Test
    public void testAddAlert() throws Exception {
        int planID = 11;
        BigDecimal consume = new BigDecimal(1.05);
        BigDecimal costPerRegistration = new BigDecimal(0.1);
        BigDecimal ctr = new BigDecimal(90.01);
        BigDecimal downloadRate = new BigDecimal(90.01);
        int emailAlert = 1;
        int result = deliveryService.addAlert(planID, consume, costPerRegistration,
                ctr, downloadRate, emailAlert);
        System.out.println("testAddAlert result====" + result);
    }

    @Test
    public void testAddAlertConfiguration() {
        AlertConf alertConf = new AlertConf();
        alertConf.setPlanID(3);
        alertConf.setCost(new BigDecimal(1231.0));
        alertConf.setCostPerRegistration(new BigDecimal(45.89));
        alertConf.setCtr(new BigDecimal(1232.2));
        alertConf.setDownloadRate(new BigDecimal(80.9));
        alertConf.setEmailAlert(1);
        alertConf.setCostPerDownload(new BigDecimal(123.89));
        alertConf.setPeriod(1);
        alertConf.setType(1);
        alertConf.setCostOperator(1);
        alertConf.setCpaOperator(1);
        alertConf.setCtrOperator(1);
        alertConf.setDownloadRateOperator(1);
        alertConf.setCostPerDownloadOperator(1);
        deliveryService.addAlertConfiguration(alertConf);
    }

    @Test
    public void testBatchInsertAlertConfigurations() {
        AlertConf alertConf = new AlertConf();
        alertConf.setPlanID(3);
        alertConf.setCost(new BigDecimal(1231.0));
        alertConf.setCostPerRegistration(new BigDecimal(45.89));
        alertConf.setCtr(new BigDecimal(1232.2));
        alertConf.setDownloadRate(new BigDecimal(80.9));
        alertConf.setEmailAlert(1);
        alertConf.setCostPerDownload(new BigDecimal(123.89));
        alertConf.setPeriod(1);
        alertConf.setType(1);
        alertConf.setCostOperator(1);
        alertConf.setCpaOperator(1);
        alertConf.setCtrOperator(1);
        alertConf.setDownloadRateOperator(1);
        alertConf.setCostPerDownloadOperator(1);

        List<Integer> planIds = Arrays.asList(81506, 80250);
        deliveryService.addAlertConfigurations(alertConf, planIds);
    }

    @Test
    public void testModifyAlert() throws Exception {
        int id = 2;
        BigDecimal consume = new BigDecimal(31.05);
        BigDecimal costPerRegistration = new BigDecimal(0.75);
        BigDecimal ctr = new BigDecimal(0.43);
        BigDecimal downloadRate = new BigDecimal(0.45);
        int emailAlert = 1;
        int result = deliveryService.modifyAlert(id, consume, costPerRegistration,
                ctr, downloadRate, emailAlert);
        System.out.println("testModifyAlert result===" + result);
    }

    @Test
    public void updateAlertConfiguration() throws Exception {
        AlertConf alertConf = new AlertConf();
        alertConf.setId(306);
        alertConf.setPlanID(3);
        alertConf.setCost(new BigDecimal(5698.90));
        alertConf.setCostPerRegistration(new BigDecimal(45.89));
        alertConf.setCtr(new BigDecimal(1232.2));
        alertConf.setDownloadRate(new BigDecimal(99.36));
        alertConf.setEmailAlert(1);
        alertConf.setCostPerDownload(new BigDecimal(123.89));
        alertConf.setPeriod(1);
        alertConf.setType(1);
        alertConf.setCostOperator(1);
        alertConf.setCpaOperator(1);
        alertConf.setCtrOperator(0);
        alertConf.setDownloadRateOperator(1);
        alertConf.setCostPerDownloadOperator(1);
        deliveryService.updateAlertConfiguration(alertConf);
    }

    @Test
    public void testGetAlert() throws Exception {
        Long planID = 3L;
        AlertQuery alertQuery = new AlertQuery(planID, AlertType.General);
        AlertConf conf = deliveryService.getAlert(alertQuery);
        System.out.println("testGetAlert　result===" + conf);
    }

    @Test
    public void testAddOrModifyAlertEmail() throws Exception {
        int id = 2;
        String email = "luzhuyou@dataeye.com";
        int result = deliveryService.addOrModifyAlertEmail(id, email);
        System.out.println("testAddOrModifyAlertEmail　result===" + result);
    }

    @Test
    public void testMonitor() throws Exception {
        Calendar calendar = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = df.format(calendar.getTime());
        deliveryService.monitor(currentDate);
        Thread.sleep(20000);
        System.out.println("testAddOrModifyAlertEmail　result===ok");
    }
}