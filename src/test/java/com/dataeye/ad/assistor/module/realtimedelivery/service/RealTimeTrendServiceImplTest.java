package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by huangzehai on 2017/2/24.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class RealTimeTrendServiceImplTest {

    @Autowired
    private RealTimeTrendService realTimeTrendService;

    /**
     * @throws Exception
     */
    @Test
    public void realTimeTrend() throws Exception {
        TrendQuery trendQuery = new TrendQuery();
        trendQuery.setPlanId(1330);
        trendQuery.setPeriod(Period.Hour);
        List<RealTimeTrend> trends = realTimeTrendService.realTimeTrend(trendQuery);
        System.out.println(trends);
    }

    @Test
    public void realTimeTrendChart() throws Exception {
        TrendQuery trendQuery = new TrendQuery();
        trendQuery.setPlanId(21);
        trendQuery.setPeriod(Period.TwentyMinutes);
        TrendIndicator[] indicators = new TrendIndicator[]{TrendIndicator.Clicks, TrendIndicator.CPA};
        trendQuery.setIndicators(indicators);
        RealTimeTrendChart<BigDecimal, Object> chart = realTimeTrendService.realTimeTrendChart(trendQuery);
        System.out.println(chart);
    }

}