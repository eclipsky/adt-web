package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.common.TrendChart;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Balance;
import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceQuery;
import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceTrendQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/19.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class BalanceServiceImplTest {
    @Autowired
    private BalanceService balanceService;

    @Test
    public void listAccountBalance() throws Exception {
        BalanceQuery query = new BalanceQuery();
        query.setCompanyId(2);
        query.setOptimizerPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        List<Balance> balances = balanceService.listAccountBalance(query);
        System.out.println(balances);
    }

    @Test
    public void balanceTrend() throws Exception {
        BalanceTrendQuery balanceTrendQuery = new BalanceTrendQuery();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 3, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 3, 27, 0, 0, 0);
        Date endDate = calendar.getTime();
        balanceTrendQuery.setStartDate(startDate);
        balanceTrendQuery.setEndDate(endDate);
        balanceTrendQuery.setCompanyId(2);
        balanceTrendQuery.setOptimizerPermissionMediumAccountIds(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        TrendChart<String, BigDecimal> balances = balanceService.balanceTrend(balanceTrendQuery);
        System.out.println(balances);
    }

}