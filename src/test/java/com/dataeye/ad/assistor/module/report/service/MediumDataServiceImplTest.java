package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.module.report.model.CommonQuery;
import com.dataeye.ad.assistor.module.report.model.MediumData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class MediumDataServiceImplTest {

    @Autowired
    private MediumDataService mediumDataService;

    @Test
    public void listMediumData() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 7, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 7, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        CommonQuery query = new CommonQuery();
        query.setCompanyId(2);
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        List<MediumData> mediumDataList = mediumDataService.listMediumData(query);
        System.out.println(mediumDataList);
    }

    @Test
    public void updateMediumData() throws Exception {
        MediumData mediumData = new MediumData();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 7, 22, 0, 0, 0);
        mediumData.setStatDate(calendar.getTime());
        mediumData.setPlanId(102890);
        mediumData.setPlanSource(0);
        mediumData.setCost(new BigDecimal(2017));
        mediumData.setExposures(965);
        mediumData.setClicks(456);
        List<MediumData> mediumDataList = new ArrayList<>();
        mediumDataList.add(mediumData);
        mediumDataService.updateMediumData(mediumDataList);
    }

}