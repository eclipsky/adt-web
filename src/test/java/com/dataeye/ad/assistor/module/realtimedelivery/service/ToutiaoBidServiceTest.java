package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Bid;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;

/**
 * Created by huangzehai on 2017/5/11.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class ToutiaoBidServiceTest {

    @Autowired
    @Qualifier("toutiaoBidService")
    private BidService bidService;

    @Test
    public void testBid() {
        Bid bid = new Bid();
        bid.setMediumAccountId(181);
        bid.setBid(new BigDecimal(1.56));
        Result bidResult = bidService.bid(bid);
//        Assert.assertTrue(bidResult.isSuccess());
        System.out.println(bidResult);
    }


}