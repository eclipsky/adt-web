package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.module.report.model.DeliveryKeyIndicatorQuery;
import com.dataeye.ad.assistor.module.result.model.Indicator;
import com.dataeye.ad.assistor.module.result.model.IndicatorWithId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2017/5/23.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:resources/spring/applicationContext.xml", "classpath*:resources/spring/dispatchServlet-servlet.xml"})
@WebAppConfiguration(value = "/WebRoot")
public class DeliveryKeyIndicatorServiceImplTest {

    @Autowired
    private DeliveryKeyIndicatorService deliveryKeyIndicatorService;

    @Test
    public void getDeliveryKeyIndicatorTotal() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        deliveryKeyIndicatorService.getDeliveryKeyIndicatorTotal(query);
    }

    @Test
    public void getCostGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        deliveryKeyIndicatorService.getCostGroupByProduct(query);
    }

    @Test
    public void getCostGroupByProductWithSpecificMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        query.setMediumId(2);
        deliveryKeyIndicatorService.getCostGroupByProduct(query);
    }

    @Test
    public void getCostGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        deliveryKeyIndicatorService.getCostGroupByMedium(query);
    }

    @Test
    public void getCostGroupByMediumWithSpecificProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        query.setProductId(1);
        deliveryKeyIndicatorService.getCostGroupByMedium(query);
    }

    @Test
    public void getCostGroupByMediumWithOtherProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        query.setProductId(-1);
        List<IndicatorWithId> indicators = deliveryKeyIndicatorService.getCostGroupByMedium(query);
        System.out.println(indicators);
    }

    @Test
    public void getDownloadsGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        deliveryKeyIndicatorService.getDownloadsGroupByProduct(query);
    }

    @Test
    public void getDownloadsGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        List<IndicatorWithId> indicators = deliveryKeyIndicatorService.getDownloadsGroupByMedium(query);
        System.out.println(indicators);
    }

    @Test
    public void getActivationsGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        List<IndicatorWithId> indicators = deliveryKeyIndicatorService.getActivationsGroupByProduct(query);
        System.out.println(indicators);
    }

    @Test
    public void getActivationsGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        List<IndicatorWithId> indicators = deliveryKeyIndicatorService.getActivationsGroupByMedium(query);
        System.out.println(indicators);
    }

    @Test
    public void getRegistrationsGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        List<IndicatorWithId> indicators = deliveryKeyIndicatorService.getRegistrationsGroupByProduct(query);
        System.out.println(indicators);
    }

    @Test
    public void getRegistrationsGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        List<IndicatorWithId> indicators = deliveryKeyIndicatorService.getRegistrationsGroupByMedium(query);
        System.out.println(indicators);
    }


    @Test
    public void getDauGroupByProduct() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        List<IndicatorWithId> indicators = deliveryKeyIndicatorService.getDauGroupByProduct(query);
        System.out.println(indicators);
    }

    @Test
    public void getDauGroupByMedium() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 4, 1, 0, 0, 0);
        Date startDate = calendar.getTime();
        calendar.set(2017, 4, 22, 0, 0, 0);
        Date endDate = calendar.getTime();
        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(2);
        List<IndicatorWithId> indicators = deliveryKeyIndicatorService.getDauGroupByMedium(query);
        System.out.println(indicators);
    }

}