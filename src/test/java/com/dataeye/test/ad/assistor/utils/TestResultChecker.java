package com.dataeye.test.ad.assistor.utils;

import com.dataeye.ad.assistor.common.CachedObjects;
import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEResponse;

import org.junit.Assert;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * The type Test http utils.
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2015.12.22 16:18:36
 */
public class TestResultChecker {


    public static void checkStatusSuccess(DEResponse deResponse) {
        Assert.assertTrue(deResponse.getStatusCode() == StatusCode.SUCCESS);
    }

    public static void checkStatusFailed(DEResponse deResponse) {
        Assert.assertTrue(deResponse.getStatusCode() != StatusCode.SUCCESS);
    }

    public static void checkStatusCode(DEResponse deResponse, int expectedStatusCode) {
        Assert.assertTrue(deResponse.getStatusCode() == expectedStatusCode);
    }

    public static void checkContentNotNull(DEResponse deResponse) {
        Assert.assertTrue(deResponse.getContent() != null);
    }

    public static void checkKeysOfContentNotNull(DEResponse deResponse, String... keys) {
        Assert.assertTrue(deResponse.getContent() != null);
        for (int i = 0; i < keys.length; i++) {
            Assert.assertTrue(((Map<String, Object>) deResponse.getContent()).get(keys[i]) != null);
        }
    }

    public static void checkContentListNotEmpty(DEResponse deResponse) {
        Assert.assertTrue(((List) deResponse.getContent()).size() > 0);
    }

    public static void checkContentListEmpty(DEResponse deResponse) {
        Assert.assertTrue(((List) deResponse.getContent()).size() == 0);
    }

    public static void checkPageDataContentNotEmpty(DEResponse deResponse) {
        String content = CachedObjects.GSON.toJson(deResponse.getContent());
        PageData pageData = CachedObjects.GSON.fromJson(content, PageData.class);
        Assert.assertTrue(((List) pageData.getContent()).size() > 0);
    }

    public static void checkPageDataContentEmpty(DEResponse deResponse) {
        String content = CachedObjects.GSON.toJson(deResponse.getContent());
        PageData pageData = CachedObjects.GSON.fromJson(content, PageData.class);
        Assert.assertTrue(((List) pageData.getContent()).size() == 0);
    }

    public static void checkKeyValueOfContentExist(DEResponse deResponse, String key, Object value) {
        Map<String, Object> content = (Map<String, Object>) deResponse.getContent();
        Assert.assertTrue(content.get(key) != null);

        if (value instanceof String) {
            Assert.assertTrue(content.get(key).equals(value));
        } else if (value instanceof List) {
            Assert.assertTrue(((List) content.get(key)).size() > 0);
        } else if (value instanceof Map) {
            Assert.assertTrue(((Map) content.get(key)).size() > 0);
        } else if (value instanceof Integer) {
//            Assert.assertTrue(((Double) content.get(key)).intValue() == value);
        } else {
            Assert.assertTrue(content.get(key) == value);
        }
    }

    public static void checkValueOfContentGtArg(DEResponse deResponse, String key, int arg) {
        Map<String, Object> content = (Map<String, Object>) deResponse.getContent();
        Assert.assertTrue(content.get(key) != null);
        Assert.assertTrue(((Double) content.get(key)).intValue() > arg);
    }

    public static void checkValueOfContentGeArg(DEResponse deResponse, String key, int arg) {
        Map<String, Object> content = (Map<String, Object>) deResponse.getContent();
        Assert.assertTrue(content.get(key) != null);
        Assert.assertTrue(((Double) content.get(key)).intValue() >= arg);
    }

    public static void checkValueOfContentEqArg(DEResponse deResponse, String key, int arg) {
        Map<String, Object> content = (Map<String, Object>) deResponse.getContent();
        Assert.assertTrue(content.get(key) != null);
        Assert.assertTrue(((Double) content.get(key)).intValue() == arg);
    }

    public static void checkValueOfContentLtArg(DEResponse deResponse, String key, int arg) {
        Map<String, Object> content = (Map<String, Object>) deResponse.getContent();
        Assert.assertTrue(content.get(key) != null);
        Assert.assertTrue(((Double) content.get(key)).intValue() < arg);
    }

}
