package com.dataeye.test.ad.assistor.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * <pre>
 * The type Spring config test.
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2015.12.19 16:17.38
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.dataeye",
        includeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = {Controller.class})})
public class MvcConfig extends WebMvcConfigurationSupport {
}
