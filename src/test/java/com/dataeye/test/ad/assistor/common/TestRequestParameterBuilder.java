package com.dataeye.test.ad.assistor.common;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * The type Test http request parameter.
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2015.12.22 16:57:43
 */
public class TestRequestParameterBuilder {

    private Map<String, Object> parameterMap = new HashMap<>();

    public TestRequestParameterBuilder() {
    }

    public TestRequestParameterBuilder put(String key, Object value) {
        parameterMap.put(key, value);
        return this;
    }

    public Map<String, Object> build() {
        return parameterMap;
    }

    public TestRequestParameterBuilder clear() {
        parameterMap.clear();
        return this;
    }
}
