package com.dataeye.test.ad.assistor.common;

/**
 * <pre>
 * The type Test constant.
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2015.12.22 16:18:34
 */
public class TestConstant {

    public static class Data {
        public static final int MEDIUM_ID_1 = 1;
        public static final int MEDIUM_ID_2 = 2;

        public static final int PLAN_ID_5= 5;
        public static final int PLAN_ID_6= 6;
        public static final int PLAN_ID_7= 7;
        public static final int PLAN_ID_8= 8;

        public static final int LANDPAGE_ID_256 = 256;
        public static final int LANDPAGE_ID_257 = 257;
        public static final int LANDPAGE_ID_258 = 258;
        public static final int LANDPAGE_ID_259 = 259;

        public static final int DATA_PKG_ID_580 = 580;
        public static final int DATA_PKG_ID_581 = 581;
        public static final int DATA_PKG_ID_582 = 582;
        public static final int DATA_PKG_ID_583 = 583;
    }

    public static class Cookie {
        public static final String UID_10105_TOKEN = "41a2e70b7a827b9bc2c4123b3adf3a752d40083fd8cc926bf0e26580b8ea471d";
    }

    public static class Url {
        // plan
        public static final String AD_REPORT_PLAN = "/ad/report/plan.do";
        public static final String PLAN_NAMELIST = "/plan/namelist.do";
        public static final String MEDIUM_NAMELIST = "/medium/namelist.do";
        // land page
        public static final String AD_REPORT_LANDPAGE = "/ad/report/landpage.do";
        public static final String LANDPAGE_NAMELIST = "/landpage/namelist.do";
        // conversion
        public static final String AD_REPORT_CONVERSION = "/ad/report/conversion.do";
        public static final String AD_REPORT_LTV = "/ad/report/ltv.do";
        public static final String DATA_PKG_NAMELIST = "/datapkg/namelist.do";
        // association
        public static final String AD_ASSOCIATION_CREATE = "/ad/association/create.do";
        public static final String AD_ASSOCIATION_EDIT = "/ad/association/edit.do";
        public static final String AD_ASSOCIATION_DEL = "/ad/association/del.do";
        public static final String AD_ASSOCIATION_LIST = "/ad/association/list.do";
        // generate report
        public static final String AD_REPORT_ADVERTISING = "/ad/report/advertising.do";
        public static final String AD_REPORT_VICE_ADVERTISING = "/ad/report/vice-advertising.do";
        public static final String AD_REPORT_PAYBACK = "/ad/report/payback.do";
        // account
        public static final String AD_ACCOUNT_CREATE = "/account/create.do";
        public static final String AD_ACCOUNT_LIST = "/account/list.do";


    }

}
