package com.dataeye.test.ad.assistor.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.xunlei.jdbc.JdbcTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;

import java.beans.PropertyVetoException;

/**
 * <pre>
 * The type Spring config.
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2015.12.19 16:28.17
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "com.dataeye",
        excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = {Controller.class})})
@PropertySource("/resources/db/datasource.properties")
public class AppConfig {

    @Autowired
    private Environment environment;

    @Bean(name = "jdbcTemplateTracking")
    public JdbcTemplate getJdbcTemplateTracking() throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass("com.mysql.jdbc.Driver");
        dataSource.setJdbcUrl(environment.getProperty("jdbc_url_tracking"));
        dataSource.setUser(environment.getProperty("jdbc_user_tracking"));
        dataSource.setPassword(environment.getProperty("jdbc_password_tracking"));
        // 连接池中保留的最小连接数
        dataSource.setMinPoolSize(1);
        // 连接池中保留的最大连接数
        dataSource.setMaxPoolSize(3);
        // 初始化时获取的连接数，取值应在 minPoolSize与maxPoolSize之间。Default: 3
        dataSource.setInitialPoolSize(2);
        // 最大空闲时间,60秒内未使用则连接被丢弃。若为0则永不丢弃。
        dataSource.setMaxIdleTime(60);
        // 当连接池中的连接耗尽的时候c3p0一次同时获取的连接数。
        dataSource.setAcquireIncrement(2);
        // JDBC的标准参数，用以控制数据源内加载的PreparedStatements数量。但由于预缓存的statements属于单个connection
        // 而不是整个连接池。所以设置这个参数需要考虑到多方面的因素。
        // 如果maxStatements与maxStatementsPerConnection均为0，则缓存被关闭。Default: 0
        dataSource.setMaxStatements(0);
        // 每60秒检查所有连接池中的空闲连接。 Default: 0
        dataSource.setIdleConnectionTestPeriod(60);
        // 定义在从数据库获取新连接失败后重复尝试的次数。Default: 30
        dataSource.setAcquireRetryAttempts(30);
        // 获取连接失败将会引起所有等待连接池来获取连接的线程抛出异常。但是数据源仍有效保留，并在下次调用getConnection()
        // 的时候继续尝试获取连接。如果设为true，那么在尝试获取连接失败后该数据源将申明已断开并永久关闭。Default: false
        dataSource.setBreakAfterAcquireFailure(false);
        // 因性能消耗大请只在需要的时候使用它。如果设为true那么在每个connection提交的时候都将校验其有效性。
        // 建议使用idleConnectionTestPeriod或automaticTestTable // 等方法来提升连接测试的性能。Default: false
        dataSource.setTestConnectionOnCheckout(false);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate;
    }

}
