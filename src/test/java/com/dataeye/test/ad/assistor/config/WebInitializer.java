package com.dataeye.test.ad.assistor.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

@Configuration
public class WebInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(javax.servlet.ServletContext container) throws ServletException {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

        // 1.Spring context listener
        rootContext.register(AppConfig.class);
        container.addListener(new ContextLoaderListener(rootContext));

        // 2.SpringMVC context
        AnnotationConfigWebApplicationContext springMvcContext = new AnnotationConfigWebApplicationContext();
        springMvcContext.register(MvcConfig.class);

        // 3.DispatcherServlet
        DispatcherServlet dispatcherServlet = new DispatcherServlet(springMvcContext);
        ServletRegistration.Dynamic dynamic = container.addServlet("dispatcherServlet", dispatcherServlet);
        dynamic.setLoadOnStartup(1);
        dynamic.addMapping("*.do");

        // 4.CharacterEncodingFilter
        //CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        //characterEncodingFilter.setEncoding("utf-8");
        //container.addFilter("characterEncodingFilter", characterEncodingFilter)
        //        .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), false, "/");

    }

}


