package com.dataeye.test.ad.assistor.common;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.config.ResourceHandler;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

/**
 * <pre>
 *
 * @author Stran <br>
 * @create 2016-02-22 17:42 <br>
 * @version 1.0 <br>
 */
@Service
public class TestDeApplicaitonListener implements ApplicationListener {

    private boolean inited = false;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (!inited) {
            // 初始化语言文件
            ResourceHandler.init();
            // 初始化配置文件
            ConfigHandler.init();
            inited = true;
        }
    }
}
