package com.dataeye.ad.assistor.schedule;

import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.module.realtimedelivery.service.BalanceAlertService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by huangzehai on 2017/5/2.
 */
@Component
public class BalanceWarningJob implements Job {
    private Logger logger = LoggerFactory.getLogger(BalanceWarningJob.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        logger.info("Start balance warning job");
        BalanceAlertService  balanceAlertService= (BalanceAlertService) ApplicationContextContainer.getBean("balanceAlertService");
        balanceAlertService.sentBalanceWarningEmail();
        logger.info("Finish balance waring job");
    }
}
