package com.dataeye.ad.assistor.schedule;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.module.realtimedelivery.service.DeliveryService;
import com.dataeye.ad.assistor.util.DateUtils;

/**
 * <pre>
 * 扫描实时投放达到告警阈值的数据，发送邮件到告警通知邮件
 * 
 * 
 * @author luzhuyou 2017/01/03
 */
@Service
public class RealTimeDeliveryMonitor implements Job {

	public static boolean isRunning = false;
    private static final Logger logger = (Logger) LoggerFactory.getLogger(DeliveryService.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		if (!isRunning) {
			isRunning = true; // 置为正在运行状态
			DeliveryService deliveryService = (DeliveryService) ApplicationContextContainer
					.getBean("deliveryService");
			logger.debug("Monitor delivery alert！");
			deliveryService.monitor(DateUtils.currentDate());
			isRunning = false; // 执行完成置为可用
		}
	}

}