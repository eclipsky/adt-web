package com.dataeye.ad.assistor.schedule;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;

import com.xunlei.util.Log;

import java.util.Map;
import java.util.Map.Entry;

/**
 * 定时任务管理类
 * 
 * @author 王德封
 */
public class QuartzManager {
    private final static Logger logger = Log.getLogger("scheduler");
    private static SchedulerFactory sf = new StdSchedulerFactory();
    private static Scheduler scheduler;

    static {
        try {
            scheduler = sf.getScheduler();
            logger.info("QuartzManager init ... ");
        } catch (SchedulerException e) {
            logger.error("QuartzManager start error", e);
        }
    }
    
    public static void addCrawlerJobs(Map<Class<? extends Job>, String> jobMap) throws SchedulerException {
        for (Entry<Class<? extends Job>, String> entry : jobMap.entrySet()) {
            try {
                Class<? extends Job> clazz = entry.getKey();
                String cronExpr = entry.getValue();
                System.out.println("==================== QuartzJob Run:" + clazz.getSimpleName() + " -> " + cronExpr);
                long id = Thread.currentThread().getId();
                String mainGroup = "Main_Group_" + id;
                JobDataMap nonUseMap = new JobDataMap();
                QuartzManager.addCronJob(clazz.getSimpleName(), clazz, nonUseMap, cronExpr, mainGroup);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void addCronJob(String jobName, Class<? extends Job> jobClass, JobDataMap jobDataMap,
            String cronExpression, String groupName) throws SchedulerException {
        try {
            JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobName, groupName).setJobData(jobDataMap)
                    .build();
            CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(jobName, groupName)
                    .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).build();
            scheduler.scheduleJob(jobDetail, cronTrigger);
        } catch (SchedulerException e) {
            logger.error("QuartzManager addCronJob error,triggerOrJobName=" + jobName + ",cronExpression="
                    + cronExpression, e);
            throw e;
        }
    }

    public static void start() throws SchedulerException {
        scheduler.start();
        logger.info("QuartzManager start ... ");
    }

    public static void standby() throws SchedulerException {
        scheduler.standby();
        logger.info("QuartzManager standby ... ");
    }

    public static void shutdown() throws SchedulerException {
        scheduler.shutdown(true);
        logger.info("QuartzManager shuting down ... ");
    }

    public static void pause() throws SchedulerException {
        scheduler.pauseAll();
        logger.info("QuartzManager pause ... ");
    }

    public static void resume() throws SchedulerException {
        scheduler.resumeAll();
        logger.info("QuartzManager resume ... ");
    }
}
