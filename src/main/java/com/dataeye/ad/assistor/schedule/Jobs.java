package com.dataeye.ad.assistor.schedule;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.quartz.Job;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.util.ServerUtil;


@Service
public class Jobs {

	public static void start() throws Exception {
	    loadQuartzJobs();
	    QuartzManager.start();
	}

    private static void loadQuartzJobs() throws ClassNotFoundException, SchedulerException {
        // 读取任务配置
        Properties props = ServerUtil.getConfigProperties("resources/ext/jobconfig.properties");
        Map<Class<? extends Job>, String> jobMap = new HashMap<Class<? extends Job>, String>();
        for (String className : props.stringPropertyNames()) {
            String cronExpr = props.getProperty(className).trim();
            Class<? extends Job> clazz = (Class<? extends Job>) Class.forName(className.trim());
            jobMap.put(clazz, cronExpr);
        }
        // 加入定时调度
        QuartzManager.addCrawlerJobs(jobMap);
    }
    
    public static void shutdown() throws Exception{
        QuartzManager.shutdown();
    }

}
