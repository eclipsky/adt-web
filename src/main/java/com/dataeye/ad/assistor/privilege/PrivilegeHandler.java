package com.dataeye.ad.assistor.privilege;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.constant.Constant.CookieName;
import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ClientException;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.exception.ParameterException;
import com.dataeye.ad.assistor.exception.ServerException;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.AccountRole;
import com.dataeye.ad.assistor.module.systemaccount.model.MenuInterfaceMapping;
import com.dataeye.ad.assistor.module.systemaccount.service.MenuInterfaceMappingService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl.Scope;
import com.dataeye.ad.assistor.util.CookieUtils;
import com.dataeye.ad.assistor.util.LogUtils;
import com.dataeye.ad.assistor.util.ValidateUtils;


/**
 * <pre>
 * 权限处理器
 * @author Ivan          <br>
 * @date 2015年1月7日 上午11:24:47
 * <br>
 *
 */
public class PrivilegeHandler {
	/** 存放Public权限的接口 */
	public static HashSet<String> PublicDo = new HashSet<String>();
	/** 存放AfterLogin权限的接口 */
	public static HashSet<String> AfterLoginDo = new HashSet<String>();
	/** 存放AfterAuth权限的接口 */
	public static HashSet<String> AfterAuthDo = new HashSet<String>();
	/** 固定IP才能访问 */
	public static HashSet<String> FIXIPDo = new HashSet<String>();
	/** 存放menuId以及其下面声明的接口列表 */
//	public static HashMap<String, Set<String>> MenuIdAndInterfaceMap = new HashMap<String, Set<String>>();
	/** 存放menuId以及其下面声明的接口和权限列表 */
	public static HashMap<String, Map<String, Integer>> menuId2InterfaceDataMap = new HashMap<String, Map<String, Integer>>();
	/** 存放具有写操作的接口 */
	public static Set<String> writeInterfaceSet = new HashSet<String>();

	/**
	 * 
	 * <pre>
	 * 扫描并设置权限 
	 * @author Ivan<br>
	 * @date 2015年3月13日 下午1:45:15
	 * <br>
	 */
	public static void start2ScanPrivileges() {
		System.err.println(">>>>>>>>>>>>>>start to scan");
		// 取出所有bean
		String[] beans = ApplicationContextContainer.getAllBeans();
		// 遍历每个bean的每个方法
		for (String bean : beans) {
			Object object = ApplicationContextContainer.getBean(bean);
			// 因为接口层的处理使用了AOP,访问的不再是原来的接口而是通过CGLIB产生的动态代理类,动态代理类是原来类的子类
			for (Method m : object.getClass().getSuperclass().getMethods()) {
				// 取出有RequestMapping和AccessControl这两种注解的
				RequestMapping requestMapping = m.getAnnotation(RequestMapping.class);
				PrivilegeControl accessControl = m.getAnnotation(PrivilegeControl.class);
				if (requestMapping != null && accessControl != null) {
					// String requestUri = requestMapping.value()[0];
					// requetUri就是具体的接口如/test/test.do,下面解析出test.do部分
					// String doName = ServerUtils.getDoFromUri(requestUri);
					String doName = requestMapping.value()[0]; // 采用全称
					if (ValidateUtils.isNotEmpty(doName)) {
						Scope scope = accessControl.scope();
//						String[] menuDescArr = accessControl.menuDesc();
						boolean write = accessControl.write();

						if (Scope.Public == scope) {// public范围的接口和login范围的接口都直接加入即可
							PublicDo.add(doName);
						} else if (Scope.AfterLogin == scope) {
							AfterLoginDo.add(doName);
						} else if (Scope.FIXIP == scope) {
							FIXIPDo.add(doName);
						}
						// 该逻辑不再使用
//						else if (Scope.AfterAuth == scope) {// auth范围的接口需要将接口与菜单ID关联
//							for (String menuDesc : menuDescArr) {
//								AfterAuthDo.add(doName);
//								Set<String> interfaceSet = MenuDescAndInterfaceMap.get(menuDesc);
//								if (interfaceSet == null) {
//									interfaceSet = new HashSet<String>();
//									interfaceSet.add(doName);
//									MenuDescAndInterfaceMap.put(menuDesc, interfaceSet);
//								} else {
//									interfaceSet.add(doName);
//								}
//							}
//						}
						// 把有写操作的接口加入到集合中
						if (write) {
							writeInterfaceSet.add(doName);
						}
					}
				}
			}
		}
		
		// 初始化菜单接口映射关系
		MenuInterfaceMappingService mappingService = (MenuInterfaceMappingService) ApplicationContextContainer.getBean("menuInterfaceMappingService");
		List<MenuInterfaceMapping> mappingList = mappingService.query();
		if(mappingList != null) {
			for(MenuInterfaceMapping mapping : mappingList) {
				String menuId = mapping.getMenuId();
//				Set<String> interfaceSet = MenuIdAndInterfaceMap.get(menuId);
//				if(interfaceSet == null) {
//					interfaceSet = new HashSet<String>();
//					MenuIdAndInterfaceMap.put(menuId, interfaceSet);
//				}
//				interfaceSet = MenuIdAndInterfaceMap.get(menuId);
//				interfaceSet.add(mapping.getInterfaceUrl());
				
				// 存放menuId以及其下面声明的接口与数据权限映射的列表
				Map<String, Integer> interfaceDataMap = menuId2InterfaceDataMap.get(menuId);
				if(interfaceDataMap == null) {
					interfaceDataMap = new HashMap<String, Integer>();
					menuId2InterfaceDataMap.put(menuId, interfaceDataMap);
				}
				interfaceDataMap.put(mapping.getInterfaceUrl(), mapping.getDataPermission());
			}
		}
		// 扫描完毕
		System.err.println(">>>>>>>>>>>>>>scan done");
		System.err.println("public:" + PublicDo);
		System.err.println("login:" + AfterLoginDo);
		System.err.println("afterAuth:" + AfterAuthDo);
		System.err.println("fixIP:" + AfterAuthDo);
//		System.err.println("MenuDescAndInterfaceMap:" + MenuIdAndInterfaceMap);
		System.err.println("menuId2InterfaceDataMap:" + menuId2InterfaceDataMap);
	}

	/**
	 * 
	 * <pre>
	 * 检查用户权限
	 *  @param context
	 *  @throws ClientException  
	 *  @author Ivan<br>
	 * @throws ServerException 
	 * @throws ParameterException 
	 * @date 2015年3月14日 上午11:23:52
	 * <br>
	 */
	public static boolean checkPrivilege(DEContext context) throws ClientException, ServerException, ParameterException {
		String doName = context.getDeParameter().getDoName();
		
		// 先判断接口是不是public权限
		if (PublicDo.contains(doName)) {// 是public权限的接口,直接通过
			LogUtils.logPriv(context.getID(), doName, "public", "success");
			return true;
		}
		
		// 验证token，需要存在登录态才能进行访问的 
		UserInSession session = SessionContainer.getCurrentUser(context.getSession());
		if(session != null) {
			String tokenInSession = session.getToken();
			// 从cookie中取token
			String token = CookieUtils.getCookieValue(context.getRequest(), CookieName.TOKEN_NAME);

			if(StringUtils.isBlank(token) && StringUtils.isBlank(tokenInSession)) {
				; // 兼容ADT旧的不考虑token的登录逻辑
			}
			// 检查新旧session是否一致，如果不一致，需要重新登录
			else if (StringUtils.isBlank(token) || StringUtils.isBlank(tokenInSession) || !tokenInSession.equals(token)) {// 当url中强制传递了token以后就以这个token重新设置用户登录行为
				// 使session失效，跳转到登陆页
				SessionContainer.clear(context.getRequest());
				if(StringUtils.isNotBlank(token)) {
					SessionContainer.tokenSessionMap.remove(token);
				}
			}
		}
					
		UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
		if(userInSession==null){
			ExceptionHandler.throwParameterException(StatusCode.LOGI_RE_LOGIN);
		}
		// 执行到这里就认为登录态没有问题了，再继续进行AfterLogin权限
		if (AfterLoginDo.contains(doName)) {
			// 判断是否在可访问接口权限列表
			boolean hasPermission = userInSession.hasInterfacePermission(doName);
			if(!hasPermission) {
				// 无权限
				ExceptionHandler.throwGeneralServerException(StatusCode.LOGI_NO_PRIVILEGE);
			}
			
			// 登录成功
			LogUtils.logPriv(context.getID(), doName, "AfterLogin", "success");

			boolean hasDataPermission = false;
			// 如果是开发工程师角色，判断是否有被授权产品，如果没有，返回空数据列表
			if(userInSession.getAccountRole() == AccountRole.ENGINEER) {
				hasDataPermission = userInSession.hasAuthorizedProduct();
			} else {
				// 判断接口是否有数据访问权限，如果没有，则返回空数据列表
				hasDataPermission = userInSession.hasDataPermission(doName);
			}
			return hasDataPermission;
		}
		// 这个接口非法的？没有配到权限系统里面来？
		ExceptionHandler.throwGeneralServerException(StatusCode.COMM_SEVER_ERROR, doName + " config not exists");
		return false;
	}

//	public static Set<String> getInterfaceSetByMenuId(String menuId) {
//		return MenuIdAndInterfaceMap.get(menuId);
//	}
	
	public static Map<String, Integer> getInterfaceDataMapByMenuId(String menuId) {
		return menuId2InterfaceDataMap.get(menuId);
	}
}