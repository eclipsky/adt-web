package com.dataeye.ad.assistor.privilege;

import ch.qos.logback.classic.Logger;
import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.AccountRole;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 数据权限处理
 * @author luzhuyou 2017/03/02
 *
 */
public class DataPermissionHandler {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(DataPermissionHandler.class);
    
	/**
	 * 根据当前登录用户所具有的可访问媒体账号ID的清单
	 * @param userInSession
	 * @return 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 */
	public static Integer[] getPermissionMediumAccountIds(UserInSession userInSession) {
        
        int companyId = userInSession.getCompanyId();
        String accountName = userInSession.getAccountName();
        
        Integer[] permissionMediumAccountIds = null;
        
        // 非企业账号时，根据系统账号查询所管辖媒体账号，企业账号时，查询所有媒有媒体账号
        if(AccountRole.COMPANY != userInSession.getAccountRole()) {
        	// 根据指定公司ID及(优化师&关注者)ADT账号获取所管理的媒体账号，如果是组长，则返回组内所有成员对应的媒体账号（包括自身所对应的媒体账号）
        	permissionMediumAccountIds = ApplicationContextContainer.getMediumAccountIdForResponsibleAndFollowerAccounts(companyId, accountName);
        	
        	// 根据系统账号对应媒体账号进行查询
        	if(permissionMediumAccountIds == null || permissionMediumAccountIds.length == 0) {
        		return null;
        	}
        } else {
        	permissionMediumAccountIds = new Integer[0];
        }
        logger.info("公司[{}]当前登录用户[{}]所具有的可访问媒体账号ID为：[{}]", new Object[]{companyId, accountName, Arrays.asList(permissionMediumAccountIds)});
        return permissionMediumAccountIds;
	}
	
	/**
	 * 根据当前登录用户(优化师ADT账号)所具有的可访问媒体账号ID的清单
	 * @param userInSession
	 * @return 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 */
	public static Integer[] getOptimizerPermissionMediumAccountIds(UserInSession userInSession) {
        
        int companyId = userInSession.getCompanyId();
        String accountName = userInSession.getAccountName();
        
        Integer[] permissionMediumAccountIds = null;
        
        // 非企业账号时，根据系统账号查询所管辖媒体账号，企业账号时，查询所有媒有媒体账号
        if(AccountRole.COMPANY != userInSession.getAccountRole()) {
        	// 根据指定公司ID及优化师ADT账号获取所管理的媒体账号，如果是组长，则返回组内所有成员对应的媒体账号（包括自身所对应的媒体账号）
        	permissionMediumAccountIds = ApplicationContextContainer.getMediumAccountIdForResponsibleAccount(companyId, accountName);
        	
        	// 根据系统账号对应媒体账号进行查询
        	if(permissionMediumAccountIds == null || permissionMediumAccountIds.length == 0) {
        		return null;
        	}
        } else {
        	permissionMediumAccountIds = new Integer[0];
        }
        logger.info("公司[{}]当前登录用户（作为负责人）[{}]所具有的可访问媒体账号ID为：[{}]", new Object[]{companyId, accountName, Arrays.asList(permissionMediumAccountIds)});
        return permissionMediumAccountIds;
	}
	
	/**
	 * 根据当前登录用户所具有的可访问媒体账号ID的清单
	 * @param userInSession
	 * @return 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 */
	public static String[] getPermissionMediumAccountNames(UserInSession userInSession) {
        
        int companyId = userInSession.getCompanyId();
        String accountName = userInSession.getAccountName();
        
        String[] permissionMediumAccountNames = null;
        
        // 非企业账号时，根据系统账号查询所管辖媒体账号，企业账号时，查询所有媒有媒体账号
        if(AccountRole.COMPANY != userInSession.getAccountRole()) {
        	// 根据指定公司ID及系统账号获取所管理的媒体账号，如果是组长，则返回组内所有成员对应的媒体账号（包括自身所对应的媒体账号）
        	permissionMediumAccountNames = ApplicationContextContainer.getMediumAccountByChildAccount(companyId, accountName);
        	
        	// 根据系统账号对应媒体账号进行查询
        	if(permissionMediumAccountNames == null || permissionMediumAccountNames.length == 0) {
        		return null;
        	}
        } else {
        	permissionMediumAccountNames = new String[0];
        }
        logger.info("公司[{}]当前登录用户（作为负责人）[{}]所具有的可访问媒体账号为：[{}]", new Object[]{companyId, accountName, Arrays.asList(permissionMediumAccountNames)});
        return permissionMediumAccountNames;
	}
	
	/**
	 * 根据当前登录用户所具有的可访问系统账号ID的清单
	 * @param userInSession
	 * @return 如果返回值为null，表示无系统账号数据访问权限，否则，有权限
	 */
	public static Integer[] getPermissionSystemAccountIds(UserInSession userInSession) {
        int companyId = userInSession.getCompanyId();
        String accountName = userInSession.getAccountName();
        
        if(userInSession.getAccountRole() == AccountRole.COMPANY) {
        	return null;
        }
		// 获取组长账号下所有组员的ADT账号
    	Set<Integer> accountIdsSet = ApplicationContextContainer.getAccountIdArray(companyId, accountName);
    	// 如果不是组长角色，则返回空值，需要手动将当前ADT账号添加上
    	if(accountIdsSet == null) {
    		accountIdsSet = new HashSet<Integer>();
    	}
    	accountIdsSet.add(userInSession.getAccountId());
    	Integer[] permissionSystemAccountIds = new Integer[accountIdsSet.size()];
    	accountIdsSet.toArray(permissionSystemAccountIds);
        
        
        logger.info("公司[{}]当前登录用户[{}]所具有的可访问系统（子）账号ID为：[{}]", new Object[]{companyId, userInSession.getAccountName(), Arrays.asList(permissionSystemAccountIds)});
        return permissionSystemAccountIds;
	}

	/**
	 * 根据当前登录用户所具有的可访问系统账号的清单
	 * @param userInSession
	 * @return 如果返回值为null，当前为企业账号访问
	 * @author luzhuyou 2017/08/16
	 */
	public static String[] getPermissionSystemAccounts(UserInSession userInSession) {
		
		int companyId = userInSession.getCompanyId();
        String accountName = userInSession.getAccountName();
        
        if(userInSession.getAccountRole() == AccountRole.COMPANY) {
        	return null;
        }
		// 获取组长账号下所有组员的ADT账号
    	Set<String> membersAccount = ApplicationContextContainer.getLeaderMemberAccounts(companyId, accountName);
    	// 如果不是组长角色，则返回空值，需要手动将当前ADT账号添加上
    	if(membersAccount == null) {
    		membersAccount = new HashSet<String>();
    	}
    	membersAccount.add(accountName);
    	String[] permissionSystemAccounts = new String[membersAccount.size()];
    	membersAccount.toArray(permissionSystemAccounts);
    	logger.info("公司[{}]当前登录用户[{}]所具有的可访问系统子账号为：[{}]", new Object[]{companyId, accountName, Arrays.asList(permissionSystemAccounts)});
    	return permissionSystemAccounts;
	}
	
	public static void main(String[] args) {
		// 获取组长账号下所有组员的ADT账号
    	Set<String> membersAccount = new HashSet<String>();
    	membersAccount.add("1");
    	membersAccount.add("2");
    	membersAccount.add("3");
    	String[] permissionSystemAccounts = new String[membersAccount.size()];
    	membersAccount.toArray(permissionSystemAccounts);
    	System.out.println(permissionSystemAccounts);
	}
}

