//package com.dataeye.ad.assistor.privilege;
//
//import com.dataeye.ad.assistor.constant.StatusCode;
//import com.dataeye.ad.assistor.context.DEContext;
//import com.dataeye.ad.assistor.context.SessionContainer;
//import com.dataeye.ad.assistor.context.UserInSession;
//import com.dataeye.ad.assistor.exception.ExceptionHandler;
//
///**
// * Created by huangzehai on 2017/4/19.
// */
//public final class PrivilegeUtils {
//    private PrivilegeUtils() {
//
//    }
//
//    public static DataPermissionDomain getDataPermissionDomain(DEContext context) {
//        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
//        if (userInSession == null) {
//            ExceptionHandler.throwParameterException(StatusCode.LOGI_RE_LOGIN);
//        }
//        int companyId = userInSession.getCompanyId();
//        Integer[] permissionMediumAccountIds = DataPermissionHandler.getPermissionMediumAccountIds(userInSession);
//        // 如果为null，则表示无数据访问权限，否则，有权限
//        Integer[] optimizerPermissionMediumAccountIds = DataPermissionHandler.getOptimizerPermissionMediumAccountIds(userInSession);
//        String[] permissionSystemAccounts = DataPermissionHandler.getPermissionSystemAccounts(userInSession);
//        DataPermissionDomain dataPermissionDomain = new DataPermissionDomain();
//        dataPermissionDomain.setCompanyId(companyId);
//        dataPermissionDomain.setPermissionMediumAccountIds(permissionMediumAccountIds);
//        dataPermissionDomain.setOptimizerPermissionMediumAccountIds(optimizerPermissionMediumAccountIds);
//        dataPermissionDomain.setPermissionSystemAccounts(permissionSystemAccounts);
//        return dataPermissionDomain;
//    }
//
//    /**
//     * 是否拥有管理者权限
//     *
//     * @param dataPermissionDomain
//     * @return
//     */
//    public static boolean hasManagerPermission(DataPermissionDomain dataPermissionDomain) {
//        return dataPermissionDomain != null && dataPermissionDomain.getOptimizerPermissionMediumAccountIds() != null;
//    }
//
//    /**
//     * 是否拥有管理者或关注者权限
//     *
//     * @param dataPermissionDomain
//     * @return
//     */
//    public static boolean hasManagerOrFollowerPermission(DataPermissionDomain dataPermissionDomain) {
//        return dataPermissionDomain != null && dataPermissionDomain.getPermissionMediumAccountIds() != null;
//    }
//}
