package com.dataeye.ad.assistor.privilege;

import java.util.Map;



/**
 * 系统账号对应具有权限查询的媒体账号ID基类
 * @author luzhuyou 2017/03/02
 *
 */
public class DataPermissionDomain {
	/** 公司ID */
	private Integer companyId;
	/**	ADT账号对应具有权限查询的媒体 */
	private Map<Integer, String> permissionMedium;
	/**	负责人账号对应具有权限查询的媒体 */
	private Map<Integer, String> permissionResponsibleMedium;
	/** 
	 * ADT账号对应具有权限查询的媒体账号ID列表
	 * 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 */
	private Integer[] permissionMediumAccountIds;
	
	/** 
	 * ADT账号对应具有权限查询的媒体账号名称列表
	 * 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 */
	private String[] permissionMediumAccountNames;
	
	/** 
	 * 负责人账号对应具有权限查询的媒体账号ID列表
	 * 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 */
	private Integer[] optimizerPermissionMediumAccountIds;
	
	/** 
	 * 系统账号对应具有权限查询的系统账号ID列表
	 * 如果返回值为null，表示无系统账号数据访问权限，否则，有权限
	 */
	private Integer[] permissionSystemAccountIds;
	
	/** 
	 * 根据当前登录用户所具有的可访问系统账号的清单
	 * 如果返回值为null，当前为企业账号访问
	 */
	private String[] permissionSystemAccounts;
	
	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	/** 
	 * ADT账号对应具有权限查询的媒体账号ID列表
	 * 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 */
	public Integer[] getPermissionMediumAccountIds() {
		return permissionMediumAccountIds;
	}

	/** 
	 * ADT账号对应具有权限查询的媒体账号ID列表
	 * 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 * @param permissionMediumAccountIds
	 */
	public void setPermissionMediumAccountIds(Integer[] permissionMediumAccountIds) {
		this.permissionMediumAccountIds = permissionMediumAccountIds;
	}

	/** 
	 * ADT账号对应具有权限查询的媒体账号名称列表
	 * 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 */
	public String[] getPermissionMediumAccountNames() {
		return permissionMediumAccountNames;
	}

	/** 
	 * ADT账号对应具有权限查询的媒体账号名称列表
	 * 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 * @param permissionMediumAccountNames
	 */
	public void setPermissionMediumAccountNames(
			String[] permissionMediumAccountNames) {
		this.permissionMediumAccountNames = permissionMediumAccountNames;
	}

	/** 
	 * 负责人账号对应具有权限查询的媒体账号ID列表
	 * 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 */
	public Integer[] getOptimizerPermissionMediumAccountIds() {
		return optimizerPermissionMediumAccountIds;
	}

	/** 
	 * 负责人账号对应具有权限查询的媒体账号ID列表
	 * 如果返回值为null，表示无媒体账号数据访问权限，否则，有权限
	 * @param optimizerPermissionMediumAccountIds
	 */
	public void setOptimizerPermissionMediumAccountIds(
			Integer[] optimizerPermissionMediumAccountIds) {
		this.optimizerPermissionMediumAccountIds = optimizerPermissionMediumAccountIds;
	}

	/** 
	 * 系统账号对应具有权限查询的系统账号ID列表
	 * 如果返回值为null，表示无系统账号数据访问权限，否则，有权限
	 */
	public Integer[] getPermissionSystemAccountIds() {
		return permissionSystemAccountIds;
	}

	/** 
	 * 系统账号对应具有权限查询的系统账号ID列表
	 * 如果返回值为null，表示无系统账号数据访问权限，否则，有权限
	 * @param permissionSystemAccountIds
	 */
	public void setPermissionSystemAccountIds(Integer[] permissionSystemAccountIds) {
		this.permissionSystemAccountIds = permissionSystemAccountIds;
	}

	/** 
	 * 根据当前登录用户所具有的可访问系统账号的清单
	 * 如果返回值为null，当前为企业账号访问
	 */
	public String[] getPermissionSystemAccounts() {
		return permissionSystemAccounts;
	}

	/** 
	 * 根据当前登录用户所具有的可访问系统账号的清单
	 * 如果返回值为null，当前为企业账号访问
	 * @param permissionSystemAccounts
	 */
	public void setPermissionSystemAccounts(String[] permissionSystemAccounts) {
		this.permissionSystemAccounts = permissionSystemAccounts;
	}

	/**	ADT账号对应具有权限查询的媒体 */
	public Map<Integer, String> getPermissionMedium() {
		return permissionMedium;
	}

	/**	
	 * ADT账号对应具有权限查询的媒体
	 * @param  permissionMedium
	 */
	public void setPermissionMedium(Map<Integer, String> permissionMedium) {
		this.permissionMedium = permissionMedium;
	}

	/**	负责人账号对应具有权限查询的媒体 */
	public Map<Integer, String> getPermissionResponsibleMedium() {
		return permissionResponsibleMedium;
	}

	/**	
	 * 负责人账号对应具有权限查询的媒体 
	 * @param permissionResponsibleMedium
	 */
	public void setPermissionResponsibleMedium(Map<Integer, String> permissionResponsibleMedium) {
		this.permissionResponsibleMedium = permissionResponsibleMedium;
	}
	
}
