package com.dataeye.ad.assistor.context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.ProceedingJoinPoint;

import com.dataeye.ad.assistor.constant.Constant.ServerCfg;

/**
 * <pre>
 * DataEye Context
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.28 17:55:32
 */
public class DEContext {
    /**
     * 请求的ID
     */
    private String ID;
    /**
     * request 对象
     */
    private HttpServletRequest request;
    /**
     * response 对象
     */
    private HttpServletResponse response;
    /**
     * session 对象
     */
    private HttpSession session;
    /**
     * 请求的类名
     */
    private String className;
    /**
     * 请求的方法名
     */
    private String method;
    /**
     * 参数处理的对象
     */
    private DEParameter deParameter;
    /**
     * 接口响应对象
     */
    private DEResponse deResponse;

    public DEContext(ProceedingJoinPoint jp) {
        // 设置request/response/session对象
        this.request = (HttpServletRequest) jp.getArgs()[0];
        this.response = (HttpServletResponse) jp.getArgs()[1];
        this.session = this.request.getSession();
        // 设置类名和方法名
        this.className = jp.getTarget().getClass().getSimpleName();
        this.method = jp.getSignature().getName();
        // 设置DeResponse
        this.deResponse = new DEResponse();
        this.ID = this.deResponse.getId();
        this.deParameter = new DEParameter(this.request, this.deResponse.getId());
        // 把DEContext对象设置到Request属性中
        // 记录该ID的一个跟踪开始
        this.request.setAttribute(ServerCfg.DE_CONTEXT, this);
    }

    // *********************getters and setters******************************
    public String getID() {
        return ID;
    }

    public void setID(String iD) {
        ID = iD;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public HttpSession getSession() {
        return session;
    }

    public String getClassName() {
        return className;
    }

    public String getMethod() {
        return method;
    }

    public DEParameter getDeParameter() {
        return deParameter;
    }

    public DEResponse getDeResponse() {
        return deResponse;
    }

}
