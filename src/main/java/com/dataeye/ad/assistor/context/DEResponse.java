package com.dataeye.ad.assistor.context;

import java.util.Map;

import com.dataeye.ad.assistor.common.CachedObjects;
import com.dataeye.ad.assistor.constant.HttpStatusCode;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.util.ServerUtils;
import com.google.gson.annotations.Expose;

/**
 * <pre>
 * 每个接口处理完的响应对象
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:50:46
 */
public class DEResponse {
	/**
	 * http 状态码
	 */
	private HttpStatusCode httpStatusCode;
	/**
	 * 标识请求的唯一ID
	 */
	@Expose
	private String id;
	/**
	 * 状态码
	 */
	@Expose
	private int statusCode;
	/**
	 * 响应的内容
	 */
	@Expose
	private Object content = "";
	/**
	 * 扩展字段
	 */
	@Expose
	private Map<String, Object> extend;

	public DEResponse() {
		this.id = ServerUtils.getResponseID();
		this.statusCode = StatusCode.SUCCESS;
		this.httpStatusCode = HttpStatusCode.SUCCESS;
	}

	public String toJson() {
		return CachedObjects.GSON_ONLY_EXPOSE.toJson(this);
	}

	public HttpStatusCode getHttpStatusCode() {
		return httpStatusCode;
	}

	public void setHttpStatusCode(HttpStatusCode httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

	public Map<String, Object> getExtend() {
		return extend;
	}

	public void setExtend(Map<String, Object> extend) {
		this.extend = extend;
	}

}
