package com.dataeye.ad.assistor.context;

import java.util.HashMap;
import java.util.Map;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

/**
 * <pre>
 * 存在在session里面的用户信息
 */
public class UserInSession extends DataPermissionDomain {
	/** 企业账号UID */
	private int companyAccountUid;
	/** token */
	private String token;
	/** 账号ID */
	private int accountId;
	/** 账号名称（邮箱） */
	private String accountName;
	/** 账号角色 */
	private int accountRole;
	/** 用户访问平台的菜单权限 */
	private String menuPermission;
	/** 授权产品 */
	private Map<Integer, String> authorizedProductMap = new HashMap<Integer, String>();
//	/** 用户访问平台的接口权限Set集合 */
//	private Set<String> interfacePermissionSet = new HashSet<String>(); 
	/** 用户访问平台的接口与数据权限Map集合 */
	private Map<String, Integer> interfaceDataPermissionMap = new HashMap<String, Integer>(); 
	
	/**
	 * 判断是否有接口访问权限
	 * @param interfaceName
	 * @return
	 */
	public boolean hasInterfacePermission(String interfaceName) {
		if(interfaceDataPermissionMap.containsKey(interfaceName)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 判断是否有数据访问权限
	 * @param interfaceName
	 * @return
	 */
	public boolean hasDataPermission(String interfaceName) {
		boolean hasDataPermission = false;
		Integer permissionType = interfaceDataPermissionMap.get(interfaceName);
		if(permissionType != null) {
			if(permissionType == 0) { // 所有权限
				hasDataPermission = true;
			} else if(permissionType == 1) { // 负责人&关注人权限
				hasDataPermission = this.hasManagerOrFollowerPermission();
			} else if(permissionType == 2) { // 负责人权限
				hasDataPermission = this.hasManagerPermission();
			}
		}
		return hasDataPermission;
	}
	
	/**
     * 是否拥有负责人权限
     *
     * @param dataPermissionDomain
     * @return
     */
    public boolean hasManagerPermission() {
        return getOptimizerPermissionMediumAccountIds() != null;
    }

    /**
     * 是否拥有负责人或关注人权限
     *
     * @param dataPermissionDomain
     * @return
     */
    public boolean hasManagerOrFollowerPermission() {
        return getPermissionMediumAccountIds() != null;
    }
	
    /**
	 * 如果是开发工程师角色，判断是否有被授权产品，如果没有，返回空数据列表
	 * @return
	 */
	public boolean hasAuthorizedProduct() {
		if(authorizedProductMap != null && authorizedProductMap.size() > 0) {
			return true;
		}
		return false;
	}
	
	public UserInSession(int accountId) {
		this.accountId = accountId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public int getAccountRole() {
		return accountRole;
	}

	public void setAccountRole(int accountRole) {
		this.accountRole = accountRole;
	}

	public String getMenuPermission() {
		return menuPermission;
	}

	public void setMenuPermission(String menuPermission) {
		this.menuPermission = menuPermission;
	}

//	public Set<String> getInterfacePermissionSet() {
//		return interfacePermissionSet;
//	}
//
//	public void setInterfacePermissionSet(Set<String> interfacePermissionSet) {
//		this.interfacePermissionSet = interfacePermissionSet;
//	}

	public int getCompanyAccountUid() {
		return companyAccountUid;
	}

	public void setCompanyAccountUid(int companyAccountUid) {
		this.companyAccountUid = companyAccountUid;
	}

	public Map<String, Integer> getInterfaceDataPermissionMap() {
		return interfaceDataPermissionMap;
	}

	public void setInterfaceDataPermissionMap(
			Map<String, Integer> interfaceDataPermissionMap) {
		this.interfaceDataPermissionMap = interfaceDataPermissionMap;
	}

	public Map<Integer, String> getAuthorizedProductMap() {
		return authorizedProductMap;
	}

	public void setAuthorizedProductMap(Map<Integer, String> authorizedProductMap) {
		this.authorizedProductMap = authorizedProductMap;
	}
}
