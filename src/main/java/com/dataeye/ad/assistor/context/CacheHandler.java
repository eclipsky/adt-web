package com.dataeye.ad.assistor.context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.privilege.PrivilegeControl;

@Controller
public class CacheHandler {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(CacheHandler.class);

    /**
     * 刷新缓存
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/20
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.Public, write = false)
    @RequestMapping("/ad/cache/refresh.do")
    public Object refresh(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("刷新缓存");
		// 重新加载公司产品信息
		ApplicationContextContainer.reloadCompanyProductMap();
		// 重新加载公司系统账号与媒体账号映射关系信息
		ApplicationContextContainer.reloadSystemAccountToMediumAccountMap(null);
		// 重新加载组长所对应所有组员系统账号信息
		ApplicationContextContainer.reloadLeaderMemberAccountsMap(null);
		// 重新加载组长所对应所有组员ADT账号ID信息到ApplicationContext
		ApplicationContextContainer.reloadLeaderMemberAccountIdsMap(null);
		// 重新加载产品分类列表信息
		ApplicationContextContainer.reloadProductCategoryList();
		// 重新加载ADT媒体对应的渠道信息
		ApplicationContextContainer.reloadAdtMediumMappingMap();
		// 重新加载开发工程师被授权产品 信息
		ApplicationContextContainer.reloadDevAuthorizedProductMap(null);
		// 重新加载配置表 信息
		ApplicationContextContainer.reloadConfigVoMap();
        return null;
    }
}
