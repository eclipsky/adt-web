package com.dataeye.ad.assistor.context;

import ch.qos.logback.classic.Logger;
import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.Constant.PtLoginAccessStatus;
import com.dataeye.ad.assistor.constant.RemoteInterfaceConstants.Tracking;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ClientException;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.company.constants.Constants.CompanyStatus;
import com.dataeye.ad.assistor.module.company.model.Company;
import com.dataeye.ad.assistor.module.company.service.TrackingCompanyService;
import com.dataeye.ad.assistor.module.systemaccount.model.PtLoginUser;
import com.dataeye.ad.assistor.module.systemaccount.service.PtLoginService;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Tracking账号审核处理器
 * @author luzhuyou 2017/07/21
 */
public class TrackingAuditHandler {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(TrackingAuditHandler.class);
	
	/**
	 * 查询Tracking账号信息
	 * @param uid
	 * @param email 邮箱号
	 * @return
	 */
	public static boolean verifyTrackingUser(int uid, String email) {
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("uid", uid+"");
		params.put("userName", email);
		
		String userVerifyInterface = ConfigHandler.getProperty(Tracking.USER_VERIFY_KEY, Tracking.USER_VERIFY);
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(userVerifyInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("检测Tracking产品账号是否需要审核接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	boolean isNeedAudit = true;
    	if(statusCode == 200) {
    		logger.info("Tracking产品账号正常，允许登入！");
    		isNeedAudit = false;
        } else if(statusCode == 403) {
    		logger.info("Tracking产品账号不存在，请进行审核！");
    		isNeedAudit = true;
        } else {
    		logger.error("检测Tracking产品账号是否需要审核接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	return isNeedAudit;
	}
	
	/**
	 * 
	 * <pre>
	 * Tracking账号审核
	 *  @param request
	 *  @author luzhuyou 2017/07/21
	 * <br>
	 */
	public static int checkUser(HttpServletRequest request) {
		logger.info("Tracking账号审核处理.");
		String cookies = request.getHeader("Cookie");
		// user:UserInSession; status：0-正常，1-token失效，返回登录页；2-帐号未初始化；3-用户审核中；4-公司状态异常； 6-用户状态异常
		
		if(StringUtils.isBlank(cookies)) {
			// 清理session，回到登录
			TrackingAuditHandler.clear(request);
			logger.error("Cookie为空，Session失效");
			return PtLoginAccessStatus.TOKEN_NULL;
		}
		// 校验token
		String token = getCookieValue(cookies, Constant.CookieName.TOKEN_NAME);
		if(null == token){
			// 清理session，回到登录
			TrackingAuditHandler.clear(request);
			logger.error("Token为空，Session失效");
			return PtLoginAccessStatus.TOKEN_NULL;
		}
		
		PtLoginService ptLoginService = (PtLoginService) ApplicationContextContainer.getBean("ptLoginService");
		TrackingCompanyService trackingCompanyService = (TrackingCompanyService) ApplicationContextContainer.getBean("trackingCompanyService");
		
		// 1. 根据token检查并获取统一登录用户信息
		PtLoginUser ptLoginUser = null;
		try {
			ptLoginUser = ptLoginService.checkAndGetPtLoginAccount(token);
		} catch (ClientException e) {
			e.printStackTrace();
			logger.error("获取统一登录用户信息异常.");
			return PtLoginAccessStatus.TOKEN_NULL;
		}
		boolean isNeedAudit = verifyTrackingUser(ptLoginUser.getUID(), ptLoginUser.getUserID());
		int status = PtLoginAccessStatus.NORMAL;

		// 获取并判断公司状态，只有正常状态才可登录
		String email = ptLoginUser.getUserID();
		Company company = trackingCompanyService.getByEmail(email);

		if(!isNeedAudit) {
			// 如果不需要审核，且公司在crm里的状态为冻结或停用，则不允许登录 modify by luzhuyou 20171207
			if(company != null && (company.getStatus() == CompanyStatus.FROZEN || company.getStatus() == CompanyStatus.DISABLE)) {
				logger.error("[{}]公司状态异常.", ptLoginUser.getCompanyName());
				status = PtLoginAccessStatus.COMPANY_ABNORMAL;
			} else {
				logger.info("校验Tracking账号通过.正常登陆！");
				status = PtLoginAccessStatus.NORMAL;
			}
		} else {
			if(company == null) {
				logger.error("您的帐号[{}]还未初始化，请确认是否需要进行初始化.", email);
				status = PtLoginAccessStatus.NOT_INIT;
			} else {
				int companyStatus = company.getStatus(); 
				if(companyStatus == CompanyStatus.BIZ_AUDIT || companyStatus == CompanyStatus.PAUSE || companyStatus == CompanyStatus.OPE_AUDIT) {
					logger.error("[{}]用户审核中.", email);
					status = PtLoginAccessStatus.IS_AUDIT;
				} else if(companyStatus == CompanyStatus.FROZEN || companyStatus == CompanyStatus.DISABLE) {
					logger.error("[{}]公司状态异常.", ptLoginUser.getCompanyName());
					status = PtLoginAccessStatus.COMPANY_ABNORMAL;
				} else if(companyStatus != 0) {
					logger.error("[{}]用户状态异常.", ptLoginUser.getCompanyName());
					status = PtLoginAccessStatus.USER_ABNORMAL;
				}
			}
		}
		return status;
	}
		

	/**
	 * 
	 * <pre>
	 * 清空当前session
	 *  @param request  
	 *  @author Ivan<br>
	 *  @date 2015年3月14日 下午5:16:08
	 * <br>
	 */
	public static void clear(HttpServletRequest request) {
		HttpSession session = request.getSession();
		clearSession(session);
	}
	
	/**
	 * 
	 * <pre>
	 * 清空session
	 *  @param session  
	 *  @author Ivan<br>
	 *  @date 2015年3月14日 下午5:15:36
	 * <br>
	 */
	private static void clearSession(HttpSession session) {
		Enumeration<String> names = session.getAttributeNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			session.removeAttribute(name);
		}
	}
	
	public static String getCookieValue(String cookies, String field) {
		return getCookieMap(cookies).get(field);
	}

	public static Map<String, String> getCookieMap(String cookies) {
		Map<String, String> cookieMap = new HashMap<String, String>();
		String[] cookieArr = cookies.split(";");
		for (String cookie : cookieArr) {
			String[] kv = cookie.split("=");
			String key = kv[0].trim();
			String value = "";
			if (kv.length == 2) {
				value = kv[1].trim();
			}
			cookieMap.put(key, value);
		}
		return cookieMap;
	}
}
