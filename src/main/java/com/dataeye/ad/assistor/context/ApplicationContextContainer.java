package com.dataeye.ad.assistor.context;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.module.dictionaries.model.ConfigVo;
import com.dataeye.ad.assistor.module.dictionaries.model.ProductCategory;
import com.dataeye.ad.assistor.module.dictionaries.model.ProductCategoryResult;
import com.dataeye.ad.assistor.module.dictionaries.service.ConfigService;
import com.dataeye.ad.assistor.module.dictionaries.service.ProductCategoryService;
import com.dataeye.ad.assistor.module.mediumaccount.model.Channel;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountVo;
import com.dataeye.ad.assistor.module.mediumaccount.service.MediumAccountService;
import com.dataeye.ad.assistor.module.mediumaccount.service.MediumService;
import com.dataeye.ad.assistor.module.product.model.ProductVo;
import com.dataeye.ad.assistor.module.product.service.ProductService;
import com.dataeye.ad.assistor.module.systemaccount.model.DevAuthorizedProductVo;
import com.dataeye.ad.assistor.module.systemaccount.model.GroupDetailVo;
import com.dataeye.ad.assistor.module.systemaccount.service.DevAuthorizedProductService;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountGroupService;
import com.dataeye.ad.assistor.util.StringUtil;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * <pre>
 * 应用程序级Context上下文容器
 * @author luzhuyou 2017.03.02
 */
@Component
public class ApplicationContextContainer implements ApplicationContextAware {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(ApplicationContextContainer.class);

	private static ApplicationContext applicationContext;
	private static String[] allBeans;
	/** 公司产品信息 */
	private static Map<Integer, List<ProductVo>> companyProductMap = new HashMap<Integer, List<ProductVo>>();
	/** 公司账号与（负责&关注）媒体账号映射关系信息 */
	private static Map<Integer, Map<String, List<MediumAccountVo>>> childAccountsToMediumAccountMap = new HashMap<Integer, Map<String, List<MediumAccountVo>>>();
	/** 公司账号与（负责）媒体账号映射关系信息 */
	private static Map<Integer, Map<String, List<MediumAccountVo>>> responsibleAccountsToMediumAccountMap = new HashMap<Integer, Map<String, List<MediumAccountVo>>>();
	/** 公司账号与（关注）媒体账号映射关系信息 */
	private static Map<Integer, Map<String, List<MediumAccountVo>>> followerAccountsToMediumAccountMap = new HashMap<Integer, Map<String, List<MediumAccountVo>>>();
	/** 组长所对应所有组员ADT账号信息 */
	private static Map<Integer, Map<String, Set<String>>> leaderMemberAccountsMap = new HashMap<Integer, Map<String, Set<String>>>();
	/** 组长所对应所有组员ADT账号ID信息 */
	private static Map<Integer, Map<String, Set<Integer>>> leaderMemberAccountIdsMap = new HashMap<Integer, Map<String, Set<Integer>>>();
	/**产品分类列表信息**/
	private static List<ProductCategoryResult> productCategoryList = new ArrayList<ProductCategoryResult>();
	/**adt媒体对应的渠道对象**/
	private static Map<Integer, List<Channel>> adtMediumMappingMap = new HashMap<Integer, List<Channel>>();
	/** 开发工程师被授权产品 **/ //Map<CompanyId, Map<AccountId, Product>>
	private static Map<Integer, Map<Integer, DevAuthorizedProductVo>> devAuthorizedProductMap = new HashMap<Integer, Map<Integer, DevAuthorizedProductVo>>();
	
	/**配置表信息,Map<vkey,value>**/
	private static Map<String,String> configVoMap = new HashMap<String,String>();
	private static Map<String,List<Integer>> configUserIdAndCompanyIdMap = new HashMap<String,List<Integer>>();
	
	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		ApplicationContextContainer.applicationContext = arg0;
		allBeans = arg0.getBeanDefinitionNames();
		
		// 初始化公司产品信息到ApplicationContext
		initCompanyProductMap();
		
		// 初始化ADT账号与媒体账号映射关系到ApplicationContext
		initAdtAccountToMediumAccountMap(null);
		
		// 初始化组长所对应所有组员ADT账号信息到ApplicationContext
		initLeaderMemberAccountsMap(null);
		
		// 初始化产品分类信息到ApplicationContext
		initProductCategoryList();
		
		// 初始化组长所对应所有组员ADT账号id信息到ApplicationContext
		initLeaderMemberAccountIdsMap(null);
		
		// 初始化adt媒体对应的渠道对象到ApplicationContext
		initAdtMediumMappingMap();
		
		// 初始化开发工程师被授权产品信息到缓存
		initDevAuthorizedProductMap(null);
		
		// 初始化配置表信息到ApplicationContext
		initConfigVoMap();
	}
	
	public static Object getBean(String bean) {
		return applicationContext.getBean(bean);
	}

	public static String[] getAllBeans() {
		return allBeans;
	}
	
	/**
	 * 初始化公司产品信息到ApplicationContext
	 */
	private static void initCompanyProductMap() {
		logger.info("初始化公司产品信息到ApplicationContext");
		ProductService productService = (ProductService) applicationContext.getBean("productService");
		List<ProductVo> productVoList = productService.query(null);
		if(productVoList != null && !productVoList.isEmpty()) {
			for(ProductVo vo : productVoList) {
				int companyId = vo.getCompanyId();
				List<ProductVo> voList = companyProductMap.get(companyId);
				if(voList == null) {
					voList = new ArrayList<ProductVo>();
					companyProductMap.put(companyId, voList);
				}
				voList.add(vo);
			}
		}
	}
	
	/**
	 * 初始化(负责人&关注者)账号与媒体账号映射关系到ApplicationContext
	 * @param companyId
	 */
	private static void initAdtAccountToMediumAccountMap(Integer companyId) {
		logger.info("初始化ADT账号与媒体账号映射关系到ApplicationContext");
		MediumAccountService mediumAccountService = (MediumAccountService) applicationContext.getBean("mediumAccountService");
		List<MediumAccountVo> mediumAccountVoList = mediumAccountService.query(companyId);
		
		if(mediumAccountVoList != null && !mediumAccountVoList.isEmpty()) {
			for(MediumAccountVo vo : mediumAccountVoList) {
				// 缓存关注人账号
				addFollowerAccountsToCache(vo);
				
				// 缓存负责人账号
				addResponsibleAccountsToCache(vo);
				
				// 缓存负责人&关注人账号
				addResponsibleAndFollowerAccountsToCache(vo);
			}
		}
	}
	
	/**
	 * 缓存关注人账号
	 * @param vo
	 */
	private static void addFollowerAccountsToCache(MediumAccountVo vo) {
		int companyId = vo.getCompanyId();
		// 缓存关注人账号
		Map<String, List<MediumAccountVo>> foolowersMap = followerAccountsToMediumAccountMap.get(companyId);
		if(foolowersMap == null) {
			foolowersMap = new HashMap<String, List<MediumAccountVo>>();
			followerAccountsToMediumAccountMap.put(companyId, foolowersMap);
		}
		// 添加关注人账号到缓存
		String followersAccountNameList = vo.getResponsibleAccounts();
		if(StringUtils.isNotBlank(followersAccountNameList)) {
			String[] systemAccountNameArr = followersAccountNameList.trim().split(",");
			for(int i=0; i<systemAccountNameArr.length; i++) {
				String systemAccountName = systemAccountNameArr[i];
				List<MediumAccountVo> mediumAccountList = foolowersMap.get(systemAccountName);
				if(mediumAccountList == null) {
					mediumAccountList = new ArrayList<MediumAccountVo>();
					foolowersMap.put(systemAccountName, mediumAccountList);
				}
				mediumAccountList.add(vo);
			}
		}
	}
	
	/**
	 * 缓存负责人ADT账号
	 * @param vo
	 */
	private static void addResponsibleAccountsToCache(MediumAccountVo vo) {
		int companyId = vo.getCompanyId();
		// 缓存负责人ADT账号
		Map<String, List<MediumAccountVo>> optimizerMap = responsibleAccountsToMediumAccountMap.get(companyId);
		if(optimizerMap == null) {
			optimizerMap = new HashMap<String, List<MediumAccountVo>>();
			responsibleAccountsToMediumAccountMap.put(companyId, optimizerMap);
		}
		// 添加负责人ADT账号到缓存
		String optimizerAccountNameList = vo.getResponsibleAccounts();
		if(StringUtils.isNotBlank(optimizerAccountNameList)) {
			String[] systemAccountNameArr = optimizerAccountNameList.trim().split(",");
			for(int i=0; i<systemAccountNameArr.length; i++) {
				String systemAccountName = systemAccountNameArr[i];
				List<MediumAccountVo> mediumAccountList = optimizerMap.get(systemAccountName);
				if(mediumAccountList == null) {
					mediumAccountList = new ArrayList<MediumAccountVo>();
					optimizerMap.put(systemAccountName, mediumAccountList);
				}
				mediumAccountList.add(vo);
			}
		}
	}
	
	/**
	 * 缓存负责人&关注人账号
	 * @param vo
	 */
	private static void addResponsibleAndFollowerAccountsToCache(MediumAccountVo vo) {
		int companyId = vo.getCompanyId();
		Map<String, List<MediumAccountVo>> optimizerAndFollowersMap = childAccountsToMediumAccountMap.get(companyId);
		if(optimizerAndFollowersMap == null) {
			optimizerAndFollowersMap = new HashMap<String, List<MediumAccountVo>>();
			childAccountsToMediumAccountMap.put(companyId, optimizerAndFollowersMap);
		}
		
		// 添加负责人ADT账号到缓存
		String responsibleAccountNameList = vo.getResponsibleAccounts();
		if(StringUtils.isNotBlank(responsibleAccountNameList)) {
			String[] systemAccountNameArr = responsibleAccountNameList.trim().split(",");
			for(int i=0; i<systemAccountNameArr.length; i++) {
				String systemAccountName = systemAccountNameArr[i];
				List<MediumAccountVo> mediumAccountList = optimizerAndFollowersMap.get(systemAccountName);
				if(mediumAccountList == null) {
					mediumAccountList = new ArrayList<MediumAccountVo>();
					optimizerAndFollowersMap.put(systemAccountName, mediumAccountList);
				}
				mediumAccountList.add(vo);
			}
		}

		// 添加关注人账号到缓存
		String followerAccountNameList = vo.getFollowerAccounts();
		if(StringUtils.isNotBlank(followerAccountNameList)) {
			String[] systemAccountNameArr = followerAccountNameList.trim().split(",");
			for(int i=0; i<systemAccountNameArr.length; i++) {
				String systemAccountName = systemAccountNameArr[i];
				List<MediumAccountVo> mediumAccountList = optimizerAndFollowersMap.get(systemAccountName);
				boolean exists = false;
				if(mediumAccountList == null) {
					mediumAccountList = new ArrayList<MediumAccountVo>();
					optimizerAndFollowersMap.put(systemAccountName, mediumAccountList);
				} else {
					// 如果关注人账号与负责人ADT账号有重复，则不添加
					for(int j=0; j<mediumAccountList.size(); j++) {
						MediumAccountVo existsAccount = mediumAccountList.get(j);
						// if(existsAccount.getMediumAccount().endsWith(vo.getMediumAccount())) {
						if(existsAccount.getMediumAccountId() == vo.getMediumAccountId()) {
							exists = true;
							break;
						}
					}
				}
				if(!exists) {
					mediumAccountList.add(vo);
				}
			}
		}
	}
	
	/**
	 * 初始化组长所对应所有组员ADT账号信息到ApplicationContext
	 */
	private static void initLeaderMemberAccountsMap(Integer companyId) {
		logger.info("初始化组长所对应所有组员ADT账号信息到ApplicationContext");
		SystemAccountGroupService systemAccountGroupService = (SystemAccountGroupService) applicationContext.getBean("systemAccountGroupService");
		List<GroupDetailVo> groupDetailVoList = systemAccountGroupService.query(null, companyId);
		
		if(groupDetailVoList != null) {
			for(int i=0; i<groupDetailVoList.size(); i++) {
				GroupDetailVo vo = groupDetailVoList.get(i);
				int tmpCompanyId = vo.getCompanyId();
				String leaderAccount = vo.getLeaderAccount();
				if(StringUtils.isBlank(leaderAccount)) {
					continue;
				}
				Map<String, Set<String>> leaderMembersMap = leaderMemberAccountsMap.get(tmpCompanyId);
				if(leaderMembersMap == null) {
					leaderMembersMap = new HashMap<String, Set<String>>();
					leaderMemberAccountsMap.put(tmpCompanyId, leaderMembersMap);
				}
				Set<String> leaderMembersSet = leaderMembersMap.get(leaderAccount);
				if(leaderMembersSet == null) {
					leaderMembersSet = new HashSet<String>();
					leaderMembersMap.put(leaderAccount, leaderMembersSet);
				}
				leaderMembersSet.add(vo.getAccountName());
			}
		}
	}
	
	/**
	 * 根据指定公司ID获取公司对应的产品
	 * @param companyId
	 * @return
	 */
	public static List<ProductVo> getProductList(int companyId) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]对应的产品", companyId);
		return companyProductMap.get(companyId);
	}
	
	/**
	 * 根据指定公司ID获取公司对应的产品
	 * @param companyId
	 * @return
	 */
	public static List<ProductVo> getProductList(int companyId, Set<Integer> productIdsSet) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]对应的产品", companyId);
		if(null == productIdsSet || productIdsSet.isEmpty()) {
			return null;
		}
		List<ProductVo> productList = companyProductMap.get(companyId);
		List<ProductVo> selectProductList = new ArrayList<ProductVo>();
		for(ProductVo vo : productList) {
			if(productIdsSet.contains(vo.getProductId())) {
				selectProductList.add(vo);
			}
		}
		return selectProductList;
	}
	
	/**
	 * 根据指定公司Id及关注人账号获取所管理的媒体账号
	 * @param companyId
	 * @param followersAccountName
	 * @return
	 */
	public static List<MediumAccountVo> queryMediumAccountVoForFollowersList(int companyId, String followersAccountName) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]下关注人账号[{}]所管理的媒体账号列表", new Object[]{companyId, followersAccountName});
		Map<String, List<MediumAccountVo>> map = followerAccountsToMediumAccountMap.get(companyId);
		if(map != null && !map.isEmpty()) {
			return map.get(followersAccountName);
		}
		return null;
	}
	
	/**
	 * 根据指定公司Id及负责人ADT账号获取所管理的媒体账号
	 * @param companyId
	 * @param optimizerAccountName
	 * @return
	 */
	public static List<MediumAccountVo> queryMediumAccountVoForResponsibleAccountList(int companyId, String optimizerAccountName) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]下负责人ADT账号[{}]所管理的媒体账号列表", new Object[]{companyId, optimizerAccountName});
		Map<String, List<MediumAccountVo>> map = responsibleAccountsToMediumAccountMap.get(companyId);
		if(map != null && !map.isEmpty()) {
			return map.get(optimizerAccountName);
		}
		return null;
	}
	
	/**
	 * 根据指定公司Id及ADT账号获取所管理的媒体账号
	 * @param companyId
	 * @param systemAccountName
	 * @return
	 */
	public static List<MediumAccountVo> getMediumAccountVoList(int companyId, String systemAccountName) {
		Map<String, List<MediumAccountVo>> map = childAccountsToMediumAccountMap.get(companyId);
		if(map != null && !map.isEmpty()) {
			//logger.debug("缓存的所管理的媒体账号Id数组为：[{}]", new Object[]{map.get(systemAccountName)});
			return map.get(systemAccountName);
		}
		return null;
	}
	

	/**
	 * 根据指定公司Id、ADT账号、ADT账号与媒体账号映射关系获取媒体
	 * @param companyId
	 * @param systemAccountName
	 * @param adtAccountsToMediumAccountMap ADT账号与媒体账号映射关系
	 * @return
	 */
	private static Map<Integer, String> getPermissionMedium(int companyId, String systemAccountName, Map<Integer,
			Map<String, List<MediumAccountVo>>> adtAccountsToMediumAccountMap) {
		Map<String, List<MediumAccountVo>> map = adtAccountsToMediumAccountMap.get(companyId);
		if(map == null || map.isEmpty()) {
			return null;
		}
		List<MediumAccountVo> mediumAccountList = map.get(systemAccountName);
		if(mediumAccountList == null || mediumAccountList.isEmpty()) {
			return null;
		}
		Map<Integer, String> mediumMap = new HashMap<Integer, String>();
		for(MediumAccountVo mediumAccount : mediumAccountList) {
			mediumMap.put(mediumAccount.getMediumId(), mediumAccount.getMediumName());
		}
//		List<Medium> result = new ArrayList<Medium>();
//		Medium medium = null;
//		for(int mediumId : mediumMap.keySet()) {
//			String mediumName = mediumMap.get(mediumId);
//			medium = new Medium();
//			medium.setMediumId(mediumId);
//			medium.setMediumName(mediumName);
//			result.add(medium);
//		}
		return mediumMap;
	}
	
	/**
	 * 根据指定公司Id及ADT账号获取所负责与关注的媒体（负责人&关注人）
	 * @param companyId
	 * @param systemAccountName
	 * @return
	 */
	public static Map<Integer, String> getPermissionMedium(int companyId, String systemAccountName) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]下ADT账号[{}]所负责与关注的媒体", new Object[]{companyId, systemAccountName});
		// 获取组长账号下所有组员的ADT账号
    	Set<String> membersAccount = ApplicationContextContainer.getLeaderMemberAccounts(companyId, systemAccountName);
    	// 如果不是组长角色，则返回空值，需要手动将当前ADT账号添加上
    	if(membersAccount == null) {
    		membersAccount = new HashSet<String>();
    	}
    	membersAccount.add(systemAccountName);
    	
    	Map<Integer, String> mediumMap = new HashMap<Integer, String>();
    	for(String member : membersAccount) {
    		// 根据所有负责人&关注人账号，获取对应媒体
    		Map<Integer, String> tmpMediumMap = getPermissionMedium(companyId, member, childAccountsToMediumAccountMap);
    		if(tmpMediumMap != null && tmpMediumMap.size() > 0) {
    			mediumMap.putAll(tmpMediumMap);
    		}
    	}
		return mediumMap;
	}
	
	/**
	 * 根据指定公司Id及ADT账号获取所负责的媒体（负责人）
	 * @param companyId
	 * @param systemAccountName
	 * @return
	 */
	public static Map<Integer, String> getPermissionResponsibleMedium(int companyId, String systemAccountName) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]下ADT账号[{}]所负责的媒体", new Object[]{companyId, systemAccountName});
		// 获取组长账号下所有组员的ADT账号
    	Set<String> membersAccount = ApplicationContextContainer.getLeaderMemberAccounts(companyId, systemAccountName);
    	// 如果不是组长角色，则返回空值，需要手动将当前ADT账号添加上
    	if(membersAccount == null) {
    		membersAccount = new HashSet<String>();
    	}
    	membersAccount.add(systemAccountName);
    	
    	Map<Integer, String> mediumMap = new HashMap<Integer, String>();
    	for(String member : membersAccount) {
    		// 根据所有负责人账号，获取对应媒体
    		Map<Integer, String> tmpMediumMap = getPermissionMedium(companyId, member, responsibleAccountsToMediumAccountMap);
    		if(tmpMediumMap != null && tmpMediumMap.size() > 0) {
    			mediumMap.putAll(tmpMediumMap);
    		}
    	}
		return mediumMap;
	}
	
	/**
	 * 根据指定公司Id及ADT账号获取所管理的媒体账号Id
	 * @param companyId
	 * @param systemAccountName
	 * @return
	 */
	private static Integer[] getMediumAccountIdArray(int companyId, String systemAccountName) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]下ADT账号[{}]所管理的媒体账号Id数组", new Object[]{companyId, systemAccountName});
		Map<String, List<MediumAccountVo>> map = childAccountsToMediumAccountMap.get(companyId);
		if(map != null && !map.isEmpty()) {
			List<MediumAccountVo> mediumAccountVoList = map.get(systemAccountName);
			logger.debug("缓存的所管理的媒体账号Id数组为：[{}]", new Object[]{mediumAccountVoList});
			if(mediumAccountVoList != null && !mediumAccountVoList.isEmpty()) {
				int size = mediumAccountVoList.size();
				Integer[] mediumAccountIdArr = new Integer[size];
				for(int i=0; i<size; i++) {
					mediumAccountIdArr[i] = mediumAccountVoList.get(i).getMediumAccountId();
				}
				return mediumAccountIdArr;
			}
		}
		return null;
	}
	
	/**
	 * 根据指定公司Id及关注人账号获取所管理的媒体账号Id
	 * @param companyId
	 * @param followerAccountName 关注人账号名称
	 * @return
	 */
	@Deprecated
	private static Integer[] getMediumAccountIdForFollowersArray(int companyId, String followerAccountName) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]下关注人账号[{}]所管理的媒体账号Id数组", new Object[]{companyId, followerAccountName});
		Map<String, List<MediumAccountVo>> map = followerAccountsToMediumAccountMap.get(companyId);
		if(map != null && !map.isEmpty()) {
			List<MediumAccountVo> mediumAccountVoList = map.get(followerAccountName);
			if(mediumAccountVoList != null && !mediumAccountVoList.isEmpty()) {
				int size = mediumAccountVoList.size();
				Integer[] mediumAccountIdArr = new Integer[size];
				for(int i=0; i<size; i++) {
					mediumAccountIdArr[i] = mediumAccountVoList.get(i).getMediumAccountId();
				}
				return mediumAccountIdArr;
			}
		}
		return null;
	}
	
	/**
	 * 根据指定公司Id及负责人ADT账号获取所管理的媒体账号Id
	 * @param companyId
	 * @param responsibleAccountName 负责人ADT账号名称
	 * @return
	 */
	private static Integer[] getMediumAccountIdForResponsibleAccountArray(int companyId, String responsibleAccountName) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]下负责人ADT账号[{}]所管理的媒体账号Id数组", new Object[]{companyId, responsibleAccountName});
		Map<String, List<MediumAccountVo>> map = responsibleAccountsToMediumAccountMap.get(companyId);
		if(map != null && !map.isEmpty()) {
			List<MediumAccountVo> mediumAccountVoList = map.get(responsibleAccountName);
			if(mediumAccountVoList != null && !mediumAccountVoList.isEmpty()) {
				int size = mediumAccountVoList.size();
				Integer[] mediumAccountIdArr = new Integer[size];
				for(int i=0; i<size; i++) {
					mediumAccountIdArr[i] = mediumAccountVoList.get(i).getMediumAccountId();
				}
				return mediumAccountIdArr;
			}
		}
		return null;
	}
	
	/**
	 * 根据指定公司Id及ADT账号获取所管理的媒体账号
	 * @param companyId
	 * @param systemAccountName
	 * @return
	 */
	private static String[] getMediumAccountNameArray(int companyId, String systemAccountName) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]下ADT账号[{}]所管理的媒体账号数组", new Object[]{companyId, systemAccountName});
		Map<String, List<MediumAccountVo>> map = childAccountsToMediumAccountMap.get(companyId);
		if(map != null && !map.isEmpty()) {
			List<MediumAccountVo> mediumAccountVoList = map.get(systemAccountName);
			if(mediumAccountVoList != null && !mediumAccountVoList.isEmpty()) {
				int size = mediumAccountVoList.size();
				String[] mediumAccountNameArr = new String[size];
				for(int i=0; i<size; i++) {
					mediumAccountNameArr[i] = mediumAccountVoList.get(i).getMediumAccount();
				}
				return mediumAccountNameArr;
			}
		}
		return null;
	}
	
	/**
	 * 根据指定公司Id及负责人ADT账号获取所管理的媒体账号Id，如果是组长，则返回组内所有成员对应的媒体账号Id（包括自身所对应的媒体账号）
	 * @param companyId
	 * @param responsibleAccountName
	 * @return
	 */
	public static Integer[] getMediumAccountIdForResponsibleAccount(int companyId, String responsibleAccountName) {
		// 获取组长账号下所有组员的ADT账号
    	Set<String> membersAccount = ApplicationContextContainer.getLeaderMemberAccounts(companyId, responsibleAccountName);
    	// 如果不是组长角色，则返回空值，需要手动将当前ADT账号添加上
    	if(membersAccount == null) {
    		membersAccount = new HashSet<String>();
    	}
    	membersAccount.add(responsibleAccountName);
    	
    	Set<Integer> membersAccountIdList = new HashSet<Integer>();
    	for(String member : membersAccount) {
    		// 根据所有组长负责人账号，获取对应媒体账号
    		Integer[] mediumAccountIdArray = ApplicationContextContainer.getMediumAccountIdForResponsibleAccountArray(companyId, member);
    		if(mediumAccountIdArray != null && mediumAccountIdArray.length > 0) {
    			membersAccountIdList.addAll(Arrays.asList(mediumAccountIdArray));
    		}
    	}
    	
    	Integer[] mediumAccountIdArray = new Integer[membersAccountIdList.size()];
		membersAccountIdList.toArray(mediumAccountIdArray);
		return mediumAccountIdArray;
	}
	
	/**
	 * 根据指定公司Id及(负责人&关注者)ADT账号获取所管理的媒体账号Id，如果是组长，则返回组内所有成员对应的媒体账号Id（包括自身所对应的媒体账号）
	 * @param companyId
	 * @param childAccountName
	 * @return
	 */
	public static Integer[] getMediumAccountIdForResponsibleAndFollowerAccounts(int companyId, String childAccountName) {
		// 获取组长账号下所有组员的ADT账号
    	Set<String> membersAccount = ApplicationContextContainer.getLeaderMemberAccounts(companyId, childAccountName);
    	// 如果不是组长角色，则返回空值，需要手动将当前ADT账号添加上
    	if(membersAccount == null) {
    		membersAccount = new HashSet<String>();
    	}
    	membersAccount.add(childAccountName);
    	
    	Set<Integer> membersAccountIdList = new HashSet<Integer>();
    	for(String member : membersAccount) {
    		// 根据所有负责人&关注人账号，获取对应媒体账号
    		Integer[] mediumAccountNameArray = ApplicationContextContainer.getMediumAccountIdArray(companyId, member);
    		if(mediumAccountNameArray != null && mediumAccountNameArray.length > 0) {
    			membersAccountIdList.addAll(Arrays.asList(mediumAccountNameArray));
    		}
    	}
    	
    	Integer[] mediumAccountNameArray = new Integer[membersAccountIdList.size()];
		membersAccountIdList.toArray(mediumAccountNameArray);
		return mediumAccountNameArray;
	}
	
	/**
	 * 根据指定公司Id及ADT账号获取所管理的媒体账号，如果是组长，则返回组内所有成员对应的媒体账号（包括自身所对应的媒体账号）
	 * @param companyId
	 * @param childAccountName
	 * @return
	 */
	public static String[] getMediumAccountByChildAccount(int companyId, String childAccountName) {
		// 获取组长账号下所有组员的ADT账号
    	Set<String> membersAccount = ApplicationContextContainer.getLeaderMemberAccounts(companyId, childAccountName);
    	// 如果不是组长角色，则返回空值，需要手动将当前ADT账号添加上
    	if(membersAccount == null) {
    		membersAccount = new HashSet<String>();
    	}
    	membersAccount.add(childAccountName);
    	
    	Set<String> membersAccountList = new HashSet<String>();
    	for(String member : membersAccount) {
    		// 根据所有ADT账号，获取对应媒体账号Id
    		String[] mediumAccountNameArray = ApplicationContextContainer.getMediumAccountNameArray(companyId, member);
    		if(mediumAccountNameArray != null && mediumAccountNameArray.length > 0) {
    			membersAccountList.addAll(Arrays.asList(mediumAccountNameArray));
    		}
    	}
    	
    	String[] mediumAccountNameArray = new String[membersAccountList.size()];
		membersAccountList.toArray(mediumAccountNameArray);
		return mediumAccountNameArray;
	}
	
	/**
	 * 根据指定公司ID及组长ADT账号获取所对应的所有组员ADT账号
	 * @param companyId
	 * @param systemAccountName
	 * @return
	 */
	public static Set<String> getLeaderMemberAccounts(int companyId, String systemAccountName) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]下组长ADT账号[{}]所对应的所有组员ADT账号", new Object[]{companyId, systemAccountName});
		Map<String, Set<String>> map = leaderMemberAccountsMap.get(companyId);
		if(map != null && !map.isEmpty()) {
			return map.get(systemAccountName);
		}
		return null;
	}
	
	/**
	 * 重新加载公司产品信息
	 * @return
	 */
	public static void reloadCompanyProductMap() {
		companyProductMap.clear();
		initCompanyProductMap();
	}
	
	/**
	 * 重新加载公司ADT账号与媒体账号映射关系信息
	 * @return
	 */
	public static void reloadSystemAccountToMediumAccountMap(Integer companyId) {
		childAccountsToMediumAccountMap.remove(companyId);
		responsibleAccountsToMediumAccountMap.remove(companyId);
		followerAccountsToMediumAccountMap.remove(companyId);
		initAdtAccountToMediumAccountMap(companyId);
	}
	
	/**
	 * 重新加载组长所对应所有组员ADT账号信息到ApplicationContext
	 * @return
	 */
	public static void reloadLeaderMemberAccountsMap(Integer companyId) {
		if(companyId == null) {
			leaderMemberAccountsMap.clear();
		}else {
			leaderMemberAccountsMap.remove(companyId);
		}
		initLeaderMemberAccountsMap(companyId);
		
		if(companyId == null) {
			leaderMemberAccountIdsMap.clear();
		}else {
			leaderMemberAccountIdsMap.remove(companyId);
		}
		initLeaderMemberAccountIdsMap(companyId);
	}
	
	/**
	 * 添加公司产品信息到ApplicationContext
	 */
	public static void addProduct(int companyId, int productId, String productName, Double shareRate, int status, String appId, int osType, int parentCategoryId, int childrenCategoryId, String parentCategoryName, String childrenCategoryName) {
		ProductVo vo = new ProductVo();
		vo.setCompanyId(companyId);
		vo.setProductId(productId);
		vo.setProductName(productName);
		vo.setShareRate(shareRate);
		vo.setStatus(status);
		vo.setAppId(appId);
		vo.setOsType(osType);
		vo.setParentCategoryId(parentCategoryId);
		vo.setChildrenCategoryId(childrenCategoryId);
		vo.setParentCategoryName(parentCategoryName);
		vo.setChildrenCategoryName(childrenCategoryName);
		List<ProductVo> voList = companyProductMap.get(companyId);
		if(voList == null) {
			voList = new ArrayList<ProductVo>();
			companyProductMap.put(companyId, voList);
		}
		voList.add(vo);
	}
	
	/**
	 * 修改公司产品信息到ApplicationContext
	 * @param companyId
	 * @param productId
	 * @param productName
	 * @param shareRate
	 * @param status
	 * @param parentCategoryId
	 * @param childrenCategoryId
	 * @param parentCategoryName
	 * @param childrenCategoryName
	 * @return
	 */
	public static int modifyProduct(int companyId, int productId, String productName, Double shareRate, int status, int parentCategoryId, int childrenCategoryId, String parentCategoryName, String childrenCategoryName) {
		
		List<ProductVo> voList = companyProductMap.get(companyId);
		if(voList != null) {
			for(int i=0; i<voList.size(); i++) {
				ProductVo vo = voList.get(i);
				if(vo.getProductId() == productId) {
					vo.setProductName(productName);
					vo.setShareRate(shareRate);
					vo.setStatus(status);
					vo.setParentCategoryId(parentCategoryId);
					vo.setChildrenCategoryId(childrenCategoryId);
					vo.setParentCategoryName(parentCategoryName);
					vo.setChildrenCategoryName(childrenCategoryName);
					return 1;
				}
			}
		}
		return 0;
	}
	
	/**
	 * 修改产品关联的trackingApp到ApplicationContext
	 * @param companyId
	 * @param productId
	 * @param appId
	 * @param appName
	 * @return
	 */
	public static int modifyProductRefApp(int companyId, int productId, String appId, String appName) {
		
		List<ProductVo> voList = companyProductMap.get(companyId);
		if(voList != null) {
			for(int i=0; i<voList.size(); i++) {
				ProductVo vo = voList.get(i);
				if(vo.getProductId() == productId) {
					vo.setAppId(appId);
					vo.setAppName(appName);
					return 1;
				}
			}
		}
		return 0;
	}
	
	/***
	 * 初始化产品分类信息
	 * parent_id=2是游戏分类的第一级，先取出第一级，再取出第二级
	 * */
	private static void initProductCategoryList(){
		logger.info("初始化产品分类信息到ApplicationContext");
		ProductCategoryService productCategoryService = (ProductCategoryService) applicationContext.getBean("productCategoryService");
		List<ProductCategory> list = productCategoryService.query(2);
		if(list.size()>0){
			for (ProductCategory pc : list) {
				ProductCategoryResult obj = new ProductCategoryResult();
				obj.setCategoryId(pc.getCategoryId());
				obj.setCategoryName(pc.getCategoryName());
				
				List<ProductCategory> listChildren = productCategoryService.query(pc.getCategoryId());
				if(listChildren.isEmpty()){
					listChildren = new ArrayList<ProductCategory>();
				}
				obj.setChildren(listChildren);
				productCategoryList.add(obj);
			}
		}
	}
			
	/**
	 * 获取所有产品分类
	 * @return
	 */
	public static List<ProductCategoryResult> getProductCategoryList() {
		logger.debug("从ApplicationContext缓存中获取所有产品分类");
		return productCategoryList;
	}
	
	/**
	 * 重新加载产品分类列表信息
	 * @return
	 */
	public static void reloadProductCategoryList() {
		productCategoryList.clear();
		initProductCategoryList();
	}
	
	
	/**
	 * 初始化组长所对应所有组员ADT账号ID信息到ApplicationContext
	 *  @param companyId
	 *  create by 2017-06-29
	 */
	private static void initLeaderMemberAccountIdsMap(Integer companyId) {
		logger.info("初始化组长所对应所有组员ADT账号id信息到ApplicationContext");
		SystemAccountGroupService systemAccountGroupService = (SystemAccountGroupService) applicationContext.getBean("systemAccountGroupService");
		List<GroupDetailVo> groupDetailVoList = systemAccountGroupService.query(null, companyId);
		
		if(groupDetailVoList != null) {
			for(int i=0; i<groupDetailVoList.size(); i++) {
				GroupDetailVo vo = groupDetailVoList.get(i);
				int tmpCompanyId = vo.getCompanyId();
				String leaderAccount = vo.getLeaderAccount();
				if(StringUtils.isBlank(leaderAccount)) {
					continue;
				}
				Map<String, Set<Integer>> leaderMembersMap = leaderMemberAccountIdsMap.get(tmpCompanyId);
				if(leaderMembersMap == null) {
					leaderMembersMap = new HashMap<String, Set<Integer>>();
					leaderMemberAccountIdsMap.put(tmpCompanyId, leaderMembersMap);
				}
				Set<Integer> leaderMembersSet = leaderMembersMap.get(leaderAccount);
				if(leaderMembersSet == null) {
					leaderMembersSet = new HashSet<Integer>();
					leaderMembersMap.put(leaderAccount, leaderMembersSet);
				}
				leaderMembersSet.add(vo.getAccountId());
				
			}
		
		}
	}
	
	/**
	 * 根据指定公司Id及ADT账号获取所管理的系统账号Id
	 * @param companyId
	 * @param systemAccountNames
	 * @return
	 */
	public static Set<Integer> getAccountIdArray(int companyId, String systemAccountNames) {
		logger.debug("从ApplicationContext缓存中获取公司ID[{}]下ADT账号[{}]所管理的系统账号Id数组", new Object[]{companyId, systemAccountNames});
		Map<String, Set<Integer>> map = leaderMemberAccountIdsMap.get(companyId);
		if(map != null && !map.isEmpty()) {
			return map.get(systemAccountNames);
		}
		return null;
	}
	
	/**
	 * 重新加载组长所对应所有组员ADT账号ID信息到ApplicationContext
	 * 
	 * @return
	 */
	public static void reloadLeaderMemberAccountIdsMap(Integer companyId) {
		if(companyId == null) {
			leaderMemberAccountIdsMap.clear();
		}else {
			leaderMemberAccountIdsMap.remove(companyId);
		}
		initLeaderMemberAccountIdsMap(companyId);
	}
	
	/**
	 * 初始化ADT媒体对应的渠道信息到ApplicationContext
	 * @create ldj 2017-08-11
	 */
	private static void initAdtMediumMappingMap() {
		logger.info("初始化ADT媒体对应的渠道信息到ApplicationContext");
		MediumService mediumService = (MediumService) applicationContext.getBean("mediumService");
		List<Channel> adtMediumMappingList = mediumService.getAdtMediumMapping();
		if(adtMediumMappingList != null && !adtMediumMappingList.isEmpty()) {
			for (Channel vo : adtMediumMappingList) {
				Integer mediumId = vo.getMediumId();
				List<Channel> list = new ArrayList<Channel>();
				if (adtMediumMappingMap.containsKey(mediumId)) {
					List<Channel> value = adtMediumMappingMap.get(mediumId);
					value.add(vo);
					list.addAll(value);
				}else{
					list.add(vo);
				}
				adtMediumMappingMap.put(mediumId, list);
			}
		}
	}
	/**
	 * 根据mediumId,获取ADT媒体对应的渠道信息
	 * @create ldj 2017-08-11
	 */
	public static List<Channel> getAdtMediumMappingList(Integer mediumId) {
		logger.debug("从ApplicationContext缓存中获取媒体ID[{}]下 对应的渠道信息", new Object[]{mediumId});
		List<Channel> list = adtMediumMappingMap.get(mediumId);
		if(list != null && !list.isEmpty()) {
			return list;
		}
		return null;
	}
	/**
	 * 重新加载ADT媒体对应的渠道信息到ApplicationContext
	 *  @create ldj 2017-08-11
	 * @return
	 */
	public static void reloadAdtMediumMappingMap() {
		adtMediumMappingMap.clear();
		initAdtMediumMappingMap();
	}
	
	/**
	 * 初始化开发工程师被授权产品信息到ApplicationContext
	 * @param companyId
	 */
	private static void initDevAuthorizedProductMap(Integer companyId) {
		logger.info("初始化开发工程师被授权产品信息到ApplicationContext");
		DevAuthorizedProductService devAuthorizedProductService = (DevAuthorizedProductService) applicationContext.getBean("devAuthorizedProductService");
		List<DevAuthorizedProductVo> devAuthorizedProductVoList = devAuthorizedProductService.query(companyId);
		if(devAuthorizedProductVoList != null && !devAuthorizedProductVoList.isEmpty()) {
			for(DevAuthorizedProductVo vo : devAuthorizedProductVoList) {
				int tmpCompanyId = vo.getCompanyId();
				Map<Integer, DevAuthorizedProductVo> voMap = devAuthorizedProductMap.get(tmpCompanyId);
				if(voMap == null) {
					voMap = new HashMap<Integer, DevAuthorizedProductVo>();
					devAuthorizedProductMap.put(tmpCompanyId, voMap);
				}
				voMap.put(vo.getAccountId(), vo);
			}
		}
	}
	
	/**
	 * 重新加载开发工程师被授权产品 信息
	 * @param companyId 如果公司ID为空，则重新加载所有公司信息
	 * @return
	 */
	public static void reloadDevAuthorizedProductMap(Integer companyId) {
		devAuthorizedProductMap.remove(companyId);
		initDevAuthorizedProductMap(companyId);
	}
	
	/**
	 * 根据指定公司ID获取公司下某账号被授权产品
	 * @param companyId
	 * @param accountId
	 * @return
	 */
	public static Map<Integer, String> getDevAuthorizedProduct(int companyId, int accountId) {
		logger.debug("从ApplicationContext缓存中根据指定公司ID[{}]获取公司下某账号[{}]被授权产品", new Object[]{companyId, accountId});
		Map<Integer, DevAuthorizedProductVo> voMap = devAuthorizedProductMap.get(companyId);
		if(voMap == null) {
			return null;
		}
		DevAuthorizedProductVo devAuthorizedProduct = voMap.get(accountId);
		if(devAuthorizedProduct == null) {
			return null;
		}
		List<ProductVo> voList = devAuthorizedProduct.getProductList();
		if(voList == null) {
			return null;
		}
		Map<Integer, String> productMap = new HashMap<Integer, String>();
		for(ProductVo vo : voList) {
			productMap.put(vo.getProductId(), vo.getProductName());
		}
		return productMap;
	}
	
	/***
	 * 初始化配置表信息到ApplicationContext
	 * */
	private static void initConfigVoMap(){
		logger.info("初始化配置表信息到ApplicationContext");
		ConfigService configService = (ConfigService) applicationContext.getBean("configService");
		List<ConfigVo> list = configService.queryAll();
		if(list.size()>0){
			for (ConfigVo vo : list) {
				String key = vo.getVkey();
				if(!configVoMap.containsKey(key)){
					configVoMap.put(key, vo.getValue());
					if(key.equals("gdt_active_company_ids")){
						List<Integer> activeCompanyList = StringUtil.stringToList(vo.getValue());
						configUserIdAndCompanyIdMap.put(key, activeCompanyList);
					}
					if(key.equals("gdt_active_user_ids")){
						List<Integer> activeUseIdsList = StringUtil.stringToList(vo.getValue());
						configUserIdAndCompanyIdMap.put(key, activeUseIdsList);
					}
				}
			}
		}
	}
	
	/**
	 * 重新加载配置表信息
	 * @return
	 */
	public static void reloadConfigVoMap() {
		configVoMap.clear();
		configUserIdAndCompanyIdMap.clear();
		initConfigVoMap();
	}
	
	/**
	 * 根据指定vkey获取对应的配置信息
	 * @param companyId
	 * @return
	 */
	public static String getConfigVo(String vkey) {
		logger.debug("从ApplicationContext缓存中获取配置对应的value", vkey);
		return configVoMap.get(vkey);
	}
	/**
	 * 根据指定vkey获取对应的配置信息
	 * @param companyId
	 * @return
	 */
	public static List<Integer> getConfigVoByList(String vkey) {
		logger.debug("从ApplicationContext缓存中获取配置对应的value", vkey);
		return configUserIdAndCompanyIdMap.get(vkey);
	}
	
}