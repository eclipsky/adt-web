package com.dataeye.ad.assistor.context;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ClientException;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.exception.ServerException;
import com.dataeye.ad.assistor.util.CookieUtils;
import com.dataeye.ad.assistor.util.ReflectUtils;
import com.dataeye.ad.assistor.util.ServerUtils;
import com.dataeye.ad.assistor.util.SqlUtils;
import com.dataeye.ad.assistor.util.ValidateUtils;
import com.qq.jutil.string.StringUtil;

/**
 * <pre>
 * 与参数解析相关的对象
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.28 17:55:17
 */
public class DEParameter extends Object {
    // ***********************************系统自己设置的,没有提供set方法的属性*********************************
    /* 保存该类所有的属性以及父类属性 */
    public static Set<String> FIELDSET;
    /* 保存该类所有的方法以及父类方法 */
    public static Set<String> METHODSET;

    /* 类加载的时候通过反射获取该类所有的属性以及方法 */
    static {
        FIELDSET = ReflectUtils.getAllFieldsIncludeParent(DEParameter.class);
        METHODSET = ReflectUtils.getAllMethodIncludeParent(DEParameter.class);
    }

    /* 存放所有参数 */
    private Map<String, String[]> parameterMap = new HashMap<String, String[]>();
    /* cookie */
	private Map<String, Cookie> cookieMap = new HashMap<String, Cookie>();
    /* request uri */
    private String uri;
    /* 接口名 */
    private String doName;
    /* 查询字符串 */
    private String queryString;
    /* url */
    private String url = "";
    /* downloadflag在外面用于控制下载请求的转发标志 */
    private int downloadflag;
    /* 请求ID */
    private String ID;
	
    /*
     * ********************************** 通用接收参数属性 ********************************* /
     */
    /* 查询开始日期 */
    private String startDate;
    /* 查询结束日期 */
    private String endDate;
    /* 查询开始时间 */
    private String startTime;
    /* 查询结束时间 */
    private String endTime;
    /* 每页显示多少条记录 */
    private int pageSize = 10;
    /* 请求第几页 */
    private int pageId = 1;
    /* 按照那一列排序 */
    private String orderBy = "";
    /* 升序还是降序 */
    private String order = SqlUtils.DESC;
    /* 搜索关键字 */
    private String searchKey = "";
    /* 操作数据的id */
    private int id = Constant.ServerCfg.INVALID_NUMBER;
    /* 批量操作数据id的集合，以逗号分隔 */
    private String ids = "";
    /* 操作数据的名称 */
    private String name;
    /* 创建时间 */
    private String createTime;

    /*
     * ********************************** 普通接收参数属性 ******************************** /
     */
    /* medium ID */
    private int mediumId = Constant.ServerCfg.INVALID_NUMBER;
    /* plan ID */
    private int planId = Constant.ServerCfg.INVALID_NUMBER;
    /* landpage ID */
    private int landpageId = Constant.ServerCfg.INVALID_NUMBER;
    /* data package ID */
    private int dataPkgId = Constant.ServerCfg.INVALID_NUMBER;
    /* 关联日期 */
    private String reportDate;
    /* 账户类型 */
    private int accountType = Constant.ServerCfg.INVALID_NUMBER;
    /* 账户 */
    private String account = "";
    /* 密码 */
    private String password = "";
    /* 计划ID列表，以逗号分隔 */
    private String planIds = "";
    /* 落地页ID列表，以逗号分隔 */
    private String landpageIds = "";
    /* 包ID列表，以逗号分隔 */
    private String dataPkgIds = "";
    /* 显示未消耗项 */
    private String showNotPay = "";

    /*
     * ********************************** 投放日报接收参数属性 ******************************** /
     */
    /**
     * 显示方式（汇总数据、按天显示数据）,上传的值分别为:total,daily
     */
    private String view;

    /**
     * 维度（推广计划、活动组、媒体）,值分别为plan,eventGroup, medium.
     */
    private String dimension;

    /**
     * 查询关键字（计划名称|活动组名称|渠道名称）.
     */
    private String query;

    /**
     * 忽略未消耗计划，值为true（忽略）或false.
     */
    private boolean ignoreZeroCostPlan;

    /**
     * 过滤未设置关联规则计划,值为true（过滤）或false.
     */
    private boolean ignoreUnboundPlan;


    /**
     * 账号ID.
     */
    private String accountId;

    /**
     * 表格链接.
     */
    private String link;

    /**
     * 投放日报子查询
     */
    private String subQuery;

    /**
     * 账号别名.
     */
    private String accountAlias;

    /**
     * 日期.
     */
    private String date;
    
    /**
     * 过滤媒体数据为空的计划，值为true（过滤）或false.
     */
    private boolean ignoreIsNullCostPlan;

    /*
     * ********************************** 回本日报接收参数属性 ******************************** /
     */
    /**
     * 媒体ID列表，逗号分隔.
     */
    private String mediumIds;
    /**
     * 账号ID列表，逗号分隔.
     */
    private String accountIds;

    /**
     * 限制返回的记录数
     */
    private int size=80;
    
    /* ********************************** 实时趋势参数属性 ******************************* */
    /**
     * 时段类型.
     */
	private String period;

	/**
	 * 实时趋势图指标.
	 */
	private String indicators;


    /**
     * <pre>
     * 参数名
     * @author Stran <br>
     * @version 1.0 <br>
     * @since 2016.09.29 18:12:21
     */
    public static class Keys {
		public static final String TOKEN = "token";
        public static final String START_DATE = "startDate";
        public static final String END_DATE = "endDate";
        public static final String PAGESIZE = "pageSize";
        public static final String PAGEID = "pageId";
        public static final String TOTAL_RECORD = "totalRecord";
        public static final String TOTAL_PAGE = "totalPage";
        public static final String CONTENT = "content";
        public static final String ID = "id";
        public static final String IDS = "ids";
        public static final String NAME = "name";
        public static final String MEDIUM_ID = "mediumId";
        public static final String MEDIUM_IDS = "mediumIds";
        public static final String PLAN_ID = "planId";
        public static final String LANDPAGE_ID = "landpageId";
        public static final String DATA_PKG_ID = "dataPkgId";
        public static final String PLAN_IDS = "planIds";
        public static final String LANDPAGE_IDS = "landpageIds";
        public static final String DATA_PKG_IDS = "dataPkgIds";
        public static final String ACCOUNT_TYPE = "accountType";
        public static final String ACCOUNT = "account";
        public static final String PASSWORD = "password";
        public static final String OPTIMIZERS = "optimizers";
        public static final String INDICATOR = "indicator";
        public static final String PRODUCT_ID = "productId";
        public static final String PRODUCT_IDS = "productIds";
        public static final String ACCOUNT_IDS="accountIds";
        public static final String MEDIUM_ACCOUNT_ID = "mediumAccountId";
        public static final String MEDIUM_ACCOUNT_IDS = "mediumAccountIds";
        public static final String EMAIL_ALERT = "emailAlert";
        public static final String MEDIUM_ACCOUNT = "mediumAccount";
        public static final String BID = "bid";
        public static final String MEDIUM_PLAN_ID = "mediumPlanId";
        public static final String PLAN_SWITCH = "planSwitch";
        public static final String BID_STRATEGY = "bidStrategy";
        public static final String PHASE = "phase";
        public static final String INDICATOR_PREFERENCE = "indicatorPreference";
        public static final String ACCOUNT_ROLE = "accountRole";
        public static final String INDICATOR_PERMISSION = "indicatorPermission";
        public static final String ALERT_CONFS = "alertConfs";
        public static final String MINUTES = "minutes";
        public static final String INDICATOR_VISIBILITY = "indicatorVisibility";
        public static final String PERIOD = "period";
        public static final String FILTER_NATURAL_FLOW = "filterNaturalFlow";
        public static final String IS_NATURAL_FLOW = "isNaturalFlow";
        public static final String MEDIUM_DATA = "mediumData";
    }

    /**
     * 构造方法
     *
     * @param request the request
     * @param ID      the id
     */
    public DEParameter(HttpServletRequest request, String ID) {
        this.ID = ID;
        if (request == null)
            return;
        // 取出所有参数并尽可能地自动设置到属性中
        autoSetField(request);
        // 解析cookie
 		CookieUtils.parseCookie(request, this.cookieMap);
        // 得到uri/queryString/url等
        this.uri = request.getRequestURI();
        this.doName = ServerUtils.getResourceName(request);
        this.queryString = parseQueryString();
        this.url = this.uri + this.queryString;
        System.out.println(this);
    }

    /**
     * 获取查询字符串
     *
     * @return string
     * @author Stran
     * @since 2016.09.29 18:12:10
     */
    public String parseQueryString() {
        StringBuilder queryString = new StringBuilder();
        if (parameterMap != null && parameterMap.size() > 0) {
            for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue()[0];
                queryString.append(key).append("=").append(value).append("&");
            }
            if (ValidateUtils.isNotEmpty(queryString)) {
                // 删除掉最后面那个&(只要有参数后面必定有一个&)
                queryString.deleteCharAt(queryString.length() - 1);
                return "?" + queryString.toString();
            }
        }
        return queryString.toString();
    }

    // ***********************常用获取参数值的方法********************************************

    /**
     * 自动设置参数值到对象中
     *
     * @param request
     */
    private void autoSetField(HttpServletRequest request) {
        /** 解析出所有的参数列表并存入parameterMap */
        this.parameterMap.putAll(request.getParameterMap());
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue()[0];
            if (StringUtil.isEmpty(value)) {
                continue;
            }
            // tomcat默认已经执行%xx decode of URL，这里的decodeWithUTF8应该是多余的了，看了tomcat源码是应该是确定的
            //value = ServerUtils.decodeWithUTF8(value).trim();
            value = value.trim();
            String setMethod = "set" + ServerUtils.toUppercaseFirstLetter(key);
            if (FIELDSET.contains(key) && METHODSET.contains(setMethod)) {// 调用set方法来设置值
                ReflectUtils.callMethodSet(DEParameter.class, key, this, value);
            }
        }
    }

    /**
	 * 
	 * <pre>
	 * 获取cookie值
	 *  @param cookieName
	 *  @return  
	 *  @author Ivan<br>
	 *  @date 2015年3月4日 上午11:15:32
	 * <br>
	 */
	public String getCookie(String cookieName) {
		Cookie cookie = cookieMap.get(cookieName);
		if (cookie != null) {
			return cookie.getValue();
		}
		return null;
	}
	
    /**
     * 获取参数值
     *
     * @param key
     * @return string
     * @author Stran
     * @since 2016.09.29 18:11:43
     */
    public String getParameter(String key) {
        String[] values = this.parameterMap.get(key);
        if (values != null && values.length > 0) {
            return values[0];
        }
        return null;
    }

    /**
     * 检查必填参数是否为非法值,如果为字符串验证是否为空，如果为数字验证是否为invalid_number
     *
     * @param parameterNames
     * @throws ClientException
     * @throws ServerException
     * @throws ServerException
     * @author Stran
     * @since 2016.09.29 18:11:37
     */
    public void checkParameter(String... parameterNames) throws ClientException, ServerException {
        for (String attr : parameterNames) {
            Class<?> clz = ReflectUtils.getFieldType(DEParameter.class, attr);
            Object object = ReflectUtils.getFieldValue(this, attr);
            if (object == null) {
                ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
            }
            if (clz == String.class) {// 是String类型，调用get方法，得到的值是否为空
                String value = (String) object;
                if (ValidateUtils.isEmpty(value)) {
                    ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
                }
            } else if (clz == Integer.class || clz == int.class) {
                if (!ValidateUtils.isValidNumber((Integer) object)) {
                    ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
                }
            } else if (clz == Long.class || clz == long.class) {
                if (!ValidateUtils.isValidNumber((Long) object)) {
                    ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
                }
            } else if (clz == List.class) {
                List l = (List) object;
                if (ValidateUtils.isEmptyList(l)) {
                    ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
                }
            } else {
                ExceptionHandler.throwClassException(StatusCode.COMM_CLASS_ERROR,
                        "unsupport class name in checkParameter:" + clz.getName());
            }
        }
    }

    public void checkDateParamsFormatValid(String... dates) throws ClientException, ServerException {
        for (String date : dates) {
            if (!date.matches("\\d{4}-\\d{2}-\\d{2}")) {
                ExceptionHandler.throwParameterException(StatusCode.COMM_TIME_FORMAT_ERROR);
            }
        }
    }

    public void checkDateRangeParamsValid(String startDate, String endDate) throws ClientException, ServerException {
        checkDateParamsFormatValid(startDate, endDate);
        if (startDate.compareTo(endDate) > 0) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
/*        if (DateUtils.daysBetween(startDate, endDate) > 60) {
            Tracker.add(ID, "date interval is too long");
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }*/
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public String getID() {
        return this.ID;
    }

    public static Set<String> getFIELDSET() {
        return FIELDSET;
    }

    public static void setFIELDSET(Set<String> fIELDSET) {
        FIELDSET = fIELDSET;
    }

    public static Set<String> getMETHODSET() {
        return METHODSET;
    }

    public static void setMETHODSET(Set<String> mETHODSET) {
        METHODSET = mETHODSET;
    }

    public Map<String, String[]> getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(Map<String, String[]> parameterMap) {
        this.parameterMap = parameterMap;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDoName() {
        return doName;
    }

    public void setDoName(String doName) {
        this.doName = doName;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDownloadflag() {
        return downloadflag;
    }

    public void setDownloadflag(int downloadflag) {
        this.downloadflag = downloadflag;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getMediumId() {
        return mediumId;
    }

    public void setMediumId(int mediumId) {
        this.mediumId = mediumId;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public int getLandpageId() {
        return landpageId;
    }

    public void setLandpageId(int landpageId) {
        this.landpageId = landpageId;
    }

    public int getDataPkgId() {
        return dataPkgId;
    }

    public void setDataPkgId(int dataPkgId) {
        this.dataPkgId = dataPkgId;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPlanIds() {
        return planIds;
    }

    public void setPlanIds(String planIds) {
        this.planIds = planIds;
    }

    public String getLandpageIds() {
        return landpageIds;
    }

    public void setLandpageIds(String landpageIds) {
        this.landpageIds = landpageIds;
    }

    public String getDataPkgIds() {
        return dataPkgIds;
    }

    public void setDataPkgIds(String dataPkgIds) {
        this.dataPkgIds = dataPkgIds;
    }

    public void setID(String iD) {
        ID = iD;
    }

    public String getShowNotPay() {
        return showNotPay;
    }

    public void setShowNotPay(String showNotPay) {
        this.showNotPay = showNotPay;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public boolean isIgnoreZeroCostPlan() {
        return ignoreZeroCostPlan;
    }

    public void setIgnoreZeroCostPlan(boolean ignoreZeroCostPlan) {
        this.ignoreZeroCostPlan = ignoreZeroCostPlan;
    }

    public boolean isIgnoreUnboundPlan() {
        return ignoreUnboundPlan;
    }

    public void setIgnoreUnboundPlan(boolean ignoreUnboundPlan) {
        this.ignoreUnboundPlan = ignoreUnboundPlan;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSubQuery() {
        return subQuery;
    }

    public void setSubQuery(String subQuery) {
        this.subQuery = subQuery;
    }
    
    public String getAccountAlias() {
        return accountAlias;
    }

    public void setAccountAlias(String accountAlias) {
        this.accountAlias = accountAlias;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(String mediumIds) {
        this.mediumIds = mediumIds;
    }

    public String getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(String accountIds) {
        this.accountIds = accountIds;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getIndicators() {
		return indicators;
	}

	public void setIndicators(String indicators) {
		this.indicators = indicators;
	}
    public boolean isIgnoreIsNullCostPlan() {
        return ignoreIsNullCostPlan;
    }

    public void setIgnoreIsNullCostPlan(boolean ignoreIsNullCostPlan) {
        this.ignoreIsNullCostPlan = ignoreIsNullCostPlan;
    }
}
