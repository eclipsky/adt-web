package com.dataeye.ad.assistor.context;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.Constant.CookieName;
import com.dataeye.ad.assistor.constant.Constant.PtLoginAccessStatus;
import com.dataeye.ad.assistor.constant.Constant.ServerCfg;
import com.dataeye.ad.assistor.exception.ClientException;
import com.dataeye.ad.assistor.exception.ServerException;
import com.dataeye.ad.assistor.module.company.constants.Constants.CompanyStatus;
import com.dataeye.ad.assistor.module.company.model.Company;
import com.dataeye.ad.assistor.module.company.service.CompanyService;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.AccountRole;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.UserStatus;
import com.dataeye.ad.assistor.module.systemaccount.model.PtLoginUser;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;
import com.dataeye.ad.assistor.module.systemaccount.service.AccountLoginInfoService;
import com.dataeye.ad.assistor.module.systemaccount.service.PtLoginService;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountService;
import com.dataeye.ad.assistor.privilege.DataPermissionHandler;
import com.dataeye.ad.assistor.privilege.PrivilegeHandler;
import com.dataeye.ad.assistor.util.CookieUtils;
import com.dataeye.ad.assistor.util.ServerUtils;

/**
 * Session容器
 * @author luzhuyou 2017/02/17
 */
public class SessionContainer {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(SessionContainer.class);
	
	
	public static String AUTHORIZED_SIGN = "dataeye2013";
	
	public static Map<String, HttpSession> tokenSessionMap = new HashMap<String, HttpSession>();
//	/**
//	 * 构建用户Session
//	 * @param context
//	 * @param account
//	 * @return
//	 * @throws ServerException
//	 */
//	public static synchronized boolean buildUserSession(DEContext context, SystemAccountVo account)
//			throws ServerException {
//		HttpSession session = context.getSession();
//		UserInSession userInSession = new UserInSession(account.getAccountId());
//		userInSession.setAccountName(account.getAccountName());;
//		userInSession.setCompanyId(account.getCompanyId());
//		userInSession.setAccountRole(account.getAccountRole());
//		userInSession.setToken(CookieUtils.getCookieValue(context.getRequest(), CookieName.TOKEN_NAME));
//		String menuPermission = account.getMenuPermission();
//		userInSession.setMenuPermission(menuPermission);
//		userInSession.setCompanyAccountUid(account.getCompanyAccountUid());
//		// 设置接口权限
////		Set<String> interfacePermissionSet = new HashSet<String>(); 
////		interfacePermissionSet.addAll(PrivilegeHandler.getInterfaceSetByMenuId(Constant.MenuId.PUBLIC));
////		interfacePermissionSet.addAll(PrivilegeHandler.getInterfaceSetByMenuId(Constant.MenuId.COMMON));
////		if(StringUtils.isNotBlank(menuPermission)) {
////			String[] menuIds = menuPermission.split(",");
////			for(String menuId : menuIds) {
////				Set<String> tmpSet = PrivilegeHandler.getInterfaceSetByMenuId(menuId);
////				if(tmpSet != null && !tmpSet.isEmpty()) {
////					interfacePermissionSet.addAll(tmpSet);
////				}
////			}
////		}
////		userInSession.setInterfacePermissionSet(interfacePermissionSet);
////		session.setAttribute(ServerCfg.SESSION_KEY_USER, userInSession);
//		
//		// 设置接口权限
//		Map<String, Integer> interfaceDataPermissionMap = new HashMap<String, Integer>(); 
//		interfaceDataPermissionMap.putAll(PrivilegeHandler.getInterfaceDataMapByMenuId(Constant.MenuId.PUBLIC));
//		interfaceDataPermissionMap.putAll(PrivilegeHandler.getInterfaceDataMapByMenuId(Constant.MenuId.COMMON));
//		if(StringUtils.isNotBlank(menuPermission)) {
//			String[] menuIds = menuPermission.split(",");
//			for(String menuId : menuIds) {
//				Map<String, Integer> tmpMap = PrivilegeHandler.getInterfaceDataMapByMenuId(menuId);
//				if(tmpMap != null && !tmpMap.isEmpty()) {
//					interfaceDataPermissionMap.putAll(tmpMap);
//				}
//			}
//		}
//		userInSession.setInterfaceDataPermissionMap(interfaceDataPermissionMap);
//		session.setAttribute(ServerCfg.SESSION_KEY_USER, userInSession);
//		
//		// 设置数据访问权限
//		Integer[] permissionMediumAccountIds = DataPermissionHandler.getPermissionMediumAccountIds(userInSession);
//    	Integer[] responsibleAccountPermissionMediumAccountIds = DataPermissionHandler.getOptimizerPermissionMediumAccountIds(userInSession);
//    	String[] permissionMediumAccountNames = DataPermissionHandler.getPermissionMediumAccountNames(userInSession);
//    	Integer[] permissionSystemAccountIds = DataPermissionHandler.getPermissionSystemAccountIds(userInSession);
//    	String[] permissionSystemAccounts = DataPermissionHandler.getPermissionSystemAccounts(userInSession);
//    	userInSession.setPermissionMediumAccountIds(permissionMediumAccountIds);
//    	userInSession.setPermissionMediumAccountNames(permissionMediumAccountNames);
//    	userInSession.setOptimizerPermissionMediumAccountIds(responsibleAccountPermissionMediumAccountIds);
//    	userInSession.setPermissionSystemAccountIds(permissionSystemAccountIds);
//    	userInSession.setPermissionSystemAccounts(permissionSystemAccounts);
//    	// 设置开发工程师被授权产品
//    	if(account.getAccountRole() == AccountRole.ENGINEER) {
//	    	userInSession.setAuthorizedProductMap(ApplicationContextContainer.getDevAuthorizedProduct(account.getCompanyId(), account.getAccountId()));
//    	}
//		return true;
//	}
	
	/**
	 * 基于Request构建用户Session
	 * @param context
	 * @param account
	 * @return
	 * @throws ServerException
	 */
	public static synchronized boolean buildUserSession(HttpServletRequest request, SystemAccountVo account)
			throws ServerException {
		
		UserInSession userInSession = new UserInSession(account.getAccountId());
		userInSession.setAccountName(account.getAccountName());;
		userInSession.setCompanyId(account.getCompanyId());
		userInSession.setAccountRole(account.getAccountRole());
		userInSession.setToken(CookieUtils.getCookieValue(request, CookieName.TOKEN_NAME));
		String menuPermission = account.getMenuPermission();
		userInSession.setMenuPermission(menuPermission);
		userInSession.setCompanyAccountUid(account.getCompanyAccountUid());
		
//		// 设置接口权限
//		Set<String> interfacePermissionSet = new HashSet<String>(); 
//		interfacePermissionSet.addAll(PrivilegeHandler.getInterfaceSetByMenuId(Constant.MenuId.PUBLIC));
//		interfacePermissionSet.addAll(PrivilegeHandler.getInterfaceSetByMenuId(Constant.MenuId.COMMON));
//		if(StringUtils.isNotBlank(menuPermission)) {
//			String[] menuIds = menuPermission.split(",");
//			for(String menuId : menuIds) {
//				Set<String> tmpSet = PrivilegeHandler.getInterfaceSetByMenuId(menuId);
//				if(tmpSet != null && !tmpSet.isEmpty()) {
//					interfacePermissionSet.addAll(tmpSet);
//				}
//			}
//		}
//		userInSession.setInterfacePermissionSet(interfacePermissionSet);
//		session.setAttribute(ServerCfg.SESSION_KEY_USER, userInSession);
		
		// 设置接口权限
		Map<String, Integer> interfaceDataPermissionMap = new HashMap<String, Integer>(); 
		interfaceDataPermissionMap.putAll(PrivilegeHandler.getInterfaceDataMapByMenuId(Constant.MenuId.PUBLIC));
		interfaceDataPermissionMap.putAll(PrivilegeHandler.getInterfaceDataMapByMenuId(Constant.MenuId.COMMON));
		if(StringUtils.isNotBlank(menuPermission)) {
			String[] menuIds = menuPermission.split(",");
			for(String menuId : menuIds) {
				Map<String, Integer> tmpMap = PrivilegeHandler.getInterfaceDataMapByMenuId(menuId);
				if(tmpMap != null && !tmpMap.isEmpty()) {
					interfaceDataPermissionMap.putAll(tmpMap);
				}
			}
		}
		userInSession.setInterfaceDataPermissionMap(interfaceDataPermissionMap);
		
		// 设置数据访问权限
		Integer[] permissionMediumAccountIds = DataPermissionHandler.getPermissionMediumAccountIds(userInSession);
    	Integer[] responsibleAccountPermissionMediumAccountIds = DataPermissionHandler.getOptimizerPermissionMediumAccountIds(userInSession);
    	String[] permissionMediumAccountNames = DataPermissionHandler.getPermissionMediumAccountNames(userInSession);
    	Integer[] permissionSystemAccountIds = DataPermissionHandler.getPermissionSystemAccountIds(userInSession);
    	String[] permissionSystemAccounts = DataPermissionHandler.getPermissionSystemAccounts(userInSession);
    	userInSession.setPermissionMediumAccountIds(permissionMediumAccountIds);
    	userInSession.setPermissionMediumAccountNames(permissionMediumAccountNames);
    	userInSession.setOptimizerPermissionMediumAccountIds(responsibleAccountPermissionMediumAccountIds);
    	userInSession.setPermissionSystemAccountIds(permissionSystemAccountIds);
    	userInSession.setPermissionSystemAccounts(permissionSystemAccounts);
    	userInSession.setPermissionMedium(ApplicationContextContainer.getPermissionMedium(account.getCompanyId(), account.getAccountName()));
    	userInSession.setPermissionResponsibleMedium(ApplicationContextContainer.getPermissionResponsibleMedium(account.getCompanyId(), account.getAccountName()));
    	// 设置开发工程师被授权产品
    	if(account.getAccountRole() == AccountRole.ENGINEER) {
	    	userInSession.setAuthorizedProductMap(ApplicationContextContainer.getDevAuthorizedProduct(account.getCompanyId(), account.getAccountId()));
    	}
    	
    	HttpSession session = request.getSession();
    	session.setAttribute(ServerCfg.SESSION_KEY_USER, userInSession);
    	Map<Integer, UserInSession> requestMap = userCacheMap.get(userInSession.getCompanyId());
    	if(requestMap == null) {
    		requestMap = new HashMap<Integer, UserInSession>();
    		userCacheMap.put(userInSession.getCompanyId(), requestMap);
    	}
    	requestMap.put(userInSession.getAccountId(), userInSession);
		return true;
	}
	
	public static Map<Integer, Map<Integer, UserInSession>> userCacheMap = new HashMap<Integer, Map<Integer, UserInSession>>();;
	
	/**
	 * 刷新HttpSession中的UserInSession
	 * @param request
	 * @return
	 */
	public static synchronized void refreshUserInSession(HttpServletRequest request) {
    	UserInSession user = SessionContainer.getCurrentUser(DEContextContainer.getContext(request).getSession());
    	if(user == null) {
    		return;
    	}
    	Map<Integer, UserInSession> requestMap = userCacheMap.get(user.getCompanyId());
    	if(requestMap == null) {
    		return;
    	}
    	SystemAccountService systemAccountService = (SystemAccountService) ApplicationContextContainer.getBean("systemAccountService");
    	for(Integer accountId : requestMap.keySet()) {
    		UserInSession userInSession = requestMap.get(accountId);
    		
    		SystemAccountVo vo = systemAccountService.get(userInSession.getAccountName());
    		if(vo != null) {
    			userInSession.setAccountRole(vo.getAccountRole());
    			String menuPermission = vo.getMenuPermission();
    			userInSession.setMenuPermission(menuPermission);
    			
    			// 设置接口权限
    			Map<String, Integer> interfaceDataPermissionMap = new HashMap<String, Integer>(); 
    			interfaceDataPermissionMap.putAll(PrivilegeHandler.getInterfaceDataMapByMenuId(Constant.MenuId.PUBLIC));
    			interfaceDataPermissionMap.putAll(PrivilegeHandler.getInterfaceDataMapByMenuId(Constant.MenuId.COMMON));
    			if(StringUtils.isNotBlank(menuPermission)) {
    				String[] menuIds = menuPermission.split(",");
    				for(String menuId : menuIds) {
    					Map<String, Integer> tmpMap = PrivilegeHandler.getInterfaceDataMapByMenuId(menuId);
    					if(tmpMap != null && !tmpMap.isEmpty()) {
    						interfaceDataPermissionMap.putAll(tmpMap);
    					}
    				}
    			}
    			userInSession.setInterfaceDataPermissionMap(interfaceDataPermissionMap);
    		}
    		
    		// 设置数据访问权限
    		Integer[] permissionMediumAccountIds = DataPermissionHandler.getPermissionMediumAccountIds(userInSession);
    		Integer[] responsibleAccountPermissionMediumAccountIds = DataPermissionHandler.getOptimizerPermissionMediumAccountIds(userInSession);
    		String[] permissionMediumAccountNames = DataPermissionHandler.getPermissionMediumAccountNames(userInSession);
    		Integer[] permissionSystemAccountIds = DataPermissionHandler.getPermissionSystemAccountIds(userInSession);
    		String[] permissionSystemAccounts = DataPermissionHandler.getPermissionSystemAccounts(userInSession);
    		userInSession.setPermissionMediumAccountIds(permissionMediumAccountIds);
    		userInSession.setPermissionMediumAccountNames(permissionMediumAccountNames);
    		userInSession.setOptimizerPermissionMediumAccountIds(responsibleAccountPermissionMediumAccountIds);
    		userInSession.setPermissionSystemAccountIds(permissionSystemAccountIds);
    		userInSession.setPermissionSystemAccounts(permissionSystemAccounts);
    		userInSession.setPermissionMedium(ApplicationContextContainer.getPermissionMedium(userInSession.getCompanyId(), userInSession.getAccountName()));
    		userInSession.setPermissionResponsibleMedium(ApplicationContextContainer.getPermissionResponsibleMedium(userInSession.getCompanyId(), userInSession.getAccountName()));
    		// 设置开发工程师被授权产品
    		if(userInSession.getAccountRole() == AccountRole.ENGINEER) {
    			userInSession.setAuthorizedProductMap(ApplicationContextContainer.getDevAuthorizedProduct(userInSession.getCompanyId(), userInSession.getAccountId()));
    		}
    		
//    		httpSession.setAttribute(ServerCfg.SESSION_KEY_USER, userInSession);
    	}
	}
	
	/**
	 * 
	 * <pre>
	 * 使当前session无效
	 *  @param request  
	 *  @author Ivan<br>
	 *  @date 2015年3月14日 下午5:15:48
	 * <br>
	 */
	public static void invalidate(HttpServletRequest request) {
		HttpSession session = request.getSession();
		UserInSession userInSession = SessionContainer.getCurrentUser(session);
		if(userInSession != null) {
			userCacheMap.remove(userInSession.getAccountId());
		}
		session.invalidate();
	}
	
	/**
	 * 
	 * <pre>
	 * 从session中取出用户信息
	 *  @param session
	 *  @return  
	 *  @author Ivan<br>
	 *  @date 2015年1月7日 下午3:49:40
	 * <br>
	 */
	public static UserInSession getCurrentUser(HttpSession session) {
		UserInSession user = getAttribute(session, ServerCfg.SESSION_KEY_USER, UserInSession.class);
		if(user != null) {
			Map<Integer, UserInSession> userCache = userCacheMap.get(user.getCompanyId());
			if(userCache != null) {
				return userCache.get(user.getAccountId());
			}
		}
		return null;
	}
	
	/**
	 * 
	 * <pre>
	 * 从session中取出用户信息(兼容旧登录页面)
	 *  @param session
	 *  @return  
	 *  @author Ivan<br>
	 *  @date 2015年1月7日 下午3:49:40
	 * <br>
	 */
	public static Map<String, Object> getCurrentUserCompatible(HttpSession session) {
		// user:UserInSession; status：0-正常，1-token失效，返回登录页；2-帐号未初始化；3-用户审核中；4-公司状态异常；5-子账号无权限开通ADT服务; 6-用户状态异常
		UserInSession userInSession = getAttribute(session, ServerCfg.SESSION_KEY_USER, UserInSession.class);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("user", userInSession);
		int status = userInSession == null ? PtLoginAccessStatus.TOKEN_NULL : PtLoginAccessStatus.NORMAL;
		result.put("status", status);
		return result;
	}
	
	/**
	 * 
	 * <pre>
	 * 从session中取出用户信息
	 *  @param session
	 *  @return  
	 *  @author Ivan<br>
	 *  @date 2015年1月7日 下午3:49:40
	 * <br>
	 */
	public static Map<String, Object> getCurrentUser(HttpServletRequest request) {
        logger.info("统一登录处理.");
        HttpSession session = request.getSession();
        UserInSession currentUser = getCurrentUser(session);
        String cookies = request.getHeader("Cookie");
        // user:UserInSession; status：0-正常，1-token失效，返回登录页；2-帐号未初始化；3-用户审核中；4-公司状态异常；5-子账号无权限开通ADT服务; 6-用户状态异常; 9-正常，显示提示信息
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("user", null);
        result.put("status", PtLoginAccessStatus.TOKEN_NULL);
        result.put("show_tips", false);
        
	    /**
	     * update by sam.xie at 2018.03.07
	     * 用于调试（签名通过就可以绕过登录直接访问），生产环境慎用，及时更新sign
	     * 1. 验证sign签名是否合法
	     * 2. 通过user获取账户信息
	     * 3. 构造session（获取权限）
	     */
	    String sign = request.getParameter("sign"); 
	    String authorizedSign = ConfigHandler.getProperty("authorized_sign", AUTHORIZED_SIGN);
	    if(authorizedSign.equals(sign)){
	        if(currentUser != null){
	            result.put("user", currentUser);
	            result.put("status", PtLoginAccessStatus.NORMAL);
	            return result;
	        }
	        String email = request.getParameter("email");
	        SystemAccountService systemAccountService = (SystemAccountService) ApplicationContextContainer.getBean("systemAccountService");
	        SystemAccountVo user = systemAccountService.get(email);
	        if(null == user){
	            logger.info("email not exists: {}", email);
	            return result;
	        }
	        SessionContainer.buildUserSession(request, user);
	        result.put("user", getCurrentUser(session));
	        result.put("status", PtLoginAccessStatus.NORMAL);
	        return result;
	    }
		
		if(StringUtils.isBlank(cookies)) {
			// 清理session，回到登录
			SessionContainer.clear(request);
			result.put("status", PtLoginAccessStatus.TOKEN_NULL);
			logger.error("Cookie为空，Session失效");
			return result;
		}
		// 校验token
		String token = getCookieValue(cookies, Constant.CookieName.TOKEN_NAME);
		if(null == token){
			// 清理session，回到登录
			SessionContainer.clear(request);
			result.put("status", PtLoginAccessStatus.TOKEN_NULL);
			logger.error("Token为空，Session失效");
			return result;
		}
		HttpSession existsSession = tokenSessionMap.get(token);
		if (existsSession == null) { // 首次获取token，与session建立关联，后面session失效，需要同时清理tokenSessionMap
			tokenSessionMap.put(token, session);
		} 
		if(currentUser != null && token.equals(currentUser.getToken())){
			result.put("user", currentUser);
			result.put("status", PtLoginAccessStatus.NORMAL);
			return result;
		}
		
		PtLoginService ptLoginService = (PtLoginService) ApplicationContextContainer.getBean("ptLoginService");
		CompanyService companyService = (CompanyService) ApplicationContextContainer.getBean("companyService");
		SystemAccountService systemAccountService = (SystemAccountService) ApplicationContextContainer.getBean("systemAccountService");
		AccountLoginInfoService accountLoginInfoService = (AccountLoginInfoService) ApplicationContextContainer.getBean("accountLoginInfoService");
		
		// 1. 根据token检查并获取统一登录用户信息
		PtLoginUser ptLoginUser = null;
		try {
			ptLoginUser = ptLoginService.checkAndGetPtLoginAccount(token);
		} catch (ClientException e) {
			e.printStackTrace();
			logger.error("获取统一登录用户信息异常.");
			result.put("status", PtLoginAccessStatus.TOKEN_NULL);
			return result;
		}
		
		// 获取并判断公司状态，只有正常状态才可登录 add by luzhuyou 2017.05.18
		String email = ptLoginUser.getUserID();
		SystemAccountVo user = systemAccountService.get(email);
		Company company = null;
		if(user != null) { 
			company = companyService.get(user.getCompanyId());
		} else if(ptLoginUser.getCreateID() == 0) {
			company = companyService.getByUid(ptLoginUser.getUID());
		} else {
			logger.error("子账号无权限开通ADT服务.");
			result.put("status", PtLoginAccessStatus.NO_PRIVILEGE);
			return result;
		} 

		if(company == null) {
			logger.error("您的帐号[{}]还未初始化，请确认是否需要进行初始化.", email);
			result.put("status", PtLoginAccessStatus.NOT_INIT);
			return result;
		} else {
			int companyStatus = company.getStatus(); 
			if(companyStatus == CompanyStatus.BIZ_AUDIT || companyStatus == CompanyStatus.PAUSE || companyStatus == CompanyStatus.OPE_AUDIT) {
				logger.error("[{}]用户审核中.", email);
				result.put("status", PtLoginAccessStatus.IS_AUDIT);
				return result;
			} else if(companyStatus == CompanyStatus.FROZEN || companyStatus == CompanyStatus.DISABLE) {
				logger.error("[{}]公司状态异常.", ptLoginUser.getCompanyName());
				result.put("status", PtLoginAccessStatus.COMPANY_ABNORMAL);
				return result;
			} else if(companyStatus != 0) {
				logger.error("[{}]用户状态异常.", ptLoginUser.getCompanyName());
				result.put("status", PtLoginAccessStatus.USER_ABNORMAL);
				return result;
			}
		}
		
		int userStatus = user.getStatus();
		try {
			// 验证用户状态
			if(userStatus != UserStatus.NORMAL) {
				logger.error("用户状态异常.UserStatus is : " + userStatus);
				result.put("status", PtLoginAccessStatus.USER_ABNORMAL);
				return result;
			}
			// 集中处理用户session的创建和token的设置
			boolean succes = SessionContainer.buildUserSession(request, user);
			if (!succes) {
				logger.error("用户Session构建失败.");
				result.put("status", PtLoginAccessStatus.USER_ABNORMAL);
				return result;
			}
		}catch(Exception e){ 
			e.printStackTrace();
			logger.error("用户Session构建失败.");
			result.put("status", PtLoginAccessStatus.USER_ABNORMAL);
			return result;
		}
		finally {
			// 记录登录记录到数据库
			if (user != null) {
				accountLoginInfoService.add(user.getCompanyId(), user.getAccountId(), ServerUtils.getIP(request), userStatus, null);
			}
		}
		
		// 进入到以下步骤则代表登录成功
		result.put("user", getCurrentUser(session));
		result.put("status", PtLoginAccessStatus.NORMAL);
		if(user.getLoginPromptTimes() > user.getAlreadyPromptTimes()) {// 登录提示次数大于已登录次数
			user.setAlreadyPromptTimes(user.getAlreadyPromptTimes()+1);
			// 将用户已登录次数加+1
			systemAccountService.modifyPromptTimes(user.getAccountId(), user.getAlreadyPromptTimes());
			result.put("show_tips", true);
		}
		return result;
	}
		
	/**
	 * 从Session里面取出属性并且转换成需要的对象类型 不存在或者转换失败返回null
	 * 
	 * @param request
	 * @param key
	 * @param clz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getAttribute(HttpSession session, String key, Class<T> clz) {
		if (session == null)
			return null;
		Object object = session.getAttribute(key);
		if (object != null) {
			try {
				return (T) object;
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}

	/**
	 * 
	 * <pre>
	 * 清空当前session
	 *  @param request  
	 *  @author Ivan<br>
	 *  @date 2015年3月14日 下午5:16:08
	 * <br>
	 */
	public static void clear(HttpServletRequest request) {
		HttpSession session = request.getSession();
		clearSession(session);
	}
	
	/**
	 * 
	 * <pre>
	 * 清空session
	 *  @param session  
	 *  @author Ivan<br>
	 *  @date 2015年3月14日 下午5:15:36
	 * <br>
	 */
	private static void clearSession(HttpSession session) {
		Enumeration<String> names = session.getAttributeNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			session.removeAttribute(name);
		}
	}
	
	public static String getCookieValue(String cookies, String field) {
		return getCookieMap(cookies).get(field);
	}

	public static Map<String, String> getCookieMap(String cookies) {
		Map<String, String> cookieMap = new HashMap<String, String>();
		String[] cookieArr = cookies.split(";");
		for (String cookie : cookieArr) {
			String[] kv = cookie.split("=");
			String key = kv[0].trim();
			String value = "";
			if (kv.length == 2) {
				value = kv[1].trim();
			}
			cookieMap.put(key, value);
		}
		return cookieMap;
	}
}
