package com.dataeye.ad.assistor.context;

import javax.servlet.http.HttpServletRequest;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.constant.Constant.ServerCfg;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.exception.ParameterException;

/**
 * Context上下文
 * @author luzhuyou 2017/02/18
 */
public class DEContextContainer {
	/**
	 * 
	 * <pre>
	 * 从HttpServletRequest中取出提前设置的DeContext属性，如果不存在返回null
	 * @param request
	 * @return  
	 * @author Ivan<br>
	 * @throws ParameterException 
	 * @throws ParamException 
	 * @date 2015年2月25日 下午2:33:48
	 * <br>
	 */
	public static final DEContext getContext(HttpServletRequest request) throws ParameterException {
		Object object = request.getAttribute(ServerCfg.DE_CONTEXT);
		if (object != null && object instanceof DEContext) {
			return (DEContext) object;
		}
		// 正常情况下不会走到这里
		ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
		return null;
	}
}
