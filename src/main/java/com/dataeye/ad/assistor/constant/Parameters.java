package com.dataeye.ad.assistor.constant;

/**
 * HTTP请求参数常量.
 * Created by huangzehai on 2017/4/27.
 */
public class Parameters {
    /**
     * DEContext常量名
     */
    public static final String CONTEXT = "CTX";
}
