package com.dataeye.ad.assistor.constant;

/**
 * Created by huangzehai on 2017/4/27.
 */
public final class Labels {

    /**
     * 未知值
     */
    public static final String UNKNOWN = "-";

    /**
     * 汇总
     */
    public static final String TOTAL = "汇总";

    /**
     * 其他.
     */
    public static final String OTHERS = "其他";

    /**
     * 其他ID.
     */
    public static final Integer OTHERS_ID = -1;

    /**
     * 到
     */
    public static final String TO = "~";

    /**
     * 自然流量
     */
    public static final String NATURAL_FLOWS = "自然流量";

    /**
     * 自然流量
     */
    public static final String NATURAL_FLOWS_SUFFIX = "(自然流量)";

    /**
     * 自然流量(所有媒体)
     */
    public static final String NATURAL_FLOW_OF_ALL_MEDIUMS = "自然流量(所有媒体)";

    private Labels() {

    }
}
