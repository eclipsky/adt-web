package com.dataeye.ad.assistor.constant;

public final class AccountRole {
    private AccountRole() {

    }

    /**
     * 企业账号
     */
    public static final int BUSINESS_ACCOUNT = 2;
}
