package com.dataeye.ad.assistor.constant;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;



/**
 * <pre>
 * 常量
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:33:31
 */
public class Constant {

	// 广告关联规则结束时间
	public static final int AD_PLAN_DEFAULT_END_TIME = 2000000002; // 2033-05-18 11:33:22

	/**
	 * <pre>
	 * server config
	 * @author Stran <br>
	 * @version 1.0 <br>
	 * @since 2016.09.30 16:33:18
	 */
	public static class ServerCfg {
		/* 默认语言 */
		public static final String DEFAULT_LANG = LANG.ZH;
		/* 允许的最小查询时间 */
		public static final int MIN_QUERY_START_TIME = 0;
		/* 查询开始时间和结束时间之间允许最大跨度多少天 */
		public static final int MAX_QUERY_TIME_SPAN_DAYS = 60;
		/* Content-Type Json */
		public static final String CONTENT_TYPE_JSON = "application/json; charset=utf-8";
		/* HTTP response content type */
		public static final String CONTENT_TYPE_EXCEL = "application/vnd.ms-excel;charset=utf-8";
		public static final String CONTENT_TYPE_STREAM = "application/octet-stream";
		/* HTTP response content encoding */
		public static final String ENCODING_UTF8 = "utf-8";
		/* 不合法的数字 */
		public static final int INVALID_NUMBER = -100;
		/** 缺省分隔符 */
		public static final String SEPARATOR_DEFAULT = ",";
		/** 下划线 */
		public static final String SEPARATOR_UNDERLINE = "_";
		/** 冒号 */
		public static final String SEPARATOR_COLON = ":";
		/** 问号 */
		public static final String SEPARATOR_QUESTION = "?";
		/** 竖线分隔符 */
		public static final String SEPARATOR_VERTICAL = "\\|";
		/** 向request里面设置属性保存DeContext的属性名 */
		public static final String DE_CONTEXT = "CTX";
		/** session attrbute DEUSER */
		public static final String SESSION_KEY_USER = "DEUSER";
		/** 表示统一登陆cookie域名 */
		public static final String OOS_COOKIE_DOMAIN = ".dataeye.com";
		/** 表示当前服务器 */
		public static  final String SERVER_DOMAIN = "adt.dataeye.com"; //TODO 修改
	}
	
	public class ServerCfg1 {
		/** parameter key download */
		public static final String PARAM_KEY_DOWNLOAD = "download";
		
		/** 不合法的数字表示 */
		public static final int INVALID_NUMBER = -100;
		/** dbConfig配置文件中 <companyID>中的值的前缀 */
		public static final String COMPANYID_PRE = "channel";
		/** 查询时允许的最小查询时间 */
		public static final int MIN_QUERY_START_TIME = 0;
		/** 最大的查询区间 */
		public static final int MAX_QUERY_TIME_SPAN_DAYS = 90;
		/** 顶级的父id */
		public static final int RESOURCE_TOP_PARENTID = -2;
		/** 资源位默认分类 */
		public static final int RESOURCE_DEFAULT_TYPE = -3;
		/** 所有的 */
		public static final int ALL = -1;
		/** 最大的下载的跨度（初始化用到） */
		public static final int MAX_DOWNLOAD_TIME_SPAN = 21;
		/** 主键冲突异常 */
		public static final String SQL_EXCEPTION_DUPLICATE_ENTRY = "Duplicate entry";
		/** demo 账号 */
		public static final String ACCOUNT_OF_DEMO = "demo@dataeye.com";
		/** 未知 */
		public static final String UNKNOWN = "UNKNOWN";
		/** 统一登陆服务器路径 */
		public static final String SSO_PATH = "http://dataeye.dataeye.com/ptlogin";
		/** 统一登陆登陆URL */
		public static final String LOGIN = SSO_PATH + "/login.jsp";

		public static final String CONTENT_TYPE_STREAM = "application/octet-stream";

	}

	/**
	 * <pre>
	 * url参数key
	 * @author Stran <br>
	 * @version 1.0 <br>
	 * @since 2016.09.30 16:33:10
	 */
	public class ParameterKey {
		/* 标志一个请求是下载请求 */
		public static final String DOWNLOAD = "download";

	}

	/**
	 * <pre>
	 * 分隔符
	 * @author Stran <br>
	 * @version 1.0 <br>
	 * @since 2016.09.30 16:32:58
	 */
	public class Separator {
		/* 默认分隔符 */
		public static final String DEFAULT = ",";
		public static final String REGX_VERTICAL_BAR = "\\|";
		public static final String COMMA = ",";
		public static final String BAR = "-";
		public static final String COLON = ":";
	}

	/*
	 * <pre>
	 * 
	 * @author Ivan <br>
	 * 
	 * @version 1.0 <br>
	 * 
	 * @date 2015年8月11日 下午6:01:46 <br>
	 */
	public class SessionName {
		/*
		 * session中表示当前用户
		 */
		public static final String CURRENT_USER = "CURRENT_USER";
	}

	/**
	 * <pre>
	 * 语言
	 * @author Stran <br>
	 * @version 1.0 <br>
	 * @since 2016.09.30 16:32:34
	 */
	public class LANG {
		/* 中文 */
		public static final String ZH = "zh";
		/* 英文 */
		public static final String EN = "en";
		/* 繁体 */
		public static final String FT = "ft";
	}

	/**
	 * <pre>
	 * 国际化相关的文件
	 * @author Stran <br>
	 * @version 1.0 <br>
	 * @since 2016.09.30 16:32:11
	 */
	public class Resource {
		public static final String DIR = "resources/i18n/";
		/* 英文 */
		public static final String EN_FILE = DIR + "en.properties";
		/* 中文 */
		public static final String ZH_FILE = DIR + "zh.properties";
		/* 繁体 */
		public static final String FT_FILE = DIR + "ft.properties";
	}

	/**
	 * <pre>
	 * 配置文件相关信息
	 * @author Stran <br>
	 * @version 1.0 <br>
	 * @since 2016.09.30 16:49:38
	 */
	public class Config {
		/* config directory */
		public static final String DIR = "resources/ext/";
		/* floating ad配置文件 */
		public static final String CONFIG_PROPERTIES = DIR + "config.properties";
	}

	/**
	 * <pre>
	 * 操作系统类型
	 * @author Stran <br>
	 * @version 1.0 <br>
	 * @since 2016.09.30 16:49:34
	 */
	public class OsType {
		/* windows */
		public static final String WINDOWS = "win";
		/* linux */
		public static final String LINUX = "linux";
		/* 其他 */
		public static final String OTHER = "other";
	}

	/**
	 * 常用的时间单位
	 * 
	 * <pre>
	 * @author Hayden<br>
	 * @date 2015年3月3日 下午5:56:06
	 * <br>
	 */
	public class Seconds {
		public static final int SECONDS_FIVE_MIN = 5 * 60;
		public static final int SECONDS_HALF_HOUR = 30 * 60;
		public static final int SECONDS_ONE_HOUR = 60 * 60;
		public static final int SECONDS_ONE_DAY = 24 * 60 * 60;
		public static final int SECONDS_ONE_WEEK = 7 * 24 * 60 * 60;
		public static final int SECONDS_ONE_MONTH = 30 * 24 * 60 * 60;
	}
	
	/**
	 * 一些公用的URL
	 * 
	 * <pre>
	 * @author Hayden<br>
	 * @date 2015年3月3日 下午6:08:01
	 * <br>
	 */
	public class CommonURL {
		// 主机域名
		public static final String URL = ServerCfg.SERVER_DOMAIN;
		// 统一登陆URL
		public static final String PT_LOGIN_URL = "https://www.dataeye.com/ptlogin/login.jsp";
		// CRM系统URL
		public static final String ADT_CRM_URL = "http://adt-crm.dataeye.com";
	}
	
	/**
	 * 通用MenuId
	 * 
	 * @author luzhuyou 2017/02/21
	 */
	public class MenuId {
		/** 公开  */
		public static final String PUBLIC = "public";
		/** 通用  */
		public static final String COMMON = "common";
	}
	
	/**
	 * 邮件类型
	 * <br>0-创建系统账号，1-密码重置，2-告警邮件
	 * @author luzhuyou 2017/02/24
	 */
	public class MailType {
		/** 0-创建系统账号  */
		public static final int CREATE_ACCOUNT = 0;
		/** 1-密码重置  */
		public static final int RESET_PWD = 1;
		/** 2-告警邮件  */
		public static final int ALARM_MAIL = 2;
		/** 3-商务审核通知邮件  */
		public static final int BIZ_AUDIT = 3;
	}
	
	/**
	 * 邮件发送状态
	 * <br>0-成功，1-失败
	 * @author luzhuyou 2017/02/24
	 */
	public class MailSendStatus {
		/** 0-成功  */
		public static final int SUCCESS = 0;
		/** 1-失败  */
		public static final int FAIL = 1;
	}
	
	/**
	 * 
	 * <pre>
	 * Cookie名字的字符串
	 * @author Ivan          <br>
	 * @date 2015年3月3日 下午6:51:36 <br>
	 * @version 1.0
	 * <br>
	 */
	public class CookieName {
		/** 客户端语言 */
		public static final String LANG = "lang";
		/** cookie里面存放token的cookiename */
		public static final String TOKEN_NAME = "tokenglobal";

	}
	
	/**
	 * <pre>
	 * 统一登录跳转ADT返回状态
	 * @author luzhuyou 2017/05/26
	 */
	public class PtLoginAccessStatus {
		/** 0-正常 */
		public static final int NORMAL = 0;
		/** 1-token失效，返回登录页 */
		public static final int TOKEN_NULL = 1;
		/** 2-帐号未初始化 */
		public static final int NOT_INIT = 2;
		/** 3-用户审核中 */
		public static final int IS_AUDIT = 3;
		/** 4-公司状态异常  */
		public static final int COMPANY_ABNORMAL = 4;
		/** 5-子账号无权限开通ADT服务 */
		public static final int NO_PRIVILEGE = 5;
		/** 6-用户状态异常*/
		public static final int USER_ABNORMAL = 6;
	}
	
	/**
	 * DataEye统一登录接入产品
	 * @author luzhuyou 2017/07/21
	 */
	public static class DeProduct {
		/** 1-ADT */
		public static final int ADT = 1;
		/** 2-Tracking */
		public static final int TRACKING = 2;
		
		private static Map<Integer, String> productMap = new HashMap<Integer, String>();
		
		public static String getName(int productId) {
			return productMap.get(productId);
		}
		static {
			productMap.put(ADT, "易投放(ADT)");
			productMap.put(TRACKING, "广告效果监测平台(Tracking)");
		}
		
		public static boolean isDeProduct(String deProduct) {
			if(StringUtils.isBlank(deProduct) || !StringUtils.isNumeric(deProduct)) {
				return false;
			}
			int deProductId = Integer.parseInt(deProduct.trim());
			if(deProductId == DeProduct.ADT || deProductId == DeProduct.TRACKING) {
				return true;
			}
			return false;
		}
	}
	
	/**
	 * 媒体类型
	 * @author luzhuyou 2017/08/10
	 */
	public static class MediumType {
		/** 1-ADT对接媒体(id范围：1-999) */
		public static final int ADT = 1;
		/** 2-Tracking通用媒体（id范围：1000-9999） */
		public static final int TRACKING_COMMON = 2;
		/** 3-自定义媒体（id范围：100000-） */
		public static final int CUSTOM = 3;
		
		/**
		 * 根据媒体id，得到媒体类型
		 * @author ldj 2017-08-10
		 */
		public static int getMediumType(int mediumId) {
			if(mediumId >= 1 && mediumId <= 999){
				return ADT;
			}else if(mediumId >= 1000 && mediumId <= 9999){
				return TRACKING_COMMON;
			}else if(mediumId >= 100000){
				return CUSTOM;
			}
			return 0;
		}
	}
	
	
}
