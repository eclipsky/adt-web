package com.dataeye.ad.assistor.constant;

/**
 * 远程接口定义常量
 * @author luzhuyou
 *
 */
public class RemoteInterfaceConstants {
	/**
	 * Tracking接口
	 * @author luzhuyou 2017/06/21
	 *
	 */
	public static class Tracking {
		/**
		 * 查询推广计划
		 */
		public static final String QUERY_CAMPAIGN_KEY = "query-campaign";
		public static final String QUERY_CAMPAIGN = "http://10.1.2.196:6940/adt/app/campaign/query";

		/**
		 * 查询渠道参数
		 */
		public static final String GET_CHANNEL_PARAMS_KEY = "get_channel_params";
		public static final String GET_CHANNEL_PARAMS = "http://10.1.2.196:6940/adt/app/campaign/get-channel-params";

		/**
		 * 预创建推广链接
		 */
		public static final String ADD_CAMPAIGN_BEFORE_KEY = "add_campaign_before";
		public static final String ADD_CAMPAIGN_BEFORE = "http://10.1.2.196:6940/adt/app/campaign/pre-create";
		
		/**
		 * 创建推广计划（广告活动）
		 */
		public static final String ADD_CAMPAIGN_KEY = "add_campaign";
		public static final String ADD_CAMPAIGN = "http://10.1.2.196:6940/adt/app/campaign/create";
		
		/**
		 * 修改推广计划（广告活动）
		 */
		public static final String MODIFY_CAMPAIGN_KEY = "modify_campaign";
		public static final String MODIFY_CAMPAIGN = "http://10.1.2.196:6940/adt/app/campaign/modify";
		
		/**
		 * 删除推广计划（广告活动）
		 */
		public static final String DELETE_CAMPAIGN_KEY = "delete_campaign";
		public static final String DELETE_CAMPAIGN = "http://10.1.2.196:6940/adt/app/campaign/delete";
		
		/**
		 * 监测地址（广告活动）
		 * */
		public static final String MONITOR_URL_KEY = "monitor_url";
		public static final String MONITOR_URL = "https://a.de0.cc/";
		
		/**
		 * 检测Tracking产品账号是否需要审核
		 * */
		public static final String USER_VERIFY_KEY = "user_verify";
		public static final String USER_VERIFY = "http://t2.dataeye.com/userVerify.do";
		
		/**
		 * 新增Tracking渠道
		 * */
		public static final String ADD_CHANNEL_KEY = "add_channel";
		public static final String ADD_CHANNEL = "http://10.1.2.196:6940/adt/channel/create";
	}
	
}
