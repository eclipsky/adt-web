package com.dataeye.ad.assistor.constant;

/**
 * 状态码常量类
 * <p>
 * 命名规范：<p>
 * <p>常量命名：以如下规则开头，以下划线作为每个单词之间的分隔；状态码内容命名：以如下规则开头，后接3位递增数字
 * <p>其中，特例状态码包括：成功，状态码为：200；未知异常，状态码为600.
 * <p>后续如再增加模块，状态码不占用20、60开头的状态码段.
 * <p>1.公共模块： 命名以COMM_开头，状态码以 10 开头；
 * <p>2.登录注册模块: 命名以LOGI_开头，状态码以 11 开头;
 * <p>3.系统账号模块：命名以ACCO_开头，状态码为 12 开头；
 * <p>4.关联关系模块：命名以ASSO_开头，状态码为 13 开头；
 * <p>5.转化表模块：命名以CONV_开头，状态码为 14 开头；
 * <p>6.统计报告模块：命名以REPO_开头，状态码为 15 开头；
 * <p>7.落地页模块：命名以LAND_开头，状态码为 16 开头；
 * <p>8.实时投放模块：命名以REAL_开头，状态码为 17 开头；
 * <p>9.投放日报模块：命名以DELI_开头，状态码为 18 开头；
 * <p>10.回本模块：命名以PAYB_开头，状态码为 19 开头；
 * <p>11.产品管理模块：命名以PROD_开头，状态码为 21 开头；
 * <p>12.媒体账号管理模块：命名以MEAC_开头，状态码为 22 开头；
 * <p>13.效果看板模块：命名以RS_开头，状态码为 23 开头；
 * <p>14.推广活动模块：命名以CAMP_开头，状态码为 24 开头;
 * <p>15.媒体管理：命名以MEDI_开头，状态码为 25 开头
 * <p>16.SDK接入测试：命名以DEBUG_开头，状态码为26开头
 * <p>17.推广计划（广告投放业务）：命名以ADPLAN_开头，状态码为27开头
 * <p>18.广告组（广告投放业务）：命名以ADVERTISING_开头，状态码为28开头
 * <p>19.广告创意（广告投放业务）：命名以ADCREATIVE_开头，状态码为29开头
 * <p>20.定向条件（广告投放业务）：命名以TARGETTAGS_开头,状态码为30开头
 *
 * @author: louis 2016.09.30
 */
public class StatusCode {

    /**
     * 200:请求正常处理
     */
    public static final int SUCCESS = 200;
    /**
     * 600:未知异常
     */
    public static final int UNKNOWN = 600;

	/* 
     * 1.公共模块： 命名以COMM_开头，状态码以 10 开头
	 */
    /**
     * 10000:正确请求但没有数据
     */
    public static final int COMM_SUCCESS_NO_DATA = 10000;
    /**
     * 10001:参数不足
     */
    public static final int COMM_PARAM_NOT_ENOUGH = 10001;
    /**
     * 10002:参数错误
     */
    public static final int COMM_PARAMETER_ERROR = 10002;
    /**
     * 10003:服务器未知异常
     */
    public static final int COMM_SEVER_ERROR = 10003;
    /**
     * 10004:数据库连接对象实例不存在
     */
    public static final int COMM_JDBCTEMPLATE_NULL = 10004;
    /**
     * 10005:sql错误
     */
    public static final int COMM_DB_SQL_ERROR = 10005;
    /**
     * 10006:服务器状态异常
     */
    public static final int COMM_SERVER_STATUS_ERROR = 10006;
    /**
     * 10007:类错误
     */
    public static final int COMM_CLASS_ERROR = 10007;
    /**
     * 10008:时间格式错误
     */
    public static final int COMM_TIME_FORMAT_ERROR = 10008;
    /**
     * 10009:文件未上传
     */
    public static final int COMM_FILE_NOT_UPLOAD = 10009;
    /**
     * 10010:文件不是TXT
     */
    public static final int COMM_NOT_TXT_FILE = 10010;
    /**
     * 10011:上传文件超过限制大小
     */
    public static final int COMM_FILE_UPLOAD_SIZE_OVER_LIMIT = 10011;
    /**
     * 10012:文件不存在
     */
    public static final int COMM_FILE_NOT_FOUND = 10012;
    /**
     * 10013:会话已过期
     */
    public static final int COMM_SESSION_EXPIRE = 10013;
    /**
     * 10014:起始日期不能为空
     */
    public static final int COMM_START_DATE_BLANK = 10014;
    /**
     * 10015:截止日期不能为空
     */
    public static final int COMM_END_DATE_BLANK = 10015;
    /**
     * 10016:已存在
     */
    public static final int COMM_EXISTS = 10016;
    /**
     * 10017:不存在
     */
    public static final int COMM_NOT_EXISTS = 10017;
    /**
     * 10018:数据库连接错误
     */
    public static final int COMM_DB_CONN_ERROR = 10018;
    /**
     * 10019:数据库初始化失败
     */
    public static final int COMM_DB_INIT_ERROR = 10019;
    /**
     * 10020:日期不能为空
     */
    public static final int COMM_DATE_BLANK = 10020;
    /**
     * 10021:非当前媒体账号负责人，无权限配置告警
     */
    public static final int COMM_NO_OPTIMIZER_PRIVILEGE = 10021;

    /**
     * 10022:API接口请求失败
     */
    public static final int COMM_API_INTERFACE_ERROR = 10022;
    /**
     * 10023:图片未上传
     */
    public static final int COMM_IMAGES_NOT_UPLOAD = 10023;

    /**
     * 10024:图片类型错误
     */
    public static final int COMM_IMAGES_TYPE_ERROR = 10024;
    /**
     * 10025:文件过大，请压缩或选择其他文件重新上传
     */
    public static final int COMM_FILE_SIZE_ERROR = 10025;
    /**
     * 10026:关键词文件未上传
     */
    public static final int COMM_KWFILE_NOT_UPLOAD = 10026;
    /**
     * 10022:接口请求令牌失效
     */
    public static final int COMM_API_INTERFACE_TOKEN_EXPIRED = 10027;

	/* 
     * 2.登录注册模块: 命名以LOGI_开头，状态码以 11 开头
	 */
    /**
     * 11000:登录态异常，需要重新登录
     */
    public static final int LOGI_RE_LOGIN = 11000;
    /**
     * 11001:用户名已存在
     */
    public static final int LOGI_USER_ID_EXISTS = 11001;
    /**
     * 11002:非法链接
     */
    public static final int LOGI_LINK_ILLEGAL = 11002;
    /**
     * 11003:用户不存在
     */
    public static final int LOGI_USERID_NOT_EXIST = 11003;
    /**
     * 11004:用户已激活(用于重复激活的提示)
     */
    public static final int LOGI_USER_ALREADY_ACTIVE = 11004;
    /**
     * 11005:用户名或者密码错误
     */
    public static final int LOGI_USERNAME_OR_PASSWORD_ERROR = 11005;
    /**
     * 11006:用户未激活
     */
    public static final int LOGI_STATUS_INACTIVE = 11006;
    /**
     * 11007:用户审核中
     */
    public static final int LOGI_STATUS_UNDER_CHECK = 11007;
    /**
     * 11008:激活邮件发送失败
     */
    public static final int LOGI_STATUS_MAIL_SEND_FAIL = 11008;
    /**
     * 11009:登录态超时(固定)
     */
    public static final int LOGI_LOGIN_EXPIRE = 11009;
    /**
     * 11010:旧密码错误
     */
    public static final int LOGI_PASSWORDOLD_WRONG = 11010;
    /**
     * 11011:无权限
     */
    public static final int LOGI_NO_PRIVILEGE = 11011;
    /**
     * 11012:用户状态异常
     */
    public static final int LOGI_STATUS_ERROR = 11012;
    /**
     * 11013:用户状态正常
     */
    public static final int LOGI_STATUS_NORMAL = 11013;
    /**
     * 11014:用户已激活
     */
    public static final int LOGI_STATUS_ACTIVE_SUCCESS = 11014;
    /**
     * 11015:邮件发送错误
     */
    public static final int LOGI_MAIL_SEND_FAIL = 11015;
    /**
     * 11016:公司状态异常
     */
    public static final int LOGI_COMPANY_STATUS_ERROR = 11016;
    /**
     * 11017:邮箱号在统一登录账号体系中不存在
     */
    public static final int LOGI_EMAIL_NOT_IN_PTLOGIN = 11017;

	/* 
     * 3.系统账号模块：命名以ACCO_开头，状态码为 12 开头
	 */
    /**
     * 12000:系统账号名称不允许为空
     */
    public static final int ACCO_ACCOUNT_NAME_IS_NULL = 12000;
    /**
     * 12001:系统账号名称已存在
     */
    public static final int ACCO_ACCOUNT_NAME_EXIST = 12001;
    /**
     * 12002:系统账号格式不正确，应为邮箱格式
     */
    public static final int ACCO_ACCOUNT_NAME_FORMAT_ERROR = 12002;
    /**
     * 12003:系统账号不存在
     */
    public static final int ACCO_ACCOUNT_ID_NOT_EXIST = 12003;
    /**
     * 12004:分组名不允许为空
     */
    public static final int ACCO_GROUP_NAME_IS_NULL = 12004;
    /**
     * 12005:组长账号不存在
     */
    public static final int ACCO_LEADER_ACCOUNT_NOT_EXIST = 12005;
    /**
     * 12006:分组名已存在
     */
    public static final int ACCO_GROUP_NAME_EXIST = 12006;
    /**
     * 12007:分组不存在
     */
    public static final int ACCO_GROUP_ID_NOT_EXIST = 12007;
    /**
     * 12008:不能删除当前系统登录账号
     */
    public static final int ACCO_ACCOUNT_DELETE_REFUSE = 12008;

    /**
     * 账户角色不能为空
     */
    public static final int ACCO_ACCOUNT_ROLE_IS_NULL = 12009;
    /**
     * 系统账号ID不能为空
     */
    public static final int ACCO_ACCOUNT_ID_IS_NULL = 12010;

	/* 
     * 4.关联关系模块：命名以ASSO_开头，状态码为 13 开头
	 */
    /**
     * 13000:计划或落地页或包已用于其他关联
     */
    public static final int ASSO_PLAN_PAGE_PKG_USED = 13000;
    /**
     * 13001:计划或落地页或包已用于其他关联且新开始日期在老开始日期之前
     */
    public static final int ASSO_PLAN_PAGE_PKG_USED_AND_START_ERROR = 13001;
    /**
     * 13002:落地页已被其他计划关联
     */
    public static final int ASSO_PKG_USED_ERROR = 13002;
    /**
     * 13003:投放包已被其他计划关联
     */
    public static final int ASSO_PAGE_USED_ERROR = 13003;
    /**
     * 13004:产品不允许为空
     */
    public static final int ASSO_PRODUCT_IS_NULL = 13004;
    /**
     * 13005:计划不允许为空
     */
    public static final int ASSO_PLAN_IS_NULL = 13005;
    /**
     * 13006:落地页不允许为空
     */
    public static final int ASSO_PAGE_IS_NULL = 13006;
    /**
     * 13007:包不允许为空
     */
    public static final int ASSO_PKG_IS_NULL = 13007;
    /**
     * 13008:统计日期不允许为空
     */
    public static final int ASSO_STAT_DATE_IS_NULL = 13008;
    /**
     * 13009:关联规则冲突
     */
    public static final int ASSO_CONFLICT = 13009;

	/* 
     * 8.实时投放模块：命名以REAL_开头，状态码为 17 开头
	 */
    /**
     * 17000:计划ID不能为空
     */
    public static final int REAL_PLAN_ID_MISSING = 17000;
    /**
     * 17001:告警配置不能为空
     */
    public static final int REAL_ALERT_CONF_MISSING = 17001;
    /**
     * 17002:告警配置格式不正确
     */
    public static final int REAL_ALERT_CONF_FORMAT_INCORRECT = 17002;

    /**
     * 账号余额邮件是否通知标志不能为空
     */
    public static final int REAL_BALANCE_EMAIL_ALERT_IS_MISSING = 17003;

    /**
     * 媒体ID不能为空.
     */
    public static final int REAL_MEDIUM_ID_MISSING = 17004;

    /**
     * 出价不能为空.
     */
    public static final int REAL_BID_MISSING = 17005;

    /**
     * 媒体计划ID不能为空.
     */
    public static final int REAL_MEDIUM_PLAN_ID_MISSING = 17006;

    /**
     * 计划开关不能为空.
     */
    public static final int REAL_PLAN_SWITCH_MISSING = 17007;
    /**
     * 出价策略不能为空.
     */
    public static final int REAL_BID_STRATEGY_MISSING = 17008;

    /**
     * 没有切换计划开关的权限
     */
    public static final int REAL_NO_PRIVILEGE_OF_PLAN_SWITCH = 17009;

    /**
     * 没有出价的权限.
     */
    public static final int REAL_NO_PRIVILEGE_OF_BID = 17010;

    /**
     * 出价阶段为空.
     */
    public static final int REAL_BID_PHASE_MISSING = 17011;

    /**
     * 分钟数为空
     */
    public static final int REAL_MINUTES_MISSING = 17012;

    /**
     * 媒体账号余额标红逻辑默认值错误
     */
    public static final int REAL_BALANCE_RED_NUM_IS_ERROR = 17013;

	/*
     * 9.投放日报模块：命名以DELI_开头，状态码为 18 开头
	 */
    /**
     * 18000:账号ID为空
     */
    public static final int DELI_ACCOUNT_ID_MISSING = 18000;
    /**
     * 排序字段不正确.
     */
    public static final int DELI_ORDER_BY_INVALID = 18001;
    /**
     * 排序方式不正确.
     */
    public static final int DELI_ORDER_INVALID = 18002;

    /**
     * 指标可见性不能为空
     */
    public static final int DELI_INDICATOR_VISIBILITY_MISSING = 18003;

    /**
     * 媒体数据不能为空
     */
    public static final int DELI_MEDIUM_DATA_MISSING = 18004;

    /**
     * 媒体数据部分属性缺失
     */
    public static final int DELI_MEDIUM_DATA_PROPERTY_MISSING = 18005;

    /**
     * 保存媒体数据失败
     */
    public static final int DELI_FAIL_TO_SAVE_MEDIUM_DATA = 18006;

    /**
     * 无权限编辑媒体数据.
     */
    public static final int DELI_NO_PERMISSION_EDIT_MEDIUM_DATA = 18007;

	/* 
	 * 10.回本模块：命名以PAYB_开头，状态码为 19 开头
	 */
	
	/*
	 * 11.产品管理模块：命名以PROD_开头，状态码为 21 开头
	 */
    /**
     * 21000:产品名称不允许为空
     */
    public static final int PROD_PRODUCT_NAME_IS_NULL = 21000;
    /**
     * 21001:CP分成率只能够在0和1之间，保留两位小数
     */
    public static final int PROD_CP_SHARE_RATE_RANGE_ERROR = 21001;
    /**
     * 21002:产品名称已存在
     */
    public static final int PROD_PRODUCT_NAME_EXISTS = 21002;
    /**
     * 21003:TrackingApp已被产品 [{0}] 关联，请先解绑.
     */
    public static final int PROD_APP_ALREADY_USED = 21003;
    /**
     * 21004:一级产品分类不允许为空
     */
    public static final int PROD_PARENT_CATEGORY_ID_IS_NULL = 21004;
    /**
     * 21005:二级产品分类不允许为空
     */
    public static final int PROD_CHILDREN_CATEGORY_ID_IS_NULL = 21005;
    /**
     * 21006:产品平台不允许为空
     */
    public static final int PROD_OS_TYPE_IS_NULL = 21006;
    /**
     * 21007:appId不允许为空
     */
    public static final int PROD_APP_ID_ERROR = 21007;
	
	/*
	 * 12.媒体账号管理模块：命名以MEAC_开头，状态码为 22 开头
	 */
    /**
     * 22000:媒体账号不允许为空
     */
    public static final int MEAC_MEDIUM_ACCOUNT_IS_NULL = 22000;
    /**
     * 22001:密码不允许为空
     */
    public static final int MEAC_PASSWORD_IS_NULL = 22001;
    /**
     * 21002:媒体折扣率只能够在0和1之间，保留两位小数
     */
    public static final int MEAC_DISCOUNT_RATE_RANGE_ERROR = 22002;
    /**
     * 21003:媒体账号已存在
     */
    public static final int PROD_MEDIUM_ACCOUNT_EXISTS = 22003;
    /**
     * 产品ID不能为空
     */
    public static final int MEAC_DISCOUNT_PRODUCT_ID_IS_NULL = 22004;
    /**
     * 媒体账号ID不能为空
     */
    public static final int MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL = 22005;

	/*
	 * 13.效果看板模块：命名以RS开头，状态码为 23 开头
	 */
    /**
     * 23000：指标不能为空.
     */
    public static final int RS_INDICATOR_BLANK = 23000;
    
    
    /*
	 * 14.推广活动模块：命名以CAMP_开头，状态码为 24 开头
	 */
    /**
     * 24000：appId不能为空
     */
    public static final int CAMP_APP_ID_IS_NULL = 24000;
    /**
     * 24001：短链活动号不能为空
     */
    public static final int CAMP_CAMPAIGN_ID_IS_NULL = 24001;
    /**
     * 24002：活动名称不能为空
     */
    public static final int CAMP_CAMPAIGN_NAME_IS_NULL = 24002;
    /**
     * 24003：媒体ID不能为空.
     */
    public static final int CAMP_MEDIUM_ID_IS_NULL = 24003;
    /**
     * 24004：平台（系统类型）不能为空.
     */
    public static final int CAMP_OS_TYPE_IS_ERROR = 24004;
    /**
     * 24005：找不到对应的Tracking媒体ID.
     */
    public static final int CAMP_NO_TRACKING_MEDIUM_ID = 24005;
    /**
     * 24006：下载地址不能为空.
     */
    public static final int CAMP_DOWNLOAD_URL_IS_NULL = 24006;
    /**
     * 24007：产品不能为空
     */
    public static final int CAMP_PRODUCT_ID_IS_NULL = 24007;
    /**
     * 24008：媒体不能为空或输入错误.
     */
    public static final int CAMP_MEDIUM_ID_IS_ERROR = 24008;
    /**
     * 24009：“是否上应用宝”不能为空或输入错误
     */
    public static final int CAMP_APP_TREASURE_IS_ERROR = 24009;
    /**
     * 24010：推广链接数量必须在1到100之间
     */
    public static final int CAMP_URL_COUNT_IS_ERROR = 24010;
    /**
     * 24011：推广链接不能为空.
     */
    public static final int CAMP_TRACK_URL_IS_NULL = 24011;
    /**
     * 24012：推广计划名称已存在
     */
    public static final int CAMP_CAMPAIGN_NAME_IS_EXISTS = 24012;
    /**
     * 24013：渠道id不能为空或输入错误
     */
    public static final int CAMP_CHANNEL_ID_IS_NULL = 24013;

    /**
     * 15.媒体管理：命名以MEDI_开头，状态码为 25 开头
     */
    /**
     * 25000：媒体已存在
     */
    public static final int MEDI_MEDIUM_IS_EXISTS = 25000;
    /**
     * 25001：媒体名称不允许为空
     */
    public static final int MEDI_MEDIUM_NAME_IS_EXISTS = 25001;

    /**
     * 16.SDK接入测试：命名以DEBUG_开头，状态码为26开头
     */
    /**
     * 26000：测试设备名称已存在
     */
    public static final int DEBUG_DEVICE_NAME_IS_EXISTS = 26000;
    /**
     * 26001：测试设备名称不允许为空
     */
    public static final int DEBUG_DEVICE_NAME_IS_NULL = 26001;
    /**
     * 26002：测试设备号不允许为空
     */
    public static final int DEBUG_DEVICE_IS_NULL = 26002;

    /**
     * 26003：测试设备的id不允许为空
     */
    public static final int DEBUG_DEVICE_ID_IS_NULL = 26003;

    //<p>17.推广计划（广告投放业务）：命名以ADPLAN_开头，状态码为27开头
    /**
     * 推广计划ID不允许为空
     */
    public static final int ADPLAN_GROUP_ID_IS_NULL = 27000;
    /**
     * 推广计划名称不允许为空
     */
    public static final int ADPLAN_GROUP_NAME_IS_NULL = 27001;
    /**
     * 日消耗限额不允许为空
     */
    public static final int ADPLAN_DAILY_BUDGET_IS_NULL = 27002;
    /**
     * 投放速度不允许为空
     */
    public static final int ADPLAN_SPEEDMODE_IS_NULL = 27003;
    /**
     * 推广计划名称名称已存在
     */
    public static final int ADPLAN_GROUP_NAME_EXISTS = 27004;


    //<p>18.广告组（广告投放业务）：命名以ADVERTISING_开头，状态码为28开头
    /**
     * 广告组id不允许为空
     */
    public static final int ADVERTISING_GROUP_ID_IS_NULL = 28000;
    /**
     * 广告组名称不允许为空
     */
    public static final int ADVERTISING_GROUP_NAME_IS_NULL = 28001;
    /**
     * 应用ID不允许为空
     */
    public static final int ADVERTISING_PRODUCT_REFS_ID_IS_NULL = 28002;

    /**
     * 定向ID不允许为空
     */
    public static final int ADVERTISING_TARGETING_ID_IS_NULL = 28003;

    /**
     * 出价方式不允许为空
     */
    public static final int ADVERTISING_COST_TYPE_IS_NULL = 28004;

    /**
     * 广告主帐号 id不允许为空
     */
    public static final int ADVERTISING_MACCOUNT_ID_IS_NULL = 28005;
    /**
     * API授权Token不允许为空
     */
    public static final int ADVERTISING_MACCESSTOKEN_ID_IS_NULL = 28006;
    /**
     * 广告组名称已存在
     */
    public static final int ADVERTISING_GROUP_NAME_EXISTS = 28007;

    //<p>19.广告创意（广告投放业务）：命名以ADCREATIVE_开头，状态码为29开头
    /**
     * 广告创意id不允许为空
     */
    public static final int ADCREATIVE_ID_IS_NULL = 29000;
    /**
     * 广告创意规格ID不允许为空
     */
    public static final int ADCREATIVE_TEMPLATE_ID_IS_NULL = 29001;
    /**
     * 广告创意名称不允许为空
     */
    public static final int ADCREATIVE_NAME_ID_IS_NULL = 29002;
    /**
     * 广告创意元素不允许为空
     */
    public static final int ADCREATIVE_ELEMENTS_IS_NULL = 29003;
    /**
     * 站点不允许为空
     */
    public static final int ADCREATIVE_SITESET_IS_NULL = 29004;
    /**
     * 视频ID不允许为空
     */
    public static final int ADCREATIVE_VIDEOS_ID_IS_NULL = 29005;

    //<p>20.定向条件（广告投放业务）：命名以TARGETTAGS_开头,状态码为30开头

    /**
     * 定向ID不允许为空
     */
    public static final int TARGETTAGS_ID_IS_NULL = 30000;

    /**
     * 定向名称不允许为空
     */
    public static final int TARGETTAGS_NAME_IS_NULL = 30001;

    /**
     * 定向条件-关键词定向-关键词不允许为空
     */
    public static final int TARGETTAGS_KEYWORD_WORD_IS_NULL = 30002;

    /**
     * 定向条件-地域定向-地点类型不允许为空
     */
    public static final int TARGETTAGS_GEOLOCATION_TYPE_IS_NULL = 30003;

    /**
     * 定向标签文件读取错误
     */
    public static final int TARGETTAGS_JSONFILE_READER_ERROR = 30004;

    /**
     * 详细定向条件-参数错误
     */
    public static final int TARGETTAGS_TARGETING_POST_ERROR = 30005;

    /**
     * 定向名称已存在
     */
    public static final int TARGETTAGS_TARGETING_NAME_EXISTS = 30006;
    /**
     * 关键词数量过多,无法添加,最大值为500
     */
    public static final int TARGETTAGS_TARGETING_KWFILE_TOO_LONG = 30007;
    /**
     * 关键词文件编码无法识别
     */
    public static final int TARGETTAGS_TARGETING_KWFILE_ENCODE_UNRECOGNIZED = 30008;

    /**
     * 监测到过长的关键词,无法添加,最大长度为10字符
     */
    public static final int TARGETTAGS_TARGETING_KWFILE_WORD_TOO_LONG = 30009;

}
