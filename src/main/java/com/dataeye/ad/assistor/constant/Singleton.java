package com.dataeye.ad.assistor.constant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * <pre>
 * 缓存一些常用到的对象，用的时候就不用再new了
 * @author Ivan          <br>
 * @date 2014年12月18日 下午5:39:14
 * <br>
 *
 */
public class Singleton {
	/** 把对象转为json字符串的工具,这个是只有添加了 @Expose 注解的 才会被添加到 json字符串中 */
	public static final Gson GSON = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	/** 把对象转为json字符串的工具,所有都会转化为json字符串 */
	public static final Gson GSON_ALL = new Gson();
	/** yyyy-MM-dd HH:mm:ss */
	public static final SimpleDateFormat SDF_YYYY_MM_DD_HH_MM_SS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/** HH */
	public static final SimpleDateFormat SDF_HH = new SimpleDateFormat("HH");
	/** mm */
	public static final SimpleDateFormat SDF_MM = new SimpleDateFormat("mm");
	/** ss */
	public static final SimpleDateFormat SDF_SS = new SimpleDateFormat("ss");
	/** yyyy-MM-dd */
	public static final SimpleDateFormat SDF_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
	/** 随机数 */
	public static final Random RANDOM = new Random();
	/** empty list */
	public static final List<String> EMPTY_LIST_STRING = new ArrayList<String>(0);
	/** empty set */
	public static final Set<String> EMPTY_SET_STRING = new HashSet<String>(0);
	/** 定时任务线程池 */
	public static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(4);
}
