package com.dataeye.ad.assistor.constant;

import com.dataeye.ad.assistor.common.CachedObjects;

import java.util.List;

/**
 * <pre>
 * Tracking 请求完的响应对象
 * @author luzhuyou 2017/03/22
 */
public class TrackingResponse<T> {
	/** 标识请求的唯一ID: 时间戳+顺序号 */
	private String ID;
	/** 状态码:  200-成功；201-参数错误...（如果有其他异常，则累加）*/
	private int statusCode;
	/** 正文: 将返回的JSON数据保存在该字段内 */
	private List<T> content;

	/**
	 * 把当前对象转换为json格式
	 *  @return  
	 *  @author luzhuyou 2017/03/13
	 */
	public String toJson() {
		return CachedObjects.GSON_ONLY_EXPOSE.toJson(this);
	}

	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "TrackingResponse [id=" + ID + ", statusCode=" + statusCode
				+ ", content=" + content + "]";
	}

}
