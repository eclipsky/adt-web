package com.dataeye.ad.assistor.constant;

/**
 * <pre>
 * 数据库相关常量
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:34:55
 */
public class Database {

	public class Columns {
		public static final String PLAN_ID = "planID";
		public static final String CHANNEL_ID = "channelID";
		public static final String LAND_PAGE_ID = "landpageID";
	}

	/**
	 * <pre>
	 * 数据库和表
	 * @author Stran <br>
	 * @version 1.0 <br>
	 * @since 2016.09.30 16:35:12
	 */
	public class Table {
		// MySQL
		public static final String DC_AD_LAND_PAGE = "dc_ad_land_page ";
		public static final String DC_AD_PLAN = "dc_ad_plan ";
		public static final String DC_AD_REPORT_DAY = "dc_ad_report_day ";
		public static final String DC_AD_CONVERSION = "dc_ad_conversion ";
		public static final String DC_AD_LTV = "dc_ad_ltv ";
		public static final String DC_AD_PAYBACK = "dc_ad_payback ";
		public static final String DC_AD_MEDIUM = "dc_ad_medium ";
		
		public static final String DC_AD_ASSOCIATION = "dc_ad_association ";
		public static final String DC_AD_ASSOCIATION_RANGE = "dc_ad_association_range";
		public static final String DC_AD_LAND_URL = "dc_ad_land_url ";
		public static final String AD_PACKAGE = "ad_package";
	}

	public static class DBField {
		public static final String ID = "id";
		public static final String MEDIUM = "medium";
		public static final String PLAN_ID = "planID";
		public static final String PLAN_NAME = "planName";
		public static final String CHANNEL_ID = "channelID";
		public static final String CHANNEL_NAME = "channelName";
		public static final String PACKAGE_ID = "package_id";
		public static final String PACKAGE_NAME = "package_name";
		public static final String TOTAL_PAY = "totalPay";
		public static final String EXPOSURE_NUM = "exposureNum";
		public static final String CLICK_NUM = "clickNum";
		public static final String CLICK_RATE = "clickRate";
		public static final String CPC = "cpc";
		public static final String CREATE_TIME = "createTime";
		public static final String NAME = "name";
		public static final String LANDPAGE_ID = "landpageID";
		public static final String URL_ID = "urlID";
		public static final String PAGE_URL = "pageUrl";
		public static final String PV = "pv";
		public static final String UV = "uv";
		public static final String ACTIVE_NUM = "activeNum";
		public static final String REG_NUM = "regNum";
		public static final String NEW_PAY_NUM = "newPayNum";
		public static final String NEW_PAY_AMOUNT = "newPayAmount";
		public static final String NEW_PAY_RATE = "newPayRate";
		public static final String DAU = "dau";
		public static final String TOTAL_PAY_RATE = "totalPayRate";
		public static final String NEW_PLAYER_NUM = "newPlayerNum";
		public static final String FIRST_DAY = "firstDay";
		public static final String FIRST_WEEK = "firstWeek";
		public static final String NEXT_WEEK = "nextWeek";
		public static final String THREE_WEEK = "threeWeek";
		public static final String FOUR_WEEK = "fourWeek";
		public static final String FIVE_WEEK = "fiveWeek";
		public static final String SIX_WEEK = "sixWeek";
		public static final String SEVEN_WEEK = "sevenWeek";
		public static final String EIGHT_WEEK = "eightWeek";
		public static final String NINE_WEEK = "nineWeek";
		public static final String TEN_WEEK = "tenWeek";
		public static final String ELEVEN_WEEK = "elevenWeek";
		public static final String TWELVE_WEEK = "twelveWeek";
		public static final String FOUR_MON = "fourMon";
		public static final String FIVE_MON = "fiveMon";
		public static final String SIX_MON = "sixMon";
		public static final String CONSUME = "consume";
		public static final String REG_COST = "regCost";
		public static final String PAYBACK_RATE = "paybackRate";
	}

}
