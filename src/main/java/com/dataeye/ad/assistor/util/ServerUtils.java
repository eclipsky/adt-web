package com.dataeye.ad.assistor.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dataeye.ad.assistor.common.CachedObjects;
import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.HttpStatusCode;
import com.xunlei.util.StringTools;

/**
 * <pre>
 * 常用的工具类
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:59:33
 */
public class ServerUtils {

    /**
     * 生成随机三位数字
     *
     * @return int
     * @author Stran
     * @since 2016.09.30 16:59:39
     */
    public static final int random3number() {
        return CachedObjects.RANDOM.nextInt(900) + 100;
    }

    /**
     * 获取16位 Request ID
     *
     * @return string
     * @author Stran
     * @since 2016.09.30 16:59:43
     */
    public static String getResponseID() {
        long winTimestamp = DateUtils.winTimestamp();
        int random = random3number();
        return winTimestamp + "" + random;
    }

    /**
     * 获取请求的接口名或者jsp名
     *
     * @param request
     * @return string
     * @author Stran
     * @since 2016.09.30 16:59:48
     */
    public static String getResourceName(HttpServletRequest request) {
        String requestUri = request.getRequestURI();
        return requestUri;
//        return getBasename(requestUri);
    }

    /**
     * 取出基础路径名/a/b/c/d则取出最后一个d
     *
     * @param uri
     * @return string
     * @author Stran
     * @since 2016.09.30 16:59:53
     */
    public static String getBasename(String uri) {
        int index = uri.lastIndexOf("/");
        if (index > -1) {
            return uri.substring(index + 1);
        }
        return null;
    }

    /**
     * 用UTF8 解码
     *
     * @param message
     * @return string
     * @author Stran
     * @since 2016.09.30 16:59:59
     */
    public static final String decodeWithUTF8(String message) {
        if (ValidateUtils.isNotEmpty(message)) {
            try {
                // 如果value中有%，并且%后面不是两个HEX字符，会解析异常，出异常则返回空字符串
                return URLDecoder.decode(message, "UTF8");
            } catch (Exception e) {
                System.out.println("decode message with UTF8 failed, message=" + message);
            }
        }
        return "";
    }

    /**
     * 首字母大写
     *
     * @param word
     * @return string
     * @author Stran
     * @since 2016.09.30 17:00:04
     */
    public static String toUppercaseFirstLetter(String word) {
        if (ValidateUtils.isNotEmpty(word)) {
            return word.substring(0, 1).toUpperCase() + word.substring(1);
        }
        return word;
    }

    /**
     * 获取操作系统类型(非常简单的判断，不严谨)
     *
     * @return string
     * @author Stran
     * @since 2016.09.30 17:00:10
     */
    public static String getOsType() {
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("win")) {
            return Constant.OsType.WINDOWS;
        } else if (osName.contains("linux")) {
            return Constant.OsType.LINUX;
        } else {
            return Constant.OsType.OTHER;
        }
    }

    /**
     * 把接口的处理结果返回给客户端
     *
     * @param response
     * @param httpStatusCode
     * @param body
     * @author Stran
     * @since 2016.09.30 17:00:19
     */
    public static void writeToResponse(HttpServletResponse response, HttpStatusCode httpStatusCode, String body) {
        response.setContentType(Constant.ServerCfg.CONTENT_TYPE_JSON);
        response.setStatus(httpStatusCode.getCode());
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            writer.write(body);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

	
	/**
	 * 获取IP
	 * 
	 * @param request
	 * @return
	 */
	public static String getIP(HttpServletRequest request) {
		// 先检查代理
		// String ip = request.getHeader("X-Real-IP");
		String ip = request.getHeader("X-Forwarded-For");
		System.out.println("X-Forwarded-For:" + ip);
		if (StringTools.isEmpty(ip)) {
			ip = request.getRemoteAddr();
			System.out.println("RemoteAddr:" + ip);
		} else {
			String[] ips = ip.split(",");
			for (int i = 0, j = ips.length; i < j; i++) {
				if ("unknown".equals(ips[i]) && i < j - 1) {
					continue;
				} else {
					ip = ips[i];
					break;
				}
			}
		}
		return ip;
	}
	
	/**
	 * 
	 * <pre>
	 * 从requestUri中得到do
	 *  @param requestUri
	 *  @return  
	 *  @author Ivan<br>
	 *  @date 2015年1月7日 下午1:52:00
	 * <br>
	 */
	public static String getDoFromUri(String requestUri) {
		// /test/test.do
		int index = requestUri.lastIndexOf("/");
		if (index > -1) {
			return requestUri.substring(index + 1);
		}
		return null;
	}
	
    public static void main(String[] args) throws ParseException {
//		System.out.println("C4D2F682C8F7F0B862BEB2784C8823560".hashCode());

        String date = "20160121";

        DateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date d = sdf.parse(date);
        String formatDate = sdf3.format(d);
        // 日期转换
        System.out.println(formatDate);

    }
}
