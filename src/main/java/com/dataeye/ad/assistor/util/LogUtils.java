package com.dataeye.ad.assistor.util;

import com.dataeye.util.log.DELogger;
import com.xunlei.jdbc.util.DebugUtil;

import org.slf4j.Logger;

/**
 * <pre>
 * 日志工具类
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:57:22
 */
public class LogUtils {

    /**
     * Tracker日志
     */
    private static Logger logTrack = DELogger.getLogger("tracker");
    /**
     * 权限日志
     */
    private static Logger logPriv = DELogger.getLogger("priv");
    /**
     * 请求日志
     */
    private static Logger logRequest = DELogger.getLogger("request");
    /**
     * SQL
     */
    private static Logger logSql = DELogger.getLogger("sql");

    /**
     * 记录跟踪日志
     *
     * @param message
     * @author Stran
     * @since 2016.09.30 16:57:27
     */
    public static void logTracker(String message) {
        logTrack.info(message);
    }

    /**
     * 记录权限过滤结果
     *
     * @param ID
     * @param doName
     * @param type
     * @param result
     * @author Stran
     * @since 2016.09.30 16:57:31
     */
    public static void logPriv(String ID, String doName, String type, String result) {
        logPriv.info("{}->{}->{}->{}", ID, doName, type, result);
    }

    public static void warnLogPriv(String ID, String doName, String type, String result) {
        logPriv.warn("{}->{}->{}->{}", ID, doName, type, result);
    }
    
    /**
	 * 
	 * <pre>
	 * 记录Sql以及执行结果(带填充的) 
	 * @author Ivan<br>
	 * @date 2014年12月30日 下午4:18:53
	 * <br>
	 */
	public static void logSql(String desc, String sql, Object... args) {
		logSql.error("{}->{}", desc, DebugUtil.merge(sql, args));
	}

    /**
     * 记录请求日志
     *
     * @param ID
     * @param doName
     * @param classAndMethod
     * @param url
     * @param responseJson
     * @param cost
     * @author Stran
     * @since 2016.09.30 16:57:36
     */
    public static void logRequest(String ID, String doName, String classAndMethod, String url, String responseJson,
                                  long cost) {
        logRequest.info("{}->{}->{}->{}->{}->{}->{}", ID, DateUtils.now(), doName, classAndMethod, url, responseJson,
                cost);
    }

    /**
     * sql日志
     *
     * @param doName  接口
     * @param fullSql sql
     */
    public static void logSql(String doName, String fullSql) {
        logSql.info("{}->{}", doName, fullSql);
    }

    /**
     * sql日志
     *
     * @param fullSql
     * @author Stran
     * @since 2016.09.26 14:44:36
     */
    public static void logSql(String fullSql) {
        logSql.info(fullSql);
    }

	public static Logger getLogTrack() {
		return logTrack;
	}

	public static Logger getLogPriv() {
		return logPriv;
	}

	public static Logger getLogRequest() {
		return logRequest;
	}

	public static Logger getLogSql() {
		return logSql;
	}

}
