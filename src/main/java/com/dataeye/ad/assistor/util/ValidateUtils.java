package com.dataeye.ad.assistor.util;


import com.dataeye.ad.assistor.constant.Constant;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * 与验证相关的工具类
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 10:04:52
 */
public class ValidateUtils {

    /**
     * 判断是不是null
     *
     * @param object
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:04:40
     */
    public static boolean isNull(Object object) {
        return object == null;
    }

    /**
     * 判断不是null
     *
     * @param object
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:04:37
     */
    public static boolean isNotNull(Object object) {
        return !isNull(object);
    }

    /**
     * 验证语言是否合法
     *
     * @param lang
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:04:32
     */
    public static boolean isValidLang(String lang) {
        if (lang != null && (Constant.LANG.EN.equals(lang) || Constant.LANG.ZH.equals(lang) || Constant.LANG.FT.equals(lang))) {
            return true;
        }
        return false;
    }

    /**
     * 是否是有效数字int
     *
     * @param number
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:04:29
     */
    public static boolean isValidNumber(int number) {
        if (number == Constant.ServerCfg.INVALID_NUMBER) {
            return false;
        }
        return true;
    }

    /**
     * 是否是有效数字long
     *
     * @param number
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:04:24
     */
    public static boolean isValidNumber(long number) {
        if (number == Constant.ServerCfg.INVALID_NUMBER) {
            return false;
        }
        return true;
    }

    /**
     * <pre>
     *
     * @param str
     * @return
     * @author Ivan<br>
     * @date 2015年3月31日 下午8:19:54
     * <br>
     */
    private static boolean isBlank(CharSequence str) {
        if (str == null) {
            return true;
        }
        int len = str.length();
        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            if (c > ' ') {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断给定参数列表中是否存在空(null和任意多个空格都认为是空)
     *
     * @param args
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:04:12
     */
    public static boolean isEmpty(CharSequence... args) {
        if (args == null) {
            return true;
        }
        for (CharSequence arg : args) {
            if (isBlank(arg)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断给定参数列表中是否都不空(null和任意多个空格都认为是空)
     *
     * @param args
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:04:08
     */
    public static boolean isNotEmpty(CharSequence... args) {
        return !isEmpty(args);
    }

    /**
     * 是否是空map
     *
     * @param map
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:04:04
     */
    public static boolean isEmptyMap(Map<?, ?> map) {
        if (map == null || map.size() == 0)
            return true;
        return false;
    }

    /**
     * map不为空
     *
     * @param map
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:04:00
     */
    public static boolean isNotEmptyMap(Map<?, ?> map) {
        return !isEmptyMap(map);
    }

    /**
     * 是否是空数组
     *
     * @param <T>
     * @param arr
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:03:49
     */
    public static <T> boolean isEmptyArray(T[] arr) {
        if (arr == null || arr.length == 0) {
            return true;
        }
        return false;
    }

    /**
     * 是否是空List
     *
     * @param list
     * @return boolean
     * @author Stran
     * @since 2016.09.30 10:03:44
     */
    public static boolean isEmptyList(List<?> list) {
        if (list == null || list.size() == 0) {
            return true;
        }
        return false;
    }

    /**
     * <pre>
     * 是否是URL
     *  @param url
     *  @return
     *  @author Stran<br>
     *  @date 2015年12月7日 下午15:51:57
     * <br>
     */
    public static boolean isUrl(String url) {
        if (url.matches("^https?://.+")) {
            return true;
        }
        return false;
    }
}
