package com.dataeye.ad.assistor.util;

import com.qq.jutil.string.StringUtil;

import java.util.*;

/**
 * <pre>
 * The type Collection utils.
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.11.30 11:30:48
 */
public class CollectionUtils {

    /**
     * 获取map中部分key对应的value list
     *
     * @param map
     * @param keys
     * @return list
     * @author Stran
     * @since 2016.06.08 15:55:59
     */
    public static List<Map<String, Object>> getValueList(Map<String, Map<String, Object>> map,
                                                         Collection<String> keys) {
        if (map.isEmpty()) {
            return Collections.emptyList();
        }

        List<Map<String, Object>> values = new ArrayList<>();
        for (String key : keys) {
            values.add(map.get(key));
        }

        return values;
    }

    public static Set<Integer> getIntSetFromIntCommaStr(String intCommaStr) {
        Set<Integer> set = Collections.emptySet();

        if (StringUtil.isNotEmpty(intCommaStr)) {
            set = new HashSet<>();
            try {
                for (String intStr : intCommaStr.split(",")) {
                    set.add(Integer.parseInt(intStr.trim()));
                }
            } catch (Exception e) {

            }
        }

        return set;
    }

    /**
     * 判断集合是否为空
     *
     * @param collection
     * @return boolean
     * @author Stran
     * @since 2016.09.30 17:00:32
     */
    public static boolean isEmptyCollection(Collection<?> collection) {
        if (collection == null || collection.size() == 0)
            return true;
        return false;
    }

    /**
     * 判断集合不空
     *
     * @param collection
     * @return boolean
     * @author Stran
     * @since 2016.09.30 17:00:37
     */
    public static boolean isNotEmptyCollection(Collection<?> collection) {
        return !isEmptyCollection(collection);
    }


    /**
     * 把String[]放入List<String>
     *
     * @param valueArr
     * @param valueList
     * @author Stran
     * @since 2016.09.30 17:00:14
     */
    public static void putStringArr2List(String[] valueArr, List<String> valueList) {
        if (ValidateUtils.isEmptyArray(valueArr) || valueList == null) {
            return;
        }
        for (String str : valueArr) {
            valueList.add(str);
        }
    }
}
