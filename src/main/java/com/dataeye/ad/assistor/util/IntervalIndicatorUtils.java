package com.dataeye.ad.assistor.util;

import com.dataeye.ad.assistor.module.realtimedelivery.model.*;

import java.util.*;

/**
 * 时段指标工具类.
 * Created by huangzehai on 2017/6/30.
 */
public final class IntervalIndicatorUtils {
    private IntervalIndicatorUtils() {

    }

    /**
     * 联结指定时段的计划、落地页、包统计数据,返回计划的完整时段统计报告
     *
     * @param planStats
     * @param pageStats
     * @param trackingStats
     * @param packageStats
     * @param period        时段
     * @return {Plan ID -> RealTimeReports }
     */
    public static Map<Integer, List<RealTimeReport>> join(List<RealTimeReport> planStats, List<PageStat> pageStats, List<TrackingStat> trackingStats, List<PackageStat> packageStats, Period period) {
        //分别根据plan id,page id, package id分组, 以便后面联结。
        return join(groupByPlanId(planStats), groupByPageId(pageStats), groupTrackingByPageId(trackingStats), groupByPackageId(packageStats), period);
    }

    /**
     * 联结指定时段的计划、落地页、CP指标
     *
     * @param planStats
     * @param pageStats
     * @param packageStats
     * @param period
     * @return
     */
    private static Map<Integer, List<RealTimeReport>> join(Map<Integer, List<RealTimeReport>> planStats, Map<Integer, List<PageStat>> pageStats, Map<Integer, List<TrackingStat>> trackingStats, Map<Integer, List<PackageStat>> packageStats, Period period) {
        //获取整点统计
        //{planId->RealTimeReport}
        Map<Integer, List<RealTimeReport>> planReports = getPeriodReportsByTenMinuteReport(planStats, period);
        //{pageId->PageStat}
        Map<Integer, List<PageStat>> pageReports = getPeriodReportsByTenMinuteReport(pageStats, period);
        //{page id -> Tracking Stat}
        Map<Integer, List<TrackingStat>> trackingReports = getPeriodReportsByTenMinuteReport(trackingStats, period);
        //{pkgId->PackageStat}
        Map<Integer, List<PackageStat>> packageReports = getPeriodReportsByTenMinuteReport(packageStats, period);

        //联结计划、落地页、包统计数据.
        for (Map.Entry<Integer, List<RealTimeReport>> planReport : planReports.entrySet()) {
            List<RealTimeReport> reportsOfPlan = planReport.getValue();
            //获取报告的落地页ID和包ID
            Integer pageId = reportsOfPlan.get(0).getPageId();
            Integer packageId = reportsOfPlan.get(0).getPackageId();

            Map<Date, PageStat> pageReportsByTime = null;
            Map<Date, TrackingStat> trackingReportsByTime = null;
            Map<Date, PackageStat> packageReportsByTime = null;

            if (pageId != null) {
                //获取该计划对应的落地页时段统计信息
                List<PageStat> pageReportsOfPlan = pageReports.get(pageId);

                //落地页统计信息按时段分组
                pageReportsByTime = groupByTime(pageReportsOfPlan, period);

                if (trackingReports != null) {
                    List<TrackingStat> trackingReportsOfPlan = trackingReports.get(pageId);
                    trackingReportsByTime = groupByTime(trackingReportsOfPlan, period);
                }
            }

            if (packageId != null) {
                //获取该计划对应的包时段统计信息
                List<PackageStat> packageReportsOfPlan = packageReports.get(packageId);
                //包统计信息按时段分组
                packageReportsByTime = groupByTime(packageReportsOfPlan, period);
            }

            //遍历一个计划的各个时段统计信息，设置相应的落地页和包统计指标
            for (RealTimeReport report : reportsOfPlan) {
                //将统计时间转换为对应的整点时段
                Date time = periodTime(report.getTime(), period);

                //将落地页指标添加到实时报告
                if (pageReportsByTime != null) {
                    //获取该时段的落地页统计
                    PageStat pageStat = pageReportsByTime.get(time);
                    if (pageStat != null) {
                        //设置落地页的到达数到实时统计报告
                        report.setReaches(pageStat.getReaches());
                        //下载数
                        report.setDownloads(pageStat.getDownloads());
                    }
                }

                //将Tracking指标添加到实时报告
                if (trackingReportsByTime != null) {
                    TrackingStat trackingStat = trackingReportsByTime.get(time);
                    if (trackingStat != null) {
                        //激活数
                        report.setActivation(trackingStat.getActivations());
                    }
                }

                //将CP指标添加到实时报告
                if (packageReportsByTime != null) {
                    //获取该时段的包统计
                    PackageStat packageStat = packageReportsByTime.get(time);
                    if (packageStat != null) {
                        //设置注册数到实时统计报告
                        report.setFirstDayRegistrations(packageStat.getFirstDayRegistrations());
                    }
                }

            }
        }
        return planReports;
    }

    /**
     * 按时间分组统计
     *
     * @param list
     * @param period 时段
     * @param <T>
     * @return
     */
    private static <T extends Time> Map<Date, T> groupByTime(List<T> list, Period period) {
        if (list == null) {
            return null;
        }
        Map<Date, T> map = new HashMap<>();
        for (T item : list) {
            //以时间段为键
            map.put(periodTime(item.getTime(), period), item);
        }
        return map;
    }

    /**
     * 根据十分钟间隔的实时报告获取20分钟、半小时、小时间隔统计数据
     *
     * @param reportsInTenMinutes
     * @param period
     * @return
     */
    private static <T extends Time> Map<Integer, List<T>> getPeriodReportsByTenMinuteReport(Map<Integer, List<T>> reportsInTenMinutes, Period period) {
        if (Period.TenMinutes == period || reportsInTenMinutes == null) {
            return reportsInTenMinutes;
        }

        //键为ID，值为该计划的时段指标列表
        Map<Integer, List<T>> reportsById = new HashMap<>();
        for (Map.Entry<Integer, List<T>> entry : reportsInTenMinutes.entrySet()) {
            //根据整点时段分组倒序排序
            Map<Date, T> reportsOfPlan = new TreeMap<>(new Comparator<Date>() {
                @Override
                public int compare(Date o1, Date o2) {
                    return o2.compareTo(o1);
                }
            });
            //列表已在数据按时间倒序排列，这里将最靠近时段的统计作为该时段的统计。
            for (T report : entry.getValue()) {
                //时间“小”的统计报告会覆盖时间“大”的统计报告.
                reportsOfPlan.put(periodTime(report.getTime(), period), report);
            }
            reportsById.put(entry.getKey(), new ArrayList<>(reportsOfPlan.values()));
        }
        return reportsById;
    }

    /**
     * 将时间转为时段.
     *
     * @param time
     * @param period
     * @return
     */
    private static Date periodTime(Date time, Period period) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        //将分钟数调整到完整的时段(10分，20分，30分，小时)
        int minute = calendar.get(Calendar.MINUTE);
        int min = minute / period.getIntervalInMinutes() * period.getIntervalInMinutes();
        calendar.set(Calendar.MINUTE, min);
        return calendar.getTime();
    }

    /**
     * 按计划ID分组
     *
     * @param planStats
     * @return
     */
    private static Map<Integer, List<RealTimeReport>> groupByPlanId(List<RealTimeReport> planStats) {
        if (planStats == null) {
            return null;
        }
        //根据计划Id分组.
        Map<Integer, List<RealTimeReport>> reportsGroupByPlanId = new HashMap<>();
        for (RealTimeReport report : planStats) {
            if (reportsGroupByPlanId.containsKey(report.getPlanId())) {
                reportsGroupByPlanId.get(report.getPlanId()).add(report);
            } else {
                List<RealTimeReport> reportsOfPlan = new ArrayList<>();
                reportsOfPlan.add(report);
                reportsGroupByPlanId.put(report.getPlanId(), reportsOfPlan);
            }
        }
        return reportsGroupByPlanId;
    }

    /**
     * 按落地页ID分组
     *
     * @param pageStats
     * @return
     */
    private static Map<Integer, List<PageStat>> groupByPageId(List<PageStat> pageStats) {
        if (pageStats == null) {
            return null;
        }
        //根据落地页Id分组.
        Map<Integer, List<PageStat>> reportsGroupByPlanId = new HashMap<>();
        for (PageStat report : pageStats) {
            if (reportsGroupByPlanId.containsKey(report.getPageId())) {
                reportsGroupByPlanId.get(report.getPageId()).add(report);
            } else {
                List<PageStat> reportsOfPlan = new ArrayList<>();
                reportsOfPlan.add(report);
                reportsGroupByPlanId.put(report.getPageId(), reportsOfPlan);
            }
        }
        return reportsGroupByPlanId;
    }

    /**
     * 按落地页ID分组统计Tracking信息
     *
     * @param trackingStats
     * @return {page id ->tracking stat list}
     */
    private static Map<Integer, List<TrackingStat>> groupTrackingByPageId(List<TrackingStat> trackingStats) {
        if (trackingStats == null) {
            return null;
        }
        //根据落地页Id分组.
        Map<Integer, List<TrackingStat>> reportsGroupByPlanId = new HashMap<>();
        for (TrackingStat report : trackingStats) {
            if (reportsGroupByPlanId.containsKey(report.getPageId())) {
                reportsGroupByPlanId.get(report.getPageId()).add(report);
            } else {
                List<TrackingStat> reportsOfPlan = new ArrayList<>();
                reportsOfPlan.add(report);
                reportsGroupByPlanId.put(report.getPageId(), reportsOfPlan);
            }
        }
        return reportsGroupByPlanId;
    }

    /**
     * 按包ID分组
     *
     * @param packageStats
     * @return
     */
    private static Map<Integer, List<PackageStat>> groupByPackageId(List<PackageStat> packageStats) {
        if (packageStats == null) {
            return null;
        }
        //根据落地页Id分组.
        Map<Integer, List<PackageStat>> reportsGroupByPlanId = new HashMap<>();
        for (PackageStat report : packageStats) {
            if (reportsGroupByPlanId.containsKey(report.getPackageId())) {
                reportsGroupByPlanId.get(report.getPackageId()).add(report);
            } else {
                List<PackageStat> reportsOfPlan = new ArrayList<>();
                reportsOfPlan.add(report);
                reportsGroupByPlanId.put(report.getPackageId(), reportsOfPlan);
            }
        }
        return reportsGroupByPlanId;
    }
}
