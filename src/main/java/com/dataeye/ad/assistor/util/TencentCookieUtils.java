package com.dataeye.ad.assistor.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by huangzehai on 2017/5/19.
 */
public final class TencentCookieUtils {

    private TencentCookieUtils() {

    }

    private static final String SKEY = "skey";

    private static final String UID_PATTERN = "&uid=(\\d+)";

    public static String getGTK(String cookie) {
        String skey = CookieUtils.get(cookie, SKEY);
        int hash = 5381;
        for (int i = 0, len = skey.length(); i < len; ++i) {
            hash += (hash << 5) + (int) (char) skey.charAt(i);
        }
        return (hash & 0x7fffffff) + "";
    }

    public static String getUid(String cookie) {
        Pattern pattern = Pattern.compile(UID_PATTERN);
        Matcher matcher = pattern.matcher(cookie);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }
}
