package com.dataeye.ad.assistor.util;

import com.dataeye.util.log.DELogger;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.URLEncoder;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpUtil {

	private final static Logger logger = DELogger.getLogger("httputil_log");

	private static DefaultHttpClient httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager());

	private static DefaultHttpClient httpsClient = new SSLClient(new ThreadSafeClientConnManager());

	{
		httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
		httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 20000);
		/**
		 * 单例模式下忽略Cookie，由Request设置Header头字段的Cookie
		 */
		httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.IGNORE_COOKIES);

		httpsClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
		httpsClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 20000);
		/**
		 * 单例模式下忽略Cookie，由Request设置Header头字段的Cookie
		 */
		httpsClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.IGNORE_COOKIES);

	}

	public static HttpClient getHttpClient() {
		// httpClient.getCookieStore().clear();
		// return httpClient;
		return getNewHttpClient();
	}

	public static HttpClient getHttpsClient(){
		// httpsClient.getCookieStore().clear();
		// return httpsClient;
		 return getNewHttpsClient();
	}

	public static HttpClient getNewHttpClient() {
		DefaultHttpClient httpClient1 = new DefaultHttpClient(new ThreadSafeClientConnManager());
		httpClient1.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
		httpClient1.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 20000);
		httpClient1.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.IGNORE_COOKIES);
		httpClient1.getCookieStore().clear();
		return httpClient1;
	}

	public static HttpClient getNewHttpsClient() {
		DefaultHttpClient httpsClient1 = new SSLClient(new ThreadSafeClientConnManager());
		httpsClient1.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
		httpsClient1.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 20000);
		httpsClient1.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.IGNORE_COOKIES);
		httpsClient1.getCookieStore().clear();
		return httpsClient1;
	}

	public static String getContent(HttpEntity entity) {
		if (entity == null) {
			return null;
		}
		String content = null;
		try {
			Header contentEncoding = entity.getContentEncoding();
			if (null != contentEncoding && contentEncoding.getValue().toLowerCase().contains("gzip")) {
				content = EntityUtils.toString(new GzipDecompressingEntity(entity), "utf-8");
			} else {
				content = EntityUtils.toString(entity, "utf-8");
			}
		} catch (Exception e) {
			logger.error(ServerUtil.printStackTrace(e));
		} finally {
			// 确保数据流关闭，否则HttpClient将不能被后续请求使用
			try {
				entity.getContent().close();
			} catch (IllegalStateException | IOException e1) {
				e1.printStackTrace();
			}
		}
		return content;
	}

	public static String urlParamReplace(String url, String... vars) {
		for (int i = 0; i < vars.length; i++) {
			url = url.replace("{" + i + "}", vars[i]);
		}
		return url;
	}

	public static String urlEncode(String url) {
		try {
			return URLEncoder.encode(url, "utf-8");
		} catch (Exception e) {
			return null;
		}
	}

	public static Map<String, String> getCookieMap(String cookies) {
		Map<String, String> cookieMap = new HashMap<String, String>();
		String[] cookieArr = cookies.split(";");
		for (String cookie : cookieArr) {
			String[] kv = cookie.split("=");
			String key = kv[0].trim();
			String value = "";
			if (kv.length == 2) {
				value = kv[1].trim();
			}
			cookieMap.put(key, value);
		}
		return cookieMap;
	}

	public static String getCookieValue(String cookies, String field) {
		return getCookieMap(cookies).get(field);
	}

	public static String get(String url) {
		DefaultHttpClient httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager());
		httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 30000);
		httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 30000);
		try {
			HttpGet httpget = new HttpGet(url);
			HttpResponse response = httpClient.execute(httpget);
			// int code = response.getStatusLine().getStatusCode();
			HttpEntity httpEntity = response.getEntity();
			if (httpEntity != null) {
				byte[] bytes = EntityUtils.toByteArray(httpEntity);
				return new String(bytes, "utf-8");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String post(String url, Map<String, String> para) throws ServerException {
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager());
			httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
			httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 20000);
			HttpPost httppost = new HttpPost(url);
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			if (para != null && para.size() > 0)
				for (Map.Entry<String, String> entry : para.entrySet()) {
					params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
				}
			// Post请求
			// 设置参数
			httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			// 发送请求
			HttpResponse httpresponse = httpClient.execute(httppost);
			HttpEntity httpEntity = httpresponse.getEntity();
			if (httpEntity != null) {
				byte[] bytes = EntityUtils.toByteArray(httpEntity);
				return new String(bytes, "utf-8");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		String url = "http://tui.qq.com/misapi/adgroup/select?filter=[{\"field\":\"ad_group_name\",\"operator\":\"CONTAINS\",\"value\":\"\"},{\"field\":\"status\",\"operator\":\"EQUALS\",\"value\":\"ADGROUP_STATUS_UN_DELETED\"},{\"field\":\"price_mode\",\"operator\":\"EQUALS\",\"value\":\"\"},{\"field\":\"product_type\",\"operator\":\"EQUALS\",\"value\":\"\"},{\"field\":\"begin_date\",\"operator\":\"EQUALS\",\"value\":\"2017-02-23\"},{\"field\":\"end_date\",\"operator\":\"EQUALS\",\"value\":\"9999-12-31\"},{\"field\":\"data_begin_date\",\"operator\":\"EQUALS\",\"value\":\"2017-02-23\"},{\"field\":\"data_end_date\",\"operator\":\"EQUALS\",\"value\":\"2017-02-23\"},{\"field\":\"is_filter_data\",\"operator\":\"EQUALS\",\"value\":\"\"}]&page=1&page_size=50&advertiser_id=10012572";
		System.out.println(urlEncode(url));

	}
}
