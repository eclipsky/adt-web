package com.dataeye.ad.assistor.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by huangzehai on 2017/5/22.
 */
public final class NumberUtils {
    private NumberUtils() {

    }

    /**
     * 两个整数相加.
     *
     * @param a
     * @param b
     * @return
     */
    public static Integer add(Integer a, Integer b) {
        if (a == null) {
            return b;
        } else if (b == null) {
            return a;
        } else {
            return a + b;
        }
    }

    /**
     * 两个浮点数相加
     *
     * @param a
     * @param b
     * @return
     */
    public static Double add(Double a, Double b) {
        if (a == null) {
            return b;
        } else if (b == null) {
            return a;
        } else {
            return a + b;
        }
    }


    /**
     * 两个十进制数相加
     *
     * @param a
     * @param b
     * @return
     */
    public static BigDecimal add(BigDecimal a, BigDecimal b) {
        if (a == null) {
            return b;
        } else if (b == null) {
            return a;
        } else {
            return a.add(b);
        }
    }

    /**
     * 计算两只十进制数的比率,譬如回本率
     *
     * @param num1
     * @param num2
     * @return
     */
    public static Double rate(BigDecimal num1, BigDecimal num2) {
        if (num1 == null || num2 == null || num2.compareTo(new BigDecimal(0)) == 0) {
            return null;
        }
        //默认保留四位小数
        return num1.divide(num2, 4, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * 计算十进制数和整数的比率，譬如CPA
     *
     * @param num1
     * @param num2
     * @return
     */
    public static BigDecimal cpa(BigDecimal num1, Integer num2) {
        if (num1 == null || num2 == null || num2 == 0) {
            return null;
        }
        //默认保留两位小数
        return num1.divide(new BigDecimal(num2), 2, RoundingMode.HALF_UP);
    }

    /**
     * 计算两个整数的比率，譬如CTR.
     *
     * @param num1
     * @param num2
     * @return
     */
    public static Double rate(Integer num1, Integer num2) {
        if (num1 == null || num2 == null || num2 == 0) {
            return null;
        }
        return (double) num1 / num2;
    }
}
