package com.dataeye.ad.assistor.util;

import com.dataeye.util.log.DELogger;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

public class ServerUtil {

	private final static Logger logger = DELogger.getLogger("server_log");

	public static String printStackTrace(Throwable e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		e.printStackTrace(pw);
		if (pw != null) {
			pw.close();
		}
		if (sw != null) {
			try {
				sw.close();
			} catch (IOException e1) {
				// e1.printStackTrace();
			}
		}
		return sw.toString();
	}

    public static Properties getConfigProperties(String configFile) {
        Properties properties = new Properties();
        try {
            properties.load(ServerUtil.class.getClassLoader().getResourceAsStream(configFile));
        } catch (IOException e) {
            logger.error("读取配置异常" + configFile + "->" + printStackTrace(e));
        }
        return properties;
    }

}
