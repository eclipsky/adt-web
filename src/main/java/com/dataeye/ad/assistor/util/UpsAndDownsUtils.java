package com.dataeye.ad.assistor.util;

import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.module.realtimedelivery.model.IndicatorUpsAndDowns;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeDeliveryVo;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeReport;
import com.dataeye.ad.assistor.module.realtimedelivery.model.UpsAndDowns;
import com.dataeye.ad.assistor.module.report.constant.Indicators;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by huangzehai on 2017/7/5.
 */
public final class UpsAndDownsUtils {
    private UpsAndDownsUtils() {

    }

    /**
     * 计算指标涨跌
     *
     * @param currentIndicators
     * @param previousIndicators
     */
    public static void computeUpsAndDowns(List<RealTimeDeliveryVo> currentIndicators, Map<Integer, RealTimeReport> previousIndicators, String indicatorPermission) {
        if (currentIndicators != null && !currentIndicators.isEmpty() && previousIndicators != null && !previousIndicators.isEmpty()) {
            for (RealTimeDeliveryVo currentIndicator : currentIndicators) {
                RealTimeReport previousIndicator = previousIndicators.get(currentIndicator.getPlanId());
                if (previousIndicator != null) {
                    //计算当前指标与之前指标的差值
                    currentIndicator.setUpsAndDowns(computeIndicatorUpsAndDowns(currentIndicator, previousIndicator, indicatorPermission));
                }
            }
        }
    }

    /**
     * 计算指标涨跌
     *
     * @param currentIndicator
     * @param previousIndicator
     * @return
     */
    private static IndicatorUpsAndDowns computeIndicatorUpsAndDowns(RealTimeDeliveryVo currentIndicator, RealTimeReport previousIndicator, String indicatorPermission) {
        IndicatorUpsAndDowns indicatorUpsAndDowns = new IndicatorUpsAndDowns();
        //计算消耗涨跌幅
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.COST)) {
            indicatorUpsAndDowns.setCost(computeCostUpsAndDowns(currentIndicator, previousIndicator));
        }
        //CTR
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CTR)) {
            indicatorUpsAndDowns.setCtr(computeCtrUpsAndDowns(currentIndicator, previousIndicator));
        }
        //下载率
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DOWNLOAD_RATE)) {
            indicatorUpsAndDowns.setDownloadRate(computeDownloadRateUpsAndDowns(currentIndicator, previousIndicator));
        }
        //激活CPA
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVE_CPA)) {
            indicatorUpsAndDowns.setActivationCpa(computeActivationCpaUpsAndDowns(currentIndicator, previousIndicator));
        }
        //注册CPA
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_CPA)) {
            indicatorUpsAndDowns.setRegistrationCpa(computeRegistrationCpaUpsAndDowns(currentIndicator, previousIndicator));
        }
        return indicatorUpsAndDowns;
    }

    /**
     * 计算消耗涨跌
     *
     * @param currentIndicator
     * @param previousIndicator
     * @return
     */
    private static UpsAndDowns computeCostUpsAndDowns(RealTimeDeliveryVo currentIndicator, RealTimeReport previousIndicator) {
        UpsAndDowns cost = new UpsAndDowns();
        //单位统一及转化
        if (currentIndicator.getTotalCost() != null && previousIndicator.getCost() != null) {
            cost.setIncrement(new BigDecimal(currentIndicator.getTotalCost()).subtract(previousIndicator.getCost()));
            if (previousIndicator.getCost() != null && previousIndicator.getCost() != null && previousIndicator.getCost().compareTo(new BigDecimal(0)) > 0) {
                NumberFormat nf = NumberFormat.getPercentInstance();
                nf.setMaximumFractionDigits(1);
                cost.setIncrementPercent(nf.format(cost.getIncrement().divide(previousIndicator.getCost(), 3, RoundingMode.HALF_UP).doubleValue()));
            } else {
                cost.setIncrementPercent(Labels.UNKNOWN);
            }
        }
        cost.setPercent(false);
        format(cost);
        return cost;
    }

    /**
     * 计算CTR涨跌
     *
     * @param currentIndicator
     * @param previousIndicator
     * @return
     */
    private static UpsAndDowns computeCtrUpsAndDowns(RealTimeDeliveryVo currentIndicator, RealTimeReport previousIndicator) {
        UpsAndDowns ctr = new UpsAndDowns();
        //单位统一及转化
        if (currentIndicator.getCtr() != null && previousIndicator.getCtr() != null && previousIndicator.getCtr() != null) {
            ctr.setIncrement(new BigDecimal(currentIndicator.getCtr() - previousIndicator.getCtr()).multiply(new BigDecimal(100)));
        }
        ctr.setPercent(true);
        format(ctr);
        return ctr;
    }

    /**
     * 计算下载率涨跌
     *
     * @param currentIndicator
     * @param previousIndicator
     * @return
     */
    private static UpsAndDowns computeDownloadRateUpsAndDowns(RealTimeDeliveryVo currentIndicator, RealTimeReport previousIndicator) {
        UpsAndDowns downloadRate = new UpsAndDowns();
        //单位统一及转化
        if (currentIndicator.getDownloadRate() != null && previousIndicator.getDownloadRate() != null && previousIndicator.getDownloadRate() != null) {
            downloadRate.setIncrement(new BigDecimal(currentIndicator.getDownloadRate() - previousIndicator.getDownloadRate()).multiply(new BigDecimal(100)));
        }
        downloadRate.setPercent(true);
        format(downloadRate);
        return downloadRate;
    }

    /**
     * 计算激活CPA涨跌
     *
     * @param currentIndicator
     * @param previousIndicator
     * @return
     */
    private static UpsAndDowns computeActivationCpaUpsAndDowns(RealTimeDeliveryVo currentIndicator, RealTimeReport previousIndicator) {
        UpsAndDowns activationCpa = new UpsAndDowns();
        //单位统一及转化
        if (currentIndicator.getActivationCpa() != null && previousIndicator.getActivationCpa() != null) {
            activationCpa.setIncrement(currentIndicator.getActivationCpa().subtract(previousIndicator.getActivationCpa()));
            if (previousIndicator.getActivationCpa() != null && previousIndicator.getActivationCpa() != null && previousIndicator.getActivationCpa().compareTo(new BigDecimal(0)) > 0) {
                NumberFormat nf = NumberFormat.getPercentInstance();
                nf.setMaximumFractionDigits(1);
                activationCpa.setIncrementPercent(nf.format(activationCpa.getIncrement().divide(previousIndicator.getActivationCpa(), 4, RoundingMode.HALF_UP)));
            } else {
                activationCpa.setIncrementPercent(Labels.UNKNOWN);
            }
        }
        activationCpa.setPercent(false);
        format(activationCpa);
        return activationCpa;
    }

    /**
     * 计算注册CPA涨跌
     *
     * @param currentIndicator
     * @param previousIndicator
     * @return
     */
    private static UpsAndDowns computeRegistrationCpaUpsAndDowns(RealTimeDeliveryVo currentIndicator, RealTimeReport previousIndicator) {
        UpsAndDowns registrationCpa = new UpsAndDowns();
        //单位统一及转化
        if (currentIndicator.getCostPerRegistrationFd() != null && previousIndicator.getCostPerRegistrationFd() != null) {
            registrationCpa.setIncrement(new BigDecimal(currentIndicator.getCostPerRegistrationFd()).subtract(previousIndicator.getCostPerRegistrationFd()));
            if (previousIndicator.getCostPerRegistrationFd() != null && previousIndicator.getCostPerRegistrationFd() != null && previousIndicator.getCostPerRegistrationFd().compareTo(new BigDecimal(0)) > 0) {
                NumberFormat nf = NumberFormat.getPercentInstance();
                nf.setMaximumFractionDigits(1);
                registrationCpa.setIncrementPercent(nf.format(registrationCpa.getIncrement().divide(previousIndicator.getCostPerRegistrationFd(), 4, RoundingMode.HALF_UP)));
            } else {
                registrationCpa.setIncrementPercent(Labels.UNKNOWN);
            }
        }
        registrationCpa.setPercent(false);
        format(registrationCpa);
        return registrationCpa;
    }

    /**
     * 格式化涨跌
     *
     * @param upsAndDowns
     */
    private static void format(UpsAndDowns upsAndDowns) {
        if (upsAndDowns.getIncrement() != null) {
            //保留两位小数
            upsAndDowns.setIncrement(upsAndDowns.getIncrement().setScale(2, RoundingMode.HALF_UP));
        }
    }
}
