package com.dataeye.ad.assistor.util;

import com.dataeye.ad.assistor.constant.Constant;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 指标工具类.
 * Created by huangzehai on 2017/6/13.
 */
public final class IndicatorUtils {
    private IndicatorUtils() {

    }

    /**
     * 解析指标偏好,将字符串转化为map.
     *
     * @param indicatorPreference
     * @return
     */
    public static Map<String, Integer> parseIndicatorPreference(String indicatorPreference) {
        Map<String, Integer> preferences = new HashMap<>();
        if (StringUtils.isNotBlank(indicatorPreference)) {
            String[] prefs = indicatorPreference.split(Constant.Separator.COMMA);
            for (String pref : prefs) {
                String[] indicatorAndPref = pref.split(Constant.Separator.COLON);
                preferences.put(indicatorAndPref[0], Integer.valueOf(indicatorAndPref[1]));
            }
        }
        return preferences;
    }
}
