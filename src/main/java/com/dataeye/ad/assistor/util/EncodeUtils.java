package com.dataeye.ad.assistor.util;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;


/**
 * 编码工具类
 * 1.将byte[]转为各种进制的字符串
 * 2.base 64 encode
 * 3.base 64 decode
 * 4.获取byte[]的md5值
 * 5.获取字符串md5值
 * 6.结合base64实现md5加密
 * 7.AES加密
 * 8.AES加密为base 64 code
 * 9.AES解密
 * 10.将base 64 code AES解密
 *
 * @author louis 2017/02/07
 */
public class EncodeUtils {

    protected final static Logger logger = LoggerFactory.getLogger(EncodeUtils.class);

    private static String key = "1234567890123456";

    public static void main(String[] args) throws Exception {
        String content = "abc123";
        System.out.println("加密前：" + content);

        System.out.println("AES加解密密钥：" + key);

        String encrypt = aesEncrypt(content, key);
        System.out.println("AES加密后：" + encrypt);

        String decrypt = aesDecrypt(encrypt, key);
        System.out.println("AES解密后：" + decrypt);

        String md5Base64Encrypt = md5Base64Encode(content);
        System.out.println("MD5-Base64加密后：" + md5Base64Encrypt);

        String md5Encrypt = md5Encode(content);
        System.out.println("MD5加密后：" + md5Encrypt);
    }

    /**
     * 将byte[]转为各种进制的字符串
     *
     * @param bytes byte[]
     * @param radix 可以转换进制的范围，从Character.MIN_RADIX到Character.MAX_RADIX，超出范围后变为10进制
     * @return 转换后的字符串
     */
    public static String binary(byte[] bytes, int radix) {
        return new BigInteger(1, bytes).toString(radix);// 这里的1代表正数
    }

    /**
     * base 64 encode
     *
     * @param bytes 待编码的byte[]
     * @return 编码后的base 64 code
     */
    public static String base64Encode(byte[] bytes) {
        return new BASE64Encoder().encode(bytes);
    }

    /**
     * base 64 decode
     *
     * @param base64Code 待解码的base 64 code
     * @return 解码后的byte[]
     * @throws Exception
     */
    public static byte[] base64Decode(String base64Code) throws Exception {
        return StringUtils.isEmpty(base64Code) ? null : new BASE64Decoder().decodeBuffer(base64Code);
    }

    /**
     * 获取byte[]的md5值 ，返回值为8为字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
     *
     * @param bytes byte[]
     * @return md5
     * @throws Exception
     */
    public static byte[] md5(byte[] bytes) throws Exception {
        // 生成一个MD5加密计算摘要
        MessageDigest md = MessageDigest.getInstance("MD5");
        // 计算md5函数
        md.update(bytes);
        // digest()最后确定返回md5 hash值
        return md.digest();
    }

    /**
     * 获取字符串md5值
     *
     * @param msg
     * @return md5
     * @throws Exception
     */
    public static byte[] md5Bytes(String msg) throws Exception {
        return StringUtils.isEmpty(msg) ? null : md5(msg.getBytes());
    }

    public static String md5(String msg) throws Exception {
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        // 生成一个MD5加密计算摘要
        MessageDigest mdInst = MessageDigest.getInstance("MD5");
        // 计算md5函数
        mdInst.update(msg.getBytes());
        // digest()最后确定返回md5 hash值
        byte[] md = mdInst.digest();

        // 把密文转换成十六进制的字符串形式
        int j = md.length;
        char str[] = new char[j * 2];
        int k = 0;
        for (int i = 0; i < j; i++) {
            byte byte0 = md[i];
            str[k++] = hexDigits[byte0 >>> 4 & 0xf];
            str[k++] = hexDigits[byte0 & 0xf];
        }
        return new String(str);
    }

    /**
     * md5加密：BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
     *
     * @param msg 待加密字符串
     * @return md5
     * @throws Exception
     */
    public static String md5Encode(String msg) throws Exception {
        return StringUtils.isEmpty(msg) ? null : md5(msg);
    }

//    /** 
//     * md5加密：BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
//     * @param msg 待加密字符串 
//     * @return md5
//     * @throws Exception 
//     */  
//    public static String md5Encode(String msg) throws Exception{  
//        return StringUtils.isEmpty(msg) ? null : new BigInteger(1, md5(msg.getBytes())).toString(16);  
//    }

    /**
     * 结合base64实现md5加密
     *
     * @param msg 待加密字符串
     * @return 获取md5后转为base64
     * @throws Exception
     */
    public static String md5Base64Encode(String msg) throws Exception {
        return StringUtils.isEmpty(msg) ? null : base64Encode(md5Bytes(msg));
    }

    /**
     * AES加密
     *
     * @param content    待加密的内容
     * @param encryptKey 加密密钥
     * @return 加密后的byte[]
     * @throws Exception
     */
    public static byte[] aesEncryptToBytes(String content, String encryptKey) throws Exception {
        // 被注释代码的方式，在window下可正常加密，linux下每次结果都会变，不满足需要，需要设置为ECB加密模式，同时指定随机数生成规则
//        KeyGenerator kgen = KeyGenerator.getInstance("AES");  
//        kgen.init(128, new SecureRandom(encryptKey.getBytes()));  
//  
//        Cipher cipher = Cipher.getInstance("AES");  
//        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"));  
//          
//        return cipher.doFinal(content.getBytes("utf-8"));  


        KeyGenerator kgen = KeyGenerator.getInstance("AES");

        //需手动指定 SecureRandom 随机数生成规则
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(encryptKey.getBytes());
        kgen.init(128, random);

        //创建密码器（采用ECB加密模式）
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        byte[] byteContent = content.getBytes("utf-8");
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"));
        return cipher.doFinal(byteContent);
    }

    /**
     * AES加密为base 64 code
     *
     * @param content    待加密的内容
     * @param encryptKey 加密密钥
     * @return 加密后的base 64 code
     * @throws Exception
     */
    public static String aesEncrypt(String content, String encryptKey) throws Exception {
        return base64Encode(aesEncryptToBytes(content, encryptKey));
    }

    /**
     * AES解密
     *
     * @param encryptBytes 待解密的byte[]
     * @param decryptKey   解密密钥
     * @return 解密后的String
     * @throws Exception
     */
    public static String aesDecryptByBytes(byte[] encryptBytes, String decryptKey) throws Exception {
        // 被注释代码的方式，在window下可正常加密，linux下每次结果都会变，不满足需要，需要设置为ECB加密模式，同时指定随机数生成规则
//        KeyGenerator kgen = KeyGenerator.getInstance("AES");  
//        kgen.init(128, new SecureRandom(decryptKey.getBytes()));  
//          
//        Cipher cipher = Cipher.getInstance("AES");  
//        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"));  
//        byte[] decryptBytes = cipher.doFinal(encryptBytes);  

        KeyGenerator kgen = KeyGenerator.getInstance("AES");

        //需手动指定 SecureRandom 随机数生成规则
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(decryptKey.getBytes());
        kgen.init(128, random);

        //创建密码器（采用ECB加密模式）
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"));
        byte[] result = cipher.doFinal(encryptBytes);
        return new String(result);
    }

    /**
     * 将base 64 code AES解密
     *
     * @param encryptStr 待解密的base 64 code
     * @param decryptKey 解密密钥
     * @return 解密后的string
     * @throws Exception
     */
    public static String aesDecrypt(String encryptStr, String decryptKey) throws Exception {
        return StringUtils.isEmpty(encryptStr) ? null : aesDecryptByBytes(base64Decode(encryptStr), decryptKey);
    }

    public final static String md5Upper(String s) {

        if (com.qq.jutil.string.StringUtil.isEmpty(s)) {
            return null;
        }

        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            byte[] strTemp = s.getBytes();
            MessageDigest mdTemp = MessageDigest.getInstance("MD5");
            mdTemp.update(strTemp);
            byte[] md = mdTemp.digest();
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str).toUpperCase();
        } catch (Exception e) {
            logger.error("SHA-1 Encrypt error!!", e);
            return null;
        }
    }

    public final static String md5Lower(String s) {
        String rs = md5Upper(s);
        if (com.qq.jutil.string.StringUtil.isEmpty(rs)) {
            return null;
        }
        return rs.toLowerCase();
    }


}  