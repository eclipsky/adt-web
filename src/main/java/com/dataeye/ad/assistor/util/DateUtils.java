package com.dataeye.ad.assistor.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.report.constant.Constants;

import org.apache.commons.lang.StringUtils;

/**
 * <pre>
 * 日期操作类
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:51:56
 */
public class DateUtils {

    /**
     * 一周之前.
     */
    public static int ONE_WEEK_AGO = -6;
    /**
     * 今天
     */
    public static int TODAY = 0;

    /**
     * 日期格式.
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    /**
     * 日期时间格式.
     */
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /**
     * 月份格式.
     */
    public static final String MONTH_FORMAT = "yyyy-MM";

    /**
     * 月日格式
     */
    public static final String MONTH_DATE_FORMAT = "MM-dd";

    /**
     * 获取毫秒级时间戳
     *
     * @return long
     * @author Stran
     * @since 2016.09.30 16:52:02
     */
    public static final long winTimestamp() {
        return System.currentTimeMillis();
    }

    /**
     * 取当天0点0分0秒的时间戳
     *
     * @return int
     * @author Stran
     * @since 2016.09.30 16:52:17
     */
    public static int getTimestamp000() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return (int) (calendar.getTimeInMillis() / 1000);
    }


    /**
     * 获取当前日期时间字符串
     *
     * @return string
     * @author Stran
     * @since 2016.09.30 16:52:21
     */
    public static final String now() {
        return new SimpleDateFormat(DATE_TIME_FORMAT).format(new Date());
    }

    /**
     * 获取昨天的日期
     *
     * @return string
     * @author Stran
     * @since 2016.09.30 16:52:25
     */
    public static final String yesterday() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-"
                + calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取当前日期字符串
     *
     * @return string
     * @author Stran
     * @since 2016.09.30 16:52:28
     */
    public static final String currentDate() {
        return new SimpleDateFormat(DATE_FORMAT).format(new Date());
    }

    /**
     * 获取当前时间字符串
     *
     * @return string
     * @author Stran
     * @since 2016.09.30 16:52:32
     */
    public static final String currentTime() {
        return new SimpleDateFormat(DATE_TIME_FORMAT).format(new Date());
    }

    /**
     * check startDate是否小于endDate
     *
     * @param startDate
     * @param endDate
     * @param df
     * @return boolean
     * @author Stran
     * @since 2016.09.22 20:21:21
     */
    public static boolean before(String startDate, String endDate, DateFormat df) {
        try {
            return df.parse(startDate).before(df.parse(endDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * check startDate是否大于endDate
     *
     * @param startDate
     * @param endDate
     * @param df
     * @return boolean
     * @author Stran
     * @since 2016.09.22 16:59:51
     */
    public static boolean after(String startDate, String endDate, DateFormat df) {
        try {
            return df.parse(startDate).after(df.parse(endDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * 将标准日期格式字符串转化为Date。
     *
     * @param dateString
     * @return
     * @throws ParseException
     */
    public static Date parse(String dateString) throws ParseException {
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        return df.parse(dateString);
    }

    /**
     * 将Date格式化为标准日期格式
     *
     * @param date
     * @return
     */
    public static String format(Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(DATE_FORMAT).format(date);
    }

    /**
     * 格式化日期
     *
     * @param date
     * @param format
     * @return
     */
    public static String format(Date date, String format) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * 生成日期范围字符串.
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static String dateRange(Date startDate, Date endDate) {
        StringBuilder dateBuilder = new StringBuilder();
        dateBuilder.append(format((startDate)));
        dateBuilder.append(Constants.TO);
        dateBuilder.append(format(endDate));
        return dateBuilder.toString();
    }

    /**
     * 获取这个月的第一天
     *
     * @return
     */
    public static Date firstDayOfThisMonth() {
        return firstDayOfMonth(null);
    }

    /**
     * 获取这个月的第一天
     *
     * @return
     */
    public static Date firstDayOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取上个月的最后一天.
     *
     * @return
     */
    public static Date lastDayOfPreviousMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }

    /**
     * 获取指定日期所在月份的最后一天.
     *
     * @param date
     * @return
     */
    public static Date lastDayOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }

    /**
     * 获取指定日期所在周的星期几
     *
     * @param date
     * @param dayOfWeek
     * @return
     */
    public static Date dayOfWeek(Date date, int dayOfWeek) {
        Calendar calendar = Calendar.getInstance();
        //将星期一为一周的第一天
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 将字符串形式的日期转化为java Date类型，如果字符串为空或者格式不正确，返回指定的日期.
     *
     * @param date
     * @param dates
     * @return
     * @throws ParseException
     */
    public static Date defaultIfBlank(String date, int dates) throws ParseException {
        if (StringUtils.isBlank(date) || !Pattern.matches("\\d{4}-\\d{2}-\\d{2}", date)) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.add(Calendar.DATE, dates);
            return calendar.getTime();
        } else {
            return parse(date);
        }
    }

    /**
     * 获取N分钟前的整十分时间.
     *
     * @param minutes
     * @return
     */
    public static Date getTimeBeforeMinutes(int minutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.add(Calendar.MINUTE, -minutes);
        calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) / 10 * 10);
        return calendar.getTime();
    }
    
    /** 
     * 获取精确到秒的时间戳 
     * @return 
     */  
    public static int getSecondTimestamp(Date date){  
        if (null == date) {  
            return 0;  
        }  
        String timestamp = String.valueOf(date.getTime());  
        int length = timestamp.length();  
        if (length > 3) {  
            return Integer.valueOf(timestamp.substring(0,length-3));  
        } else {  
            return 0;  
        }  
    }

    public static void main(String[] args) throws ParseException {
        // String date = "2015-08-09";
        // System.out.println(DateUtils.getNextDate(date, "yyyy-MM-dd"));

       /* String startDate = "20151228";
        String endDate = "20160103";
        System.out.println(DateUtils.getTimestamp000());*/
    	
    	/*Date nowDate = DateUtils.parse(DateUtils.now());
    	int num = 60;
    	Date statDate = DateUtils.parse("2017-08-10");
    	String statDateStr ="2017-08-10";
    	
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(nowDate);
    	cal.add(Calendar.DATE, -num);
    	String nowDateStr = DateUtils.format(cal.getTime());
    	DateFormat df = new SimpleDateFormat(DateUtils.DATE_FORMAT);
    	
    	
    	//投放日期要>=(当前日期-1)，
    	if(statDate != null && num >= 0 && DateUtils.after(nowDateStr,statDateStr , df)){
    		System.out.println(nowDateStr+"有"+num+"回本率，=="+statDateStr);
    	}
    	System.out.println("没有====="+nowDateStr+"有"+num+"回本率，=="+statDateStr);
*/

    		System.out.println(getSecondTimestamp(new Date()));	
    		System.out.println(System.currentTimeMillis());	
    		
    }

}
