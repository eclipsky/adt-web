package com.dataeye.ad.assistor.util;

import java.io.StringWriter;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.constant.Constant.CommonURL;
import com.dataeye.ad.assistor.constant.Constant.DeProduct;
import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.exception.ServerException;

/**
 * <pre>
 * 发送邮件的工具
 * @author Ivan          <br>
 * @date 2014年12月26日 下午3:32:20
 * <br>
 * 
 */
public class MailUtils {

	/**
	 * 
	 * <pre>
	 * 发送告警邮件
	 *  @param subject 邮件主题
	 *  @param content 邮件内容
	 *  @param to 用逗号分开的邮箱列表
	 *  @return  true:发送成功,false:发送失败
	 *  @author Ivan<br>
	 *  @date 2014年12月26日 下午3:34:16
	 * <br>
	 */
	public static boolean sendAlarmMail(String subject, String content, String to) {
		try {
			
			JavaMailWithAttachment se = new JavaMailWithAttachment(false);
			return se.doSendHtmlEmail(subject, content, to, null, Constant.MailType.ALARM_MAIL);//
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 发送创建系统账号的邮件
	 * 
	 * @param email
	 * @param password
	 * @param accountAlias 账号别名
	 * @return
	 * @author luzhuyou 2017/02/18
	 * @throws ServerException
	 */
	public static boolean sendCreateSystemAccountMail(String email, String password, String accountAlias)
			throws ServerException {
		
		try {
			// 根据语言获取邮件主题和邮件模板
			String tplFile = "createaccount.vm";
			String mailSubject = "已为您创建ADT系统账号";
			
			// 获取邮件内容
			String mailContent = "";
			VelocityEngine velocityEngine = (VelocityEngine) ApplicationContextContainer.getBean("velocityEngine");
			Template t = velocityEngine.getTemplate(tplFile);
			VelocityContext context = new VelocityContext();
			context.put("email", email);
			context.put("password", password);
			context.put("accountAlias", accountAlias);
			context.put("website", CommonURL.PT_LOGIN_URL);
			StringWriter writer = new StringWriter();
			t.merge(context, writer);
			mailContent = writer.toString();
			

			JavaMailWithAttachment se = new JavaMailWithAttachment(false);
			return se.doSendHtmlEmail(mailSubject, mailContent, email, null, Constant.MailType.CREATE_ACCOUNT);

		} catch (Exception e) {
			ExceptionHandler.throwMailSendException(StatusCode.LOGI_MAIL_SEND_FAIL, e.getMessage());
		}
		return false;
	}
	
	/**
	 * 发送重置系统账号登录密码的邮件
	 * 
	 * @param email
	 * @param password
	 * @param accountAlias 账号别名
	 * @return
	 * @author luzhuyou 2017/02/18
	 * @throws ServerException
	 */
	public static boolean sendResetPwdMail(String email, String password, String accountAlias)
			throws ServerException {
		
		try {
			// 根据语言获取邮件主题和邮件模板
			String tplFile = "resetpwd.vm";
			String mailSubject = "您的ADT系统账号密码已重置";
			
			// 获取邮件内容
			String mailContent = "";
			VelocityEngine velocityEngine = (VelocityEngine) ApplicationContextContainer.getBean("velocityEngine");
			Template t = velocityEngine.getTemplate(tplFile);
			VelocityContext context = new VelocityContext();
			context.put("email", email);
			context.put("accountAlias", accountAlias);
			context.put("password", password);
			context.put("website", CommonURL.PT_LOGIN_URL);
			StringWriter writer = new StringWriter();
			t.merge(context, writer);
			mailContent = writer.toString();
			

			JavaMailWithAttachment se = new JavaMailWithAttachment(false);
			return se.doSendHtmlEmail(mailSubject, mailContent, email, null, Constant.MailType.RESET_PWD);

		} catch (Exception e) {
			ExceptionHandler.throwMailSendException(StatusCode.LOGI_MAIL_SEND_FAIL, e.getMessage());
		}
		return false;
	}
	
	/**
	 * 发送运营审核通知邮件（新客户接入ADT/Tracking时发送邮件）
	 * @param productType 1-adt, 2-tracking
	 * @param companyName
	 * @param customerName
	 * @param customerAccount
	 * @param customerTel
	 * @param customerQq
	 * @param receiveUserEmail
	 * @return
	 * @throws ServerException
	 * @author luzhuyou 2017/05/18
	 */
	public static boolean sendNewAccountAccessMail(int productType, String companyName, String customerName, String customerAccount, String customerTel, String customerQq, String receiveUserEmail)
			throws ServerException {
		
		try {
			// 根据语言获取邮件主题和邮件模板
			String tplFile = "new-account-access.vm";
			String mailSubject = "客户[" + companyName + "]申请接入" + DeProduct.getName(productType) + "，请进行审核";
			
			// 获取邮件内容
			String mailContent = "";
			VelocityEngine velocityEngine = (VelocityEngine) ApplicationContextContainer.getBean("velocityEngine");
			Template t = velocityEngine.getTemplate(tplFile);
			VelocityContext context = new VelocityContext();
			context.put("companyName", companyName);
			context.put("customerName", customerName);
			context.put("customerAccount", customerAccount);
			context.put("customerTel", customerTel);
			context.put("customerQq", customerQq);
			context.put("receiveUser", receiveUserEmail);
			context.put("website", CommonURL.ADT_CRM_URL);
			context.put("deProductName", DeProduct.getName(productType));
			
			StringWriter writer = new StringWriter();
			t.merge(context, writer);
			mailContent = writer.toString();
			

			JavaMailWithAttachment se = new JavaMailWithAttachment(false);
			return se.doSendHtmlEmail(mailSubject, mailContent, receiveUserEmail, null, Constant.MailType.BIZ_AUDIT);

		} catch (Exception e) {
			ExceptionHandler.throwMailSendException(StatusCode.LOGI_MAIL_SEND_FAIL, e.getMessage());
		}
		return false;
	}
}
