package com.dataeye.ad.assistor.util;

public final class TuiUtils {

    private static final String LOGIN_ID = "login_id";

    private static final String XSRF_TOKEN = "XSRF-TOKEN";

    private TuiUtils() {

    }

    /**
     * 获取登录ID.
     *
     * @param cookie
     * @return
     */
    public static String getLoginId(String cookie) {
        return CookieUtils.parse(cookie).get(LOGIN_ID);
    }

    public static String getCsrfToken(String cookie) {
        return CookieUtils.parse(cookie).get(XSRF_TOKEN);
    }


}
