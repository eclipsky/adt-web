//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.dataeye.ad.assistor.util.redis;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.*;

import java.util.*;

public class RedisConnectionUtils {
    private static final Logger LOG = LoggerFactory.getLogger(RedisConnectionUtils.class);
    private static JedisPoolConfig jedisPoolConfig;
    private static final int maxTotal = 2000;
    private static final int maxIdle = 100;
    private static final int maxWaitMillis = 2000;
    private static Properties props = new Properties();
    private static final String defaultClusterNodes = "192.168.1.107:7000,192.168.1.107:7001,192.168.1.107:7002,192.168.1.107:7003,192.168.1.107:7004,192.168.1.107:7005,192.168.1.107:7006,192.168.1.107:7007,192.168.1.107:7008,192.168.1.107:7009,192.168.1.108:7000,192.168.1.108:7001,192.168.1.108:7002,192.168.1.108:7003,192.168.1.108:7004,192.168.1.108:7005,192.168.1.108:7006,192.168.1.108:7007,192.168.1.108:7008,192.168.1.108:7009,192.168.1.109:7000,192.168.1.109:7001,192.168.1.109:7002,192.168.1.109:7003,192.168.1.109:7004,192.168.1.109:7005,192.168.1.109:7006,192.168.1.109:7007,192.168.1.109:7008,192.168.1.109:7009";
    private static JedisCluster jedisCluster;
    private static final String defaultStandaloneMaster = "192.168.1.180:6379";
    private static final String defaultStandaloneSlaves = "192.168.1.180:6381";
    private static JedisPool jedisPool;
    private static ShardedJedisPool sharedJedisPool;
    private static final String defaultSentinelMaster = "mymaster";
    private static final String defaultSentinelNodes = "192.168.1.205:26379,192.168.1.204:26379,192.168.1.169:26379";
    private static JedisSentinelPool jedisSentinelPool;

    public RedisConnectionUtils() {
    }

    private static void initJedisPoolConfig() {
        LOG.info("init jedisPoolConfig...");
        jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(getIntProperty("redis.pool.maxTotal", 2000));
        jedisPoolConfig.setMaxIdle(getIntProperty("redis.pool.maxIdle", 100));
        jedisPoolConfig.setMaxWaitMillis((long) getIntProperty("redis.pool.maxWaitMillis", 2000));
        jedisPoolConfig.setTestOnBorrow(false);
        jedisPoolConfig.setTestOnReturn(false);
        LOG.info("maxTotal:" + jedisPoolConfig.getMaxTotal() + ", maxIdle:" + jedisPoolConfig.getMaxIdle() + ", maxWaitMillis:" + jedisPoolConfig.getMaxWaitMillis());
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static synchronized void initJedisStandalone() {
        if (null == jedisPool || null == sharedJedisPool) {
            Set master = getHostAndPortProperty("redis.standalone.master", "192.168.1.180:6379");
            Set slaves = getHostAndPortProperty("redis.standalone.slaves", "192.168.1.180:6381");
            LOG.info("Redis standalone shardinfo, master" + master + ", slaves" + slaves);
            HostAndPort hpmaster = (HostAndPort) master.toArray()[0];
            ArrayList shardInfoList = new ArrayList();
            Iterator jedis = slaves.iterator();

            while (jedis.hasNext()) {
                HostAndPort shardedJedis = (HostAndPort) jedis.next();
                shardInfoList.add(new JedisShardInfo(shardedJedis.getHost(), shardedJedis.getPort()));
            }

            shardInfoList.add(new JedisShardInfo(hpmaster.getHost(), hpmaster.getPort()));
            Jedis jedis1 = null;
            ShardedJedis shardedJedis1 = null;

            try {
                jedisPool = new JedisPool(jedisPoolConfig, hpmaster.getHost(), hpmaster.getPort());
                sharedJedisPool = new ShardedJedisPool(jedisPoolConfig, shardInfoList);
                jedis1 = jedisPool.getResource();
                shardedJedis1 = sharedJedisPool.getResource();
                jedis1.set("sam", "Life is short, Enjoy more!");
                LOG.info("Redis standalone echo: " + shardedJedis1.get("sam"));
            } catch (Exception var15) {
                LOG.error("Redis standalone doesn\'t response ,Please check !");
                var15.printStackTrace();
            } finally {
                try {
                    if (null != jedis1) {
                        jedis1.close();
                    }

                    if (null != shardedJedis1) {
                        shardedJedis1.close();
                    }
                } catch (Exception var14) {
                    LOG.error("Jedis Close Error! ", var14);
                }

            }
        }

    }

    public static synchronized void initJedisSentinel() {
        if (null == jedisSentinelPool) {
            Set sentinels = getSentinelProperty("redis.sentinel.nodes", "192.168.1.205:26379,192.168.1.204:26379,192.168.1.169:26379");
            String masterName = getStringProperty("redis.sentinel.master", "mymaster");
            LOG.info("Redis sentinel, master: " + masterName + ", sentinels:" + sentinels);
            Jedis jedis = null;

            try {
                jedisSentinelPool = new JedisSentinelPool(masterName, sentinels, jedisPoolConfig);
                jedis = jedisSentinelPool.getResource();
                jedis.set("han", "I\'ve got a bad feeling about this!");
                LOG.info("Redis sentinel echo: " + jedis.get("han"));
            } catch (Exception var12) {
                LOG.error("Redis sentinel doesn\'t response ,Please check !");
                var12.printStackTrace();
            } finally {
                try {
                    if (null != jedis) {
                        jedis.close();
                    }
                } catch (Exception var11) {
                    LOG.error("Jedis Close Error! ", var11);
                }

            }
        }

    }

    public static synchronized void initJedisCluster() {
        if (null == jedisCluster) {
            Set nodes = getHostAndPortProperty("redis.cluster.nodes", "192.168.1.204:7000,192.168.1.205:7000,192.168.1.180:7000");
            LOG.info("Redis cluster nodes: " + nodes);

            try {
                jedisCluster = new JedisCluster(nodes, jedisPoolConfig);
                jedisCluster.set("yoda", "May the force be with you!");
                LOG.info("Redis cluster echo: " + jedisCluster.get("yoda"));
            } catch (Exception var2) {
                LOG.error("Redis cluster doesn\'t response ,Please check !");
                var2.printStackTrace();
            }
        }

    }

    private static int getIntProperty(String key, int defaultValue) {
        int val = defaultValue;

        try {
            if (StringUtils.isNotBlank(props.getProperty(key))) {
                val = Integer.parseInt(props.getProperty(key));
            }
        } catch (Exception var4) {
            LOG.info("property is not exists or invalid ,use default[" + key + ":" + defaultValue + "]");
        }

        return val;
    }

    private static String getStringProperty(String key, String defaultValue) {
        String val = defaultValue.trim();

        try {
            if (StringUtils.isNotBlank(props.getProperty(key))) {
                val = props.getProperty(key);
            }
        } catch (Exception var4) {
            LOG.info("property is not exists or invalid ,use default[" + key + ":" + defaultValue + "]");
        }

        return val;
    }

    private static Set<HostAndPort> getHostAndPortProperty(String key, String defaultValue) {
        LinkedHashSet nodes = new LinkedHashSet();
        String hostAndPort = props.getProperty(key);

        hostAndPort = StringUtils.isEmpty(hostAndPort) ? defaultValue : hostAndPort;
        String[] hostAndPorts = hostAndPort.split(",");
        String[] arr$ = hostAndPorts;
        int len$ = hostAndPorts.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            String node = arr$[i$];
            String host = node.split(":")[0];
            int port = Integer.parseInt(node.split(":")[1]);
            nodes.add(new HostAndPort(host, port));
        }

        return nodes;
    }

    private static Set<String> getSentinelProperty(String key, String defaultValue) {
        LinkedHashSet nodes = new LinkedHashSet();
        String hostAndPort = props.getProperty(key);
        hostAndPort = StringUtils.isEmpty(hostAndPort) ? defaultValue : hostAndPort;
        String[] hostAndPorts = hostAndPort.split(",");
        String[] arr$ = hostAndPorts;
        int len$ = hostAndPorts.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            String node = arr$[i$];
            nodes.add(node);
        }

        return nodes;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static JedisPool getJedisPool() {
        return jedisPool;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static ShardedJedisPool getShardedJedisPool() {
        return sharedJedisPool;
    }

    public static JedisSentinelPool getJedisSentinelPool() {
        initJedisSentinel();
        return jedisSentinelPool;
    }

    public static JedisCluster getJedisCluster() {
        initJedisCluster();
        return jedisCluster;
    }

    static {
        try {
            props.load(RedisConnectionUtils.class.getResourceAsStream("/resources/db/redis.properties"));
        } catch (Exception var1) {
            LOG.error("can\'t find redis.properties in classpath, use default");
        }

        initJedisPoolConfig();
    }
}
