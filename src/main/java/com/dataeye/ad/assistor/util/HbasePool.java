package com.dataeye.ad.assistor.util;

import com.dataeye.util.log.DELogger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.Properties;

public class HbasePool {

    private final static Logger logger = DELogger.getLogger("hbase_client_log");
    private final static String propertiesFileName = "/resources/db/hbase.properties";

    private static String hbase_zookeeper_quorum = "dcnamenode1,dchbase1,dchbase2";
    private static String hbase_zookeeper_property_clientPort = "2181";

    private static Configuration configuration = null;

    static {
        try {
            init();
        } catch (Throwable t) {
            logger.error("HbasePool init error", t);
        }
    }

    private static void init() {
        reload();
    }

    public static Properties loadFromFile() {
        Properties properties = new Properties();
        try {
            properties.load(HbasePool.class.getResourceAsStream(propertiesFileName));
            return properties;
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("read hbase.properties in classpath error");
        }

        return null;
    }

    public static void reload() {
        Properties properties = loadFromFile();
        if (properties != null) {
            System.out.println("get hbase properties succ ");
            Object obj1 = properties.get("hbase.zookeeper.quorum");
            Object obj2 = properties.get("hbase.zookeeper.property.clientPort");
            if (obj1 == null || obj2 == null) {
                return;
            }
            String tmphbase_zookeeper_quorum = (String) obj1;
            String tmphbase_zookeeper_property_clientPort = (String) obj2;
            if (!hbase_zookeeper_quorum.equals(tmphbase_zookeeper_quorum)
                    || !hbase_zookeeper_property_clientPort.equals(tmphbase_zookeeper_property_clientPort)) {
                hbase_zookeeper_quorum = tmphbase_zookeeper_quorum;
                hbase_zookeeper_property_clientPort = tmphbase_zookeeper_property_clientPort;
            }
        }

        configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", hbase_zookeeper_quorum);
        configuration.set("hbase.zookeeper.property.clientPort", hbase_zookeeper_property_clientPort);
        // configuration.set("hbase.master", hbase_master);
        // TODO:存在多个Hbase集群或配置时，如何清除掉老的connection？？

        if (connection != null) {
            try {
                connection.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        connection = null;

    }

    private static HConnection connection = null;

    public static HConnection getConnection() throws Exception {
        if (connection == null) {
            synchronized (HbasePool.class) {
                if (connection == null) {
                    connection = HConnectionManager.createConnection(configuration);
                }
            }
        }
        return connection;
    }

    public static void close(HTableInterface table) {
        try {
            if (table != null)
                table.close();
        } catch (IOException e) {
            logger.error("HbasePool table close error", e);
        }
    }
}
