package com.dataeye.ad.assistor.util;

import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ServerException;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;

/**
 * <pre>
 * The type File utils.
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:54:00
 */
public class FileUtils {
    /**
     * 这个方法主要是用来读取放在classpath下面的文件 获取这些文件的绝对路径 比如语言文件放在classpath下面的i18n目录,
     * 传递参数i18n即可得到i18n目录的绝对路径
     *
     * @param resource
     * @return string
     * @author Stran
     * @since 2016.09.30 16:54:03
     */
    public static String getPathOfClasspathResource(String resource) {
        String path = ServerUtils.class.getResource("/").getPath();
        // 这里要根据操作系统判断
        // windows系统得到的路径形如/E:/Project/DataEyeWebPlus/WebRoot/WEB-INF/classes/
        // 要去掉前面的/
        if (ServerUtils.getOsType().equals(Constant.OsType.WINDOWS)) {
            path = path.replaceFirst("/", "");
        }
        return path + resource;
    }

    public static void downloadExcel(HttpServletResponse response, File file) throws Exception {
        downloadFile(response, Constant.ServerCfg.CONTENT_TYPE_EXCEL, file);
    }

    public static void downloadFile(HttpServletResponse response, File file, String fileName) throws Exception {
        downloadFile(response, Constant.ServerCfg.CONTENT_TYPE_STREAM, file, fileName);
    }

    private static void downloadFile(HttpServletResponse response, String contentType, File file) throws Exception {
        downloadFile(response, contentType, file, file.getName());
    }

    /**
     * 下载文件
     *
     * @param response
     * @param contentType
     * @param file
     * @param fileName
     * @throws Exception
     * @author Stran
     * @since 2016.09.30 16:54:16
     */
    private static void downloadFile(HttpServletResponse response, String contentType, File file, String fileName)
            throws Exception {
        if (file == null) {
            return;
        }
        response.setContentType(contentType);
        if (file.length() < Integer.MAX_VALUE) {
            response.setContentLength((int) file.length());
        }
        response.setContentLength((int) file.length());
        response.setHeader("Content-Disposition",
                "attachment;filename=" + URLEncoder.encode(fileName, Constant.ServerCfg.ENCODING_UTF8));
        ServletOutputStream servletOutputStream = null;
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            servletOutputStream = response.getOutputStream();
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            bos = new BufferedOutputStream(servletOutputStream);
            byte[] buff = new byte[1024];
            int bytesRead;
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
            bos.flush();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (bis != null) {
                    bis.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (servletOutputStream != null) {
                    servletOutputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (bos != null) {
                    bos.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * <pre>
     * 这个方法用来删除指定目录的文件
     *
     * @param filePath
     * @author Stran<br>
     * @date 2015年11月13日 下午2:29:40 <br>
     */
    public static void removeFile(String filePath) {
        if (filePath == null) {
            System.out.println("filePath参数为null");
            return;
        }

        File file = new File(filePath);
        try {
            // 文件或目录不存在退出
            if (!file.exists()) {
                return;
            }
            // 如果是文件,进行删除并退出
            if (file.isFile()) {
                file.delete();
                return;
            }

            // 如果目录中有文件递归删除文件
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                removeFile(files[i].getAbsolutePath());
            }

            // 删除目录
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Save multipart file.
     *
     * @param multipartFile
     * @return string
     * @throws ServerException
     * @author Stran
     * @since 2016.07.13 14:40:27
     */
    public static String saveMultipartFile(MultipartFile multipartFile, String fileDir) throws ServerException {
        File folder = new File(fileDir);
        if (!folder.exists() && !folder.isDirectory()) {
            folder.mkdirs();
        }

        String saveName = null;
        try {
            byte[] digest = MessageDigest.getInstance("MD5").digest(multipartFile.getBytes());
            String hashStr = new BigInteger(1, digest).toString(16);
            String uploadName = multipartFile.getOriginalFilename();
            String suffix = uploadName.substring(uploadName.lastIndexOf("."));
            saveName = hashStr + "." + Math.abs((long) (uploadName.hashCode() % 10000)) + suffix;
            File saveFile = new File(fileDir + saveName);
            multipartFile.transferTo(saveFile);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServerException(StatusCode.COMM_SEVER_ERROR);
        }

        return saveName;
    }
}
