package com.dataeye.ad.assistor.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.json.JSONArray;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;

import com.xunlei.util.Log;

/**
 * HTTP GET/POST方式请求
 *
 * @author kyle
 */
public class HttpRequest {

    private static final Logger logger = Log.getLogger("sync_qile_log");


    /**
     * 向指定URL发送GET方法的请求
     *
     * @param url    发送请求的URL
     * @param params 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     */
    public static String get(String url, String params) {
        StringBuilder result = new StringBuilder();
        BufferedReader in = null;
        try {
            String urlNameString = url + "?" + params;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(), "utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result.toString();
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url    发送请求的 URL
     * @param params 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String post(String url, String params) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = null;
        try {
            logger.debug("HTTP REQUEST: URL=[{}]", url.concat("&").concat(params));
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(params);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "utf-8"));
            String lines;
            StringBuffer sb = new StringBuffer("");
            while ((lines = in.readLine()) != null) {
                sb.append(lines);
            }
//            System.out.println(sb);
            result = sb.toString();

        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！" + e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url    发送请求的 URL
     * @param params 请求参数
     * @return 所代表远程资源的响应结果
     */
    public static String post(String url, Map<String, String> params) {
        DefaultHttpClient httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager());
        httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 60000);
        httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 60000);
        httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BEST_MATCH);

        HttpPost httpPost = new HttpPost(url);

        List<NameValuePair> httpParams = new ArrayList<NameValuePair>();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            httpParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }
        logger.debug("HTTP REQUEST: URL=[{}], PARAMS={}", new Object[]{url, httpParams});
        String httpResult = null;
        try {
            // 设置参数
            httpPost.setEntity(new UrlEncodedFormEntity(httpParams, "UTF-8"));

            // 发送请求
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();

            if (httpEntity != null) {
                byte[] bytes = EntityUtils.toByteArray(httpEntity);
                httpResult = new String(bytes, "utf-8");
//				System.out.println(httpResult);
            }
        } catch (Exception e) {
            logger.error("HTTP REQUEST ERROR: URL=[{}], PARAMS={}", new Object[]{url, httpParams});
            e.printStackTrace();
        }
        return httpResult;
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url    发送请求的 URL
     * @param params 请求参数
     * @return 所代表远程资源的响应结果
     */
    public static String postFile(String url, Map<String, ContentBody> params) {
        DefaultHttpClient httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager());
        httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 60000);
        httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 60000);
        httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BEST_MATCH);

        HttpPost httpPost = new HttpPost(url);

		/*List<NameValuePair> httpParams = new ArrayList<NameValuePair>();
        for (Map.Entry<String, String> entry : params.entrySet()) {
			httpParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
		}*/
        String httpResult = null;
        try {
            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, "--------------------HV2ymHFg03ehbqgZCaKO6jyH", Charset.defaultCharset());
            for (Map.Entry<String, ContentBody> entry : params.entrySet()) {
                multipartEntity.addPart(entry.getKey(), entry.getValue());
            }
            logger.debug("HTTP REQUEST: URL=[{}], PARAMS={}", new Object[]{url, multipartEntity});


            // 设置参数
            httpPost.setEntity(multipartEntity);
            httpPost.addHeader("Content-Type", "multipart/form-data; boundary=--------------------HV2ymHFg03ehbqgZCaKO6jyH");
            // 发送请求
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();

            if (httpEntity != null) {
                byte[] bytes = EntityUtils.toByteArray(httpEntity);
                httpResult = new String(bytes, "utf-8");
//				System.out.println(httpResult);
            }
        } catch (Exception e) {
            logger.error("HTTP REQUEST ERROR: URL=[{}], PARAMS={}", new Object[]{url, params});
            e.printStackTrace();
        }
        return httpResult;
    }

//    private static String genChannelInterfaceParam(Map<String, String> params) throws UnsupportedEncodingException {
//    	StringBuilder sb = new StringBuilder("");
//    	sb.append("signature=").append(URLEncoder.encode("939d0a08fc397b1a152d9257f494224c0daae6b3","UTF-8"));
//    	sb.append("&timestamp=").append(URLEncoder.encode("1484302622","UTF-8"));
//        params.put("signature", "939d0a08fc397b1a152d9257f494224c0daae6b3");
//        params.put("timestamp", "1484302622");
//    	return sb.toString();
//    }
//    
//    private static String genDeliveryInterfaceParam(Map<String, String> params) throws UnsupportedEncodingException {
//    	
//    	StringBuilder sb = new StringBuilder("");
//    	sb.append("signature=").append(URLEncoder.encode("939d0a08fc397b1a152d9257f494224c0daae6b3","UTF-8"));
//    	sb.append("&timestamp=").append(URLEncoder.encode("1484302622","UTF-8"));
//    	sb.append("&channel_id=").append(URLEncoder.encode("507","UTF-8"));
//    	sb.append("&is_time=").append(URLEncoder.encode("1481472000","UTF-8"));
//    	sb.append("&ie_time=").append(URLEncoder.encode("1581472000","UTF-8"));
//    	sb.append("&contain=").append(URLEncoder.encode("0","UTF-8"));
//        params.put("signature", "939d0a08fc397b1a152d9257f494224c0daae6b3");
//        params.put("timestamp", "1484302622");
//        params.put("channel_id", "507");
//        params.put("is_time", "1484302622");
//        params.put("ie_time", "1581472000");
//        params.put("contain", "0");
//    	return sb.toString();
//    }

    private static String genPaybackInterfaceParam(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder("");
        sb.append("signature=").append(URLEncoder.encode("939d0a08fc397b1a152d9257f494224c0daae6b3", "UTF-8"));
        sb.append("&timestamp=").append(URLEncoder.encode("1484302622", "UTF-8"));
        sb.append("&channel_id=").append(URLEncoder.encode("507", "UTF-8"));
        sb.append("&cb_time=").append(URLEncoder.encode("1468339200", "UTF-8"));
        sb.append("&ce_time=").append(URLEncoder.encode("1484236800", "UTF-8"));
        sb.append("&is_time=").append(URLEncoder.encode("1468339200", "UTF-8"));
        sb.append("&ie_time=").append(URLEncoder.encode("1484236800", "UTF-8"));
        sb.append("&contain=").append(URLEncoder.encode("1", "UTF-8"));
        params.put("signature", "939d0a08fc397b1a152d9257f494224c0daae6b3");
        params.put("timestamp", "1484302622");
        params.put("channel_id", "507");
        params.put("cb_time", "1468339200");
        params.put("ce_time", "1484236800");
        params.put("is_time", "1468339200");
        params.put("ie_time", "1484236800");
        params.put("contain", "1");
        return sb.toString();
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        String urlPath = null;
        String params = null;
        Map<String, String> map = new HashMap<String, String>();
//    	urlPath = new String("http://twzw.center.gzyouai.com/qile/server/longYu.php?p=1"); 
//    	params = genChannelInterfaceParam();
        urlPath = new String("http://twzw.center.gzyouai.com/qile/server/longYu.php?p=2");
        params = genPaybackInterfaceParam(map);
//    	urlPath = new String("http://twzw.center.gzyouai.com/qile/server/longYu.php?p=3"); 
//    	params = genDeliveryInterfaceParam();

        String result = null;
//        result = sendPost(urlPath, params);
        result = post(urlPath, map);

        System.out.println(result);

        JSONArray jsonArray = JSONArray.fromObject(result);

        List<Map<String, Object>> mapListJson = (List) jsonArray;
        for (int i = 0; i < mapListJson.size(); i++) {
            Map<String, Object> obj = mapListJson.get(i);
            System.out.println("Map<Index>  --------------------  :" + (i + 1));
            for (Entry<String, Object> entry : obj.entrySet()) {
                String strkey1 = entry.getKey();
                Object strval1 = entry.getValue();
                System.out.println("KEY:" + strkey1 + "  -->  Value:" + strval1);
            }
        }
//        Channel[] jb = json2Object(result);
//        System.out.println(mapListJson);
    }
}


class Channel {
    private String channelId;
    private String channelName;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    @Override
    public String toString() {
        return "Channel [channelId=" + channelId + ", channelName="
                + channelName + "]";
    }

}