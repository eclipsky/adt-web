package com.dataeye.ad.assistor.util;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.constant.Database.DBField;

/**
 * <pre>
 *
 * @author Stran <br>
 * @create 2016.12.02 14:46 <br>
 * @version 1.0 <br>
 */
public class SqlUtils {

    public static final String ASC = "ASC";
    public static final String DESC = "DESC";

    public static String getAndIn(String dbField, String values) {
        return " AND " + dbField + " IN(" + values + ")";
    }

    public static String getLimit(PageData pageData) {
        return getLimit((pageData.getPageId() - 1) * pageData.getPageSize(), pageData.getPageSize());
    }

    public static String getOrderBy(String orderBy, String order) {
        String ret;
        switch (orderBy) {
            case "impNum":
                ret = DBField.EXPOSURE_NUM;
                break;
            case "ctr":
                ret = DBField.CLICK_RATE;
                break;
            case "createDate":
                ret = DBField.CREATE_TIME;
                break;
            default:
                ret = orderBy;
        }

        return " ORDER BY " + ret + getOrder(order);
    }

    private static String getLimit(int rowCount, int offset) {
        return " LIMIT " + rowCount + "," + offset;
    }

    private static String getOrder(String order) {
        if (order.equalsIgnoreCase(ASC)) {
            return " " + ASC;
        }

        return " " + DESC;
    }


}
