package com.dataeye.ad.assistor.util;

import com.dataeye.ad.assistor.module.realtimedelivery.model.*;

import java.util.*;

/**
 * Created by huangzehai on 2017/7/5.
 */
public final class TimeIndicatorUtils {
    private TimeIndicatorUtils() {

    }

    /**
     * 联结指定时段的计划、落地页、包统计数据,返回计划的完整时段统计报告
     *
     * @param planIndicators
     * @param pageStats
     * @param packageStats
     * @return {Plan ID -> RealTimeReport}
     */
    public static List<RealTimeReport> join(List<RealTimeReport> planIndicators, List<PageStat> pageStats,  List<PackageStat> packageStats) {
        //分别根据plan id,page id, package id分组, 以便后面联结。

        Map<Integer, PageStat> pageReports = groupByPageId(pageStats);
//        Map<Integer, TrackingStat> trackingReports = groupTrackingByPageId(trackingStats);
        Map<Integer, PackageStat> packageReports = groupByPackageId(packageStats);

        //联结计划、落地页、包统计数据.
        for (RealTimeReport planIndicator : planIndicators) {
            //获取报告的落地页ID和包ID
            Integer pageId = planIndicator.getPageId();
            Integer packageId = planIndicator.getPackageId();

            if (pageId != null && pageReports != null) {
                PageStat pageIndicator = pageReports.get(pageId);
                if (pageIndicator != null) {
                    planIndicator.setReaches(pageIndicator.getReaches());
                    planIndicator.setDownloads(pageIndicator.getDownloads());
                }
            }

            if (packageId != null && packageReports != null) {
                PackageStat packageIndicator = packageReports.get(packageId);
                if (packageIndicator != null) {
                    planIndicator.setFirstDayRegistrations(packageIndicator.getFirstDayRegistrations());
                    planIndicator.setActivation(packageIndicator.getActivations());
                }
            }
        }
        return planIndicators;
    }


    /**
     * 按落地页ID分组
     *
     * @param pageIndicators
     * @return {page id -> page indicator}
     */
    private static Map<Integer, PageStat> groupByPageId(List<PageStat> pageIndicators) {
        if (pageIndicators == null) {
            return null;
        }
        //根据落地页Id分组.
        Map<Integer, PageStat> indicatorsByPageId = new HashMap<>();
        for (PageStat indicator : pageIndicators) {
            indicatorsByPageId.put(indicator.getPageId(), indicator);
        }
        return indicatorsByPageId;
    }

    /**
     * 按包ID分组
     *
     * @param packageIndicators
     * @return
     */
    private static Map<Integer, PackageStat> groupByPackageId(List<PackageStat> packageIndicators) {
        if (packageIndicators == null) {
            return null;
        }
        //根据落地页Id分组.
        Map<Integer, PackageStat> indicatorsByPkgId = new HashMap<>();
        for (PackageStat indicator : packageIndicators) {
            indicatorsByPkgId.put(indicator.getPackageId(), indicator);
        }
        return indicatorsByPkgId;
    }
}
