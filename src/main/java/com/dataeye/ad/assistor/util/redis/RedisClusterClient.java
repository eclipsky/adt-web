//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.dataeye.ad.assistor.util.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;


public class RedisClusterClient implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(RedisClusterClient.class);
    private static JedisCluster jedisCluster = RedisConnectionUtils.getJedisCluster();
    private static RedisClusterClient redisClusterClient = new RedisClusterClient();
    private ConcurrentHashMap<String, AtomicLong> costMap = new ConcurrentHashMap();
    private ConcurrentHashMap<String, AtomicLong> timesMap = new ConcurrentHashMap();

    private RedisClusterClient() {
    }

    public static RedisClusterClient getInstance() {
        return redisClusterClient;
    }
    
    // == add by stan zhang 20170220
    public boolean setbit(String key, long offset, boolean value){
    	return jedisCluster.setbit(key, offset, value);
    }
    
    public boolean setbit(String key, long offset, boolean value, int ttl, boolean asyncExpire){
    	boolean result = jedisCluster.setbit(key, offset, value);
    	if(asyncExpire) {
            RedisExpireHelper.expireKey(key, ttl);
        } else {
            this.expire(key, ttl);
        }
    	return result;
    }
    
    public boolean getbit(String key, long offset){
    	return jedisCluster.getbit(key, offset);
    }
    
    /**
	 * 获取所有的keys
	 * @param pattern
	 * @return
	 */
	public TreeSet<String> keys(String pattern){
		TreeSet<String> keys = new TreeSet<String>();
		Map<String, JedisPool> clusterNodes = jedisCluster.getClusterNodes();
		for(String k : clusterNodes.keySet()){
			System.out.println("Getting keys from: "+k);
			//JedisPool jp = clusterNodes.get(k); 
			String[] kArray = k.split(":");
			String host = kArray[0];
			String port = kArray[1];
			Jedis jedis = new Jedis(host, Integer.valueOf(port), 100000);
			//Jedis connection = jp.getResource();  
			try {  
				keys.addAll(jedis.keys(pattern));  
			} catch(Exception e){
				System.out.println("Getting keys error: "+e);
			} finally{  
				System.out.println("Connection closed.");
				jedis.close();//用完一定要close这个链接！！！  
			}  
		}  
		System.out.println("Keys gotten!");
		return keys; 
	}

    public String get(String key) {
        return jedisCluster.get(key);
    }

    public String get(String key, boolean fromMaster) {
        return fromMaster?jedisCluster.get(key):this.get(key);
    }
    
	public String lpop(String key){
		return jedisCluster.lpop(key);
	}

    public Long setnx(String key, String value) {
        return jedisCluster.setnx(key, value);
    }

    public Long setnx(String key, String value, int seconds) {
        long result = jedisCluster.setnx(key, value).longValue();
        if(result == 1L) {
            jedisCluster.expire(key, seconds);
        }

        return Long.valueOf(result);
    }

    public void set(String key, String value) {
        jedisCluster.set(key, value);
    }
    
    public void set(String key, String value, int ttl, boolean asyncExpire){
    	jedisCluster.set(key, value);
    	if(asyncExpire) {
            RedisExpireHelper.expireKey(key, ttl);
        } else {
            this.expire(key, ttl);
        }
    }
    
    public void set(String key, String value, int seconds) {
        jedisCluster.set(key, value);
        jedisCluster.expire(key, seconds);
    }

    public Long del(String key) {
        return jedisCluster.del(key);
    }

    public Boolean lpush(String key, String... strings) {
        String[] arr$ = strings;
        int len$ = strings.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            String string = arr$[i$];
            jedisCluster.lpush(key, new String[]{string});
        }

        return Boolean.valueOf(true);
    }

    public Boolean rpush(String key, String... strings) {
        String[] arr$ = strings;
        int len$ = strings.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            String string = arr$[i$];
            jedisCluster.rpush(key, new String[]{string});
        }

        return Boolean.valueOf(true);
    }

    public List<String> lrange(String key, long start, long end) {
        LOG.debug("return the specified elements of the list stored at the specified key. key=" + key);
        return jedisCluster.lrange(key, start, end);
    }

    public Long llen(String key) {
        return jedisCluster.llen(key);
    }

    public long sadd(String key, String member) {
        long begin = System.currentTimeMillis();
        long result = jedisCluster.sadd(key, new String[]{member}).longValue();
        this.record("sadd", System.currentTimeMillis() - begin);
        return result;
    }

    public long sadd(String key, String member, int ttl) {
        long result = this.sadd(key, member);
        this.expire(key, ttl);
        return result;
    }

    public long sadd(String key, String member, int ttl, boolean asyncExpire) {
        long result = this.sadd(key, member);
        if(asyncExpire) {
            RedisExpireHelper.expireKey(key, ttl);
        } else {
            this.expire(key, ttl);
        }

        return result;
    }

    public long sadd(String key, String... member) {
        long begin = System.currentTimeMillis();
        long result = jedisCluster.sadd(key, member).longValue();
        this.record("sadd", System.currentTimeMillis() - begin);
        return result;
    }

    public long sadd(String key, int ttl, String... member) {
        long result = this.sadd(key, member);
        this.expire(key, ttl);
        return result;
    }

    public long sadd(String key, int ttl, boolean asyncExpire, String... member) {
        long result = this.sadd(key, member);
        if(asyncExpire) {
            RedisExpireHelper.expireKey(key, ttl);
        } else {
            this.expire(key, ttl);
        }

        return result;
    }

    public Boolean sismember(String key, String member) {
        return jedisCluster.sismember(key, member);
    }

    public Boolean sismember(String key, String member, boolean fromMaster) {
        return jedisCluster.sismember(key, member);
    }

    public Set<String> smembers(String key) {
        return jedisCluster.smembers(key);
    }

    public Long srem(String key, String... members) {
        return jedisCluster.srem(key, members);
    }

    public Long scard(String key) {
        return jedisCluster.scard(key);
    }

    public boolean hset(String key, String field, String value) {
        long result = jedisCluster.hset(key, field, value).longValue();
        return result == 1L;
    }
    
    public boolean hset(String key, String field, String value, int ttl, boolean asyncExpire){
    	long result = jedisCluster.hset(key, field, value).longValue();
    	if(asyncExpire) {
            RedisExpireHelper.expireKey(key, ttl);
        } else {
            this.expire(key, ttl);
        }
        return result == 1L;
    }
    
    public String hget(String key, String field) {
        return jedisCluster.hget(key, field);
    }
    
    public boolean hexists(String key, String field){
    	return jedisCluster.hexists(key, field);
    }

    public String hget(String key, String field, boolean fromMaster) {
        return fromMaster?jedisCluster.hget(key, field):this.hget(key, field);
    }

    public Long hdel(String key, String field) {
        return jedisCluster.hdel(key, new String[]{field});
    }

    public Long hlen(String key) {
        return jedisCluster.hlen(key);
    }

    public Map<String, String> hgetAll(String key) {
        return jedisCluster.hgetAll(key);
    }

    public List<String> hmget(String key, String... fields) {
        return jedisCluster.hmget(key, fields);
    }

    public String hmset(String key, Map<String, String> hash) {
        return jedisCluster.hmset(key, hash);
    }
    public Long hincrBy(String key, String field, int value) {
        long begin = System.currentTimeMillis();
        long result = jedisCluster.hincrBy(key, field, (long)value).longValue();
        this.record("hincrBy", System.currentTimeMillis() - begin);
        return Long.valueOf(result);
    }

    public Long hincrBy(String key, String field, int value, int ttl) {
        long count = this.hincrBy(key, field, value).longValue();
        this.expire(key, ttl);
        return Long.valueOf(count);
    }

    public Long hincrBy(String key, String field, int value, int ttl, boolean asyncExpire) {
        long count = this.hincrBy(key, field, value).longValue();
        if(asyncExpire) {
            RedisExpireHelper.expireKey(key, ttl);
        } else {
            this.expire(key, ttl);
        }

        return Long.valueOf(count);
    }

    public Long incr(String key) {
        return jedisCluster.incr(key);
    }

    public Long incrby(String key, long increment) {
        return jedisCluster.incrBy(key, increment);
    }

    public Long incrBy(String key, long increment) {
        return jedisCluster.incrBy(key, increment);
    }

    public Double incrByFloat(String key, double increment) {
        return jedisCluster.incrByFloat(key, increment);
    }

    public Long expire(String key, int seconds) {
        long result = jedisCluster.expire(key, seconds).longValue();
        return Long.valueOf(result);
    }

    public Long expireAt(String key, long unixTime) {
        return jedisCluster.expireAt(key, unixTime);
    }

    public Map<String, JedisPool> getClusterNodes() {
        return jedisCluster.getClusterNodes();
    }

    public void record(String command, long milliseconds) {
        AtomicLong times = (AtomicLong)this.timesMap.get(command);
        AtomicLong cost = (AtomicLong)this.costMap.get(command);
        if(null == times) {
            times = new AtomicLong(0L);
        }

        if(null == cost) {
            cost = new AtomicLong(0L);
        }

        long totalTimes = times.incrementAndGet();
        long totalCost = cost.addAndGet(milliseconds);
        this.timesMap.put(command, times);
        this.costMap.put(command, cost);
        if(totalTimes % 10000L == 0L) {
            LOG.info(">>>>>>>>>>redis stats, command:" + command + ", 10000 commands cost: " + totalCost + " ms");
            times.set(0L);
            cost.set(0L);
        }

    }
}
