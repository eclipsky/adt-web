package com.dataeye.ad.assistor.util;

import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.module.report.constant.Constants;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by huangzehai on 2017/4/27.
 */
public final class CurrencyUtils {
    /**
     * 单位万.
     */
    private static final String TEN_THOUSAND_UNIT = "万";

    /**
     * 万
     */
    private static final int TEN_THOUSAND = 10000;

    /**
     * 百
     */
    private static final int HUNDRED = 100;

    private CurrencyUtils() {

    }

    /**
     * 将分转化为元.
     *
     * @param fen
     * @return
     */
    public static final BigDecimal fenToYuan(BigDecimal fen) {
        if (fen == null) {
            return fen;
        }
        return fen.divide(new BigDecimal(HUNDRED), 2, RoundingMode.HALF_UP);
    }

    /**
     * 将元转化为分.
     *
     * @param yuan
     * @return
     */
    public static final BigDecimal yuanToFen(BigDecimal yuan) {
        if (yuan == null) {
            return yuan;
        }
        return yuan.multiply(new BigDecimal(HUNDRED)).setScale(0, RoundingMode.HALF_UP);
    }

    /**
     * 将元转化为文本形式，保留两位小数，四舍五入.
     *
     * @param yuan
     * @return
     */
    public static final String yuanToText(BigDecimal yuan) {
        if (yuan == null) {
            return Labels.UNKNOWN;
        }
        return yuan.setScale(2, RoundingMode.HALF_UP).toString();
    }

    /**
     * 将大于等于一万的金额转化为以万为单位.
     *
     * @param money
     * @return
     */
    public static String yuanToWan(BigDecimal money) {
        if (money == null) {
            return Constants.UNKNOWN;
        } else if (money.compareTo(new BigDecimal(TEN_THOUSAND)) >= 0) {
            return money.divide(new BigDecimal(TEN_THOUSAND)).setScale(2, RoundingMode.HALF_UP).toString() + TEN_THOUSAND_UNIT;
        } else {
            return money.toString();
        }
    }
}
