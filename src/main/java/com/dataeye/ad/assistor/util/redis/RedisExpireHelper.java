//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.dataeye.ad.assistor.util.redis;

import com.xunlei.util.Log;
import org.slf4j.Logger;
import redis.clients.jedis.JedisPool;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class RedisExpireHelper {
    private static final Logger LOG = Log.getLogger(RedisExpireHelper.class);
    private static ConcurrentHashMap<String, Integer> expireClusterMap = new ConcurrentHashMap();
    /** @deprecated */
    @Deprecated
    private static ConcurrentHashMap<String, Integer> expireMap = new ConcurrentHashMap();

    public RedisExpireHelper() {
    }

    public static void expireKey(String key, int ttl) {
        expireClusterMap.put(key, Integer.valueOf(ttl));
    }

    /** @deprecated */
    @Deprecated
    public static void expireKey(String key, int ttl, boolean isCluster) {
        if(isCluster) {
            expireClusterMap.put(key, Integer.valueOf(ttl));
        } else {
            expireMap.put(key, Integer.valueOf(ttl));
        }

    }

    public static int getSleepTime(int mapSize) {
        return mapSize < 1000?300:(mapSize < 10000?120:60);
    }

    static {
        (new RedisExpireHelper.RedisKeyExpirer()).start();
    }

    static class RedisKeyExpirer extends Thread {
        RedisClusterClient redisClient = RedisClusterClient.getInstance();
        Set<String> toDelKeys = new HashSet();

        RedisKeyExpirer() {
        }

        public void run() {
            while(true) {
                try {
                    int t = RedisExpireHelper.expireClusterMap.size();
                    RedisExpireHelper.LOG.info("expireClusterMap size-->" + t);
                    long sleepTime = (long) RedisExpireHelper.getSleepTime(t);
                    Iterator i$ = RedisExpireHelper.expireClusterMap.entrySet().iterator();

                    while(i$.hasNext()) {
                        Entry expiredKeys = (Entry)i$.next();
                        this.redisClient.expire((String)expiredKeys.getKey(), ((Integer)expiredKeys.getValue()).intValue());
                        this.toDelKeys.add((String)expiredKeys.getKey());
                    }

                    i$ = this.toDelKeys.iterator();

                    while(i$.hasNext()) {
                        String expiredKeys1 = (String)i$.next();
                        RedisExpireHelper.expireClusterMap.remove(expiredKeys1);
                    }

                    this.toDelKeys.clear();
                    RedisExpireHelper.LOG.info("Finish expire all the keys in expireClusterMap, sleep for " + sleepTime + "s");
                    
                    //查看redis-cluster的连接数
                    Map<String, JedisPool> poolMap = redisClient.getClusterNodes();
                    for(String host : poolMap.keySet()){
                    	JedisPool thisPool = poolMap.get(host);
                    	LOG.info("ReidsPoolInfo-->host:"+host+",numActive:"+thisPool.getNumActive()+",numIdle:"+thisPool.getNumIdle()+",numWaiters:"+thisPool.getNumWaiters());
                    }
                    
                    Thread.sleep(sleepTime * 1000L);
                } catch (Throwable var6) {
                    RedisExpireHelper.LOG.error("expireing key faileds", var6);
                }
            }
        }
    }
}
