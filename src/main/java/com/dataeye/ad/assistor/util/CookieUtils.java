package com.dataeye.ad.assistor.util;

import com.xunlei.util.StringTools;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by huangzehai on 2017/5/12.
 */
public final class CookieUtils {
    private CookieUtils() {

    }

    public static Map<String, String> parse(String cookies) {
        Map<String, String> cookieMap = new HashMap<>();
        String[] cookieArr = cookies.split(";");
        for (String cookie : cookieArr) {
            String[] kv = cookie.split("=");
            String key = kv[0].trim();
            String value = "";
            if (kv.length == 2) {
                value = kv[1].trim();
            }
            cookieMap.put(key, value);
        }
        return cookieMap;
    }

    /**
     * 获取Cookie的某个属性
     *
     * @param cookie
     * @param name
     * @return
     */
    public static String get(String cookie, String name) {
        if (StringUtils.isBlank(cookie) || StringUtils.isBlank(name)) {
            return null;
        }
        return CookieUtils.parse(cookie).get(name);
    }

    /**
     *
     * <pre>
     * 从request中解析出cookie
     *  @param request
     *  @param cookieMap
     *  @author Ivan<br>
     *  @date 2015年3月14日 下午5:06:25
     * <br>
     */
    public static void parseCookie(HttpServletRequest request, Map<String, Cookie> cookieMap) {
        String cookieString = request.getHeader("Cookie");
        if (StringTools.isNotEmpty(cookieString)) {
            CookieDecoder.decode(cookieString, cookieMap);
        }
    }

    /**
     *
     * <pre>
     * 解析cookie值
     *  @param request
     *  @param key
     *  @return
     *  @author Ivan<br>
     *  @date 2015年3月14日 下午5:10:54
     * <br>
     */
    public static String getCookieValue(HttpServletRequest request, String key) {
        Map<String, Cookie> cookieMap = new HashMap<String, Cookie>();
        parseCookie(request, cookieMap);
        Cookie cookie = cookieMap.get(key);
        if (cookie != null) {
            return cookie.getValue();
        }
        return null;
    }

}
