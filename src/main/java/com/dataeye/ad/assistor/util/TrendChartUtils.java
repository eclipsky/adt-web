package com.dataeye.ad.assistor.util;

import com.dataeye.ad.assistor.common.Chart;
import com.dataeye.ad.assistor.common.DailyRecord;
import com.dataeye.ad.assistor.common.TrendChart;
import com.dataeye.ad.assistor.common.WeeklyRecord;
import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.module.report.model.Period;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by huangzehai on 2017/4/27.
 */
public final class TrendChartUtils {
    private static final int LINES_LIMIT = 3;
    private static final String TIME_PATTERN = "HH:mm";

    private TrendChartUtils() {

    }

    /**
     * 转化为趋势图
     *
     * @param startDate
     * @param endDate
     * @param indicators
     * @param defaultValue
     * @param <V>
     * @return
     */
    public static <V> TrendChart<String, V> toTrendChart(Date startDate, Date endDate, List<DailyRecord<String, V>> indicators, V defaultValue) {
        return toTrendChart(startDate, endDate, Period.Day, indicators, defaultValue, false, false);
    }

    /**
     * 计算比率时段趋势图的
     *
     * @param chart1
     * @param chart2
     * @return
     */
    public static TrendChart<String, Double> toPeriodRateTrendChart(TrendChart<String, Integer> chart1, TrendChart<String, Integer> chart2) {
        Map<String, Chart<String, Integer>> indicatorsByTitle2 = groupByTitle(chart2.getIndicators());
        //比率趋势图
        TrendChart<String, Double> trendChart = new TrendChart<>();
        //设置比率趋势图的X轴
        trendChart.setDates(chart1.getDates());
        //比率Y轴列表
        List<Chart<String, Double>> indicators = new ArrayList<>();
        for (Chart<String, Integer> indicator1 : chart1.getIndicators()) {
            if (indicatorsByTitle2.containsKey(indicator1.getTitle())) {
                Chart indicator2 = indicatorsByTitle2.get(indicator1.getTitle());
                //构建比率单条Y轴
                Chart indicator = new Chart();
                indicator.setTitle(indicator1.getTitle());
                indicator.setPercent(indicator1.isPercent());
                indicator.setValues(divide(indicator1.getValues(), indicator2.getValues()));
                indicators.add(indicator);
            }
        }
        trendChart.setIndicators(indicators);
        return trendChart;
    }

    /**
     * 计算CPA时段趋势图
     *
     * @param chart1
     * @param chart2
     * @return
     */
    public static TrendChart<String, BigDecimal> toPeriodCpaTrendChart(TrendChart<String, BigDecimal> chart1, TrendChart<String, Integer> chart2) {
        Map<String, Chart<String, Integer>> indicatorsByTitle2 = groupByTitle(chart2.getIndicators());
        //比率趋势图
        TrendChart<String, BigDecimal> trendChart = new TrendChart<>();
        //设置比率趋势图的X轴
        trendChart.setDates(chart1.getDates());
        //比率Y轴列表
        List<Chart<String, BigDecimal>> indicators = new ArrayList<>();
        for (Chart<String, BigDecimal> indicator1 : chart1.getIndicators()) {
            if (indicatorsByTitle2.containsKey(indicator1.getTitle())) {
                Chart indicator2 = indicatorsByTitle2.get(indicator1.getTitle());
                //构建比率单条Y轴
                Chart<String, BigDecimal> indicator = new Chart();
                indicator.setTitle(indicator1.getTitle());
                indicator.setPercent(indicator1.isPercent());
                indicator.setValues(cpa(indicator1.getValues(), indicator2.getValues()));
                indicators.add(indicator);
            }
        }
        trendChart.setIndicators(indicators);
        return trendChart;
    }

    /**
     * 计算CTR时段趋势图
     *
     * @param chart1
     * @param chart2
     * @return
     */
    public static TrendChart<String, Double> toPeriodCtrTrendChart(TrendChart<String, Integer> chart1, TrendChart<String, Integer> chart2) {
        Map<String, Chart<String, Integer>> indicatorsByTitle2 = groupByTitle(chart2.getIndicators());
        //比率趋势图
        TrendChart<String, Double> trendChart = new TrendChart<>();
        //设置比率趋势图的X轴
        trendChart.setDates(chart1.getDates());
        //比率Y轴列表
        List<Chart<String, Double>> indicators = new ArrayList<>();
        for (Chart<String, Integer> indicator1 : chart1.getIndicators()) {
            if (indicatorsByTitle2.containsKey(indicator1.getTitle())) {
                Chart indicator2 = indicatorsByTitle2.get(indicator1.getTitle());
                //构建比率单条Y轴
                Chart<String, Double> indicator = new Chart();
                indicator.setTitle(indicator1.getTitle());
                indicator.setPercent(indicator1.isPercent());
                indicator.setValues(ctr(indicator1.getValues(), indicator2.getValues()));
                indicators.add(indicator);
            }
        }
        trendChart.setIndicators(indicators);
        return trendChart;
    }

    /**
     * 列表对应元素相除
     *
     * @param values1
     * @param values2
     * @return
     */
    private static List<Double> divide(List<Integer> values1, List<Integer> values2) {
        if (values1 == null || values2 == null) {
            return null;
        }
        //value1和values2 size相等
        List<Double> values = new ArrayList<>();
        for (int i = 0; i < values1.size(); i++) {
            Integer value1 = values1.get(i);
            Integer value2 = values2.get(i);
            if (value1 != null && value2 != null && value2 != 0) {
                values.add((double) value1 / value2);
            } else {
                values.add(0.0);
            }
        }
        return values;
    }

    /**
     * 列表对应元素相除
     *
     * @param values1
     * @param values2
     * @return
     */
    private static List<BigDecimal> cpa(List<BigDecimal> values1, List<Integer> values2) {
        if (values1 == null || values2 == null) {
            return null;
        }
        //value1和values2 size相等
        List<BigDecimal> values = new ArrayList<>();
        for (int i = 0; i < values1.size(); i++) {
            BigDecimal value1 = values1.get(i);
            Integer value2 = values2.get(i);
            if (value1 != null && value2 != null && value2 != 0) {
                values.add(NumberUtils.cpa(value1, value2));
            } else {
                values.add(new BigDecimal(0));
            }
        }
        return values;
    }

    /**
     * 列表对应元素相除
     *
     * @param values1
     * @param values2
     * @return
     */
    private static List<Double> ctr(List<Integer> values1, List<Integer> values2) {
        if (values1 == null || values2 == null) {
            return null;
        }
        //value1和values2 size相等
        List<Double> values = new ArrayList<>();
        for (int i = 0; i < values1.size(); i++) {
            Integer value1 = values1.get(i);
            Integer value2 = values2.get(i);
            if (value1 != null && value2 != null && value2 != 0) {
                values.add(NumberUtils.rate(value1, value2));
            } else {
                values.add(0.0);
            }
        }
        return values;
    }


    /**
     * 按标题分组
     *
     * @param charts
     * @param <V>
     * @return
     */
    private static <V> Map<String, Chart<String, V>> groupByTitle(List<Chart<String, V>> charts) {
        Map<String, Chart<String, V>> map = new HashMap<>();
        for (Chart<String, V> chart : charts) {
            map.put(chart.getTitle(), chart);
        }
        return map;
    }

    /**
     * 转化为趋势图
     *
     * @param startDate      起始日期
     * @param endDate        截止日期
     * @param period         时段
     * @param indicators     指标列表
     * @param defaultValue   缺失点默认值
     * @param isPercent      Y轴的值是否要显示为百分比，供前端使用
     * @param isDefaultChart 是否是默认的无过滤条件的趋势图
     * @return
     */
    public static <V> TrendChart<String, V> toTrendChart(Date startDate, Date endDate, Period period, List<? extends DailyRecord<String, V>> indicators, V defaultValue, boolean isPercent, boolean isDefaultChart) {
        if (indicators == null) {
            return null;
        }
        //初始化周日期
        if (period == Period.Week) {
            initDateForWeek(indicators);
        }

        //按名称分组
        Map<String, List<DailyRecord<String, V>>> indicatorsByName = groupByName(indicators);

        //构建趋势图对象
        TrendChart<String, V> trendChart = new TrendChart<>();
        //设置横轴数据
        trendChart.setDates(generateDates(startDate, endDate, period));
        //构建纵轴
        List<Chart<String, V>> charts = new ArrayList<>();
        for (Map.Entry<String, List<DailyRecord<String, V>>> entry : indicatorsByName.entrySet()) {
            //构建一条折线纵轴数据
            Chart<String, V> chart = new Chart<>();
            //设置折现标题
            chart.setTitle(entry.getKey());
            chart.setPercent(isPercent);
            List<V> values;
            if (period == Period.Day || period == Period.Week || period == Period.Month) {
                values = values(startDate, endDate, period, defaultValue, entry);
            } else {
                //时段统计仅仅统计一天
                values = valuesByMinutes(startDate, period, defaultValue, entry);
            }

            chart.setValues(values);
            charts.add(chart);
            //默认返回限定数额的折线
            if (isDefaultChart && charts.size() >= LINES_LIMIT) {
                break;
            }
        }
        trendChart.setIndicators(charts);
        return trendChart;
    }

    /**
     * 根据十分钟间隔的实时报告获取20分钟、半小时、小时间隔统计数据
     *
     * @param reportsInTenMinutes
     * @param period
     * @return
     */
    private static <V> List<DailyRecord<String, V>> getIndicatorByTenMinuteIndicator(List<DailyRecord<String, V>> reportsInTenMinutes, Period period) {
        if (Period.TenMinutes == period || reportsInTenMinutes == null) {
            return reportsInTenMinutes;
        }
        //根据整点时段分组
        Map<Date, DailyRecord<String, V>> periodReports = new TreeMap<>();
        //这里将最靠近时段的统计作为该时段的统计。
        for (int i = reportsInTenMinutes.size() - 1; i >= 0; i--) {
            DailyRecord<String, V> report = reportsInTenMinutes.get(i);
            //时间“小”的统计报告会覆盖时间“大”的统计报告.
            Date time = periodTime(report.getDate(), period);
            report.setDate(time);
            periodReports.put(time, report);
        }
        return new ArrayList<>(periodReports.values());
    }

    /**
     * 将时间转为时段.
     *
     * @param time
     * @param period
     * @return
     */
    private static Date periodTime(Date time, Period period) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        int minute = calendar.get(Calendar.MINUTE);
        int min = minute / period.getIntervalInMinutes() * period.getIntervalInMinutes();
        calendar.set(Calendar.MINUTE, min);
        return calendar.getTime();
    }

    /**
     * 生成纵轴值列表
     *
     * @param startDate
     * @param endDate
     * @param period
     * @param defaultValue
     * @param entry
     * @param <V>
     * @return
     */
    private static <V> List<V> values(Date startDate, Date endDate, Period period, V defaultValue, Map.Entry<String, List<DailyRecord<String, V>>> entry) {
        List<V> values = new ArrayList<>();
        //补全日期
        Calendar calendar = getCalendar();
        calendar.setTime(startDate);
        if (period == Period.Week) {
            calendar.setFirstDayOfWeek(Calendar.MONDAY);
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        } else if (period == Period.Month) {
            calendar.set(Calendar.DATE, 1);
        }
        List<DailyRecord<String, V>> dailyRecords = entry.getValue();
        int index = 0;
        while (!calendar.getTime().after(endDate)) {
            if (index < dailyRecords.size() && calendar.getTime().equals(dailyRecords.get(index).getDate())) {
                V value = dailyRecords.get(index).getValue();
                if (value == null) {
                    value = defaultValue;
                }
                values.add(value);
                index++;
            } else {
                //缺失日期，设置其值为0
                values.add(defaultValue);
            }
            switch (period) {
                case Day:
                    calendar.add(Calendar.DATE, 1);
                    break;
                case Week:
                    calendar.add(Calendar.WEEK_OF_YEAR, 1);
                    break;
                case Month:
                    calendar.add(Calendar.MONTH, 1);
                    break;
            }
        }
        return values;
    }

    /**
     * 生成时段统计纵轴值列表
     *
     * @param startDate
     * @param period
     * @param defaultValue
     * @param entry
     * @param <V>
     * @return
     */
    private static <V> List<V> valuesByMinutes(Date startDate, Period period, V defaultValue, Map.Entry<String, List<DailyRecord<String, V>>> entry) {
        //补全时间
        //设置起始时间为今天零点
        Calendar calendar = getCalendar();
        calendar.setTime(startDate);
        Calendar endCalendar = getCalendar();
        endCalendar.setTime(startDate);
        endCalendar.add(Calendar.MINUTE, period.getIntervalInMinutes());
        Date endTime = getEndTime(startDate);
        //包含完整时段的Y轴值列表
        List<V> fullTrends = new ArrayList<>();
        List<DailyRecord<String, V>> records = entry.getValue();
        //由十分钟的数据获取响应时段的统计数据
        records = getIndicatorByTenMinuteIndicator(records, period);
        int index = 0, first = 0;
        //如果存在，则跳过零点,认为0点的指标值为0.
        if (records != null && !records.isEmpty() && records.get(0).getDate().equals(calendar.getTime())) {
            index = first = 1;
        }
        while (endCalendar.getTime().before(endTime)) {
            if (index < records.size() && endCalendar.getTime().equals(records.get(index).getDate())) {
                //获得该时间点的统计数据
                //计算差值
                //计算指标
                if (index == first) {
                    //第一个时段，取第一点的值。
                    DailyRecord<String, V> trend = records.get(index);
                    trend.setDate(calendar.getTime());
                    fullTrends.add(trend.getValue());
                } else {
                    //其他时段，取时段起始点的差值
                    DailyRecord<String, V> previousTrend = records.get(index - 1);
                    DailyRecord<String, V> currentTrend = records.get(index);
                    DailyRecord<String, V> trend = diff(currentTrend, previousTrend);
                    trend.setDate(calendar.getTime());
                    fullTrends.add(trend.getValue());
                }
                index++;
            } else {
                //没有零点统计数据，将零点统计数据初始化为0
                //该时间点的统计缺失,将当前时间点数据设置为上一个时间点的数据
                fullTrends.add(defaultValue);
            }
            calendar.add(Calendar.MINUTE, period.getIntervalInMinutes());
            endCalendar.add(Calendar.MINUTE, period.getIntervalInMinutes());
        }
        return fullTrends;
    }


    private static Calendar getCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    /**
     * 计算两个时段的差值
     *
     * @param curr
     * @param prev
     * @param <V>
     * @return
     */
    private static <V> DailyRecord<String, V> diff(DailyRecord<String, V> curr, DailyRecord<String, V> prev) {
        DailyRecord<String, V> periodRecord = new DailyRecord<>();
        periodRecord.setValue(subtract(curr.getValue(), prev.getValue()));
        return periodRecord;
    }

    /**
     * 减法
     *
     * @param number1
     * @param number2
     * @param <V>
     * @return
     */
    private static <V> V subtract(V number1, V number2) {
        if (number1 == null || number2 == null) {
            return null;
        }
        if (number1 instanceof Integer && number2 instanceof Integer) {
            return (V) Integer.valueOf((Integer) number1 - (Integer) number2);
        }
        if (number1 instanceof Long && number2 instanceof Long) {
            return (V) Long.valueOf((Long) number1 - (Long) number2);
        }
        if (number1 instanceof Double && number2 instanceof Double) {
            return (V) Double.valueOf((Integer) number1 - (Double) number2);
        }
        if (number1 instanceof BigDecimal && number2 instanceof BigDecimal) {
            return (V) ((BigDecimal) number1).subtract((BigDecimal) number2);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * 初始化按周统计日期
     *
     * @param indicators
     * @param <V>
     */

    private static <V> void initDateForWeek(List<? extends DailyRecord<String, V>> indicators) {
        for (DailyRecord<String, V> indicator : indicators) {
            if (indicator instanceof WeeklyRecord) {
                WeeklyRecord<String, V> weeklyIndicator = (WeeklyRecord<String, V>) indicator;
                Calendar calendar = getCalendar();
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                calendar.set(Calendar.YEAR, weeklyIndicator.getYear());
                calendar.set(Calendar.WEEK_OF_YEAR, weeklyIndicator.getWeekOfYear() + 1);
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                weeklyIndicator.setDate(calendar.getTime());
            }
        }
    }

    /**
     * 按名字分组
     *
     * @param indicators
     * @param <V>
     * @return
     */
    private static <V> Map<String, List<DailyRecord<String, V>>> groupByName(List<? extends DailyRecord<String, V>> indicators) {
        Map<String, List<DailyRecord<String, V>>> map = new TreeMap<>();
        for (DailyRecord<String, V> indicator : indicators) {
            if (indicator.getName() != null) {
                String name = indicator.getName();
                //健壮性考虑
                if (name == null) {
                    name = Labels.OTHERS;
                }
                if (map.containsKey(name)) {
                    map.get(name).add(indicator);
                } else {
                    List<DailyRecord<String, V>> indicatorsByName = new ArrayList<>();
                    indicatorsByName.add(indicator);
                    map.put(indicator.getName(), indicatorsByName);
                }
            }
        }
        return map;
    }

    /**
     * 生成趋势图横轴日期列表
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private static List<String> generateDates(Date startDate, Date endDate, Period period) {
        switch (period) {
            case Day:
                return generateDates(startDate, endDate);
            case Week:
                return generateDatesForWeek(startDate, endDate);
            case Month:
                return generateDatesForMonth(startDate, endDate);
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                return generateTimes(startDate, period);
            default:
                return null;
        }
    }

    /**
     * 生成按周统计日期范围列表
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private static List<String> generateDatesForWeek(Date startDate, Date endDate) {
        List<String> dates = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(startDate);
        int index = 0;
        while (!calendar.getTime().after(endDate)) {
            StringBuilder dateRangeBuilder = new StringBuilder();
            if (index == 0) {
                dateRangeBuilder.append(DateUtils.format(startDate,DateUtils.MONTH_DATE_FORMAT));
            } else {
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                dateRangeBuilder.append(DateUtils.format(calendar.getTime(),DateUtils.MONTH_DATE_FORMAT));
            }

            dateRangeBuilder.append(Labels.TO);
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            if (calendar.getTime().after(endDate)) {
                dateRangeBuilder.append(DateUtils.format(endDate,DateUtils.MONTH_DATE_FORMAT));
            } else {
                dateRangeBuilder.append(DateUtils.format(calendar.getTime(),DateUtils.MONTH_DATE_FORMAT));
            }

            //添加日期范围
            dates.add(dateRangeBuilder.toString());
            //增加一周
            calendar.add(Calendar.WEEK_OF_YEAR, 1);
            //用一周的第一天来比较
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            index++;
        }
        return dates;
    }

    /**
     * 生成按月统计日期范围列表
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private static List<String> generateDatesForMonth(Date startDate, Date endDate) {
        List<String> dates = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        int index = 0;
        while (!calendar.getTime().after(endDate)) {
            StringBuilder dateRangeBuilder = new StringBuilder();
            
            if (index == 0) {
                dateRangeBuilder.append(DateUtils.format(startDate, DateUtils.MONTH_DATE_FORMAT));
            } else {
                dateRangeBuilder.append(DateUtils.format(calendar.getTime(), DateUtils.MONTH_DATE_FORMAT));
            }
            dateRangeBuilder.append(Labels.TO);
            //获取一个月的最后一天
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            if (calendar.getTime().after(endDate)) {
                dateRangeBuilder.append(DateUtils.format(endDate, DateUtils.MONTH_DATE_FORMAT));
            } else {
                dateRangeBuilder.append(DateUtils.format(calendar.getTime(), DateUtils.MONTH_DATE_FORMAT));
            }

            //添加日期范围
            dates.add(dateRangeBuilder.toString());
            //增加一月
            calendar.add(Calendar.MONTH, 1);
            //用一个月的第一天来进行下一轮比较
            calendar.set(Calendar.DATE, 1);
            index++;
        }
        return dates;
    }

    /**
     * 生成时段列表
     *
     * @param startDate
     * @param period
     * @return
     */
    private static List<String> generateTimes(Date startDate, Period period) {
        List<String> dates = new ArrayList<>();
        Calendar calendar = getCalendar();
        calendar.setTime(startDate);
        Date endTime = getEndTime(startDate);
        DateFormat df = new SimpleDateFormat(TIME_PATTERN);
        while (calendar.getTime().before(endTime)) {
            dates.add(df.format(calendar.getTime()));
            calendar.add(Calendar.MINUTE, period.getIntervalInMinutes());
        }
        return dates;
    }

    /**
     * 获取时段统计的截止时间
     *
     * @param startDate
     * @return
     */
    private static Date getEndTime(Date startDate) {
        Date endTime;
        if (isToday(startDate)) {
            endTime = new Date();
        } else {
            Calendar endDateCalendar = getCalendar();
            endDateCalendar.setTime(startDate);
            endDateCalendar.add(Calendar.DATE, 1);
            endTime = endDateCalendar.getTime();
        }
        return endTime;
    }

    /**
     * 是否是今天或未来
     *
     * @param date
     * @return
     */
    private static boolean isToday(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return !date.before(calendar.getTime());
    }


    /**
     * 得到当前日期所在周的开始日期和结束日期
     * create by ldj 2017-07-31
     **/
    public static List<Date> getFirstAndLastOfWeek(String dataStr) throws Exception {
        List<Date> result = new ArrayList<Date>();
        Calendar cal = Calendar.getInstance();

        cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(dataStr));

        int d = 0;
        if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
            d = -6;
        } else {
            d = 2 - cal.get(Calendar.DAY_OF_WEEK);
        }
        cal.add(Calendar.DAY_OF_WEEK, d);
        // 所在周开始日期
        Date weekStartDate = cal.getTime();
        cal.add(Calendar.DAY_OF_WEEK, 6);
        // 所在周结束日期
        Date weekEndDate = cal.getTime();
        result.add(weekStartDate);
        result.add(weekEndDate);
        return result;

    }

    /**
     * 生成趋势图横轴日期列表
     * 周统计
     *
     * @param startDate
     * @param endDate
     * @return create by ldj 2017-07-31
     */
    private static List<String> generateDatesByWeek(Date startDate, Date endDate) throws Exception {
        List<String> dates = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        while (!calendar.getTime().after(endDate)) {
            List<Date> firstAndLastOfWeek = getFirstAndLastOfWeek(DateUtils.format(calendar.getTime()));
            Date lastDate = firstAndLastOfWeek.get(1);
            String weekStartDate = DateUtils.format(firstAndLastOfWeek.get(0),DateUtils.MONTH_DATE_FORMAT);
            String weekEndDate = DateUtils.format(firstAndLastOfWeek.get(1),DateUtils.MONTH_DATE_FORMAT);
            if (calendar.getTime().equals(startDate)) {
                weekStartDate = DateUtils.format(startDate,DateUtils.MONTH_DATE_FORMAT);
            }
            if (lastDate.equals(endDate)) {
                weekEndDate = DateUtils.format(endDate,DateUtils.MONTH_DATE_FORMAT);
            }
            dates.add(weekStartDate + "~" + weekEndDate);
            calendar.setTime(lastDate);
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }

    /**
     * 转化为趋势图
     * 周统计
     *
     * @param indicators
     * @return create by ldj 2017-07-31
     */
    public static <V> TrendChart<String, V> toTrendChartByWeek(Date startDate, Date endDate, List<DailyRecord<String, V>> indicators, V defaultValue, boolean isPercent, boolean isDefaultChart) throws Exception {
        if (indicators == null) {
            return null;
        }
        //Group by name.
        Map<String, List<DailyRecord<String, V>>> map = new HashMap<>();
        for (DailyRecord<String, V> indicator : indicators) {
            if (map.containsKey(indicator.getName())) {
                map.get(indicator.getName()).add(indicator);
            } else {
                List<DailyRecord<String, V>> indicatorsByName = new ArrayList<>();
                indicatorsByName.add(indicator);
                map.put(indicator.getName(), indicatorsByName);
            }
        }

        //构建趋势图对象
        TrendChart<String, V> trendChart = new TrendChart<>();
        //设置横轴数据
        trendChart.setDates(generateDatesByWeek(startDate, endDate));
        //构建纵轴
        List<Chart<String, V>> charts = new ArrayList<>();
        for (Map.Entry<String, List<DailyRecord<String, V>>> entry : map.entrySet()) {
            //构建一条折线纵轴数据
            Chart<String, V> chart = new Chart<>();
            //设置折现标题
            chart.setTitle(entry.getKey());
            chart.setPercent(isPercent);
            List<V> values = new ArrayList<>();

            //补全日期
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            List<DailyRecord<String, V>> DailyRecords = entry.getValue();
            int index = 0;
            while (!calendar.getTime().after(endDate)) {
                /*int year = calendar.get(Calendar.YEAR);
                int week = calendar.get(Calendar.WEEK_OF_YEAR);
            	int yearRs = WeeklyRecords.get(index).getYear();
            	int weekRs = WeeklyRecords.get(index).getWeek();*/
                List<Date> firstAndLastOfWeek = getFirstAndLastOfWeek(DateUtils.format(calendar.getTime()));
                Date firstDate = firstAndLastOfWeek.get(0);
                Date lastDate = firstAndLastOfWeek.get(1);
                if (index < DailyRecords.size() && firstDate.equals(DailyRecords.get(index).getDate())) {
                    V value = DailyRecords.get(index).getValue();
                    if (value == null) {
                        value = defaultValue;
                    }
                    values.add(value);
                    index++;
                } else {
                    //缺失日期，设置其值为0
                    values.add(defaultValue);
                }
                calendar.setTime(lastDate);
                calendar.add(Calendar.DATE, 1);
            }
            chart.setValues(values);
            charts.add(chart);
        }
        if (isDefaultChart && charts.size() > LINES_LIMIT) {
            //仅显示前面两条折线
            trendChart.setIndicators(charts.subList(0, LINES_LIMIT));
        } else {
            trendChart.setIndicators(charts);
        }
        return trendChart;
    }

    /**
     * 转化为累计趋势图
     *
     * @param indicators
     * @return
     */
    public static <V> TrendChart<String, V> toAccumulativeTrendChart(Date startDate, Date endDate, List<DailyRecord<String, V>> indicators, V defaultValue, boolean isPercent, boolean isDefaultChart) {
        if (indicators == null) {
            return null;
        }
        //Group by name.
        Map<String, List<DailyRecord<String, V>>> map = new HashMap<>();
        for (DailyRecord<String, V> indicator : indicators) {
            if (map.containsKey(indicator.getName())) {
                map.get(indicator.getName()).add(indicator);
            } else {
                List<DailyRecord<String, V>> indicatorsByName = new ArrayList<>();
                indicatorsByName.add(indicator);
                map.put(indicator.getName(), indicatorsByName);
            }
        }

        //构建趋势图对象
        TrendChart<String, V> trendChart = new TrendChart<>();
        //设置横轴数据
        trendChart.setDates(generateDates(startDate, endDate));
        //构建纵轴
        List<Chart<String, V>> charts = new ArrayList<>();

        for (Map.Entry<String, List<DailyRecord<String, V>>> entry : map.entrySet()) {
            //构建一条折线纵轴数据
            Chart<String, V> chart = new Chart<>();
            //设置折现标题
            chart.setTitle(entry.getKey());
            chart.setPercent(isPercent);
            List<V> values = new ArrayList<>();
            //补全日期
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            List<DailyRecord<String, V>> DailyRecords = entry.getValue();
            V previousValue = defaultValue;
            int index = 0;
            while (!calendar.getTime().after(endDate)) {
                if (index < DailyRecords.size() && calendar.getTime().equals(DailyRecords.get(index).getDate())) {
                    V value = DailyRecords.get(index).getValue();
                    if (value != null) {
                        previousValue = value;
                    }

                    if (value == null) {
                        value = previousValue;
                    }
                    values.add(value);
                    index++;
                } else {
                    //缺失日期，设置前面的值
                    values.add(previousValue);
                }
                calendar.add(Calendar.DATE, 1);
            }
            chart.setValues(values);
            charts.add(chart);
        }
        if (isDefaultChart && charts.size() > LINES_LIMIT) {
            //仅显示前面三条折线
            trendChart.setIndicators(charts.subList(0, LINES_LIMIT));
        } else {
            trendChart.setIndicators(charts);
        }
        return trendChart;
    }

    /**
     * 生成趋势图横轴日期列表
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private static List<String> generateDates(Date startDate, Date endDate) {
        List<String> dates = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        while (!calendar.getTime().after(endDate)) {
            dates.add(DateUtils.format(calendar.getTime(), DateUtils.MONTH_DATE_FORMAT));
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }
}
