package com.dataeye.ad.assistor.common;


import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by huangzehai on 2017/4/7.
 */
public class TrendChart<N, V> {
    /**
     * 日期列表（X轴）.
     */
    @Expose
    private List<String> dates;

    /**
     * 右边指标（纵轴）
     */
    @Expose
    private List<Chart<N, V>> indicators;

    public List<String> getDates() {
        return dates;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }

    public List<Chart<N, V>> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Chart<N, V>> indicators) {
        this.indicators = indicators;
    }

    @Override
    public String toString() {
        return "TrendChart{" +
                "dates=" + dates +
                ", indicators=" + indicators +
                '}';
    }
}
