package com.dataeye.ad.assistor.common;

import com.google.gson.annotations.Expose;

/**
 * 列表项.
 * Created by huangzhai on 2017/2/22.
 */
public class Item<V, L> {
    @Expose
    private V value;
    @Expose
    private L label;

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public L getLabel() {
        return label;
    }

    public void setLabel(L label) {
        this.label = label;
    }

    public Item(V value, L label) {
        this.value = value;
        this.label = label;
    }

    @Override
    public String toString() {
        return "Item{" +
                "value=" + value +
                ", label=" + label +
                '}';
    }
}
