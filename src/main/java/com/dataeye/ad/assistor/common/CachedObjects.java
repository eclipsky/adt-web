package com.dataeye.ad.assistor.common;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * <pre>
 * 缓存一些对象
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:47:42
 */
public class CachedObjects {
	/* 生成随机数的Random对象 */
	public static final Random RANDOM = new Random();
	/* 格式化对象为json字符串 */
	public static final Gson GSON = new Gson();
	/* 把对象转为json字符串的工具,这个是只有添加了 @Expose 注解的 才会被添加到 json字符串中 */
	public static Gson GSON_ONLY_EXPOSE = null;
	/* , Joiner */
	public static final String REGX_INT_COMMA_STR = "\\d+( *, *\\d+)*";
	/* 整数精保留 */
	public static final DecimalFormat integerFormat = new DecimalFormat("#,###");
	/* 小数精保留两位小数，四舍五入 */
	public static final DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
	/* 小数转换成百分数，保留两位小数，四舍五入 */
	public static final DecimalFormat decimalPercentageFormat = new DecimalFormat("#0.00%");

	static {
		GSON_ONLY_EXPOSE = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		decimalPercentageFormat.setRoundingMode(RoundingMode.HALF_UP);
		decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
	}

}
