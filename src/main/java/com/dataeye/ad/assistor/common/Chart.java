package com.dataeye.ad.assistor.common;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by huangzehai on 2017/2/22.
 */
public class Chart<N, V> {
    /**
     * 标题.
     */
    @Expose
    private N title;
    /**
     * 纵轴坐标.
     */
    @Expose
    private List<V> values;

    /**
     * 是否是百分比.
     */
    @Expose
    private boolean isPercent;

    /**
     * 通用标志位
     */
    @Expose
    private Boolean flag;

    public N getTitle() {
        return title;
    }

    public void setTitle(N title) {
        this.title = title;
    }

    public List<V> getValues() {
        return values;
    }

    public void setValues(List<V> values) {
        this.values = values;
    }

    public boolean isPercent() {
        return isPercent;
    }

    public void setPercent(boolean percent) {
        isPercent = percent;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Chart{" +
                "title=" + title +
                ", values=" + values +
                ", isPercent=" + isPercent +
                ", flag=" + flag +
                '}';
    }
}
