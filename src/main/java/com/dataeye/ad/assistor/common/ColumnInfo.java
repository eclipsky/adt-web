package com.dataeye.ad.assistor.common;

import com.google.gson.annotations.Expose;

/**
 * <pre>
 *
 * @author Stran <br>
 * @create 2016.12.02 14:03 <br>
 * @version 1.0 <br>
 */
public class ColumnInfo {

    @Expose
    private String title;
    @Expose
    private String field;
    @Expose
    private boolean sortable;
    /**
     * 分组名称.
     */
    @Expose
    private String group;

    public ColumnInfo(String title, String field, boolean sortable) {
        this.title = title;
        this.field = field;
        this.sortable = sortable;
    }

    public ColumnInfo(String title, String field, boolean sortable, String group) {
        this.title = title;
        this.field = field;
        this.sortable = sortable;
        this.group = group;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public boolean isSortable() {
        return sortable;
    }

    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }
}
