package com.dataeye.ad.assistor.common;

public class WeeklyRecord<N, V> extends DailyRecord<N, V> {
    private int year;
    private int weekOfYear;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getWeekOfYear() {
        return weekOfYear;
    }

    public void setWeekOfYear(int weekOfYear) {
        this.weekOfYear = weekOfYear;
    }

    @Override
    public String toString() {
        return "WeeklyRecord{" +
                "year=" + year +
                ", weekOfYear=" + weekOfYear +
                "} " + super.toString();
    }
}
