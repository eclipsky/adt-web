package com.dataeye.ad.assistor.common;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;

/**
 * <pre>
 * The type Page data helper.
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.10.08 14:31:41
 */
public class PageDataHelper {

	/**
	 * 从mybatis分页插件查询数据后，将返回的数据封装为PageData对象
	 * @param pages
	 * @return
	 * @author luzhuyou 2017/02/17
	 */
	public static <E> PageData getPageData(Page<E> pages) {
        PageData pageData = new PageData(pages.getPageSize(), pages.getPageNum());
        pageData.setContent(pages.getResult());
        pageData.setTotalRecord((int)pages.getTotal());
        pageData.setTotalPage(pages.getPages());
        return pageData;
    }
	
	/**
	 * 将空数据集合封装为PageData对象
	 * @param pages
	 * @return
	 * @author luzhuyou 2017/02/17
	 */
	public static <E> PageData getEmptyPageData(int pageSize, int pageId, Object content) {
        PageData pageData = new PageData(pageSize, pageId);
        pageData.setContent(content);
        pageData.setTotalRecord(0);
        pageData.setTotalPage(0);
        return pageData;
    }
	
    public static PageData getPageDataByList(int pageSize, int pageId, List list) {
        PageData pageData = new PageData(pageSize, pageId);

        // 设置总页数，如果请求的页码大于总页数，则修改页面为最后一页
        pageData.setContent(list);
        pageData.setTotalRecord(list.size());
        pageData.setPageAmount();
        if (pageId > pageData.getTotalPage()) {
            pageData.setPageId(pageData.getTotalPage());
        }

        // 获取分页数据
        int startIndex = (pageData.getPageId() - 1) * pageData.getPageSize();
        int endIndex = pageData.getPageId() * pageData.getPageSize() - 1;
        endIndex = (endIndex > (list.size() - 1)) ? list.size() - 1 : endIndex;
        pageData.setContent(list.subList(startIndex, endIndex + 1));

        return pageData;
    }

    public static PageData getPageData(int pageSize, int pageId, int totalRecord) {
        PageData pageData = new PageData(pageSize, pageId);
        pageData.setTotalRecord(totalRecord);
        pageData.setPageAmount();
        return pageData;
    }

    public static PageData getPageData(int pageSize, int pageId, int totalRecord, Map<String, Object> column) {
        PageData pageData = new PageData(pageSize, pageId);
        pageData.setTotalRecord(totalRecord);
        pageData.setPageAmount();
        pageData.setColumn(column);
        return pageData;
    }


}
