package com.dataeye.ad.assistor.common;

import java.util.Date;

/**
 * 趋势图的一个记录
 * Created by huangzehai on 2017/4/27.
 */
public class DailyRecord<N, V> {
    private Date date;
    private N name;
    private V value;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public N getName() {
        return name;
    }

    public void setName(N name) {
        this.name = name;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "DailyRecord{" +
                "date=" + date +
                ", name=" + name +
                ", value=" + value +
                '}';
    }
}
