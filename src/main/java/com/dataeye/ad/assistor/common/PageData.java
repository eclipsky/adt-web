package com.dataeye.ad.assistor.common;

import java.util.HashMap;
import java.util.Map;

import com.dataeye.ad.assistor.context.DEParameter.Keys;
import com.google.gson.annotations.Expose;

/**
 * <pre>
 * 分页数据
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:47:50
 */
public class PageData {
    /**
     * 总共有多少条记录
     */
    @Expose
    private int totalRecord;
    /**
     * 每页显示多少记录
     */
    @Expose
    private int pageSize;
    /**
     * 总动可以分多少页
     */
    @Expose
    private int totalPage;
    /**
     * 当前返回的是第几页
     */
    @Expose
    private int pageId;
    /**
     * 当前页数据
     */
    @Expose
    private Object content;
    /**
     * 当前页汇总数据
     */
    @Expose
    private Object sumData;
    /**
     * 列名
     */
    @Expose
    private Map<String, Object> column;
    /**
     * 结果集排序字段
     */
    private String orderBy;
    /**
     * 排序规则
     */
    private int order;

    public PageData() {
    }

    public PageData(int pageSize, int pageID) {
        setPageSize(pageSize);
        setPageId(pageID);
    }

    public void setPageAmount() {
        if (totalRecord > 0 && pageSize > 0) {
            this.totalPage = (totalRecord + pageSize - 1) / pageSize;
        } else if (pageSize == -1) {
            this.totalPage = 1;
        }

    }

    public void setPageId(int pageId) {
        this.pageId = (pageId <= 0) ? 1 : pageId;
    }

    private Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(Keys.TOTAL_PAGE, this.totalPage);
        map.put(Keys.PAGESIZE, this.pageSize);
        map.put(Keys.PAGEID, this.pageId);
        map.put(Keys.TOTAL_RECORD, this.totalRecord);
        map.put(Keys.CONTENT, this.content);
        return map;
    }

	public int getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

	public Object getSumData() {
		return sumData;
	}

	public void setSumData(Object sumData) {
		this.sumData = sumData;
	}

	public Map<String, Object> getColumn() {
		return column;
	}

	public void setColumn(Map<String, Object> column) {
		this.column = column;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getPageId() {
		return pageId;
	}

}
