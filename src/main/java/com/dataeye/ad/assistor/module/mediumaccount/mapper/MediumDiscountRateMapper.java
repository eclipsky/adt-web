package com.dataeye.ad.assistor.module.mediumaccount.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.mediumaccount.model.MediumDiscountRate;

/**
 * 媒体折扣率映射器.
 * Created by luzhuyou 2017/02/20
 */
@MapperScan
public interface MediumDiscountRateMapper {

	/**
	 * 新增媒体折扣率
	 * @param mediumDiscountRate
	 * @return
	 */
	public int add(MediumDiscountRate mediumDiscountRate);

	/**
	 * 修改媒体折扣率
	 * @param mediumDiscountRate
	 * @return
	 */
	public int update(MediumDiscountRate mediumDiscountRate);

}
