package com.dataeye.ad.assistor.module.advertisement.model;

import java.util.Date;



/**
 * 推广计划组
 * Created by ldj
 */
public class PlanGroupCreatePreVO {

	private Integer Id;
	/**计划组id**/
    private Integer planGroupId;
	/**计划组名称**/
    private String planGroupName;
	/**日限额，单位为元，精确到分**/
    private Integer dailyBudget;
	/**投放速度模式**/
    private Integer speedMode;
	/**客户设置的状态，即开关操作*/
    private Integer planGroupStatus;
	/**标的物类型*/
	private Integer productType;
	
	/**媒体帐号*/
	private String mediumAccountName;

	private Integer mediumAccountId;
	private Integer mediumId;
	private String mediumName;
	
	private Integer productId;
	
	private String productNames;
	private String productIds;
	private Integer planGroupType;
	
	private Integer systemAccountId;
	private Integer companyId;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	
		
	public Integer getPlanGroupId() {
		return planGroupId;
	}
	public void setPlanGroupId(Integer planGroupId) {
		this.planGroupId = planGroupId;
	}
	public String getPlanGroupName() {
		return planGroupName;
	}
	public void setPlanGroupName(String planGroupName) {
		this.planGroupName = planGroupName;
	}
	public Integer getSpeedMode() {
		return speedMode;
	}
	public void setSpeedMode(Integer speedMode) {
		this.speedMode = speedMode;
	}
	public Integer getPlanGroupStatus() {
		return planGroupStatus;
	}
	public void setPlanGroupStatus(Integer planGroupStatus) {
		this.planGroupStatus = planGroupStatus;
	}
	public String getMediumAccountName() {
		return mediumAccountName;
	}
	public void setMediumAccountName(String mediumAccountName) {
		this.mediumAccountName = mediumAccountName;
	}
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductNames() {
		return productNames;
	}
	public void setProductNames(String productNames) {
		this.productNames = productNames;
	}
	
	public Integer getProductType() {
		return productType;
	}
	public void setProductType(Integer productType) {
		this.productType = productType;
	}
	public String getProductIds() {
		return productIds;
	}
	public void setProductIds(String productIds) {
		this.productIds = productIds;
	}
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public Integer getPlanGroupType() {
		return planGroupType;
	}
	public void setPlanGroupType(Integer planGroupType) {
		this.planGroupType = planGroupType;
	}
	public Integer getSystemAccountId() {
		return systemAccountId;
	}
	public void setSystemAccountId(Integer systemAccountId) {
		this.systemAccountId = systemAccountId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getDailyBudget() {
		return dailyBudget;
	}
	public void setDailyBudget(Integer dailyBudget) {
		this.dailyBudget = dailyBudget;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "PlanGroupCreatePreVO [Id=" + Id + ", planGroupId="
				+ planGroupId + ", planGroupName=" + planGroupName
				+ ", dailyBudget=" + dailyBudget + ", speedMode=" + speedMode
				+ ", planGroupStatus=" + planGroupStatus + ", productType="
				+ productType + ", mediumAccountName=" + mediumAccountName
				+ ", mediumAccountId=" + mediumAccountId + ", mediumId="
				+ mediumId + ", mediumName=" + mediumName + ", productId="
				+ productId + ", productNames=" + productNames
				+ ", productIds=" + productIds + ", planGroupType="
				+ planGroupType + ", systemAccountId=" + systemAccountId
				+ ", companyId=" + companyId + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + "]";
	}

	
}
