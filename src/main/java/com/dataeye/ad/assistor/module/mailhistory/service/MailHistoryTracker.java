package com.dataeye.ad.assistor.module.mailhistory.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.mailhistory.mapper.MailHistoryMapper;
import com.dataeye.ad.assistor.module.mailhistory.model.MailHistory;

/**
 * 邮件发送历史Service
 * @author luzhuyou 2017/02/20
 *
 */
@Service("mailHistoryTracker")
public class MailHistoryTracker {

	@Autowired
	private MailHistoryMapper mailHistoryMapper;
	
	/**
	 * 新增邮件发送历史
	 * @param mailType 邮件类型：0-创建系统账号，1-密码重置，2-告警邮件
	 * @param sender 发件人
	 * @param recipients 收件人
	 * @param sendStatus 发送状态：0-成功，1-失败
	 * @param remark 备注，记录异常
	 * @return 新增记录主键ID
	 */
	public int add(int mailType, String sender, String recipients, int sendStatus, String remark) {
		MailHistory mailHistory = new MailHistory();
		mailHistory.setMailType(mailType);
		mailHistory.setSender(sender);
		mailHistory.setRecipients(recipients);
		mailHistory.setSendStatus(sendStatus);
		mailHistory.setRemark(remark);
		mailHistory.setCreateTime(new Date());
		mailHistoryMapper.add(mailHistory);
		return mailHistory.getId();
	}

}
