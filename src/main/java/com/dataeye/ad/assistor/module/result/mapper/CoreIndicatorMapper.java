package com.dataeye.ad.assistor.module.result.mapper;

import com.dataeye.ad.assistor.module.result.model.CoreIndicator;
import com.dataeye.ad.assistor.module.result.model.CoreIndicatorQuery;
import com.dataeye.ad.assistor.module.result.model.DailyIndicator;
import com.dataeye.ad.assistor.module.result.model.Indicator;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import org.mybatis.spring.annotation.MapperScan;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/6.
 */
@MapperScan
public interface CoreIndicatorMapper {
    /**
     * 获取总体核心指标.
     *
     * @return
     */
    CoreIndicator getTotalCost(CoreIndicatorQuery dataPermissionDomain);

    /**
     *  获取总充值
     * @param dataPermissionDomain
     * @return
     */
    BigDecimal getTotalRecharge(CoreIndicatorQuery dataPermissionDomain);

    /**
     *  获取真实充值
     * @param dataPermissionDomain
     * @return
     */
    BigDecimal getRealRecharge(CoreIndicatorQuery dataPermissionDomain);

    /**
     * 获取当前总账号余额
     *
     * @param dataPermissionDomain
     * @return
     */
    BigDecimal getTotalBalance(CoreIndicatorQuery dataPermissionDomain);

    /**
     *  获取真实充值
     * @param dataPermissionDomain
     * @return
     */
    BigDecimal getTotalRegistrations(CoreIndicatorQuery dataPermissionDomain);

    /**
     * 获取DAU.
     * @param dataPermissionDomain
     * @return
     */
    Long getDau(CoreIndicatorQuery dataPermissionDomain);

    /**
     * 按媒体分组统计消耗.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCostGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计充值.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getRechargeGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计回本率
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getPaybackRateGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计余额
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getBalanceGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计CPA.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCpaGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计DAU
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getDauGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);


    /**
     * 按产品分组统计消耗.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCostGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计充值.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getRechargeGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计回本率
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getPaybackRateGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计余额
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getBalanceGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计CPA.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCpaGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计DAU
     *
     * @param coreIndicatorQuery
     * @returnl
     */
    List<Indicator> getDauGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计消耗.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCostGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计充值.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getRechargeGroupByOs(DataPermissionDomain coreIndicatorQuery);

    /**
     * 按产品分组统计回本率
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getPaybackRateGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计CPA.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCpaGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计DAU
     *
     * @param coreIndicatorQuery
     * @returnl
     */
    List<Indicator> getDauGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计日消耗趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyCostTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计日充值趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyRechargeTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计日余额趋势
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyBalanceTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计日CPA趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyCpaTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计日DAU趋势
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyDauTrendGroupByMedium(DataPermissionDomain coreIndicatorQuery);


    /**
     * 按产品分组统计日消耗趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyCostTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日充值趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyRechargeTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日余额趋势
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyBalanceTrendGroupByProduct(DataPermissionDomain coreIndicatorQuery);

    /**
     * 按产品分组统计日CPA趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyCpaTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日DAU趋势
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyDauTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日消耗趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyCostTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日充值趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyRechargeTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日CPA趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyCpaTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日DAU趋势
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<DailyIndicator> dailyDauTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery);
}
