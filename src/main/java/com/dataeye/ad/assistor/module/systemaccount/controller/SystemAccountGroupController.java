package com.dataeye.ad.assistor.module.systemaccount.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants;
import com.dataeye.ad.assistor.module.systemaccount.model.GroupDetailVo;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountGroup;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountGroupService;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.EncodeUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * Created by luzhuyou
 * 系统账号分组控制器.
 */
@Controller
public class SystemAccountGroupController {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(SystemAccountGroupController.class);

	@Autowired
	private SystemAccountGroupService systemAccountGroupService;
	@Autowired
    private SystemAccountService systemAccountService;
	
	/**
     * 获取分组信息
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/17
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false, menuDesc = "系统账号分组")
    @RequestMapping("/ad/systemaccount/queryGroup.do")
    public Object queryGroup(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("获取分组信息");
		DEContext context = DEContextContainer.getContext(request);
		int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
        
        return systemAccountGroupService.queryBasicGroup(companyId);
    }
	
	/**
     * 查询分组系统账户列表
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/17
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/systemaccount/queryGroupAccount.do")
    public Object queryGroupAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("查询分组系统账户列表");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        
        String groupId = parameter.getParameter("groupId");
        if(StringUtils.isBlank(groupId) || !StringUtils.isNumeric(groupId)) {
        	groupId = null;
		}
        PageHelper.startPage(parameter.getPageId(), parameter.getPageSize());
        List<GroupDetailVo> result = systemAccountGroupService.query(Integer.parseInt(groupId), companyId);
        PageData pageData = PageDataHelper.getPageData((Page)result);
        return pageData;
    }
    
    /**
     * 创建新分组
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/17
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/systemaccount/addGroup.do")
    public Object addGroup(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("创建新分组");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String groupName = parameter.getParameter("groupName");
        String leaderAccount = parameter.getParameter("leaderAccount");
        String memberAccounts = parameter.getParameter("memberAccounts");
        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
        if(StringUtils.isBlank(groupName)) {
        	logger.error("分组名不允许为空");
            ExceptionHandler.throwParameterException(StatusCode.ACCO_GROUP_NAME_IS_NULL);
        }
        if(StringUtils.isBlank(leaderAccount)) {
        	leaderAccount = "";
        } else {
        	// 检查组长账号是否存在
            SystemAccountVo account = systemAccountService.get(leaderAccount);
    		if (account == null) {
    			logger.error("组长账号不存在");
    			ExceptionHandler.throwParameterException(StatusCode.ACCO_LEADER_ACCOUNT_NOT_EXIST);
    		}
        }
        
        if(StringUtils.isBlank(memberAccounts)) {
        	memberAccounts = "";
        } 
        
        String memberAccountsMd5 = EncodeUtils.md5Base64Encode(memberAccounts);
        
        // 校验分组名称是否已存在
        SystemAccountGroup group = systemAccountGroupService.get(null, groupName, companyId);
        if (group != null) {
        	logger.error("分组名已存在");
        	ExceptionHandler.throwParameterException(StatusCode.ACCO_GROUP_NAME_EXIST);
        }
        
		// 新增系统账号分组信息
        int result = systemAccountGroupService.add(groupName, leaderAccount, memberAccounts, companyId, memberAccountsMd5);
     // 刷新UserInSession
        SessionContainer.refreshUserInSession(request);
		// 修改系统账号分组信息
        return result;
    }
    
    /**
     * 修改分组
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/17
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/systemaccount/modifyGroup.do")
    public Object modifyGroup(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String groupId = parameter.getParameter("groupId");
        String groupName = parameter.getParameter("groupName");
        String leaderAccount = parameter.getParameter("leaderAccount");
        String memberAccounts = parameter.getParameter("memberAccounts");
        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
        if(StringUtils.isBlank(groupId)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        if(StringUtils.isBlank(groupName)) {
        	logger.error("分组名不允许为空");
            ExceptionHandler.throwParameterException(StatusCode.ACCO_GROUP_NAME_IS_NULL);
        }
        
        if(StringUtils.isBlank(leaderAccount)) {
        	leaderAccount = "";
        } else {
        	// 检查组长账号是否存在
            SystemAccountVo account = systemAccountService.get(leaderAccount);
    		if (account == null) {
    			logger.error("组长账号不存在");
    			ExceptionHandler.throwParameterException(StatusCode.ACCO_LEADER_ACCOUNT_NOT_EXIST);
    		}
        }
        
        if(StringUtils.isBlank(memberAccounts)) {
        	memberAccounts = "";
        }
        String memberAccountsMd5 = EncodeUtils.md5Base64Encode(memberAccounts);
        
        boolean isMemberAccountsModify = false;
        // 校验分组名称是否已存在
        SystemAccountGroup group = systemAccountGroupService.get(null, groupName, companyId);
        if (group != null) {
        	if(group.getGroupId() != Integer.parseInt(groupId)) {
        		logger.error("分组名已存在");
    			ExceptionHandler.throwParameterException(StatusCode.ACCO_GROUP_NAME_EXIST);
        	} else {
        		// 判断组员是否有变更
            	if(memberAccountsMd5 == null)  {
            		if(group.getMemberAccountsMd5() != null) {
            			isMemberAccountsModify = true;
            		}
            	} else if(!memberAccountsMd5.equals(group.getMemberAccountsMd5())) {
        			isMemberAccountsModify = true;
        		}
        	}
        } 
        // 如果分组名修改为不存在的名称，根据分组ID查询出原纪录，判断是否组员发生变更
        else {
        	group = systemAccountGroupService.get(Integer.parseInt(groupId), null, companyId);
            if (group != null) {
            	// 判断组员是否有变更
            	if(memberAccountsMd5 == null)  {
            		if(group.getMemberAccountsMd5() != null) {
            			isMemberAccountsModify = true;
            		}
            	} else if(!memberAccountsMd5.equals(group.getMemberAccountsMd5())) {
        			isMemberAccountsModify = true;
        		}
            } else {
            	logger.error("分组不存在");
    			ExceptionHandler.throwParameterException(StatusCode.ACCO_GROUP_ID_NOT_EXIST);
            }
        }
        int result = systemAccountGroupService.modify(Integer.parseInt(groupId), groupName, leaderAccount, memberAccounts, companyId, memberAccountsMd5, isMemberAccountsModify);

       // 刷新UserInSession
        SessionContainer.refreshUserInSession(request);
		// 修改系统账号分组信息
        return result;
    }
    
    /**
     * 解散分组，将分组信息删除，同时把所有该分组中的组员改为未分组，如果组长不同时担任多个组的组长，则将分组组长角色改为普通系统账号
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/17
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/systemaccount/deleteGroup.do")
    public Object deleteGroup(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("解散分组");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String groupId = parameter.getParameter("groupId");
        if(StringUtils.isBlank(groupId) || !StringUtils.isNumeric(groupId)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
        
        // 解散分组，将分组信息删除，同时把所有该分组中的组员改为未分组
        int result = systemAccountGroupService.delete(Integer.parseInt(groupId), companyId);
        
       // 刷新UserInSession
        SessionContainer.refreshUserInSession(request);
		// 修改系统账号分组信息
        return result;
    }
    
    /**
     * 删除分组某个组员，将组员变为未分组
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/17
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/systemaccount/deleteGroupMember.do")
    public Object deleteGroupMember(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("删除分组某个组员，将组员变为未分组");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String groupId = parameter.getParameter("groupId");
        String accountName = parameter.getParameter("accountName");
        String accountRole = parameter.getParameter("accountRole");
        if(StringUtils.isBlank(groupId) || !StringUtils.isNumeric(groupId) 
        		|| StringUtils.isBlank(accountName) || StringUtils.isBlank(accountRole) 
        		|| !Constants.AccountRole.isAccountRole(Integer.parseInt(accountRole.trim()))) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
		int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();

        // 删除分组某个组员，将组员变为未分组，如果是删除组长，并且该组长只担任一个组的组长，则组长角色更新为普通系统账号
        int result = systemAccountGroupService.deleteGroupMember(Integer.parseInt(groupId.trim()), accountName.trim(), Integer.parseInt(accountRole.trim()), companyId);
     // 刷新UserInSession
        SessionContainer.refreshUserInSession(request);
		// 修改系统账号分组信息
        return result;
    }
}
