package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.model.DeliveryStatus;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationStatus;
import com.dataeye.ad.assistor.util.CookieUtils;
import com.google.gson.Gson;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by huangzehai on 2017/5/18.
 */
@Service("baiduPlanSwitchService")
public class BaiduPlanSwitchService extends AbstractMediumPlanService<DeliveryStatus> implements PlanSwitchService {

    private static final String URL = "http://feedads.baidu.com/nirvana/request.ajax?path=sirius/MOD/unit";
    private static final String USER_ID_KEY = "SAMPLING_USER_ID";
    private static final String USER_ID_KEY_2 = "__cas__id__3";
    private static final String TOKEN = "__cas__st__3";

    /**
     * 成功消息。
     */
    private static final String SUCCESS = "切换计划开关成功";
    /**
     * 失败消息.
     */
    private static final String FAIL = "切换计划开关失败";

    @Override
    public Result updateStatus(DeliveryStatus status) {
        return execute(status);
    }

    @Override
    protected String url(String cookie, Long mediumPlanId) {
        return URL;
    }

    @Override
    protected List<NameValuePair> parameters(String cookie, DeliveryStatus status) {
        String userId = CookieUtils.get(cookie, USER_ID_KEY);
        if(StringUtils.isEmpty(userId)){
        	userId = CookieUtils.get(cookie, USER_ID_KEY_2);
        }
        String token = CookieUtils.get(cookie, TOKEN);
        List<NameValuePair> params = new ArrayList<>();

        List<Long> mediumPlanIds = new ArrayList<>();
        mediumPlanIds.add(status.getMediumPlanId());

        params.add(new BasicNameValuePair("userid", userId));
        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("reqid", UUID.randomUUID().toString()));
        params.add(new BasicNameValuePair("path", "sirius/MOD/unit"));
        //计划ID
        params.add(new BasicNameValuePair("eventId", UUID.randomUUID().toString()));
        //操作
        Params param = new Params();
        int pausestat = status.getStatus() == OperationStatus.Disable ? 1 : 0;
        param.setItems(new Items(pausestat));
        param.setUnitid(Arrays.asList(status.getMediumPlanId()));
        Gson gson = new Gson();
        params.add(new BasicNameValuePair("params", gson.toJson(param)));
        return params;
    }

    private class Params {
        private Items items;

        private List<Long> unitid;

        public Items getItems() {
            return items;
        }

        public void setItems(Items items) {
            this.items = items;
        }

        public List<Long> getUnitid() {
            return unitid;
        }

        public void setUnitid(List<Long> unitid) {
            this.unitid = unitid;
        }

        @Override
        public String toString() {
            return "Params{" +
                    "items=" + items +
                    ", unitid=" + unitid +
                    '}';
        }
    }

    private class Items {
        private int pausestat;

        public Items(int pausestat) {
            this.pausestat = pausestat;
        }

        public int getPausestat() {
            return pausestat;
        }

        public void setPausestat(int pausestat) {
            this.pausestat = pausestat;
        }

        @Override
        public String toString() {
            return "Items{" +
                    "pausestat=" + pausestat +
                    '}';
        }
    }

    @Override
    protected void requestHeader(HttpRequestBase request, String cookie) {
        String userId = CookieUtils.get(cookie, USER_ID_KEY);
        request.setHeader(HOST, "feedads.baidu.com");
        request.setHeader(ORIGIN, "http://feedads.baidu.com");
        request.setHeader(REFERER, String.format("http://feedads.baidu.com/nirvana/main.html?userid=%s", userId));
    }

    @Override
    protected Result response(String content) {
        Gson gson = new Gson();
        Response response = gson.fromJson(content, Response.class);
        Result result = new Result();
        //Code为0是操作成功.
        if (response.getStatus() == 200 && response.getError() == null) {
            result.setSuccess(true);
            result.setMessage(SUCCESS);
        } else {
            result.setSuccess(false);
            result.setMessage(FAIL);
        }
        return result;
    }


    private class Response {
        private int status;
        private Object data;
        private Object error;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }

        public Object getError() {
            return error;
        }

        public void setError(Object error) {
            this.error = error;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "status=" + status +
                    ", data=" + data +
                    ", error=" + error +
                    '}';
        }
    }
}
