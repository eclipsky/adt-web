package com.dataeye.ad.assistor.module.mediumaccount.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.mediumaccount.model.CrawlerPage;

/**
 * 爬虫页面映射器
 * create by ldj 2017-06-23
 * */
@MapperScan
public interface CrawlerPageMapper {

	/**
	 * 根据媒体id查询爬虫页面
	 * **/
	public List<CrawlerPage> query(Integer mediumId);
}
