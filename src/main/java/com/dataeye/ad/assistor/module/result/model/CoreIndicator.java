package com.dataeye.ad.assistor.module.result.model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;

/**
 * 核心指标
 * Created by huangzehai on 2017/4/6.
 */
public class CoreIndicator {
    /**
     * 累计消耗。
     */
    @Expose
    private BigDecimal cost;

    /**
     * 真实消耗.
     */
    private BigDecimal realCost;
    /**
     * 累计充值.
     */
    @Expose
    private BigDecimal recharge;

    /**
     * 回本率
     */
    @Expose
    private Double paybackRate;

    /**
     * 媒体账户余额
     */
    @Expose
    private BigDecimal balance;
    /**
     * 总体CPA
     */
    @Expose
    private BigDecimal cpa;
    /**
     * DAU.
     */
    @Expose
    private Long dau;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getRecharge() {
        return recharge;
    }

    public void setRecharge(BigDecimal recharge) {
        this.recharge = recharge;
    }

    public Double getPaybackRate() {
        return paybackRate;
    }

    public void setPaybackRate(Double paybackRate) {
        this.paybackRate = paybackRate;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getCpa() {
        return cpa;
    }

    public void setCpa(BigDecimal cpa) {
        this.cpa = cpa;
    }

    public Long getDau() {
        return dau;
    }

    public void setDau(Long dau) {
        this.dau = dau;
    }

    public BigDecimal getRealCost() {
        return realCost;
    }

    public void setRealCost(BigDecimal realCost) {
        this.realCost = realCost;
    }

    @Override
    public String toString() {
        return "CoreIndicator{" +
                "cost=" + cost +
                ", realCost=" + realCost +
                ", recharge=" + recharge +
                ", paybackRate=" + paybackRate +
                ", balance=" + balance +
                ", cpa=" + cpa +
                ", dau=" + dau +
                '}';
    }
}
