package com.dataeye.ad.assistor.module.advertisement.mapper;

import com.dataeye.ad.assistor.module.advertisement.model.TargetingSelectVo;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * @author lingliqi
 * @date 2017-12-19 17:03
 */
@MapperScan
public interface ApiTxAdTargetingMapper {

    /**
     * 查询公共定向包
     *
     * @param mediumAccountId 媒体账号ID
     * @param isPublic        是否为公共定向包
     * @return
     */
    List<TargetingSelectVo> queryForSelect(@Param("mediumAccountId") Integer mediumAccountId, @Param("isPublic") int isPublic);

    /**
     * 新增定向
     *
     * @param vo
     */
    void add(TargetingSelectVo vo);

    /**
     * 删除定向
     *
     * @param mediumAccountId
     * @param targetingId
     */
    void delete(@Param("mediumAccountId") Integer mediumAccountId, @Param("targetingId") Integer targetingId);

    /**
     * 更新定向
     *
     * @param vo
     */
    void update(TargetingSelectVo vo);

    /**
     * 查询指定记录是否存在
     *
     * @param mediumAccountId
     * @param targetingName
     * @param targetingId
     * @return
     */
    Integer getRecordByTargetingName(@Param("mediumAccountId") Integer mediumAccountId,
                                     @Param("targetingName") String targetingName, @Param("targetingId") Integer targetingId);
}
