package com.dataeye.ad.assistor.module.realtimedelivery.constant;

/**
 * Created by huangzehai on 2017/6/28.
 */
public final class Constants {
    private Constants() {

    }

    /**
     * 是否启用出价服务键.
     */
    public static final String ENABLE_BID_SERVICE = "enable_bid_service";
}
