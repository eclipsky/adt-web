package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.constant.AccountRole;
import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.module.realtimedelivery.service.UpsAndDownsPreferenceService;
import com.dataeye.ad.assistor.module.report.constant.Indicators;
import com.dataeye.ad.assistor.module.report.constant.MenuIds;
import com.dataeye.ad.assistor.module.report.mapper.IndicatorMapper;
import com.dataeye.ad.assistor.module.report.model.Indicator;
import com.dataeye.ad.assistor.module.report.model.IndicatorHabit;
import com.dataeye.ad.assistor.module.report.model.IndicatorPreferenceQuery;
import com.dataeye.ad.assistor.util.IndicatorUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by huangzehai on 2017/6/8.
 */
@Service
public class IndicatorServiceImpl implements IndicatorService {

    private Logger logger = LoggerFactory.getLogger(IndicatorServiceImpl.class);

    /**
     * 账号角色：代理
     */
    private static final int PROXY = 4;
    /**
     * 开发工程师
     */
    private static final int DEVELOPER = 3;

    /**
     * 操作成功
     */
    private static final int SUCCESS = 1;

    private List<Indicator> indicators;

    @Autowired
    private IndicatorMapper indicatorMapper;

    @Autowired
    private UpsAndDownsPreferenceService upsAndDownsPreferenceService;


    /**
     * 更新指标偏好
     *
     * @param indicatorHabit
     * @return
     */
    @Override
    public int updateIndicatorPreference(IndicatorHabit indicatorHabit) {
        return indicatorMapper.updateIndicatorPreference(indicatorHabit);
    }

    /**
     * 获取用户的某个菜单的指标显示偏好.
     *
     * @param query
     * @return
     */
    @Override
    public List<Indicator> getIndicatorPreference(IndicatorPreferenceQuery query) {
        logger.info("Get indicator preference for query {}", query);
        String indicatorPreference = indicatorMapper.getIndicatorPreference(query);
        List<Indicator> indicators = this.listIndicators();
        Map<String, Indicator> indicatorMap = listToMap(indicators);
        List<Indicator> indicatorList = new ArrayList<>();
        //解析指标偏好
        if (StringUtils.isNotBlank(indicatorPreference)) {
            //按逗号分隔符拆分指标
            String[] prefs = indicatorPreference.split(Constant.Separator.COMMA);
            for (String pref : prefs) {
                //拆分指标及其显示偏好
                String[] indicatorAndVisible = pref.split(Constant.Separator.COLON);
                if (indicatorAndVisible != null && indicatorAndVisible.length == 2) {
                    Indicator ind = indicatorMap.get(indicatorAndVisible[0]);
                    if (ind != null) {
                        Indicator indicator = new Indicator();
                        indicator.setIndicatorId(ind.getIndicatorId());
                        indicator.setIndicatorKey(ind.getIndicatorKey());
                        indicator.setIndicatorName(ind.getIndicatorName());
                        indicator.setIndicatorVisible(Integer.valueOf(indicatorAndVisible[1]));
                        indicatorList.add(indicator);
                    }
                }
            }
        }
        if (indicatorList.isEmpty() && query.getAccountRole() == AccountRole.BUSINESS_ACCOUNT && !query.isInitialized()) {
            logger.info("Initialize preference for business account {}", query.getAccountId());
            initPreference(query);
            //再次获取指标偏好
            indicatorList = this.getIndicatorPreference(query);
        }
        return indicatorList;
    }

    /**
     * 初始化指标偏好和涨跌幅偏好
     *
     * @param query
     */
    private void initPreference(IndicatorPreferenceQuery query) {
        //给企业账号创建指标偏好
        String indicatorPermission = StringUtils.join(Indicators.BUSINESS_ACCOUNT_INDICATORS, Constant.Separator.COMMA);
        query.setInitialized(true);
        this.addIndicatorPreference(query.getCompanyId(), query.getAccountId(), query.getAccountRole(), indicatorPermission,query.getMenuId());
    }

    /**
     * 列出所有的指标.
     *
     * @return
     */
    @Override
    public List<Indicator> listIndicators() {
        if (this.indicators == null) {
            this.indicators = indicatorMapper.listIndicators();
        }
        return this.indicators;
    }

    /**
     * 给新建的ADT系统账号添加默认的指标偏好设定。
     *
     * @param companyId
     * @param accountId
     * @param indicatorPermission
     * @param menuId
     * @return
     */
    @Override
    public int addIndicatorPreference(int companyId, int accountId, int accountRole, String indicatorPermission,String menuId) {
        //如果是开发工程师，不添加指标偏好
        if (accountRole == DEVELOPER) {
            return SUCCESS;
        }

        //添加LTV指标偏好
        if(menuId == null || menuId.equals(MenuIds.LTV_ANALYSIS_MENU_ID)){
        	IndicatorHabit ltvIndicatorPreference = new IndicatorHabit();
            ltvIndicatorPreference.setCompanyId(companyId);
            ltvIndicatorPreference.setAccountId(accountId);
            ltvIndicatorPreference.setMenuId(MenuIds.LTV_ANALYSIS_MENU_ID);
            ltvIndicatorPreference.setIndicatorHabit(generateIndicatorPreference(Indicators.DAILY_LTV_ANALYSIS_INDICATORS));
            indicatorMapper.insertIndicatorPreference(ltvIndicatorPreference);
        }

        //添加投放日报指标偏好
        if(menuId == null || menuId.equals(MenuIds.DAILY_DELIVERY_REPORT_MENU_ID)){
        	IndicatorHabit deliveryIndicatorPreference = new IndicatorHabit();
        	deliveryIndicatorPreference.setCompanyId(companyId);
        	deliveryIndicatorPreference.setAccountId(accountId);
        	deliveryIndicatorPreference.setMenuId(MenuIds.DAILY_DELIVERY_REPORT_MENU_ID);
        	deliveryIndicatorPreference.setIndicatorHabit(generateIndicatorPreference(indicatorPermission, Indicators.DAILY_DELIVERY_REPORT_INDICATORS));
        	indicatorMapper.insertIndicatorPreference(deliveryIndicatorPreference);
        }

        //添加实时操盘指标偏好
        if(menuId == null || menuId.equals(MenuIds.REAL_TIME_OPERATION_REPORT_MENU_ID)){
        	IndicatorHabit realTimeIndicatorPreference = new IndicatorHabit();
        	realTimeIndicatorPreference.setCompanyId(companyId);
        	realTimeIndicatorPreference.setAccountId(accountId);
        	realTimeIndicatorPreference.setMenuId(MenuIds.REAL_TIME_OPERATION_REPORT_MENU_ID);
        	//实时操盘不需要指标权限控制,但不给代理账号开放出价和切换计划开关的功能
        	if (accountRole == PROXY) {
        		realTimeIndicatorPreference.setIndicatorHabit(generateIndicatorPreference(indicatorPermission, Indicators.PROXY_REAL_TIME_OPERATION_INDICATORS));
        	} else {
        		realTimeIndicatorPreference.setIndicatorHabit(generateIndicatorPreference(indicatorPermission, Indicators.REAL_TIME_OPERATION_INDICATORS));
        	}
        	indicatorMapper.insertIndicatorPreference(realTimeIndicatorPreference);        	
        }
        
      //添加回本日报指标偏好
        if(menuId == null || menuId.equals(MenuIds.PAYBACK_REPORT_MENU_ID)){
        	IndicatorHabit paybackIndicatorPreference = new IndicatorHabit();
        	paybackIndicatorPreference.setCompanyId(companyId);
        	paybackIndicatorPreference.setAccountId(accountId);
        	paybackIndicatorPreference.setMenuId(MenuIds.PAYBACK_REPORT_MENU_ID);
        	paybackIndicatorPreference.setIndicatorHabit(generateIndicatorPreference(Indicators.PAYBACK_REPORT_INDICATORS));
        	indicatorMapper.insertIndicatorPreference(paybackIndicatorPreference);
        }
        return 0;
    }

    /**
     * 因为指标偏好权限发生变动后更改用户的指标偏好
     *
     * @param companyId
     * @param accountId
     * @param accountRole
     * @param indicatorPermission
     * @return
     */
    @Override
    public void updateIndicatorPreference(int companyId, int accountId, int accountRole, String indicatorPermission) {
        if (accountRole == DEVELOPER) {
            //账号角色由优化师或代理转为开发工程师,删除该账号的指标偏好
            indicatorMapper.deleteIndicatorPreferenceByAccountId(accountId);
        } else {
            if (indicatorMapper.hasIndicatorPreference(accountId)) {
                updateDailyDeliveryReportIndicatorPreference(companyId, accountId, indicatorPermission);
                updateRealTimeOperationIndicatorPreference(companyId, accountId, indicatorPermission);
            } else {
                //原来是开发工程师，现在变成了优化师或者代理
                this.addIndicatorPreference(companyId, accountId, accountRole, indicatorPermission,null);
            }
        }
    }

    /**
     * 更新投放日报指标偏好
     *
     * @param companyId
     * @param accountId
     * @param indicatorPermission
     */
    private void updateDailyDeliveryReportIndicatorPreference(int companyId, int accountId, String indicatorPermission) {
        IndicatorPreferenceQuery query = new IndicatorPreferenceQuery();
        query.setAccountId(accountId);
        query.setMenuId(MenuIds.DAILY_DELIVERY_REPORT_MENU_ID);
        String indicatorPreference = indicatorMapper.getIndicatorPreference(query);
        IndicatorHabit indicatorHabit = new IndicatorHabit();
        indicatorHabit.setAccountId(accountId);
        indicatorHabit.setCompanyId(companyId);
        indicatorHabit.setMenuId(MenuIds.DAILY_DELIVERY_REPORT_MENU_ID);
        indicatorHabit.setIndicatorHabit(generateIndicatorPreference(indicatorPermission, indicatorPreference, Indicators.DAILY_DELIVERY_REPORT_INDICATORS));
        this.updateIndicatorPreference(indicatorHabit);
    }

    /**
     * 更新实时投放指标偏好
     *
     * @param companyId
     * @param accountId
     * @param indicatorPermission
     */
    private void updateRealTimeOperationIndicatorPreference(int companyId, int accountId, String indicatorPermission) {
        IndicatorPreferenceQuery query = new IndicatorPreferenceQuery();
        query.setAccountId(accountId);
        query.setMenuId(MenuIds.REAL_TIME_OPERATION_REPORT_MENU_ID);
        String indicatorPreference = indicatorMapper.getIndicatorPreference(query);
        IndicatorHabit indicatorHabit = new IndicatorHabit();
        indicatorHabit.setAccountId(accountId);
        indicatorHabit.setCompanyId(companyId);
        indicatorHabit.setMenuId(MenuIds.REAL_TIME_OPERATION_REPORT_MENU_ID);
        indicatorHabit.setIndicatorHabit(generateIndicatorPreference(indicatorPermission, indicatorPreference, Indicators.REAL_TIME_OPERATION_INDICATORS));
        this.updateIndicatorPreference(indicatorHabit);
    }

    /**
     * 生成指标偏好
     *
     * @param indicatorPermission
     * @param indicators
     * @return
     */
    private String generateIndicatorPreference(String indicatorPermission, List<String> indicators) {
        List<Indicator> indicatorList = this.listIndicators();
        List<String> authorizedIndicators = getAuthorizedIndicators(indicatorPermission, indicators);
        StringBuilder indicatorBuilder = new StringBuilder();
        Map<String, Indicator> indicatorMap = listToMap(indicatorList);
        int index = 0;
        for (String indicator : authorizedIndicators) {
            indicatorBuilder.append(indicator);
            indicatorBuilder.append(Constant.Separator.COLON);
            indicatorBuilder.append(indicatorMap.get(indicator).getIndicatorVisible());
            if (index < authorizedIndicators.size() - 1) {
                indicatorBuilder.append(Constant.Separator.COMMA);
            }
            index++;
        }
        return indicatorBuilder.toString();
    }

    /**
     * 生成指标偏好
     * 不进行指标权限过滤
     *
     * @param indicatorPermission
     * @param indicators
     * @return
     */
    private String generateIndicatorPreference(List<String> indicators) {
        List<Indicator> indicatorList = this.listIndicators();
        StringBuilder indicatorBuilder = new StringBuilder();
        Map<String, Indicator> indicatorMap = listToMap(indicatorList);
        int index = 0;
        for (String indicator : indicators) {
            indicatorBuilder.append(indicator);
            indicatorBuilder.append(Constant.Separator.COLON);
            indicatorBuilder.append(indicatorMap.get(indicator).getIndicatorVisible());
            if (index < indicators.size() - 1) {
                indicatorBuilder.append(Constant.Separator.COMMA);
            }
            index++;
        }
        return indicatorBuilder.toString();
    }


    /**
     * 由变动的指标权限和旧的指标偏好生成新的指标偏好
     *
     * @param indicatorPermission
     * @param oldIndicatorPreference
     * @param indicators
     * @return
     */
    private String generateIndicatorPreference(String indicatorPermission, String oldIndicatorPreference, List<String> indicators) {
        List<String> authorizedIndicators = getAuthorizedIndicators(indicatorPermission, indicators);
        List<Indicator> indicatorList = this.listIndicators();
        StringBuilder indicatorBuilder = new StringBuilder();
        Map<String, Indicator> indicatorMap = listToMap(indicatorList);
        Map<String, Integer> oldPrefs = IndicatorUtils.parseIndicatorPreference(oldIndicatorPreference);
        int index = 0;
        for (String indicatorKey : authorizedIndicators) {
            indicatorBuilder.append(indicatorKey);
            indicatorBuilder.append(Constant.Separator.COLON);
            //对已授权的指标，使用用户的偏好设定
            if (oldPrefs.containsKey(indicatorKey)) {
                indicatorBuilder.append(oldPrefs.get(indicatorKey));
            } else {
                //对新增的授权指标，使用默认的偏好设定
                Indicator indicator = indicatorMap.get(indicatorKey);
                int visible;
                if (indicator == null) {
                    //设置为默认不显示.
                    visible = 0;
                } else {
                    visible = indicator.getIndicatorVisible();
                }
                indicatorBuilder.append(visible);
            }
            if (index < authorizedIndicators.size() - 1) {
                indicatorBuilder.append(Constant.Separator.COMMA);
            }
            index++;
        }
        return indicatorBuilder.toString();
    }


    /**
     * 获取授权访问的指标列表
     *
     * @param indicatorPermission
     * @param indicators
     * @return
     */
    private List<String> getAuthorizedIndicators(String indicatorPermission, List<String> indicators) {
        List<String> authorizedIndicators = new ArrayList<>();
        if (StringUtils.isNotBlank(indicatorPermission)) {
            List<String> authenticatedIndicators = new ArrayList<>();
            for (String indicatorKey : indicatorPermission.split(Constant.Separator.COMMA)) {
                authenticatedIndicators.add(indicatorKey.trim());
            }
            for (String indicator : indicators) {
                if (authenticatedIndicators.contains(indicator)) {
                    authorizedIndicators.add(indicator);
                }
            }
        }
        return authorizedIndicators;
    }

    /**
     * 将指标列表转化为以指标ID为键的Map
     *
     * @param indicators
     * @return
     */
    private Map<String, Indicator> listToMap(List<Indicator> indicators) {
        Map<String, Indicator> map = new HashMap<>();
        if (indicators != null) {
            for (Indicator indicator : indicators) {
                map.put(indicator.getIndicatorId(), indicator);
            }
        }
        return map;
    }
}
