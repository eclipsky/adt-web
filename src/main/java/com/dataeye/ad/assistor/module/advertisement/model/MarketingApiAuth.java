package com.dataeye.ad.assistor.module.advertisement.model;


/**
 * GDT授权账户表
 * @author ldj
 *
 */
public class MarketingApiAuth {

	/**广告主账号id**/
	private Integer accountId;
	private String accessToken;
	/**媒体帐号ID**/
	private Integer mediumAccountId;
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	@Override
	public String toString() {
		return "MarketingApiAuth [accountId=" + accountId + ", accessToken="
				+ accessToken + ", mediumAccountId=" + mediumAccountId + "]";
	}
	
	
}
