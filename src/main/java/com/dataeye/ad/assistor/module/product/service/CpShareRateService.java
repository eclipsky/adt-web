package com.dataeye.ad.assistor.module.product.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.product.mapper.CpShareRateMapper;
import com.dataeye.ad.assistor.module.product.model.CpShareRate;

/**
 * CP分成率Service
 * @author luzhuyou 2017/02/20
 *
 */
@Service
public class CpShareRateService {

	@Autowired
	private CpShareRateMapper cpShareRateMapper;
	
	/**
	 * 新增CP分成率
	 * @param companyId
	 * @param productId
	 * @param shareRate
	 * @return
	 */
	public int add(int companyId, int productId, double shareRate) {
		CpShareRate cpShareRate = new CpShareRate();
		cpShareRate.setCompanyId(companyId);
		cpShareRate.setProductId(productId);
		cpShareRate.setShareRate(shareRate);
		cpShareRate.setCreateTime(new Date());
		cpShareRate.setUpdateTime(new Date());
		cpShareRateMapper.add(cpShareRate);
		return cpShareRate.getShareRateId();
	}

	/**
	 * 修改CP分成率
	 * @param productId
	 * @param shareRate
	 * @return
	 */
	public int modify(int productId, double shareRate) {
		CpShareRate cpShareRate = new CpShareRate();
		cpShareRate.setProductId(productId);
		cpShareRate.setShareRate(shareRate);
		cpShareRate.setUpdateTime(new Date());
		return cpShareRateMapper.update(cpShareRate);
	}

}
