package com.dataeye.ad.assistor.module.realtimedelivery.model;

import java.util.Date;

/**
 * 操作日志
 * Created by huangzehai on 2017/6/28.
 */
public class OperationLog {
    /**
     * 操作Id.
     */
    private long opId;
    /**
     * 公司ID.
     */
    private int companyId;
    /**
     * 计划ID.
     */
    private int planId;

    /**
     * 计划名称
     */
    private String planName;
    /**
     * 操作时间
     */
    private Date opTime;
    /**
     * ADT账号ID.
     */
    private int accountId;

    /**
     * ADT账号名称.
     */
    private String accountName;

    /**
     * 操作编码.
     */
    private int opCode;
    /**
     * 操作名称
     */
    private String opName;
    /**
     * 改动后的值
     */
    private String currentValue;
    /**
     * 改动前的值
     */
    private String previousValue;
    /**
     * 结果:1:成功;0:失败.
     */
    private int result;

    private String message;

    public long getOpId() {
        return opId;
    }

    public void setOpId(long opId) {
        this.opId = opId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public Date getOpTime() {
        return opTime;
    }

    public void setOpTime(Date opTime) {
        this.opTime = opTime;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getOpName() {
        return opName;
    }

    public void setOpName(String opName) {
        this.opName = opName;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public String getPreviousValue() {
        return previousValue;
    }

    public void setPreviousValue(String previousValue) {
        this.previousValue = previousValue;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOpCode() {
        return opCode;
    }

    public void setOpCode(int opCode) {
        this.opCode = opCode;
    }

    @Override
    public String toString() {
        return "OperationLog{" +
                "opId=" + opId +
                ", companyId=" + companyId +
                ", planId=" + planId +
                ", planName='" + planName + '\'' +
                ", opTime=" + opTime +
                ", accountId=" + accountId +
                ", accountName='" + accountName + '\'' +
                ", opCode=" + opCode +
                ", opName='" + opName + '\'' +
                ", currentValue='" + currentValue + '\'' +
                ", previousValue='" + previousValue + '\'' +
                ", result=" + result +
                ", message='" + message + '\'' +
                '}';
    }
}
