package com.dataeye.ad.assistor.module.report.model;

import com.dataeye.ad.assistor.module.report.constant.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * 投放日报视图
 * Created by huangzehai on 2017/5/11.
 */
public class DeliveryReportView {
    @Expose
    private Long id;

    /**
     * 日期|日期范围
     */
    @Expose
    @SerializedName(value = "date")
    private Link dateLink;

    /**
     * 媒体|平台|渠道.
     */
    @Expose
    @SerializedName(value = "medium")
    private Link mediumLink;

    /**
     * 媒体账号
     */
    @Expose
    @SerializedName(value = "mediumAccount")
    private Link accountLink;

    /**
     * 计划|策略
     */
    @Expose
    @SerializedName(value = "plan")
    private Link planLink;


    /**
     * 总消耗|总花费文本形式.
     */
    @Expose
    private String cost;

    /**
     * 曝光数.
     */
    @Expose
    private Integer exposures;
    /**
     * 点击数.
     */
    @Expose
    private Integer clicks;

    /**
     * 未排重点击数.
     */
    @Expose
    private Integer totalClicks;

    /**
     * 点击率.
     */
    @Expose
    private String ctr;


    /**
     * CPC.平均每点击花费.
     */
    @Expose
    private String cpc;

    /**
     * 到达数
     */
    @Expose
    private Integer reaches;

    /**
     * 到达率.
     */
    @Expose
    private String reachRate;

    /**
     * 停留时长
     */
    @Expose
    private String retentionTime;

    /**
     * 下载点击数独立访客（UV）
     */
    @Expose
    private Integer downloads;

    /**
     * 下载率.
     */
    @Expose
    private String downloadRate;
    /**
     * 激活数.
     */
    @Expose
    private Integer activations;

    /**
     * 激活率.
     */
    @Expose
    private String activationRate;

    /**
     * 激活CPA.
     */
    @Expose
    private String activationCpa;

    /**
     * 注册数.
     */
    @Expose
    private Integer registrations;

    /**
     * 点击注册率.
     * */
    @Expose
    private String registrationRate;

    /**
     * 注册CPA.
     */
    @Expose
    private String registrationCpa;
    
    /**
     * 首日注册CPA(帐号)
     */
    @Expose
    private String registrationAccountNumCpa;

    /**
     * 激活注册率
     */
    @Expose
    private String activationRegistrationRate;

    /**
     * 次日留存率.
     */
    @Expose
    private String day2RetentionRate = Constants.UNKNOWN;


    /**
     * 三日留存率.
     */
    @Expose
    private String day3RetentionRate = Constants.UNKNOWN;


    /**
     * 七日留存率.
     */
    @Expose
    private String day7RetentionRate = Constants.UNKNOWN;


    /**
     * 15日留存率.
     */
    @Expose
    private String day15RetentionRate = Constants.UNKNOWN;
    
    /**
     * 三十日留存率.
     */
    @Expose
    private String day30RetentionRate = Constants.UNKNOWN;

    /**
     * 新增充值人数.
     */
    @Expose
    private Integer newlyIncreasedRecharges;

    /**
     * 新增充值金额.
     */
    @Expose
    private BigDecimal newlyIncreasedRechargeAmount;

    /**
     * 新增用户付费率
     */
    @Expose
    private String newlyIncreasedPayRate;

    /**
     * 首日回本率.
     */
    @Expose
    private String firstDayPaybackRate;

    /**
     * 首日LTV
     */
    @Expose
    private String ltv1Day;

    /**
     * 7日LTV
     */
    @Expose
    private String ltv7Days;

    /**
     * 30日LTV
     */
    @Expose
    private String ltv30Days;

    /**
     * 投放包总充值.
     */
    @Expose
    private BigDecimal totalRechargeAmount = new BigDecimal(0);

    /**
     * 总充值人数.
     */
    @Expose
    private String totalRecharges;

    /**
     * 累计充值
     */
    @Expose
    private String accumulatedRecharge;

    /**
     * 付费率.
     */
    @Expose
    private String payRate;

    /**
     * 日活跃用户DAU.
     */
    @Expose
    private Integer dau;

    /**
     * 是否自然流量
     */
    @Expose
    private boolean isNaturalFlow;

    /**
     * 媒体账号.(仅供下载表格使用)
     */
    private String mediumAccount;
    
    /**
     * 媒体类型
     * */
    @Expose
    private Integer mediumType;
    
    /**
     * 媒体名称
     * */
    @Expose
    private String mediumName;
    
    /**
     * 注册帐号数
     */
    @Expose
    private Integer registerAccountNum;
    
  //add indicator by ldj 2018-03-06
    /**
     * 首日注册设备数
     * */
    @Expose
    private Integer firstDayRegistrations;
    
    /**
     * 首日注册账号数
     * */
    @Expose
    private Integer firstDayRegisterAccountNum;
    
    /**
     * 子账号名称
     */
    @Expose
    private String systemAccountName;

    /**
     * 账号id
     */
    @Expose
    private Integer systemAccountId;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Link getDateLink() {
        return dateLink;
    }

    public void setDateLink(Link dateLink) {
        this.dateLink = dateLink;
    }

    public Link getMediumLink() {
        return mediumLink;
    }

    public void setMediumLink(Link mediumLink) {
        this.mediumLink = mediumLink;
    }

    public Link getAccountLink() {
        return accountLink;
    }

    public void setAccountLink(Link accountLink) {
        this.accountLink = accountLink;
    }

    public Link getPlanLink() {
        return planLink;
    }

    public void setPlanLink(Link planLink) {
        this.planLink = planLink;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public Integer getExposures() {
        return exposures;
    }

    public void setExposures(Integer exposures) {
        this.exposures = exposures;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Integer getTotalClicks() {
        return totalClicks;
    }

    public void setTotalClicks(Integer totalClicks) {
        this.totalClicks = totalClicks;
    }

    public String getCtr() {
        return ctr;
    }

    public void setCtr(String ctr) {
        this.ctr = ctr;
    }

    public String getCpc() {
        return cpc;
    }

    public void setCpc(String cpc) {
        this.cpc = cpc;
    }

    public Integer getReaches() {
        return reaches;
    }

    public void setReaches(Integer reaches) {
        this.reaches = reaches;
    }

    public String getReachRate() {
        return reachRate;
    }

    public void setReachRate(String reachRate) {
        this.reachRate = reachRate;
    }

    public String getRetentionTime() {
        return retentionTime;
    }

    public void setRetentionTime(String retentionTime) {
        this.retentionTime = retentionTime;
    }

    public Integer getDownloads() {
        return downloads;
    }

    public void setDownloads(Integer downloads) {
        this.downloads = downloads;
    }

    public String getDownloadRate() {
        return downloadRate;
    }

    public void setDownloadRate(String downloadRate) {
        this.downloadRate = downloadRate;
    }

    public Integer getActivations() {
        return activations;
    }

    public void setActivations(Integer activations) {
        this.activations = activations;
    }

    public String getActivationRate() {
        return activationRate;
    }

    public void setActivationRate(String activationRate) {
        this.activationRate = activationRate;
    }

    public String getActivationCpa() {
        return activationCpa;
    }

    public void setActivationCpa(String activationCpa) {
        this.activationCpa = activationCpa;
    }

    public Integer getRegistrations() {
        return registrations;
    }

    public void setRegistrations(Integer registrations) {
        this.registrations = registrations;
    }

    public String getRegistrationRate() {
        return registrationRate;
    }

    public void setRegistrationRate(String registrationRate) {
        this.registrationRate = registrationRate;
    }

    public String getRegistrationCpa() {
        return registrationCpa;
    }

    public void setRegistrationCpa(String registrationCpa) {
        this.registrationCpa = registrationCpa;
    }

    public String getActivationRegistrationRate() {
        return activationRegistrationRate;
    }

    public void setActivationRegistrationRate(String activationRegistrationRate) {
        this.activationRegistrationRate = activationRegistrationRate;
    }

    public String getDay2RetentionRate() {
        return day2RetentionRate;
    }

    public void setDay2RetentionRate(String day2RetentionRate) {
        this.day2RetentionRate = day2RetentionRate;
    }

    public String getDay3RetentionRate() {
        return day3RetentionRate;
    }

    public void setDay3RetentionRate(String day3RetentionRate) {
        this.day3RetentionRate = day3RetentionRate;
    }

    public String getDay7RetentionRate() {
        return day7RetentionRate;
    }

    public void setDay7RetentionRate(String day7RetentionRate) {
        this.day7RetentionRate = day7RetentionRate;
    }

    public String getDay30RetentionRate() {
        return day30RetentionRate;
    }

    public void setDay30RetentionRate(String day30RetentionRate) {
        this.day30RetentionRate = day30RetentionRate;
    }

    public Integer getNewlyIncreasedRecharges() {
        return newlyIncreasedRecharges;
    }

    public void setNewlyIncreasedRecharges(Integer newlyIncreasedRecharges) {
        this.newlyIncreasedRecharges = newlyIncreasedRecharges;
    }

    public BigDecimal getNewlyIncreasedRechargeAmount() {
        return newlyIncreasedRechargeAmount;
    }

    public void setNewlyIncreasedRechargeAmount(BigDecimal newlyIncreasedRechargeAmount) {
        this.newlyIncreasedRechargeAmount = newlyIncreasedRechargeAmount;
    }

    public String getNewlyIncreasedPayRate() {
        return newlyIncreasedPayRate;
    }

    public void setNewlyIncreasedPayRate(String newlyIncreasedPayRate) {
        this.newlyIncreasedPayRate = newlyIncreasedPayRate;
    }

    public String getFirstDayPaybackRate() {
        return firstDayPaybackRate;
    }

    public void setFirstDayPaybackRate(String firstDayPaybackRate) {
        this.firstDayPaybackRate = firstDayPaybackRate;
    }

    public String getLtv1Day() {
        return ltv1Day;
    }

    public void setLtv1Day(String ltv1Day) {
        this.ltv1Day = ltv1Day;
    }

    public BigDecimal getTotalRechargeAmount() {
        return totalRechargeAmount;
    }

    public void setTotalRechargeAmount(BigDecimal totalRechargeAmount) {
        this.totalRechargeAmount = totalRechargeAmount;
    }

    public String getTotalRecharges() {
        return totalRecharges;
    }

    public void setTotalRecharges(String totalRecharges) {
        this.totalRecharges = totalRecharges;
    }

    public String getAccumulatedRecharge() {
        return accumulatedRecharge;
    }

    public void setAccumulatedRecharge(String accumulatedRecharge) {
        this.accumulatedRecharge = accumulatedRecharge;
    }

    public String getPayRate() {
        return payRate;
    }

    public void setPayRate(String payRate) {
        this.payRate = payRate;
    }

    public Integer getDau() {
        return dau;
    }

    public void setDau(Integer dau) {
        this.dau = dau;
    }

    public String getMediumAccount() {
        return mediumAccount;
    }

    public void setMediumAccount(String mediumAccount) {
        this.mediumAccount = mediumAccount;
    }

    public boolean isNaturalFlow() {
        return isNaturalFlow;
    }

    public void setNaturalFlow(boolean naturalFlow) {
        isNaturalFlow = naturalFlow;
    }

    public Integer getMediumType() {
		return mediumType;
	}

	public void setMediumType(Integer mediumType) {
		this.mediumType = mediumType;
	}

    public String getLtv7Days() {
        return ltv7Days;
    }

    public void setLtv7Days(String ltv7Days) {
        this.ltv7Days = ltv7Days;
    }

    public String getLtv30Days() {
        return ltv30Days;
    }

    public void setLtv30Days(String ltv30Days) {
        this.ltv30Days = ltv30Days;
    }
    

	public String getMediumName() {
		return mediumName;
	}

	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	public Integer getRegisterAccountNum() {
		return registerAccountNum;
	}

	public void setRegisterAccountNum(Integer registerAccountNum) {
		this.registerAccountNum = registerAccountNum;
	}

	public String getRegistrationAccountNumCpa() {
		return registrationAccountNumCpa;
	}

	public void setRegistrationAccountNumCpa(String registrationAccountNumCpa) {
		this.registrationAccountNumCpa = registrationAccountNumCpa;
	}

	public String getDay15RetentionRate() {
		return day15RetentionRate;
	}

	public void setDay15RetentionRate(String day15RetentionRate) {
		this.day15RetentionRate = day15RetentionRate;
	}

	public Integer getFirstDayRegistrations() {
		return firstDayRegistrations;
	}

	public void setFirstDayRegistrations(Integer firstDayRegistrations) {
		this.firstDayRegistrations = firstDayRegistrations;
	}

	public Integer getFirstDayRegisterAccountNum() {
		return firstDayRegisterAccountNum;
	}

	public void setFirstDayRegisterAccountNum(Integer firstDayRegisterAccountNum) {
		this.firstDayRegisterAccountNum = firstDayRegisterAccountNum;
	}

	public String getSystemAccountName() {
		return systemAccountName;
	}

	public void setSystemAccountName(String systemAccountName) {
		this.systemAccountName = systemAccountName;
	}

	public Integer getSystemAccountId() {
		return systemAccountId;
	}

	public void setSystemAccountId(Integer systemAccountId) {
		this.systemAccountId = systemAccountId;
	}

	@Override
	public String toString() {
		return "DeliveryReportView [id=" + id + ", dateLink=" + dateLink
				+ ", mediumLink=" + mediumLink + ", accountLink=" + accountLink
				+ ", planLink=" + planLink + ", cost=" + cost + ", exposures="
				+ exposures + ", clicks=" + clicks + ", totalClicks="
				+ totalClicks + ", ctr=" + ctr + ", cpc=" + cpc + ", reaches="
				+ reaches + ", reachRate=" + reachRate + ", retentionTime="
				+ retentionTime + ", downloads=" + downloads
				+ ", downloadRate=" + downloadRate + ", activations="
				+ activations + ", activationRate=" + activationRate
				+ ", activationCpa=" + activationCpa + ", registrations="
				+ registrations + ", registrationRate=" + registrationRate
				+ ", registrationCpa=" + registrationCpa
				+ ", registrationAccountNumCpa=" + registrationAccountNumCpa
				+ ", activationRegistrationRate=" + activationRegistrationRate
				+ ", day2RetentionRate=" + day2RetentionRate
				+ ", day3RetentionRate=" + day3RetentionRate
				+ ", day7RetentionRate=" + day7RetentionRate
				+ ", day15RetentionRate=" + day15RetentionRate
				+ ", day30RetentionRate=" + day30RetentionRate
				+ ", newlyIncreasedRecharges=" + newlyIncreasedRecharges
				+ ", newlyIncreasedRechargeAmount="
				+ newlyIncreasedRechargeAmount + ", newlyIncreasedPayRate="
				+ newlyIncreasedPayRate + ", firstDayPaybackRate="
				+ firstDayPaybackRate + ", ltv1Day=" + ltv1Day + ", ltv7Days="
				+ ltv7Days + ", ltv30Days=" + ltv30Days
				+ ", totalRechargeAmount=" + totalRechargeAmount
				+ ", totalRecharges=" + totalRecharges
				+ ", accumulatedRecharge=" + accumulatedRecharge + ", payRate="
				+ payRate + ", dau=" + dau + ", isNaturalFlow=" + isNaturalFlow
				+ ", mediumAccount=" + mediumAccount + ", mediumType="
				+ mediumType + ", mediumName=" + mediumName
				+ ", registerAccountNum=" + registerAccountNum
				+ ", firstDayRegistrations=" + firstDayRegistrations
				+ ", firstDayRegisterAccountNum=" + firstDayRegisterAccountNum
				+ ", systemAccountName=" + systemAccountName
				+ ", systemAccountId=" + systemAccountId + "]";
	}
}
