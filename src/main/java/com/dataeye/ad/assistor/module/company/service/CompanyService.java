package com.dataeye.ad.assistor.module.company.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.company.mapper.CompanyMapper;
import com.dataeye.ad.assistor.module.company.model.Company;

/**
 * 公司Service
 * @author luzhuyou 2017/05/10
 *
 */
@Service("companyService")
public class CompanyService {

	@Autowired
	private CompanyMapper companyMapper;

	/**
	 * 新增产品信息
	 * @param companyName
	 * @param shareRate
	 * @param status
	 * @param companyId
	 * @return
	 */
	public int add(Company company) {
		// 新增公司信息
		company.setCreateTime(new Date());
		company.setUpdateTime(new Date());
		companyMapper.add(company);
		
		return company.getCompanyId();
	}

	/**
	 * 根据公司ID获取公司信息
	 * @param companyId
	 * @return
	 */
	public Company get(int companyId) {
		Company company = companyMapper.get(companyId);
		return company;
	}

	/**
	 * 根据邮箱号获取公司信息
	 * @param email
	 * @return
	 */
	public Company getByEmail(String email) {
		Company company = companyMapper.getByEmail(email);
		return company;
	}
	
	/**
	 * 根据UID获取公司信息
	 * @param uid
	 * @return
	 */
	public Company getByUid(int uid) {
		Company company = companyMapper.getByUid(uid);
		return company;
	}

}
