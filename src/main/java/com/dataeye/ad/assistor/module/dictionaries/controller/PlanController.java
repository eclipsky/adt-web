package com.dataeye.ad.assistor.module.dictionaries.controller;

import com.dataeye.ad.assistor.context.*;
import com.dataeye.ad.assistor.module.dictionaries.model.PlanVo;
import com.dataeye.ad.assistor.module.dictionaries.service.PlanService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by luzhuyou
 * 投放计划控制器.
 */
@Controller
public class PlanController {

    @Autowired
    private PlanService planService;

    /**
     * 下拉框查询投放计划(根据系统账号所管辖媒体账号进行查询)
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/plan/queryPlanForSelector.do")
    public Object queryPlanForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = DEContextContainer.getContext(request);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (permissionMediumAccountIds == null) {
            return null;
        }
        DEParameter parameter = context.getDeParameter();
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        String mediumIds = parameter.getParameter(DEParameter.Keys.MEDIUM_IDS);
        String mediumAccountIds = parameter.getParameter(DEParameter.Keys.ACCOUNT_IDS);
        //构建查询对象
        PlanVo query = new PlanVo();
        query.setCompanyId(companyId);
        query.setPermissionMediumAccountIds(permissionMediumAccountIds);
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setMediumIds(StringUtil.stringToList(mediumIds));
        query.setMediumAccountIds(StringUtil.stringToList(mediumAccountIds));
        return planService.query(query);
    }

}
