package com.dataeye.ad.assistor.module.mailhistory.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.mailhistory.model.MailHistory;

/**
 * 邮件发送历史映射器.
 * Created by luzhuyou 2017/02/24
 */
@MapperScan
public interface MailHistoryMapper {

	/**
	 * 新增邮件发送历史
	 * @param mailHistory
	 * @return
	 */
	public int add(MailHistory mailHistory);

}
