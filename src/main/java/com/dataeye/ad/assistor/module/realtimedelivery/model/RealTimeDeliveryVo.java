package com.dataeye.ad.assistor.module.realtimedelivery.model;


import java.math.BigDecimal;

/**
 * 实时投放对象
 *
 * @author luzhuyou 2017/01/04
 */
public class RealTimeDeliveryVo {
    /**
     * 当前日期
     */
    private String currentDate;
    /**
     * 产品名称.
     */
    private String productName;
    /**
     * 媒体ID
     */
    private Integer mediumId;
    /**
     * 媒体名称
     */
    private String medium;
    /**
     * 媒体账号ID
     */
    private Integer mediumAccountId;
    /**
     * 媒体账号
     */
    private String account;
    /**
     * 计划ID
     */
    private Integer planId;
    /**
     * 计划名称
     */
    private String planName;
    /**
     * 计划来源，供返回给编辑媒体数据接口
     */
    private Integer planSource;
    /**
     * 媒体计划ID.
     */
    private String mediumPlanId;
    /**
     * 总消耗
     */
    private Double totalCost;
    /**
     * 曝光数
     */
    private Integer exposures;
    /**
     * 点击数
     */
    private Integer clicks;
    /**
     * 未排重点击数(ADT点击数)
     */
    private Integer totalClicks;
    /**
     * CTR
     */
    private Double ctr;
    /**
     * CPC.
     */
    private BigDecimal cpc;
    /**
     * 到达数.
     */
    private Integer reaches;
    /**
     * 到达率.
     */
    private Double reachRate;
    /**
     * 下载数
     */
    private Integer downloadUV;
    /**
     * 下载率
     */
    private Double downloadRate;
    /**
     * 停留时间.
     */
    private Integer retentionTime;
    /**
     * 激活数
     */
    private Integer activations;
    /**
     * 激活率.
     */
    private Double activationRate;
    /**
     * 激活CPA.
     */
    private BigDecimal activationCpa;
    /**
     * 首日注册设备数
     */
    private Integer registerNumFd;
    /**
     * 首日注册账号数
     */
    private Integer registerAccountNumFd;
    /**
     * 注册率.
     */
    private Double registrationRateFd;
    /**
     * 首日注册设备CPA
     */
    private Double costPerRegistrationFd;
    /**
     * 首日注册账号CPA
     */
    private Double costPerRegistrationAccountFd;
    /**
     * 出价，单位分。
     */
    private BigDecimal bid;
    /**
     * 出价策略.
     */
    private String bidStrategy;
    /**
     * 扩展字段
     */
    private String planExtend;
    /**
     * 媒体计划开关，1-打开，0-关闭
     */
    private int planSwitch;
    /**
     * 媒体URL
     */
    private String mediumURL;
    /**
     * 消耗告警阈值
     */
    private Double totalCostThreshold;
    /**
     * CPA告警阈值
     */
    private Double costPerRegistrationThreshold;
    /**
     * CTR告警阈值
     */
    private Double ctrThreshold;
    /**
     * 下载率告警阈值
     */
    private Double downloadRateThreshold;
    /**
     * 指标涨跌.
     */
    private IndicatorUpsAndDowns upsAndDowns;
    /**
     * 获取唯一键（medium|account|planID）
     *
     * @return
     */
    public String getKey() {
        if (medium != null && account != null && planId != null) {
            return medium.concat("|").concat(account).concat("|").concat(String.valueOf(planId));
        }
        return null;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Integer getPlanSource() {
        return planSource;
    }

    public void setPlanSource(Integer planSource) {
        this.planSource = planSource;
    }

    public String getMediumPlanId() {
        return mediumPlanId;
    }

    public void setMediumPlanId(String mediumPlanId) {
        this.mediumPlanId = mediumPlanId;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public Integer getExposures() {
        return exposures;
    }

    public void setExposures(Integer exposures) {
        this.exposures = exposures;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Integer getTotalClicks() {
        return totalClicks;
    }

    public void setTotalClicks(Integer totalClicks) {
        this.totalClicks = totalClicks;
    }

    public Double getCtr() {
        return ctr;
    }

    public void setCtr(Double ctr) {
        this.ctr = ctr;
    }

    public BigDecimal getCpc() {
        return cpc;
    }

    public void setCpc(BigDecimal cpc) {
        this.cpc = cpc;
    }

    public Integer getReaches() {
        return reaches;
    }

    public void setReaches(Integer reaches) {
        this.reaches = reaches;
    }

    public Double getReachRate() {
        return reachRate;
    }

    public void setReachRate(Double reachRate) {
        this.reachRate = reachRate;
    }

    public Integer getDownloadUV() {
        return downloadUV;
    }

    public void setDownloadUV(Integer downloadUV) {
        this.downloadUV = downloadUV;
    }

    public Double getDownloadRate() {
        return downloadRate;
    }

    public void setDownloadRate(Double downloadRate) {
        this.downloadRate = downloadRate;
    }

    public Integer getRetentionTime() {
        return retentionTime;
    }

    public void setRetentionTime(Integer retentionTime) {
        this.retentionTime = retentionTime;
    }

    public Integer getActivations() {
        return activations;
    }

    public void setActivations(Integer activations) {
        this.activations = activations;
    }

    public Double getActivationRate() {
        return activationRate;
    }

    public void setActivationRate(Double activationRate) {
        this.activationRate = activationRate;
    }

    public BigDecimal getActivationCpa() {
        return activationCpa;
    }

    public void setActivationCpa(BigDecimal activationCpa) {
        this.activationCpa = activationCpa;
    }

    public Integer getRegisterNumFd() {
        return registerNumFd;
    }

    public void setRegisterNumFd(Integer registerNumFd) {
        this.registerNumFd = registerNumFd;
    }

    public Integer getRegisterAccountNumFd() {
        return registerAccountNumFd;
    }

    public void setRegisterAccountNumFd(Integer registerAccountNumFd) {
        this.registerAccountNumFd = registerAccountNumFd;
    }

    public Double getRegistrationRateFd() {
        return registrationRateFd;
    }

    public void setRegistrationRateFd(Double registrationRateFd) {
        this.registrationRateFd = registrationRateFd;
    }

    public Double getCostPerRegistrationFd() {
        return costPerRegistrationFd;
    }

    public void setCostPerRegistrationFd(Double costPerRegistrationFd) {
        this.costPerRegistrationFd = costPerRegistrationFd;
    }

    public Double getCostPerRegistrationAccountFd() {
        return costPerRegistrationAccountFd;
    }

    public void setCostPerRegistrationAccountFd(Double costPerRegistrationAccountFd) {
        this.costPerRegistrationAccountFd = costPerRegistrationAccountFd;
    }

    public BigDecimal getBid() {
        return bid;
    }

    public void setBid(BigDecimal bid) {
        this.bid = bid;
    }

    public String getBidStrategy() {
        return bidStrategy;
    }

    public void setBidStrategy(String bidStrategy) {
        this.bidStrategy = bidStrategy;
    }

    public String getPlanExtend() {
        return planExtend;
    }

    public void setPlanExtend(String planExtend) {
        this.planExtend = planExtend;
    }

    public int getPlanSwitch() {
        return planSwitch;
    }

    public void setPlanSwitch(int planSwitch) {
        this.planSwitch = planSwitch;
    }

    public String getMediumURL() {
        return mediumURL;
    }

    public void setMediumURL(String mediumURL) {
        this.mediumURL = mediumURL;
    }

    public Double getTotalCostThreshold() {
        return totalCostThreshold;
    }

    public void setTotalCostThreshold(Double totalCostThreshold) {
        this.totalCostThreshold = totalCostThreshold;
    }

    public Double getCostPerRegistrationThreshold() {
        return costPerRegistrationThreshold;
    }

    public void setCostPerRegistrationThreshold(Double costPerRegistrationThreshold) {
        this.costPerRegistrationThreshold = costPerRegistrationThreshold;
    }

    public Double getCtrThreshold() {
        return ctrThreshold;
    }

    public void setCtrThreshold(Double ctrThreshold) {
        this.ctrThreshold = ctrThreshold;
    }

    public Double getDownloadRateThreshold() {
        return downloadRateThreshold;
    }

    public void setDownloadRateThreshold(Double downloadRateThreshold) {
        this.downloadRateThreshold = downloadRateThreshold;
    }

    public IndicatorUpsAndDowns getUpsAndDowns() {
        return upsAndDowns;
    }

    public void setUpsAndDowns(IndicatorUpsAndDowns upsAndDowns) {
        this.upsAndDowns = upsAndDowns;
    }

    @Override
    public String toString() {
        return "RealTimeDeliveryVo{" +
                "currentDate='" + currentDate + '\'' +
                ", productName='" + productName + '\'' +
                ", mediumId=" + mediumId +
                ", medium='" + medium + '\'' +
                ", mediumAccountId=" + mediumAccountId +
                ", account='" + account + '\'' +
                ", planId=" + planId +
                ", planName='" + planName + '\'' +
                ", planSource=" + planSource +
                ", mediumPlanId='" + mediumPlanId + '\'' +
                ", totalCost=" + totalCost +
                ", exposures=" + exposures +
                ", clicks=" + clicks +
                ", totalClicks=" + totalClicks +
                ", ctr=" + ctr +
                ", cpc=" + cpc +
                ", reaches=" + reaches +
                ", reachRate=" + reachRate +
                ", downloadUV=" + downloadUV +
                ", downloadRate=" + downloadRate +
                ", retentionTime=" + retentionTime +
                ", activations=" + activations +
                ", activationRate=" + activationRate +
                ", activationCpa=" + activationCpa +
                ", registerNumFd=" + registerNumFd +
                ", registerAccountNumFd=" + registerAccountNumFd +
                ", registrationRateFd=" + registrationRateFd +
                ", costPerRegistrationFd=" + costPerRegistrationFd +
                ", costPerRegistrationAccountFd=" + costPerRegistrationAccountFd +
                ", bid=" + bid +
                ", bidStrategy='" + bidStrategy + '\'' +
                ", planExtend='" + planExtend + '\'' +
                ", planSwitch=" + planSwitch +
                ", mediumURL='" + mediumURL + '\'' +
                ", totalCostThreshold=" + totalCostThreshold +
                ", costPerRegistrationThreshold=" + costPerRegistrationThreshold +
                ", ctrThreshold=" + ctrThreshold +
                ", downloadRateThreshold=" + downloadRateThreshold +
                ", upsAndDowns=" + upsAndDowns +
                '}';
    }
}
