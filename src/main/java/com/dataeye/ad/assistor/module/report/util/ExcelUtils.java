package com.dataeye.ad.assistor.module.report.util;

import com.dataeye.ad.assistor.common.ColumnInfo;
import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.module.report.constant.Columns;
import com.dataeye.ad.assistor.module.report.constant.Indicators;
import com.dataeye.ad.assistor.module.report.model.DeliveryReportView;
import com.dataeye.ad.assistor.module.report.model.Link;
import com.dataeye.ad.assistor.util.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.List;
import java.util.Map;

/**
 * Created by huangzehai on 2017/1/19.
 */
public final class ExcelUtils {
    private ExcelUtils() {

    }

    /**
     * 构建投放日报计划维度Excel。
     *
     * @param wb                  Workbook
     * @param reports             Delivery report
     * @param sheet               Sheet
     * @param indicatorPermission
     */
    public static void buildWorkBookForPlan(HSSFWorkbook wb, List<DeliveryReportView> reports, Sheet sheet, String indicatorPermission) {
        addHead(wb, sheet, Columns.getAuthorizedColumns(Columns.PLAN_REPORT, indicatorPermission));
        if (reports != null) {
            int rowIndex = 1;
            for (DeliveryReportView report : reports) {
                Row row = sheet.createRow(rowIndex++);
                int index = 0;
                row.createCell(index++).setCellValue(getLinkLabel(report.getDateLink()));
                row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getMediumName()));
                row.createCell(index++).setCellValue(getLinkLabel(report.getAccountLink()));
                row.createCell(index++).setCellValue(getLinkLabel(report.getPlanLink()));
                addIndices(report, row, index, indicatorPermission);
            }
        }
    }

    /**
     * 获取链接标签
     *
     * @param link
     * @return
     */
    private static String getLinkLabel(Link link) {
        if (link == null) {
            return Labels.UNKNOWN;
        } else {
            return link.getLabel();
        }
    }

    /**
     * 生成投放日报(媒体维度)Excel.
     *
     * @param wb
     * @param reports
     * @param sheet
     */
    public static void buildWorkBookForMedium(HSSFWorkbook wb, List<DeliveryReportView> reports, Sheet sheet, String indicatorPermission) {
        addHead(wb, sheet, Columns.getAuthorizedColumns(Columns.MEDIUM_REPORT, indicatorPermission));
        if (reports != null) {
            int rowIndex = 1;
            for (DeliveryReportView report : reports) {
                Row row = sheet.createRow(rowIndex++);
                int index = 0;
                row.createCell(index++).setCellValue(getLinkLabel(report.getDateLink()));
                row.createCell(index++).setCellValue(getLinkLabel(report.getMediumLink()));
                row.createCell(index++).setCellValue(getLinkLabel(report.getAccountLink()));
                addIndices(report, row, index, indicatorPermission);
            }
        }
    }

    /**
     * 生成投放日报（弹出表格）Excel
     *
     * @param wb
     * @param reports
     * @param sheet
     */
    public static void buildWorkBookForMediumDetail(HSSFWorkbook wb, List<DeliveryReportView> reports, Sheet sheet, String indicatorPermission) {
        addHead(wb, sheet, Columns.getAuthorizedColumns(Columns.MEDIUM_DETAIL_REPORT_DOWNLOAD, indicatorPermission));
        if (reports != null) {
            int rowIndex = 1;
            for (DeliveryReportView report : reports) {
                Row row = sheet.createRow(rowIndex++);
                int index = 0;
                row.createCell(index++).setCellValue(report.getMediumAccount());
                row.createCell(index++).setCellValue(report.getPlanLink().getLabel());
                addIndices(report, row, index, indicatorPermission);
            }
        }
    }

    /**
     * 依次添加统计指标列
     *
     * @param report
     * @param row
     * @param index               列起始坐标
     * @param indicatorPermission
     */
    private static void addIndices(DeliveryReportView report, Row row, int index, String indicatorPermission) {
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.COST)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getCost()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.SHOW_NUM)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getExposures()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CLICK_NUM)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getClicks()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.TOTAL_CLICK_NUM)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getTotalClicks()));
        }
//        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DAILY_UNIQUE_CLICK_NUM)) {
//            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getDailyUniqueClicks()));
//        }
//        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DAILY_UNIQUE_CLICK_NUM)) {
//            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getDailyUniqueClicks()));
//        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CTR)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getCtr()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CPC)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getCpc()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REACH)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getReaches()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REACH_RATE)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getReachRate()));
        }
        //        row.createCell(index++).setCellValue(report.getRetentionTime());
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DOWNLOAD_TIMES)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getDownloads()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DOWNLOAD_RATE)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getDownloadRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVE_NUM)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getActivations()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVE_RATE)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getActivationRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVE_CPA)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getActivationCpa()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTRATIONS_FD)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getFirstDayRegistrations()));
        }
        /*if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_ACCOUNT_NUM_FD)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getFirstDayRegisterAccountNum()));
        }*/
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_NUM)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getRegistrations()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_ACCOUNT_NUM)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getRegisterAccountNum()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVATION_REGISTRATION_RATE)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getActivationRegistrationRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_RATE)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getRegistrationRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_CPA)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getRegistrationCpa()));
        }
        /*if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_ACCOUNT_NUM_CPA)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getRegistrationAccountNumCpa()));
        }*/
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DAU)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getDau()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.NEW_ACCOUNT_PAY_NUM)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull((report.getNewlyIncreasedRecharges())));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.NEW_ACCOUNT_PAY_AMOUNT)) {
            if (report.getNewlyIncreasedRechargeAmount() == null) {
                row.createCell(index++).setCellValue(Labels.UNKNOWN);
            } else {
                row.createCell(index++).setCellValue(report.getNewlyIncreasedRechargeAmount().doubleValue());
            }
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.NEWLY_INCREASED_PAY_RATE)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getNewlyIncreasedPayRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.TOTAL_PAY_NUM)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getTotalRecharges()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.TOTAL_PAY_AMOUNT)) {
            if (report.getTotalRechargeAmount() == null) {
                row.createCell(index++).setCellValue(Labels.UNKNOWN);
            } else {
                row.createCell(index++).setCellValue(report.getTotalRechargeAmount().doubleValue());
            }
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CUMULATIVE_AMOUNT)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getAccumulatedRecharge()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.PAY_RATE)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getPayRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.PAYBACK_RATE_1DAY)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getFirstDayPaybackRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.LTV_1DAY)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getLtv1Day()));
        }

        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.LTV_7DAYS)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getLtv7Days()));
        }

        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.LTV_30DAYS)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getLtv30Days()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.RETAIN_D2)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getDay2RetentionRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.RETAIN_D3)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getDay3RetentionRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.RETAIN_D7)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getDay7RetentionRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.RETAIN_D15)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getDay15RetentionRate()));
        }
        if (StringUtils.containsIgnoreCase(indicatorPermission, Indicators.RETAIN_D30)) {
            row.createCell(index++).setCellValue(StringUtil.defaultIfNull(report.getDay30RetentionRate()));
        }
        
    }

    /**
     * 添加表头
     *
     * @param wb
     * @param sheet
     * @param columns
     */
    private static void addHead(HSSFWorkbook wb, Sheet sheet, Map<String, Object> columns) {
        //表头在Excel的第一行
        Row row = sheet.createRow(0);
        //从第一列开始
        int index = 0;
        for (Object column : columns.values()) {
            ColumnInfo columnInfo = (ColumnInfo) column;
            Cell cell = row.createCell(index++);
            cell.setCellValue(columnInfo.getTitle());
            cell.setCellStyle(headStyle(wb));
        }
    }

    /**
     * 表头样式。
     *
     * @param wb
     * @return
     */
    private static HSSFCellStyle headStyle(HSSFWorkbook wb) {
        HSSFCellStyle style = wb.createCellStyle();
        //将背景色设置为80%灰.
        style.setFillForegroundColor(HSSFColor.GREY_80_PERCENT.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        //将字体设置为白色.
        HSSFFont font = wb.createFont();
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
        return style;
    }

    
    /**
     * 生成投放日报子账户Tab的Excel
     *
     * @param wb
     * @param reports
     * @param sheet
     */
    public static void buildWorkBookForSystemAccount(HSSFWorkbook wb, List<DeliveryReportView> reports, Sheet sheet, String indicatorPermission) {
        addHead(wb, sheet, Columns.getAuthorizedColumns(Columns.SYSTEM_ACCOUNT_REPORT_DOWNLOAD, indicatorPermission));
        if (reports != null) {
            int rowIndex = 1;
            for (DeliveryReportView report : reports) {
                Row row = sheet.createRow(rowIndex++);
                int index = 0;
                row.createCell(index++).setCellValue(report.getSystemAccountName());
                addIndices(report, row, index, indicatorPermission);
            }
        }
    }
}
