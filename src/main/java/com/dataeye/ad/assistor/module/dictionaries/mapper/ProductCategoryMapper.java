package com.dataeye.ad.assistor.module.dictionaries.mapper;

import java.util.List;
import org.mybatis.spring.annotation.MapperScan;
import com.dataeye.ad.assistor.module.dictionaries.model.ProductCategory;

/**
 * create by ldj
 * 产品分类映射器
 * 
 **/
@MapperScan
public interface ProductCategoryMapper {
	
	/**查询产品分类*/
	public List<ProductCategory> query(Integer parentId);
		

	/**更id查询产品分类*/
	public ProductCategory get(Integer categoryId);
}
