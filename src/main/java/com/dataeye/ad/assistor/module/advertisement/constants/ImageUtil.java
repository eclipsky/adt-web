package com.dataeye.ad.assistor.module.advertisement.constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;

public class ImageUtil {

	public static final String UPLOAD_FILE_PATH = File.separator +"upload"+File.separator ;
	
	  /** 
     * 生成md5 
     * @param file 图片文件 
     * @return MD5值 
     * @throws FileNotFoundException 
     */  
    public static String getStringMd5(File file) throws FileNotFoundException {  
        String value = null;  
        FileInputStream in = new FileInputStream(file);  
        try {  
            MappedByteBuffer byteBuffer = in.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());  
            MessageDigest md5 = MessageDigest.getInstance("MD5");  
            md5.update(byteBuffer);  
            BigInteger bi = new BigInteger(1, md5.digest());  
            value = bi.toString(16);  
            if(value != null && value.length() != 32){
            	value="0"+value;
            }
        } catch (Exception e) {  
            e.printStackTrace();  
        } finally {  
            if(null != in) {  
                try {  
                    in.close();  
                } catch (IOException e) {  
                    e.printStackTrace();  
                }  
            }  
        }  
        return value;  
    } 
    
    // 是否为图片
    public static boolean isImage(String suffix) {
        boolean isImage = false;

        if (".jpg".equals(suffix) || ".gif".equals(suffix) || ".png".equals(suffix)
                || ".jpeg".equals(suffix) || ".bmp".equals(suffix)) {
            isImage = true;
        }

        return isImage;
    }

}
