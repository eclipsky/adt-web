package com.dataeye.ad.assistor.module.result.model;

import com.dataeye.ad.assistor.module.realtimedelivery.model.Chart;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by huangzehai on 2017/4/7.
 */
public class TrendChart<T> {
    /**
     * 日期列表（X轴）.
     */
    @Expose
    private List<String> dates;

    /**
     * 右边指标（纵轴）
     */
    @Expose
    private List<Chart<T>> indicators;

    public List<String> getDates() {
        return dates;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }

    public List<Chart<T>> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Chart<T>> indicators) {
        this.indicators = indicators;
    }

    @Override
    public String toString() {
        return "TrendChart{" +
                "dates=" + dates +
                ", indicators=" + indicators +
                '}';
    }
}
