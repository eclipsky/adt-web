package com.dataeye.ad.assistor.module.advertisement.model;

import com.google.gson.annotations.Expose;

/**
 * 文件
 * （图片或视频）
 * */
public class AdFiles {

	/**文件 id*/
	@Expose
	private String file_id;
	/**文件宽度，单位 px*/
	@Expose
	private Integer width;
	/**文件高度，单位 px*/
	@Expose
	private String height;
	/**文件大小 单位 B(byte)*/
	@Expose
	private String file_size;
	/**文件类型*/
	@Expose
	private String type;
	/**文件文件签名，使用文件文件的 md5 值，用于检查上传文件文件的完整性*/
	private String signature;
	/**预览地址*/
	@Expose
	private String preview_url;
	public String getFile_id() {
		return file_id;
	}
	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getFile_size() {
		return file_size;
	}
	public void setFile_size(String file_size) {
		this.file_size = file_size;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getPreview_url() {
		return preview_url;
	}
	public void setPreview_url(String preview_url) {
		this.preview_url = preview_url;
	}
	@Override
	public String toString() {
		return "AdFiles [file_id=" + file_id + ", width=" + width + ", height="
				+ height + ", file_size=" + file_size + ", type=" + type
				+ ", signature=" + signature + ", preview_url=" + preview_url
				+ "]";
	}

}
