package com.dataeye.ad.assistor.module.systemaccount.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.google.gson.annotations.Expose;

import java.util.Arrays;

/**
 * 系统账号下拉框列表参数Vo
 * 
 * @author luzhuyou 2017-02-23
 */
public class SystemAccountSelectorVo extends DataPermissionDomain {
	/** 账号ID */
	@Expose
	private Integer accountId;
	/** 账号名称（邮箱，登录账号） */
	@Expose
	private String accountName;
	/** 账号别名 */
	@Expose
	private String accountAlias;
	/** 分组ID */
	@Expose
	private Integer groupId;
	/** 账号名称及别名 */
	@Expose
	private String accountNameAndAlias;
	/** 公司ID */
	private Integer companyId;
	/** 请求分组ID集合参数 */
	private Integer[] requestGroupIds;

	/**
	 * 是否组长候选人
	 *
	 */
	private boolean isGroupLeaderCandidate;

	/**
	 *  是否是负责人或关注者候选人
	 */
	private boolean isManagerOrFollowerCandidate;

	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountAlias() {
		return accountAlias;
	}
	public void setAccountAlias(String accountAlias) {
		this.accountAlias = accountAlias;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getAccountNameAndAlias() {
		return accountNameAndAlias;
	}
	public void setAccountNameAndAlias(String accountNameAndAlias) {
		this.accountNameAndAlias = accountNameAndAlias;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer[] getRequestGroupIds() {
		return requestGroupIds;
	}
	public void setRequestGroupIds(Integer[] requestGroupIds) {
		this.requestGroupIds = requestGroupIds;
	}

	public boolean isGroupLeaderCandidate() {
		return isGroupLeaderCandidate;
	}

	public void setGroupLeaderCandidate(boolean groupLeaderCandidate) {
		isGroupLeaderCandidate = groupLeaderCandidate;
	}

	public boolean isManagerOrFollowerCandidate() {
		return isManagerOrFollowerCandidate;
	}

	public void setManagerOrFollowerCandidate(boolean managerOrFollowerCandidate) {
		isManagerOrFollowerCandidate = managerOrFollowerCandidate;
	}

	@Override
	public String toString() {
		return "SystemAccountSelectorVo{" +
				"accountId=" + accountId +
				", accountName='" + accountName + '\'' +
				", accountAlias='" + accountAlias + '\'' +
				", groupId=" + groupId +
				", accountNameAndAlias='" + accountNameAndAlias + '\'' +
				", companyId=" + companyId +
				", requestGroupIds=" + Arrays.toString(requestGroupIds) +
				", isGroupLeaderCandidate=" + isGroupLeaderCandidate +
				", isManagerOrFollowerCandidate=" + isManagerOrFollowerCandidate +
				'}';
	}
}
