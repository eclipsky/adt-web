package com.dataeye.ad.assistor.module.advertisement.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

/**
 * @author lingliqi
 * @date 2017-12-23 10:47
 */
public class AdProductVO {

    @Expose
    private String productRefsId;
    /** TrackingAppID */
	private String appId;

    @Expose
    private Integer mediumAccountId;

    @Expose
    private String productName;

    @Expose
    private String productType;

    private String productInfo;

    private String subordinateProductList;

    @Expose
    private String productIconUrl;

    private Date createTime;

    private Date updateTime;

    public String getProductRefsId() {
        return productRefsId;
    }

    public void setProductRefsId(String productRefsId) {
        this.productRefsId = productRefsId;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getSubordinateProductList() {
        return subordinateProductList;
    }

    public void setSubordinateProductList(String subordinateProductList) {
        this.subordinateProductList = subordinateProductList;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getProductIconUrl() {
        return productIconUrl;
    }

    public void setProductIconUrl(String productIconUrl) {
        this.productIconUrl = productIconUrl;
    }

    public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@Override
	public String toString() {
		return "AdProductVO [productRefsId=" + productRefsId + ", appId="
				+ appId + ", mediumAccountId=" + mediumAccountId
				+ ", productName=" + productName + ", productType="
				+ productType + ", productInfo=" + productInfo
				+ ", subordinateProductList=" + subordinateProductList
				+ ", productIconUrl=" + productIconUrl + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}
}
