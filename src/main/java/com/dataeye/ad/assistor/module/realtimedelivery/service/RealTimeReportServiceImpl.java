package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.mapper.RealTimeTrendMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.model.*;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.dataeye.ad.assistor.util.IntervalIndicatorUtils;
import com.dataeye.ad.assistor.util.TimeIndicatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by huangzehai on 2017/2/27.
 */
@Service
public class RealTimeReportServiceImpl implements RealTimeReportService {
    @Autowired
    private RealTimeTrendMapper realTimeTrendMapper;

    /**
     * 获取所有用户最近一个小时、最近半小时，最近20分钟，最近10分钟的实时时段指标
     *
     * @return
     */
    @Override
    public Map<Period, Map<Integer, RealTimeReport>> getLastPeriodIndicators() {
        //获取所有公司的实时统计报告
        return getLastPeriodIndicators(null);
    }


    /**
     * 获取登录用户最近一个小时、最近半小时，最近20分钟，最近10分钟的实时时段指标
     *
     * @param dataPermissionDomain
     * @return
     */
    @Override
    public Map<Period, Map<Integer, RealTimeReport>> getLastPeriodIndicators(DataPermissionDomain dataPermissionDomain) {
        Map<Period, Map<Integer, RealTimeReport>> reportsByPeriod = new HashMap<>();
        //分别获取投放计划、落地页、CP最近两小时的实时统计数据,统计时间精确到十分钟.
        RealTimeReportQuery query = new RealTimeReportQuery();
        if (dataPermissionDomain != null) {
            query.setCompanyId(dataPermissionDomain.getCompanyId());
            query.setPermissionMediumAccountIds(dataPermissionDomain.getPermissionMediumAccountIds());
        }
        query.setStartTime(getStartTime());
        List<RealTimeReport> planStats = realTimeTrendMapper.realTimePlanStatInTenMinutes(query);
        List<PageStat> pageStats = realTimeTrendMapper.realTimePageStatInTenMinutes(query);
        List<PackageStat> packageStats = realTimeTrendMapper.realTimePackageStatInTenMinutes(query);

        //分别统计各个时段
        for (Period period : Period.values()) {
            //计算最近时段动态边界——起始时间和截止时间
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, -period.getIntervalInMinutes());
            Date endTime = calendar.getTime();
            calendar.add(Calendar.MINUTE, -period.getIntervalInMinutes());
            Date startTime = calendar.getTime();

            //联结投放计划、落地页、CP数据.
//            Map<Integer, List<RealTimeReport>> reportsGroupByPlanId = join(groupByPlanId(planStats), groupByPageId(pageStats), groupByPackageId(packageStats), period);
            Map<Integer, List<RealTimeReport>> reportsGroupByPlanId = IntervalIndicatorUtils.join(planStats, pageStats, null, packageStats, period);

            Map<Integer, RealTimeReport> reportsByGroupId = new HashMap<>();
            //分别计算最近两个时间点的差值
            for (Map.Entry<Integer, List<RealTimeReport>> entry : reportsGroupByPlanId.entrySet()) {
                //计算差值
                if (entry.getValue() != null && entry.getValue().size() >= 2) {
                    //时段的终点
                    RealTimeReport endRealTimeReport = entry.getValue().get(0);
                    //时段的始点
                    RealTimeReport startRealTimeReport = entry.getValue().get(1);
                    if (!endRealTimeReport.getTime().before(endTime) && !startRealTimeReport.getTime().before(startTime)) {
                        //计算时段起始和截止的差值作为该时段的值
                        RealTimeReport report = diff(endRealTimeReport, startRealTimeReport);
                        //计算比率
                        computeRate(report);
                        reportsByGroupId.put(entry.getKey(), report);
                    }
                }
            }
            reportsByPeriod.put(period, reportsByGroupId);
        }
        return reportsByPeriod;
    }

    /**
     * 获取指定时间的指标
     *
     * @param query
     * @return
     */
    @Override
    public Map<Integer, RealTimeReport> getIndicatorAtTime(RealTimeReportQuery query) {
        List<RealTimeReport> planIndicators = realTimeTrendMapper.getPlanIndicatorAtTime(query);
        Set<Integer> pageIds = new HashSet<>();
        Set<Integer> packageIds = new HashSet<>();
        for (RealTimeReport planIndicator : planIndicators) {
            if (planIndicator.getPlanId() != null) {
                pageIds.add(planIndicator.getPageId());
            }

            if (planIndicator.getPackageId() != null) {
                packageIds.add(planIndicator.getPackageId());
            }
        }

        query.setPageIds(new ArrayList<>(pageIds));
        query.setPackageIds(new ArrayList<>(packageIds));
        List<PageStat> pageIndicators = realTimeTrendMapper.getPageIndicatorAtTime(query);
        List<PackageStat> pkgIndicators = realTimeTrendMapper.getPackageIndicatorAtTime(query);
        List<RealTimeReport> indicators = TimeIndicatorUtils.join(planIndicators, pageIndicators,pkgIndicators);
        return groupByPlanId(indicators);
    }

    /**
     * 根据计划ID分组
     *
     * @param indicators
     * @return
     */
    private Map<Integer, RealTimeReport> groupByPlanId(List<RealTimeReport> indicators) {
        if (indicators == null) {
            return null;
        }
        //根据落地页Id分组.
        Map<Integer, RealTimeReport> indicatorsByPlanId = new HashMap<>();
        for (RealTimeReport indicator : indicators) {
            //计算比率
            computeRate(indicator);
            indicatorsByPlanId.put(indicator.getPlanId(), indicator);
        }
        return indicatorsByPlanId;
    }


    /**
     * 获取时段统计开始时间.
     *
     * @return
     */
    private Date getStartTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -2);
        return calendar.getTime();
    }

    /**
     * 由时段起始点的累加值计算差值，得到该时段的数值
     *
     * @param currentReport
     * @param previousReport
     * @return
     */
    private RealTimeReport diff(RealTimeReport currentReport, RealTimeReport previousReport) {
        RealTimeReport diffReport = new RealTimeReport();
        diffReport.setPlanId(currentReport.getPlanId());
        diffReport.setPlanName(currentReport.getPlanName());
        diffReport.setMediumName(currentReport.getMediumName());
        diffReport.setResponsibleAccounts(currentReport.getResponsibleAccounts());
        diffReport.setTime(currentReport.getTime());
        diffReport.setTimeRange(timeRange(previousReport.getTime(), currentReport.getTime()));
        diffReport.setCost(subtract(currentReport.getCost(), previousReport.getCost()));
        diffReport.setExposures(subtract(currentReport.getExposures(), previousReport.getExposures()));
        diffReport.setClicks(subtract(currentReport.getClicks(), previousReport.getClicks()));
        diffReport.setReaches(subtract(currentReport.getReaches(), previousReport.getReaches()));
        diffReport.setDownloads(subtract(currentReport.getDownloads(), previousReport.getDownloads()));
        diffReport.setFirstDayRegistrations(subtract(currentReport.getFirstDayRegistrations(), previousReport.getFirstDayRegistrations()));
        return diffReport;
    }

    /**
     * 计算差值
     *
     * @param num1
     * @param num2
     * @return
     */
    private BigDecimal subtract(BigDecimal num1, BigDecimal num2) {
        if (num1 == null || num2 == null) {
            return null;
        } else {
            return num1.subtract(num2);
        }
    }


    /**
     * 计算差值
     *
     * @param num1
     * @param num2
     * @return
     */
    private Long subtract(Long num1, Long num2) {
        if (num1 == null || num2 == null) {
            return null;
        } else {
            return num1 - num2;
        }
    }


    /**
     * 计算差值
     *
     * @param num1
     * @param num2
     * @return
     */
    private Integer subtract(Integer num1, Integer num2) {
        if (num1 == null || num2 == null) {
            return null;
        } else {
            return num1 - num2;
        }
    }

    /**
     * 格式化时段为可读形式
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private String timeRange(Date startDate, Date endDate) {
        if (startDate == null || endDate == null) {
            return null;
        }
        DateFormat startDf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        DateFormat endDf = new SimpleDateFormat("HH:mm");
        StringBuilder timeRangeBuilder = new StringBuilder();
        timeRangeBuilder.append(startDf.format(startDate));
        timeRangeBuilder.append(" - ");
        timeRangeBuilder.append(endDf.format(endDate));
        return timeRangeBuilder.toString();
    }

    /**
     * 计算比率指标
     *
     * @param indicator
     */
    private void computeRate(RealTimeReport indicator) {
        //计算CTR
        if (indicator.getClicks() != null && indicator.getExposures() != null && indicator.getExposures() != 0) {
            indicator.setCtr((double) indicator.getClicks() / indicator.getExposures());
        }

        //计算下载率
        if (indicator.getDownloads() != null && indicator.getReaches() != null && indicator.getReaches() != 0) {
            indicator.setDownloadRate((double) indicator.getDownloads() / indicator.getReaches());
        }

        //计算下载单价
        if (indicator.getCost() != null && indicator.getDownloads() != null && indicator.getDownloads() != 0) {
            indicator.setCostPerDownload(indicator.getCost().divide(new BigDecimal(indicator.getDownloads()), 2, RoundingMode.HALF_UP));
        }

        //计算激活率
        if (indicator.getActivation() != null && indicator.getClicks() != null && indicator.getClicks() != 0) {
            indicator.setActivationRate((double) indicator.getActivation() / indicator.getClicks());
        }

        //计算激活CPA
        if (indicator.getCost() != null && indicator.getActivation() != null && indicator.getActivation() != 0) {
            indicator.setActivationCpa(indicator.getCost().divide(new BigDecimal(indicator.getActivation()), 2, RoundingMode.HALF_UP));
        }

        //计算注册率
        if (indicator.getFirstDayRegistrations() != null && indicator.getClicks() != null && indicator.getClicks() != 0) {
            indicator.setRegistrationRateFd((double) indicator.getFirstDayRegistrations() / indicator.getClicks());
        }


        //计算注册设备CPA
        if (indicator.getCost() != null && indicator.getFirstDayRegistrations() != null && indicator.getFirstDayRegistrations() != 0) {
            indicator.setCostPerRegistrationFd(indicator.getCost().divide(new BigDecimal(indicator.getFirstDayRegistrations()), 2, RoundingMode.HALF_UP));
        }

    }
}
