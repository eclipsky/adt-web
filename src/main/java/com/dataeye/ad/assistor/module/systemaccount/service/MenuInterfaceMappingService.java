package com.dataeye.ad.assistor.module.systemaccount.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.systemaccount.mapper.MenuInterfaceMappingMapper;
import com.dataeye.ad.assistor.module.systemaccount.model.MenuInterfaceMapping;

/**
 * 菜单接口映射Service
 * @author luzhuyou 2017/02/21
 *
 */
@Service
public class MenuInterfaceMappingService {

	@Autowired
	private MenuInterfaceMappingMapper menuInterfaceMappingMapper;
	
	/**
	 * 查询菜单接口映射信息列表
	 * @return
	 */
	public List<MenuInterfaceMapping> query() {
		return menuInterfaceMappingMapper.query();
	}
}
