package com.dataeye.ad.assistor.module.naturalflow.util;

import com.dataeye.ad.assistor.constant.Labels;
import org.apache.commons.lang.StringUtils;

public final class NaturalFlowUtils {
    private NaturalFlowUtils() {

    }

    public static final String formatProductName(String productName) {
        return StringUtils.defaultIfEmpty(productName, Labels.UNKNOWN) + Labels.NATURAL_FLOWS_SUFFIX;
    }
}
