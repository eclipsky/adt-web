package com.dataeye.ad.assistor.module.advertisement.model;

import java.util.Date;

/**
 * 广告创意关联关系
 * @author ldj
 *
 */
public class AdCreativeRelationVo {

	/**广告组ID**/
	private Integer adGroupId;
	/**广告ID**/
	private Integer adsId;
	/**广告创意ID**/
	private Integer adcreativeId;
	/**媒体ID**/
	private Integer mediumId;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;

	/**媒体帐号ID**/
	private Integer mediumAccountId;

	public Integer getAdGroupId() {
		return adGroupId;
	}

	public void setAdGroupId(Integer adGroupId) {
		this.adGroupId = adGroupId;
	}

	public Integer getAdsId() {
		return adsId;
	}

	public void setAdsId(Integer adsId) {
		this.adsId = adsId;
	}

	public Integer getAdcreativeId() {
		return adcreativeId;
	}

	public void setAdcreativeId(Integer adcreativeId) {
		this.adcreativeId = adcreativeId;
	}

	public Integer getMediumId() {
		return mediumId;
	}

	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getMediumAccountId() {
		return mediumAccountId;
	}

	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}

	@Override
	public String toString() {
		return "AdCreativeRelationVo [adGroupId=" + adGroupId + ", adsId="
				+ adsId + ", adcreativeId=" + adcreativeId + ", mediumId="
				+ mediumId + ", createTime=" + createTime + ", updateTime="
				+ updateTime + ", mediumAccountId=" + mediumAccountId + "]";
	}


	
	
	
	
}
