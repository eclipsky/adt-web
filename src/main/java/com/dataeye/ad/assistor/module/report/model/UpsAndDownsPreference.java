package com.dataeye.ad.assistor.module.report.model;

import com.google.gson.annotations.Expose;

/**
 * 指标涨跌偏好
 * Created by huangzehai on 2017/7/5.
 */
public class UpsAndDownsPreference {
    /**
     * 公司ID.
     */
    @Expose
    private Integer companyId;

    /**
     * ADT账号ID.
     */
    @Expose
    private Integer accountId;

    /**
     * 分钟数
     */
    @Expose
    private int minutes;

    /**
     * 指标可见性.
     */
    @Expose
    private String indicatorVisibility;

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public String getIndicatorVisibility() {
        return indicatorVisibility;
    }

    public void setIndicatorVisibility(String indicatorVisibility) {
        this.indicatorVisibility = indicatorVisibility;
    }

    @Override
    public String toString() {
        return "UpsAndDownsPreference{" +
                "companyId=" + companyId +
                ", accountId=" + accountId +
                ", minutes=" + minutes +
                ", indicatorVisibility='" + indicatorVisibility + '\'' +
                '}';
    }
}
