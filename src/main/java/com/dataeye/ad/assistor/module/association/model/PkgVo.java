package com.dataeye.ad.assistor.module.association.model;

/**
 * @author lingliqi
 * @date 2018-02-09 16:08
 */
public class PkgVo {

    private Integer pkgId;

    private Integer productId;

    public Integer getPkgId() {
        return pkgId;
    }

    public void setPkgId(Integer pkgId) {
        this.pkgId = pkgId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "PkgVo{" +
                "pkgId=" + pkgId +
                ", productId=" + productId +
                '}';
    }
}
