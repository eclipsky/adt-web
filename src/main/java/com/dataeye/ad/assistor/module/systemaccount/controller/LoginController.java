package com.dataeye.ad.assistor.module.systemaccount.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.Constant.CookieName;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.company.model.Company;
import com.dataeye.ad.assistor.module.company.model.TrackingCompany;
import com.dataeye.ad.assistor.module.company.service.CompanyService;
import com.dataeye.ad.assistor.module.systemaccount.model.PtLoginUser;
import com.dataeye.ad.assistor.module.systemaccount.service.AccountLoginInfoService;
import com.dataeye.ad.assistor.module.systemaccount.service.PtLoginService;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.privilege.PrivilegeControl.Scope;

/**
 * Created by luzhuyou
 * 系统账号控制器.
 */
@Controller
public class LoginController {
	
    private static final Logger logger = (Logger) LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private SystemAccountService systemAccountService;
    @Autowired
    private AccountLoginInfoService accountLoginInfoService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private PtLoginService ptLoginService;
    
//    /**
//	 * 
//	 * <pre>
//	 * 用户登录逻辑处理
//	 *  @param request
//	 *  @param response
//	 *  @throws Exception  
//	 *  @author Ivan<br>
//	 *  @date 2015年3月4日 下午3:17:59
//	 * <br>
//	 */
//	@PrivilegeControl(scope = Scope.Public)
//	@RequestMapping(value="/user/login.do",method=RequestMethod.POST)
//	public Object userLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
//		SessionContainer.clear(request);
//		DEContext context = DEContextContainer.getContext(request);
//		DEParameter parameter = context.getDeParameter();
//		String accountName = parameter.getParameter("userID");
//		String password = parameter.getParameter("password");
//		// 判断必填参数是否为空
//		boolean isEmpty = ValidateUtils.isEmpty(accountName, password);
//		if (isEmpty) {
//			ExceptionHandler.throwParameterException(StatusCode.COMM_PARAM_NOT_ENOUGH);
//		}
//		String ip = ServerUtils.getIP(request);
//        int status = 200;
//        
//		SystemAccountVo user = systemAccountService.get(accountName);
//		if (user == null) {
//			logger.info("getUserByUserID is null");
//			status = StatusCode.LOGI_USERNAME_OR_PASSWORD_ERROR;
//			ExceptionHandler.throwParameterException(StatusCode.LOGI_USERNAME_OR_PASSWORD_ERROR);
//		}
//		try {
//			// 正常登录逻辑，验证用户名和密码是否正确
//			if (!password.equals(user.getPassword())) {// 密码不正确
//				logger.info("password not correct");
//				status = StatusCode.LOGI_USERNAME_OR_PASSWORD_ERROR;
//				ExceptionHandler.throwParameterException(StatusCode.LOGI_USERNAME_OR_PASSWORD_ERROR);
//			}
//			
//			// 获取并判断公司状态，只有正常状态才可登录 add by luzhuyou 2017.05.18
//			Company company = companyService.get(user.getCompanyId());
//			if(company == null || company.getStatus() != 0) {
//				logger.info("Company Status is abnormal");
//				status = StatusCode.LOGI_COMPANY_STATUS_ERROR;
//				ExceptionHandler.throwPermissionException(StatusCode.LOGI_COMPANY_STATUS_ERROR);
//			}
//			
//			// 用户名和密码都正确了，验证用户状态 
//			int userStatus = user.getStatus();
//			switch (userStatus) {
//			case UserStatus.NORMAL: // 可创建应用
//				// 正常处理逻辑都集中在下面处理
//				break;
//			default:
//				logger.info("userStatus is abnormal");
//				status = StatusCode.LOGI_STATUS_ERROR;
//				ExceptionHandler.throwPermissionException(StatusCode.LOGI_STATUS_ERROR);
//				break;
//			}
//			// 集中处理用户session的创建和token的设置
//			logger.info("userStatus check ok ");
//			boolean succes = SessionContainer.buildUserSession(context, user);
//			if (!succes) {
//				logger.info("buildUserSession fail");
//				status = StatusCode.LOGI_USERNAME_OR_PASSWORD_ERROR;
//				ExceptionHandler.throwParameterException(StatusCode.LOGI_USERNAME_OR_PASSWORD_ERROR);
//			}
//		} finally {
//			// 记录登录记录到数据库
//			if (user != null) {
//				accountLoginInfoService.add(user.getCompanyId(), user.getAccountId(), ip, status, null);
//			}
//		}
//		return status;
//	}

	/**
	 * 
	 * <pre>
	 * 注销
	 *  @param request
	 *  @param response
	 *  @throws Exception  
	 *  @author Ivan<br>
	 *  @date 2015年3月17日 上午10:13:04
	 * <br>
	 */
	@PrivilegeControl(scope = Scope.AfterLogin)
	@RequestMapping("/user/logout.do")
	public Object loginOut(HttpServletRequest request, HttpServletResponse response) throws Exception {
		DEContext context = DEContextContainer.getContext(request);
		DEParameter parameter = context.getDeParameter();
		String accountName = parameter.getParameter("userID");
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        String currentLoginAccount = userInSession.getAccountName();
        if(StringUtils.isBlank(accountName) || !accountName.equals(currentLoginAccount)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        
		// 使session失效，跳转到登陆页
        SessionContainer.tokenSessionMap.remove(parameter.getCookie(CookieName.TOKEN_NAME));
        Cookie newCookie=new Cookie(CookieName.TOKEN_NAME, null); //假如要删除名称为username的Cookie
        newCookie.setDomain(Constant.ServerCfg.OOS_COOKIE_DOMAIN);
        newCookie.setMaxAge(0); //立即删除型
        newCookie.setPath("/"); //项目所有目录均有效，这句很关键，否则不敢保证删除
        response.addCookie(newCookie); //重新写入，将覆盖之前的
        
        SessionContainer.invalidate(request);
        
		return null;
	}
	
//	/**
//	 * 由统一账号登录
//	 *  @param request
//	 *  @param response
//	 *  @throws Exception  
//	 *  @author luzhuyou 2017/05/19
//	 */
//	@PrivilegeControl(scope = Scope.Public)
//	@RequestMapping("/uni/ptlogin.do")
//	public Object ptLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
//		DEContext context = DEContextContainer.getContext(request);
//		DEParameter parameter = context.getDeParameter();
//		
//		logger.info("统一登录处理.");
//		String token = parameter.getCookie(Constant.CookieName.TOKEN_NAME);
//		if (StringUtils.isBlank(token)) {
//			logger.info("统一登录异常:token empty");
//			ExceptionHandler.throwPermissionException(StatusCode.LOGI_RE_LOGIN);
//		}
//		
//		// 1. 根据token检查并获取统一登录用户信息
//		PtLoginUser ptLoginUser = ptLoginService.checkAndGetPtLoginAccount(token);
//		
//		// 获取并判断公司状态，只有正常状态才可登录 add by luzhuyou 2017.05.18
//		String email = ptLoginUser.getUserID();
//		SystemAccountVo user = systemAccountService.get(email);
//		if (user == null) {// 如果用户在ADT中不存在，检查公司信息是否存在
//			if(ptLoginUser.getCreateID() == 0) {
//				Company company = companyService.getByUid(ptLoginUser.getUID());
//				if(company == null) {
//					logger.info("您的帐号[{}]还未初始化，请确认是否需要进行初始化.", email);
//					ExceptionHandler.throwParameterException(StatusCode.LOGI_USERID_NOT_EXIST);
//				} else {
//					int companyStatus = company.getStatus(); 
//					if(companyStatus == 1 || companyStatus == 2 || companyStatus == 3) {
//						logger.info("[{}]用户审核中.", email);
//						ExceptionHandler.throwPermissionException(StatusCode.LOGI_STATUS_UNDER_CHECK);
//					} else if(companyStatus != 0) {
//						logger.info("[{}]公司状态异常.", ptLoginUser.getCompanyName());
//						ExceptionHandler.throwPermissionException(StatusCode.LOGI_COMPANY_STATUS_ERROR);
//					}
//				}
//			} else {
//				logger.info("子账号无权限开通ADT服务.");
//				ExceptionHandler.throwParameterException(StatusCode.LOGI_NO_PRIVILEGE);
//			}
//		}
//		
//		int status = 200;
//		try {
//			// 验证用户状态
//			int userStatus = user.getStatus();
//			switch (userStatus) {
//			case UserStatus.NORMAL: // 可创建应用
//				// 正常处理逻辑都集中在下面处理
//				break;
//			default:
//				logger.info("用户状态异常.UserStatus is : " + userStatus);
//				status = StatusCode.LOGI_STATUS_ERROR;
//				ExceptionHandler.throwPermissionException(StatusCode.LOGI_STATUS_ERROR);
//				break;
//			}
//			// 集中处理用户session的创建和token的设置
//			logger.info("用户检验成功，构建用户Session.");
//			boolean succes = SessionContainer.buildUserSession(context, user);
//			if (!succes) {
//				logger.info("用户Session构建失败.");
//				status = StatusCode.LOGI_STATUS_ERROR;
//				ExceptionHandler.throwParameterException(StatusCode.LOGI_STATUS_ERROR);
//			}
//		} finally {
//			// 记录登录记录到数据库
//			if (user != null) {
//				accountLoginInfoService.add(user.getAccountId(), ServerUtils.getIP(request), status, null);
//			}
//		}
//		return null;
//	}
	
	
	/**
	 * 由统一账号登录,注册公司信息
	 *  @param request
	 *  @param response
	 *  @throws Exception  
	 *  @author luzhuyou 2017/05/19
	 */
	@PrivilegeControl(scope = Scope.Public)
	@RequestMapping("/uni/reg.do")
	public Object registerCompany(HttpServletRequest request, HttpServletResponse response) throws Exception {
		DEContext context = DEContextContainer.getContext(request);
		DEParameter parameter = context.getDeParameter();
		
		logger.info("注册公司信息.");
		String token = parameter.getCookie(Constant.CookieName.TOKEN_NAME);
		if (StringUtils.isBlank(token)) {
			logger.info("统一登录异常:token empty");
			ExceptionHandler.throwPermissionException(StatusCode.LOGI_RE_LOGIN);
		}
		
		// 1. 根据token检查并获取统一登录用户信息
		PtLoginUser ptLoginUser = ptLoginService.checkAndGetPtLoginAccount(token);
		
		Company company = ptLoginService.registerCompany(ptLoginUser);
		return company;
	}
	
	/**
	 * 由统一账号登录,注册Tracking产品账号信息
	 *  @param request
	 *  @param response
	 *  @throws Exception  
	 *  @author luzhuyou 2017/05/19
	 */
	@PrivilegeControl(scope = Scope.Public)
	@RequestMapping("/uni/reg-tracking-user.do")
	public Object registerTrackingAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
		DEContext context = DEContextContainer.getContext(request);
		DEParameter parameter = context.getDeParameter();
		
		logger.info("注册Tracking产品账号信息.");
		String token = parameter.getCookie(Constant.CookieName.TOKEN_NAME);
		if (StringUtils.isBlank(token)) {
			logger.info("统一登录异常:token empty");
			ExceptionHandler.throwPermissionException(StatusCode.LOGI_RE_LOGIN);
		}
		
		// 1. 根据token检查并获取统一登录用户信息
		PtLoginUser ptLoginUser = ptLoginService.checkAndGetPtLoginAccount(token);
		
		TrackingCompany company = ptLoginService.registerTrackingAccount(ptLoginUser);
		return company;
	}
	
}
