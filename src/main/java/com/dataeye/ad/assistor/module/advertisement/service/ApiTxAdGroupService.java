package com.dataeye.ad.assistor.module.advertisement.service;


import java.util.Date;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.advertisement.mapper.ApiTxAdGroupMapper;
import com.dataeye.ad.assistor.module.advertisement.model.AdGroupQueryVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdvertisementGroupSelectVo;
import com.dataeye.ad.assistor.module.advertisement.model.ApiTxAdGroupVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdcreativeTemplate;
import com.dataeye.ad.assistor.module.advertisement.model.MarketingApiAuth;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupVO;

@Service("apiTxAdGroupService")
public class ApiTxAdGroupService {

    @Autowired
    private ApiTxAdGroupMapper adGroupRelationMapper;

    /**
     * 查询广告关联关系
     *
     * @param planGroupId
     * @param adGroupId
     * @param mediumId
     * @return
     */
    public List<ApiTxAdGroupVo> query(AdGroupQueryVo vo) {
        return adGroupRelationMapper.query(vo);
    }

    /**
     * 下拉框查询广告关联关系
     *
     * @param planGroupId
     * @param adGroupId
     * @param mediumId
     * @return
     */
    public List<AdvertisementGroupSelectVo> queryForSelect(AdGroupQueryVo vo) {
        return adGroupRelationMapper.queryForSelect(vo);
    }

    /**
     * 新建广告关联关系
     *
     * @param planGroupId
     * @param adGroupId
     * @param mediumId
     * @return
     */
    public void add(Integer adGroupId, Integer companyId, Integer systemAccountId, String adGroupName, Integer planGroupId, Integer mediumId,
    		Integer productId, Integer mediumAccountId, Integer targetingId, Integer adcreativeTemplateId, String siteSet,Integer dailyBudget) {
        ApiTxAdGroupVo relation = new ApiTxAdGroupVo();
        relation.setPlanGroupId(planGroupId);
        relation.setCompanyId(companyId);
        relation.setSystemAccountId(systemAccountId);
        relation.setAdGroupName(adGroupName);
        relation.setDailyBudget(dailyBudget);
        relation.setStatus(1);
        relation.setProductId(productId);
        relation.setMediumAccountId(mediumAccountId);
        relation.setAdGroupId(adGroupId);
        relation.setMediumId(mediumId);
        relation.setTargetingId(targetingId);
        relation.setAdcreativeTemplateId(adcreativeTemplateId);
        relation.setSiteSet(siteSet);
        relation.setCreateTime(new Date());
        relation.setUpdateTime(new Date());
        adGroupRelationMapper.add(relation);
    }

    /**
     * 更新广告关联关系
     *
     * @param planGroupId
     * @param adGroupId
     * @param mediumId
     * @return
     */
    public void update(Integer adGroupId, Integer planGroupId, Integer mediumId, Integer mediumAccountId,
                       String adGroupName, Integer targetingId,Integer dailyBudget, Integer status) {
        ApiTxAdGroupVo relation = new ApiTxAdGroupVo();
        relation.setPlanGroupId(planGroupId);
        relation.setMediumAccountId(mediumAccountId);
        relation.setAdGroupName(adGroupName);
        if(status != null){
        	relation.setStatus(status);
        }
        if(dailyBudget != null){
        	relation.setDailyBudget(dailyBudget);
        }
        relation.setTargetingId(targetingId);
        relation.setAdGroupId(adGroupId);
        relation.setMediumId(mediumId);
        relation.setUpdateTime(new Date());
        adGroupRelationMapper.update(relation);
    }

    /**
     * 删除广告关联关系
     *
     * @param planGroupId
     * @param adGroupId
     * @param mediumId
     * @param mediumAccountId
     * @return
     */
    public int delete(Integer planGroupId, Integer adGroupId, Integer mediumId, Integer mediumAccountId) {
        ApiTxAdGroupVo relation = new ApiTxAdGroupVo();
        relation.setPlanGroupId(planGroupId);
        if (adGroupId != null) {
            relation.setAdGroupId(adGroupId);
        }
        relation.setMediumId(mediumId);
        relation.setMediumAccountId(mediumAccountId);
        return adGroupRelationMapper.delete(relation);
    }


    /**
     * 获得GDT授权账户信息
     */
    public MarketingApiAuth getAccessTokey(Integer mediumAccountId) {
        return adGroupRelationMapper.getAccessTokey(mediumAccountId);
    }

    /**
     * 查询广告创意版位
     * productType  (PRODUCT_TYPE_APP_IOS,PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM)
     */
    public List<AdcreativeTemplate> queryAdcreativeTemplate(String productType) {
        return adGroupRelationMapper.queryAdcreativeTemplate(productType);
    }

    /**
     * 查询计划下的产品信息
     *
     * @param planGroupId
     * @param companyId
     * @param mediumAccountId
     * @return
     */
    public PlanGroupVO queryProductInfosByPlanGroupId(Integer companyId, Integer planGroupId, Integer mediumAccountId) {
        AdGroupQueryVo vo = new AdGroupQueryVo();
        vo.setCompanyId(companyId);
        vo.setPlanGroupId(planGroupId);
        vo.setMediumAccountId(mediumAccountId);
        return adGroupRelationMapper.queryProductInfosByPlanGroupId(vo);
    }

    /**
     * 根据广告ID删除广告关联关系
     *
     * @param adGroupId
     * @param mediumAccountId
     * @return
     */
    public int deleteByAdGroupId(Integer adGroupId, Integer mediumAccountId) {
        ApiTxAdGroupVo relation = new ApiTxAdGroupVo();
        relation.setAdGroupId(adGroupId);
        relation.setMediumAccountId(mediumAccountId);
        return adGroupRelationMapper.deleteByAdGroupId(relation);
    }

    /**
     * 检查推广计划名称是否存在，同一账户下不允许重名
     *
     * @param mediumAccountId 媒体账户ID
     * @param adGroupName     广告组名称
     */
    public boolean checkAdGroupName(Integer mediumAccountId, String adGroupName, Integer adGroupId) {
        // 名称长度为120字节，在UTF编码下，最多为40字符
        final int maxLength = 40;
        Integer existRecord = adGroupRelationMapper.getRecordByAdGroupName(mediumAccountId, adGroupName, adGroupId);
        if (adGroupName.length() <= maxLength && existRecord == null) {
            return true;
        } else {
            return false;
        }

    }
    
    /**
     * 查询广告关联关系
     *
     * @param planGroupId
     * @param adGroupId
     * @param mediumId
     * @return
     */
    public ApiTxAdGroupVo getAdGroup(Integer adGroupId) {
        return adGroupRelationMapper.getAdGroup(adGroupId);
    }
}
