package com.dataeye.ad.assistor.module.systemaccount.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.systemaccount.model.CrmManagerAccount;

/**
 * CRM管理人员账号映射器.
 * Created by luzhuyou
 */
@MapperScan
public interface CrmManagerAccountMapper {

	/**
	 *  根据登录邮箱号获取账号信息
	 * @param email
	 * @return
	 */
	public CrmManagerAccount getByEmail(String email);
	
	/**
	 *  根据账号ID获取账号信息
	 * @param email
	 * @return
	 */
	public CrmManagerAccount getById(int accountId);

}
