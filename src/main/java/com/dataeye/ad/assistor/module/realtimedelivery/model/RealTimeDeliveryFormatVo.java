package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.dataeye.ad.assistor.constant.Constant.MediumType;
import com.dataeye.ad.assistor.constant.Constant.Separator;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.BidStrategy;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.annotations.Expose;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

import org.apache.commons.lang.StringUtils;


/**
 * 格式化好供展示的实时投放对象
 *
 * @author luzhuyou 2017/01/04
 */
public class RealTimeDeliveryFormatVo {

    private final int COLOR_RED = 1;
    private final int ALERT_CONF_STATUS_TRUE = 1;
    /**
     * 当前日期
     */
    @Expose
    private String currentDate;
    /**
     * 产品名称.
     */
    @Expose
    private String productName;
    /**
     * 媒体ID
     */
    @Expose
    private Integer mediumId;
    /**
     * 媒体名称
     */
    @Expose
    private String medium;
    /**
     * 媒体账号
     */
    @Expose
    private String account;
    /**
     * 媒体账号ID
     */
    @Expose
    private Integer mediumAccountId;
    /**
     * 计划Id
     */
    @Expose
    private Integer planID;
    /**
     * 计划名称
     */
    @Expose
    private String planName;
    /**
     * 计划来源
     */
    @Expose
    private Integer planSource;
    /**
     * 媒体计划Id
     */
    @Expose
    private String mediumPlanId;
    /**
     * 总消耗
     */
    @Expose
    private String totalCost;
    /**
     * 曝光数
     */
    @Expose
    private String exposures;
    /**
     * 点击数
     */
    @Expose
    private String clicks;
    /**
     * 未排重点击数
     */
    @Expose
    private String totalClicks;
    /**
     * CTR
     */
    @Expose
    private String ctr;
    /**
     * CPC
     */
    @Expose
    private String cpc;
    /**
     * 到达数.
     */
    @Expose
    private String reaches;
    /**
     * 到达率.
     */
    @Expose
    private String reachRate;
    /**
     * 下载数
     */
    @Expose
    private String downloadUV;
    /**
     * 下载率
     */
    @Expose
    private String downloadRate;
    /**
     * 停留时间.
     */
    @Expose
    private String retentionTime;
    /**
     * 激活数
     */
    @Expose
    private String activations;
    /**
     * 激活率.
     */
    @Expose
    private String activationRate;
    /**
     * 激活CPA.
     */
    @Expose
    private String activationCpa;
    /**
     * 首日注册设备数
     */
    @Expose
    private String firstDayRegistrations;
    /**
     * 首日注册账号数
     */
    @Expose
    private String firstDayRegisterAccountNum;
    /**
     * 点击注册率
     */
    @Expose
    private String firstDayRegistrationRate;
    /**
     * 首日注册CPA（设备）
     */
    @Expose
    private String costPerFirstDayRegistration;
    /**
     * 首日注册CPA（账号）
     */
    @Expose
    private String costPerFirstDayRegisterAccountNum;
    /**
     * 出价.
     */
    @Expose
    private String bid;
    /**
     * 出价策略.
     */
    @Expose
    private String bidStrategy;
    /**
     * OCPC出价阶段。(第一阶段：1；第二阶段：2)仅供今日头条OCPC出价的计划使用.
     */
    @Expose
    private String phase;
    /**
     * 媒体计划开关，1-打开，0-关闭
     */
    @Expose
    private Integer planSwitch;
    /**
     * 该计划是否支持当前用户出价.
     */
    @Expose
    private boolean isBidAvailable;
    /**
     * 该计划是否支持当前用户启用关闭投放
     */
    @Expose
    private boolean isPlanSwitchAvailable;
    /**
     * 媒体URL
     */
    @Expose
    private String mediumURL;
    /**
     * 计划名称颜色：0-正常，1-红色
     */
    @Expose
    private int planNameColor;
    /**
     * 消耗颜色：0-正常，1-红色
     */
    @Expose
    private int totalCostColor = 0;
    /**
     * CPA颜色：0-正常，1-红色
     */
    @Expose
    private int costPerRegistrationColor = 0;
    /**
     * 注册数颜色
     */
    @Expose
    private int registrationsColor = 0;
    /**
     * CTR颜色：0-正常，1-红色
     */
    @Expose
    private int ctrColor = 0;
    /**
     * 下载率颜色：0-正常，1-红色
     */
    @Expose
    private int downloadRateColor = 0;
    /**
     * 告警设置状态：0-未设置，1-已设置
     */
    @Expose
    private int alertConfStatus = 0;
    /**
     * 指标涨跌.
     */
    @Expose
    private IndicatorUpsAndDowns upsAndDowns;
    /**
     * 是否计划负责人
     */
    @Expose
    private boolean isManager;
    /**
     * 出价不可用消息编码
     */
    @Expose
    private Integer bidMessageCode;
    /**
     * 开关不可用消息编码
     */
    @Expose
    private Integer switchMessageCode;
    /**
     * 媒体类型
     */
    @Expose
    private Integer mediumType;
    @Expose
    private boolean isEditable;


    public RealTimeDeliveryFormatVo() {
    }

    /**
     * 将实时投放对象RealTimeDeliveryVo的结果转换为格式化的RealTimeDeliveryFormatVo对象实例
     *
     * @param vo
     */
    public RealTimeDeliveryFormatVo(RealTimeDeliveryVo vo) {
        if (vo != null) {
            NumberFormat nf = NumberFormat.getPercentInstance();
            //保留两位小数
            nf.setMinimumFractionDigits(2);
            //四舍五入
            nf.setRoundingMode(RoundingMode.HALF_UP);
            this.setCurrentDate(vo.getCurrentDate());
            this.setProductName(StringUtil.defaultIfNull(vo.getProductName()));
            this.setMediumId(vo.getMediumId());
            this.setMedium(vo.getMedium());
            this.setAccount(vo.getAccount());
            this.setMediumAccountId(vo.getMediumAccountId());
            this.setPlanId(vo.getPlanId());
            this.setPlanName(vo.getPlanName());
            this.setPlanSource(vo.getPlanSource());
            this.setMediumPlanId(vo.getMediumPlanId());
            this.setTotalCost(vo.getTotalCost() == null ? Separator.BAR : (new BigDecimal(vo.getTotalCost()).setScale(2, RoundingMode.HALF_UP)) + "");
            this.setExposures(vo.getExposures() == null ? Separator.BAR : String.valueOf(vo.getExposures()));
            this.setClicks(vo.getClicks() == null ? Separator.BAR : String.valueOf(vo.getClicks()));
            this.setTotalClicks(StringUtil.defaultIfNull(vo.getTotalClicks()));
            this.setCtr(vo.getCtr() == null ? Separator.BAR : (new BigDecimal(vo.getCtr() * 100).setScale(2, RoundingMode.HALF_UP) + "%"));
            this.setReaches(vo.getReaches() == null ? Separator.BAR : vo.getReaches().toString());
            this.setReachRate(vo.getReachRate() == null ? Separator.BAR : nf.format(vo.getReachRate()));
            this.setDownloadUV(vo.getDownloadUV() == null ? Separator.BAR : String.valueOf(vo.getDownloadUV()));
            this.setDownloadRate(vo.getDownloadRate() == null ? Separator.BAR : (new BigDecimal(vo.getDownloadRate() * 100).setScale(2, RoundingMode.HALF_UP) + "%"));
            this.setRetentionTime(vo.getRetentionTime() == null ? Separator.BAR : vo.getRetentionTime().toString());
            this.setActivations(vo.getActivations() == null ? Separator.BAR : String.valueOf(vo.getActivations()));
            this.setFirstDayRegistrations(vo.getRegisterNumFd() == null ? Separator.BAR : String.valueOf(vo.getRegisterNumFd()));
            this.setFirstDayRegisterAccountNum(vo.getRegisterAccountNumFd() == null ? Separator.BAR : String.valueOf(vo.getRegisterAccountNumFd()));
            this.setActivationRate(vo.getActivationRate() == null ? Separator.BAR : nf.format(vo.getActivationRate()));
            this.setFirstDayRegistrationRate(vo.getRegistrationRateFd() == null ? Separator.BAR : nf.format(vo.getRegistrationRateFd()));
            this.setActivationCpa(vo.getActivationCpa() == null ? Separator.BAR : vo.getActivationCpa().setScale(2, RoundingMode.HALF_UP).toString());
            this.setCostPerFirstDayRegistration(vo.getCostPerRegistrationFd() == null ? Separator.BAR : (new BigDecimal(vo.getCostPerRegistrationFd()).setScale(2, RoundingMode.HALF_UP)) + "");
            this.setCostPerFirstDayRegisterAccountNum(vo.getCostPerRegistrationAccountFd() == null ? Separator.BAR : (new BigDecimal(vo.getCostPerRegistrationAccountFd()).setScale(2, RoundingMode.HALF_UP)) + "");
            this.setCpc(vo.getCpc() == null ? Separator.BAR : vo.getCpc().setScale(2, RoundingMode.HALF_UP).toString());
            this.setBid(vo.getBid() == null ? Separator.BAR : CurrencyUtils.fenToYuan(vo.getBid()).toString());
            this.setBidStrategy(BidStrategy.getStandardBidStrategy(vo.getMediumId(), StringUtil.defaultIfNull(vo.getBidStrategy())));
            this.setPhase(BidStrategy.getBidPhase(vo.getMediumId(), StringUtil.defaultIfNull(vo.getPlanExtend())));
            this.setPlanSwitch(vo.getPlanSwitch());
            this.setMediumURL(vo.getMediumURL());
            this.setUpsAndDowns(vo.getUpsAndDowns());
            if (vo.getMediumId() != null && !StringUtils.isBlank(vo.getMediumId() + "")) {
                this.setMediumType(MediumType.getMediumType(vo.getMediumId()));
            } else {
                this.setMediumType(0);
            }
        }
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public Integer getPlanId() {
        return planID;
    }

    public void setPlanId(Integer planId) {
        this.planID = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Integer getPlanSource() {
        return planSource;
    }

    public void setPlanSource(Integer planSource) {
        this.planSource = planSource;
    }

    public String getMediumPlanId() {
        return mediumPlanId;
    }

    public void setMediumPlanId(String mediumPlanId) {
        this.mediumPlanId = mediumPlanId;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getExposures() {
        return exposures;
    }

    public void setExposures(String exposures) {
        this.exposures = exposures;
    }

    public String getClicks() {
        return clicks;
    }

    public void setClicks(String clicks) {
        this.clicks = clicks;
    }

    public String getTotalClicks() {
        return totalClicks;
    }

    public void setTotalClicks(String totalClicks) {
        this.totalClicks = totalClicks;
    }

    public String getCtr() {
        return ctr;
    }

    public void setCtr(String ctr) {
        this.ctr = ctr;
    }

    public String getCpc() {
        return cpc;
    }

    public void setCpc(String cpc) {
        this.cpc = cpc;
    }

    public String getReaches() {
        return reaches;
    }

    public void setReaches(String reaches) {
        this.reaches = reaches;
    }

    public String getReachRate() {
        return reachRate;
    }

    public void setReachRate(String reachRate) {
        this.reachRate = reachRate;
    }

    public String getDownloadUV() {
        return downloadUV;
    }

    public void setDownloadUV(String downloadUV) {
        this.downloadUV = downloadUV;
    }

    public String getDownloadRate() {
        return downloadRate;
    }

    public void setDownloadRate(String downloadRate) {
        this.downloadRate = downloadRate;
    }

    public String getRetentionTime() {
        return retentionTime;
    }

    public void setRetentionTime(String retentionTime) {
        this.retentionTime = retentionTime;
    }

    public String getActivations() {
        return activations;
    }

    public void setActivations(String activations) {
        this.activations = activations;
    }

    public String getActivationRate() {
        return activationRate;
    }

    public void setActivationRate(String activationRate) {
        this.activationRate = activationRate;
    }

    public String getActivationCpa() {
        return activationCpa;
    }

    public void setActivationCpa(String activationCpa) {
        this.activationCpa = activationCpa;
    }

    public String getFirstDayRegistrations() {
        return firstDayRegistrations;
    }

    public void setFirstDayRegistrations(String firstDayRegistrations) {
        this.firstDayRegistrations = firstDayRegistrations;
    }

    public String getFirstDayRegisterAccountNum() {
        return firstDayRegisterAccountNum;
    }

    public void setFirstDayRegisterAccountNum(String firstDayRegisterAccountNum) {
        this.firstDayRegisterAccountNum = firstDayRegisterAccountNum;
    }

    public String getFirstDayRegistrationRate() {
        return firstDayRegistrationRate;
    }

    public void setFirstDayRegistrationRate(String firstDayRegistrationRate) {
        this.firstDayRegistrationRate = firstDayRegistrationRate;
    }

    public String getCostPerFirstDayRegistration() {
        return costPerFirstDayRegistration;
    }

    public void setCostPerFirstDayRegistration(String costPerFirstDayRegistration) {
        this.costPerFirstDayRegistration = costPerFirstDayRegistration;
    }

    public String getCostPerFirstDayRegisterAccountNum() {
        return costPerFirstDayRegisterAccountNum;
    }

    public void setCostPerFirstDayRegisterAccountNum(String costPerFirstDayRegisterAccountNum) {
        this.costPerFirstDayRegisterAccountNum = costPerFirstDayRegisterAccountNum;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getBidStrategy() {
        return bidStrategy;
    }

    public void setBidStrategy(String bidStrategy) {
        this.bidStrategy = bidStrategy;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public Integer getPlanSwitch() {
        return planSwitch;
    }

    public void setPlanSwitch(Integer planSwitch) {
        this.planSwitch = planSwitch;
    }

    public boolean isBidAvailable() {
        return isBidAvailable;
    }

    public void setBidAvailable(boolean bidAvailable) {
        isBidAvailable = bidAvailable;
    }

    public boolean isPlanSwitchAvailable() {
        return isPlanSwitchAvailable;
    }

    public void setPlanSwitchAvailable(boolean planSwitchAvailable) {
        isPlanSwitchAvailable = planSwitchAvailable;
    }

    public String getMediumURL() {
        return mediumURL;
    }

    public void setMediumURL(String mediumURL) {
        this.mediumURL = mediumURL;
    }

    public int getPlanNameColor() {
        return planNameColor;
    }

    public void setPlanNameColor(int planNameColor) {
        this.planNameColor = planNameColor;
    }

    public int getTotalCostColor() {
        return totalCostColor;
    }

    public void setTotalCostColor(int totalCostColor) {
        this.totalCostColor = totalCostColor;
    }

    public int getCostPerRegistrationColor() {
        return costPerRegistrationColor;
    }

    public void setCostPerRegistrationColor(int costPerRegistrationColor) {
        this.costPerRegistrationColor = costPerRegistrationColor;
    }

    public int getRegistrationsColor() {
        return registrationsColor;
    }

    public void setRegistrationsColor(int registrationsColor) {
        this.registrationsColor = registrationsColor;
    }

    public int getCtrColor() {
        return ctrColor;
    }

    public void setCtrColor(int ctrColor) {
        this.ctrColor = ctrColor;
    }

    public int getDownloadRateColor() {
        return downloadRateColor;
    }

    public void setDownloadRateColor(int downloadRateColor) {
        this.downloadRateColor = downloadRateColor;
    }

    public int getAlertConfStatus() {
        return alertConfStatus;
    }

    public void setAlertConfStatus(int alertConfStatus) {
        this.alertConfStatus = alertConfStatus;
    }

    public IndicatorUpsAndDowns getUpsAndDowns() {
        return upsAndDowns;
    }

    public void setUpsAndDowns(IndicatorUpsAndDowns upsAndDowns) {
        this.upsAndDowns = upsAndDowns;
    }

    public boolean isManager() {
        return isManager;
    }

    public void setManager(boolean manager) {
        isManager = manager;
    }

    public Integer getBidMessageCode() {
        return bidMessageCode;
    }

    public void setBidMessageCode(Integer bidMessageCode) {
        this.bidMessageCode = bidMessageCode;
    }

    public Integer getSwitchMessageCode() {
        return switchMessageCode;
    }

    public void setSwitchMessageCode(Integer switchMessageCode) {
        this.switchMessageCode = switchMessageCode;
    }

    public Integer getMediumType() {
        return mediumType;
    }

    public void setMediumType(Integer mediumType) {
        this.mediumType = mediumType;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

}
