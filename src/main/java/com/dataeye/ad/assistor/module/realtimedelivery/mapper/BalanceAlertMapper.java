package com.dataeye.ad.assistor.module.realtimedelivery.mapper;

import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceAlertConf;

import java.util.List;

/**
 * Created by huangzehai on 2017/4/28.
 */
public interface BalanceAlertMapper {
    /**
     * 保存余额告警配置.
     *
     * @param balanceAlertConf
     */
    int saveBalanceAlertConf(BalanceAlertConf balanceAlertConf);

    /**
     * 获取指定系统账号的余额告警配置.
     *
     * @param accountId
     */
    BalanceAlertConf getBalanceAlertConfByAccountId(int accountId);

    /**
     * 获取需要余额告警的邮件列表
     *
     * @return
     */
    List<String> getWaringEmails();
    
    /**
     * 根据公司id，获取该公司内的余额告警配置.
     *
     * @param companyId
     */
    List<BalanceAlertConf> getBalanceAlertConfByCompanyId(int companyId);
    
    /**
     * 修改余额告警配置.
     *
     * @param balanceAlertConf
     */
    int updateBalanceAlertConf(BalanceAlertConf balanceAlertConf);
}
