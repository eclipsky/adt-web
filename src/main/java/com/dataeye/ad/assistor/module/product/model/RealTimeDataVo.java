package com.dataeye.ad.assistor.module.product.model;

import com.google.gson.annotations.Expose;


/**
 * 实时统计数据
 * 
 * */
public class RealTimeDataVo {
	
	@Expose
	private String channelId;  //渠道ID
	
	@Expose
	private String channelName; //渠道名称
	
	@Expose
	private String clickA;  //总点击次数
	
	@Expose
	private String clickU; //有效点击次数
	
	@Expose
	private String activeNum; //激活数
	@Expose
	private String activeNumRate; //激活率
	@Expose
	private String registerNum;  //注册数
	@Expose
	private String registerNumRate; //注册率
	@Expose
	private String loginNum; //今日累计活跃
	@Expose
	private String totalPayAmount;  //今日累计付费
	@Expose
	private String registerAccountNum;  //注册帐号数
	
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getClickA() {
		return clickA;
	}
	public void setClickA(String clickA) {
		this.clickA = clickA;
	}
	public String getClickU() {
		return clickU;
	}
	public void setClickU(String clickU) {
		this.clickU = clickU;
	}
	public String getActiveNum() {
		return activeNum;
	}
	public void setActiveNum(String activeNum) {
		this.activeNum = activeNum;
	}
	public String getActiveNumRate() {
		return activeNumRate;
	}
	public void setActiveNumRate(String activeNumRate) {
		this.activeNumRate = activeNumRate;
	}
	public String getRegisterNum() {
		return registerNum;
	}
	public void setRegisterNum(String registerNum) {
		this.registerNum = registerNum;
	}
	public String getRegisterNumRate() {
		return registerNumRate;
	}
	public void setRegisterNumRate(String registerNumRate) {
		this.registerNumRate = registerNumRate;
	}
	public String getLoginNum() {
		return loginNum;
	}
	public void setLoginNum(String loginNum) {
		this.loginNum = loginNum;
	}
	public String getTotalPayAmount() {
		return totalPayAmount;
	}
	public void setTotalPayAmount(String totalPayAmount) {
		this.totalPayAmount = totalPayAmount;
	}
	public String getRegisterAccountNum() {
		return registerAccountNum;
	}
	public void setRegisterAccountNum(String registerAccountNum) {
		this.registerAccountNum = registerAccountNum;
	}
	@Override
	public String toString() {
		return "RealTimeDataVo [channelId=" + channelId + ", channelName="
				+ channelName + ", clickA=" + clickA + ", clickU=" + clickU
				+ ", activeNum=" + activeNum + ", activeNumRate="
				+ activeNumRate + ", registerNum=" + registerNum
				+ ", registerNumRate=" + registerNumRate + ", loginNum="
				+ loginNum + ", totalPayAmount=" + totalPayAmount
				+ ", registerAccountNum=" + registerAccountNum + "]";
	}
	
}
