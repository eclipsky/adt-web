package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.constant.OpCode;
import com.dataeye.ad.assistor.module.realtimedelivery.mapper.OperationLogMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLog;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLogQuery;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLogView;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.dataeye.ad.assistor.util.DateUtils;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作日志服务
 * Created by huangzehai on 2017/6/28.
 */
@Service
public class OperationLogServiceImpl implements OperationLogService {
    private static final Map<Integer, String> RESULTS = new HashMap<>();

    static {
        RESULTS.put(1, "成功");
        RESULTS.put(0, "失败");
    }

    @Autowired
    private OperationLogMapper operationLogMapper;

    @Override
    public void addOperationLog(OperationLog log) {
        operationLogMapper.addOperationLog(log);
    }

    /**
     * 列出操作日志
     *
     * @param operationLogQuery
     * @return
     */
    @Override
    public Page<OperationLogView> listOperationLogs(OperationLogQuery operationLogQuery) {
        Page<OperationLog> logs = (Page<OperationLog>) operationLogMapper.listOperationLogs(operationLogQuery);
        //返回分页插件的List实现
        Page<OperationLogView> logViews = new Page<>();
        DateFormat df = new SimpleDateFormat(DateUtils.DATE_TIME_FORMAT);
        for (OperationLog log : logs) {
            OperationLogView view = new OperationLogView();
            view.setOpId(log.getOpId());
            view.setPlanId(log.getPlanId());
            view.setPlanName(log.getPlanName());
            view.setOpTime(df.format(log.getOpTime()));
            view.setAccountName(log.getAccountName());
            view.setOpName(OpCode.getOpName(log.getOpCode()));
            view.setCurrentValue(log.getCurrentValue());
            view.setPreviousValue(log.getPreviousValue());
            view.setResult(RESULTS.get(log.getResult()));
            view.setMessage(log.getMessage());
            logViews.add(view);
        }
        //复制分页信息
        logViews.setTotal(logs.getTotal());
        logViews.setPageSize(logs.getPageSize());
        logViews.setPages(logs.getPages());
        logViews.setPageNum(logs.getPageNum());
        return logViews;
    }

}
