package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.List;

/**
 * Created by huangzehai on 2017/5/4.
 */
public class MediumAccountQuery extends DataPermissionDomain {
    /**
     * 媒体ID列表.
     */
    private List<Integer> mediumIds;

    public List<Integer> getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(List<Integer> mediumIds) {
        this.mediumIds = mediumIds;
    }
}
