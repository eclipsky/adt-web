package com.dataeye.ad.assistor.module.payback.controller;

import java.text.ParseException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.payback.constants.Constants.PayBackIndicator;
import com.dataeye.ad.assistor.module.payback.model.PaybackQuery;
import com.dataeye.ad.assistor.module.payback.service.PaybackContrastiveService;
import com.dataeye.ad.assistor.module.payback.model.Period;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.StringUtil;

/***
 * 
 * 回本对比分析 
 * create by ldj 2017-07-27
 * */
@Controller
public class PaybackContrastiveController {
	@Autowired
	private PaybackContrastiveService payBackContrastiveService;
	
	
	/**
	 * 按【推广计划】分别获取回本的趋势图
	 * **/
	@PrivilegeControl(scope = PrivilegeControl.Scope.Public,write = false)
	@RequestMapping("/ad/payback-contrastive/plan.do")
	public Object contrastiveByPlan(HttpServletRequest request , HttpServletResponse response)  throws Exception {
		DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (permissionMediumAccountIds == null) {
            return null;
        }
        //参数解析与校验
        PaybackQuery query = parseQuery(parameter);
        // 如果为null，则表示开始时间大于等于当前时间，则没有回本率数据
        if (query == null) {
            return null;
        }
        query.setCompanyId(companyId);
        query.setPermissionMediumAccountIds(permissionMediumAccountIds);
        
        switch (query.getPeriod()) {
            case Day:
            	return payBackContrastiveService.payBackContrastiveDailyQueryByPlan(query);
            case Week:
            	return payBackContrastiveService.payBackContrastiveWeeklyQueryByPlan(query);
            case Month:
            	return payBackContrastiveService.payBackContrastiveMonthlyQueryByPlan(query);
		}
		return null;
	}
	
	/**
	 * 按【媒体帐号】分别获取回本的趋势图
	 * **/
	@PrivilegeControl(scope = PrivilegeControl.Scope.Public,write = false)
	@RequestMapping("/ad/payback-contrastive/medium-account.do")
	public Object contrastiveByMediumAccount(HttpServletRequest request , HttpServletResponse response)  throws Exception {
		DEContext context = (DEContext) request.getAttribute("CTX");
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (permissionMediumAccountIds == null) {
            return null;
        }
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        PaybackQuery query = parseQuery(parameter);
     // 如果为null，则表示开始时间大于等于当前时间，则没有回本率数据
        if (query == null) {
            return null;
        }
        query.setCompanyId(companyId);
        query.setPermissionMediumAccountIds(permissionMediumAccountIds);
       
        switch (query.getPeriod()) {
            case Day:
            	return payBackContrastiveService.payBackContrastiveDailyQueryByMediumAccount(query);
            case Week:
            	return payBackContrastiveService.payBackContrastiveWeeklyQueryByMediumAccount(query);
            case Month:
            	return payBackContrastiveService.payBackContrastiveMonthlyQueryByMediumAccount(query);
		}
		return null;
	}
	
	
	/**
	 * 按【媒体】分别获取回本的趋势图
	 * **/
	@PrivilegeControl(scope = PrivilegeControl.Scope.Public,write = false)
	@RequestMapping("/ad/payback-contrastive/medium.do")
	public Object contrastiveByMedium(HttpServletRequest request , HttpServletResponse response)  throws Exception {
		DEContext context = (DEContext) request.getAttribute("CTX");
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (permissionMediumAccountIds == null) {
            return null;
        }
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        PaybackQuery query = parseQuery(parameter);
     // 如果为null，则表示开始时间大于等于当前时间，则没有回本率数据
        if (query == null) {
            return null;
        }
        query.setCompanyId(companyId);
        query.setPermissionMediumAccountIds(permissionMediumAccountIds);
       
        switch (query.getPeriod()) {
            case Day:
            	return payBackContrastiveService.payBackContrastiveDailyQueryByMedium(query);
            case Week:
            	return payBackContrastiveService.payBackContrastiveWeeklyQueryByMedium(query);
            case Month:
            	return payBackContrastiveService.payBackContrastiveMonthlyQueryByMedium(query);
		}
		return null;
	}
	
	/**
	 * 按【产品】分别获取回本的趋势图
	 * **/
	@PrivilegeControl(scope = PrivilegeControl.Scope.Public,write = false)
	@RequestMapping("/ad/payback-contrastive/product.do")
	public Object contrastiveByProduct(HttpServletRequest request , HttpServletResponse response)  throws Exception {
		DEContext context = (DEContext) request.getAttribute("CTX");
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (permissionMediumAccountIds == null) {
            return null;
        }
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        PaybackQuery query = parseQuery(parameter);
     // 如果为null，则表示开始时间大于等于当前时间，则没有回本率数据
        if (query == null) {
            return null;
        }
        query.setCompanyId(companyId);
        query.setPermissionMediumAccountIds(permissionMediumAccountIds);
        
        switch (query.getPeriod()) {
            case Day:
            	return payBackContrastiveService.payBackContrastiveDailyQueryByProduct(query);
            case Week:
            	return payBackContrastiveService.payBackContrastiveWeeklyQueryByProduct(query);
            case Month:
            	return payBackContrastiveService.payBackContrastiveMonthlyQueryByProduct(query);
		}
		return null;
	}
	

	  /**
     * 解析统计参数
     *
     * @param parameter
     * @return
     * @throws ParseException
     */
    private PaybackQuery parseQuery(DEParameter parameter) throws ParseException {
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }
        String indicatorKey = parameter.getParameter("indicator");
        //校验查询指标，X日回本率，
        if (StringUtils.isBlank(indicatorKey) || !validateInd(Integer.parseInt(indicatorKey))) {
        	ExceptionHandler.throwParameterException(StatusCode.RS_INDICATOR_BLANK);
        }
        
        String period = parameter.getParameter(DEParameter.Keys.PERIOD);
        
        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        Date now = DateUtils.parse(DateUtils.currentDate());
        if(startDate.getTime() >= now.getTime()){
        	return null;
        }
        //构建回本查询条件
        PaybackQuery query = new PaybackQuery();
        query.setStartDate(startDate);
        
        if (StringUtils.isBlank(period)) {
            //设置默认时段为天
            query.setPeriod(Period.Day);
        } else {
            query.setPeriod(Period.parse(period));
        }
        if(query.getPeriod() == Period.Month){  //如果是按月统计
        	//不统计当前月份及未来月份
            Date firstDateOfThisMonth = DateUtils.firstDayOfThisMonth();
            if (!endDate.before(firstDateOfThisMonth)) {
                endDate = DateUtils.lastDayOfPreviousMonth();
            }else{  //自动补全月份到当前月的月底
            	endDate = DateUtils.lastDayOfMonth(endDate);
            }
        }
        query.setEndDate(endDate);
        //解析产品ID列表
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setPlanIds(StringUtil.stringToList(parameter.getPlanIds()));
        query.setAccountIds(StringUtil.stringToList(parameter.getAccountIds()));
        query.setMediumIds(StringUtil.stringToList(parameter.getMediumIds()));
        query.setSize(parameter.getSize());
        query.setIndicator(PayBackIndicator.getdailyIndicator(Integer.parseInt(indicatorKey)));
        return query;
    }
    
    /**
     * 检验传递的指标参数是否正确，
     * 
     * **/
    public boolean validateInd(int indicatorKey) {
		boolean flag = false;
		if(PayBackIndicator.getdailyIndicator(indicatorKey) != null && !PayBackIndicator.getdailyIndicator(indicatorKey).isEmpty()){
			flag = true;
    	}
    	return flag;
	}

	
}
