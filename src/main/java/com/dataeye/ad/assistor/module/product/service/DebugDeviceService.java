package com.dataeye.ad.assistor.module.product.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.product.constants.Constants.TrackingInterface;
import com.dataeye.ad.assistor.module.product.model.DebugDevice;
import com.dataeye.ad.assistor.module.systemaccount.model.PtLoginResponse;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


/**
 * 测试设备Service
 * @author ldj 2017/10/30
 *
 */
@Service("debugDeviceService")
public class DebugDeviceService {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(DebugDeviceService.class);

    /**
     * 查询测试设备列表
     * */
    public List<DebugDevice> query(Integer uid,String appid) {
    	return queryDebugDevice(uid, appid);
	}
    /**
     * 添加测试设备列表
     * */
    public String add(Integer uid,String appid, Integer osType, String deviceName,String device) {
		//查看测试设备名称是否存在
    	isDebugDeviceNameExists(uid, appid, deviceName);
    	return addDebugDevice(uid, appid, osType, deviceName, device);
	}

    /**
	 * 添加一条测试设备信息
	 * @param uid	创建人id
	 * @param appid	产品appid
	 * @param platform	系统类型
	 * @param deviceName	测试设备名称
	 * @param device 测试设备号
	 * @return
	 * @author ldj  2017-10-30
	 * */
	public String addDebugDevice(Integer uid,String appid, Integer platform, String deviceName,String device) {
		//1.构建参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("uid", uid+"");
		params.put("appid", appid);
		params.put("platform", platform+"");
		params.put("deviceName", deviceName);
		params.put("device", device);
		//2.HTTP请求获取数据
		String addDebugDeviceInterface = ConfigHandler.getProperty(TrackingInterface.ADD_DEBUG_DEVICE_KEY, TrackingInterface.ADD_DEBUG_DEVICE_URL);
		String httpResponse = HttpRequest.post(addDebugDeviceInterface, params);
		logger.debug("[httpResponse]:"+httpResponse);
		if(StringUtils.isBlank(httpResponse)) {
    		logger.info("添加一条测试设备信息的接口请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
		// 3.结果数据封装，转换为Map
		JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("添加一条测试设备信息的接口响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	return "SUCCESS";
	}
	 /**
     * 查询测试设备列表
     * @param uid	创建人id
	 * @param appid	产品appid
     * @author ldj  2017-10-30
     * */
    public List<DebugDevice> queryDebugDevice(Integer uid,String appid) {
    	//1.构建参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("uid", uid+"");
		params.put("appid", appid);
		//2.HTTP请求获取数据
		String queryDebugDeviceInterface = ConfigHandler.getProperty(TrackingInterface.QUERY_DEBUG_DEVICE_KEY, TrackingInterface.QUERY_DEBUG_DEVICE_URL);
		String httpResponse = HttpRequest.post(queryDebugDeviceInterface, params);
		logger.debug("[httpResponse]:"+httpResponse);
		if(StringUtils.isBlank(httpResponse)) {
    		logger.info("查询测试设备信息的接口请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
		// 3.结果数据封装，转换为Map
		JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("查询测试设备信息的接口响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	PtLoginResponse<List<DebugDevice>> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<PtLoginResponse<List<DebugDevice>>>(){}.getType());
    	if(response == null || response.getContent() == null) {
        	logger.info("查询测试设备信息的接口请求返回数据为空！");
        	return null;
        }
    	return response.getContent();
	}
    
    /**
	 * 判断一个设备名称是否已经存在
	 * @param uid	创建人id
	 * @param appid	产品appid
	 * @param deviceName	测试设备名称
	 * @return
	 * @author ldj  2017-10-30
	 * */
	public void isDebugDeviceNameExists(Integer uid,String appid, String deviceName) {
		//1.构建参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("uid", uid+"");
		params.put("appid", appid);
		params.put("deviceName", deviceName);
		//2.HTTP请求获取数据
		String debugDeviceInterface = ConfigHandler.getProperty(TrackingInterface.QUERY_DEVICENAME_EXISTS_KEY, TrackingInterface.QUERY_DEVICENAME_EXISTS_URL);
		String httpResponse = HttpRequest.post(debugDeviceInterface, params);
		logger.debug("[httpResponse]:"+httpResponse);
		if(StringUtils.isBlank(httpResponse)) {
    		logger.info("判断一个设备名称是否已经存在的接口请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
		// 3.结果数据封装，转换为Map
		JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("判断一个设备名称是否已经存在的接口响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	JsonObject contentJsonObject = jsonObject.get("content").getAsJsonObject();
    	//JsonObject contentJsonObject = StringUtil.jsonParser.parse(content).getAsJsonObject();
    	int flag = contentJsonObject.get("flag").getAsInt();
    	if(flag == 1) {
    		ExceptionHandler.throwParameterException(StatusCode.DEBUG_DEVICE_NAME_IS_EXISTS);
        }
	}
	
	/**
	 * 删除一条测试设备信息
	 * @param uid	创建人id
	 * @param appid	产品appid
	 * @param id 测试设备的id
	 * @return
	 * @author ldj  2017-10-30
	 * */
	public String deleteDebugDevice(Integer uid,String appid, Integer id) {
		//1.构建参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("uid", uid+"");
		params.put("appid", appid);
		params.put("id", id+"");
		//2.HTTP请求获取数据
		String addDebugDeviceInterface = ConfigHandler.getProperty(TrackingInterface.DELETE_DEBUG_DEVICE_KEY, TrackingInterface.DELETE_DEBUG_DEVICE_URL);
		String httpResponse = HttpRequest.post(addDebugDeviceInterface, params);
		logger.debug("[httpResponse]:"+httpResponse);
		if(StringUtils.isBlank(httpResponse)) {
    		logger.info("添加一条测试设备信息的接口请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
		// 3.结果数据封装，转换为Map
		JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("添加一条测试设备信息的接口响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	return "SUCCESS";
	}
}
