package com.dataeye.ad.assistor.module.advertisement.model;

import java.util.Date;
import java.util.List;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

/**
 * 广告查询参数vo
 * @author ldj
 *
 */
public class AdGroupQueryVo extends DataPermissionDomain {

	/**广告组ID**/
	private Integer adGroupId;
	/**推广计划ID**/
	private Integer planGroupId;
	/**ADT账号ID**/
	private Integer systemAccountId;
	/**
     * 媒体ID列表.
     */
    private List<Integer> mediumIds;
    /**
     * 媒体账号ID列表.
     */
    private List<Integer> mediumAccountIds;

    /**
     * 产品ID列表.
     */
    private List<Integer> productIds;
    
    /**
     * 起始日期.
     */
    private Date startDate;
    /**
     * 截止日期.
     */
    private Date endDate;
    
    /**
     * 推广计划ID列表.
     */
    private List<Integer> planGroupIds;
    
    /**广告组ID列表**/
	private List<Integer> adGroupIds;
	
	private Integer targetingId;
	/**媒体帐号ID**/
	private Integer mediumAccountId;
	
	private Integer productType;
	
	public Integer getPlanGroupId() {
		return planGroupId;
	}
	public void setPlanGroupId(Integer planGroupId) {
		this.planGroupId = planGroupId;
	}
	public Integer getAdGroupId() {
		return adGroupId;
	}
	public void setAdGroupId(Integer adGroupId) {
		this.adGroupId = adGroupId;
	}
	public Integer getSystemAccountId() {
		return systemAccountId;
	}
	public void setSystemAccountId(Integer systemAccountId) {
		this.systemAccountId = systemAccountId;
	}
	public List<Integer> getMediumIds() {
		return mediumIds;
	}
	public void setMediumIds(List<Integer> mediumIds) {
		this.mediumIds = mediumIds;
	}
	public List<Integer> getMediumAccountIds() {
		return mediumAccountIds;
	}
	public void setMediumAccountIds(List<Integer> mediumAccountIds) {
		this.mediumAccountIds = mediumAccountIds;
	}
	public List<Integer> getProductIds() {
		return productIds;
	}
	public void setProductIds(List<Integer> productIds) {
		this.productIds = productIds;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public List<Integer> getPlanGroupIds() {
		return planGroupIds;
	}
	public void setPlanGroupIds(List<Integer> planGroupIds) {
		this.planGroupIds = planGroupIds;
	}
	
	public List<Integer> getAdGroupIds() {
		return adGroupIds;
	}
	public void setAdGroupIds(List<Integer> adGroupIds) {
		this.adGroupIds = adGroupIds;
	}
	
	public Integer getTargetingId() {
		return targetingId;
	}
	public void setTargetingId(Integer targetingId) {
		this.targetingId = targetingId;
	}
	
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	
	public Integer getProductType() {
		return productType;
	}
	public void setProductType(Integer productType) {
		this.productType = productType;
	}
	@Override
	public String toString() {
		return "AdGroupQueryVo [adGroupId=" + adGroupId + ", planGroupId="
				+ planGroupId + ", systemAccountId=" + systemAccountId
				+ ", mediumIds=" + mediumIds + ", mediumAccountIds="
				+ mediumAccountIds + ", productIds=" + productIds
				+ ", startDate=" + startDate + ", endDate=" + endDate
				+ ", planGroupIds=" + planGroupIds + ", adGroupIds="
				+ adGroupIds + ", targetingId=" + targetingId
				+ ", mediumAccountId=" + mediumAccountId + ", productType="
				+ productType + "]";
	}
	
	
}
