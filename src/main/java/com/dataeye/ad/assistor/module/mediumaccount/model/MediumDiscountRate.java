package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.Date;

/**
 * 媒体折扣率
 * @author luzhuyou 2017/02/20
 *
 */
public class MediumDiscountRate {

	/** 折扣率ID */
	private Integer discountRateId;
	/** 媒体账户ID */
	private Integer mediumAccountId;
	/** 折扣率 */
	private Double discountRate;
	/** 生效日期 */
	private Date effectDate;
	/** 失效日期 */
	private Date invalidDate;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	public Integer getDiscountRateId() {
		return discountRateId;
	}
	public void setDiscountRateId(Integer discountRateId) {
		this.discountRateId = discountRateId;
	}
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public Double getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(Double discountRate) {
		this.discountRate = discountRate;
	}
	public Date getEffectDate() {
		return effectDate;
	}
	public void setEffectDate(Date effectDate) {
		this.effectDate = effectDate;
	}
	public Date getInvalidDate() {
		return invalidDate;
	}
	public void setInvalidDate(Date invalidDate) {
		this.invalidDate = invalidDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "MediumDiscountRate [discountRateId=" + discountRateId
				+ ", mediumAccountId=" + mediumAccountId + ", discountRate="
				+ discountRate + ", effectDate=" + effectDate
				+ ", invalidDate=" + invalidDate + ", remark=" + remark
				+ ", createTime=" + createTime + ", updateTime=" + updateTime
				+ "]";
	}
}
