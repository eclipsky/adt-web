package com.dataeye.ad.assistor.module.systemaccount.controller;

import ch.qos.logback.classic.Logger;
import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.*;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.company.model.Company;
import com.dataeye.ad.assistor.module.company.service.CompanyService;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccount;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountGroupVo;
import com.dataeye.ad.assistor.module.mediumaccount.service.MediumAccountService;
import com.dataeye.ad.assistor.module.product.service.ProductService;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.AccountRole;
import com.dataeye.ad.assistor.module.systemaccount.model.DevAuthorizedProductVo;
import com.dataeye.ad.assistor.module.systemaccount.model.PtLoginUser;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountSelectorVo;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;
import com.dataeye.ad.assistor.module.systemaccount.service.DevAuthorizedProductService;
import com.dataeye.ad.assistor.module.systemaccount.service.PtLoginService;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.EncodeUtils;
import com.dataeye.ad.assistor.util.MailUtils;
import com.dataeye.ad.assistor.util.RandomUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luzhuyou
 * 系统账号控制器.
 */
@Controller
public class SystemAccountController {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(SystemAccountController.class);

    @Autowired
    private SystemAccountService systemAccountService;
    @Autowired
    private PtLoginService ptLoginService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private MediumAccountService mediumAccountService;
    @Autowired
    private DevAuthorizedProductService devAuthorizedProductService;
    @Autowired
    private ProductService productService;

    /**
     * 查询系统账号列表信息
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/17
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/systemaccount/queryAccount.do")
    public Object queryAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("查询系统账号列表信息");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();

        String account = parameter.getParameter("account");
        if (StringUtils.isBlank(account)) {
            account = null;
        }

        String groupType = parameter.getParameter("groupType");
        if (StringUtils.isBlank(groupType) || !StringUtils.isNumeric(groupType) || !Constants.GroupType.isGroupType(Integer.parseInt(groupType.trim()))) {
            groupType = Constants.GroupType.ALL + "";
        }

        // 如果所传参数中分组ID不为空且为数字
        String groupId = parameter.getParameter("groupId");
        Integer detailGroupId = null;
        if (StringUtils.isNotBlank(groupId) && StringUtils.isNumeric(groupId) && !groupId.trim().equals(Constants.GroupId.ALL)) {
            detailGroupId = Integer.parseInt(groupId.trim());
        } else if (Constants.GroupType.NONE == Integer.parseInt(groupType.trim())) {
            detailGroupId = Constants.GroupId.NONE;
        }

        PageHelper.startPage(parameter.getPageId(), parameter.getPageSize());
        List<SystemAccountVo> systemAccountresult = systemAccountService.query(account==null?null:account.trim(), detailGroupId, companyId);
        for (SystemAccountVo vo : systemAccountresult) {
        	if(vo.getAccountRole().intValue() == AccountRole.ENGINEER){
       		 DevAuthorizedProductVo oldDevAuthorizedProduct = devAuthorizedProductService.getDevAuthorizedProduct(vo.getAccountId());
       		 if(oldDevAuthorizedProduct != null && oldDevAuthorizedProduct.getProductIds() != null){
       			 String[] productIds = oldDevAuthorizedProduct.getProductIds().split(",");
       			 String productNames = productService.getProductNamesByProductIds(productIds);
       			 vo.setProductNames(productNames);
         	   }
            }else{
            	List<MediumAccountGroupVo> responsibleAccountList = new ArrayList<MediumAccountGroupVo>();
            	List<MediumAccount> responsibleMediumAccountList = mediumAccountService.getResponsibleFollowersGroupMedium(companyId, vo.getAccountName(),null,null);
            	if(responsibleMediumAccountList.size()>0){
            		responsibleAccountList = mediumAccountService.getMediumAccountListGroupMedium(responsibleMediumAccountList);
            	}
            	
            	List<MediumAccountGroupVo> followerAccountList = new ArrayList<MediumAccountGroupVo>();
            	List<MediumAccount> followerMediumAccountList = mediumAccountService.getResponsibleFollowersGroupMedium(companyId, null,vo.getAccountName(),null);
            	if(followerMediumAccountList.size()>0){
            		followerAccountList = mediumAccountService.getMediumAccountListGroupMedium(followerMediumAccountList);
            	}
            	vo.setResponsibleAccountList(responsibleAccountList);
            	vo.setFollowerAccountList(followerAccountList);
            }
		}
        PageData pageData = PageDataHelper.getPageData((Page) systemAccountresult);
        return pageData;
    }

    /**
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/systemaccount/getAccount.do")
    public Object getAccountById(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String accountId = parameter.getAccountId();
        if (StringUtils.isBlank(accountId) || !NumberUtils.isDigits(accountId)) {
            ExceptionHandler.throwParameterException(StatusCode.ACCO_ACCOUNT_ID_IS_NULL);
        }
        return this.systemAccountService.get(Integer.valueOf(accountId));
    }

    /**
     * 新增系统账号
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/17
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/systemaccount/addAccount.do")
    public Object addAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("新增系统账号");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String accountName = parameter.getParameter("accountName");
        String accountAlias = parameter.getParameter("accountAlias");
        String mobile = parameter.getParameter("mobile");
        String status = parameter.getParameter("status");
        String menuPermission = parameter.getParameter("menuPermission");
        String responsibleAccounts = parameter.getParameter("responsibleAccounts");
        String followerAccounts = parameter.getParameter("followerAccounts");
        String productIds = parameter.getParameter("productIds");
        String accountRole = parameter.getParameter(DEParameter.Keys.ACCOUNT_ROLE);
        String indicatorPermission = parameter.getParameter(DEParameter.Keys.INDICATOR_PERMISSION);
        if (StringUtils.isBlank(accountName)) {
            logger.error("系统账号名称不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ACCO_ACCOUNT_NAME_IS_NULL);
        }
        if (StringUtils.isBlank(accountAlias)) {
            accountAlias = null;
        }
        if (StringUtils.isBlank(mobile)) {
            mobile = null;
        }
        if (StringUtils.isBlank(status) || !StringUtils.isNumeric(status) || !Constants.UserStatus.isUserStatus(Integer.parseInt(status.trim()))) {
            status = Constants.UserStatus.NORMAL + "";
        }
        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();

        // 将游戏转换为小写
        accountName = accountName.toLowerCase().trim();
        // 校验系统账号是否为邮箱
        if (!accountName.matches(Constants.EMAIL_REGEX)) {
            logger.error("系统账号格式不正确，应为邮箱格式.");
            ExceptionHandler.throwParameterException(StatusCode.ACCO_ACCOUNT_NAME_FORMAT_ERROR);
        }

        // 校验账号名称（邮箱）是否已存在
        SystemAccountVo account = systemAccountService.get(accountName);
        if (account != null) {
            logger.error("系统账号名称已存在.");
            ExceptionHandler.throwParameterException(StatusCode.ACCO_ACCOUNT_NAME_EXIST);
        }

        if (StringUtils.isBlank(accountRole)) {
            ExceptionHandler.throwParameterException(StatusCode.ACCO_ACCOUNT_ROLE_IS_NULL);
        }

        if(Integer.valueOf(accountRole) != AccountRole.ENGINEER && StringUtils.isNotBlank(productIds)){
        	 ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        // 获取系统账号初始密码
        String initialPwd = RandomUtils.getRandomString(Constants.INITIAL_PWD_LENGTH);

        // 加密初始密码
        String encodePwd = EncodeUtils.md5Encode(initialPwd);

        // 新增系统账号(添加到统一登录账户表)
        Company company = companyService.get(companyId);
        PtLoginUser user = new PtLoginUser();
        user.setCompanyName(company.getCompanyName());
        user.setUserID(accountName);
        user.setCreateID(company.getUid());
        user.setPassword(encodePwd);
        user.setUserName(accountAlias);
        user.setTel(mobile);
        user = ptLoginService.addPtLoginSubAccount(user); 
        logger.info(user.toString());

        // 新增系统账号(添加到ADT系统账户表)
        systemAccountService.add(accountName, accountAlias, user.getPassword(), mobile, Integer.parseInt(status), Constants.GroupId.NONE,
        		menuPermission, companyId, Integer.valueOf(accountRole), indicatorPermission, responsibleAccounts, followerAccounts,productIds);

        if (accountAlias != null) {
            accountAlias = accountName.concat("(").concat(accountAlias).concat(")");
        } else {
            accountAlias = accountName;
        }
        // 发送账号创建成功提醒邮件
        if (!encodePwd.equals(user.getPassword())) {
            initialPwd = "您在DataEye所注册账号的旧密码，如已忘记，请于登录页面点击忘记密码";
        }
        MailUtils.sendCreateSystemAccountMail(accountName, initialPwd, accountAlias);

        return user;
    }

    /**
     * 修改系统账号（修改系统账号时不允许修改密码）
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/17
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/systemaccount/modifyAccount.do")
    public Object modifyAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("修改系统账号");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String accountId = parameter.getParameter("accountId");
        String accountName = parameter.getParameter("accountName");
        String accountAlias = parameter.getParameter("accountAlias");
        String mobile = parameter.getParameter("mobile");
        String status = parameter.getParameter("status");
        String menuPermission = parameter.getParameter("menuPermission");
        String indicatorPermission = parameter.getParameter(DEParameter.Keys.INDICATOR_PERMISSION);
        String accountRole = parameter.getParameter(DEParameter.Keys.ACCOUNT_ROLE);
        String responsibleAccounts = parameter.getParameter("responsibleAccounts");
        String followerAccounts = parameter.getParameter("followerAccounts");
        String productIds = parameter.getParameter("productIds");
        
        if (StringUtils.isBlank(accountId) || !StringUtils.isNumeric(accountId) || StringUtils.isBlank(accountName)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }

        if (StringUtils.isBlank(accountRole)) {
            ExceptionHandler.throwParameterException(StatusCode.ACCO_ACCOUNT_ROLE_IS_NULL);
        }

        if(Integer.valueOf(accountRole) != AccountRole.ENGINEER && StringUtils.isNotBlank(productIds)){
        	 ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        if (StringUtils.isBlank(status) || !StringUtils.isNumeric(status) || !Constants.UserStatus.isUserStatus(Integer.parseInt(status.trim()))) {
            status = Constants.UserStatus.NORMAL + "";
        }

        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();

        int result = systemAccountService.modify(Integer.parseInt(accountId), accountName, accountAlias, Integer.valueOf(accountRole), mobile,
        		Integer.parseInt(status), menuPermission, indicatorPermission, companyId, null, responsibleAccounts, followerAccounts,productIds);
        // 刷新UserInSession
        SessionContainer.refreshUserInSession(request);
        return result;
    }

    /**
     * 删除系统账号
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/17
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/systemaccount/deleteAccount.do")
    public Object deleteAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("删除系统账号");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String accountId = parameter.getParameter("accountId");
        if (StringUtils.isBlank(accountId) || !StringUtils.isNumeric(accountId)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        SystemAccountVo systemAccountVo = systemAccountService.get(Integer.parseInt(accountId));
        if (systemAccountVo == null) {
            logger.error("系统账号不存在");
            ExceptionHandler.throwParameterException(StatusCode.ACCO_ACCOUNT_ID_NOT_EXIST);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession.getAccountId() == Integer.parseInt(accountId)) {
            logger.error("不能删除当前系统登录账号");
            ExceptionHandler.throwParameterException(StatusCode.ACCO_ACCOUNT_DELETE_REFUSE);
        }
        return systemAccountService.delete(systemAccountVo);
    }

    /**
     * 系统账号密码重置
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/18
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/systemaccount/resetPassword.do")
    public Object resetPassword(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("系统账号密码重置");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String accountId = parameter.getParameter("accountId");
        if (StringUtils.isBlank(accountId) || !StringUtils.isNumeric(accountId)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }

        // 校验账号ID是否存在
        SystemAccountVo account = systemAccountService.get(Integer.parseInt(accountId.trim()));
        if (account == null) {
            logger.error("系统账号不存在");
            ExceptionHandler.throwParameterException(StatusCode.ACCO_ACCOUNT_ID_NOT_EXIST);
        }

        // 获取系统账号初始密码
        String initialPwd = RandomUtils.getRandomString(Constants.INITIAL_PWD_LENGTH);

        // 加密初始密码
        String encodePwd = EncodeUtils.md5Encode(initialPwd);

        // 发送账号创建成功提醒邮件
        String accountName = account.getAccountName();
        String accountAlias = account.getAccountAlias();
        if (accountAlias != null) {
            accountAlias = accountName.concat("(").concat(accountAlias).concat(")");
        } else {
            accountAlias = accountName;
        }
        MailUtils.sendResetPwdMail(accountName, initialPwd, accountAlias);

        // 修改密码
        return systemAccountService.modifyPassword(account.getAccountId(), encodePwd);

    }


    /**
     * 修改系统账号密码
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/systemaccount/modifyPassword.do")
    public Object modifyPassword(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("修改系统账号密码");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        // 解析请求参数
        String accountId = parameter.getParameter("accountId");
        String passwordOld = parameter.getParameter("passwordOld");
        String password = parameter.getParameter("password");

        if (StringUtils.isBlank(accountId) || !StringUtils.isNumeric(accountId) || StringUtils.isBlank(passwordOld) || StringUtils.isBlank(password)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }

        // 校验账号ID是否存在
        SystemAccountVo account = systemAccountService.get(Integer.parseInt(accountId.trim()));
        if (account == null) {
            logger.error("系统账号不存在");
            ExceptionHandler.throwParameterException(StatusCode.ACCO_ACCOUNT_ID_NOT_EXIST);
        }

        if (!passwordOld.equals(account.getPassword())) {
            logger.error("旧密码错误");
            ExceptionHandler.throwParameterException(StatusCode.LOGI_PASSWORDOLD_WRONG);
        }
        logger.info("modify system account include password");
        // 修改密码
        return systemAccountService.modifyPassword(account.getAccountId(), password);
    }

    /**
     * 下拉框查询系统账号
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/23
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/systemaccount/queryAccountForSelector.do")
    public Object queryAccountForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("下拉框查询系统账号");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String groupType = parameter.getParameter("groupType");
        if (StringUtils.isBlank(groupType) || !StringUtils.isNumeric(groupType) || !Constants.GroupType.isGroupType(Integer.parseInt(groupType.trim()))) {
            groupType = Constants.GroupType.ALL + "";
        }
        int arrayLength = 0;
        // 如果所传参数中分组类型为未分组，则分组ID设置为-1
        Integer noneGroupId = null;
        if (Constants.GroupType.NONE == Integer.parseInt(groupType.trim())) {
            noneGroupId = Constants.GroupId.NONE;
            arrayLength++;
        }
        // 如果所传参数中分组ID不为空且为数字
        String groupId = parameter.getParameter("groupId");
        Integer detailGroupId = null;
        if (StringUtils.isNotBlank(groupId) && StringUtils.isNumeric(groupId)) {
            detailGroupId = Integer.parseInt(groupId.trim());
            arrayLength++;
        }

        // 如果分组ID为未分组（ID=-1）且分组ID为具体分组ID，则将两个分组ID都加入数组，查询它们的并集
        Integer[] groupIds = null;
        if (arrayLength > 0) {
            int newArrayLength = 0;
            groupIds = new Integer[arrayLength];
            if (noneGroupId != null) {
                groupIds[newArrayLength++] = noneGroupId;
            }
            if (detailGroupId != null) {
                groupIds[newArrayLength++] = detailGroupId;
            }
        }

        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
        List<SystemAccountSelectorVo> result = systemAccountService.queryForSelector(groupIds, companyId, false, false);
        return result;
    }

    /**
     *  组长候选人列表
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/systemaccount/group-leader-candidates.do")
    public Object groupLeadeCandidates(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        return systemAccountService.queryForSelector(null,userInSession.getCompanyId(),true,false);
    }

    /**
     *  负责人或关注者候选人列表
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/systemaccount/manager-follower-candidates.do")
    public Object managerFollowerCandidates(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        return systemAccountService.queryForSelector(null,userInSession.getCompanyId(),false,true);
    }

    /**
     * 下拉框查询子账户
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/systemaccount/querySubAccountForSelector.do")
    public Object querySubAccountForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        return systemAccountService.querySubAccountForSelector(userInSession.getPermissionSystemAccountIds(), userInSession.getCompanyId());
    }
}
