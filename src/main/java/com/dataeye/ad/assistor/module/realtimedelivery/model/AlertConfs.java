package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.google.gson.annotations.Expose;

/**
 * Created by haungzehai on 2017/2/28.
 */
public class AlertConfs {
    @Expose
    private AlertConf general;
    @Expose
    private AlertConf advanced;

    public AlertConf getGeneral() {
        return general;
    }

    public void setGeneral(AlertConf general) {
        this.general = general;
    }

    public AlertConf getAdvanced() {
        return advanced;
    }

    public void setAdvanced(AlertConf advanced) {
        this.advanced = advanced;
    }

    public AlertConfs(AlertConf general, AlertConf advanced) {
        this.general = general;
        this.advanced = advanced;
    }

    @Override
    public String toString() {
        return "AlertConfs{" +
                "general=" + general +
                ", advanced=" + advanced +
                '}';
    }
}
