package com.dataeye.ad.assistor.module.systemaccount.model;

import java.util.Date;


/**
 * 系统账号登录信息实体类
 * 
 * @author luzhuyou 2017-02-18
 */
public class AccountLoginInfo {
	/** 公司ID */
	private Integer companyId;
	/** 账号ID */
	private Integer loginId;
	/** 账号ID */
	private Integer accountId;
	/** 登录IP */
	private String loginIp;
	/** 状态码 */
	private Integer statusCode;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getLoginId() {
		return loginId;
	}
	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public String getLoginIp() {
		return loginIp;
	}
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "AccountLoginInfo [companyId=" + companyId + ", loginId="
				+ loginId + ", accountId=" + accountId + ", loginIp=" + loginIp
				+ ", statusCode=" + statusCode + ", remark=" + remark
				+ ", createTime=" + createTime + "]";
	}
	
}
