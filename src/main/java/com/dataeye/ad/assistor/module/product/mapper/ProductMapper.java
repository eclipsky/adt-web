package com.dataeye.ad.assistor.module.product.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.product.model.Product;
import com.dataeye.ad.assistor.module.product.model.ProductSelectorVo;
import com.dataeye.ad.assistor.module.product.model.ProductVo;

/**
 * 产品信息映射器.
 * Created by luzhuyou
 */
@MapperScan
public interface ProductMapper {

	/**
	 * 查询产品信息列表
	 * @param companyId
	 * @return
	 */
    public List<ProductVo> query(Product product);

    /**
	 *  根据产品名称获取产品信息
	 * @param productName
	 * @param companyId
	 * @param product_id
	 * @param app_id
	 * @return
	 */
	public ProductVo get(Product vo);
	
	/**
	 * 新增产品信息
	 * @param productVo
	 * @return
	 */
	public int add(Product product);

	/**
	 * 修改产品信息
	 * @param productVo
	 * @return
	 */
	public int update(Product product);

	/**
	 * 下拉框查询产品
	 * @param companyId
	 * @return
	 */
	public List<ProductSelectorVo> queryForSelector(ProductSelectorVo vo);
	

	/**
	 * 修改关联TrackingApp
	 * @param productVo
	 * @return
	 */
	public int updateRefApp(Product product);
	
	/**
	 * 根据产品id查询投放媒体帐号
	 * @param productName
	 * @param companyId
	 * @return
	 */
	public ProductVo getMediumAccountInfoByProudctId(Product vo);

	/**
	 * 根据多个产品ID查询产品列表
	 * @param productIds 产品ID，如果多个则以逗号分隔
	 * @return
	 */
	public List<ProductVo> queryByProductIds(String[] productIds);
	
	
	/**
	 * 根据多个产品ID查询产品列表
	 * @param productIds 产品ID，如果多个则以逗号分隔
	 * @return
	 */
	public String getProductNamesByProductIds(String[] productIds);
	
}
