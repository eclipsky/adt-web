package com.dataeye.ad.assistor.module.advertisement.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



/**
 * 广告创意
 * Created by ldj
 */
public class AdcreativeVO {

	/**推广计划id*/
    private Integer planGroupId;
    
	@Expose
    private Integer adcreativeId;

	@Expose
    private String adcreativeName;

	//创意规格 id，
    private Integer adcreativeTemplateId;

	@Expose
    private Object adcreativeElements;
    
	//落地页 url
	@Expose
    private String destination_url;
        
    private String siteSet;
    //应用直达页 URL
    private String deepLink;
    /*
     * 标的物 id
     **/
    private String productRefsId;



	public Integer getPlanGroupId() {
		return planGroupId;
	}

	public void setPlanGroupId(Integer planGroupId) {
		this.planGroupId = planGroupId;
	}

	public Integer getAdcreativeId() {
		return adcreativeId;
	}

	public void setAdcreativeId(Integer adcreativeId) {
		this.adcreativeId = adcreativeId;
	}

	public String getAdcreativeName() {
		return adcreativeName;
	}

	public void setAdcreativeName(String adcreativeName) {
		this.adcreativeName = adcreativeName;
	}

	public Integer getAdcreativeTemplateId() {
		return adcreativeTemplateId;
	}

	public void setAdcreativeTemplateId(Integer adcreativeTemplateId) {
		this.adcreativeTemplateId = adcreativeTemplateId;
	}

	public Object getAdcreativeElements() {
		return adcreativeElements;
	}

	public void setAdcreativeElements(Object adcreativeElements) {
		this.adcreativeElements = adcreativeElements;
	}

	public String getDestination_url() {
		return destination_url;
	}

	public void setDestination_url(String destination_url) {
		this.destination_url = destination_url;
	}

	public String getSiteSet() {
		return siteSet;
	}

	public void setSiteSet(String siteSet) {
		this.siteSet = siteSet;
	}

	public String getDeepLink() {
		return deepLink;
	}

	public void setDeepLink(String deepLink) {
		this.deepLink = deepLink;
	}

	public String getProductRefsId() {
		return productRefsId;
	}

	public void setProductRefsId(String productRefsId) {
		this.productRefsId = productRefsId;
	}

	@Override
	public String toString() {
		return "AdcreativeVO [planGroupId=" + planGroupId + ", adcreativeId="
				+ adcreativeId + ", adcreativeName=" + adcreativeName
				+ ", adcreativeTemplateId=" + adcreativeTemplateId
				+ ", adcreativeElements=" + adcreativeElements
				+ ", destination_url=" + destination_url + ", siteSet="
				+ siteSet + ", deepLink=" + deepLink + ", productRefsId="
				+ productRefsId + "]";
	}


}
