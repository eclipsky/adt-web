package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

/**
 * Created by huangzehai on 2017/7/11.
 */
public class OperationLogQuery extends DataPermissionDomain {
    private Integer planId;

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    @Override
    public String toString() {
        return "OperationLogQuery{" +
                "planId=" + planId +
                "} " + super.toString();
    }
}
