package com.dataeye.ad.assistor.module.realtimedelivery.constant;

/**
 * Created by huangzehai on 2017/5/19.
 */
public final class PlanSwitchMessages {

    /**
     * 成功消息。
     */
    public static final String SUCCESS = "切换计划开关成功";
    /**
     * 失败消息.
     */
    public static final String FAIL = "切换计划开关失败";

    private PlanSwitchMessages() {

    }
}
