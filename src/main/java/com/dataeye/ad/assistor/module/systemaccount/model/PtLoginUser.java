package com.dataeye.ad.assistor.module.systemaccount.model;

/**
 * 统一登录用户信息
 * @author luzhuyou 2017/05/10
 *
 */
public class PtLoginUser {
	/**
	 * 用户ID
	 */
	private int UID;
	/**
	 * 邮箱号
	 */
	private String userID;
	/**
	 * 用户名称
	 */
	private String userName;
	/**
	 * 公司名称
	 */
	private String companyName;
	/**
	 * 电话
	 */
	private String tel;
	/**
	 * QQ
	 */
	private String qq;
	/**
	 * 创建时间
	 */
	private int createTime;
	/**
	 * 创建人UID
	 */
	private int createID;
	/**
	 * 状态
	 */
	private int status;
	/**
	 * 密码
	 */
	private String password;
	public int getUID() {
		return UID;
	}
	public void setUID(int uID) {
		UID = uID;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public int getCreateTime() {
		return createTime;
	}
	public void setCreateTime(int createTime) {
		this.createTime = createTime;
	}
	public int getCreateID() {
		return createID;
	}
	public void setCreateID(int createID) {
		this.createID = createID;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "PtLoginUser [UID=" + UID + ", userID=" + userID + ", userName="
				+ userName + ", companyName=" + companyName + ", tel=" + tel
				+ ", qq=" + qq + ", createTime=" + createTime + ", createID="
				+ createID + ", status=" + status + ", password=" + password
				+ "]";
	}
	
}
