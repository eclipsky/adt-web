package com.dataeye.ad.assistor.module.stat.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.association.model.AssociationRuleHistoryVo;
import com.dataeye.ad.assistor.module.stat.model.PlanStat;
import com.dataeye.ad.assistor.module.stat.model.PlanStatHisVo;

/**
 * 计划统计映射器.
 * Created by luzhuyou 2017/03/16
 */
@MapperScan
public interface PlanStatMapper {

	/**
	 * 修改（按天）计划统计表
	 * @param planStat
	 * @return
	 */
	public int updateDayStat(PlanStat planStat);
	
	/**
	 * 修改（实时）计划统计表
	 * @param planStat
	 * @return
	 */
	public int updateRealTimeStat(PlanStat planStat);
	
	/**
	 * 查询历史上落地页或包已被绑定的计划关联关系（不包含指定计划）
	 * @return
	 */
	public List<PlanStatHisVo> queryHisPossibleConflictRelation(AssociationRuleHistoryVo vo);

}
