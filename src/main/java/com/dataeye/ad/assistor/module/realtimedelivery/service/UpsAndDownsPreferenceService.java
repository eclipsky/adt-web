package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.report.model.IndicatorPreferenceQuery;
import com.dataeye.ad.assistor.module.report.model.UpsAndDownsPreference;

/**
 * Created by huangzehai on 2017/7/6.
 */
public interface UpsAndDownsPreferenceService {

    /**
     * 获取指定ADT账号的指标涨跌偏好.
     *
     * @param query
     * @return
     */
    UpsAndDownsPreference getUpsAndDownsPreference(IndicatorPreferenceQuery query);

    /**
     * 更新指标涨跌偏好
     *
     * @param upsAndDownsPreference
     * @return
     */
    int updateUpsAndDownsPreference(UpsAndDownsPreference upsAndDownsPreference);

    /**
     * 是否存在指定ADT账号的指标涨跌偏好
     *
     * @param accountId
     * @return
     */
    boolean hasUpsAndDownsPreference(int accountId);

    /**
     * 插入指标涨跌偏好
     *
     * @param upsAndDownsPreference
     * @return
     */
    int insertUpsAndDownsPreference(UpsAndDownsPreference upsAndDownsPreference);

    /**
     * 删除指标涨跌偏好
     *
     * @param accountId
     * @return
     */
    int deleteUpsAndDownsPreference(int accountId);

    /**
     * 添加默认指标涨跌显示偏好.
     *
     * @param companyId
     * @param accountId
     * @param indicatorPermission
     * @return
     */
    int addDefaultUpsAndDownsPreference(int companyId, int accountId, int accountRole, String indicatorPermission);

    /**
     * 由于指标权限发生改变，更新指标涨跌显示偏好
     *
     * @param companyId
     * @param accountId
     * @param accountRole
     * @param indicatorPermission
     */
    void updateUpsAndDownsPreferenceAsPermissionChanged(int companyId, int accountId, int accountRole, String indicatorPermission);
}
