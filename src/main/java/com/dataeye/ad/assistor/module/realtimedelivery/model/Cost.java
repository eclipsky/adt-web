package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;

/**
 * Created by huangzehai on 2017/4/27.
 */
public class Cost {
    /**
     * 当前消耗.
     */
    @Expose
    private BigDecimal cost;

    /**
     * 媒体ID.
     */
    private Integer mediumId;

    /**
     * 媒体账号ID.
     */
    private Integer mediumAccountId;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    @Override
    public String toString() {
        return "Cost{" +
                "cost=" + cost +
                ", mediumId=" + mediumId +
                ", mediumAccountId=" + mediumAccountId +
                '}';
    }
}
