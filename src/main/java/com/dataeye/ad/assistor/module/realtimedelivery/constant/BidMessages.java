package com.dataeye.ad.assistor.module.realtimedelivery.constant;

/**
 * Created by huangzehai on 2017/5/19.
 */
public final class BidMessages {
    private BidMessages() {
    }

    /**
     * 成功消息。
     */
    public static final String SUCCESS = "修改出价成功";
    /**
     * 失败消息.
     */
    public static final String FAIL = "修改出价失败";
}
