package com.dataeye.ad.assistor.module.product.model;


import java.util.List;

import com.google.gson.annotations.Expose;


/**
 * 测试日志
 * @author ldj 2017/10/30
 *
 */
public class DebugLogData {

	/** 日志状态 */
	@Expose
	private Integer status;
	/**时间 */
	@Expose
	private String receiveTime;
	/** 基础信息 */
	@Expose
	private List<Link> basicInfo;
	/** 事件信息 */
	@Expose
	private List<Link> eventInfo;
	/** 错误信息 */
	@Expose
	private List<Link> errorInfo;
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getReceiveTime() {
		return receiveTime;
	}
	public void setReceiveTime(String receiveTime) {
		this.receiveTime = receiveTime;
	}
	public List<Link> getBasicInfo() {
		return basicInfo;
	}
	public void setBasicInfo(List<Link> basicInfo) {
		this.basicInfo = basicInfo;
	}
	public List<Link> getEventInfo() {
		return eventInfo;
	}
	public void setEventInfo(List<Link> eventInfo) {
		this.eventInfo = eventInfo;
	}
	public List<Link> getErrorInfo() {
		return errorInfo;
	}
	public void setErrorInfo(List<Link> errorInfo) {
		this.errorInfo = errorInfo;
	}
	@Override
	public String toString() {
		return "DebugLogData [status=" + status + ", receiveTime="
				+ receiveTime + ", basicInfo=" + basicInfo + ", eventInfo="
				+ eventInfo + ", errorInfo=" + errorInfo + "]";
	}
	
	
}
