package com.dataeye.ad.assistor.module.report.model;

/**
 * Created by huangzehai on 2017/5/23.
 */
public class DeliveryKeyIndicatorQuery extends DateRangeQuery {

    private Integer productId;
    private Integer mediumId;
    /**
     * 是否过滤自然流量.
     */
    private boolean filterNaturalFlow;


    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public boolean isFilterNaturalFlow() {
        return filterNaturalFlow;
    }

    public void setFilterNaturalFlow(boolean filterNaturalFlow) {
        this.filterNaturalFlow = filterNaturalFlow;
    }

    @Override
    public String toString() {
        return "DeliveryKeyIndicatorQuery{" +
                ", productId=" + productId +
                ", mediumId=" + mediumId +
                ", filterNaturalFlow=" + filterNaturalFlow +
                "} " + super.toString();
    }
}
