package com.dataeye.ad.assistor.module.advertisement.model.tencent;

import java.util.List;

/**
 * 广告组
 * */
public class Adgroup {

	/**
	 * 广告组 id
	 * */
	private Integer adgroup_id;
	/**
	 * 广告组名称
	 * */
	private String adgroup_name;
	/**
	 * 投放站点集合
	 * */
	private List<String> site_set;
	/**
	 * 广告优化目标类型
	 * */
	private String optimization_goal;
	/**
	 * 计费类型
	 * */
	private String billing_event;
	/**
	 *广告出价，单位为分
	 * */
	private Integer bid_amount;
	/**
	 * 日限额，单位为分
	 * */
	private Integer daily_budget;
	/**
	 *标的物类型，
	 * */
	private String product_type;
	/**
	 * 标的物 id
	 * */
	private String product_refs_id;
	/**
	 * 子标的物 id
	 * */
	private String sub_product_refs_id;
	/**
	 * 标的物 id
	 * */
	private Integer targeting_id;
	/**
	 * 开始投放日期，日期格式， YYYY-mm-dd
	 * */
	private String begin_date;
	/**
	 * 结束投放日期，日期格式， YYYY-mm-dd
	 * */
	private String end_date;
	/**
	 * 投放时间段，格式为 48 * 7 位字符串，且都为 0 和 1
	 * 0 为不投放， 1 为投放
	 * */
	private String time_series;
	/**
	 * 客户设置的状态
	 * */
	private String configured_status;
	/**
	 * 系统状态
	 * */
	private String system_status;
	/**
	 *审核消息
	 * */
	private String reject_message;
	public Integer getAdgroup_id() {
		return adgroup_id;
	}
	public void setAdgroup_id(Integer adgroup_id) {
		this.adgroup_id = adgroup_id;
	}
	public String getAdgroup_name() {
		return adgroup_name;
	}
	public void setAdgroup_name(String adgroup_name) {
		this.adgroup_name = adgroup_name;
	}
	public List<String> getSite_set() {
		return site_set;
	}
	public void setSite_set(List<String> site_set) {
		this.site_set = site_set;
	}
	public String getOptimization_goal() {
		return optimization_goal;
	}
	public void setOptimization_goal(String optimization_goal) {
		this.optimization_goal = optimization_goal;
	}
	public String getBilling_event() {
		return billing_event;
	}
	public void setBilling_event(String billing_event) {
		this.billing_event = billing_event;
	}
	public Integer getBid_amount() {
		return bid_amount;
	}
	public void setBid_amount(Integer bid_amount) {
		this.bid_amount = bid_amount;
	}
	public Integer getDaily_budget() {
		return daily_budget;
	}
	public void setDaily_budget(Integer daily_budget) {
		this.daily_budget = daily_budget;
	}
	public String getProduct_type() {
		return product_type;
	}
	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}
	public String getProduct_refs_id() {
		return product_refs_id;
	}
	public void setProduct_refs_id(String product_refs_id) {
		this.product_refs_id = product_refs_id;
	}
	public String getSub_product_refs_id() {
		return sub_product_refs_id;
	}
	public void setSub_product_refs_id(String sub_product_refs_id) {
		this.sub_product_refs_id = sub_product_refs_id;
	}
	public Integer getTargeting_id() {
		return targeting_id;
	}
	public void setTargeting_id(Integer targeting_id) {
		this.targeting_id = targeting_id;
	}
	public String getBegin_date() {
		return begin_date;
	}
	public void setBegin_date(String begin_date) {
		this.begin_date = begin_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getTime_series() {
		return time_series;
	}
	public void setTime_series(String time_series) {
		this.time_series = time_series;
	}
	public String getConfigured_status() {
		return configured_status;
	}
	public void setConfigured_status(String configured_status) {
		this.configured_status = configured_status;
	}
	public String getSystem_status() {
		return system_status;
	}
	public void setSystem_status(String system_status) {
		this.system_status = system_status;
	}
	public String getReject_message() {
		return reject_message;
	}
	public void setReject_message(String reject_message) {
		this.reject_message = reject_message;
	}
	@Override
	public String toString() {
		return "Adgroup [adgroup_id=" + adgroup_id + ", adgroup_name="
				+ adgroup_name + ", site_set=" + site_set
				+ ", optimization_goal=" + optimization_goal
				+ ", billing_event=" + billing_event + ", bid_amount="
				+ bid_amount + ", daily_budget=" + daily_budget
				+ ", product_type=" + product_type + ", product_refs_id="
				+ product_refs_id + ", sub_product_refs_id="
				+ sub_product_refs_id + ", targeting_id=" + targeting_id
				+ ", begin_date=" + begin_date + ", end_date=" + end_date
				+ ", time_series=" + time_series + ", configured_status="
				+ configured_status + ", system_status=" + system_status
				+ ", reject_message=" + reject_message + "]";
	}

}
