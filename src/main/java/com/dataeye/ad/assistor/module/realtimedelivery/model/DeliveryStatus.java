package com.dataeye.ad.assistor.module.realtimedelivery.model;

/**
 * Created by huangzeahai on 2017/5/15.
 */
public class DeliveryStatus extends MediumPlan {
    /**
     * 投放状态.
     */
    private OperationStatus status;

    public OperationStatus getStatus() {
        return status;
    }

    public void setStatus(OperationStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "DeliveryStatus{" +
                "status=" + status +
                "} " + super.toString();
    }
}
