package com.dataeye.ad.assistor.module.realtimedelivery.service;

import java.util.List;

import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceAlertConf;

/**
 * Created by huangzehai on 2017/4/28.
 */
public interface BalanceAlertService {
    /**
     * 保存余额告警配置.
     *
     * @param balanceAlertConf
     */
    int saveBalanceAlertConf(BalanceAlertConf balanceAlertConf);

    /**
     * 获取指定系统账号的余额告警配置.
     *
     * @param accountId
     */
    BalanceAlertConf getBalanceAlertConfByAccountId(int accountId);

    /**
     * 发送余额告警邮件
     */
    void sentBalanceWarningEmail();
    
    /**
     * 根据公司id，获取该公司内的余额告警配置.
     *
     * @param companyId
     */
    List<BalanceAlertConf> getBalanceAlertConfByCompanyId(int companyId);
    
    /**
     * 修改余额告警配置.
     *
     * @param balanceAlertConf
     */
    int updateBalanceAlertConf(int accountId,int companyId, int balanceRedNum);
}
