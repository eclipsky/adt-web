package com.dataeye.ad.assistor.module.advertisement.controller;


import java.awt.image.BufferedImage;
import java.io.File;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.ImageUtil;
import com.dataeye.ad.assistor.module.advertisement.model.AdFiles;
import com.dataeye.ad.assistor.module.advertisement.model.MarketingApiAuth;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.AdImages;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.AdVideos;
import com.dataeye.ad.assistor.module.advertisement.service.ApiTxAdGroupService;
import com.dataeye.ad.assistor.module.advertisement.service.CreativeService;
import com.dataeye.ad.assistor.module.advertisement.service.tencent.AdVideoService;
import com.dataeye.ad.assistor.module.advertisement.service.tencent.ImageService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;


/**
 * Created by ldj
 * 广告投放
 * 广告创意控制器.
 */
@Controller
public class AdCreativeInfoController {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(AdCreativeInfoController.class);

	@Autowired
	private CreativeService creativeService;
	@Autowired
	private ImageService imageService;
	@Autowired
	private ApiTxAdGroupService apiTxAdGroupService;
	@Autowired
	private AdVideoService adVideoService;
   
    /**
     * 查询广告创意列表
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017-11-23
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amAdcreativeInfo/query.do")
    public Object queryAdcreativeTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("查询广告创意列表");
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
    	String adGroupId = parameter.getParameter("adGroupId");
    	if (StringUtils.isBlank(adGroupId)) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_ID_IS_NULL);
        }
    	String mediumAccountId = parameter.getParameter("mediumAccountId");
    	if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        return creativeService.queryAdcreative(Integer.parseInt(adGroupId), Integer.parseInt(mediumAccountId));
    }
    
    
    /**
     * 添加图片文件
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017-11-29
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amFiles/add.do")
    public Object addImages(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("添加图片文件");
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
    	
    	String mediumAccountId = parameter.getParameter("mediumAccountId");
    	if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
    	String width = parameter.getParameter("width");
    	if (StringUtils.isBlank(width)) {
    		width = "0";
        }
    	String height = parameter.getParameter("height");
    	if (StringUtils.isBlank(height)) {
    		height="0";
        }
    	String file_size = parameter.getParameter("file_size");
    	if (StringUtils.isBlank(file_size)) {
    		file_size = "0";
        }
    	String sizePX = width + "x" + height;
 
    	String temp = request.getSession().getServletContext().getRealPath(File.separator)+ ImageUtil.UPLOAD_FILE_PATH;
    	MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(Integer.parseInt(mediumAccountId));
		int mAccountId = 0;
		String accessToken = "";
		if(tAccountVo != null ){
			mAccountId = tAccountVo.getAccountId();
			accessToken = tAccountVo.getAccessToken();
		}
		AdFiles adFiles = new AdFiles();
		File dir = new File(temp);
        if (!dir.exists()) {
            dir.mkdir();
        }
    	 String fileName = "";
    	 String signature = "";
    	 File local = null;
    	 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
     	 MultipartFile mf = multipartRequest.getFile("adFile");
     	 if(mf != null ){

     		fileName = mf.getOriginalFilename();
            String suffix = fileName.indexOf(".") != -1 ? fileName.substring(
                    fileName.lastIndexOf("."), fileName.length()) : null;
            suffix = (suffix != null ? suffix : "");
            String uuid = UUID.randomUUID().toString().replaceAll("\\-", "");
            fileName = uuid + suffix.toLowerCase();
            if(ImageUtil.isImage(suffix)){   //图片
            	local = new File(temp + fileName);
            	mf.transferTo(local);
            	signature = ImageUtil.getStringMd5(local);
            	
            	BufferedImage bufferedImage = ImageIO.read(local);   
            	int imageWidth = bufferedImage.getWidth();   
            	int imageHeight = bufferedImage.getHeight();
            	String imagePX = imageWidth + "x" + imageHeight;
            	long length = local.length();
            	long maxLength = 3*1024*1024;
            	//小于等于 3M,不超过版位规定的大小 
            	if(!imagePX.equals(sizePX) || (length>Long.parseLong(file_size)) || Long.parseLong(file_size) > maxLength || signature.length() != 32){
            		ExceptionHandler.throwParameterException(StatusCode.COMM_FILE_SIZE_ERROR);
            	}
            	AdImages  imageFile = imageService.add(mAccountId, local, signature, accessToken);
            	adFiles.setFile_id(imageFile.getImage_id());
            	adFiles.setPreview_url(imageFile.getPreview_url());
            	adFiles.setWidth(imageFile.getWidth());
            	adFiles.setHeight(imageFile.getHeight());
            	adFiles.setFile_size(imageFile.getFile_size());
            	local.delete();
            }else if (suffix.equals(".mp4") || suffix.equals(".MP4")) {  //视频
            	
            	local = new File(temp + fileName);
            	mf.transferTo(local);
            	signature = ImageUtil.getStringMd5(local);
            	Integer video_id = adVideoService.add(mAccountId, local, signature, accessToken, null);
            	AdVideos adVideos = adVideoService.get(mAccountId, video_id, accessToken);
            	if(adVideos != null){
            		String imagePX = adVideos.getWidth() + "x" + adVideos.getHeight(); 
            		if(!imagePX.equals(sizePX) || (Long.parseLong(adVideos.getFile_size())>Long.parseLong(file_size))  || signature.length() != 32){
            			ExceptionHandler.throwParameterException(StatusCode.COMM_FILE_SIZE_ERROR);
            		}
            		adFiles.setFile_id(adVideos.getVideo_id());
            		adFiles.setPreview_url(adVideos.getPreview_url());
            		adFiles.setWidth(adVideos.getWidth());
            		adFiles.setHeight(adVideos.getHeight());
            		adFiles.setFile_size(adVideos.getFile_size());
            		local.delete();
            	}
			}else{
            	ExceptionHandler.throwParameterException(StatusCode.COMM_IMAGES_TYPE_ERROR);
            }
     	 }
     	 if(mf == null || local == null){
     		ExceptionHandler.throwParameterException(StatusCode.COMM_IMAGES_NOT_UPLOAD);
     	 }
     	
		
        return adFiles;
    }
    

    
   
}
