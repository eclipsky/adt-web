package com.dataeye.ad.assistor.module.mediumaccount.mapper;

import java.util.List;

import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountQuery;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccount;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountSelectorVo;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountVo;
import com.dataeye.ad.assistor.module.mediumaccount.model.OptimizerQuery;

/**
 * 媒体账户映射器.
 * Created by luzhuyou
 */
@MapperScan
public interface MediumAccountMapper {

    /**
     * 查询媒体账号列表信息
     * @param mediumAccountVo
     * @return
     * @author luzhuyou 2017/02/20
     */
	public List<MediumAccountVo> query(MediumAccountVo mediumAccountVo);

	/**
	 * 查询单个媒体账号信息
	 * @param mediumAccountVo
	 * @return
	 */
	public MediumAccountVo get(MediumAccountVo mediumAccountVo);

	/**
	 * 根据媒体账号ID获取媒体账号信息.
	 * @param mediumAccountQuery
	 * @return
	 */
	MediumAccountVo getMediumAccount(MediumAccountQuery mediumAccountQuery);

	/**
	 * 新增媒体账号
	 * @param account
	 * @return
	 */
	public int add(MediumAccount account);

	/**
	 * 修改媒体账号信息
	 * @param account
	 */
	public void update(MediumAccount account);

	/**
     * 获取媒体账户
     * <tt>产品</tt>
     * <br>如果产品ID为空，则查询该公司下所有产品
     * <br>如果只传一个媒体ID，则返回该公司下该媒体ID对应账户
     * <br>如果媒体ID为包含“,”的多个ID组成的字符串，则返回它们对应的所有账户
     * <tt>媒体</tt>
     * <br>如果媒体ID为空，则查询该公司下所选择的产品对应的所有媒体
     * <br>如果只传一个媒体ID，则返回媒体ID对应账户
     * <br>如果媒体ID为包含“,”的多个ID组成的字符串，则返回它们对应的所有账户
     *
     * @param mediumAccountSelectorVo
     * @return 媒体账户列表
     * @author luzhuyou 2017/02/22
     */
	public List<MediumAccountSelectorVo> queryForSelector(MediumAccountSelectorVo mediumAccountSelectorVo);

	/**
     * 更新媒体账户表对应SystemAccountNameList字段
     * @param mediumAccount
     * @return
     * @author luzhuyou 2017/03/01
     */
	public int updateSystemAccountNameList(MediumAccount mediumAccount);
	
    /**
     * 根据媒体账号和产品获取优化师账号
     *
     * @param optimizerQuery
     * @return
     */
    String getResponsibleAccountsByAccount(OptimizerQuery optimizerQuery);
    
    public List<MediumAccount> getMediumAccountByMediumAndAccount(MediumAccount MediumAccount);
    
    /**
     * 根据媒体账号 获得负责人或关注人帐号字符串,帐号id字符串
     * @param companyId
     * @param accountNames
     * @author ldj 2017-08-28
     */
    public MediumAccount getResponsibleFollowersGroupAccount(MediumAccount MediumAccount);
    
    /**
     * 根据媒体账号 获得负责人或关注人帐号字符串,媒体信息和媒体帐号信息
     * @param companyId
     * @param accountNames
     * @author ldj 2017-09-04
     */
    public List<MediumAccount> getResponsibleFollowersGroupMedium(MediumAccount MediumAccount);
    
    /**
     * 获取所有的媒体账号和负责人关系
     * @author ldj 2017-09-04
     * @return
     */
    List<MediumAccount> listMediumAccountRelation();
    
    /**
     * 查询子账户负责或关注的媒体帐号
     * @author ldj 2017-11-23
     * @return
     */
    List<Integer> getMediumAccountIdByRespOrFollowers(MediumAccount MediumAccount);
    
}
