package com.dataeye.ad.assistor.module.dictionaries.model;

import java.util.Date;

import com.google.gson.annotations.Expose;

/**
 * 投放计划表
 * @author luzhuyou 2017/03/02
 *
 */
public class Plan {
	
	/** 计划ID */
	@Expose
	private Integer planId;
	/** 计划名称 */
	@Expose
	private String planName;
	/** 媒体ID */
	private Integer mediumId;
	/** 媒体账户ID */
	private Integer mediumAccountId;
	/** 公司ID */
	private Integer companyId;
	/** 状态：0-正常，1-删除 */
	private Integer status;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	/** 系统类型：0-Others, 1-iOS, 2-Android */
	private Integer osType;
	/**
	 * 计划开关.
	 */
	private int planSwitch;
	/**
	 * 出价，单位分
	 */
	private int planBid;

	/**
	 *  扩展字段.
	 */
	private String planExtend;
	
	/**
	 * 媒体平台计划ID
	 * */
	private String mPlanId;
	
	
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}

	public int getPlanSwitch() {
		return planSwitch;
	}

	public void setPlanSwitch(int planSwitch) {
		this.planSwitch = planSwitch;
	}

	public int getPlanBid() {
		return planBid;
	}

	public void setPlanBid(int planBid) {
		this.planBid = planBid;
	}

	public String getPlanExtend() {
		return planExtend;
	}

	public void setPlanExtend(String planExtend) {
		this.planExtend = planExtend;
	}
	public String getmPlanId() {
		return mPlanId;
	}
	public void setmPlanId(String mPlanId) {
		this.mPlanId = mPlanId;
	}
	@Override
	public String toString() {
		return "Plan [planId=" + planId + ", planName=" + planName
				+ ", mediumId=" + mediumId + ", mediumAccountId="
				+ mediumAccountId + ", companyId=" + companyId + ", status="
				+ status + ", remark=" + remark + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", osType=" + osType
				+ ", planSwitch=" + planSwitch + ", planBid=" + planBid
				+ ", planExtend=" + planExtend + ", mPlanId=" + mPlanId + "]";
	}
}
