package com.dataeye.ad.assistor.module.report.model;

/**
 * Created by huangzehai on 2017/1/5.
 * 下载表格子查询.
 */
public class SubQuery {
    /**
     * 链接ID.
     */
    private LinkID linkId;

    private String date;
    /**
     * 媒体ID.
     */
    private Integer mediumId;
    /**
     * 活动组ID.
     */
    private Integer groupId;
    /**
     * 账号ID.
     */
    private Long accountId;
    /**
     * 计划ID.
     */
    private Integer planId;

    public LinkID getLinkId() {
        return linkId;
    }

    public void setLinkId(LinkID linkId) {
        this.linkId = linkId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }
}
