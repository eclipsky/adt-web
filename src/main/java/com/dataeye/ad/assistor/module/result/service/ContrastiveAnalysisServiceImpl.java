package com.dataeye.ad.assistor.module.result.service;

import com.dataeye.ad.assistor.common.Item;
import com.dataeye.ad.assistor.module.product.model.Product;
import com.dataeye.ad.assistor.module.result.mapper.ContrastiveAnalysisMapper;
import com.dataeye.ad.assistor.module.result.model.ContrastiveAnalysisQuery;
import com.dataeye.ad.assistor.module.result.model.DailyIndicator;
import com.dataeye.ad.assistor.module.result.model.TrendChart;
import com.dataeye.ad.assistor.module.result.util.IndicatorUtils;
import com.dataeye.ad.assistor.module.result.util.TrendChartUtils;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccount;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/10.
 */
@Service
public class ContrastiveAnalysisServiceImpl implements ContrastiveAnalysisService {

    @Autowired
    private ContrastiveAnalysisMapper contrastiveAnalysisMapper;

    @Override
    public TrendChart<BigDecimal> dailyCostTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery) {
        List<DailyIndicator> indicators = contrastiveAnalysisMapper.dailyCostTrendGroupByOptimizer(contrastiveAnalysisQuery);
        IndicatorUtils.formatIndicators(indicators);
        boolean isDefaultChart = isDefaultChart(contrastiveAnalysisQuery);
        return TrendChartUtils.toTrendChart(contrastiveAnalysisQuery, indicators, false, isDefaultChart);
    }

    @Override
    public TrendChart<BigDecimal> dailyCtrTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery) {
        boolean isDefaultChart = isDefaultChart(contrastiveAnalysisQuery);
        return TrendChartUtils.toTrendChart(contrastiveAnalysisQuery, contrastiveAnalysisMapper.dailyCtrTrendGroupByOptimizer(contrastiveAnalysisQuery), true, isDefaultChart);
    }

    @Override
    public TrendChart<BigDecimal> dailyDownloadRateTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery) {
        boolean isDefaultChart = isDefaultChart(contrastiveAnalysisQuery);
        return TrendChartUtils.toTrendChart(contrastiveAnalysisQuery, contrastiveAnalysisMapper.dailyDownloadRateTrendGroupByOptimizer(contrastiveAnalysisQuery), true, isDefaultChart);
    }

    @Override
    public TrendChart<BigDecimal> dailyCpaTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery) {
        List<DailyIndicator> indicators = contrastiveAnalysisMapper.dailyCpaTrendGroupByOptimizer(contrastiveAnalysisQuery);
        IndicatorUtils.formatIndicators(indicators);
        boolean isDefaultChart = isDefaultChart(contrastiveAnalysisQuery);
        return TrendChartUtils.toTrendChart(contrastiveAnalysisQuery, indicators, false, isDefaultChart);
    }

    /**
     * 按优化师分组统计七日回本率趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailySevenDayPaybackRateTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery) {
        List<DailyIndicator> indicators = contrastiveAnalysisMapper.dailySevenDayPaybackRateTrendGroupByOptimizer(contrastiveAnalysisQuery);
        boolean isDefaultChart = isDefaultChart(contrastiveAnalysisQuery);
        return TrendChartUtils.toTrendChart(contrastiveAnalysisQuery, indicators, true, isDefaultChart);
    }

    /**
     * 列出授权访问的优化师列表.
     * @return
     */
    public List<Item<String,String>> listOptimizers(DataPermissionDomain dataPermissionDomain){
        List<SystemAccount> optimizers =  contrastiveAnalysisMapper.listOptimizers(dataPermissionDomain);
        List<Item<String,String>> items = new ArrayList<>();
        for(SystemAccount o:optimizers){
        	String optimizer = o.getAccountName();
        	String accountAlias = o.getAccountAlias();
            items.add(new Item<>(optimizer,accountAlias));
        }
        return items;
    }

    @Override
    public List<Item<Integer, String>> listProducts(int companyId) {
        List<Product> products = contrastiveAnalysisMapper.listProducts(companyId);
        List<Item<Integer, String>> productList = new ArrayList<>();
        if (products != null) {
            for (Product product : products) {
                productList.add(new Item<>(product.getProductId(), product.getProductName()));
            }
        }
        return productList;
    }

    /**
     * @param contrastiveAnalysisQuery
     * @return
     */
    private boolean isDefaultChart(ContrastiveAnalysisQuery contrastiveAnalysisQuery) {
        return contrastiveAnalysisQuery.getOptimizers() == null || contrastiveAnalysisQuery.getOptimizers().isEmpty();
    }
    
    @Override
    public TrendChart<BigDecimal> dailySevenDayRetentionRateTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery) {
        List<DailyIndicator> indicators = contrastiveAnalysisMapper.dailySevenDayRetentionRateTrendGroupByOptimizer(contrastiveAnalysisQuery);
        boolean isDefaultChart = isDefaultChart(contrastiveAnalysisQuery);
        return TrendChartUtils.toTrendChart(contrastiveAnalysisQuery, indicators, true, isDefaultChart);
    }

}
