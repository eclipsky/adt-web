package com.dataeye.ad.assistor.module.advertisement.mapper;


import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.advertisement.model.AdCreativeRelationVo;

/**
 * 广告投放
 * 广告创意关联关系映射器.
 * Created by ldj
 */
@MapperScan
public interface ApiTxAdCreativeMapper {

	/**
	 * 查询广告创意关联关系
	 * @param vo
	 * @return
	 */
	public List<AdCreativeRelationVo> query(AdCreativeRelationVo vo);

	
	/**
	 * 创建广告创意关联关系
	 * @param relation
	 */
	public void add(AdCreativeRelationVo relation);
	

	/**
	 * 删除广告创意关联关系
	 * @param relation
	 * @return
	 */
	public int delete(AdCreativeRelationVo relation);
	
	

}
