package com.dataeye.ad.assistor.module.report.model;

import com.dataeye.ad.assistor.module.report.constant.Defaults;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2016/12/27.
 */
public class DeliveryReportQuery extends DateRangeQuery implements Cloneable {
    /**
     * 媒体账号ID列表.
     */
    private List<Integer> accountIds;
    /**
     * 计划名称.
     */
    private String query;
    /**
     * 过滤消耗为0的计划.改成  过滤媒体数据为空的计划
     */
    private boolean ignoreZeroCostPlan = false;
    /**
     * 过滤未关联规则计划.
     */
    private boolean ignoreUnboundPlan = false;

    /**
     * 过滤自然流量
     */
    private boolean filterNaturalFlow;
    /**
     * 当前页。
     */
    private int currentPage = Defaults.CURRENT_PAGE;
    /**
     * 分页大小。
     */
    private int pageSize = Defaults.PAGE_SIZE;

    /**
     * 排序字段
     */
    private String orderBy;

    /**
     * 排序方式.升序(asc)和降序(desc)
     */
    private String order;

    /**
     * 计划ID.
     */
    private Integer planId;

    /**
     * 媒体ID.
     */
    private List<Integer> mediumIds;

    /**
     * 产品ID列表.
     */
    private List<Integer> productIds;

    /**
     * ADT系统账号ID
     *
     * @return
     */
    private Integer accountId;

    /**
     * 是否自然流量
     */
    private boolean isNaturalFlow;

    /**
     * 是否不要使用数据库分页.
     */
    private boolean notPaging;

    /**
     * 过滤媒体数据为空的计划，值为true（过滤）或false.
     */
    private boolean ignoreIsNullCostPlan = false;
    
    /**
     * 计划ID列表.
     */
    private List<Integer> planIds;
    
    /**
     * 子账户id
     */
    private List<Integer> systemAccountIds;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public boolean isIgnoreZeroCostPlan() {
        return ignoreZeroCostPlan;
    }

    public void setIgnoreZeroCostPlan(boolean ignoreZeroCostPlan) {
        this.ignoreZeroCostPlan = ignoreZeroCostPlan;
    }

    public boolean isIgnoreUnboundPlan() {
        return ignoreUnboundPlan;
    }

    public void setIgnoreUnboundPlan(boolean ignoreUnboundPlan) {
        this.ignoreUnboundPlan = ignoreUnboundPlan;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public List<Integer> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<Integer> accountIds) {
        this.accountIds = accountIds;
    }

    public List<Integer> getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(List<Integer> mediumIds) {
        this.mediumIds = mediumIds;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public boolean isFilterNaturalFlow() {
        return filterNaturalFlow;
    }

    public void setFilterNaturalFlow(boolean filterNaturalFlow) {
        this.filterNaturalFlow = filterNaturalFlow;
    }

    public boolean isNaturalFlow() {
        return isNaturalFlow;
    }

    public void setNaturalFlow(boolean naturalFlow) {
        isNaturalFlow = naturalFlow;
    }

    public boolean isNotPaging() {
        return notPaging;
    }

    public void setNotPaging(boolean notPaging) {
        this.notPaging = notPaging;
    }

    public boolean isIgnoreIsNullCostPlan() {
        return ignoreIsNullCostPlan;
    }

    public void setIgnoreIsNullCostPlan(boolean ignoreIsNullCostPlan) {
        this.ignoreIsNullCostPlan = ignoreIsNullCostPlan;
    }
    public List<Integer> getPlanIds() {
		return planIds;
	}
	public void setPlanIds(List<Integer> planIds) {
		this.planIds = planIds;
	}
	

	public List<Integer> getSystemAccountIds() {
		return systemAccountIds;
	}

	public void setSystemAccountIds(List<Integer> systemAccountIds) {
		this.systemAccountIds = systemAccountIds;
	}

	/**
     * 计算分页偏移量.
     *
     * @return
     */
    public int getOffset() {
        return (currentPage - 1) * pageSize;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
	public String toString() {
		return "DeliveryReportQuery [accountIds=" + accountIds + ", query="
				+ query + ", ignoreZeroCostPlan=" + ignoreZeroCostPlan
				+ ", ignoreUnboundPlan=" + ignoreUnboundPlan
				+ ", filterNaturalFlow=" + filterNaturalFlow + ", currentPage="
				+ currentPage + ", pageSize=" + pageSize + ", orderBy="
				+ orderBy + ", order=" + order + ", planId=" + planId
				+ ", mediumIds=" + mediumIds + ", productIds=" + productIds
				+ ", accountId=" + accountId + ", isNaturalFlow="
				+ isNaturalFlow + ", notPaging=" + notPaging
				+ ", ignoreIsNullCostPlan=" + ignoreIsNullCostPlan
				+ ", systemAccountIds=" + systemAccountIds
				+ ", planIds=" + planIds + "]";
	}
}
