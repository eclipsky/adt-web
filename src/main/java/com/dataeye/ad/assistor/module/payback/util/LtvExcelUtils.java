package com.dataeye.ad.assistor.module.payback.util;

import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.module.payback.model.LtvDailyReport;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.math.BigDecimal;
import java.util.List;

public final class LtvExcelUtils {
    private static final String SHEET_NAME = "LTV分析";
    private static final String[] DAILY_PAYBACK_REPORT_COLUMN_NAMES = {"日期", "注册账号数", "首日LTV", "2日LTV", "3日LTV", "4日LTV", "5日LTV", "6日LTV", "7日LTV", "15日LTV", "30日LTV", "60日LTV", "90日LTV",
            "首日累计充值", "2日累计充值", "3日累计充值", "4日累计充值", "5日累计充值", "6日累计充值", "7日累计充值", "15日累计充值", "30日累计充值", "60日累计充值", "90日累计充值"};

    private LtvExcelUtils() {

    }

    public static Workbook buildDailyPaybackReport(List<LtvDailyReport> reports) {
        HSSFWorkbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet(SHEET_NAME);
        ExcelUtils.addHead(sheet, DAILY_PAYBACK_REPORT_COLUMN_NAMES);
        Row row;

        int rowIndex = 1;
        for (LtvDailyReport report : reports) {
            row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getDate()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getRegistrations()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv1Day()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv2Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv3Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv4Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv5Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv6Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv7Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv15Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv30Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv60Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getLtv90Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount1Day()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount2Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount3Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount4Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount5Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount6Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount7Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount15Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount30Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount60Days()));
            row.createCell(columnIndex++).setCellValue(value(report.getPayAmount90Days()));
        }
        return wb;
    }

    /**
     * 获取LTV的值
     *
     * @param number
     * @return
     */
    private static String value(BigDecimal number) {
        if (number == null) {
            return Labels.UNKNOWN;
        } else {
            return String.valueOf(number.doubleValue());
        }
    }


}
