package com.dataeye.ad.assistor.module.report.controller;

import com.dataeye.ad.assistor.common.Item;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.report.model.DeliveryContrastiveAnalysisIndicator;
import com.dataeye.ad.assistor.module.report.model.DeliveryTrendQuery;
import com.dataeye.ad.assistor.module.report.model.Period;
import com.dataeye.ad.assistor.module.report.model.PlanQuery;
import com.dataeye.ad.assistor.module.report.service.DeliveryContrastiveAnalysisService;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.StringUtil;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 投放日报活动对比控制器
 * Created by huangzehai on 2017/4/27.
 */
@Controller
public class DeliveryContrastiveAnalysisController {
    @Autowired
    private DeliveryContrastiveAnalysisService deliveryContrastiveAnalysisService;

    @Autowired
    private SystemAccountService systemAccountService;

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/trend/medium-account.do")
    public Object trendByMediumAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }
        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        String mediumIds = parameter.getParameter(DEParameter.Keys.MEDIUM_IDS);
        String mediumAccountIds = parameter.getParameter(DEParameter.Keys.ACCOUNT_IDS);
        String indicator = parameter.getParameter(DEParameter.Keys.INDICATOR);
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        String period = parameter.getParameter(DEParameter.Keys.PERIOD);

        //构建查询对象
        DeliveryTrendQuery query = new DeliveryTrendQuery();
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setMediumAccountIds(StringUtil.stringToList(mediumAccountIds));
        query.setMediumIds(StringUtil.stringToList(mediumIds));
        if (StringUtils.isBlank(period)) {
            //设置默认时段为天
            query.setPeriod(Period.Day);
        } else {
            query.setPeriod(Period.parse(period));
        }

        DeliveryContrastiveAnalysisIndicator contrastiveIndicator = DeliveryContrastiveAnalysisIndicator.parse(indicator);
        if (contrastiveIndicator == null) {
            //设置默认指标
            contrastiveIndicator = DeliveryContrastiveAnalysisIndicator.Cost;
        }

        //检查指标是否授权访问
        validateIndicatorAuthentication(context, contrastiveIndicator);

        resetDateRange(query);

        switch (contrastiveIndicator) {
            case Cost:
                return deliveryContrastiveAnalysisService.costTrendGroupByMediumAccount(query);
            case CTR:
                return deliveryContrastiveAnalysisService.ctrTrendGroupByMediumAccount(query);
            case DownloadRate:
                return deliveryContrastiveAnalysisService.downloadRateTrendGroupByMediumAccount(query);
            case REGISTRATION:
                return deliveryContrastiveAnalysisService.registrationTrendGroupByMediumAccount(query);
            case CPA:
                return deliveryContrastiveAnalysisService.cpaTrendGroupByMediumAccount(query);
            case Recharge:
                return deliveryContrastiveAnalysisService.rechargeTrendGroupByMediumAccount(query);
        }
        return null;

    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/trend/medium.do")
    public Object trendByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }
        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        String indicator = parameter.getParameter(DEParameter.Keys.INDICATOR);
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        String mediumIds = parameter.getParameter(DEParameter.Keys.MEDIUM_IDS);
        String period = parameter.getParameter(DEParameter.Keys.PERIOD);
        DeliveryTrendQuery query = new DeliveryTrendQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setMediumIds(StringUtil.stringToList(mediumIds));
        if (StringUtils.isBlank(period)) {
            //设置默认时段为天
            query.setPeriod(Period.Day);
        } else {
            query.setPeriod(Period.parse(period));
        }

        DeliveryContrastiveAnalysisIndicator contrastiveIndicator = DeliveryContrastiveAnalysisIndicator.parse(indicator);
        if (contrastiveIndicator == null) {
            //设置默认指标
            contrastiveIndicator = DeliveryContrastiveAnalysisIndicator.Cost;
        }

        //检查指标是否授权访问
        validateIndicatorAuthentication(context, contrastiveIndicator);

        resetDateRange(query);

        switch (contrastiveIndicator) {
            case Cost:
                return deliveryContrastiveAnalysisService.costTrendGroupByMedium(query);
            case CTR:
                return deliveryContrastiveAnalysisService.ctrTrendGroupByMedium(query);
            case DownloadRate:
                return deliveryContrastiveAnalysisService.downloadRateTrendGroupByMedium(query);
            case REGISTRATION:
                return deliveryContrastiveAnalysisService.registrationTrendGroupByMedium(query);
            case CPA:
                return deliveryContrastiveAnalysisService.cpaTrendGroupByMedium(query);
            case Recharge:
                return deliveryContrastiveAnalysisService.rechargeTrendGroupByMedium(query);
        }
        return null;

    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/trend/product.do")
    public Object trendByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession == null) {
            return null;
        }
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }
        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        String indicator = parameter.getParameter(DEParameter.Keys.INDICATOR);
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        String period = parameter.getParameter(DEParameter.Keys.PERIOD);
        DeliveryTrendQuery query = new DeliveryTrendQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        query.setProductIds(StringUtil.stringToList(productIds));
        if (StringUtils.isBlank(period)) {
            //设置默认时段为天
            query.setPeriod(Period.Day);
        } else {
            query.setPeriod(Period.parse(period));
        }

        DeliveryContrastiveAnalysisIndicator contrastiveIndicator = DeliveryContrastiveAnalysisIndicator.parse(indicator);
        if (contrastiveIndicator == null) {
            //设置默认指标
            contrastiveIndicator = DeliveryContrastiveAnalysisIndicator.Cost;
        }

        //检查指标是否授权访问
        validateIndicatorAuthentication(context, contrastiveIndicator);

        resetDateRange(query);

        switch (contrastiveIndicator) {
            case Cost:
                return deliveryContrastiveAnalysisService.costTrendGroupByProduct(query);
            case CTR:
                return deliveryContrastiveAnalysisService.ctrTrendGroupByProduct(query);
            case DownloadRate:
                return deliveryContrastiveAnalysisService.downloadRateTrendGroupByProduct(query);
            case REGISTRATION:
                return deliveryContrastiveAnalysisService.registrationTrendGroupByProduct(query);
            case CPA:
                return deliveryContrastiveAnalysisService.cpaTrendGroupByProduct(query);
            case Recharge:
                return deliveryContrastiveAnalysisService.rechargeTrendGroupByProduct(query);
        }
        return null;

    }

    /**
     * 检查指标是否授权访问
     *
     * @param context
     * @param deliveryContrastiveAnalysisIndicator
     */
    private void validateIndicatorAuthentication(DEContext context, DeliveryContrastiveAnalysisIndicator deliveryContrastiveAnalysisIndicator) {
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        SystemAccountVo systemAccount = systemAccountService.get(userInSession.getAccountId());
        //如果指标未授权，抛出异常.
        if (!StringUtils.containsIgnoreCase(systemAccount.getIndicatorPermission(), deliveryContrastiveAnalysisIndicator.getIndicatorId())) {
            ExceptionHandler.throwGeneralServerException(StatusCode.LOGI_NO_PRIVILEGE);
        }
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/trend/plan.do")
    public Object trendByPlan(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (!userInSession.hasManagerOrFollowerPermission()) {
            return null;
        }
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }
        //转化起始截止日期
        //起始日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        //截止日期
        Date endDate = DateUtils.parse(parameter.getEndDate());
        //计划ID
        String planIds = parameter.getParameter(DEParameter.Keys.PLAN_IDS);
        //指标名称
        String indicator = parameter.getParameter(DEParameter.Keys.INDICATOR);
        //产品ID
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        //时段
        String period = parameter.getParameter(DEParameter.Keys.PERIOD);
        //媒体ID
        String mediumIds = parameter.getParameter(DEParameter.Keys.MEDIUM_IDS);
        //媒体账号ID
        String mediumAccountIds = parameter.getParameter(DEParameter.Keys.ACCOUNT_IDS);

        //构建查询对象.
        DeliveryTrendQuery query = new DeliveryTrendQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setPlanIds(StringUtil.stringToList(planIds));
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setMediumIds(StringUtil.stringToList(mediumIds));
        query.setMediumAccountIds(StringUtil.stringToList(mediumAccountIds));
        if (StringUtils.isBlank(period)) {
            //设置默认时段为天
            query.setPeriod(Period.Day);
        } else {
            query.setPeriod(Period.parse(period));
        }

        resetDateRange(query);

        DeliveryContrastiveAnalysisIndicator deliveryContrastiveAnalysisIndicator = DeliveryContrastiveAnalysisIndicator.parse(indicator);
        if (deliveryContrastiveAnalysisIndicator == null) {
            //设置默认指标
            deliveryContrastiveAnalysisIndicator = DeliveryContrastiveAnalysisIndicator.Cost;
        }

        //检查指标是否授权访问
        validateIndicatorAuthentication(context, deliveryContrastiveAnalysisIndicator);

        switch (deliveryContrastiveAnalysisIndicator) {
            case Cost:
                return deliveryContrastiveAnalysisService.costTrendGroupByPlan(query);
            case CTR:
                return deliveryContrastiveAnalysisService.ctrTrendGroupByPlan(query);
            case DownloadRate:
                return deliveryContrastiveAnalysisService.downloadRateTrendGroupByPlan(query);
            case REGISTRATION:
                return deliveryContrastiveAnalysisService.registrationTrendGroupByPlan(query);
            case CPA:
                return deliveryContrastiveAnalysisService.cpaTrendGroupByPlan(query);
            case Recharge:
                return deliveryContrastiveAnalysisService.rechargeTrendGroupByPlan(query);
        }
        return null;
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/plan/list.do")
    public Object listPlans(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (!userInSession.hasManagerOrFollowerPermission()) {
            return null;
        }
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }
        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        PlanQuery planQuery = new PlanQuery();
        planQuery.setCompanyId(userInSession.getCompanyId());
        planQuery.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        planQuery.setStartDate(startDate);
        planQuery.setEndDate(endDate);
        planQuery.setProductIds(StringUtil.stringToList(parameter.getParameter(DEParameter.Keys.PRODUCT_IDS)));
        planQuery.setMediumIds(StringUtil.stringToList(parameter.getParameter(DEParameter.Keys.MEDIUM_IDS)));
        planQuery.setMediumAccountIds(StringUtil.stringToList(parameter.getParameter(DEParameter.Keys.ACCOUNT_IDS)));
        return deliveryContrastiveAnalysisService.listPlans(planQuery);
    }

    /**
     * 投放分析-对比分析-授权指标列表
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/delivery/trend/indicators.do")
    public Object indicators(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        SystemAccountVo systemAccount = systemAccountService.get(userInSession.getAccountId());
        List<Item<String, String>> indicators = new ArrayList<>();
        for (DeliveryContrastiveAnalysisIndicator indicator : DeliveryContrastiveAnalysisIndicator.values()) {
            //仅仅返回授权的指标
            if (StringUtils.containsIgnoreCase(systemAccount.getIndicatorPermission(), indicator.getIndicatorId())) {
                indicators.add(new Item<>(indicator.name(), indicator.getLabel()));
            }
        }
        return indicators;
    }

    /**
     * 重新设置周和月的日期范围
     *
     * @param query
     */
    private void resetDateRange(DeliveryTrendQuery query) {
        Period period = query.getPeriod();
        if (period != null) {
            switch (query.getPeriod()) {
                case Month:
                    query.setStartDate(DateUtils.firstDayOfMonth(query.getStartDate()));
                    query.setEndDate(DateUtils.lastDayOfMonth(query.getEndDate()));
                    break;
                case Week:
                    query.setStartDate(DateUtils.dayOfWeek(query.getStartDate(), Calendar.MONDAY));
                    query.setEndDate(DateUtils.dayOfWeek(query.getEndDate(), Calendar.SUNDAY));
                    break;
            }
        }
    }
}
