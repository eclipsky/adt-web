package com.dataeye.ad.assistor.module.realtimedelivery.model;

import java.util.Date;

/**
 * Created by huangzehai on 2017/3/9.
 */
public class Time {
    /**
     * 统计时间
     */
    private Date time;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Time{" +
                "time=" + time +
                '}';
    }
}
