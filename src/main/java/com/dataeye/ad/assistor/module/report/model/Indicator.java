package com.dataeye.ad.assistor.module.report.model;

import com.google.gson.annotations.Expose;

/**
 * 指标
 * Created by huangzehai on 2017/6/8.
 */
public class Indicator {
    /**
     * 指标ID.
     */
    @Expose
    private String indicatorId;
    /**
     * 指标键名.
     */
    @Expose
    private String indicatorKey;
    /**
     * 指标名称.
     */
    @Expose
    private String indicatorName;

    /**
     * 指标状态.
     */
    @Expose
    private int indicatorVisible;

    public String getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(String indicatorId) {
        this.indicatorId = indicatorId;
    }

    public String getIndicatorKey() {
        return indicatorKey;
    }

    public void setIndicatorKey(String indicatorKey) {
        this.indicatorKey = indicatorKey;
    }

    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public int getIndicatorVisible() {
        return indicatorVisible;
    }

    public void setIndicatorVisible(int indicatorVisible) {
        this.indicatorVisible = indicatorVisible;
    }

    @Override
    public String toString() {
        return "Indicator{" +
                "indicatorId='" + indicatorId + '\'' +
                ", indicatorKey='" + indicatorKey + '\'' +
                ", indicatorName='" + indicatorName + '\'' +
                ", indicatorVisible=" + indicatorVisible +
                '}';
    }
}
