package com.dataeye.ad.assistor.module.systemaccount.model;

import com.google.gson.annotations.Expose;

/**
 * 分组系统账号信息
 * @author luzhuyou 2017/02/17
 */
public class GroupDetailVo {
	/** 账号名称（邮箱，登录账号） */
	@Expose
	private String accountName;
	/** 账号别名 */
	@Expose
	private String accountAlias;
	/** 分组ID */
	@Expose
	private Integer groupId;
	/** 分组名称 */
	@Expose
	private String groupName;
	/** 账户角色:0-普通系统账户，1-组长，2-企业账户 */
	@Expose
	private Integer accountRole;
	/** 账号ID */
	@Expose
	private Integer accountId;
	/** 公司ID */
	private Integer companyId;
	/** 组长账号 */
	private String leaderAccount;
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountAlias() {
		return accountAlias;
	}
	public void setAccountAlias(String accountAlias) {
		this.accountAlias = accountAlias;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Integer getAccountRole() {
		return accountRole;
	}
	public void setAccountRole(Integer accountRole) {
		this.accountRole = accountRole;
	}
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getLeaderAccount() {
		return leaderAccount;
	}
	public void setLeaderAccount(String leaderAccount) {
		this.leaderAccount = leaderAccount;
	}
	@Override
	public String toString() {
		return "GroupDetailVo [accountName=" + accountName + ", accountAlias="
				+ accountAlias + ", groupId=" + groupId + ", groupName="
				+ groupName + ", accountRole=" + accountRole + ", accountId="
				+ accountId + ", companyId=" + companyId + ", leaderAccount="
				+ leaderAccount + "]";
	}
	
}
