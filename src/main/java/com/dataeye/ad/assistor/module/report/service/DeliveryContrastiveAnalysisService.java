package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.common.TrendChart;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import com.dataeye.ad.assistor.module.report.model.DeliveryTrendQuery;
import com.dataeye.ad.assistor.module.report.model.PlanQuery;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/27.
 */
public interface DeliveryContrastiveAnalysisService {

    /**
     * 消耗趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, BigDecimal> costTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, Double> ctrTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, Double> downloadRateTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, Integer> registrationTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, BigDecimal> cpaTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, BigDecimal> rechargeTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 消耗日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, BigDecimal> costTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, Double> ctrTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, Double> downloadRateTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, Integer> registrationTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, BigDecimal> cpaTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    TrendChart<String, BigDecimal> rechargeTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);


    /**
     * 获取计划列表.
     *
     * @param planQuery
     * @return
     */
    List<Plan> listPlans(PlanQuery planQuery);

    /**
     * 消耗日趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    TrendChart<String, BigDecimal> costTrendGroupByMedium(DeliveryTrendQuery query);

    /**
     * CTR日趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    TrendChart<String, Double> ctrTrendGroupByMedium(DeliveryTrendQuery query);

    /**
     * 下载率日趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    TrendChart<String, Double> downloadRateTrendGroupByMedium(DeliveryTrendQuery query);

    /**
     * 注册数日趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    TrendChart<String, Integer> registrationTrendGroupByMedium(DeliveryTrendQuery query);

    /**
     * CPA日趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    TrendChart<String, BigDecimal> cpaTrendGroupByMedium(DeliveryTrendQuery query);

    /**
     * 总充值日趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    TrendChart<String, BigDecimal> rechargeTrendGroupByMedium(DeliveryTrendQuery query);

    TrendChart<String, BigDecimal> costTrendGroupByProduct(DeliveryTrendQuery query);

    TrendChart<String, Double> ctrTrendGroupByProduct(DeliveryTrendQuery query);

    TrendChart<String, Double> downloadRateTrendGroupByProduct(DeliveryTrendQuery query);
    TrendChart<String, Integer> registrationTrendGroupByProduct(DeliveryTrendQuery query);

    TrendChart<String, BigDecimal> cpaTrendGroupByProduct(DeliveryTrendQuery query);

    TrendChart<String, BigDecimal> rechargeTrendGroupByProduct(DeliveryTrendQuery query);
}
