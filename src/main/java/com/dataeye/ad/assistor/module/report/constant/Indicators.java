package com.dataeye.ad.assistor.module.report.constant;

import java.util.Arrays;
import java.util.List;

/**
 * 指标ID与菜单的指标集.
 * Created by huangzehai on 2017/6/9.
 */
public final class Indicators {
    private Indicators() {

    }

    public static final String COST = "1-1";
    public static final String CPC = "1-10";
    public static final String PLAN_SWITCH = "1-2";
    public static final String PLAN_BID = "1-3";
    public static final String SHOW_NUM = "1-4";
    public static final String CLICK_NUM = "1-5";
    public static final String TOTAL_CLICK_NUM = "1-6";
    //    public static final String DAILY_UNIQUE_CLICK_NUM = "1-7";
    public static final String CTR = "1-8";
    //    public static final String CPM = "1-9";
    public static final String REACH = "2-1";
    public static final String REACH_RATE = "2-2";
    public static final String DOWNLOAD_TIMES = "2-3";
    public static final String DOWNLOAD_RATE = "2-4";
    public static final String ACTIVE_NUM = "3-1";
    public static final String ACTIVE_RATE = "3-2";
    public static final String ACTIVE_CPA = "3-3";
    public static final String REGISTRATIONS_FD = "3-9";
    public static final String REGISTER_ACCOUNT_NUM_FD = "3-10";
    public static final String REGISTER_NUM = "3-4";
    public static final String REGISTER_RATE = "3-5";
    public static final String REGISTER_CPA = "3-6";
    public static final String REGISTER_ACCOUNT_NUM_CPA = "3-11";
    public static final String ACTIVATION_REGISTRATION_RATE = "3-7";
    public static final String RETAIN_D2 = "4-1";
    public static final String RETAIN_D3 = "4-2";
    public static final String RETAIN_D7 = "4-3";
    public static final String RETAIN_D15 = "4-5";
    public static final String RETAIN_D30 = "4-4";
    public static final String NEW_ACCOUNT_PAY_NUM = "5-1";
    public static final String NEW_ACCOUNT_PAY_AMOUNT = "5-2";
    public static final String TOTAL_PAY_AMOUNT = "5-4";
    public static final String TOTAL_PAY_NUM = "5-5";
    public static final String CUMULATIVE_AMOUNT = "5-6";
    public static final String PAY_RATE = "5-7";
    public static final String NEWLY_INCREASED_PAY_RATE = "5-8";
    public static final String PAYBACK_RATE_1DAY = "8-1";
    public static final String LTV_1DAY = "7-2";
    public static final String LTV_2DAYS = "7-3";
    public static final String LTV_3DAYS = "7-4";
    public static final String LTV_4DAYS = "7-5";
    public static final String LTV_5DAYS = "7-6";
    public static final String LTV_6DAYS = "7-7";
    public static final String LTV_7DAYS = "7-8";
    public static final String LTV_15DAYS = "7-9";
    public static final String LTV_30DAYS = "7-10";
    public static final String LTV_60DAYS = "7-11";
    public static final String LTV_90DAYS = "7-12";

    public static final String DAU = "6-1";
    public static final String REGISTER_ACCOUNT_NUM = "3-8";
    public static final String PAYBACK_RATE_2DAY = "8-2";
    public static final String PAYBACK_RATE_3DAY = "8-3";
    public static final String PAYBACK_RATE_4DAY = "8-4";
    public static final String PAYBACK_RATE_5DAY = "8-5";
    public static final String PAYBACK_RATE_6DAY = "8-6";
    public static final String PAYBACK_RATE_7DAY = "8-7";
    public static final String PAYBACK_RATE_15DAY = "8-8";
    public static final String PAYBACK_RATE_30DAY = "8-9";
    public static final String PAYBACK_RATE_60DAY = "8-10";
    public static final String PAYBACK_RATE_90DAY = "8-11";
    public static final String ARPU = "5-9";
    public static final String ARPPU = "5-10";
    

    /**
     * 投放日报指标
     */
    public static final List<String> DAILY_DELIVERY_REPORT_INDICATORS = Arrays.asList(COST, SHOW_NUM, CLICK_NUM, TOTAL_CLICK_NUM, CTR, CPC, REACH, REACH_RATE, DOWNLOAD_TIMES, DOWNLOAD_RATE, ACTIVE_NUM, ACTIVE_RATE, ACTIVE_CPA, REGISTRATIONS_FD, /*REGISTER_ACCOUNT_NUM_FD,*/ REGISTER_NUM, REGISTER_ACCOUNT_NUM, REGISTER_RATE, REGISTER_CPA, /*REGISTER_ACCOUNT_NUM_CPA,*/ RETAIN_D2, RETAIN_D3, RETAIN_D7, RETAIN_D15, RETAIN_D30, NEW_ACCOUNT_PAY_NUM, NEW_ACCOUNT_PAY_AMOUNT, PAYBACK_RATE_1DAY, TOTAL_PAY_AMOUNT, TOTAL_PAY_NUM, CUMULATIVE_AMOUNT, PAY_RATE, DAU, ACTIVATION_REGISTRATION_RATE, NEWLY_INCREASED_PAY_RATE, LTV_1DAY, LTV_7DAYS, LTV_30DAYS);


    /**
     * ltv分析指标
     */
    public static final List<String> DAILY_LTV_ANALYSIS_INDICATORS = Arrays.asList(REGISTER_ACCOUNT_NUM, LTV_1DAY, LTV_2DAYS, LTV_3DAYS, LTV_4DAYS, LTV_5DAYS, LTV_6DAYS, LTV_7DAYS, LTV_15DAYS, LTV_30DAYS,LTV_60DAYS,LTV_90DAYS);

    /**
     * 实时操盘指标(代理)
     */
    public static final List<String> PROXY_REAL_TIME_OPERATION_INDICATORS = Arrays.asList(COST, SHOW_NUM, CLICK_NUM, TOTAL_CLICK_NUM, CTR, REACH, REACH_RATE, DOWNLOAD_TIMES, DOWNLOAD_RATE, ACTIVE_NUM, ACTIVE_RATE, ACTIVE_CPA, REGISTER_NUM, REGISTER_ACCOUNT_NUM, REGISTER_RATE, REGISTER_CPA, CPC);

    /**
     * 实时操盘指标
     */
    public static final List<String> REAL_TIME_OPERATION_INDICATORS = Arrays.asList(PLAN_SWITCH, PLAN_BID, COST, SHOW_NUM, CLICK_NUM, TOTAL_CLICK_NUM, CTR, REACH, REACH_RATE, DOWNLOAD_TIMES, DOWNLOAD_RATE, ACTIVE_NUM, ACTIVE_RATE, ACTIVE_CPA, REGISTER_NUM, REGISTER_ACCOUNT_NUM, REGISTER_RATE, REGISTER_CPA, CPC);

    /**
     * 实时操盘涨跌指标.
     */
    public static final List<String> UPS_AND_DOWNS_INDICATORS = Arrays.asList(COST, CTR, DOWNLOAD_RATE, ACTIVE_CPA, REGISTER_CPA);

    /**
     * 企业账号授权指标
     */
    public static final List<String> BUSINESS_ACCOUNT_INDICATORS = Arrays.asList(COST, CPC, PLAN_SWITCH, PLAN_BID, SHOW_NUM, CLICK_NUM, TOTAL_CLICK_NUM, CTR, REACH, REACH_RATE, DOWNLOAD_TIMES, DOWNLOAD_RATE, ACTIVE_NUM, ACTIVE_RATE, ACTIVE_CPA, REGISTRATIONS_FD, /*REGISTER_ACCOUNT_NUM_FD,*/ REGISTER_NUM, REGISTER_ACCOUNT_NUM, REGISTER_RATE, REGISTER_CPA, ACTIVATION_REGISTRATION_RATE, /*REGISTER_ACCOUNT_NUM_CPA,*/ RETAIN_D2, RETAIN_D3, RETAIN_D7, RETAIN_D15, RETAIN_D30, NEW_ACCOUNT_PAY_NUM, NEW_ACCOUNT_PAY_AMOUNT, PAYBACK_RATE_1DAY, TOTAL_PAY_AMOUNT, TOTAL_PAY_NUM, CUMULATIVE_AMOUNT, PAY_RATE, NEWLY_INCREASED_PAY_RATE, LTV_1DAY, LTV_2DAYS, LTV_3DAYS, LTV_4DAYS, LTV_5DAYS, LTV_6DAYS, LTV_7DAYS, LTV_15DAYS, LTV_30DAYS, LTV_60DAYS, LTV_90DAYS, DAU);

    /**
     * 回本日报指标
     */
    public static final List<String> PAYBACK_REPORT_INDICATORS = Arrays.asList(COST,REGISTER_ACCOUNT_NUM,REGISTER_CPA, TOTAL_PAY_NUM, TOTAL_PAY_AMOUNT,CUMULATIVE_AMOUNT, PAY_RATE, ARPU, ARPPU, PAYBACK_RATE_1DAY, PAYBACK_RATE_2DAY, PAYBACK_RATE_3DAY, PAYBACK_RATE_4DAY,PAYBACK_RATE_5DAY,PAYBACK_RATE_6DAY,PAYBACK_RATE_7DAY,PAYBACK_RATE_15DAY,PAYBACK_RATE_30DAY,PAYBACK_RATE_60DAY,PAYBACK_RATE_90DAY);

}
