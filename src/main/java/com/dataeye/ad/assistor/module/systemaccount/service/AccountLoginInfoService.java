package com.dataeye.ad.assistor.module.systemaccount.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.systemaccount.mapper.AccountLoginInfoMapper;
import com.dataeye.ad.assistor.module.systemaccount.model.AccountLoginInfo;

/**
 * 系统账号Service
 * @author luzhuyou 2017/02/16
 *
 */
@Service
public class AccountLoginInfoService {

	@Autowired
	private AccountLoginInfoMapper accountLoginInfoMapper;
	
	/**
	 * 新增系统账号登录信息
	 * @param companyId
	 * @param accountId
	 * @param loginIp
	 * @param statusCode
	 * @param remark
	 * @return
	 * @author luzhuyou 2017/02/17
	 */
	public int add(int companyId, int accountId, String loginIp, int statusCode, String remark) {
		AccountLoginInfo accountLoginInfo = new AccountLoginInfo();
		accountLoginInfo.setCompanyId(companyId);
		accountLoginInfo.setAccountId(accountId);
		accountLoginInfo.setLoginIp(loginIp);
		accountLoginInfo.setStatusCode(statusCode);
		accountLoginInfo.setRemark(remark);
		accountLoginInfo.setCreateTime(new Date());
		return accountLoginInfoMapper.add(accountLoginInfo);
	}

}
