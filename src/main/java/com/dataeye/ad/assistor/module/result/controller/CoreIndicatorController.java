package com.dataeye.ad.assistor.module.result.controller;

import java.text.ParseException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.result.model.CoreIndicatorQuery;
import com.dataeye.ad.assistor.module.result.service.CoreIndicatorService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;

/**
 * 效果看板核心指标Controller.
 * Created by huangzehai on 2017/4/6.
 */
@Controller
public class CoreIndicatorController {

    @Autowired
    private CoreIndicatorService coreIndicatorService;

    /**
     * 获取查询参数并构建查询对象
     *
     * @param request
     * @return
     * @throws ParseException
     */
    private CoreIndicatorQuery getCoreIndicatorQuery(HttpServletRequest request) throws ParseException {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());

        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }

        //是否过滤自然流量
        String filterNaturalFlow = parameter.getParameter(DEParameter.Keys.FILTER_NATURAL_FLOW);

        //转化起始截止日期
        Date startDate = DateUtils.defaultIfBlank(parameter.getStartDate(), DateUtils.ONE_WEEK_AGO);
        Date endDate = DateUtils.defaultIfBlank(parameter.getEndDate(), DateUtils.TODAY);
        CoreIndicatorQuery coreIndicatorQuery = new CoreIndicatorQuery();
        coreIndicatorQuery.setCompanyId(userInSession.getCompanyId());
        coreIndicatorQuery.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        coreIndicatorQuery.setPermissionSystemAccounts(userInSession.getPermissionSystemAccounts());
        coreIndicatorQuery.setStartDate(startDate);
        coreIndicatorQuery.setEndDate(endDate);
        coreIndicatorQuery.setFilterNaturalFlow(Boolean.valueOf(filterNaturalFlow));
        return coreIndicatorQuery;
    }

    /**
     * 核心指标总体状况
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/overall.do")
    public Object overall(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getOverallCoreIndicator(coreIndicatorQuery);
    }

    /**
     * 消耗占比-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cost/medium.do")
    public Object costGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getCostGroupByMedium(coreIndicatorQuery);
    }

    /**
     * 充值占比-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/recharge/medium.do")
    public Object rechargeGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getRechargeGroupByMedium(coreIndicatorQuery);
    }

    /**
     * 回本率-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/paybackrate/medium.do")
    public Object paybackRateGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getPaybackRateGroupByMedium(coreIndicatorQuery);
    }


    /**
     * 余额-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/balance/medium.do")
    public Object balanceGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getBalanceGroupByMedium(coreIndicatorQuery);
    }

    /**
     * 汇总CPA-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cpa/medium.do")
    public Object cpaGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getCpaGroupByMedium(coreIndicatorQuery);
    }

    /**
     * DAU-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/dau/medium.do")
    public Object dauGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getDauGroupByMedium(coreIndicatorQuery);
    }

    /**
     * 消耗占比-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cost/product.do")
    public Object costGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getCostGroupByProduct(coreIndicatorQuery);
    }

    /**
     * 充值占比-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/recharge/product.do")
    public Object rechargeGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getRechargeGroupByProduct(coreIndicatorQuery);
    }

    /**
     * 回本率-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/paybackrate/product.do")
    public Object paybackRateGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getPaybackRateGroupByProduct(coreIndicatorQuery);
    }

    /**
     * 余额-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/balance/product.do")
    public Object balanceGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getBalanceGroupByProduct(coreIndicatorQuery);
    }

    /**
     * 汇总CPA-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cpa/product.do")
    public Object cpaGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getCpaGroupByProduct(coreIndicatorQuery);
    }

    /**
     * DAU-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/dau/product.do")
    public Object dauGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getDauGroupByProduct(coreIndicatorQuery);
    }

    /**
     * 消耗占比-分系统
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cost/os.do")
    public Object costGroupByOs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getCostGroupByOs(coreIndicatorQuery);
    }

    /**
     * 充值占比-分系统
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/recharge/os.do")
    public Object rechargeGroupByOs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getRechargeGroupByOs(coreIndicatorQuery);
    }

    /**
     * 回本率-分平台
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/paybackrate/os.do")
    public Object paybackRateGroupByOs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getPaybackRateGroupByOs(coreIndicatorQuery);
    }

    /**
     * 汇总CPA-分平台
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cpa/os.do")
    public Object cpaGroupByOs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getCpaGroupByOs(coreIndicatorQuery);
    }

    /**
     * DAU-分平台
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/dau/os.do")
    public Object dauGroupByOs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.getDauGroupByOs(coreIndicatorQuery);
    }

    /**
     * 消耗日趋势图-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cost/trend/medium.do")
    public Object costTrendGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyCostTrendGroupByMedium(coreIndicatorQuery);
    }

    /**
     * 充值日趋势图-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/recharge/trend/medium.do")
    public Object rechargeTrendGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyRechargeTrendGroupByMedium(coreIndicatorQuery);
    }

    /**
     * 余额日趋势图-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/balance/trend/medium.do")
    public Object balanceTrendGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyBalanceTrendGroupByMedium(coreIndicatorQuery);
    }


    /**
     * CPA日趋势图-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cpa/trend/medium.do")
    public Object cpaTrendGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyCpaTrendGroupByMedium(coreIndicatorQuery);
    }

    /**
     * DAU日趋势图-分媒体
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/dau/trend/medium.do")
    public Object dauTrendGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyDauTrendGroupByMedium(coreIndicatorQuery);
    }

    /**
     * 消耗日趋势图-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cost/trend/product.do")
    public Object costTrendGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyCostTrendGroupByProduct(coreIndicatorQuery);
    }

    /**
     * 充值日趋势图-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/recharge/trend/product.do")
    public Object rechargeTrendGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyRechargeTrendGroupByProduct(coreIndicatorQuery);
    }

    /**
     * 余额日趋势图-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/balance/trend/product.do")
    public Object balanceTrendGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyBalanceTrendGroupByProduct(coreIndicatorQuery);
    }


    /**
     * CPA日趋势图-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cpa/trend/product.do")
    public Object cpaTrendGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyCpaTrendGroupByProduct(coreIndicatorQuery);
    }

    /**
     * DAU日趋势图-分产品
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/dau/trend/product.do")
    public Object dauTrendGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyDauTrendGroupByProduct(coreIndicatorQuery);
    }

    /**
     * 消耗日趋势图-分平台
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cost/trend/os.do")
    public Object costTrendGroupByOs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyCostTrendGroupByOs(coreIndicatorQuery);
    }

    /**
     * 充值日趋势图-分平台
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/recharge/trend/os.do")
    public Object rechargeTrendGroupByOs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyRechargeTrendGroupByOs(coreIndicatorQuery);
    }


    /**
     * CPA日趋势图-分平台
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/cpa/trend/os.do")
    public Object cpaTrendGroupByOs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyCpaTrendGroupByOs(coreIndicatorQuery);
    }

    /**
     * DAU日趋势图-分平台
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/dau/trend/os.do")
    public Object dauTrendGroupByOs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CoreIndicatorQuery coreIndicatorQuery = getCoreIndicatorQuery(request);
        if (coreIndicatorQuery == null) {
            return null;
        }
        return coreIndicatorService.dailyDauTrendGroupByOs(coreIndicatorQuery);
    }

}
