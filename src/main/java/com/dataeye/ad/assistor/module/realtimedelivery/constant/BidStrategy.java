package com.dataeye.ad.assistor.module.realtimedelivery.constant;

import java.util.HashMap;
import java.util.Map;

import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;

/**
 * Created by huangzeahi on 2017/5/17.
 */
public class BidStrategy {
    public static final String OCPC = "OCPC";
    public static final String CPC = "CPC";
    public static final String CPM = "CPM";
    public static final String CPV = "CPV";
    public static final String CPA = "CPA";
    public static final String OCPM = "OCPM";

    private static final Map<Integer, Map<String, String>> STRATEGIES = new HashMap<>();

    static {
        //智汇推
        Map<String, String> tui = new HashMap<>();
        tui.put("PRICE_MODE_CPC", CPC);
        tui.put("PRICE_MODE_CPM", CPM);
        //百度信息流推广
        Map<String, String> baiduFeedAds = new HashMap<>();
        baiduFeedAds.put("1", CPC);
        baiduFeedAds.put("3", OCPC);
        //广点通
        Map<String, String> tencent = new HashMap<>();
        tencent.put("1", CPC);
        tencent.put("4", CPM);
        //新浪超级粉丝通
        Map<String, String> adWeiboNew = new HashMap<>();
        adWeiboNew.put("1", OCPM);
        adWeiboNew.put("4", CPM);
        //UC头条
        Map<String, String> uc = new HashMap<>();
        uc.put("1", CPC);
        uc.put("2", CPM);
        STRATEGIES.put(Medium.Tui, tui);
        STRATEGIES.put(Medium.BaiduFeedAds, baiduFeedAds);
        STRATEGIES.put(Medium.Tencent, tencent);
        STRATEGIES.put(Medium.AdWeiboNew, adWeiboNew);
        STRATEGIES.put(Medium.Uc, uc);
    }

    public static String getStandardBidStrategy(Integer mediumId, String bidStrategy) {
        if (STRATEGIES.containsKey(mediumId) && STRATEGIES.get(mediumId).containsKey(bidStrategy)) {
            return STRATEGIES.get(mediumId).get(bidStrategy);
        } else {
            return bidStrategy;
        }
    }
    
    public static String getBidPhase(Integer mediumId, String phase) {
        if (mediumId != null && phase != null && mediumId == Medium.BaiduFeedAds && phase.contains("ocpcLevel")) {
        	JsonObject jsonObject = StringUtil.jsonParser.parse(phase).getAsJsonObject();
            return jsonObject.get("ocpcLevel").getAsInt()+"";
        } else {
            return phase;
        }
    }

}
