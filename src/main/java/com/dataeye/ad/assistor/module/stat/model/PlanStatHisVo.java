package com.dataeye.ad.assistor.module.stat.model;

import java.util.Date;

/**
 * 历史计划关联关系VO
 * @author luzhuyou 2017/08/03
 *
 */
public class PlanStatHisVo {
	
	/**
	 * 统计日期
	 */
	private String statDate;
	/**
	 * 计划ID
	 */
	private int planId;
	/**
	 * 计划名称
	 */
	private String planName;
	/**
	 * 公司ID
	 */
	private int companyId;
	/**
	 * 落地页ID
	 */
	private int pageId;
	/**
	 * 落地页名称
	 */
	private String pageName;
	/**
	 * 包ID
	 */
	private int pkgId;
	/**
	 * 包名称
	 */
	private String pkgName;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/** 
	 * 系统类型：0-Others, 1-iOS, 2-Android 
	 */
	private Integer osType;
	
	public String getStatDate() {
		return statDate;
	}
	public void setStatDate(String statDate) {
		this.statDate = statDate;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getPageId() {
		return pageId;
	}
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public int getPkgId() {
		return pkgId;
	}
	public void setPkgId(int pkgId) {
		this.pkgId = pkgId;
	}
	public String getPkgName() {
		return pkgName;
	}
	public void setPkgName(String pkgName) {
		this.pkgName = pkgName;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	@Override
	public String toString() {
		return "PlanStatHisVo [statDate=" + statDate + ", planId=" + planId
				+ ", planName=" + planName + ", companyId=" + companyId
				+ ", pageId=" + pageId + ", pageName=" + pageName + ", pkgId="
				+ pkgId + ", pkgName=" + pkgName + ", updateTime=" + updateTime
				+ ", osType=" + osType + "]";
	}
	
}
