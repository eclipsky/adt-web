package com.dataeye.ad.assistor.module.realtimedelivery.controller;

import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.product.service.ProductService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by huangzehai on 2017/7/6.
 */
@Controller
public class NaturalFlowController {

    @Autowired
    private ProductService productService;

    /**
     * 自然流量
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/real-time/natural-flow.do")
    public Object bid(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession == null) {
            ExceptionHandler.throwParameterException(StatusCode.LOGI_RE_LOGIN);
        }
        return productService.getNatureRealTimeData(userInSession.getCompanyId(), userInSession.getCompanyAccountUid());
    }
}
