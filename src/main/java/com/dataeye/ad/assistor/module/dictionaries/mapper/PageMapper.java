package com.dataeye.ad.assistor.module.dictionaries.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.dictionaries.model.Page;

/**
 * 落地页表映射器.
 * Created by luzhuyou 2017/03/02
 */
@MapperScan
public interface PageMapper {

	/**
	 * 查询落地页列表
	 * @param page
	 * @return
	 */
	public List<Page> query(Page page);

}
