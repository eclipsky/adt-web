package com.dataeye.ad.assistor.module.result.service;

import com.dataeye.ad.assistor.common.Item;
import com.dataeye.ad.assistor.module.product.model.Product;
import com.dataeye.ad.assistor.module.result.model.ContrastiveAnalysisQuery;
import com.dataeye.ad.assistor.module.result.model.TrendChart;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/10.
 */
public interface ContrastiveAnalysisService {
    /**
     * 按优化师分组统计日消耗趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    TrendChart<BigDecimal> dailyCostTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);

    /**
     * 按优化师分组统计日CTR趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    TrendChart<BigDecimal> dailyCtrTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);


    /**
     * 按优化师分组统计日下载率趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    TrendChart<BigDecimal> dailyDownloadRateTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);


    /**
     * 按优化师分组统计日CPA趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    TrendChart<BigDecimal> dailyCpaTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);

    /**
     * 按优化师分组统计七日回本率趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    TrendChart<BigDecimal> dailySevenDayPaybackRateTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);

    /**
     * 获取关联表中的优化师列表.
     * @param dataPermissionDomain
     * @return
     */
    List<Item<String,String>> listOptimizers(DataPermissionDomain dataPermissionDomain);

    /**
     *  获取关联表中的产品列表.
     * @param companyId
     * @return
     */
    List<Item<Integer,String>> listProducts(int companyId);
    
    /**
     * 按优化师分组统计七日回本率趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    TrendChart<BigDecimal> dailySevenDayRetentionRateTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);
}

