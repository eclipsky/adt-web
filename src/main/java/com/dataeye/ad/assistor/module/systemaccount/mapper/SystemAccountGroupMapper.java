package com.dataeye.ad.assistor.module.systemaccount.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.systemaccount.model.GroupBasicVo;
import com.dataeye.ad.assistor.module.systemaccount.model.GroupDetailVo;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountGroup;

/**
 * 系统账户分组映射器.
 * Created by luzhuyou
 */
@MapperScan
public interface SystemAccountGroupMapper {

	/**
	 * 查询基本分组信息列表
	 * @param companyId
	 * @return
	 */
    public List<GroupBasicVo> queryBasicGroup(int companyId);

    /**
     * 查询分组系统账户列表
     * @param group
     * @return
     */
	public List<GroupDetailVo> query(SystemAccountGroup group);

	/**
	 * 根据名称或ID查询单条分组系统账号
	 * @param group
	 * @return
	 */
	public SystemAccountGroup get(SystemAccountGroup group);

	/**
	 * 创建新分组
	 * @param group
	 * @return
	 */
	public int add(SystemAccountGroup group);

	/**
	 * 修改分组
	 * @param group
	 * @return
	 */
	public int update(SystemAccountGroup group);

	/**
	 * 根据分组ID查询相同组长账号的记录
	 * @param paramGroup
	 */
	public List<SystemAccountGroup> querySameLeaderAccountByGroupId(SystemAccountGroup paramGroup);
	
	/**
	 * 根据组长账号查询相同组长账号的记录
	 * @param paramGroup
	 */
	public List<SystemAccountGroup> querySameLeaderAccountByLeaderAccount(SystemAccountGroup paramGroup);

	/**
	 * 删除分组信息
	 * @param groupId
	 * @return
	 */
	public int delete(int groupId);

	/**
	 * 重置组长账号为空
	 * @param paramGroup
	 * @return
	 */
	public int updateLeaderAccount(SystemAccountGroup paramGroup);

}
