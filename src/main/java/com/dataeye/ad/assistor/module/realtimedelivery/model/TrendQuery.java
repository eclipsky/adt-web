package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

/**
 * Created by huangzehai on 2017/2/22.
 */
public class TrendQuery extends DataPermissionDomain {
    /**
     * 计划ID.
     */
    private int planId;
    /**
     * 时段类型.
     */
    private Period period;

    /**
     * 落地页ID，SQL性能优化使用.
     */
    private long pageId;

    /**
     * 包ID，SQL性能优化使用.
     */
    private long packageId;

    /**
     * 右边指标.
     */
    private TrendIndicator[] indicators;

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public TrendIndicator[] getIndicators() {
        return indicators;
    }

    public void setIndicators(TrendIndicator[] indicators) {
        this.indicators = indicators;
    }

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public long getPackageId() {
        return packageId;
    }

    public void setPackageId(long packageId) {
        this.packageId = packageId;
    }
}
