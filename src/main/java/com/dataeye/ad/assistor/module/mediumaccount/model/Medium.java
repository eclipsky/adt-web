package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.Date;

import com.google.gson.annotations.Expose;

/**
 * 媒体
 * @author luzhuyou 2017/02/22
 *
 */
public class Medium {
	/** 媒体ID */
	@Expose
	private Integer mediumId;
	/** 媒体名称 */
	@Expose
	private String mediumName;
	/** 媒体主页 */
	private String mediumHomepage;
	/** 备注 */
	private String remark;
	/** 状态 */
	private Integer status;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	/** 产品ID数组 */
	private String[] productIds;
	/** 公司ID */
	private Integer companyId;
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	public String getMediumHomepage() {
		return mediumHomepage;
	}
	public void setMediumHomepage(String mediumHomepage) {
		this.mediumHomepage = mediumHomepage;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String[] getProductIds() {
		return productIds;
	}
	public void setProductIds(String[] productIds) {
		this.productIds = productIds;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	@Override
	public String toString() {
		return "Medium [mediumId=" + mediumId + ", mediumName=" + mediumName
				+ ", mediumHomepage=" + mediumHomepage + ", remark=" + remark
				+ ", status=" + status + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", companyId=" + companyId
				+ "]";
	}
	
}
