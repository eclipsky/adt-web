package com.dataeye.ad.assistor.module.advertisement.model;

import com.dataeye.ad.assistor.module.advertisement.model.tencent.TargetingBean;
import com.google.gson.annotations.Expose;

/**
 * @author lingliqi
 * @date 2017-12-04 11:22
 */
public class TargetingVO {

    @Expose
    private Integer targetingId;

    @Expose
    private String targetingName;

    @Expose
    private TargetingBean targeting;

    @Expose
    private String description;
    
    //0-否；1-是
    @Expose
    private Integer isPublic;

    public Integer getTargetingId() {
        return targetingId;
    }

    public void setTargetingId(Integer targetingId) {
        this.targetingId = targetingId;
    }

    public String getTargetingName() {
        return targetingName;
    }

    public void setTargetingName(String targetingName) {
        this.targetingName = targetingName;
    }

    public TargetingBean getTargeting() {
        return targeting;
    }

    public void setTargeting(TargetingBean targeting) {
        this.targeting = targeting;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
	public Integer getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}

	public TargetingVO() {
    }

    public TargetingVO(String targetingName, TargetingBean targeting, String description) {
        this.targetingName = targetingName;
        this.targeting = targeting;
        this.description = description;
    }

    public TargetingVO(Integer targetingId, String targetingName, TargetingBean targeting, String description) {
        this.targetingId = targetingId;
        this.targetingName = targetingName;
        this.targeting = targeting;
        this.description = description;
    }

    @Override
	public String toString() {
		return "TargetingVO [targetingId=" + targetingId + ", targetingName="
				+ targetingName + ", targeting=" + targeting + ", description="
				+ description + ", isPublic=" + isPublic + "]";
	}
}
