package com.dataeye.ad.assistor.module.mediumaccount.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.common.Item;
import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.mediumaccount.constants.Constants;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountSelectorVo;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountVo;
import com.dataeye.ad.assistor.module.mediumaccount.model.OptimizerQuery;
import com.dataeye.ad.assistor.module.mediumaccount.model.Optimizers;
import com.dataeye.ad.assistor.module.mediumaccount.service.MediumAccountService;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.github.pagehelper.PageHelper;

/**
 * Created by luzhuyou
 * 媒体账号控制器.
 */
@Controller
public class MediumAccountController {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(MediumAccountController.class);

    @Autowired
    private MediumAccountService mediumAccountService;
    @Autowired
    private SystemAccountService systemAccountService;

    /**
     * 查询媒体账号列表信息
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/20
     * @update ldj 2017-08-23
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/mediumaccount/query.do")
    public Object queryByCompanyAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();

       /* String productId = parameter.getParameter("productId");
        if (StringUtils.isBlank(productId) || !StringUtils.isNumeric(productId)) {
            productId = null;
        }*/
        String mediumId = parameter.getParameter("mediumId");
        if (StringUtils.isBlank(mediumId) || !StringUtils.isNumeric(mediumId)) {
            mediumId = null;
        }
        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();

        String mediumIds = parameter.getParameter("mediumIds");//媒体
        String mediumAccountIds = parameter.getParameter("mediumAccountIds");//媒体账号
        String account = parameter.getParameter("account");//负责人/关注人
        if(StringUtils.isBlank(mediumIds)) {
        	mediumIds = null;
		}
        if (StringUtils.isBlank(mediumAccountIds)) {
        	mediumAccountIds = null;
        }
        if (StringUtils.isBlank(account)) {
        	account = null;
        } else {
        	account = account.trim();
        }
        
        List<MediumAccountVo> list = mediumAccountService.query(companyId,
                mediumId == null ? null : Integer.parseInt(mediumId), null, mediumIds, mediumAccountIds, account);
        ////循环媒体帐号列表，查找负责的系统帐号别名和关注的系统帐号别名
        List<MediumAccountVo> result = mediumAccountService.queryMediumAccountAndAlias(list);
        PageData pageData = PageDataHelper.getPageDataByList(parameter.getPageSize(), parameter.getPageId(), result);
        return pageData;
    }

    /**
     * 基于系统账号查询媒体账号列表
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/20
     *  update ldj by 2018-01-08,修改内容：新增一列 ‘媒体账号日限额’，目前仅新增广点通的账号显示且支持编辑，历史广点通账号和其他媒体账号显示 ‘-’，
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/mediumaccount/queryBySystemAccount.do")
    public Object queryBySystemAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();

       /* String productId = parameter.getParameter("productId");
        if (StringUtils.isBlank(productId) || !StringUtils.isNumeric(productId)) {
            productId = null;
        }*/
        String mediumId = parameter.getParameter("mediumId");
        if (StringUtils.isBlank(mediumId) || !StringUtils.isNumeric(mediumId)) {
            mediumId = null;
        }
        String mediumIds = parameter.getParameter("mediumIds");//媒体
        String mediumAccountIds = parameter.getParameter("mediumAccountIds");//媒体账号
        if(StringUtils.isBlank(mediumIds)) {
        	mediumIds = null;
		}
        if (StringUtils.isBlank(mediumAccountIds)) {
        	mediumAccountIds = null;
        }
        UserInSession userInSession = (UserInSession) SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();

        
        // 如果为null，则表示无数据访问权限，否则，有权限
//        if (!userInSession.hasManagerPermission()) {
//            return null;
//        }

        PageHelper.startPage(parameter.getPageId(), parameter.getPageSize());
        List<MediumAccountVo> result = mediumAccountService.query(companyId,
                mediumId == null ? null : Integer.parseInt(mediumId), userInSession.getOptimizerPermissionMediumAccountIds(), mediumIds, mediumAccountIds, null);
        PageData pageData = PageDataHelper.getPageData((com.github.pagehelper.Page)result);
        return pageData;
    }

    /**
     * 新增媒体账号
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/20
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/mediumaccount/add.do")
    public Object add(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("新增媒体账号");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String productIds = parameter.getParameter("productIds");
        String mediumId = parameter.getParameter("mediumId");
        String mediumAccount = parameter.getParameter("mediumAccount");
        String mediumAccountAlias = parameter.getParameter("mediumAccountAlias");
        String password = parameter.getParameter("password");
        String discountRate = parameter.getParameter("discountRate");
        String followerAccounts = parameter.getParameter("followerAccounts");
        String responsibleAccounts = parameter.getParameter("responsibleAccounts");
        String status = parameter.getParameter("status");
        String pageType = parameter.getParameter("pageType");
        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();

        if (StringUtils.isBlank(mediumId) || !StringUtils.isNumeric(mediumId)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        if (StringUtils.isBlank(mediumAccount)) {
            logger.error("媒体账号不允许为空");
            ExceptionHandler.throwParameterException(StatusCode.MEAC_MEDIUM_ACCOUNT_IS_NULL);
        }
        if (StringUtils.isBlank(password)) {
            logger.error("密码不允许为空");
            ExceptionHandler.throwParameterException(StatusCode.MEAC_PASSWORD_IS_NULL);
        }

        if (StringUtils.isBlank(mediumAccountAlias)) {
            mediumAccountAlias = null;
        } else {
            mediumAccountAlias = mediumAccountAlias.trim();
        }

        if (StringUtils.isBlank(discountRate)) {
            discountRate = Constants.DEFALUT_MEDIUM_DISCOUNT_RATE + "";
        }
        if (Double.parseDouble(discountRate.trim()) > 300 || Double.parseDouble(discountRate.trim()) < 0) {
            logger.error("媒体折扣率只能够在0和300之间，保留两位小数.");
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_RATE_RANGE_ERROR);
        }

        if (StringUtils.isBlank(status) || !StringUtils.isNumeric(status)) {
            status = Constants.MediumAccountStatus.NORMAL + "";
        }
        
        if (StringUtils.isBlank(pageType) || !StringUtils.isNumeric(pageType)) {
        	pageType = "1";
        }

        // 密码加密
      //update by 2017-06-20 通过调用加密接口加密
        password = mediumAccountService.getEncryptPassword(password.trim());

        // 检查媒体账号是否存在
        MediumAccountVo account = mediumAccountService.get(companyId, Integer.parseInt(mediumId), mediumAccount.trim());
        if (account != null && account.getMediumId() == Integer.parseInt(mediumId)) {
            logger.error("媒体账号已存在.");
            ExceptionHandler.throwParameterException(StatusCode.PROD_MEDIUM_ACCOUNT_EXISTS);
        }

        // 新增媒体账号信息
        int result = mediumAccountService.add(companyId, productIds, Integer.parseInt(mediumId), mediumAccount.trim(), mediumAccountAlias, password, Double.parseDouble(discountRate) / 100, followerAccounts, Integer.parseInt(status), responsibleAccounts,Integer.parseInt(pageType));
        
        // 刷新UserInSession
        SessionContainer.refreshUserInSession(request);
        
        return result;
    }

    /**
     * 修改媒体账号
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/20
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/mediumaccount/modify.do")
    public Object modify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("修改媒体账号");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        String productIds = parameter.getParameter("productIds");
        String mediumId = parameter.getParameter("mediumId");
        String mediumAccount = parameter.getParameter("mediumAccount");
        String mediumAccountAlias = parameter.getParameter("mediumAccountAlias");
        String password = parameter.getParameter("password");
        String discountRate = parameter.getParameter("discountRate");
        String followerAccounts = parameter.getParameter("followerAccounts");
        String status = parameter.getParameter("status");
        String responsibleAccounts = parameter.getParameter("responsibleAccounts");
        String dailyBudget = parameter.getParameter("dailyBudget");
        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
        int accountRole = SessionContainer.getCurrentUser(context.getSession()).getAccountRole();

        if (StringUtils.isBlank(mediumAccountId) || !StringUtils.isNumeric(mediumAccountId)
                || StringUtils.isBlank(mediumId) || !StringUtils.isNumeric(mediumId)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        if (StringUtils.isBlank(mediumAccount)) {
            logger.error("媒体账号不允许为空");
            ExceptionHandler.throwParameterException(StatusCode.MEAC_MEDIUM_ACCOUNT_IS_NULL);
        }

        if (StringUtils.isBlank(mediumAccountAlias)) {
            mediumAccountAlias = "";
        }

        if (StringUtils.isBlank(discountRate)) {
            discountRate = Constants.DEFALUT_MEDIUM_DISCOUNT_RATE + "";
        }
        if (Double.parseDouble(discountRate.trim()) > 300 || Double.parseDouble(discountRate.trim()) < 0) {
            logger.error("媒体折扣率只能够在0和300之间，保留两位小数.");
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_RATE_RANGE_ERROR);
        }

        if (StringUtils.isBlank(status) || !StringUtils.isNumeric(status)) {
            status = Constants.MediumAccountStatus.NORMAL + "";
        }

        // 如果传入密码不为空，则为密码修改，进行密码加密
        if (StringUtils.isNotBlank(password)) {
          //update by 2017-06-20 通过调用加密接口加密
            password = mediumAccountService.getEncryptPassword(password.trim());
        } else {
            password = null;
        }

        // 检查媒体账号是否存在
        MediumAccountVo account = mediumAccountService.get(companyId, Integer.parseInt(mediumId), mediumAccount);
        if (account != null && account.getMediumAccountId() != Integer.parseInt(mediumAccountId.trim())) {
            logger.error("媒体账号已存在.");
            ExceptionHandler.throwParameterException(StatusCode.PROD_MEDIUM_ACCOUNT_EXISTS);
        }

        Integer dailyBudgetParam = 0;
        if (!StringUtils.isBlank(dailyBudget) && NumberUtils.isNumber(dailyBudget) && (CurrencyUtils.yuanToFen(new BigDecimal(dailyBudget)).intValue() >= 50 && CurrencyUtils.yuanToFen(new BigDecimal(dailyBudget)).intValue() <= 400000000)) {
        	dailyBudgetParam = CurrencyUtils.yuanToFen(new BigDecimal(dailyBudget)).intValue();
        }
        // 修改媒体账号信息
        int result = mediumAccountService.modify(companyId, Integer.parseInt(mediumAccountId.trim()), productIds, Integer.parseInt(mediumId), mediumAccount, mediumAccountAlias, password, Double.parseDouble(discountRate) / 100, followerAccounts, Integer.parseInt(status), responsibleAccounts,accountRole,dailyBudgetParam);
        
        // 刷新UserInSession
        SessionContainer.refreshUserInSession(request);
        
        return result;
    }

    /**
     * 下拉框获取媒体账号
     * <br><tt>产品</tt>
     * <br>如果产品ID为空，则查询该公司下所有产品
     * <br>如果只传一个媒体ID，则返回该公司下该媒体ID对应账户
     * <br>如果媒体ID为包含“,”的多个ID组成的字符串，则返回它们对应的所有账户
     * <tt>媒体</tt>
     * <br>如果媒体ID为空，则查询该公司下所选择的产品对应的所有媒体
     * <br>如果只传一个媒体ID，则返回媒体ID对应账户
     * <br>如果媒体ID为包含“,”的多个ID组成的字符串，则返回它们对应的所有账户
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/22
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/mediumaccount/queryMediumAccountForSelector.do")
    public Object queryForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String productIds = parameter.getParameter("productIds");
        String mediumIds = parameter.getParameter("mediumIds");
        if (StringUtils.isBlank(productIds)) {
            productIds = null;
        }
        if (StringUtils.isBlank(mediumIds)) {
            mediumIds = null;
        }
        Integer loginStatus = null;
        if (StringUtils.isNotBlank(parameter.getParameter("loginStatus"))) {
        	loginStatus = Integer.parseInt(parameter.getParameter("loginStatus"));
        }

        UserInSession userInSession = (UserInSession) SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();

        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
//        if (permissionMediumAccountIds == null) {
//            return null;
//        }
        List<MediumAccountSelectorVo> result = mediumAccountService.queryForSelector(companyId, productIds, mediumIds, permissionMediumAccountIds,loginStatus);
        return result;
    }
    
    /**
     * 下拉框获取优化师（负责人）媒体账号
     * <br><tt>产品</tt>
     * <br>如果产品ID为空，则查询该公司下所有产品
     * <br>如果只传一个媒体ID，则返回该公司下该媒体ID对应账户
     * <br>如果媒体ID为包含“,”的多个ID组成的字符串，则返回它们对应的所有账户
     * <tt>媒体</tt>
     * <br>如果媒体ID为空，则查询该公司下所选择的产品对应的所有媒体
     * <br>如果只传一个媒体ID，则返回媒体ID对应账户
     * <br>如果媒体ID为包含“,”的多个ID组成的字符串，则返回它们对应的所有账户
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/08/17
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/mediumaccount/queryMediumAccountForOptimizerSelector.do")
    public Object queryForOptimizerSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String productIds = parameter.getParameter("productIds");
        String mediumIds = parameter.getParameter("mediumIds");
        Integer loginStatus = null;
        if (StringUtils.isBlank(productIds)) {
            productIds = null;
        }
        if (StringUtils.isBlank(mediumIds)) {
            mediumIds = null;
        }
        if (StringUtils.isNotBlank(parameter.getParameter("loginStatus"))) {
        	loginStatus = Integer.parseInt(parameter.getParameter("loginStatus"));
        }

        UserInSession userInSession = (UserInSession) SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();

        
        
        // 如果为null，则表示无数据访问权限，否则，有权限
//        if (!userInSession.hasManagerPermission()) {
//            return null;
//        }
        List<MediumAccountSelectorVo> result = mediumAccountService.queryForSelector(companyId, productIds, mediumIds, userInSession.getOptimizerPermissionMediumAccountIds(),loginStatus);
        return result;
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/mediumaccount/optimizers.do")
    public Object getOptimizersByAccountAndProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute("CTX");
        DEParameter parameter = context.getDeParameter();
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession == null) {
            ExceptionHandler.throwParameterException(StatusCode.LOGI_RE_LOGIN);
        }
        int companyId = userInSession.getCompanyId();
        String mediumAccountId = parameter.getParameter(DEParameter.Keys.MEDIUM_ACCOUNT_ID);
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        String productId = parameter.getParameter(DEParameter.Keys.PRODUCT_ID);
        if (StringUtils.isBlank(productId)) {
            //ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_PRODUCT_ID_IS_NULL);
        	productId = null;
        }
        OptimizerQuery optimizerQuery = new OptimizerQuery();
        optimizerQuery.setCompanyId(companyId);
        optimizerQuery.setMediumAccountId(Integer.valueOf(mediumAccountId));
        optimizerQuery.setProductId(Integer.valueOf(productId));
        List<Item<String, String>> items = mediumAccountService.getResponsibleAccountsByAccount(optimizerQuery);
        Optimizers optimizers = new Optimizers();
        optimizers.setOptimizers(items);
        String currentAccountName = userInSession.getAccountName();
        int accountRole = userInSession.getAccountRole();
        //如果当期账号是普通账户或者组长, 设置默认优化师ADT账号
        if ((accountRole == 0 || accountRole == 1) && items != null && !items.isEmpty()) {
            for (Item<String, String> item : items) {
                if (StringUtils.equalsIgnoreCase(item.getValue(), currentAccountName)) {
                    optimizers.setDefaultOptimizer(new Item<>(currentAccountName, currentAccountName));
                    break;
                }
            }
        }
        return optimizers;
    }
    
    /**
     * 下拉框查询媒体帐号列表信息（包含媒体类型、媒体名称、媒体帐号列表、关注人或负责人）
     * @create ldj 2017-08-25
     * **/
    @PrivilegeControl(scope = PrivilegeControl.Scope.Public, write = false)
    @RequestMapping("/ad/mediumaccount/queryMediumAccountGroupForSelector.do")
    public Object queryMediumAccountGroupForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute("CTX");
        DEParameter parameter = context.getDeParameter();
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession == null) {
            ExceptionHandler.throwParameterException(StatusCode.LOGI_RE_LOGIN);
        }
        int companyId = userInSession.getCompanyId();
        String accountName = parameter.getParameter("accountName");
        if (StringUtils.isBlank(accountName)) {
        	accountName = null;
        }
        return mediumAccountService.MediumAccountGroup(companyId, accountName);
    }

}
