package com.dataeye.ad.assistor.module.report.constant;

/**
 * 菜单ID常量
 * Created by huangzehai on 2017/6/19.
 */
public final class MenuIds {

    private MenuIds() {

    }

    /**
     * 投放日报菜单ID.
     */
    public static final String DAILY_DELIVERY_REPORT_MENU_ID = "4-1";
    /**
     * 实时操盘菜单ID
     */
    public static final String REAL_TIME_OPERATION_REPORT_MENU_ID = "3-1";


    /**
     * LTV分析菜单ID
     */
    public static final String LTV_ANALYSIS_MENU_ID = "5-3";
    
    /**
     * 回本日报菜单ID
     */
    public static final String PAYBACK_REPORT_MENU_ID = "5-1";

}
