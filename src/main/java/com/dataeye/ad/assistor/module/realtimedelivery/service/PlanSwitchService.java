package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.model.DeliveryStatus;

/**
 * Created by huangzehai on 2017/5/15.
 */
public interface PlanSwitchService {
    /**
     * 修改投放状态
     *
     * @param status
     */
    Result updateStatus(DeliveryStatus status);
}
