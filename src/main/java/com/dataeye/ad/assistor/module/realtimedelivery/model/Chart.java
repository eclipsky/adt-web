package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by huangzehai on 2017/2/22.
 */
public class Chart<V> {
    /**
     * 标题.
     */
    @Expose
    private String title;
    /**
     * 纵轴坐标.
     */
    @Expose
    private List<V> values;

    /**
     * 是否是百分比.
     */
    @Expose
    private boolean isPercent;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<V> getValues() {
        return values;
    }

    public void setValues(List<V> values) {
        this.values = values;
    }

    public boolean isPercent() {
        return isPercent;
    }

    public void setPercent(boolean percent) {
        isPercent = percent;
    }

    @Override
    public String toString() {
        return "Chart{" +
                "title='" + title + '\'' +
                ", values=" + values +
                ", isPercent=" + isPercent +
                '}';
    }
}
