package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by huangzehai on 2017/2/22.
 */
public class RealTimeTrendChart<L, R> {
    /**
     * 时间列表（X轴）.
     */
    @Expose
    private List<String> times;

    /**
     * 消耗列表.(纵轴)
     */
    @Expose
    private Chart<L> totalCosts;

    /**
     * 右边指标（纵轴）
     */
    @Expose
    private List<Chart<R>> indicators;

    public List<String> getTimes() {
        return times;
    }

    public void setTimes(List<String> times) {
        this.times = times;
    }

    public Chart<L> getTotalCosts() {
        return totalCosts;
    }

    public void setTotalCosts(Chart<L> totalCosts) {
        this.totalCosts = totalCosts;
    }

    public List<Chart<R>> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Chart<R>> indicators) {
        this.indicators = indicators;
    }

    @Override
    public String toString() {
        return "RealTimeTrendChart{" +
                "times=" + times +
                ", totalCosts=" + totalCosts +
                ", indicators=" + indicators +
                '}';
    }
}
