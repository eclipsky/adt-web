package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.model.DeliveryStatus;
import com.dataeye.ad.assistor.util.CookieUtils;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by huangzehai on 2017/5/16.
 */
@Service("toutiaoPlanSwitchService")
public class ToutiaoPlanSwitchService extends AbstractMediumPlanService<DeliveryStatus> implements PlanSwitchService {

    private static final String URL = "https://ad.toutiao.com/overture/ad/update_status/";

    private static final String CSRF_TOKEN = "csrftoken";

    /**
     * 成功消息。
     */
    private static final String SUCCESS = "切换计划开关成功";
    /**
     * 失败消息.
     */
    private static final String FAIL = "切换计划开关失败";

    /**
     * 成功状态.
     */
    private static final String STATUS_SUCCES = "success";

    @Override
    public Result updateStatus(DeliveryStatus status) {
        return execute(status);
    }

    @Override
    protected String url(String cookie, Long mediumPlanId) {
        return URL;
    }

    @Override
    protected List<NameValuePair> parameters(String cookie, DeliveryStatus status) {
        List<NameValuePair> params = new ArrayList<>();
        Gson gson = new Gson();
        List<Long> mediumPlanIds = new ArrayList<>();
        mediumPlanIds.add(status.getMediumPlanId());
        //计划ID
        params.add(new BasicNameValuePair("ad_list", gson.toJson(mediumPlanIds)));
        //操作
        params.add(new BasicNameValuePair("operation", StringUtils.lowerCase(status.getStatus().name())));
        return params;
    }

    @Override
    protected void requestHeader(HttpRequestBase request, String cookie) {
        request.setHeader("X-CSRFToken", CookieUtils.get(cookie, CSRF_TOKEN));
        request.setHeader(HOST, "ad.toutiao.com");
        request.setHeader(ORIGIN, "https://ad.toutiao.com");
        request.setHeader(REFERER, "https://ad.toutiao.com/overture/data/campaign/ad/?campaign_id=");
    }

    @Override
    protected Result response(String content) {
        Gson gson = new Gson();
        Response response = gson.fromJson(content, Response.class);
        Result result = new Result();
        //Code为0是操作成功.
        if (StringUtils.equalsIgnoreCase(STATUS_SUCCES, response.getStatus())) {
            result.setSuccess(true);
            result.setMessage(SUCCESS);
        } else {
            result.setSuccess(false);
            result.setMessage(FAIL);
        }
        return result;
    }

    /**
     * 响应
     */
    private class Response {
        private String status;
        private Object data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }
    }
}
