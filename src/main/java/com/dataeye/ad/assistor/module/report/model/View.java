package com.dataeye.ad.assistor.module.report.model;

import org.apache.commons.lang.StringUtils;

/**
 * 显示方式（汇总数据、按天显示数据）,上传的值分别为:total,daily.
 * Created by huangzehai on 2017/1/4.
 */
public enum View {
    /**
     * 期间汇总
     */
    Total,
    /**
     * 按天汇总
     */
    Daily;

    public static View parse(String text) {
        for (View view : View.values()) {
            if (StringUtils.equalsIgnoreCase(view.name(), text)) {
                return view;
            }
        }
        return null;
    }
}
