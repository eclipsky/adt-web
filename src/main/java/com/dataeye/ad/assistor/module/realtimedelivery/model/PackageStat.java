package com.dataeye.ad.assistor.module.realtimedelivery.model;

/**
 * Created by huangzehai on 2017/3/2.
 */
public class
PackageStat extends Time{
    /**
     * 包ID.
     */
    private Integer packageId;
    /**
     * 激活数.
     */
    private Integer activations;
    /**
     * 首日注册设备数
     */
    private Integer firstDayRegistrations;
    /**
     * 首日注册账号数
     */
    private Integer firstDayRegisterAccountNum;

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getActivations() {
        return activations;
    }

    public void setActivations(Integer activations) {
        this.activations = activations;
    }

    public Integer getFirstDayRegistrations() {
        return firstDayRegistrations;
    }

    public void setFirstDayRegistrations(Integer firstDayRegistrations) {
        this.firstDayRegistrations = firstDayRegistrations;
    }

    public Integer getFirstDayRegisterAccountNum() {
        return firstDayRegisterAccountNum;
    }

    public void setFirstDayRegisterAccountNum(Integer firstDayRegisterAccountNum) {
        this.firstDayRegisterAccountNum = firstDayRegisterAccountNum;
    }

    @Override
    public String toString() {
        return "PackageStat{" +
                "packageId=" + packageId +
                ", activations=" + activations +
                ", firstDayRegistrations=" + firstDayRegistrations +
                ", firstDayRegisterAccountNum=" + firstDayRegisterAccountNum +
                '}';
    }
}
