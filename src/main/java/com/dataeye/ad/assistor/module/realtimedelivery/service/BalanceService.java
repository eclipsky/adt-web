package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.common.TrendChart;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.module.mediumaccount.model.Medium;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountSelectorVo;
import com.dataeye.ad.assistor.module.realtimedelivery.model.*;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/19.
 */
public interface BalanceService {
    /**
     * 列出账户余额
     */
    List<Balance> listAccountBalance(BalanceQuery balanceQuery);

    /**
     * 账户余额趋势图
     */
    TrendChart<String, BigDecimal> balanceTrend(BalanceTrendQuery balanceTrendQuery);

    /**
     * 获取负责人管理的媒体账户列表
     *
     * @param mediumAccountQuery
     * @return
     */
    List<MediumAccountSelectorVo> listManagingMediumAccounts(MediumAccountQuery mediumAccountQuery);

    /**
     * 获取负责人管理的媒体列表
     * @param mediumQuery
     * @return
     */
    List<Medium> listManagingMediums(UserInSession userInSession);
}
