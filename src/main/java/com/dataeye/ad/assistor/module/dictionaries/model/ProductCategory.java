package com.dataeye.ad.assistor.module.dictionaries.model;

import com.google.gson.annotations.Expose;

/**
 * 产品分类
 * 
 * */
public class ProductCategory {
	/**分类ID*/
	@Expose
	private Integer categoryId;
	
	/**分类名称*/
	@Expose
	private String categoryName;
	
	/**父级id*/
	@Expose
	private Integer parentId;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	@Override
	public String toString() {
		return "ProductCategory [categoryId=" + categoryId + ", categoryName="
				+ categoryName + ", parentId=" + parentId + "]";
	}

	

	
	
	
	

}
