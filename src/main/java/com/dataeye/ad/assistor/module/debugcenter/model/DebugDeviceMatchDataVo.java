package com.dataeye.ad.assistor.module.debugcenter.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lingliqi
 * @date 2018-01-18 10:49
 */
public class DebugDeviceMatchDataVo {
    @Expose
    public List<KeyValue<String, String>> deviceInfo = new ArrayList<KeyValue<String, String>>();

    @Expose
    public List<KeyValue<String, String>> matchInfo = new ArrayList<KeyValue<String, String>>();

    public List<KeyValue<String, String>> getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(List<KeyValue<String, String>> deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public List<KeyValue<String, String>> getMatchInfo() {
        return matchInfo;
    }

    public void setMatchInfo(List<KeyValue<String, String>> matchInfo) {
        this.matchInfo = matchInfo;
    }
}
