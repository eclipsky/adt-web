package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2017/3/7.
 */
public class RealTimeReportQuery extends DataPermissionDomain {

    /**
     * 起始时间
     */
    private Date startTime;

    /**
     * 落地页ID列表.
     */
    private List<Integer> pageIds;

    /**
     * 包ID列表
     */
    private List<Integer> packageIds;
    

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public List<Integer> getPageIds() {
        return pageIds;
    }

    public void setPageIds(List<Integer> pageIds) {
        this.pageIds = pageIds;
    }

    public List<Integer> getPackageIds() {
        return packageIds;
    }

    public void setPackageIds(List<Integer> packageIds) {
        this.packageIds = packageIds;
    }
   

    @Override
    public String toString() {
        return "RealTimeReportQuery{" +
                "startTime=" + startTime +
                ", pageIds=" + pageIds +
                ", packageIds=" + packageIds +
                "} " + super.toString();
    }
}
