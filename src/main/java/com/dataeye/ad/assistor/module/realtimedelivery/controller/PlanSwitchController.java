package com.dataeye.ad.assistor.module.realtimedelivery.controller;

import static com.dataeye.ad.assistor.module.realtimedelivery.constant.Constants.ENABLE_BID_SERVICE;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.model.DeliveryStatus;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationStatus;
import com.dataeye.ad.assistor.module.realtimedelivery.service.PlanSwitchService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;

/**
 * 投放状态控制器.
 * Created by huangzehai on 2017/5/15.
 */
@Controller
public class PlanSwitchController {

    private Logger logger = LoggerFactory.getLogger(PlanSwitchController.class);

    private static final String PLAN_SWITCH_SUCCESS_MESSAGE = "切换计划开关成功!";

    @Autowired
    @Qualifier("planSwitchService")
    private PlanSwitchService planSwitchService;

    /**
     * 出价.
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/realtimedelivery/plan-switch.do")
    public Object updatePlanSwitch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        DEParameter parameter = context.getDeParameter();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
//        if (!userInSession.hasManagerPermission()) {
//            return null;
//        }
        String planSwitch = parameter.getParameter(DEParameter.Keys.PLAN_SWITCH);
        int mediumId = parameter.getMediumId();
        String mediumAccountId = parameter.getParameter(DEParameter.Keys.MEDIUM_ACCOUNT_ID);
        String mediumPlanId = parameter.getParameter(DEParameter.Keys.MEDIUM_PLAN_ID);
        String planId = parameter.getParameter(DEParameter.Keys.PLAN_ID);
        OperationStatus status = OperationStatus.parse(planSwitch);

        //校验参数
        if (StringUtils.isBlank(planSwitch) || status == null) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_PLAN_SWITCH_MISSING);
        }

        if (mediumId <= 0) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_MEDIUM_ID_MISSING);
        }

        if (StringUtils.isBlank(mediumAccountId) || !NumberUtils.isDigits(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }

        if (StringUtils.isBlank(mediumPlanId) || !NumberUtils.isDigits(mediumPlanId)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_MEDIUM_PLAN_ID_MISSING);
        }

        if (StringUtils.isBlank(planId) || !NumberUtils.isDigits(planId)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_PLAN_ID_MISSING);
        }

        //检查用户是否拥有切换计划投放开关的权限
        List<Integer> managedMediumAccountIds = Arrays.asList(userInSession.getOptimizerPermissionMediumAccountIds());
        if (!managedMediumAccountIds.contains(Integer.valueOf(mediumAccountId))) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_NO_PRIVILEGE_OF_PLAN_SWITCH);
        }

        DeliveryStatus deliStatus = new DeliveryStatus();
        deliStatus.setStatus(status);
        deliStatus.setMediumId(mediumId);
        deliStatus.setMediumAccountId(Integer.valueOf(mediumAccountId));
        deliStatus.setMediumPlanId(Long.valueOf(mediumPlanId));
        deliStatus.setPlanId(Integer.valueOf(planId));
        deliStatus.setCompanyId(userInSession.getCompanyId());
        deliStatus.setAccountId(userInSession.getAccountId());

        String enableBidService = ConfigHandler.getProperty(ENABLE_BID_SERVICE, "false");
        if (Boolean.valueOf(enableBidService)) {
            //启用出价服务时
            logger.info("Update plan switch: {}", planSwitch);
            return planSwitchService.updateStatus(deliStatus);
        } else {
            //禁用出价服务时
            logger.info("Not update plan switch as bid service has been disabled.");
            Result result = new Result();
            result.setSuccess(true);
            result.setMessage(PLAN_SWITCH_SUCCESS_MESSAGE);
            return result;
        }
    }
}
