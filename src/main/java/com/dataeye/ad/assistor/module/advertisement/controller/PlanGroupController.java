package com.dataeye.ad.assistor.module.advertisement.controller;


import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ProductType;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.SpeedMode;
import com.dataeye.ad.assistor.module.advertisement.model.AdGroupQueryVo;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupCreatePreVO;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupVO;
import com.dataeye.ad.assistor.module.advertisement.service.ApiTxCampaignService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.StringUtil;


/**
 * Created by ldj
 * 广告投放
 * 推广计划控制器.
 */
@Controller
public class PlanGroupController {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(PlanGroupController.class);

    @Autowired
    private ApiTxCampaignService apiTxCampaignService;

    /**
     * 查询推广计划列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017/11/18
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amPlanGroup/query.do")
    public Object query(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("查询投放业务推广计划列表");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        AdGroupQueryVo vo = parseQuery(context);

        // 如果为null，则表示无数据访问权限，否则，有权限
        if (null == vo.getPermissionMediumAccountIds()) {
            return null;
        }
        DEParameter parameter = context.getDeParameter();

        List<PlanGroupVO> result = apiTxCampaignService.queryAdPlanGroup(vo);
        PageData pageData = PageDataHelper.getPageDataByList(parameter.getPageSize(), parameter.getPageId(), result);
        return pageData;
    }


    /**
     * 创建推广计划列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017/11/22
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amPlanGroup/add.do")
    public Object add(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("创建投放业务推广计划列表");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        DEParameter parameter = context.getDeParameter();
        //解析创建推广计划的请求参数
        String planGroupName = parameter.getParameter("planGroupName");
        String dailyBudget = parameter.getParameter("dailyBudget");
        String speedMode = parameter.getParameter("speedMode");
        String productType = parameter.getParameter("productType");
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        if (StringUtils.isBlank(planGroupName)) {
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_NAME_IS_NULL);
        }
        if (StringUtils.isBlank(dailyBudget) || !NumberUtils.isNumber(dailyBudget) || (CurrencyUtils.yuanToFen(new BigDecimal(dailyBudget)).intValue() < 50 && CurrencyUtils.yuanToFen(new BigDecimal(dailyBudget)).intValue() > 400000000)) {
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_DAILY_BUDGET_IS_NULL);
        }
        if (StringUtils.isBlank(speedMode) || SpeedMode.parse(Integer.parseInt(speedMode)) == null) {
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_SPEEDMODE_IS_NULL);
        }
        if (StringUtils.isBlank(productType) || ProductType.parse(Integer.parseInt(productType)) == null) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
        }
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());

        return apiTxCampaignService.add(planGroupName.trim(), new BigDecimal(dailyBudget), Integer.parseInt(productType), Integer.parseInt(speedMode), Integer.parseInt(mediumAccountId), userInSession.getCompanyId(), userInSession.getAccountId(), null, null);
    }

    /**
     * 更新推广计划列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017/11/22
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amPlanGroup/modify.do")
    public Object modify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("更新投放业务推广计划列表");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        PlanGroupCreatePreVO vo = parseModify(context);

        return apiTxCampaignService.modify(vo);
    }

    /**
     * 删除推广计划列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017/11/22
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amPlanGroup/delete.do")
    public Object delete(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("删除投放业务推广计划列表");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String planGroupId = parameter.getParameter("planGroupId");
        String mediumId = parameter.getParameter("mediumId");
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        //校验参数
        if (StringUtils.isBlank(planGroupId)) {
            logger.error("投放推广计划ID不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_ID_IS_NULL);
        }
        if (StringUtils.isBlank(mediumId)) {
            logger.error("投放媒体ID不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.REAL_MEDIUM_ID_MISSING);
        }
        if (StringUtils.isBlank(mediumAccountId)) {
            logger.error("投放媒体帐号ID不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        return apiTxCampaignService.delete(Integer.parseInt(planGroupId), Integer.parseInt(mediumId), Integer.parseInt(mediumAccountId));
    }


    /**
     * 下拉框查询推广计划列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017/11/18
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amPlanGroup/queryForSelect.do")
    public Object queryForSelect(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("下拉框查询投放业务推广计划列表");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        AdGroupQueryVo vo = parseQuerySelect(context);

        // 如果为null，则表示无数据访问权限，否则，有权限
        if (null == vo.getPermissionMediumAccountIds()) {
            return null;
        }
        return apiTxCampaignService.queryAdPlanGroupSelect(vo);
    }

    /**
     * 推广计划名称校验
     *
     * @param request
     * @param response
     * @return Boolean True - 校验通过；False - 校验失败
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amPlanGroup/checkName.do")
    public Object checkPlanGroupName(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("广告组名称校验");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        String planGroupName = parameter.getParameter("planGroupName");
        String planGroupIdStr = parameter.getParameter("planGroupId");
        Integer planGroupId = null;
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_MEDIUM_ACCOUNT_IS_NULL);
        }
        if (StringUtils.isBlank(planGroupName)) {
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_NAME_IS_NULL);
        }
        if (StringUtils.isNotBlank(planGroupIdStr)) {
            planGroupId = Integer.parseInt(planGroupIdStr);
        }
        return apiTxCampaignService.checkAdPlanGroupName(Integer.parseInt(mediumAccountId), planGroupName, planGroupId);
    }


    /**
     * 解析请求参数
     *
     * @param context
     * @return
     * @throws ParseException
     * @author ldj 2017/11/20
     */
    public AdGroupQueryVo parseQuery(DEContext context) throws ParseException {
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }

        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        //构建推广计划查询条件
        AdGroupQueryVo queryVo = new AdGroupQueryVo();

        queryVo.setStartDate(startDate);
        queryVo.setEndDate(endDate);

        queryVo.setPlanGroupIds(StringUtil.stringToList(parameter.getParameter("planGroupIds")));

        queryVo.setProductIds(StringUtil.stringToList(parameter.getParameter("productIds")));
        queryVo.setMediumAccountIds(StringUtil.stringToList(parameter.getParameter("mediumAccountIds")));
        queryVo.setMediumIds(StringUtil.stringToList(parameter.getParameter("mediumIds")));


        // 解析媒体账号权限
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();

        queryVo.setCompanyId(companyId);
        queryVo.setPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());

        //解析系统帐号权限
        Integer[] permissionSystemAccountIds = userInSession.getPermissionSystemAccountIds();
        if (permissionSystemAccountIds != null && permissionSystemAccountIds.length > 0) {
            queryVo.setPermissionSystemAccountIds(permissionSystemAccountIds);
        }
        return queryVo;
    }

    /**
     * 解析请求参数
     *
     * @param context
     * @return
     * @throws ParseException
     * @author ldj 2017/11/22
     */
    public PlanGroupCreatePreVO parseModify(DEContext context) throws ParseException {
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        PlanGroupCreatePreVO vo = new PlanGroupCreatePreVO();
        String planGroupId = parameter.getParameter("planGroupId");
        String mediumId = parameter.getParameter("mediumId");
        String planGroupName = parameter.getParameter("planGroupName");
        String dailyBudget = parameter.getParameter("dailyBudget");
        String speedMode = parameter.getParameter("speedMode");
        String planGroupStatus = parameter.getParameter("planGroupStatus");
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        //校验参数
        if (StringUtils.isBlank(planGroupId)) {
            logger.error("投放推广计划ID不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_ID_IS_NULL);
        }
        if (StringUtils.isNotBlank(planGroupName)) {
            vo.setPlanGroupName(planGroupName.trim());
        }
        //最小值 5000，最大值 400000000
        if (!StringUtils.isBlank(dailyBudget) && NumberUtils.isNumber(dailyBudget) && (CurrencyUtils.yuanToFen(new BigDecimal(dailyBudget)).intValue() >= 50 && CurrencyUtils.yuanToFen(new BigDecimal(dailyBudget)).intValue() <= 400000000)) {
            vo.setDailyBudget(CurrencyUtils.yuanToFen(new BigDecimal(dailyBudget)).intValue());
        }
        if (StringUtils.isNotBlank(speedMode) && NumberUtils.isNumber(speedMode)) {
            vo.setSpeedMode(Integer.parseInt(speedMode));
        }
        if (StringUtils.isNotBlank(planGroupStatus) && NumberUtils.isNumber(planGroupStatus)) {
            vo.setPlanGroupStatus(Integer.parseInt(planGroupStatus));
        }
        if (StringUtils.isBlank(mediumId)) {
            logger.error("投放媒体ID不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.REAL_MEDIUM_ID_MISSING);
        }
        if (StringUtils.isBlank(mediumAccountId)) {
            logger.error("投放媒体帐号ID不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();

        vo.setCompanyId(companyId);
        vo.setPlanGroupId(Integer.parseInt(planGroupId));
        vo.setMediumAccountId(Integer.parseInt(mediumAccountId));
        vo.setMediumId(Integer.parseInt(mediumId));
        return vo;
    }

    /**
     * 解析请求参数
     *
     * @param context
     * @return
     * @throws ParseException
     * @author ldj 2017/11/29
     */
    public AdGroupQueryVo parseQuerySelect(DEContext context) throws ParseException {
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        //构建推广计划查询条件
        AdGroupQueryVo queryVo = new AdGroupQueryVo();

        queryVo.setPlanGroupIds(StringUtil.stringToList(parameter.getParameter("planGroupIds")));

        queryVo.setProductIds(StringUtil.stringToList(parameter.getParameter("productIds")));
        queryVo.setMediumAccountIds(StringUtil.stringToList(parameter.getParameter("mediumAccountIds")));
        queryVo.setMediumIds(StringUtil.stringToList(parameter.getParameter("mediumIds")));

        String productType = parameter.getParameter("productType");
        //校验参数
        if (StringUtils.isNotBlank(productType)) {
            queryVo.setProductType(Integer.parseInt(productType));
        }
        // 解析媒体账号权限
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();

        queryVo.setCompanyId(companyId);
        queryVo.setPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());

        //解析系统帐号权限
        Integer[] permissionSystemAccountIds = userInSession.getPermissionSystemAccountIds();
        if (permissionSystemAccountIds != null && permissionSystemAccountIds.length > 0) {
            queryVo.setPermissionSystemAccountIds(permissionSystemAccountIds);
        }
        return queryVo;
    }


}
