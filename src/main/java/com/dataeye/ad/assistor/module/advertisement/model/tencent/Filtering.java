package com.dataeye.ad.assistor.module.advertisement.model.tencent;

import java.util.List;



/**
 * @author ldj
 * @date 2017-12-12
 */
public class Filtering {


    private String field;

    private String operator;

    private List<String> values;

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

}
