package com.dataeye.ad.assistor.module.report.model;

import com.google.gson.annotations.Expose;

/**
 * 投放关键指标
 * Created by huangzehai on 2017/5/23.
 */
public class DeliveryKeyIndicator {
    /**
     * 累计消耗
     */
    @Expose
    private String cost;
    /**
     * 累计下载
     */
    @Expose
    private String downloads;
    /**
     * 累计激活
     */
    @Expose
    private String activations;

    /**
     * 累计注册
     */
    @Expose
    private String registrations;

    /**
     * DAU.
     */
    @Expose
    private String dau;

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDownloads() {
        return downloads;
    }

    public void setDownloads(String downloads) {
        this.downloads = downloads;
    }

    public String getActivations() {
        return activations;
    }

    public void setActivations(String activations) {
        this.activations = activations;
    }

    public String getRegistrations() {
        return registrations;
    }

    public void setRegistrations(String registrations) {
        this.registrations = registrations;
    }

    public String getDau() {
        return dau;
    }

    public void setDau(String dau) {
        this.dau = dau;
    }

    @Override
    public String toString() {
        return "DeliveryKeyIndicator{" +
                "cost=" + cost +
                ", downloads=" + downloads +
                ", activations=" + activations +
                ", registrations=" + registrations +
                ", dau=" + dau +
                '}';
    }
}
