package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;

/**
 * 余额
 * Created by huangzehai on 2017/4/19.
 */
public class Balance {
    /**
     * 媒体名称.
     */
    @Expose
    private String mediumName;
    /**
     * 媒体账户名称.
     */
    @Expose
    private String mediumAccount;
    /**
     * 媒体账户别名.
     */
    @Expose
    private String mediumAccountAlias;
    /**
     * 当前余额.
     */
    @Expose
    private BigDecimal balance;

    /**
     * 是否需要告警.
     */
    @Expose
    private boolean warning;

    /**
     * 媒体ID.
     */
    private Integer mediumId;

    /**
     * 媒体账号ID.
     */
    private Integer mediumAccountId;

    @Expose
    private int status;
    
    /**昨日余额*/
    @Expose
    private BigDecimal yesterdayBalance;
    
    private Integer companyId;

    public String getMediumName() {
        return mediumName;
    }

    public void setMediumName(String mediumName) {
        this.mediumName = mediumName;
    }

    public String getMediumAccount() {
        return mediumAccount;
    }

    public void setMediumAccount(String mediumAccount) {
        this.mediumAccount = mediumAccount;
    }

    public String getMediumAccountAlias() {
        return mediumAccountAlias;
    }

    public void setMediumAccountAlias(String mediumAccountAlias) {
        this.mediumAccountAlias = mediumAccountAlias;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public boolean isWarning() {
        return warning;
    }

    public void setWarning(boolean warning) {
        this.warning = warning;
    }

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public BigDecimal getYesterdayBalance() {
		return yesterdayBalance;
	}
	public void setYesterdayBalance(BigDecimal yesterdayBalance) {
		this.yesterdayBalance = yesterdayBalance;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	@Override
	public String toString() {
		return "Balance [mediumName=" + mediumName + ", mediumAccount="
				+ mediumAccount + ", mediumAccountAlias=" + mediumAccountAlias
				+ ", balance=" + balance + ", warning=" + warning
				+ ", mediumId=" + mediumId + ", mediumAccountId="
				+ mediumAccountId + ", status=" + status
				+ ", yesterdayBalance=" + yesterdayBalance + ", companyId="
				+ companyId + "]";
	}
}
