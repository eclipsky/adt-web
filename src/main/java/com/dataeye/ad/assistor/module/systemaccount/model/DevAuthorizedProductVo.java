package com.dataeye.ad.assistor.module.systemaccount.model;

import java.util.Date;
import java.util.List;

import com.dataeye.ad.assistor.module.product.model.ProductVo;


/**
 * 开发工程师授权产品VO
 * 
 * @author luzhuyou 2017-08-29
 */
public class DevAuthorizedProductVo {
	/** 账号ID */
	private Integer accountId;
	/** 公司ID */
	private Integer companyId;
	/** 授权产品：多个用逗号分隔 */
	private String productIds;
	/** 授权产品*/
	private List<ProductVo> productList;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getProductIds() {
		return productIds;
	}
	public void setProductIds(String productIds) {
		this.productIds = productIds;
	}
	public List<ProductVo> getProductList() {
		return productList;
	}
	public void setProductList(List<ProductVo> productList) {
		this.productList = productList;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
}
