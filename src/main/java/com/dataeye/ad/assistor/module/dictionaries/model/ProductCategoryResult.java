package com.dataeye.ad.assistor.module.dictionaries.model;

import java.util.List;

import com.google.gson.annotations.Expose;

public class ProductCategoryResult {
	/**分类ID*/
	@Expose
	private Integer categoryId;
	
	/**分类名称*/
	@Expose
	private String categoryName;
	
	/**父级id*/
	@Expose
	private List<ProductCategory> children;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<ProductCategory> getChildren() {
		return children;
	}

	public void setChildren(List<ProductCategory> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "ProductCategoryResult [categoryId=" + categoryId
				+ ", categoryName=" + categoryName + ", children=" + children
				+ "]";
	}

	

}
