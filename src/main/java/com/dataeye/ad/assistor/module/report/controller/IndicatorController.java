package com.dataeye.ad.assistor.module.report.controller;

import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.report.constant.Indicators;
import com.dataeye.ad.assistor.module.report.constant.MenuIds;
import com.dataeye.ad.assistor.module.report.model.IndicatorHabit;
import com.dataeye.ad.assistor.module.report.model.IndicatorPreferenceQuery;
import com.dataeye.ad.assistor.module.report.service.IndicatorService;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccount;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by huangzehai on 2017/6/9.
 */
@Controller
public class IndicatorController {


    @Autowired
    private IndicatorService indicatorService;

    @Autowired
    private SystemAccountService systemAccountService;

    /**
     * 投放日报指标偏好
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/indicator-preference.do")
    public Object dailyDeliveryReportIndicatorPreference(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        IndicatorPreferenceQuery query = new IndicatorPreferenceQuery();
        query.setMenuId(MenuIds.DAILY_DELIVERY_REPORT_MENU_ID);
        query.setAccountId(userInSession.getAccountId());
        query.setAccountRole(userInSession.getAccountRole());
        query.setCompanyId(userInSession.getCompanyId());
        return indicatorService.getIndicatorPreference(query);
    }

    /**
     * 实时操盘指标偏好
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/real-time/indicator-preference.do")
    public Object realTimeOperationIndicatorPreference(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        IndicatorPreferenceQuery query = new IndicatorPreferenceQuery();
        query.setMenuId(MenuIds.REAL_TIME_OPERATION_REPORT_MENU_ID);
        query.setAccountId(userInSession.getAccountId());
        query.setAccountRole(userInSession.getAccountRole());
        query.setCompanyId(userInSession.getCompanyId());
        return indicatorService.getIndicatorPreference(query);
    }


    /**
     * LTV指标偏好
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/ltv/indicator-preference.do")
    public Object getLtvIndicatorPreference(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        IndicatorPreferenceQuery query = new IndicatorPreferenceQuery();
        query.setMenuId(MenuIds.LTV_ANALYSIS_MENU_ID);
        query.setAccountId(userInSession.getAccountId());
        query.setAccountRole(userInSession.getAccountRole());
        query.setCompanyId(userInSession.getCompanyId());
        return indicatorService.getIndicatorPreference(query);
    }


    /**
     * 更新实时操盘指标偏好
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/real-time/indicator-preference/update.do")
    public Object updateRealTimeIndicatorPreference(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        DEParameter parameter = context.getDeParameter();
        String indicatorPreference = parameter.getParameter(DEParameter.Keys.INDICATOR_PREFERENCE);
        if (StringUtils.isBlank(indicatorPreference)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_INDICATOR_VISIBILITY_MISSING);
        }
        IndicatorHabit indicatorHabit = new IndicatorHabit();
        indicatorHabit.setCompanyId(userInSession.getCompanyId());
        indicatorHabit.setAccountId(userInSession.getAccountId());
        indicatorHabit.setMenuId(MenuIds.REAL_TIME_OPERATION_REPORT_MENU_ID);
        indicatorHabit.setIndicatorHabit(indicatorPreference);
        return indicatorService.updateIndicatorPreference(indicatorHabit);
    }

    /**
     * 更新投放日报指标偏好
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/indicator-preference/update.do")
    public Object updateDailyDeliveryReportIndicatorPreference(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        DEParameter parameter = context.getDeParameter();
        String indicatorPreference = parameter.getParameter(DEParameter.Keys.INDICATOR_PREFERENCE);
        if (StringUtils.isBlank(indicatorPreference)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_INDICATOR_VISIBILITY_MISSING);
        }
        IndicatorHabit indicatorHabit = new IndicatorHabit();
        indicatorHabit.setCompanyId(userInSession.getCompanyId());
        indicatorHabit.setAccountId(userInSession.getAccountId());
        indicatorHabit.setMenuId(MenuIds.DAILY_DELIVERY_REPORT_MENU_ID);
        indicatorHabit.setIndicatorHabit(indicatorPreference);
        return indicatorService.updateIndicatorPreference(indicatorHabit);
    }


    /**
     * 更新LTV指标偏好
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/ltv/indicator-preference/update.do")
    public Object updateLtvIndicatorPreference(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        DEParameter parameter = context.getDeParameter();
        String indicatorPreference = parameter.getParameter(DEParameter.Keys.INDICATOR_PREFERENCE);
        if (StringUtils.isBlank(indicatorPreference)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_INDICATOR_VISIBILITY_MISSING);
        }
        IndicatorHabit indicatorHabit = new IndicatorHabit();
        indicatorHabit.setCompanyId(userInSession.getCompanyId());
        indicatorHabit.setAccountId(userInSession.getAccountId());
        indicatorHabit.setMenuId(MenuIds.LTV_ANALYSIS_MENU_ID);
        indicatorHabit.setIndicatorHabit(indicatorPreference);
        return indicatorService.updateIndicatorPreference(indicatorHabit);
    }

    /**
     * 列出所有指标
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/systemaccount/indicators.do")
    public Object listIndicators(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return indicatorService.listIndicators();
    }

    /**
     * 添加7日LTV和30日LTV两个指标
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.Public, write = false)
    @RequestMapping("/ad/indicator/add-ltv-indicators.do")
    public Object addMoreIndicators(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        DEParameter parameter = context.getDeParameter();
        if (!StringUtils.equals("897354:DeDc", parameter.getParameter("psw"))) {
            ExceptionHandler.throwParameterException(StatusCode.LOGI_NO_PRIVILEGE);
        }

        List<SystemAccount> systemAccounts = systemAccountService.listSystemAccounts();
        if (systemAccounts != null) {
            List<Integer> roles = Arrays.asList(0, 1, 2);
            List<String> newIndicatorsForOptimizer = Arrays.asList(Indicators.LTV_7DAYS, Indicators.LTV_30DAYS);
            for (SystemAccount systemAccount : systemAccounts) {
                //0-优化师，1-优化师组长，2-企业账户 添加出价、切换开关指标
                if (roles.contains(systemAccount.getAccountRole())) {
                    systemAccount.setIndicatorPermission(addIndicators(systemAccount.getIndicatorPermission(), newIndicatorsForOptimizer));
                    systemAccountService.updateSystemAccount(systemAccount);
                }
            }
        }
        return "Successfully add indicators of LTV to system account";
    }

    /**
     * 添加新增指标
     *
     * @param indicatorPermission
     * @param newIndicators
     * @return
     */
    private String addIndicators(String indicatorPermission, List<String> newIndicators) {
        Set<String> indicators = new TreeSet<>();
        if (StringUtils.isNotBlank(indicatorPermission)) {
            indicators.addAll(Arrays.asList(indicatorPermission.split(Constant.Separator.COMMA)));
        }
        indicators.addAll(newIndicators);
        return StringUtils.join(indicators, Constant.Separator.COMMA);
    }
    
    /**
     * 获取菜单对应的指标偏好
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/indicator-preference/query.do")
    public Object queryIndicatorPreference(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        IndicatorPreferenceQuery query = new IndicatorPreferenceQuery();
        query.setMenuId(MenuIds.PAYBACK_REPORT_MENU_ID);
        //回本日报--5-1 MenuIds.DAILY_DELIVERY_REPORT_MENU_ID
        query.setAccountId(userInSession.getAccountId());
        query.setAccountRole(userInSession.getAccountRole());
        query.setCompanyId(userInSession.getCompanyId());
        return indicatorService.getIndicatorPreference(query);
    }

    
    /**
     * 更新菜单对应的指标偏好
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/indicator-preference/update.do")
    public Object updateIndicatorPreference(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        DEParameter parameter = context.getDeParameter();
        String indicatorPreference = parameter.getParameter(DEParameter.Keys.INDICATOR_PREFERENCE);
        if (StringUtils.isBlank(indicatorPreference)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_INDICATOR_VISIBILITY_MISSING);
        }
        IndicatorHabit indicatorHabit = new IndicatorHabit();
        indicatorHabit.setCompanyId(userInSession.getCompanyId());
        indicatorHabit.setAccountId(userInSession.getAccountId());
        indicatorHabit.setMenuId(MenuIds.PAYBACK_REPORT_MENU_ID);
        indicatorHabit.setIndicatorHabit(indicatorPreference);
        return indicatorService.updateIndicatorPreference(indicatorHabit);
    }
}
