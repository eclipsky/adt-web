package com.dataeye.ad.assistor.module.report.mapper;

import com.dataeye.ad.assistor.module.report.model.UpsAndDownsPreference;
import org.apache.ibatis.annotations.Param;

/**
 * Created by huangzehai on 2017/7/5.
 */
public interface UpsAndDownsPreferenceMapper {

    /**
     * 获取指定ADT账号的指标涨跌偏好.
     *
     * @param accountId
     * @return
     */
    UpsAndDownsPreference getUpsAndDownsPreference(@Param("accountId") int accountId);

    /**
     * 是否存在指定ADT账号的指标涨跌偏好
     *
     * @param accountId
     * @return
     */
    boolean hasUpsAndDownsPreference(@Param("accountId") int accountId);

    /**
     * 插入指标涨跌偏好
     *
     * @param upsAndDownsPreference
     * @return
     */
    int insertUpsAndDownsPreference(UpsAndDownsPreference upsAndDownsPreference);

    /**
     * 更新指标涨跌偏好
     *
     * @param upsAndDownsPreference
     * @return
     */
    int updateUpsAndDownsPreference(UpsAndDownsPreference upsAndDownsPreference);

    /**
     * 删除指标涨跌偏好
     *
     * @param accountId
     * @return
     */
    int deleteUpsAndDownsPreference(@Param("accountId") int accountId);
}
