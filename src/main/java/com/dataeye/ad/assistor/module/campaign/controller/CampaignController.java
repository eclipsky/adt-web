package com.dataeye.ad.assistor.module.campaign.controller;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.Constant.MediumType;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.*;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.campaign.constants.CampaignEventsType;
import com.dataeye.ad.assistor.module.campaign.constants.TrackingResChannelParams;
import com.dataeye.ad.assistor.module.campaign.constants.TrackingResTrackUrl;
import com.dataeye.ad.assistor.module.campaign.model.CampaignCreateReqVo;
import com.dataeye.ad.assistor.module.campaign.model.CampaignQueryReqVo;
import com.dataeye.ad.assistor.module.campaign.model.CampaignSelectorVo;
import com.dataeye.ad.assistor.module.campaign.model.CampaignVo;
import com.dataeye.ad.assistor.module.campaign.service.CampaignRemoteService;
import com.dataeye.ad.assistor.module.campaign.service.CampaignService;
import com.dataeye.ad.assistor.module.campaign.util.ExcelUtils;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Controller
public class CampaignController {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(CampaignController.class);
    
    private static final String CONTENT_TYPE = "application/msexcel;charset=UTF-8";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String CONTENT_DISPOSITION_VALUE = "attachment;filename=campaign.xls";
    
    @Autowired
    private CampaignService campaignService;
    @Autowired
    private CampaignRemoteService campaignRemoteService;

	/**
     * 查询渠道参数
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/campaign/queryChannelParams.do")
    public Object queryChannelParams(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Query Channel Params![/ad/campaign/queryChannelParams.do]");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        
        // 1. 参数解析与校验
        String appId = parameter.getParameter("appId");
        String mediumId = parameter.getParameter("mediumId");
        String osType = parameter.getParameter("osType");
        String channelId = parameter.getParameter("channelId");
        // 校验appId是否为空
        if (StringUtils.isBlank(appId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_APP_ID_IS_NULL);
        }
        // 校验mediumId是否为空
        if (StringUtils.isBlank(mediumId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_MEDIUM_ID_IS_NULL);
        }
        // 校验osType是否为空
        if (StringUtils.isBlank(osType)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
        }
        /*//检验媒体为3-智汇推的时候，是否使用外链必填
        if (Integer.parseInt(mediumId.trim()) == 3 && StringUtils.isBlank(trackingAccessType)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_TRACKING_ACCESSTYPE_IS_NULL);
        }*/
       // 校验渠道id是否为空
        if (StringUtils.isBlank(channelId) && StringUtils.isNumeric(channelId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CHANNEL_ID_IS_NULL);
        }
      //如果是adt媒体，则渠道id和媒体id不相等
        if(MediumType.getMediumType(Integer.parseInt(mediumId.trim())) == MediumType.ADT && Integer.parseInt(mediumId.trim()) == Integer.parseInt(channelId.trim())){
        	ExceptionHandler.throwParameterException(StatusCode.CAMP_CHANNEL_ID_IS_NULL);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyAccountUid = userInSession.getCompanyAccountUid();
        
        TrackingResChannelParams result = campaignService.getChannelParams(appId, companyAccountUid, Integer.parseInt(mediumId.trim()), Integer.parseInt(osType.trim()),channelId.trim());
        return result;
    }
    
    /**
     * 查询推广计划
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/campaign/query.do")
    public Object query(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Query Campaign List![/ad/campaign/query.do]");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        CampaignQueryReqVo vo = parseQuery(context);
        
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (null == vo.getPermissionMediumAccountIds()) {
            return null;
        }
        DEParameter parameter = context.getDeParameter();
        
        List<CampaignVo> result = campaignService.query(vo);
        if(result.size() > 1){
        	sort(result);
        }
        PageData pageData = PageDataHelper.getPageDataByList(parameter.getPageSize(), parameter.getPageId(), result);
        return pageData;
    }
    
    /**
     * 解析请求参数
     * @param context
     * @return
     * @throws ParseException
     * @author luzhuyou 2017/06/19
     */
    public CampaignQueryReqVo parseQuery(DEContext context) throws ParseException {
    	//参数解析与校验
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }

        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        //构建推广活动查询条件
        CampaignQueryReqVo vo = new CampaignQueryReqVo();
        vo.setStartDate(startDate);
        vo.setEndDate(endDate);
        String campaignIds = parameter.getParameter("campaignIds");
        if(StringUtils.isBlank(campaignIds)){
        	campaignIds = null;
        }

        //解析产品ID列表
        vo.setProductIds(StringUtil.stringToList(parameter.getParameter("productIds")));
        vo.setCampaignIds(StringUtil.stringToListString(campaignIds));
        vo.setMediumAccountIds(StringUtil.stringToList(parameter.getParameter("mediumAccountIds")));
        vo.setMediumIds(StringUtil.stringToList(parameter.getParameter("mediumIds")));
        vo.setAppIds(parameter.getParameter("appIds"));
        vo.setTrackingCampaignIds(campaignIds);
        
        // 解析媒体账号权限
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        int uid = userInSession.getCompanyAccountUid();
        
        vo.setCompanyId(companyId);
        vo.setPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());
        vo.setUid(uid);
        
        //解析系统帐号权限
        Integer[] permissionSystemAccountIds = userInSession.getPermissionSystemAccountIds();
        if(permissionSystemAccountIds != null && permissionSystemAccountIds.length>0){
        	String adtAccountIds = StringUtil.join(permissionSystemAccountIds, ",");
        	vo.setAdtAccountIds(adtAccountIds);
        	vo.setPermissionSystemAccountIds(permissionSystemAccountIds);
        }
        return vo;
    }
    
    /**
     * 下拉框查询推广计划
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/campaign/queryForSelector.do")
    public Object queryForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Query CampaignSelector List![/ad/campaign/queryForSelector.do]");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        CampaignQueryReqVo vo = parseQuerySelector(context);
        
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (null == vo.getPermissionMediumAccountIds()) {
            return null;
        }
        List<CampaignSelectorVo> result = campaignService.queryForSelector(vo);
        return result;
    }
    
    /**
     * 解析下拉框查询推广计划请求参数
     * @param context
     * @return
     * @throws ParseException
     * @author luzhuyou 2017/06/19
     */
    public CampaignQueryReqVo parseQuerySelector(DEContext context) throws ParseException {
    	//参数解析与校验
        DEParameter parameter = context.getDeParameter();
        //构建下拉框查询推广活动查询条件
        CampaignQueryReqVo vo = new CampaignQueryReqVo();

        //解析产品ID列表
        vo.setProductIds(StringUtil.stringToList(parameter.getParameter("productIds")));
        vo.setMediumAccountIds(StringUtil.stringToList(parameter.getParameter("mediumAccountIds")));
        vo.setMediumIds(StringUtil.stringToList(parameter.getParameter("mediumIds")));
        
        // 解析媒体账号权限
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        
        vo.setCompanyId(companyId);
        vo.setPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());
        
        //解析系统帐号权限
        Integer[] permissionSystemAccountIds = userInSession.getPermissionSystemAccountIds();
        if(permissionSystemAccountIds != null && permissionSystemAccountIds.length>0){
        	String adtAccountIds = StringUtil.join(permissionSystemAccountIds, ",");
        	vo.setAdtAccountIds(adtAccountIds);
        	vo.setPermissionSystemAccountIds(permissionSystemAccountIds);
        }
        return vo;
    }
    
    /**
     * 预创建推广链接
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/campaign/createCampaignBefore.do")
    public Object createCampaignBefore(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Query Campaign List![/ad/campaign/createCampaignBefore.do]");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        CampaignCreateReqVo vo = parseCreateCampaignBefore(context);
        
        TrackingResTrackUrl result = campaignService.preCreate(vo);
        return result;
    }
    
    /**
     * 解析预创建推广链接请求参数
     * @param context
     * @return
     * @throws ParseException
     * @author luzhuyou 2017/06/23
     */
    public CampaignCreateReqVo parseCreateCampaignBefore(DEContext context) throws ParseException {
    	//参数解析与校验
        DEParameter parameter = context.getDeParameter();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyAccountUid = userInSession.getCompanyAccountUid();
        int companyId = userInSession.getCompanyId();
        int accountId = userInSession.getAccountId();
        //构建推广活动查询条件
        CampaignCreateReqVo vo = new CampaignCreateReqVo();

        String appId = parameter.getParameter("appId");
        String campaignName = parameter.getParameter("campaignName");
        String mediumId = parameter.getParameter("mediumId");
        String osType = parameter.getParameter("osType");
        String downloadUrl = parameter.getParameter("downloadUrl");
        String channelId = parameter.getParameter("channelId");
        // 校验appId是否为空
        if (StringUtils.isBlank(appId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_APP_ID_IS_NULL);
        }
        // 校验campaignName是否为空
        if (StringUtils.isBlank(campaignName)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_NAME_IS_NULL);
        }
        // 校验媒体是否为空
        if (StringUtils.isBlank(mediumId) || !StringUtils.isNumeric(mediumId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_MEDIUM_ID_IS_ERROR);
        }
        // 校验osType是否为空
        if (StringUtils.isBlank(osType) || !StringUtils.isNumeric(osType)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
        }
        // 校验下载地址是否为空
        if (StringUtils.isBlank(downloadUrl)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_DOWNLOAD_URL_IS_NULL);
        }
      /*//检验媒体为3-智汇推的时候，是否使用外链必填
        if (Integer.parseInt(mediumId.trim()) == 3 && StringUtils.isBlank(trackingAccessType)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_TRACKING_ACCESSTYPE_IS_NULL);
        }*/
     // 校验渠道id是否为空
        if (StringUtils.isBlank(channelId) && StringUtils.isNumeric(channelId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CHANNEL_ID_IS_NULL);
        }
        //如果是adt媒体，则渠道id和媒体id不相等
        if(MediumType.getMediumType(Integer.parseInt(mediumId.trim())) == MediumType.ADT && Integer.parseInt(mediumId.trim()) == Integer.parseInt(channelId.trim())){
        	ExceptionHandler.throwParameterException(StatusCode.CAMP_CHANNEL_ID_IS_NULL);
        }
        
        vo.setAppId(appId.trim());
        vo.setCompanyAccountUid(companyAccountUid);
        vo.setMediumId(Integer.parseInt(mediumId.trim()));
        vo.setCampaignName(campaignName.trim());
        vo.setOsType(Integer.parseInt(osType.trim()));
        vo.setCompanyId(companyId);
        vo.setDownloadUrl(downloadUrl.trim());
        vo.setSystemAccountId(accountId);
        //vo.setTrackingAccessType(StringUtils.isBlank(trackingAccessType) ? null : Integer.parseInt(trackingAccessType.trim()));
        vo.setTrackingMediumId(Integer.parseInt(channelId.trim()));
        return vo;
    }
    
    /**
     * 创建推广活动
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/campaign/add.do")
    public Object add(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Add Campaign![/ad/campaign/add.do]");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        CampaignCreateReqVo vo = parseAdd(context);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int userId = userInSession.getAccountId();
        int companyId = userInSession.getCompanyId();
        vo.setSystemAccountId(userId);
        vo.setCompanyId(companyId);
        campaignService.create(vo);
        return null;
    }
	
	/**
     * 解析预创建推广链接请求参数
     * @param context
     * @return
     * @throws ParseException
     * @author luzhuyou 2017/06/23
     * @update ldj 2018-01-16 上应用宝对应应用宝，不上应用宝对应落地页，应用宝和联盟推广逻辑相同， 联盟推广==2
     */
    public CampaignCreateReqVo parseAdd(DEContext context) throws ParseException {
    	CampaignCreateReqVo vo = parseCreateCampaignBefore(context);
    	//参数解析与校验
        DEParameter parameter = context.getDeParameter();
        
        String productId = parameter.getParameter("productId");
        String appTreasure = parameter.getParameter("appTreasure");
        String urlCount = parameter.getParameter("urlCount");
        String campaignId = parameter.getParameter("campaignId");
        String trackUrl = parameter.getParameter("trackUrl");
        String thirdAdParams = parameter.getParameter("thirdAdParams");
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        String eventType = parameter.getParameter("eventType");
        
        // 校验productId是否为空
        if (StringUtils.isBlank(productId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_PRODUCT_ID_IS_NULL);
        }
        // 校验mediumAccountId是否为空
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        /*//检验媒体为3-智汇推的时候，是否使用外链必填
        if (vo.getMediumId() == 3 && StringUtils.isBlank(trackingAccessType)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_TRACKING_ACCESSTYPE_IS_NULL);
        }*/
        // 校验智汇推或广点通且为Android下“是否上应用宝”是否为空或输入错误 媒体为3-广点通或13-智汇推且为Android时必填
        if (((vo.getMediumId() == 3 && vo.getTrackingMediumId()!=null && vo.getTrackingMediumId().intValue() == 1021) || vo.getMediumId() == 13) && (2 == vo.getOsType())  && (StringUtils.isBlank(appTreasure) || !StringUtils.isNumeric(appTreasure))) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_APP_TREASURE_IS_ERROR);
        }
        // 校验非智汇推或非广点通下“推广链接数量”是否为空或输入错误（推广链接数量必须在1到100之间）, 媒体为3-智汇推，并且使用外链的时候
        if (((vo.getMediumId() == 3 && vo.getTrackingMediumId()!=null && vo.getTrackingMediumId().intValue() == 1167) || vo.getMediumId() != 3) && vo.getMediumId() != 13 && (StringUtils.isBlank(urlCount) || !StringUtils.isNumeric(urlCount))) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_URL_COUNT_IS_ERROR);
        }
        // 校验“活动ID”在智汇推或广点通下android平台上应用宝或是iOS时，是否为空
        if (((vo.getMediumId() == 3 && vo.getTrackingMediumId()!=null && vo.getTrackingMediumId().intValue() == 1021) || vo.getMediumId() == 13) && (
        		StringUtils.isBlank(campaignId) && (
        			(2 == vo.getOsType() && ("1".equals(appTreasure.trim()) || "2".equals(appTreasure.trim()))//Android上应用宝
        			|| 1 == vo.getOsType()))//iOS
        		)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_ID_IS_NULL);
        }
        
        // 校验推广链接是否为空
        if (StringUtils.isNotBlank(campaignId) && StringUtils.isBlank(trackUrl)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_TRACK_URL_IS_NULL);
        }

        vo.setAppTreasure(StringUtils.isBlank(appTreasure) ? 0 : Integer.parseInt(appTreasure.trim()));
        vo.setUrlCount(StringUtils.isBlank(urlCount) ? 1 : Integer.parseInt(urlCount.trim()));
        vo.setCampaignId(StringUtils.isBlank(campaignId) ? "" : campaignId.trim());
        vo.setTrackUrl(StringUtils.isBlank(trackUrl) ? "" : trackUrl.trim());
        vo.setThirdAdParams(StringUtils.isBlank(thirdAdParams) ? "" : thirdAdParams);
        vo.setProductId(Integer.parseInt(productId));
        vo.setMediumAccountId(Integer.parseInt(mediumAccountId));
        
        if(StringUtils.isNotBlank(eventType) && CampaignEventsType.parse(eventType)!=null){
        	vo.setEventType(CampaignEventsType.parse(eventType)+"");
        }
        
        return vo;
    }
    
    /**
     * 更新推广计划
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/campaign/modify.do")
    public Object modify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Modify Campaign![/ad/campaign/modify.do]");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        
        // 1. 参数解析与校验
        String appId = parameter.getParameter("appId");
        String campaignId = parameter.getParameter("campaignId");
        String campaignName = parameter.getParameter("campaignName");
        String downloadUrl = parameter.getParameter("downloadUrl");
        String trackUrl = parameter.getParameter("trackUrl");
        String thirdAdParams = parameter.getParameter("thirdAdParams");
        // 校验appId是否为空
        if (StringUtils.isBlank(appId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_APP_ID_IS_NULL);
        }
        // 校验campaignId是否为空
        if (StringUtils.isBlank(campaignId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_ID_IS_NULL);
        }
        // 校验活动名称是否为空
        if (StringUtils.isBlank(campaignName)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_NAME_IS_NULL);
        }
        // 校验下载地址是否为空
        if (StringUtils.isBlank(downloadUrl)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_DOWNLOAD_URL_IS_NULL);
        }
        // 校验推广链接是否为空
        if (StringUtils.isBlank(trackUrl)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_TRACK_URL_IS_NULL);
        }
        
        
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyAccountUid = userInSession.getCompanyAccountUid();
        int companyId = userInSession.getCompanyId();
        
        CampaignVo vo = new CampaignVo();
        vo.setAppId(appId);
        vo.setCampaignId(campaignId);
        vo.setCampaignName(campaignName);
        vo.setCompanyAccountUid(companyAccountUid);
        vo.setDownloadUrl(downloadUrl);
        vo.setTrackUrl(trackUrl);
        vo.setThirdAdParams(thirdAdParams);
        vo.setCompanyId(companyId);
        int result = campaignService.modify(vo);
        return result;
    }
	
	/**
     * 删除推广计划
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/campaign/delete.do")
    public Object delete(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Delete Campaign![/ad/campaign/delete.do]");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        
        // 1. 参数解析与校验
        String appId = parameter.getParameter("appId");
        String campaignId = parameter.getParameter("campaignId");
        // 校验appId是否为空
        if (StringUtils.isBlank(appId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_APP_ID_IS_NULL);
        }
        // 校验campaignId是否为空
        if (StringUtils.isBlank(campaignId)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_ID_IS_NULL);
        }
        
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyAccountUid = userInSession.getCompanyAccountUid();
        
        int result = campaignService.delete(appId.trim(), companyAccountUid, campaignId.trim());
        return result;
    }
	

	public void sort(List<CampaignVo> list) {
		Collections.sort(list, new Comparator<CampaignVo>() {
	           @Override
	           public int compare(CampaignVo o1, CampaignVo o2) {
	        	   if(o1.getCreateTime().equals(o2.getCreateTime())){
	        		   if(StringUtils.isNotBlank(o1.getCampaignName()) && StringUtils.isNotBlank(o2.getCampaignName())){
	        			   return o1.getCampaignName().compareTo(o2.getCampaignName());
	        		   }else if (StringUtils.isNotBlank(o1.getCampaignName())) {
	        			   return 1;
					   }else if (StringUtils.isNotBlank(o2.getCampaignName())) {
	        			   return -1;
					   }
	        		   return 0;
		           	}else{
		           		return o2.getCreateTime().compareTo(o1.getCreateTime());
		           	}
	           }
	       });
	}
	
	/**
     * 监测链接下载当前表格
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/campaign/downloadTable.do")
    public void downloadTable(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        CampaignQueryReqVo vo = parseQuery(context);
        List<CampaignVo> result = new ArrayList<CampaignVo>();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (vo.getPermissionMediumAccountIds() != null) {
        	result = campaignService.query(vo);
        }
        //返回文件
        sort(result);
        Workbook wb = ExcelUtils.buildWorkBookForCampaign(result);
        response.setContentType(CONTENT_TYPE);
        response.addHeader(CONTENT_DISPOSITION, CONTENT_DISPOSITION_VALUE);
        ServletOutputStream out = response.getOutputStream();
        wb.write(out);
        out.flush();
        out.close();
    }
    
    /**
     * 批量更新更新推广计划
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/campaign/batch/modify.do")
    public Object modifyBatchCampaign(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Batch Modify Campaign![/ad/campaign/batch/modify.do]");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        
        // 1. 参数解析与校验
        String campaignIdList = parameter.getParameter("campaignIdList");
       // String campaignName = parameter.getParameter("campaignName");
        String downloadUrl = parameter.getParameter("downloadUrl");
        // 校验campaignId是否为空
        if (StringUtils.isBlank(campaignIdList)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_ID_IS_NULL);
        }
        // 校验活动名称是否为空
      /*  if (StringUtils.isBlank(campaignName)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_NAME_IS_NULL);
        }*/
        // 校验下载地址是否为空
        if (StringUtils.isBlank(downloadUrl)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_DOWNLOAD_URL_IS_NULL);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyAccountUid = userInSession.getCompanyAccountUid();
        JsonArray campaignArray = StringUtil.jsonParser.parse(campaignIdList).getAsJsonArray();
        if (campaignArray.size() <= 0) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_ID_IS_NULL);
        }
        List<CampaignVo> responseResult = StringUtil.gson.fromJson(campaignArray, new TypeToken<List<CampaignVo>>() {}.getType());
        int result = 0;
        for (CampaignVo vo : responseResult) {
            if (vo.getCampaignId() == null || StringUtils.isBlank(vo.getCampaignId())) {
                ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_ID_IS_NULL);
            }
            if (vo.getAppId() == null || StringUtils.isBlank(vo.getAppId())) {
                ExceptionHandler.throwParameterException(StatusCode.CAMP_APP_ID_IS_NULL);
            }
            if (vo.getCampaignName() == null || StringUtils.isBlank(vo.getCampaignName())) {
            	ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_NAME_IS_NULL);
            }
            vo.setCompanyAccountUid(companyAccountUid);
            vo.setDownloadUrl(downloadUrl);
            campaignService.modify(vo);
        }
        
        return result;
    }
	
	/**
     * 批量删除推广计划
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/campaign/batch/delete.do")
    public Object deleteBatchCampaign(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Batch Delete Campaign![/ad/campaign/batch/delete.do]");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        
        // 1. 参数解析与校验
        String campaignIdList = parameter.getParameter("campaignIdList");
        if (StringUtils.isBlank(campaignIdList)) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_ID_IS_NULL);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyAccountUid = userInSession.getCompanyAccountUid();
        JsonArray campaignArray = StringUtil.jsonParser.parse(campaignIdList).getAsJsonArray();
        if (campaignArray.size() <= 0) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_ID_IS_NULL);
        }
        List<CampaignVo> responseResult = StringUtil.gson.fromJson(campaignArray, new TypeToken<List<CampaignVo>>() {}.getType());
        int result = 0;
        for (CampaignVo vo : responseResult) {
            if (vo.getCampaignId() == null || StringUtils.isBlank(vo.getCampaignId())) {
                ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_ID_IS_NULL);
            }
            if (vo.getAppId() == null || StringUtils.isBlank(vo.getAppId())) {
                ExceptionHandler.throwParameterException(StatusCode.CAMP_APP_ID_IS_NULL);
            }
            result = campaignService.delete(vo.getAppId().trim(), companyAccountUid, vo.getCampaignId().trim());
        }
        return result;
    }
	
}
