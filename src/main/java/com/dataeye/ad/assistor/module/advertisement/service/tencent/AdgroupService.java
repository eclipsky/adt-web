package com.dataeye.ad.assistor.module.advertisement.service.tencent;



import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.BillingEvent;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ConfiguredStatus;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ProductType;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.SystemStatus;
import com.dataeye.ad.assistor.module.advertisement.constants.InterfaceConstants.TencentInterface;
import com.dataeye.ad.assistor.module.advertisement.model.AdvertisementGroup;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.Adgroup;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.Filtering;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.HttpUtil;
import com.dataeye.ad.assistor.util.RandomUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


/**
 * 广点通API
 * 广告组Service
 * */
@Service("adgroupService")
public class AdgroupService {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AdgroupService.class);

	/**
	 * 获取广告组
	 * @param account_id
	 * @param adgroup_id
	 * @param campaign_id
	 * @param adgroup_name 
	 * @return 
	 */
	public List<AdvertisementGroup> query(Integer account_id,Integer adgroup_id,Integer campaign_id,String accessToken,String adgroup_name){
		validateParamsByAccount(account_id, accessToken);
		// 1.构建请求参数
		String Tencent_params= StringUtils.substring(TencentInterface.TENCENT_PARAMETERS, 1);
		String params1 = HttpUtil.urlParamReplace(Tencent_params,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		StringBuilder params = new StringBuilder("");
		params.append(params1);
		params.append("&account_id="+account_id);
		if(adgroup_id != null){
			params.append("&adgroup_id="+adgroup_id);
		}
		List<Filtering> filterList = new ArrayList<>();
		if(campaign_id != null){
			Filtering filterVo = new Filtering();
			filterVo.setField("campaign_id");
			filterVo.setOperator("EQUALS");
			List<String> filterValues = new ArrayList<>();
			filterValues.add(campaign_id+"");
			filterVo.setValues(filterValues);
			filterList.add(filterVo);
		}
		if(StringUtils.isNotBlank(adgroup_name)){
			Filtering filterVo = new Filtering();
			filterVo.setField("adgroup_name");
			filterVo.setOperator("EQUALS");
			List<String> filterValues = new ArrayList<>();
			filterValues.add(adgroup_name);
			filterVo.setValues(filterValues);
			filterList.add(filterVo);
			
		}
		if(!filterList.isEmpty()){
			params.append("&filtering="+HttpUtil.urlEncode(JSONArray.fromObject(filterList).toString()));
		}
	
		String getAdgroupInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADGROUPS_GET;
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.get(getAdgroupInterface,params.toString());
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>查询广告组API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{getAdgroupInterface, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>查询广告组API请求数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{getAdgroupInterface, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
    	JsonObject data=jsonObject.get("data").getAsJsonObject();
    	JsonArray dataJsonObject = data.get("list").getAsJsonArray();
    	List<Adgroup>  response = StringUtil.gson.fromJson(dataJsonObject, new TypeToken<List<Adgroup>>(){}.getType());
    	List<AdvertisementGroup> result = new ArrayList<>();
    	//得到计划信息
    	for (Adgroup adVo : response) {
    		AdvertisementGroup adGroupVO = new AdvertisementGroup();
    		adGroupVO.setAdGroupId(adVo.getAdgroup_id());
    		adGroupVO.setAdGroupName(adVo.getAdgroup_name());
    		adGroupVO.setSiteSet(adVo.getSite_set().get(0));
    		adGroupVO.setCostType(BillingEvent.valueOf(adVo.getBilling_event()).getValue());
    		adGroupVO.setBid(CurrencyUtils.fenToYuan(new BigDecimal(adVo.getBid_amount())));
    		adGroupVO.setProductRefsId(adVo.getProduct_refs_id());
    		adGroupVO.setStatus(ConfiguredStatus.valueOf(adVo.getConfigured_status()).getValue());
    		adGroupVO.setSystemStatus(SystemStatus.valueOf(adVo.getSystem_status()).getValue());
    		adGroupVO.setTargetingId(adVo.getTargeting_id());
    		adGroupVO.setRejectMessage(adVo.getReject_message());
    		adGroupVO.setBeginDate(adVo.getBegin_date());
    		adGroupVO.setEndDate(adVo.getEnd_date());
    		adGroupVO.setTimeSeries(adVo.getTime_series());
			result.add(adGroupVO);
		}
        return result;
	}
	
	/**
	 * 创建广告组
	 * @param account_id(必填)   
	 * @param campaign_id(必填)
	 * @param adgroup_name(必填)
	 * @param site_set(必填)
	 * @param product_type(必填)
	 * @param begin_date(必填)
	 * @param end_date(必填)
	 * @param billing_event(必填)
	 * @param bid_amount(必填) 广告出价，单位为分
	 * @param optimization_goal(必填) 广告优化目标类型
	 * @param product_refs_id
	 * @param targeting_id
	 * @param targeting
	 * @param time_series 
	 * @return adgroup_id  广告组 id
	 */
	public int add(Integer account_id,Integer campaign_id,String adgroup_name,String site_set,String product_type,String begin_date,String end_date,
			String billing_event,Integer bid_amount,String product_refs_id,String targeting_id,String time_series,String accessToken) {
		//校验参数
		validateParamsByAdd(account_id, accessToken, campaign_id, adgroup_name, site_set, product_type, begin_date, end_date, billing_event, bid_amount, targeting_id);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("campaign_id", campaign_id+"");
		params.put("adgroup_name", adgroup_name);
		String site =  "['"+site_set+"']";
		params.put("site_set", JSONArray.fromObject(site).toString());
		params.put("product_type", product_type);
		params.put("begin_date", begin_date);
		params.put("end_date", end_date);
		params.put("billing_event", billing_event);
		params.put("bid_amount", bid_amount+"");
		String optimization_goal = "";
		if(billing_event != null && billing_event.equals("BILLINGEVENT_CLICK")){
			optimization_goal = "OPTIMIZATIONGOAL_CLICK";
		} 
		if(billing_event != null && billing_event.equals("BILLINGEVENT_IMPRESSION")){
			optimization_goal = "OPTIMIZATIONGOAL_IMPRESSION";
		}
		params.put("optimization_goal", optimization_goal);
		params.put("targeting_id", targeting_id);
		if(StringUtils.isNotBlank(product_refs_id)){
			params.put("product_refs_id", product_refs_id);
			
		} 
		if(StringUtils.isNotBlank(time_series)){
			params.put("time_series", time_series);
			
		}
		
		String addAdgroupInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADGROUPS_ADD;
		String addAdgroupInterfaceUrl = HttpUtil.urlParamReplace(addAdgroupInterface,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(addAdgroupInterfaceUrl, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>创建广告组API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{addAdgroupInterfaceUrl, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>创建广告组API请求数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{addAdgroupInterfaceUrl, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }

        JsonObject data = jsonObject.get("data").getAsJsonObject();    	
        return data.get("adgroup_id").getAsInt();
	}
	
	/**
	 * 删除广告组
	 * @param account_id（必填）
	 * @param adgroup_id（必填）
	 * @return
	 */
	public void delete(Integer account_id,Integer adgroup_id,String accessToken) {
		validateParamsByAdgroupId(account_id, accessToken, adgroup_id);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("adgroup_id", adgroup_id+"");
		
		String deleteUrl = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADGROUPS_DELETE;
		String deleteAdGroupInterface = HttpUtil.urlParamReplace(deleteUrl,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(deleteAdGroupInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>删除广告组API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{deleteAdGroupInterface, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>删除广告组API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{deleteAdGroupInterface, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
	}
	
	/**
	 * 更新广告组
	 * @param account_id（必填）
	 * @param adgroup_id（必填）
	 * @param adgroup_name
	 * @param optimization_goal 
	 * @param bid_amount
	 * @param daily_budget
	 * @param targeting_id 
	 * @param begin_date
	 * @param end_date
	 * @param time_series
	 * @param configured_status
	 * @param customized_category
	 */
	public int update(Integer account_id,Integer adgroup_id,String adgroup_name,String optimization_goal,Integer bid_amount,Integer daily_budget,String targeting_id,
			String begin_date,String end_date,String time_series,String configured_status,String customized_category,String accessToken) {
		validateParamsByAdgroupId(account_id, accessToken, adgroup_id);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("adgroup_id", adgroup_id+"");
		if(StringUtils.isNotBlank(adgroup_name)){
			params.put("adgroup_name", adgroup_name);
		}
		if(StringUtils.isNotBlank(optimization_goal)){
			params.put("optimization_goal", optimization_goal);
		}
		if(bid_amount != null){
			params.put("bid_amount", bid_amount+"");
		}
		if(daily_budget != null){
			params.put("daily_budget", daily_budget+"");
		}
		if(StringUtils.isNotBlank(targeting_id)){
			params.put("targeting_id", targeting_id);
		}
		if(StringUtils.isNotBlank(begin_date)){
			params.put("begin_date", begin_date);
		}
		if(StringUtils.isNotBlank(end_date)){
			params.put("end_date", end_date);
		}
		if(StringUtils.isNotBlank(time_series)){
			params.put("time_series", time_series);
		}
		if(StringUtils.isNotBlank(configured_status)){
			params.put("configured_status", configured_status);
		}
		if(StringUtils.isNotBlank(customized_category)){
			params.put("customized_category", customized_category);
		}
		String updateAdgroupInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADGROUPS_UPDATE;
		String updateAdgroupInterfaceUrl = HttpUtil.urlParamReplace(updateAdgroupInterface,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(updateAdgroupInterfaceUrl, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>编辑广告组API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{updateAdgroupInterfaceUrl, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>编辑广告组API请求数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{updateAdgroupInterfaceUrl, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
    	JsonObject data = jsonObject.get("data").getAsJsonObject();    	
        return data.get("adgroup_id").getAsInt();
	}
	
	//参数校验
		private void validateParamsByAccount(Integer account_id,String accessToken){
			if (account_id == null || account_id == 0) {
	            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCOUNT_ID_IS_NULL);
	        }
	        if (StringUtils.isBlank(accessToken)) {
	            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCESSTOKEN_ID_IS_NULL);
	        }
		}
		
		private void validateParamsByAdd(Integer account_id,String accessToken,Integer campaign_id,String adgroup_name,String site_set,String product_type,String begin_date,
				String end_date,String billing_event,Integer bid_amount,String targeting_id){
			validateParamsByAccount(account_id, accessToken);
			if (campaign_id == null || campaign_id == 0) {
	            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_ID_IS_NULL);
	        }
			if (StringUtils.isBlank(adgroup_name)) {
	            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_NAME_IS_NULL);
	        }
			if(StringUtils.isBlank(site_set)){
            	ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_SITESET_IS_NULL);
            }
	        if (StringUtils.isBlank(begin_date)) {
	            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
	        }
	        if (StringUtils.isBlank(end_date)) {
	            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
	        }
	        if(bid_amount == null || bid_amount == 0) {
				ExceptionHandler.throwParameterException(StatusCode.REAL_BID_MISSING);
		    }
	        if (StringUtils.isBlank(targeting_id)) {
	            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_TARGETING_ID_IS_NULL);
	        }
	        if (StringUtils.isBlank(product_type) || StringUtils.isBlank(ProductType.valueOf(product_type).name())) {
	            ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
	        }
		}
		
		private void validateParamsByAdgroupId(Integer account_id,String accessToken,Integer adgroup_id){
			validateParamsByAccount(account_id, accessToken);
			if (adgroup_id == null || adgroup_id == 0) {
	            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_ID_IS_NULL);
	        }
		}
	

}
