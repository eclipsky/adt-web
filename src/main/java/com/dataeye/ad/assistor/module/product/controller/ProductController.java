package com.dataeye.ad.assistor.module.product.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccount;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountGroupVo;
import com.dataeye.ad.assistor.module.mediumaccount.service.MediumAccountService;
import com.dataeye.ad.assistor.module.product.constants.Constants;
import com.dataeye.ad.assistor.module.product.model.ProductSelectorVo;
import com.dataeye.ad.assistor.module.product.model.ProductVo;
import com.dataeye.ad.assistor.module.product.model.RealTimeDataVo;
import com.dataeye.ad.assistor.module.product.service.ProductService;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.AccountRole;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;

/**
 * Created by luzhuyou 2017/02/20
 * 产品管理控制器.
 */
@Controller
public class ProductController {
	
    private static final Logger logger = (Logger) LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;
    @Autowired
    private MediumAccountService mediumAccountService;

    /**
     * 查询产品列表信息
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/20
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/product/query.do")
    public Object query(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("查询产品列表信息");
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
        String productName = parameter.getParameter("productName");
        String productId = parameter.getParameter("productId");
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        
        List<ProductVo> result = new ArrayList<ProductVo>();
        List<ProductVo> productList = null;
        // 如果是开发工程师角色，只查询被授权的产品
        if(userInSession.getAccountRole() == AccountRole.ENGINEER) {
        	Map<Integer, String> authorizedProductMap = userInSession.getAuthorizedProductMap();
        	if(authorizedProductMap != null) {
        		Set<Integer> productIds = authorizedProductMap.keySet();
        		productList = productService.queryFromCache(companyId, productIds);
        	}
        } else {
        	productList = productService.queryFromCache(companyId);
        }
        
        if(productList != null) {
        	
        	for(ProductVo vo : productList) {
        		//如果没有传入查询参数，则所有的产品信息都保存
        		if((StringUtils.isEmpty(productName) && StringUtils.isBlank(productId)) || (!StringUtils.isBlank(productName) && vo.getProductName().indexOf(productName.trim()) != -1)){
        			List<MediumAccountGroupVo> mediumAccountList = new ArrayList<MediumAccountGroupVo>();
                	List<MediumAccount> responsibleMediumAccountList = mediumAccountService.getResponsibleFollowersGroupMedium(companyId, null,null,vo.getProductId());
                	if(responsibleMediumAccountList.size()>0){
                		mediumAccountList = mediumAccountService.getMediumAccountListGroupMedium(responsibleMediumAccountList);
                	}
                	vo.setMediumAccountList(mediumAccountList);
        			result.add(vo);
        			//根据产品id查询单个产品信息
        		}else if(!StringUtils.isBlank(productId) && vo.getProductId().intValue() == Integer.parseInt(productId)) {
        			ProductVo productGroup = productService.getMediumAccountInfoByProudctId(companyId, vo.getProductId());
        			String mediumAccountIds = "";
        			String mediumAccountNames = "";
        			if(productGroup != null){
        				mediumAccountIds = productGroup.getMediumAccountIds();
        				mediumAccountNames = productGroup.getMediumAccountNames();
        			}
        			vo.setMediumAccountIds(mediumAccountIds);
        			vo.setMediumAccountNames(mediumAccountNames);
        			result.add(vo);
        		}
        	}
        }
        PageData pageData = PageDataHelper.getPageDataByList(parameter.getPageSize(), parameter.getPageId(), result);
        return pageData;
    }
	
    /**
     * 新增产品信息
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/20
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/product/add.do")
    public Object add(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("新增产品信息.");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String productName = parameter.getParameter("productName");
        String appId = parameter.getParameter("appId");
        String shareRate = parameter.getParameter("shareRate");
        String status = parameter.getParameter("status");
        
        String osType = parameter.getParameter("osType");
        String parentCategoryId = parameter.getParameter("parentCategoryId");
        String childrenCategoryId = parameter.getParameter("childrenCategoryId");
        String mediumAccountIds = parameter.getParameter(DEParameter.Keys.MEDIUM_ACCOUNT_IDS);
        if (StringUtils.isBlank(mediumAccountIds)) {
        	mediumAccountIds = null;
        }
        if(StringUtils.isBlank(productName)) {
        	logger.error("产品名称不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.PROD_PRODUCT_NAME_IS_NULL);
        }
        if(StringUtils.isBlank(shareRate)) {
        	shareRate = Constants.DEFALUT_CP_SHARE_RATE +"";
        }
        if(Double.parseDouble(shareRate.trim()) > 1.00 || Double.parseDouble(shareRate.trim()) < 0) {
        	logger.error("CP分成率只能够在0和1之间，保留两位小数.");
        	ExceptionHandler.throwParameterException(StatusCode.PROD_CP_SHARE_RATE_RANGE_ERROR);
        }
        if(StringUtils.isBlank(status) || !StringUtils.isNumeric(status)) {
        	status = Constants.ProductStatus.NORMAL + "";
        }
        if(StringUtils.isBlank(parentCategoryId)) {
        	ExceptionHandler.throwParameterException(StatusCode.PROD_PARENT_CATEGORY_ID_IS_NULL);
        }
        if(StringUtils.isBlank(childrenCategoryId)) {
        	ExceptionHandler.throwParameterException(StatusCode.PROD_CHILDREN_CATEGORY_ID_IS_NULL);
        }
        if(StringUtils.isBlank(osType)) {
        	ExceptionHandler.throwParameterException(StatusCode.PROD_OS_TYPE_IS_NULL);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        
        // 校验产品名称是否已存在
        ProductVo account = productService.get(productName.trim(), companyId);
		if (account != null) {
			logger.error("产品名称已存在.");
			ExceptionHandler.throwParameterException(StatusCode.PROD_PRODUCT_NAME_EXISTS);
		}
		
		//获取新建产品的appid，从tracking 中获取appId
		//获取uid
		int uid = userInSession.getCompanyAccountUid();
        Integer childrencategory = Integer.parseInt(childrenCategoryId);
		appId = productService.getProductRefAppId(productName.trim(),childrencategory==0?Integer.parseInt(parentCategoryId):childrencategory, Integer.parseInt(osType),uid);
		if(StringUtils.isBlank(appId)) {
			ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ID_ERROR);
        }
		
		Double shareRateDouble = new BigDecimal(shareRate.trim()).setScale(2, RoundingMode.HALF_UP).doubleValue();
		// 新增产品信息
        int productId = productService.add(productName.trim(), shareRateDouble, Integer.parseInt(status), companyId, appId,Integer.parseInt(osType),Integer.parseInt(parentCategoryId),childrencategory,mediumAccountIds);
        // 刷新UserInSession
        SessionContainer.refreshUserInSession(request);
        return productId;
    }
    
    
    /**
     * 下拉框查询产品
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/23
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/product/queryProductForSelector.do")
    public Object queryProductForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        
        List<ProductSelectorVo> result = productService.queryForSelector(companyId, userInSession.getPermissionMediumAccountIds(), userInSession.getPermissionSystemAccounts());
        return result;
    }

	/**
     * 修改产品关联的trackingApp
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/02/20
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/product/modifyRefApp.do")
    public Object modifyRefApp(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("修改产品关联的trackingApp.");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String productId = parameter.getParameter("productId");
        String appId = parameter.getParameter("appId");
        String appName = parameter.getParameter("appName");
        if(StringUtils.isBlank(productId) || !StringUtils.isNumeric(productId)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        
        if(StringUtils.isBlank(appId)) {
        	appId = "";
        	appName = "";
        }
        
        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
		
        int result = productService.modifyRefApp(Integer.parseInt(productId.trim()), appId.trim(), appName.trim(), companyId);
        
        return result;
    }
    
    /**
     * 修改产品信息
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author ldj 2017/06/12
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/product/modify.do")
    public Object modify(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("修改产品信息.");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String productId = parameter.getParameter("productId");
        String productName = parameter.getParameter("productName");
        String shareRate = parameter.getParameter("shareRate");
        String status = parameter.getParameter("status");
        
        String parentCategoryId = parameter.getParameter("parentCategoryId");
        String childrenCategoryId = parameter.getParameter("childrenCategoryId");
        String mediumAccountIds = parameter.getParameter(DEParameter.Keys.MEDIUM_ACCOUNT_IDS);
        if (StringUtils.isBlank(mediumAccountIds)) {
        	mediumAccountIds = null;
        }
        if(StringUtils.isBlank(productId) || !StringUtils.isNumeric(productId)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        if(StringUtils.isBlank(productName)) {
        	logger.error("产品名称不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.PROD_PRODUCT_NAME_IS_NULL);
        }
        
        if(StringUtils.isBlank(shareRate)) {
        	shareRate = Constants.DEFALUT_CP_SHARE_RATE +"";
        }
        if(Double.parseDouble(shareRate.trim()) > 1.00 || Double.parseDouble(shareRate.trim()) < 0) {
        	logger.error("CP分成率只能够在0和1之间，保留两位小数.");
        	ExceptionHandler.throwParameterException(StatusCode.PROD_CP_SHARE_RATE_RANGE_ERROR);
        }
        
        if(StringUtils.isBlank(status) || !StringUtils.isNumeric(status)) {
        	status = Constants.ProductStatus.NORMAL + "";
        }
        
        if(StringUtils.isBlank(parentCategoryId)) {
        	ExceptionHandler.throwParameterException(StatusCode.PROD_PARENT_CATEGORY_ID_IS_NULL);
        }
        if(StringUtils.isBlank(childrenCategoryId)) {
        	ExceptionHandler.throwParameterException(StatusCode.PROD_CHILDREN_CATEGORY_ID_IS_NULL);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        // 校验产品名称是否已存在
        ProductVo account = productService.get(productName.trim(), companyId);
		if (account != null && account.getProductId() != Integer.parseInt(productId.trim())) {
			logger.error("产品名称已存在.");
			ExceptionHandler.throwParameterException(StatusCode.PROD_PRODUCT_NAME_EXISTS);
		}
		 // 校验产品名称是否已存在
        ProductVo oldProduct = productService.get(null, companyId, null, Integer.parseInt(productId.trim()));
		//同步tracking的app名称
		int uid = userInSession.getCompanyAccountUid();
        Integer childrencategory = Integer.parseInt(childrenCategoryId);
        if(oldProduct != null && oldProduct.getAppId() !=null ){
        	productService.modifyApp(oldProduct.getAppId(),productName.trim(),childrencategory==0?Integer.parseInt(parentCategoryId):childrencategory, oldProduct.getOsType(),uid);
        }
		
		Double shareRateDouble = new BigDecimal(shareRate.trim()).setScale(2, RoundingMode.HALF_UP).doubleValue();
		
        int result = productService.modify(Integer.parseInt(productId.trim()), productName, shareRateDouble, Integer.parseInt(status), companyId
        		,Integer.parseInt(parentCategoryId),Integer.parseInt(childrenCategoryId),mediumAccountIds);
        
        // 刷新UserInSession
        SessionContainer.refreshUserInSession(request);
        
        return result;
    }
    
    /**
     * 获取实时统计数据 （SDK接入测试）
     *@param appid
     *@author ldj 2017-06-15 
     *
     * */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/product/sdk-validate.do")
    public Object querySDKValidate(HttpServletRequest request, HttpServletResponse response) throws Exception  {
    	logger.info(" 获取实时统计数据 .");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String appId = parameter.getParameter("appId");
        if(StringUtils.isBlank(appId)) {
			ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ID_ERROR);
        }
        //获取uid
        int uid = SessionContainer.getCurrentUser(context.getSession()).getCompanyAccountUid();
  		
    	List<RealTimeDataVo> result = productService.getRealTimeInfo(appId,uid);
    	
    	return result;
	}
    
    /**
     * 下拉框查询产品
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author ldj 2017/02/23
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/product/queryProductForSelectorByResponsible.do")
    public Object queryProductForSelectorByResponsible(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        
        List<ProductSelectorVo> result = productService.queryForSelector(companyId, userInSession.getOptimizerPermissionMediumAccountIds(), userInSession.getPermissionSystemAccounts());
        return result;
    }
	
    
    
}
