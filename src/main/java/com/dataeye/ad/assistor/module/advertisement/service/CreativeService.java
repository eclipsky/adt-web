package com.dataeye.ad.assistor.module.advertisement.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ProductType;
import com.dataeye.ad.assistor.module.advertisement.model.AdCreativeRelationVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdcreativeVO;
import com.dataeye.ad.assistor.module.advertisement.model.MarketingApiAuth;
import com.dataeye.ad.assistor.module.advertisement.service.tencent.AdCreativeService;
/**
 * 广告创意
 * */
@Service("creativeService")
public class CreativeService {

	
	@Autowired
	private ApiTxAdCreativeService apiTxAdCreativeService;
	@Autowired
	private ApiTxAdGroupService apiTxAdGroupService;
	
	@Autowired
	private AdCreativeService adCreativeService;

	/**
	 *查询广告创意
	 * @param adGroupId
	 * @param mediumAccountId
	 * @return
	 */
	public List<AdcreativeVO> queryAdcreative(Integer adGroupId, Integer mediumAccountId) {
		List<AdcreativeVO> result = new ArrayList<AdcreativeVO>();
		//1.根据查询参数找到入库的广告创意关联关系列表
		List<AdCreativeRelationVo> adGroupRelationList = apiTxAdCreativeService.query(adGroupId, mediumAccountId);
		MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
		int mAccountId = 0;
		String accessToken = "";
		if(tAccountVo != null ){
			mAccountId = tAccountVo.getAccountId();
			accessToken = tAccountVo.getAccessToken();
		}
		
		for (AdCreativeRelationVo adRelationVo : adGroupRelationList) {
			Integer adcreativeId = adRelationVo.getAdcreativeId();
			List<AdcreativeVO> responseResult = adCreativeService.query(mAccountId, adcreativeId, accessToken);
			if(responseResult != null && responseResult.size() > 0){
				result.addAll(responseResult);
			}
		}
		return result;
	}
	
    /**
     * 创建广告创意
     * @param planGroupId 推广计划id
     * @param adcreativeName 广告创意名称
     * @param adcreativeTemplateId 创意规格ID
     * @param adcreativeElements 创意元素
     * @param destination_url 落地页 url
     * @param siteSet 投放站点集合
     * @param productType 标的物类型
     * @param deep_link 应用直达页 URL
     * @param product_refs_id 标的物 id
     * 
     * @param mediumAccount
     * @param mAccountId
     * @param accessToken
     * */
    public int add(Integer planGroupId,String adcreativeName,Integer adcreativeTemplateId,String adcreativeElements,String destination_url,String siteSet,
    		Integer productType,String deepLink,String productRefsId,Integer mediumAccountId,Integer mAccountId,String accessToken){
		if(mediumAccountId != null){
			MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
			if(tAccountVo != null){
				mAccountId = tAccountVo.getAccountId();
				accessToken = tAccountVo.getAccessToken();
			}
		}
		return adCreativeService.add(mAccountId, planGroupId, adcreativeName, adcreativeTemplateId, adcreativeElements, destination_url, siteSet, ProductType.parse(productType).name(), deepLink, productRefsId, accessToken);
	}
    
    /**
     * 更新广告创意
     * @param adcreative_id 创意id
     * @param adcreativeName 广告创意名称
     * @param adcreativeTemplateId 创意规格ID
     * @param adcreativeElements 创意元素
     * @param destination_url 落地页 url
     * @param siteSet 投放站点集合
     * @param productType 标的物类型
     * @param deep_link 应用直达页 URL
     * @param product_refs_id 标的物 id
     * 
     * @param mediumAccount
     * @param mAccountId
     * @param accessToken
     * */
    public int update(Integer adcreativeId,String adcreativeName,String adcreativeElements,String destination_url,String deepLink,Integer mediumAccountId,Integer mAccountId,String accessToken){
		if(mediumAccountId != null){
			MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
			if(tAccountVo != null){
				mAccountId = tAccountVo.getAccountId();
				accessToken = tAccountVo.getAccessToken();
			}
		}
		return adCreativeService.update(mAccountId, adcreativeId, adcreativeName, adcreativeElements, destination_url, deepLink, accessToken);
	}
    
    /**
     * 删除广告创意
     * @param adcreative_id 创意id
     * @param mediumAccount
     * @param mAccountId
     * @param accessToken
     * */
    public void delete(Integer adcreativeId,Integer mediumAccountId,Integer mAccountId,String accessToken){
		if(mediumAccountId != null){
			MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
			if(tAccountVo != null){
				mAccountId = tAccountVo.getAccountId();
				accessToken = tAccountVo.getAccessToken();
			}
		}
		adCreativeService.delete(mAccountId, adcreativeId, accessToken);
	}

}
