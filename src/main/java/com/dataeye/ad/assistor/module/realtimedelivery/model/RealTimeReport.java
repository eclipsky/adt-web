package com.dataeye.ad.assistor.module.realtimedelivery.model;

import java.math.BigDecimal;

/**
 * Created by huangzehai on 2017/2/27.
 */
public class RealTimeReport extends Time {
    /**
     * 计划ID.
     */
    private Integer planId;
    /**
     * 落地页ID
     */
    private Integer pageId;
    /**
     * 包ID.
     */
    private Integer packageId;
    /**
     * 消耗
     */
    private BigDecimal cost;
    /**
     * 曝光
     */
    private Integer exposures;
    /**
     * 点击数
     */
    private Integer clicks;
    /**
     * 点击通过率CTR
     */
    private Double ctr;
    /**
     * 到达数.
     */
    private Integer reaches;
    /**
     * 下载数
     */
    private Integer downloads;
    /**
     * 下载率
     */
    private Double downloadRate;
    /**
     * 下载单价
     */
    private BigDecimal costPerDownload;
    /**
     * 激活数
     */
    private Integer activation;
    /**
     * 激活率
     */
    private Double activationRate;
    /**
     * 激活CPA.
     */
    private BigDecimal activationCpa;
    /**
     * 首日注册设备数
     */
    private Integer firstDayRegistrations;
    /**
     * 注册率.
     */
    private Double registrationRateFd;
    /**
     * 首日注册设备CPA
     */
    private BigDecimal costPerRegistrationFd;


    private String planName;

    private String mediumName;

    private String responsibleAccounts;

    private String timeRange;

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Integer getExposures() {
        return exposures;
    }

    public void setExposures(Integer exposures) {
        this.exposures = exposures;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Double getCtr() {
        return ctr;
    }

    public void setCtr(Double ctr) {
        this.ctr = ctr;
    }

    public Integer getReaches() {
        return reaches;
    }

    public void setReaches(Integer reaches) {
        this.reaches = reaches;
    }

    public Integer getDownloads() {
        return downloads;
    }

    public void setDownloads(Integer downloads) {
        this.downloads = downloads;
    }

    public Double getDownloadRate() {
        return downloadRate;
    }

    public void setDownloadRate(Double downloadRate) {
        this.downloadRate = downloadRate;
    }

    public Integer getActivation() {
        return activation;
    }

    public void setActivation(Integer activation) {
        this.activation = activation;
    }

    public Double getActivationRate() {
        return activationRate;
    }

    public void setActivationRate(Double activationRate) {
        this.activationRate = activationRate;
    }

    public BigDecimal getActivationCpa() {
        return activationCpa;
    }

    public void setActivationCpa(BigDecimal activationCpa) {
        this.activationCpa = activationCpa;
    }

    public BigDecimal getCostPerDownload() {
        return costPerDownload;
    }

    public void setCostPerDownload(BigDecimal costPerDownload) {
        this.costPerDownload = costPerDownload;
    }

    public Integer getFirstDayRegistrations() {
        return firstDayRegistrations;
    }

    public void setFirstDayRegistrations(Integer firstDayRegistrations) {
        this.firstDayRegistrations = firstDayRegistrations;
    }

    public Double getRegistrationRateFd() {
        return registrationRateFd;
    }

    public void setRegistrationRateFd(Double registrationRateFd) {
        this.registrationRateFd = registrationRateFd;
    }

    public BigDecimal getCostPerRegistrationFd() {
        return costPerRegistrationFd;
    }

    public void setCostPerRegistrationFd(BigDecimal costPerRegistrationFd) {
        this.costPerRegistrationFd = costPerRegistrationFd;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getMediumName() {
        return mediumName;
    }

    public void setMediumName(String mediumName) {
        this.mediumName = mediumName;
    }

    public String getResponsibleAccounts() {
        return responsibleAccounts;
    }

    public void setResponsibleAccounts(String responsibleAccounts) {
        this.responsibleAccounts = responsibleAccounts;
    }

    public String getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(String timeRange) {
        this.timeRange = timeRange;
    }

    @Override
    public String toString() {
        return "RealTimeReport{" +
                "planId=" + planId +
                ", pageId=" + pageId +
                ", packageId=" + packageId +
                ", cost=" + cost +
                ", exposures=" + exposures +
                ", clicks=" + clicks +
                ", ctr=" + ctr +
                ", reaches=" + reaches +
                ", downloads=" + downloads +
                ", downloadRate=" + downloadRate +
                ", activation=" + activation +
                ", activationRate=" + activationRate +
                ", activationCpa=" + activationCpa +
                ", firstDayRegistrations=" + firstDayRegistrations +
                ", registrationRateFd=" + registrationRateFd +
                ", costPerRegistrationFd=" + costPerRegistrationFd +
                ", planName='" + planName + '\'' +
                ", mediumName='" + mediumName + '\'' +
                ", responsibleAccounts='" + responsibleAccounts + '\'' +
                ", timeRange='" + timeRange + '\'' +
                '}';
    }
}
