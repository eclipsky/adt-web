package com.dataeye.ad.assistor.module.result.model;

import com.dataeye.ad.assistor.module.report.model.DateRangeQuery;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.Date;

/**
 * Created by huangzehai on 2017/4/7.
 */
public class CoreIndicatorQuery extends DateRangeQuery {
    /**
     * 是否过滤自然流量
     */
    private boolean filterNaturalFlow;

    public boolean isFilterNaturalFlow() {
        return filterNaturalFlow;
    }

    public void setFilterNaturalFlow(boolean filterNaturalFlow) {
        this.filterNaturalFlow = filterNaturalFlow;
    }

    @Override
    public String toString() {
        return "CoreIndicatorQuery{" +
                "filterNaturalFlow=" + filterNaturalFlow +
                "} " + super.toString();
    }
}
