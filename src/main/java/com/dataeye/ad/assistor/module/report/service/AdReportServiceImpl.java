package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.Constant.MediumType;
import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlowQuery;
import com.dataeye.ad.assistor.module.naturalflow.service.NaturalFlowService;
import com.dataeye.ad.assistor.module.naturalflow.util.NaturalFlowUtils;
import com.dataeye.ad.assistor.module.report.constant.*;
import com.dataeye.ad.assistor.module.report.mapper.AdReportMapper;
import com.dataeye.ad.assistor.module.report.model.*;
import com.dataeye.ad.assistor.module.report.util.ExcelUtils;
import com.dataeye.ad.assistor.module.systemaccount.mapper.SystemAccountMapper;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.NumberUtils;
import com.dataeye.ad.assistor.util.StringUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.jruby.compiler.ir.operands.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.zip.CRC32;

/**
 * Created by huangzehai on 2016/12/27.
 */
@Service
public class AdReportServiceImpl implements AdReportService {

    private static final String SHEET_NAME = "report";

    @Autowired
    private AdReportMapper adReportMapper;

    @Autowired
    private SystemAccountMapper systemAccountMapper;

    @Autowired
    private NaturalFlowService naturalFlowService;

    /**
     * 投放日报-推广计划-按期间汇总
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public PageData planSummaryReport(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.ACCOUNT_AND_PLAN, Defaults.ASC);
        int totalCount = adReportMapper.planSummaryReportTotalCount(query);
        SystemAccountVo systemAccount = systemAccountMapper.getById(query.getAccountId());
        PageData pageData = PageDataHelper.getPageData(query.getPageSize(), query.getCurrentPage(), totalCount, Columns.getAuthorizedColumns(Columns.PLAN_REPORT, systemAccount.getIndicatorPermission()));
        List<AdReport> reports = null;
        String dateRange = DateUtils.dateRange(query.getStartDate(), query.getEndDate());
        if (totalCount > 0) {
            reports = adReportMapper.planSummaryReport(query);
            for (AdReport report : reports) {
                //Date link.
                Link dateLink = new Link();
                dateLink.setLabel(dateRange);
                dateLink.setValue(dateRange);
                dateLink.setLink(true);
                dateLink.setId(LinkID.Date);
                report.setDateLink(dateLink);

                //Account link
                Link accountLink = new Link();
                accountLink.setLabel(StringUtils.defaultIfEmpty(report.getAccountAlias(), report.getAccount()));
                accountLink.setValue(String.valueOf(report.getAccountId()));
                report.setAccountLink(accountLink);

                //Plan link;
                Link planLink = new Link();
                planLink.setLabel(report.getPlan());
                planLink.setValue(String.valueOf(report.getPlanId()));
                report.setPlanLink(planLink);
            }

        }
        //自然流量
        if (!query.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProduct(toNaturalFlowQuery(query));
            if (reports == null && naturalFlows != null) {
                reports = new ArrayList<>();
            }
            addNaturalFlowAsPlanItem(reports, naturalFlows, dateRange);
        }
        List<DeliveryReportView> deliveryReportViews = formatData(reports, query);
        pageData.setContent(deliveryReportViews);


        //设置汇总数据
        AdReport total = null;
        if (totalCount > 0) {
            total = adReportMapper.planSummaryReportTotal(query);
        }
        //自然流量
        if (!query.isFilterNaturalFlow()) {
            NaturalFlow naturalFlowTotal = naturalFlowService.getNaturalFlowTotal(toNaturalFlowQuery(query));
            total = addNaturalFlow(total, naturalFlowTotal);
        }
        setTotalLink(total, query);
        List<AdReport> totalList = new ArrayList<>();
        totalList.add(total);
        List<DeliveryReportView> deliveryReportTotal = formatData(totalList, query);
        pageData.setSumData(deliveryReportTotal.get(0));
        return pageData;
    }

    /**
     * Set link for total.
     *
     * @param total
     * @param query
     */
    private void setTotalLink(AdReport total, DeliveryReportQuery query) {
        if (total != null) {
            //Date link.
            Link dateLink = new Link();
            String dateRange = DateUtils.dateRange(query.getStartDate(), query.getEndDate());
            dateLink.setLabel(dateRange);
            dateLink.setValue(dateRange);
            dateLink.setLink(true);
            dateLink.setTotal(true);
            dateLink.setId(LinkID.Date);
            total.setDateLink(dateLink);

            //Account link
            Link totalLink = new Link();
            totalLink.setLabel(Labels.TOTAL);
            totalLink.setValue(Labels.TOTAL);
            totalLink.setTotal(true);
            total.setAccountLink(totalLink);
            //Plan link;
            total.setPlanLink(totalLink);
            //Medium link
            total.setMediumLink(totalLink);
        }
    }

    /**
     * 投放日报-推广计划-按期间汇总-详细（展开日期）
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public List<DeliveryReportView> planSummaryReportDetail(DeliveryReportQuery query) {
        List<AdReport> reports;
        if (query.isNaturalFlow()) {
            //自然流量
            reports = new ArrayList<>();
            NaturalFlowQuery naturalFlowQuery = toNaturalFlowQuery(query);
            naturalFlowQuery.setProductId(query.getPlanId());
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProductAndDate(naturalFlowQuery);
            addNaturalFlowAsPlanItem(reports, naturalFlows);
        } else {
            //投放日报
            setDefaultOrder(query, Defaults.DATE, Defaults.DESC);
            reports = adReportMapper.planSummaryReportDetail(query);
            for (AdReport report : reports) {
                //Date link.
                Link dateLink = new Link();
                dateLink.setLabel(report.getDate());
                dateLink.setValue(report.getDate());
                report.setDateLink(dateLink);

                //Account link
                Link accountLink = new Link();
                accountLink.setLabel(StringUtils.defaultIfEmpty(report.getAccountAlias(), report.getAccount()));
                accountLink.setValue(String.valueOf(report.getAccountId()));
                report.setAccountLink(accountLink);

                //Plan link;
                Link planLink = new Link();
                planLink.setLabel(report.getPlan());
                planLink.setValue(String.valueOf(report.getPlanId()));
                report.setPlanLink(planLink);
            }
        }

        //不过滤消耗为0的计划时补全日期
        if (!query.isIgnoreZeroCostPlan()) {
            dateCompletion(reports, query.getStartDate(), query.getEndDate(), query.getOrder());
            //日期补全后变成了按日期排序，重新按指定字段排序
            sort(reports, query.getOrderBy(), query.getOrder());
        }
        return formatData(reports, query);
    }

    /**
     * 投放日报-推广计划-按天汇总
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public PageData planDailyReport(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.DATE, Defaults.DESC);
        //数据库中存在的记录数
        int totalCount = adReportMapper.planDailyReportTotalCount(query);
        //日期补全后的记录数
        int totalRecords = totalCount;

        if (!query.isIgnoreZeroCostPlan()) {
            //通过查询日期范围计算日期补全后的记录数
            totalRecords = getTotalCount(query.getStartDate(), query.getEndDate());
            //如果是按日期排序,通过修正
            if (StringUtils.equalsIgnoreCase(query.getOrderBy(), Fields.DATE)) {
                //按日期排序时，通过修正查询日期的起止日期来获取相应分页的数据
                Date pagingStartDate = getPagingStartDate(query.getStartDate(), query.getEndDate(), query.getCurrentPage(), query.getPageSize());
                Date pagingEndDate = getPagingEndDate(query.getEndDate(), query.getCurrentPage(), query.getPageSize());
                query.setStartDate(pagingStartDate);
                query.setEndDate(pagingEndDate);
                query.setNotPaging(true);
            } else {
                //按非日期字段排序，一次获取所有数据，补全数据后，在应用层分页
                query.setNotPaging(true);
            }
        }

        SystemAccountVo systemAccount = systemAccountMapper.getById(query.getAccountId());
        PageData pageData = PageDataHelper.getPageData(query.getPageSize(), query.getCurrentPage(), totalRecords, Columns.getAuthorizedColumns(Columns.PLAN_REPORT, systemAccount.getIndicatorPermission()));
        List<AdReport> reports = null;
        if (totalCount > 0) {
            if (query.isFilterNaturalFlow()) {
                reports = adReportMapper.planDailyReport(query);
            } else {
                reports = adReportMapper.planDailyReportWithNaturalFlow(query);
            }

            for (AdReport report : reports) {
                //Date link.
                Link dateLink = new Link();
                dateLink.setLabel(report.getDate());
                dateLink.setValue(report.getDate());
                report.setDateLink(dateLink);

                //Account link
                Link accountLink = new Link();
                accountLink.setLabel(Constants.UNKNOWN);
                accountLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
                report.setAccountLink(accountLink);

                //Plan link;
                Link planLink = new Link();
                planLink.setLabel(Constants.TOTAL);
                planLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
                planLink.setLink(true);
                planLink.setId(LinkID.Plan);
                report.setPlanLink(planLink);
            }
        }

        //不过滤消耗为0的计划时补全日期
        if (!query.isIgnoreZeroCostPlan()) {
            dateCompletion(reports, query.getStartDate(), query.getEndDate(), query.getOrder(), TableType.PlanDaily);
            //日期补全后变成了按日期排序，重新按指定字段排序
            sort(reports, query.getOrderBy(), query.getOrder());
            //如果不是按日期字段排序，列表数据需要修正为当前分页数据
            if (!StringUtils.equalsIgnoreCase(query.getOrderBy(), Fields.DATE)) {
                reports = getPagingData(reports, query.getCurrentPage(), query.getPageSize());
            }
        }

        List<DeliveryReportView> deliveryReportViews = formatData(reports, query);
        pageData.setContent(deliveryReportViews);
        return pageData;
    }

    /**
     * 获取当前分页数据
     *
     * @param list
     * @param pageNumber
     * @param pageSize
     * @param <T>
     * @return
     */
    private <T> List<T> getPagingData(List<T> list, int pageNumber, int pageSize) {
        if (list == null) {
            return null;
        }
        int fromIndex = pageSize * (pageNumber - 1);
        //防止前端传递参数不正确
        if (fromIndex >= list.size()) {
            fromIndex = 0;
        }
        int toIndex = pageSize * pageNumber;
        toIndex = Math.min(toIndex, list.size());
        return list.subList(fromIndex, toIndex);
    }

    /**
     * 添加自然流量到汇总结果
     *
     * @param report
     * @param naturalFlow
     */
    private AdReport addNaturalFlow(AdReport report, NaturalFlow naturalFlow) {
        if (naturalFlow == null) {
            return report;
        }

        if (report == null) {
            report = new AdReport();

            if (naturalFlow.getStatDate() != null) {
                String date = DateUtils.format(naturalFlow.getStatDate());
                Link dateLink = new Link();
                dateLink.setLabel(date);
                dateLink.setValue(date);
                report.setDateLink(dateLink);
                report.setDate(date);
            }

            //Medium link
            Link mediumLink = new Link();
            mediumLink.setLabel(Constants.TOTAL);
            mediumLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
            mediumLink.setLink(true);
            mediumLink.setId(LinkID.DailyMediumTotal);
            report.setMediumLink(mediumLink);

            //Account link
            Link accountLink = new Link();
            accountLink.setLabel(Constants.UNKNOWN);
            accountLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
            report.setAccountLink(accountLink);

            //Plan link;
            Link planLink = new Link();
            planLink.setLabel(Constants.TOTAL);
            planLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
            planLink.setLink(true);
            planLink.setId(LinkID.Plan);
            report.setPlanLink(planLink);
        }

        //激活数
        report.setActivations(NumberUtils.add(report.getActivations(), naturalFlow.getActivations()));
        //注册数
        report.setRegistrations(NumberUtils.add(report.getRegistrations(), naturalFlow.getRegistrations()));
        //DAU
        report.setDau((NumberUtils.add(report.getDau(), naturalFlow.getDau())));
        //总充值用户数
        report.setTotalNumberOfRecharge(NumberUtils.add(report.getTotalNumberOfRecharge(), naturalFlow.getTotalPayingUsers()));
        //总充值
        report.setTotalRecharges(NumberUtils.add(report.getTotalRecharges(), naturalFlow.getTotalPayingAmount()));
        //累计充值
        report.setAccumulatedRecharge(NumberUtils.add(report.getAccumulatedRecharge(), naturalFlow.getAccumulatedPayingAmount()));
        //新增充值金额充值金额
        report.setNewlyIncreasedRechargeAmount(NumberUtils.add(report.getNewlyIncreasedRechargeAmount(), naturalFlow.getNewlyIncreasedRechargeAmount()));
        //首日充值金额
        report.setPayAmount1Day(NumberUtils.add(report.getPayAmount1Day(), naturalFlow.getPayAmount1day()));
        //7日充值金额
        report.setPayAmount7Days(NumberUtils.add(report.getPayAmount7Days(), naturalFlow.getPayAmount7days()));
        //30日充值金额
        report.setPayAmount30Days(NumberUtils.add(report.getPayAmount30Days(), naturalFlow.getPayAmount30days()));
        report.setRegisterAccountNum(NumberUtils.add(report.getRegisterAccountNum(), naturalFlow.getRegisterAccountNum()));
        report.setFirstDayRegisterAccountNum(NumberUtils.add(report.getFirstDayRegisterAccountNum(), naturalFlow.getFirstDayRegisterAccountNum()));
        report.setFirstDayRegistrations(NumberUtils.add(report.getFirstDayRegistrations(), naturalFlow.getFirstDayRegistrations()));
        
        report.setRegisterAccountNum(NumberUtils.add(report.getRegisterAccountNum(), naturalFlow.getRegisterAccountNum()));
        report.setRegisterAccountNumD3(NumberUtils.add(report.getRegisterAccountNumD3(), naturalFlow.getRegisterAccountNumD3()));
        report.setRegisterAccountNumD7(NumberUtils.add(report.getRegisterAccountNumD7(), naturalFlow.getRegisterAccountNumD7()));
        report.setRegisterAccountNumD15(NumberUtils.add(report.getRegisterAccountNumD15(), naturalFlow.getRegisterAccountNumD15()));
        report.setRegisterAccountNumD30(NumberUtils.add(report.getRegisterAccountNumD30(), naturalFlow.getRegisterAccountNumD30()));
        
      //首日LTV
      report.setLtv1Day(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount1Day()), report.getRegisterAccountNum()));
      //7日LTV
      report.setLtv7Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount7Days()), report.getRegisterAccountNumD7()));
      //30日LTV
      report.setLtv30Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount30Days()), report.getRegisterAccountNumD30()));
      
      report.setRetainD1(NumberUtils.add(report.getRetainD1(), naturalFlow.getRetainD1()));
      report.setRetainD3(NumberUtils.add(report.getRetainD3(), naturalFlow.getRetainD3()));
      report.setRetainD14(NumberUtils.add(report.getRetainD14(), naturalFlow.getRetainD14()));
      report.setRetainD30(NumberUtils.add(report.getRetainD30(), naturalFlow.getRetainD30()));
      
      report.setNextDayRetentionRate(NumberUtils.rate(report.getRetainD1(), report.getRegisterAccountNum()));
      report.setThreeDayRetentionRate(NumberUtils.rate(report.getRetainD3(), report.getRegisterAccountNumD3()));
      report.setSevenDayRetentionRate(NumberUtils.rate(report.getRetainD7(), report.getRegisterAccountNumD7()));
      report.setFifteenDayRetentionRate(NumberUtils.rate(report.getRetainD14(), report.getRegisterAccountNumD15()));
      report.setThirtyDayRetentionRate(NumberUtils.rate(report.getRetainD30(), report.getRegisterAccountNumD30()));
        //修正添加自然流量后的比率
        recomputeRate(report);

        return report;
    }

    /**
     * 添加自然流量为媒体项
     *
     * @param reports
     * @param naturalFlows
     * @param dateRange
     */
    private void addNaturalFlowAsMediumItem(List<AdReport> reports, List<NaturalFlow> naturalFlows, String dateRange) {
        addNaturalFlowAsItem(reports, naturalFlows, dateRange, false);
    }

    /**
     * 添加自然流量为子项
     *
     * @param reports
     * @param naturalFlows
     * @param dateRange
     */
    private void addNaturalFlowAsPlanItem(List<AdReport> reports, List<NaturalFlow> naturalFlows, String dateRange) {
        addNaturalFlowAsItem(reports, naturalFlows, dateRange, true);
    }

    /**
     * 添加自然流量为子项
     *
     * @param reports
     * @param naturalFlows
     */
    private void addNaturalFlowAsMediumItem(List<AdReport> reports, List<NaturalFlow> naturalFlows) {
        addNaturalFlowAsItem(reports, naturalFlows, null, false);
    }

    /**
     * 添加自然流量为子项
     *
     * @param reports
     * @param naturalFlows
     */
    private void addNaturalFlowAsPlanItem(List<AdReport> reports, List<NaturalFlow> naturalFlows) {
        addNaturalFlowAsItem(reports, naturalFlows, null, true);
    }

    /**
     * 将自然流量添加到计划|媒体列表.
     *
     * @param reports
     * @param naturalFlows
     * @param dateRange    日期范围
     */
    private void addNaturalFlowAsItem(List<AdReport> reports, List<NaturalFlow> naturalFlows, String dateRange, boolean isPlan) {
        if (naturalFlows != null && !naturalFlows.isEmpty()) {
            List<AdReport> naturalFlowReports = new ArrayList<>();
            for (NaturalFlow naturalFlow : naturalFlows) {
                AdReport report = new AdReport();
                Link dateLink = new Link();
                if (StringUtils.isBlank(dateRange)) {
                    dateLink.setValue(DateUtils.format(naturalFlow.getStatDate()));
                    dateLink.setLabel(dateLink.getValue());
                } else {
                    dateLink.setValue(dateRange);
                    dateLink.setLabel(dateRange);
                    dateLink.setLink(true);
                    dateLink.setId(LinkID.Date);
                }

                report.setDateLink(dateLink);
                Link planLink = new Link();
                planLink.setLabel(NaturalFlowUtils.formatProductName(naturalFlow.getProductName()));
                planLink.setValue(String.valueOf(naturalFlow.getProductId()));
                if (isPlan) {
                    report.setPlanLink(planLink);
                } else {
                    report.setMediumLink(planLink);
                }
                //将自然流量的产品ID设置到计划ID
                report.setPlanId(naturalFlow.getProductId());
                //统计日期
                report.setDate(DateUtils.format(naturalFlow.getStatDate()));
                //激活数
                report.setActivations(naturalFlow.getActivations());
                //激活注册率
                report.setActivationRegistrationRate(NumberUtils.rate(naturalFlow.getRegistrations(), naturalFlow.getActivations()));
                //注册数
                report.setRegistrations(naturalFlow.getRegistrations());
                //总充值用户数
                report.setTotalNumberOfRecharge(naturalFlow.getTotalPayingUsers());
                //DAU
                report.setDau(naturalFlow.getDau());
                
                //首日充值金额
                report.setNewlyIncreasedRechargeAmount(naturalFlow.getNewlyIncreasedRechargeAmount());
                //首日充值账号数
                //report.setFirstDayRegisterAccountNum(firstDayRegisterAccountNum);
                //付费率
                report.setPayRate(NumberUtils.rate(naturalFlow.getTotalPayingUsers(), naturalFlow.getDau()));
                //总充值
                report.setTotalRecharges(naturalFlow.getTotalPayingAmount());
                //累计充值
                report.setAccumulatedRecharge(naturalFlow.getAccumulatedPayingAmount());
                //首日充值
                report.setPayAmount1Day(naturalFlow.getPayAmount1day());
                //7日充值
                report.setPayAmount7Days(naturalFlow.getPayAmount7days());
                //30日充值
                report.setPayAmount30Days(naturalFlow.getPayAmount30days());
                //注册账号数
                report.setRegisterAccountNum(naturalFlow.getRegisterAccountNum());
                //留存率
                report.setNextDayRetentionRate(naturalFlow.getNextDayRetentionRate());
                report.setThreeDayRetentionRate(naturalFlow.getThreeDayRetentionRate());
                report.setSevenDayRetentionRate(naturalFlow.getSevenDayRetentionRate());
                report.setFifteenDayRetentionRate(naturalFlow.getFifteenDayRetentionRate());
                report.setThirtyDayRetentionRate(naturalFlow.getThirtyDayRetentionRate());
                report.setFirstDayRegisterAccountNum(naturalFlow.getFirstDayRegisterAccountNum());
                report.setFirstDayRegistrations(naturalFlow.getFirstDayRegistrations());
                report.setLtv1Day(naturalFlow.getLtv1Day());
                report.setLtv7Days(naturalFlow.getLtv7Days());
                report.setLtv30Days(naturalFlow.getLtv30Days());
                //自然流量标志位
                report.setNaturalFlow(true);
                naturalFlowReports.add(report);

            }
            //将自然流量放到投放日报的前面
            reports.addAll(0, naturalFlowReports);
        }
    }

    /**
     * 添加自然流量后需要重新计算以修正某些比率
     */
    private void recomputeRate(AdReport report) {
        //激活率
        report.setActivationRate(NumberUtils.rate(report.getActivations(), report.getTotalClicks()));

        //激活CPA
        report.setActivationCpa(NumberUtils.cpa(report.getTotalCost(), report.getActivations()));

        //激活注册率
        report.setActivationRegistrationRate(NumberUtils.rate(report.getFirstDayRegistrations(), report.getActivations()));
        
        //注册率
        report.setRegistrationRate(NumberUtils.rate(report.getFirstDayRegistrations(), report.getTotalClicks()));

        //注册CPA
        report.setCostPerRegistration(NumberUtils.cpa(report.getTotalCost(), report.getFirstDayRegistrations()));

        //付费率
        report.setPayRate(NumberUtils.rate(report.getTotalNumberOfRecharge(), report.getDau()));

        //首日回本率
        report.setFirstDayPaybackRate(NumberUtils.rate(report.getNewlyIncreasedRechargeAmount(), report.getTotalCost()));

      //注册CPA(帐号)
        report.setRegistrationAccountNumCpa(NumberUtils.cpa(report.getTotalCost(), report.getFirstDayRegisterAccountNum()));
//        //首日LTV
//        report.setLtv1Day(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount1Day()), report.getRegistrations()));
//        //7日LTV
//        report.setLtv7Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount7Days()), report.getRegistrations()));
//        //30日LTV
//        report.setLtv30Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount30Days()), report.getRegistrations()));
    }


    /**
     * 按日期分组.
     *
     * @param naturalFlows
     * @return
     */
    private Map<String, NaturalFlow> groupByDate(List<NaturalFlow> naturalFlows) {
        Map<String, NaturalFlow> naturalFlowMap = new HashMap<>();
        if (naturalFlows != null) {
            for (NaturalFlow naturalFlow : naturalFlows) {
                naturalFlowMap.put(DateUtils.format(naturalFlow.getStatDate()), naturalFlow);
            }
        }
        return naturalFlowMap;
    }

    /**
     * 按日期分组统计AdReport.
     *
     * @param adReports
     * @return
     */
    private Map<String, AdReport> groupAdReportByDate(List<AdReport> adReports) {
        //按日期倒序排序
        Map<String, AdReport> map = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String date1, String date2) {
                return date2.compareTo(date1);
            }
        });
        if (adReports != null) {
            for (AdReport adReport : adReports) {
                map.put(adReport.getDate(), adReport);
            }
        }
        return map;
    }

    /**
     * 投放日报-推广计划-按天汇总-详细
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public List<DeliveryReportView> planDailyReportDetail(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.ACCOUNT_AND_PLAN, Defaults.ASC);
        List<AdReport> reports = adReportMapper.planDailyReportDetail(query);
        for (AdReport report : reports) {
            //Date link.
            Link dateLink = new Link();
            dateLink.setLabel(report.getDate());
            dateLink.setValue(report.getDate());
            report.setDateLink(dateLink);

            //Account link
            Link accountLink = new Link();
            accountLink.setLabel(StringUtils.defaultIfEmpty(report.getAccountAlias(), report.getAccount()));
            accountLink.setValue(String.valueOf(report.getAccountId()));
            report.setAccountLink(accountLink);

            //Plan link;
            Link planLink = new Link();
            planLink.setLabel(report.getPlan());
            planLink.setValue(String.valueOf(report.getPlanId()));
            report.setPlanLink(planLink);
        }

        //自然流量
        if (!query.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProductAndDate(toNaturalFlowQuery(query));
            if (reports == null) {
                reports = new ArrayList<>();
            }
            addNaturalFlowAsPlanItem(reports, naturalFlows);
        }
        return formatData(reports, query);
    }

    /**
     * 转化为自然流量查询
     *
     * @param query
     * @return
     */
    private NaturalFlowQuery toNaturalFlowQuery(DeliveryReportQuery query) {
        NaturalFlowQuery naturalFlowQuery = new NaturalFlowQuery();
        naturalFlowQuery.setStartDate(query.getStartDate());
        naturalFlowQuery.setEndDate(query.getEndDate());
        naturalFlowQuery.setProductIds(query.getProductIds());
        naturalFlowQuery.setCompanyId(query.getCompanyId());
        naturalFlowQuery.setPermissionMediumAccountIds(query.getPermissionMediumAccountIds());
        naturalFlowQuery.setPermissionSystemAccounts(query.getPermissionSystemAccounts());
        return naturalFlowQuery;
    }

    @Override
    public PageData mediumDailyReport(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.DATE, Defaults.DESC);
        int totalCount = adReportMapper.mediumDailyReportTotalCount(query);
        int totalRecords = totalCount;
        if (!query.isIgnoreZeroCostPlan()) {
            //通过查询日期范围计算日期补全后的记录数
            totalRecords = getTotalCount(query.getStartDate(), query.getEndDate());
            //如果是按日期排序,通过修正
            if (StringUtils.equalsIgnoreCase(query.getOrderBy(), Fields.DATE)) {
                //按日期排序时，通过修正查询日期的起止日期来获取相应分页的数据
                Date pagingStartDate = getPagingStartDate(query.getStartDate(), query.getEndDate(), query.getCurrentPage(), query.getPageSize());
                Date pagingEndDate = getPagingEndDate(query.getEndDate(), query.getCurrentPage(), query.getPageSize());
                query.setStartDate(pagingStartDate);
                query.setEndDate(pagingEndDate);
                query.setNotPaging(true);
            } else {
                //按非日期字段排序，一次获取所有数据，补全数据后，在应用层分页
                query.setNotPaging(true);
            }
        }

        SystemAccountVo systemAccount = systemAccountMapper.getById(query.getAccountId());
        PageData pageData = PageDataHelper.getPageData(query.getPageSize(), query.getCurrentPage(), totalRecords, Columns.getAuthorizedColumns(Columns.MEDIUM_REPORT, systemAccount.getIndicatorPermission()));
        List<AdReport> reports = null;
        if (totalCount > 0) {
            if (query.isFilterNaturalFlow()) {
                reports = adReportMapper.mediumDailyReport(query);
            } else {
                reports = adReportMapper.mediumDailyReportWithNaturalFlow(query);
            }
            for (AdReport report : reports) {
                //Date link.
                Link dateLink = new Link();
                dateLink.setLabel(report.getDate());
                dateLink.setValue(report.getDate());
                report.setDateLink(dateLink);

                //Medium link
                Link mediumLink = new Link();
                mediumLink.setLabel(Constants.TOTAL);
                mediumLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
                mediumLink.setLink(true);
                mediumLink.setId(LinkID.DailyMediumTotal);
                report.setMediumLink(mediumLink);

                //Account link
                Link accountLink = new Link();
                accountLink.setLabel(Constants.UNKNOWN);
                accountLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
                report.setAccountLink(accountLink);
            }
        }

        //不过滤消耗为0的计划时补全日期
        if (!query.isIgnoreZeroCostPlan()) {
            dateCompletion(reports, query.getStartDate(), query.getEndDate(), query.getOrder(), TableType.MediumDaily);
            //日期补全后变成了按日期排序，重新按指定字段排序
            sort(reports, query.getOrderBy(), query.getOrder());
            //如果不是按日期字段排序，列表数据需要修正为当前分页数据
            if (!StringUtils.equalsIgnoreCase(query.getOrderBy(), Fields.DATE)) {
                reports = getPagingData(reports, query.getCurrentPage(), query.getPageSize());
            }
        }

        List<DeliveryReportView> deliveryReportViews = formatData(reports, query);
        pageData.setContent(deliveryReportViews);
        return pageData;
    }

    /**
     * 投放日报-媒体维度-按天显示数据-展开媒体
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public List<DeliveryReportView> mediumDailyReportExpandMedium(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.TOTAL_COST, Defaults.DESC);
        List<AdReport> reports = adReportMapper.mediumDailyReportExpandMedium(query);
        for (AdReport report : reports) {
            //Date link.
            Link dateLink = new Link();
            dateLink.setLabel(report.getDate());
            dateLink.setValue(report.getDate());
            report.setDateLink(dateLink);

            //Medium link
            Link mediumLink = new Link();
            mediumLink.setLabel(report.getMedium());
            mediumLink.setValue(String.valueOf(report.getMediumId()));
            mediumLink.setLink(true);
            mediumLink.setId(LinkID.DailyMedium);
            report.setMediumLink(mediumLink);

            //Account link
            Link accountLink = new Link();
            accountLink.setLabel(Constants.UNKNOWN);
            accountLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
            report.setAccountLink(accountLink);
        }

        //自然流量
        if (!query.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProductAndDate(toNaturalFlowQuery(query));
            if (reports == null) {
                reports = new ArrayList<>();
            }
            addNaturalFlowAsMediumItem(reports, naturalFlows);
        }
        return formatData(reports, query);
    }

    /**
     * 投放日报-媒体维度-按天显示数据-展开账号.
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public List<DeliveryReportView> mediumDailyReportExpandEventGroup(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.ACCOUNT, Defaults.ASC);
        List<AdReport> reports = adReportMapper.mediumDailyReportExpandEventGroup(query);
        for (AdReport report : reports) {
            //Date link.
            Link dateLink = new Link();
            dateLink.setLabel(report.getDate());
            dateLink.setValue(report.getDate());
            report.setDateLink(dateLink);

            //Medium link
            Link mediumLink = new Link();
            mediumLink.setLabel(report.getMedium());
            mediumLink.setValue(String.valueOf(report.getMediumId()));
            report.setMediumLink(mediumLink);

            //Account link
            Link accountLink = new Link();
            accountLink.setLabel(StringUtils.defaultIfEmpty(report.getAccountAlias(), report.getAccount()));
            accountLink.setValue(String.valueOf(report.getAccountId()));
            accountLink.setLink(true);
            accountLink.setPopup(true);
            accountLink.setId(LinkID.DailyAccount);
            report.setAccountLink(accountLink);
        }
        return formatData(reports, query);
    }


    /**
     * 投放日报-媒体维度-按天显示数据-展开账号计划。
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public InnerTable mediumDailyReportExpandAccountAndPlan(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.PLAN, Defaults.ASC);

        List<AdReport> reports = adReportMapper.mediumDailyReportExpandAccountAndPlan(query);
        for (AdReport report : reports) {
            //Account link
//            Link accountLink = new Link();
//            accountLink.setLabel(StringUtils.defaultIfEmpty(report.getAccountAlias(), report.getAccount()));
//            accountLink.setValue(String.valueOf(report.getAccountId()));
//            report.setAccountLink(accountLink);

            //Plan link;
            Link planLink = new Link();
            planLink.setLabel(report.getPlan());
            planLink.setValue(String.valueOf(report.getPlanId()));
            report.setPlanLink(planLink);
        }
        List<DeliveryReportView> deliveryReportViews = formatData(reports, query);
        SystemAccountVo systemAccount = systemAccountMapper.getById(query.getAccountId());
        InnerTable table = new InnerTable();
        table.setColumn(Columns.getAuthorizedColumns(Columns.MEDIUM_DETAIL_REPORT, systemAccount.getIndicatorPermission()));
        table.setContent(deliveryReportViews);
        return table;
    }

    /**
     * 投放日报-媒体维度-汇总数据
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public PageData mediumSummaryReport(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.TOTAL_COST, Defaults.DESC);
        int totalCount = adReportMapper.mediumSummaryReportTotalCount(query);
        SystemAccountVo systemAccount = systemAccountMapper.getById(query.getAccountId());
        PageData pageData = PageDataHelper.getPageData(query.getPageSize(), query.getCurrentPage(), totalCount, Columns.getAuthorizedColumns(Columns.MEDIUM_REPORT, systemAccount.getIndicatorPermission()));
        List<AdReport> reports = null;
        if (totalCount > 0) {
            String dateRange = DateUtils.dateRange(query.getStartDate(), query.getEndDate());
            reports = adReportMapper.mediumSummaryReport(query);
            for (AdReport report : reports) {
                //Date link.
                Link dateLink = new Link();
                dateLink.setLabel(dateRange);
                dateLink.setValue(dateRange);
                dateLink.setLink(true);
                dateLink.setId(LinkID.Date);
                report.setDateLink(dateLink);

                //Medium link
                Link mediumLink = new Link();
                mediumLink.setLabel(report.getMedium());
                mediumLink.setValue(String.valueOf(report.getMediumId()));
                mediumLink.setLink(true);
                mediumLink.setId(LinkID.Medium);
                report.setMediumLink(mediumLink);

//            //Account link
                Link accountLink = new Link();
                accountLink.setLabel(Constants.UNKNOWN);
                accountLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
                report.setAccountLink(accountLink);
            }
        }

        //自然流量
        if (!query.isFilterNaturalFlow()) {
            String dateRange = DateUtils.dateRange(query.getStartDate(), query.getEndDate());
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProduct(toNaturalFlowQuery(query));
            if (reports == null && naturalFlows != null) {
                reports = new ArrayList<>();
            }
            addNaturalFlowAsMediumItem(reports, naturalFlows, dateRange);
        }
        List<DeliveryReportView> deliveryReportViews = formatData(reports, query);
        pageData.setContent(deliveryReportViews);

        //设置汇总数据
        AdReport total = null;
        if (totalCount > 0) {
            total = adReportMapper.mediumSummaryReportTotal(query);
        }

        //添加自然流量到汇总
        if (!query.isFilterNaturalFlow()) {
            NaturalFlow naturalFlowTotal = naturalFlowService.getNaturalFlowTotal(toNaturalFlowQuery(query));
            total = addNaturalFlow(total, naturalFlowTotal);
        }
        setTotalLink(total, query);
        List<AdReport> totalList = new ArrayList<>();
        totalList.add(total);
        List<DeliveryReportView> deliveryReportTotal = formatData(totalList, query);
        pageData.setSumData(deliveryReportTotal.get(0));
        return pageData;
    }

    /**
     * 投放日报-媒体维度-汇总数据-展开日期
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public List<DeliveryReportView> mediumSummaryReportExpandDate(DeliveryReportQuery query) {
        List<AdReport> reports;
        if (query.isNaturalFlow()) {
            //自然流量
            reports = new ArrayList<>();
            NaturalFlowQuery naturalFlowQuery = toNaturalFlowQuery(query);
            if (query.getMediumIds() != null && !query.getMediumIds().isEmpty()) {
                //此时媒体ID列表只有一个媒体ID
                naturalFlowQuery.setProductId(query.getMediumIds().get(0));
            }
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProductAndDate(naturalFlowQuery);
            addNaturalFlowAsMediumItem(reports, naturalFlows);
        } else {
            setDefaultOrder(query, Defaults.DATE, Defaults.DESC);
            reports = adReportMapper.mediumSummaryReportExpandDate(query);
            for (AdReport report : reports) {
                //Date link.
                Link dateLink = new Link();
                dateLink.setLabel(report.getDate());
                dateLink.setValue(report.getDate());
                report.setDateLink(dateLink);

                //Medium link
                Link mediumLink = new Link();
                mediumLink.setLabel(report.getMedium());
                mediumLink.setValue(String.valueOf(report.getMediumId()));
                mediumLink.setLink(true);
                mediumLink.setId(LinkID.DateMedium);
                report.setMediumLink(mediumLink);

                //Account link
                Link accountLink = new Link();
                accountLink.setLabel(Constants.UNKNOWN);
                accountLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
                report.setAccountLink(accountLink);
            }
        }
        //不过滤消耗为0的计划时补全日期
        if (!query.isIgnoreZeroCostPlan()) {
            dateCompletion(reports, query.getStartDate(), query.getEndDate(), query.getOrder());
            //日期补全后变成了按日期排序，重新按指定字段排序
            sort(reports, query.getOrderBy(), query.getOrder());
        }
        return formatData(reports, query);
    }


    /**
     * 投放日报-媒体维度-汇总数据-展开活动组
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public List<DeliveryReportView> mediumSummaryReportExpandEventGroup(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.ACCOUNT, Defaults.ASC);
        List<AdReport> reports = adReportMapper.mediumSummaryReportExpandEventGroup(query);
        for (AdReport report : reports) {
            //Date link.
            Link dateLink = new Link();
            dateLink.setLabel(report.getDate());
            dateLink.setValue(report.getDate());
            report.setDateLink(dateLink);

            //Medium link
            Link mediumLink = new Link();
            mediumLink.setLabel(report.getMedium());
            mediumLink.setValue(String.valueOf(report.getMediumId()));
            report.setMediumLink(mediumLink);

            //Account link
            Link accountLink = new Link();
            accountLink.setLabel(StringUtils.defaultIfEmpty(report.getAccountAlias(), report.getAccount()));
            accountLink.setValue(String.valueOf(report.getAccountId()));
            accountLink.setLink(true);
            accountLink.setPopup(true);
            accountLink.setId(LinkID.DateAccount);
            report.setAccountLink(accountLink);
        }
        return formatData(reports, query);
    }

    /**
     * 投放日报-媒体维度-汇总数据-展开账号和计划
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public InnerTable mediumSummaryReportExpandAccountAndPlan(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.PLAN, Defaults.ASC);
        List<AdReport> reports = adReportMapper.mediumSummaryReportExpandAccountAndPlan(query);
        for (AdReport report : reports) {
            //Account link
//            Link accountLink = new Link();
//            accountLink.setLabel(StringUtils.defaultIfEmpty(report.getAccountAlias(), report.getAccount()));
//            accountLink.setValue(String.valueOf(report.getAccountId()));
//            report.setAccountLink(accountLink);

            //Plan link;
            Link planLink = new Link();
            planLink.setLabel(report.getPlan());
            planLink.setValue(String.valueOf(report.getPlanId()));
            report.setPlanLink(planLink);
        }
        InnerTable table = new InnerTable();
        SystemAccountVo systemAccount = systemAccountMapper.getById(query.getAccountId());
        table.setColumn(Columns.getAuthorizedColumns(Columns.MEDIUM_DETAIL_REPORT, systemAccount.getIndicatorPermission()));
        List<DeliveryReportView> deliveryReportViews = formatData(reports, query);
        table.setContent(deliveryReportViews);
        return table;
    }

    /**
     * 投放日报-媒体维度-汇总数据-展开活动组-不展开日期
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public List<DeliveryReportView> mediumSummaryReportExpandEventGroupNotDate(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.ACCOUNT, Defaults.ASC);
        List<AdReport> reports = adReportMapper.mediumSummaryReportExpandEventGroupNotDate(query);
        for (AdReport report : reports) {
            //Date link.
            Link dateLink = new Link();
            String dateRange = DateUtils.dateRange(query.getStartDate(), query.getEndDate());
            dateLink.setLabel(dateRange);
            dateLink.setValue(dateRange);
            report.setDateLink(dateLink);

            //Medium link
            Link mediumLink = new Link();
            mediumLink.setLabel(report.getMedium());
            mediumLink.setValue(String.valueOf(report.getMediumId()));
            report.setMediumLink(mediumLink);

            //Account link
            Link accountLink = new Link();
            accountLink.setLabel(StringUtils.defaultIfEmpty(report.getAccountAlias(), report.getAccount()));
            accountLink.setValue(String.valueOf(report.getAccountId()));
            accountLink.setLink(true);
            accountLink.setPopup(true);
            accountLink.setId(LinkID.Account);
            report.setAccountLink(accountLink);
        }
        return formatData(reports, query);
    }

    /**
     * 投放日报-媒体维度-汇总数据-展开账号和计划-不展开日期
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public InnerTable mediumSummaryReportExpandAccountAndPlanNotDate(DeliveryReportQuery query) {
        setDefaultOrder(query, Defaults.PLAN, Defaults.ASC);
        List<AdReport> reports = adReportMapper.mediumSummaryReportExpandAccountAndPlanNotDate(query);
        for (AdReport report : reports) {
            //Account link
//            Link accountLink = new Link();
//            accountLink.setLabel(StringUtils.defaultIfEmpty(report.getAccountAlias(), report.getAccount()));
//            accountLink.setValue(String.valueOf(report.getAccountId()));
//            report.setAccountLink(accountLink);

            //Plan link;
            Link planLink = new Link();
            planLink.setLabel(report.getPlan());
            planLink.setValue(String.valueOf(report.getPlanId()));
            report.setPlanLink(planLink);
        }
        InnerTable table = new InnerTable();
        SystemAccountVo systemAccount = systemAccountMapper.getById(query.getAccountId());
        table.setColumn(Columns.getAuthorizedColumns(Columns.MEDIUM_DETAIL_REPORT, systemAccount.getIndicatorPermission()));
        List<DeliveryReportView> deliveryReportViews = formatData(reports, query);
        table.setContent(deliveryReportViews);
        return table;
    }

    /**
     * 格式化数据转化为视图对象并隐藏未授权的指标
     *
     * @param reports
     */
    private List<DeliveryReportView> formatData(List<AdReport> reports, DeliveryReportQuery query) {
        if (reports == null) {
            return null;
        }

        List<DeliveryReportView> deliveryReportViews = new ArrayList<>();
        SystemAccountVo systemAccount = systemAccountMapper.getById(query.getAccountId());
        for (AdReport report : reports) {
            DeliveryReportView view = formatData(report);
            //隐藏未授权的指标
            hideUnauthorizedIndicator(view, systemAccount.getIndicatorPermission());
            deliveryReportViews.add(view);
        }
        return deliveryReportViews;
    }

    /**
     * 隐藏隐藏未授权的指标
     *
     * @param deliveryReportView
     * @param indicatorPermission
     */
    private void hideUnauthorizedIndicator(DeliveryReportView deliveryReportView, String indicatorPermission) {
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.COST)) {
            deliveryReportView.setCost(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.SHOW_NUM)) {
            deliveryReportView.setExposures(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CLICK_NUM)) {
            deliveryReportView.setClicks(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.TOTAL_CLICK_NUM)) {
            deliveryReportView.setTotalClicks(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CTR)) {
            deliveryReportView.setCtr(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CPC)) {
            deliveryReportView.setCpc(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REACH)) {
            deliveryReportView.setReaches(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REACH_RATE)) {
            deliveryReportView.setReachRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DOWNLOAD_TIMES)) {
            deliveryReportView.setDownloads(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DOWNLOAD_RATE)) {
            deliveryReportView.setDownloadRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVE_NUM)) {
            deliveryReportView.setActivations(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVE_RATE)) {
            deliveryReportView.setActivationRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVE_CPA)) {
            deliveryReportView.setActivationCpa(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_NUM)) {
            deliveryReportView.setRegistrations(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_RATE)) {
            deliveryReportView.setRegistrationRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVATION_REGISTRATION_RATE)) {
            deliveryReportView.setActivationRegistrationRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_CPA)) {
            deliveryReportView.setRegistrationCpa(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.RETAIN_D2)) {
            deliveryReportView.setDay2RetentionRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.RETAIN_D3)) {
            deliveryReportView.setDay3RetentionRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.RETAIN_D7)) {
            deliveryReportView.setDay7RetentionRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.RETAIN_D30)) {
            deliveryReportView.setDay30RetentionRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.NEW_ACCOUNT_PAY_NUM)) {
            deliveryReportView.setNewlyIncreasedRecharges(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.NEW_ACCOUNT_PAY_AMOUNT)) {
            deliveryReportView.setNewlyIncreasedRechargeAmount(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.NEWLY_INCREASED_PAY_RATE)) {
            deliveryReportView.setNewlyIncreasedPayRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.PAYBACK_RATE_1DAY)) {
            deliveryReportView.setFirstDayPaybackRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.LTV_1DAY)) {
            deliveryReportView.setLtv1Day(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.LTV_7DAYS)) {
            deliveryReportView.setLtv7Days(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.LTV_30DAYS)) {
            deliveryReportView.setLtv30Days(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.TOTAL_PAY_AMOUNT)) {
            deliveryReportView.setTotalRechargeAmount(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.TOTAL_PAY_NUM)) {
            deliveryReportView.setTotalRecharges(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CUMULATIVE_AMOUNT)) {
            deliveryReportView.setAccumulatedRecharge(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.PAY_RATE)) {
            deliveryReportView.setPayRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DAU)) {
            deliveryReportView.setDau(null);
        }
    }

    /**
     * 格式化投放日报指标
     *
     * @param report
     * @return
     */
    private DeliveryReportView formatData(AdReport report) {
        if (report == null) {
            return null;
        }

        NumberFormat nf = NumberFormat.getPercentInstance();
        //保留两位小数
        nf.setMinimumFractionDigits(2);
        //四舍五入
        nf.setRoundingMode(RoundingMode.HALF_UP);
        DeliveryReportView view = new DeliveryReportView();
        if (!StringUtils.isBlank(report.getMediumId() + "")) {
            view.setMediumType(MediumType.getMediumType(report.getMediumId()));
        }
        if (report.getMedium() != null) {
        	view.setMediumName(report.getMedium());
        } else {
            view.setMediumName(Labels.UNKNOWN);
        }
        if (report.getSystemAccountId() != null) {
        	view.setSystemAccountId(report.getSystemAccountId());
        } else {
            view.setSystemAccountId(0);
        }
        if (report.getSystemAccountName() != null) {
        	view.setSystemAccountName(report.getSystemAccountName());
        } else {
            view.setSystemAccountName(Labels.UNKNOWN);
        }
        view.setDateLink(report.getDateLink());
        view.setMediumLink(report.getMediumLink());
        view.setAccountLink(report.getAccountLink());
        view.setPlanLink(report.getPlanLink());
        view.setMediumAccount(StringUtils.defaultIfEmpty(report.getAccountAlias(), report.getAccount()));
        view.setExposures(report.getExposures());
        view.setClicks(report.getClicks());
        view.setTotalClicks(report.getTotalClicks());
        view.setReaches(report.getLandingPageUV());
        view.setDownloads(report.getDownloadUV());
        view.setActivations(report.getActivations());
        view.setRegistrations(report.getRegistrations());
        view.setNewlyIncreasedRechargeAmount(report.getNewlyIncreasedRechargeAmount());
        view.setNewlyIncreasedRecharges(report.getNewlyIncreasedRecharges());
        view.setNewlyIncreasedPayRate(percent(report.getNewlyIncreasedPayRate(), nf));
        view.setTotalRechargeAmount(report.getTotalRecharges());
        view.setTotalRecharges(StringUtil.defaultIfNull(report.getTotalNumberOfRecharge()));
        view.setAccumulatedRecharge(CurrencyUtils.yuanToText(report.getAccumulatedRecharge()));
        view.setDau(report.getDau());
        view.setRegisterAccountNum(report.getRegisterAccountNum());
        view.setFirstDayRegisterAccountNum(report.getFirstDayRegisterAccountNum());
        view.setFirstDayRegistrations(report.getFirstDayRegistrations());

//        view.setRetentionTime(Labels.UNKNOWN);
        view.setActivationRate(percent(report.getActivationRate(), nf));
        //激活CPA
        if (report.getActivationCpa() != null) {
            view.setActivationCpa(report.getActivationCpa().setScale(2, RoundingMode.HALF_UP).toString());
        } else {
            view.setActivationCpa(Labels.UNKNOWN);
        }

        //激活注册率
        view.setActivationRegistrationRate(percent(report.getActivationRegistrationRate(), nf));

        //注册率
        view.setRegistrationRate(percent(report.getRegistrationRate(), nf));


        //计算各个比率的百分比
        view.setCtr(percent(report.getCtr(), nf));
        view.setReachRate(percent(report.getReachRate(), nf));
        view.setDownloadRate(percent(report.getDownloadRate(), nf));
        view.setFirstDayPaybackRate(percent(report.getFirstDayPaybackRate(), nf));
        //首日LTV
        /*BigDecimal ltv1Day = NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount1Day()), report.getRegisterAccountNum());
        BigDecimal ltv7Days = NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount7Days()), report.getRegisterAccountNum());
        BigDecimal ltv30Days = NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount30Days()), report.getRegisterAccountNum());*/
        view.setLtv1Day(report.getLtv1Day()==null?Labels.UNKNOWN:report.getLtv1Day().setScale(2, RoundingMode.HALF_UP).toString());
        view.setLtv7Days(report.getLtv7Days()==null?Labels.UNKNOWN:report.getLtv7Days().setScale(2, RoundingMode.HALF_UP).toString());
        view.setLtv30Days(report.getLtv30Days()==null?Labels.UNKNOWN:report.getLtv30Days().setScale(2, RoundingMode.HALF_UP).toString());
        view.setPayRate(percent(report.getPayRate(), nf));

        //留存率
        view.setDay2RetentionRate(percent(report.getNextDayRetentionRate(), nf));
        view.setDay3RetentionRate(percent(report.getThreeDayRetentionRate(), nf));
        view.setDay7RetentionRate(percent(report.getSevenDayRetentionRate(), nf));
        view.setDay15RetentionRate(percent(report.getFifteenDayRetentionRate(), nf));
        view.setDay30RetentionRate(percent(report.getThirtyDayRetentionRate(), nf));
        //金钱类数据保留两位小数，并四舍五入.
        if (report.getTotalCost() != null) {
            report.setTotalCost(report.getTotalCost().setScale(2, RoundingMode.HALF_UP));
            view.setCost(report.getTotalCost().toString());
        } else if (report.getTotalCost() == null && view.getMediumType().intValue() == MediumType.ADT) {
            view.setCost(Labels.UNKNOWN);
        }

        if (report.getCpc() == null) {
            view.setCpc(Labels.UNKNOWN);
        } else {
            report.setCpc(report.getCpc().setScale(2, RoundingMode.HALF_UP));
            view.setCpc(report.getCpc().toString());
        }

        if (report.getCostPerRegistration() == null) {
            view.setRegistrationCpa(Labels.UNKNOWN);
        } else {
            report.setCostPerRegistration(report.getCostPerRegistration().setScale(2, RoundingMode.HALF_UP));
            view.setRegistrationCpa(report.getCostPerRegistration().toString());
        }
        if (report.getRegistrationAccountNumCpa() == null) {
            view.setRegistrationAccountNumCpa(Labels.UNKNOWN);
        } else {
            report.setRegistrationAccountNumCpa(report.getRegistrationAccountNumCpa().setScale(2, RoundingMode.HALF_UP));
            view.setRegistrationAccountNumCpa(report.getRegistrationAccountNumCpa().toString());
        }

        view.setNaturalFlow(report.isNaturalFlow());

        //计算行唯一标识
        view.setId(generateRowId(report));

        return view;
    }

    /**
     * 生成浮点数的百分比形式。
     *
     * @param value
     * @param nf
     * @return
     */
    private String percent(Double value, NumberFormat nf) {
        if (value == null) {
            return Labels.UNKNOWN;
        } else {
            return nf.format(value);
        }
    }

    /**
     * 生成行ID，仅前端表格组件使用。
     *
     * @param report
     * @return
     */
    private long generateRowId(AdReport report) {
        StringBuilder idBuilder = new StringBuilder();
        if (report.getDateLink() == null) {
            idBuilder.append(report.getDate());
        } else {
            idBuilder.append(report.getDateLink().getValue());
        }

        if (report.getMediumLink() == null) {
            idBuilder.append(report.getMediumId());
        } else {
            idBuilder.append(report.getMediumLink().getValue());
        }

        if (report.getAccountLink() == null) {
            idBuilder.append(report.getAccountId());
        } else {
            idBuilder.append(report.getAccountLink().getValue());
        }

        if (report.getPlanLink() == null) {
            idBuilder.append(report.getPlanId());
        } else {
            idBuilder.append(report.getPlanLink().getValue());
        }
        if (report.getSystemAccountId() == null) {
            idBuilder.append(0);
        } else {
            idBuilder.append(report.getSystemAccountId());
        }
        //生成联结后的ID字符串的CRC码
        CRC32 crc32 = new CRC32();
        crc32.update(idBuilder.toString().getBytes());
        return crc32.getValue();
    }

    /**
     * 获取投放日报分页数据.
     *
     * @param view      查看方式
     * @param dimension 维度
     * @param query     查询条件
     * @return 分页数据.
     */
    @Override
    public PageData adReport(View view, Dimension dimension, DeliveryReportQuery query) {
        //根据维度和查看方式选择相应的数据访问接口.
        switch (dimension) {
            case Plan:
                switch (view) {
                    case Total:
                        return planSummaryReport(query);
                    case Daily:
                        return planDailyReport(query);
                }
                break;
            case Medium:
                switch (view) {
                    case Total:
                        return mediumSummaryReport(query);
                    case Daily:
                        return mediumDailyReport(query);
                }
                break;
        }
        return null;
    }

    /**
     * 获取嵌套表数据.
     *
     * @param view      查看方式
     * @param dimension 维度
     * @param link      链接ID
     * @param query     查询条件
     * @return
     */
    @Override
    public Object adReportDetail(View view, Dimension dimension, LinkID link, DeliveryReportQuery query) {
        //根据维度和查看方式选择相应的数据访问接口.
        switch (dimension) {
            case Plan:
                switch (view) {
                    case Total:
                        return planSummaryReportDetail(query);
                    case Daily:
                        return planDailyReportDetail(query);
                }
                break;
            case Medium:
                switch (view) {
                    case Total:
                        switch (link) {
                            case Date:
                                return mediumSummaryReportExpandDate(query);
                            case DateMedium:
                                return mediumSummaryReportExpandEventGroup(query);
                            case DateAccount:
                                return mediumSummaryReportExpandAccountAndPlan(query);
                            case Medium:
                                return mediumSummaryReportExpandEventGroupNotDate(query);
                            case Account:
                                return mediumSummaryReportExpandAccountAndPlanNotDate(query);
                        }
                        break;

                    case Daily:
                        switch (link) {
                            case DailyMediumTotal:
                                return mediumDailyReportExpandMedium(query);
                            case DailyMedium:
                                return mediumDailyReportExpandEventGroup(query);
                            case DailyAccount:
                                return mediumDailyReportExpandAccountAndPlan(query);
                        }
                        break;
                }
                break;
        }
        return null;
    }

    /**
     * 设置默认排序方式
     *
     * @param query
     * @param defaultOrderBy
     * @param defaultOrder
     */
    private void setDefaultOrder(DeliveryReportQuery query, String defaultOrderBy, String defaultOrder) {
        if (StringUtils.isBlank(query.getOrderBy())) {
            query.setOrderBy(defaultOrderBy);
        }

        if (StringUtils.isBlank(query.getOrder())) {
            query.setOrder(defaultOrder);
        }
    }

    /**
     * 生成报告Excel.
     *
     * @param reports    投放日报列表
     * @param reportType 报告类型
     * @param accountId  系统账号ID
     * @return
     */
    @Override
    public Workbook toWorkBook(List<DeliveryReportView> reports, ReportType reportType, int accountId) {
        SystemAccountVo systemAccount = this.systemAccountMapper.getById(accountId);
        HSSFWorkbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet(SHEET_NAME);
        switch (reportType) {
            case Plan:
                ExcelUtils.buildWorkBookForPlan(workbook, reports, sheet, systemAccount.getIndicatorPermission());
                break;
            case Medium:
                ExcelUtils.buildWorkBookForMedium(workbook, reports, sheet, systemAccount.getIndicatorPermission());
                break;
            case MediumDetail:
                ExcelUtils.buildWorkBookForMediumDetail(workbook, reports, sheet, systemAccount.getIndicatorPermission());
                break;
            case systemAccount:
                ExcelUtils.buildWorkBookForSystemAccount(workbook, reports, sheet, systemAccount.getIndicatorPermission());
                break;
        }
        return workbook;
    }

    /**
     * 考虑到自然流量的因素，一些比率指标不能直接通过SQL排序，需要在应用层重新排序
     *
     * @param reports
     * @param orderBy
     * @param order
     */
    private void sort(final List<AdReport> reports, final String orderBy, final String order) {
        if (reports != null) {
            Collections.sort(reports, new Comparator<AdReport>() {
                @Override
                public int compare(AdReport o1, AdReport o2) {
                    int result = 0;
                    if (StringUtils.equalsIgnoreCase(orderBy, Fields.DATE)) {
                        result = indicatorCompare(o1.getDate(), o2.getDate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.COST)) {
                        result = indicatorCompare(o1.getTotalCost(), o2.getTotalCost());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.EXPOSURES)) {
                        result = indicatorCompare(o1.getExposures(), o2.getExposures());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.CLICKS)) {
                        result = indicatorCompare(o1.getClicks(), o2.getClicks());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.TOTAL_CLICKS)) {
                        result = indicatorCompare(o1.getTotalClicks(), o2.getTotalClicks());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.CTR)) {
                        result = indicatorCompare(o1.getCtr(), o2.getCtr());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.CPC)) {
                        result = indicatorCompare(o1.getCpc(), o2.getCpc());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.REACHES)) {
                        result = indicatorCompare(o1.getLandingPageUV(), o2.getLandingPageUV());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.REACH_RATE)) {
                        result = indicatorCompare(o1.getReachRate(), o2.getReachRate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.DOWNLOADS)) {
                        result = indicatorCompare(o1.getDownloadUV(), o2.getDownloadUV());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.DOWNLOAD_RATE)) {
                        result = indicatorCompare(o1.getDownloadRate(), o2.getDownloadRate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.ACTIVATIONS)) {
                        result = indicatorCompare(o1.getActivations(), o2.getActivations());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.ACTIVATION_RATE)) {
                        result = indicatorCompare(o1.getActivationRate(), o2.getActivationRate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.ACTIVATION_CPA)) {
                        result = indicatorCompare(o1.getActivationCpa(), o2.getActivationCpa());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.REGISTRATIONS)) {
                        result = indicatorCompare(o1.getRegistrations(), o2.getRegistrations());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.REGISTRATION_RATE)) {
                        result = indicatorCompare(o1.getRegistrationRate(), o2.getRegistrationRate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.REGISTRATION_CPA)) {
                        result = indicatorCompare(o1.getCostPerRegistration(), o2.getCostPerRegistration());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.ACTIVATION_REGISTRATION_RATE)) {
                        result = indicatorCompare(o1.getActivationRegistrationRate(), o2.getActivationRegistrationRate());
                    } /*else if (StringUtils.equalsIgnoreCase(orderBy, Fields.DAY2_RETENTION_RATE)) {
                        result = indicatorCompare(o1.getNextDayRetentionRate(), o2.getNextDayRetentionRate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.DAY3_RETENTION_RATE)) {
                        result = indicatorCompare(o1.getThreeDayRetentionRate(), o2.getThreeDayRetentionRate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.DAY7_RETENTION_RATE)) {
                        result = indicatorCompare(o1.getSevenDayRetentionRate(), o2.getSevenDayRetentionRate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.DAY30_RETENTION_RATE)) {
                        result = indicatorCompare(o1.getThirtyDayRetentionRate(), o2.getThirtyDayRetentionRate());
                    }*/ else if (StringUtils.equalsIgnoreCase(orderBy, Fields.NEWLY_INCREASED_RECHARGES)) {
                        result = indicatorCompare(o1.getNewlyIncreasedRecharges(), o2.getNewlyIncreasedRecharges());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.NEWLY_ACCOUNT_PAY_AMOUNT)) {
                        result = indicatorCompare(o1.getNewlyIncreasedRechargeAmount(), o2.getNewlyIncreasedRechargeAmount());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.NEWLY_INCREASED_PAY_RATE)) {
                        result = indicatorCompare(o1.getNewlyIncreasedPayRate(), o2.getNewlyIncreasedPayRate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.FIRST_DAY_PAYBACK_RATE)) {
                        result = indicatorCompare(o1.getFirstDayPaybackRate(), o2.getFirstDayPaybackRate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.LTV_1DAY)) {
                        //首日LTV
                        BigDecimal ltv1 = NumberUtils.cpa(CurrencyUtils.fenToYuan(o1.getPayAmount1Day()), o1.getRegisterAccountNum());
                        BigDecimal ltv2 = NumberUtils.cpa(CurrencyUtils.fenToYuan(o2.getPayAmount1Day()), o2.getRegisterAccountNum());
                        result = indicatorCompare(ltv1, ltv2);
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.LTV_7DAYS)) {
                        //7日LTV
                        BigDecimal ltv1 = NumberUtils.cpa(CurrencyUtils.fenToYuan(o1.getPayAmount7Days()), o1.getRegisterAccountNum());
                        BigDecimal ltv2 = NumberUtils.cpa(CurrencyUtils.fenToYuan(o2.getPayAmount7Days()), o2.getRegisterAccountNum());
                        result = indicatorCompare(ltv1, ltv2);
                    }else if (StringUtils.equalsIgnoreCase(orderBy, Fields.LTV_15DAYS)) {
                        //7日LTV
                        BigDecimal ltv1 = NumberUtils.cpa(CurrencyUtils.fenToYuan(o1.getPayAmount7Days()), o1.getRegisterAccountNum());
                        BigDecimal ltv2 = NumberUtils.cpa(CurrencyUtils.fenToYuan(o2.getPayAmount7Days()), o2.getRegisterAccountNum());
                        result = indicatorCompare(ltv1, ltv2);
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.LTV_30DAYS)) {
                        //30日LTV
                        BigDecimal ltv1 = NumberUtils.cpa(CurrencyUtils.fenToYuan(o1.getPayAmount30Days()), o1.getRegisterAccountNum());
                        BigDecimal ltv2 = NumberUtils.cpa(CurrencyUtils.fenToYuan(o2.getPayAmount30Days()), o2.getRegisterAccountNum());
                        result = indicatorCompare(ltv1, ltv2);
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.TOTAL_RECHARGE_AMOUNT)) {
                        result = indicatorCompare(o1.getTotalRecharges(), o2.getTotalRecharges());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.TOTAL_RECHARGES)) {
                        result = indicatorCompare(o1.getTotalNumberOfRecharge(), o2.getTotalNumberOfRecharge());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.ACCUMULATED_RECHARGE)) {
                        result = indicatorCompare(o1.getAccumulatedRecharge(), o2.getAccumulatedRecharge());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.PAY_RATE)) {
                        result = indicatorCompare(o1.getPayRate(), o2.getPayRate());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.DAU)) {
                        result = indicatorCompare(o1.getDau(), o2.getDau());
                    } else if (StringUtils.equalsIgnoreCase(orderBy, Fields.REGISTERACCOUNTNUM)) {
                        result = indicatorCompare(o1.getRegisterAccountNum(), o2.getRegisterAccountNum());
                    }else if (StringUtils.equalsIgnoreCase(orderBy, Fields.FIRSTDAYREGISTRATIONS)) {
                        result = indicatorCompare(o1.getFirstDayRegistrations(), o2.getFirstDayRegistrations());
                    }else if (StringUtils.equalsIgnoreCase(orderBy, Fields.FIRSTDAYREGISTERACCOUNTNUM)) {
                        result = indicatorCompare(o1.getFirstDayRegisterAccountNum(), o2.getFirstDayRegisterAccountNum());
                    }
                    
                    //降序
                    if (StringUtils.equalsIgnoreCase(Defaults.DESC, order)) {
                        result = -result;
                    }
                    return result;
                }
            });
        }
    }


    /**
     * 比较两个相同类型的值
     *
     * @param num1
     * @param num2
     * @param <N>
     * @return
     */
    private <N extends Comparable> int indicatorCompare(N num1, N num2) {
        if (num1 == null && num2 == null) {
            return 0;
        } else if (num1 == null) {
            return -1;
        } else if (num2 == null) {
            return 1;
        } else {
            return num1.compareTo(num2);
        }
    }


    private void dateCompletion(List<AdReport> reports, Date startDate, Date endDate, String order) {
        dateCompletion(reports, startDate, endDate, order, null);
    }

    /**
     * 补全日期
     *
     * @param reports   投放日期，倒序排列
     * @param startDate
     * @param endDate
     */
    private void dateCompletion(List<AdReport> reports, Date startDate, Date endDate, String order, TableType tableType) {
        if (reports != null) {
            if (StringUtils.equalsIgnoreCase("desc", order)) {
                Collections.sort(reports, new Comparator<AdReport>() {
                    @Override
                    public int compare(AdReport o1, AdReport o2) {
                        return o2.getDate().compareTo(o1.getDate());
                    }
                });
                dateCompletionInReverseOrder(reports, startDate, endDate, tableType);
            } else {
                Collections.sort(reports, new Comparator<AdReport>() {
                    @Override
                    public int compare(AdReport o1, AdReport o2) {
                        return o1.getDate().compareTo(o2.getDate());
                    }
                });
                dateCompletionInAscendingOrder(reports, startDate, endDate, tableType);
            }
        }
    }

    /**
     * 日期补全，升序排列
     *
     * @param reports
     * @param startDate
     * @param endDate
     */
    private void dateCompletionInAscendingOrder(List<AdReport> reports, Date startDate, Date endDate, TableType tableType) {
        if (reports != null) {
            AdReport template = null;
            if (!reports.isEmpty()) {
                //获取列表的第一个报告为样本
                template = reports.get(0);
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            int index = 0;
            while (!calendar.getTime().after(endDate)) {
                String date = DateUtils.format(calendar.getTime());
                if (index >= reports.size() || !StringUtils.equalsIgnoreCase(reports.get(index).getDate(), date)) {
                    //添加默认值
                    AdReport missingReport = generateMissingReport(template, tableType);
                    Link dateLink = new Link();
                    dateLink.setValue(date);
                    dateLink.setLabel(date);
                    missingReport.setDateLink(dateLink);
                    reports.add(index, missingReport);
                }
                calendar.add(Calendar.DATE, 1);
                index++;
            }
        }
    }

    /**
     * 日期补全，降序排列
     *
     * @param reports
     * @param startDate
     * @param endDate
     */
    private void dateCompletionInReverseOrder(List<AdReport> reports, Date startDate, Date endDate, TableType tableType) {
        if (reports != null) {
            AdReport template = null;
            if (!reports.isEmpty()) {
                //获取列表的第一个报告为样本
                template = reports.get(0);
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(endDate);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            int index = 0;
            while (!calendar.getTime().before(startDate)) {
                String date = DateUtils.format(calendar.getTime());
                if (index >= reports.size() || !StringUtils.equalsIgnoreCase(reports.get(index).getDate(), date)) {
                    //添加默认值
                    AdReport missingReport = generateMissingReport(template, tableType);
                    Link dateLink = new Link();
                    dateLink.setValue(date);
                    dateLink.setLabel(date);
                    missingReport.setDateLink(dateLink);
                    reports.add(index, missingReport);
                }
                calendar.add(Calendar.DATE, -1);
                index++;
            }
        }
    }

    private Date getPagingStartDate(Date startDate, Date endDate, int pageNumber, int pageSize) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DATE, -(pageSize * pageNumber - 1));
        Date pagingStartDate = calendar.getTime();
        if (pagingStartDate.before(startDate)) {
            return startDate;
        } else {
            return pagingStartDate;
        }
    }

    private Date getPagingEndDate(Date endDate, int pageNumber, int pageSize) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DATE, -pageSize * (pageNumber - 1));
        return calendar.getTime();
    }

    /**
     * 计算总记录数
     *
     * @return
     */
    private int getTotalCount(Date startDate, Date endDate) {
        long diff = endDate.getTime() - startDate.getTime();
        long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        if (days < Integer.MIN_VALUE || days > Integer.MAX_VALUE) {
            throw new IllegalArgumentException
                    (days + " cannot be cast to int without changing its value.");
        }
        return (int) days + 1;
    }

    /**
     * 生成缺失日期的报告.
     *
     * @param template
     */
    private AdReport generateMissingReport(AdReport template, TableType tableType) {
        AdReport missingReport = new AdReport();
        if (template != null) {
            missingReport.setPlanLink(template.getPlanLink());
            missingReport.setMediumLink(template.getMediumLink());
            missingReport.setAccountLink(template.getAccountLink());
        } else {
            if (tableType != null) {
                switch (tableType) {
                    case PlanDaily:
                        //Account link
                        Link accountLink = new Link();
                        accountLink.setLabel(Constants.UNKNOWN);
                        accountLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
                        missingReport.setAccountLink(accountLink);

                        //Plan link;
                        Link planLink = new Link();
                        planLink.setLabel(Constants.TOTAL);
                        planLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
                        planLink.setLink(true);
                        planLink.setId(LinkID.Plan);
                        missingReport.setPlanLink(planLink);
                        break;
                    case MediumDaily:
                        //Medium link
                        Link mediumLink = new Link();
                        mediumLink.setLabel(Constants.TOTAL);
                        mediumLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
                        mediumLink.setLink(true);
                        mediumLink.setId(LinkID.DailyMediumTotal);
                        missingReport.setMediumLink(mediumLink);

                        //Account link
                        Link mediumAccountLink = new Link();
                        mediumAccountLink.setLabel(Constants.UNKNOWN);
                        mediumAccountLink.setValue(String.valueOf(Constants.UNKNOWN_ID));
                        missingReport.setAccountLink(mediumAccountLink);
                        break;
                }
            }
        }
        //指标设置为0
        missingReport.setTotalCost(BigDecimal.valueOf(0));
        missingReport.setExposures(0);
        missingReport.setClicks(0);
        missingReport.setTotalClicks(0);
        missingReport.setCtr(0.0);
        missingReport.setCpc(BigDecimal.valueOf(0));
        missingReport.setLandingPageUV(0);
        missingReport.setReachRate(0.0);
        missingReport.setDownloadUV(0);
        missingReport.setDownloadRate(0.0);
        missingReport.setActivations(0);
        missingReport.setActivationRate(0.0);
        missingReport.setActivationRegistrationRate(0.0);
        missingReport.setActivationCpa(BigDecimal.valueOf(0));
        missingReport.setRegistrations(0);
        missingReport.setRegistrationRate(0.0);
        missingReport.setCostPerRegistration(BigDecimal.valueOf(0));
        missingReport.setNextDayRetentionRate(0.0);
        missingReport.setThreeDayRetentionRate(0.0);
        missingReport.setSevenDayRetentionRate(0.0);
        missingReport.setThirtyDayRetentionRate(0.0);
        //新增充值人数
        missingReport.setNewlyIncreasedRecharges(0);
        missingReport.setNewlyIncreasedRechargeAmount(BigDecimal.valueOf(0));
        missingReport.setNewlyIncreasedPayRate(0.0);
        missingReport.setFirstDayPaybackRate(0.0);
        missingReport.setTotalRecharges(BigDecimal.valueOf(0));
        missingReport.setTotalNumberOfRecharge(0);
        missingReport.setAccumulatedRecharge(BigDecimal.valueOf(0));
        missingReport.setPayRate(0.0);
        missingReport.setDau(0);
        missingReport.setFirstDayRegisterAccountNum(0);
        missingReport.setFirstDayRegistrations(0);
        missingReport.setFifteenDayRetentionRate(0.0);
        return missingReport;
    }

    /**
     * 表格类型
     */
    private enum TableType {
        PlanDaily, MediumDaily
    }

	@Override
	public PageData queryindicatorByAccount(DeliveryReportQuery query) {
		setDefaultOrder(query, Defaults.SYSTEM_ACCOUNT_ID, Defaults.DESC);
		int totalCount = adReportMapper.queryindicatorByAccountTotalCount(query);
        PageData pageData = PageDataHelper.getPageData(query.getPageSize(), query.getCurrentPage(), totalCount);
        List<AdReport> reports = null;
        if (totalCount > 0) {
        	reports = adReportMapper.queryindicatorByAccount(query);
        }
        List<DeliveryReportView> deliveryReportViews = formatData(reports, query);
        pageData.setContent(deliveryReportViews);
        return pageData;
	}

}
