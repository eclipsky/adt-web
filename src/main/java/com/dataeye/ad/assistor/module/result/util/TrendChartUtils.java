package com.dataeye.ad.assistor.module.result.util;

import com.dataeye.ad.assistor.module.realtimedelivery.model.Chart;
import com.dataeye.ad.assistor.module.result.model.CoreIndicatorQuery;
import com.dataeye.ad.assistor.module.result.model.DailyIndicator;
import com.dataeye.ad.assistor.module.result.model.TrendChart;
import com.dataeye.ad.assistor.util.DateUtils;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by huangzehai on 2017/4/11.
 */
public final class TrendChartUtils {
    private static final String OTHERS = "其他";
    private static final int LINES_LIMIT = 2;

    private TrendChartUtils() {

    }

    /**
     * 转化为趋势图
     *
     * @param coreIndicatorQuery
     * @param indicators
     * @return
     */
    public static TrendChart<BigDecimal> toTrendChart(CoreIndicatorQuery coreIndicatorQuery, List<DailyIndicator> indicators) {
        return toTrendChart(coreIndicatorQuery, indicators, false);
    }

    public static TrendChart<BigDecimal> toTrendChart(CoreIndicatorQuery coreIndicatorQuery, List<DailyIndicator> indicators, boolean isPercent) {
        return toTrendChart(coreIndicatorQuery, indicators, isPercent, false);
    }

    /**
     * 转化为趋势图
     *
     * @param coreIndicatorQuery
     * @param indicators
     * @return
     */
    public static TrendChart<BigDecimal> toTrendChart(CoreIndicatorQuery coreIndicatorQuery, List<DailyIndicator> indicators, boolean isPercent, boolean isDefaultChart) {
        if (indicators == null) {
            return null;
        }
        //Group by name.
        Map<String, List<DailyIndicator>> map = new HashMap<>();
        for (DailyIndicator indicator : indicators) {
            if (map.containsKey(indicator.getName())) {
                map.get(indicator.getName()).add(indicator);
            } else {
                List<DailyIndicator> indicatorsByName = new ArrayList<>();
                indicatorsByName.add(indicator);
                map.put(indicator.getName(), indicatorsByName);
            }
        }

        //构建趋势图对象
        TrendChart<BigDecimal> trendChart = new TrendChart<>();
        //设置横轴数据
        trendChart.setDates(generateDates(coreIndicatorQuery.getStartDate(), coreIndicatorQuery.getEndDate()));
        //构建纵轴
        List<Chart<BigDecimal>> charts = new ArrayList<>();
        for (Map.Entry<String, List<DailyIndicator>> entry : map.entrySet()) {
            //构建一条折线纵轴数据
            Chart<BigDecimal> chart = new Chart<>();
            //设置折现标题
            chart.setTitle(StringUtils.defaultIfEmpty(entry.getKey(), OTHERS));
            chart.setPercent(isPercent);
            List<BigDecimal> values = new ArrayList<>();

            //补全日期
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(coreIndicatorQuery.getStartDate());
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            List<DailyIndicator> dailyIndicators = entry.getValue();
            int index = 0;
            while (!calendar.getTime().after(coreIndicatorQuery.getEndDate())) {
                if (index < dailyIndicators.size() && calendar.getTime().equals(dailyIndicators.get(index).getDate())) {
                    BigDecimal value = dailyIndicators.get(index).getValue();
                    if (value == null) {
                        value = new BigDecimal(0);
                    }
                    values.add(value);
                    index++;
                } else {
                    //缺失日期，设置其值为0
                    values.add(new BigDecimal(0));
                }
                calendar.add(Calendar.DATE, 1);
            }
            chart.setValues(values);
            charts.add(chart);
        }
        if (isDefaultChart && charts.size() > LINES_LIMIT) {
            //仅显示前面两条折线
            trendChart.setIndicators(charts.subList(0, LINES_LIMIT));
        } else {
            trendChart.setIndicators(charts);
        }
        return trendChart;
    }

    /**
     * 生成趋势图横轴日期列表
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private static List<String> generateDates(Date startDate, Date endDate) {
        List<String> dates = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        while (!calendar.getTime().after(endDate)) {
            dates.add(DateUtils.format(calendar.getTime()));
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }
}
