package com.dataeye.ad.assistor.module.mediumaccount.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.mediumaccount.mapper.MediumDiscountRateMapper;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumDiscountRate;

/**
 * 媒体折扣率Service
 * @author luzhuyou 2017/02/20
 *
 */
@Service
public class MediumDiscountRateService {

	@Autowired
	private MediumDiscountRateMapper mediumDiscountRateMapper;
	
	/**
	 * 新增媒体折扣率
	 * @param mediumAccountId
	 * @param discountRate
	 * @return
	 */
	public int add(int mediumAccountId, double discountRate) {
		MediumDiscountRate mediumDiscountRate = new MediumDiscountRate();
		mediumDiscountRate.setMediumAccountId(mediumAccountId);
		mediumDiscountRate.setDiscountRate(discountRate);
		mediumDiscountRate.setCreateTime(new Date());
		mediumDiscountRate.setUpdateTime(new Date());
		return mediumDiscountRateMapper.add(mediumDiscountRate);
	}

	/**
	 * 修改媒体折扣率
	 * @param mediumAccountId
	 * @param discountRate
	 * @return
	 */
	public int modify(int mediumAccountId, double discountRate) {
		MediumDiscountRate mediumDiscountRate = new MediumDiscountRate();
		mediumDiscountRate.setMediumAccountId(mediumAccountId);
		mediumDiscountRate.setDiscountRate(discountRate);
		mediumDiscountRate.setCreateTime(new Date());
		mediumDiscountRate.setUpdateTime(new Date());
		return mediumDiscountRateMapper.update(mediumDiscountRate);
	}

}
