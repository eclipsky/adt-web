package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.List;

import com.google.gson.annotations.Expose;

public class MediumAccountGroupVo {
	
	/** 媒体名称 */
	@Expose
	private String mediumName;
	/** 媒体帐号集合集合*/
	@Expose
	private List<String> list;
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	public List<String> getList() {
		return list;
	}
	public void setList(List<String> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		return "MediumAccountGroupVo [mediumName=" + mediumName + ", list="
				+ list + "]";
	}
	
	
}
