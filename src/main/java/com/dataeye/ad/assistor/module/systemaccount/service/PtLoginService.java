package com.dataeye.ad.assistor.module.systemaccount.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.Constant.DeProduct;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.company.constants.Constants.CompanyStatus;
import com.dataeye.ad.assistor.module.company.model.Company;
import com.dataeye.ad.assistor.module.company.model.TrackingCompany;
import com.dataeye.ad.assistor.module.company.service.CompanyService;
import com.dataeye.ad.assistor.module.company.service.TrackingCompanyService;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.GroupId;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.PtLoginInterface;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.UserStatus;
import com.dataeye.ad.assistor.module.systemaccount.model.PtLoginResponse;
import com.dataeye.ad.assistor.module.systemaccount.model.PtLoginUser;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.MailUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

@Service
public class PtLoginService {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(PtLoginService.class);
	@Autowired
	private CompanyService companyService;
	@Autowired
	private TrackingCompanyService trackingCompanyService;
	@Autowired
    private SystemAccountService systemAccountService;
	@Autowired
    private CrmManagerAccountService crmManagerAccountService;
	
	/**
	 * 根据token检查并获取统一登录用户信息
	 * @param token
	 * @return
	 * @author luzhuyou 2017/05/10
	 */
	public PtLoginUser checkAndGetPtLoginAccount(String token) {
		// 1.构建查询参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("token", token);
		// 2.HTTP请求获取数据
		String getTokenInfoInterface = ConfigHandler.getProperty(PtLoginInterface.GET_TOKEN_INFO_KEY, PtLoginInterface.GET_TOKEN_INFO);
    	String httpResponse = HttpRequest.post(getTokenInfoInterface, params);
    	// 判断邮件是否已经被使用
    	
    	if(httpResponse == null) {
    		logger.info("统一登录获取统一登录用户信息接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("统一登录获取统一登录用户信息接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	// {"statusCode":200,"content":{"UID":42643,"userID":"jennyli@dataeye.com","userName":"李金华","companyName":"","tel":"","qq":"","createTime":1489141568,"createID":42408,"status":1},"id":"1494409362512K"}
    	PtLoginResponse<PtLoginUser> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<PtLoginResponse<PtLoginUser>>(){}.getType());
    	
        if(response == null || response.getContent() == null) {
        	logger.info("统一登录用户信息不存在.");
        	ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
        return response.getContent();
	}

	/**
	 * 公司信息注册
	 * @param ptLoginUser 统一登录用户信息
	 * @author luzhuyou 2017/05/10
	 */
	public Company registerCompany(PtLoginUser ptLoginUser) {
		String accountName = ptLoginUser.getUserID();
		// 1. 获取并判断公司状态，只有正常状态才可登录 add by luzhuyou 2017.05.18
		Company company = companyService.getByEmail(accountName);
		if(company != null) {
			int companyStatus = company.getStatus(); 
			if(companyStatus == 1 || companyStatus == 2 || companyStatus == 3) {
				logger.info("[{}]用户审核中.", accountName);
			} else if(companyStatus == 0) {
				logger.info("[{}]公司已注册.", ptLoginUser.getCompanyName());
				ExceptionHandler.throwPermissionException(StatusCode.LOGI_USER_ID_EXISTS);
			} else {
				logger.info("[{}]公司状态异常.", ptLoginUser.getCompanyName());
				ExceptionHandler.throwPermissionException(StatusCode.LOGI_COMPANY_STATUS_ERROR);
			}
		} else {
			// 2. 在ADT创建公司信息，公司状态为“4-待运营审核”
			company = new Company();
			company.setCompanyName(ptLoginUser.getCompanyName());
			company.setEmail(accountName);
			company.setUid(ptLoginUser.getUID());
			company.setUserName(ptLoginUser.getUserName());
			company.setTel(ptLoginUser.getTel());
			company.setQq(ptLoginUser.getQq());
			company.setStatus(CompanyStatus.OPE_AUDIT); // 首次创建，进入运营审核状态
			companyService.add(company);
			
			SystemAccountVo account = systemAccountService.get(accountName);
			if (account==null) {
				String encodePwd = this.getPwd(accountName);
				if(StringUtils.isBlank(encodePwd)) {
					encodePwd = Constants.DEFAULT_ENCODE_PWD;
				}
				String menuPermission = ConfigHandler.getProperty(Constants.COMPANY_MENU_PERMISSION_KEY, Constants.DEFAULT_COMPANY_MENU_PERMISSION);
				String indicatorPermission = ConfigHandler.getProperty(Constants.COMPANY_INDICATOR_PERMISSION_KEY, Constants.DEFAULT_COMPANY_INDICATOR_PERMISSION);
				// 新增一条状态为失效的系统账号信息，在完成运营审核后，状态恢复为正常
				systemAccountService.add(accountName, ptLoginUser.getUserName(), encodePwd, ptLoginUser.getTel(), UserStatus.INVALID, GroupId.NONE, menuPermission, company.getCompanyId(), Constants.AccountRole.COMPANY,indicatorPermission,null,null,null);
			}
		}
		// 3. 获取运营人员账号信息，用于接收邮件
		String bizAuditAccount = ConfigHandler.getProperty(Constants.OPER_AUDIT_ACCOUNT_KEY, Constants.DEFAULT_OPER_AUDIT_ACCOUNT);
		
		// 4. 发送邮件进行给运营人员进行审核
		MailUtils.sendNewAccountAccessMail(DeProduct.ADT, ptLoginUser.getCompanyName(), 
				ptLoginUser.getUserName(), ptLoginUser.getUserID(), ptLoginUser.getTel(), 
				ptLoginUser.getQq(), bizAuditAccount);
		
		return company;
	}
	
	/**
	 * Tracking账号信息注册
	 * @param ptLoginUser 统一登录用户信息
	 * @author luzhuyou 2017/07/21
	 */
	public TrackingCompany registerTrackingAccount(PtLoginUser ptLoginUser) {
		String accountName = ptLoginUser.getUserID();
		
		TrackingCompany company = trackingCompanyService.getByEmail(accountName);
		if(company != null) {
			int companyStatus = company.getStatus(); 
			if(companyStatus == 1 || companyStatus == 2 || companyStatus == 3) {
				logger.info("[{}]用户审核中.", accountName);
			} else if(companyStatus == 0) {
				logger.info("[{}]公司已注册.", ptLoginUser.getCompanyName());
				ExceptionHandler.throwPermissionException(StatusCode.LOGI_USER_ID_EXISTS);
			} else {
				logger.info("[{}]公司状态异常.", ptLoginUser.getCompanyName());
				ExceptionHandler.throwPermissionException(StatusCode.LOGI_COMPANY_STATUS_ERROR);
			}
		} else {
			// 2. 在ADT创建公司信息，公司状态为“4-待运营审核”
			company = new TrackingCompany();
			company.setCompanyName(ptLoginUser.getCompanyName());
			company.setEmail(accountName);
			company.setUid(ptLoginUser.getUID());
			company.setUserName(ptLoginUser.getUserName());
			company.setTel(ptLoginUser.getTel());
			company.setQq(ptLoginUser.getQq());
			company.setStatus(CompanyStatus.OPE_AUDIT); // 首次创建，进入运营审核状态
			trackingCompanyService.add(company);
		}
		// 3. 获取商务人员账号信息，用于接收邮件
		String bizAuditAccount = ConfigHandler.getProperty(Constants.OPER_AUDIT_ACCOUNT_KEY, Constants.DEFAULT_OPER_AUDIT_ACCOUNT);
		
		// 4. 发送邮件进行给商务人员进行审核
		MailUtils.sendNewAccountAccessMail(DeProduct.TRACKING, ptLoginUser.getCompanyName(), 
				ptLoginUser.getUserName(), ptLoginUser.getUserID(), ptLoginUser.getTel(), 
				ptLoginUser.getQq(), bizAuditAccount);
		
		return company;
	}
	
	/**
	 * 根据统一登录用户邮箱获取密码
	 * @param accountName
	 * @return
	 * @author luzhuyou 2017/05/10
	 */
	public String getPwd(String accountName) {
		// 1.构建查询参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", accountName);
		// 2.HTTP请求获取数据
		String getPwdInterface = ConfigHandler.getProperty(PtLoginInterface.GET_PWD_KEY, PtLoginInterface.GET_PWD);
    	String httpResponse = HttpRequest.post(getPwdInterface, params);
    	// 判断邮件是否已经被使用
    	
    	if(httpResponse == null) {
    		logger.info("统一登录获取密码接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode == 406){
        	logger.info("[{}]邮箱号在统一登录账号体系中不存在!", accountName);
        	ExceptionHandler.throwGeneralServerException(StatusCode.LOGI_EMAIL_NOT_IN_PTLOGIN, new Object[]{accountName});
        }else if(statusCode != 200) {
    		logger.info("统一登录获取密码接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	// {"statusCode":200,"content":"f088d6463e3512523b63539c1a97b725","id":"1494409060811v"}
    	PtLoginResponse<String> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<PtLoginResponse<String>>(){}.getType());
    	
        if(response == null || response.getContent() == null) {
        	logger.info("统一登录用户密码不存在");
        	return null;
        }
        return response.getContent();
	}
	
	/**
	 * 新增ADT账户同步到统一登录系统
	 * @param user
	 */
	public PtLoginUser addPtLoginSubAccount(PtLoginUser user) {
		// 1.构建查询参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", user.getUserID());
		params.put("createrID", user.getCreateID()+"");
		params.put("password", user.getPassword());
		params.put("userName", user.getUserName());
		params.put("companyName", user.getCompanyName());
		params.put("tel", user.getTel() == null ? "" : user.getTel());
		params.put("qq", ""); //这个必须写空，如果不传统一登录对应接口获取到的值为null，插入数据库的时候就会出错
		
		String addAdtAccountInterface = ConfigHandler.getProperty(PtLoginInterface.ADD_ADT_ACCOUNT_KEY, PtLoginInterface.ADD_ADT_ACCOUNT);
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(addAdtAccountInterface, params);
    	// 判断邮件是否已经被使用
    	
    	if(httpResponse == null) {
    		logger.info("统一登录新增ADT账户接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("统一登录新增ADT账户接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	// {"statusCode":200,"content":"f088d6463e3512523b63539c1a97b725","id":"1494409060811v"}
    	PtLoginResponse<PtLoginUser> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<PtLoginResponse<PtLoginUser>>(){}.getType());
    	
        if(response == null || response.getContent() == null) {
        	logger.info("新增ADT账户同步到统一登录系统失败");
        	ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
        return response.getContent();
	}
}
