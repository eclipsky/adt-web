package com.dataeye.ad.assistor.module.mediumaccount.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

/**
 * 优化师查询条件.
 * Created by huangzehai on 2017/4/25.
 */
public class OptimizerQuery extends DataPermissionDomain {
    private int mediumAccountId;
    private int productId;

    public int getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(int mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "OptimizerQuery{" +
                "mediumAccountId=" + mediumAccountId +
                ", productId=" + productId +
                '}';
    }
}
