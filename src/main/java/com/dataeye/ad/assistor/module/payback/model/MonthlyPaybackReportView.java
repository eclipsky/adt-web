package com.dataeye.ad.assistor.module.payback.model;

import com.google.gson.annotations.Expose;

public class MonthlyPaybackReportView extends BasicPaybackReportView {
    @Expose
    private Double paybackRateAfter1Month;

    @Expose
    private Double paybackRateAfter2Months;

    @Expose
    private Double paybackRateAfter3Months;

    @Expose
    private Double paybackRateAfter4Months;

    @Expose
    private Double paybackRateAfter5Months;

    @Expose
    private Double paybackRateAfter6Months;

    @Expose
    private Double paybackRateAfter7Months;


    private String paybackRatePercentAfter1Month;

    private String paybackRatePercentAfter2Months;

    private String paybackRatePercentAfter3Months;

    private String paybackRatePercentAfter4Months;

    private String paybackRatePercentAfter5Months;

    private String paybackRatePercentAfter6Months;

    private String paybackRatePercentAfter7Months;

    public Double getPaybackRateAfter1Month() {
        return paybackRateAfter1Month;
    }

    public void setPaybackRateAfter1Month(Double paybackRateAfter1Month) {
        this.paybackRateAfter1Month = paybackRateAfter1Month;
    }

    public Double getPaybackRateAfter2Months() {
        return paybackRateAfter2Months;
    }

    public void setPaybackRateAfter2Months(Double paybackRateAfter2Months) {
        this.paybackRateAfter2Months = paybackRateAfter2Months;
    }

    public Double getPaybackRateAfter3Months() {
        return paybackRateAfter3Months;
    }

    public void setPaybackRateAfter3Months(Double paybackRateAfter3Months) {
        this.paybackRateAfter3Months = paybackRateAfter3Months;
    }

    public Double getPaybackRateAfter4Months() {
        return paybackRateAfter4Months;
    }

    public void setPaybackRateAfter4Months(Double paybackRateAfter4Months) {
        this.paybackRateAfter4Months = paybackRateAfter4Months;
    }

    public Double getPaybackRateAfter5Months() {
        return paybackRateAfter5Months;
    }

    public void setPaybackRateAfter5Months(Double paybackRateAfter5Months) {
        this.paybackRateAfter5Months = paybackRateAfter5Months;
    }

    public Double getPaybackRateAfter6Months() {
        return paybackRateAfter6Months;
    }

    public void setPaybackRateAfter6Months(Double paybackRateAfter6Months) {
        this.paybackRateAfter6Months = paybackRateAfter6Months;
    }

    public Double getPaybackRateAfter7Months() {
        return paybackRateAfter7Months;
    }

    public void setPaybackRateAfter7Months(Double paybackRateAfter7Months) {
        this.paybackRateAfter7Months = paybackRateAfter7Months;
    }

    public String getPaybackRatePercentAfter1Month() {
        return paybackRatePercentAfter1Month;
    }

    public void setPaybackRatePercentAfter1Month(String paybackRatePercentAfter1Month) {
        this.paybackRatePercentAfter1Month = paybackRatePercentAfter1Month;
    }

    public String getPaybackRatePercentAfter2Months() {
        return paybackRatePercentAfter2Months;
    }

    public void setPaybackRatePercentAfter2Months(String paybackRatePercentAfter2Months) {
        this.paybackRatePercentAfter2Months = paybackRatePercentAfter2Months;
    }

    public String getPaybackRatePercentAfter3Months() {
        return paybackRatePercentAfter3Months;
    }

    public void setPaybackRatePercentAfter3Months(String paybackRatePercentAfter3Months) {
        this.paybackRatePercentAfter3Months = paybackRatePercentAfter3Months;
    }

    public String getPaybackRatePercentAfter4Months() {
        return paybackRatePercentAfter4Months;
    }

    public void setPaybackRatePercentAfter4Months(String paybackRatePercentAfter4Months) {
        this.paybackRatePercentAfter4Months = paybackRatePercentAfter4Months;
    }

    public String getPaybackRatePercentAfter5Months() {
        return paybackRatePercentAfter5Months;
    }

    public void setPaybackRatePercentAfter5Months(String paybackRatePercentAfter5Months) {
        this.paybackRatePercentAfter5Months = paybackRatePercentAfter5Months;
    }

    public String getPaybackRatePercentAfter6Months() {
        return paybackRatePercentAfter6Months;
    }

    public void setPaybackRatePercentAfter6Months(String paybackRatePercentAfter6Months) {
        this.paybackRatePercentAfter6Months = paybackRatePercentAfter6Months;
    }

    public String getPaybackRatePercentAfter7Months() {
        return paybackRatePercentAfter7Months;
    }

    public void setPaybackRatePercentAfter7Months(String paybackRatePercentAfter7Months) {
        this.paybackRatePercentAfter7Months = paybackRatePercentAfter7Months;
    }

    @Override
    public String toString() {
        return "MonthlyPaybackReportView{" +
                "paybackRateAfter1Month=" + paybackRateAfter1Month +
                ", paybackRateAfter2Months=" + paybackRateAfter2Months +
                ", paybackRateAfter3Months=" + paybackRateAfter3Months +
                ", paybackRateAfter4Months=" + paybackRateAfter4Months +
                ", paybackRateAfter5Months=" + paybackRateAfter5Months +
                ", paybackRateAfter6Months=" + paybackRateAfter6Months +
                ", paybackRateAfter7Months=" + paybackRateAfter7Months +
                ", paybackRatePercentAfter1Month='" + paybackRatePercentAfter1Month + '\'' +
                ", paybackRatePercentAfter2Months='" + paybackRatePercentAfter2Months + '\'' +
                ", paybackRatePercentAfter3Months='" + paybackRatePercentAfter3Months + '\'' +
                ", paybackRatePercentAfter4Months='" + paybackRatePercentAfter4Months + '\'' +
                ", paybackRatePercentAfter5Months='" + paybackRatePercentAfter5Months + '\'' +
                ", paybackRatePercentAfter6Months='" + paybackRatePercentAfter6Months + '\'' +
                ", paybackRatePercentAfter7Months='" + paybackRatePercentAfter7Months + '\'' +
                "} " + super.toString();
    }
}
