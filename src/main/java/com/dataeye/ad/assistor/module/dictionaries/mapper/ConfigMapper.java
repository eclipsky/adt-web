package com.dataeye.ad.assistor.module.dictionaries.mapper;


import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.dictionaries.model.ConfigVo;

/**
 * 配置表映射器.
 * Created by ldj 2018/03/29
 */
@MapperScan
public interface ConfigMapper {

	/**
	 * 查询配置表列表
	 * @param configVo
	 * @return
	 */
	public List<ConfigVo> query(String vkey);

}
