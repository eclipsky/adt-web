package com.dataeye.ad.assistor.module.payback.model;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Created by huangzehai on 2017/1/17.
 */
public class BasicPaybackReport {

    /**
     * 日期.
     */
    private Date date;

    /**
     * 充值金额
     */
    private BigDecimal totalRechargeAmount;

    /**
     * 消耗.
     */
    private BigDecimal totalCost;

    /**
     * 真实消耗.
     */
    private BigDecimal realCost;

    /**
     * 新增注册数.
     */
    private Integer registrations;

    /**
     * ARPU:充值金额除以DAU
     */
    private BigDecimal arpu;

    /**
     * 注册CPA
    
    private BigDecimal cpa;
     */

    /**
     * ARPPU:充值金额除以充值账号数
     */
    private BigDecimal arppu;
    
    /**
     * 充值账号数
     */
    private Integer totalRecharges;
    
    /**
     *  注册账号数.
     */
    private Integer registerAccountNum;
    
    /**
     * 累计充值（当日注册账号累计到所选日期末的充值）
     */
    private BigDecimal accumulatedRecharge;
    
    /**
     * 首日注册设备数
     * */
    private Integer firstDayRegistrations;
    
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getTotalRechargeAmount() {
        return totalRechargeAmount;
    }

    public void setTotalRechargeAmount(BigDecimal totalRechargeAmount) {
        this.totalRechargeAmount = totalRechargeAmount;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public BigDecimal getRealCost() {
        return realCost;
    }

    public void setRealCost(BigDecimal realCost) {
        this.realCost = realCost;
    }

    public Integer getRegistrations() {
        return registrations;
    }

    public void setRegistrations(Integer registrations) {
        this.registrations = registrations;
    }

    public BigDecimal getArpu() {
        return arpu;
    }

    public void setArpu(BigDecimal arpu) {
        this.arpu = arpu;
    }

    public BigDecimal getArppu() {
		return arppu;
	}

	public void setArppu(BigDecimal arppu) {
		this.arppu = arppu;
	}

	public Integer getTotalRecharges() {
		return totalRecharges;
	}

	public void setTotalRecharges(Integer totalRecharges) {
		this.totalRecharges = totalRecharges;
	}

	public Integer getRegisterAccountNum() {
		return registerAccountNum;
	}

	public void setRegisterAccountNum(Integer registerAccountNum) {
		this.registerAccountNum = registerAccountNum;
	}

	public BigDecimal getAccumulatedRecharge() {
		return accumulatedRecharge;
	}

	public void setAccumulatedRecharge(BigDecimal accumulatedRecharge) {
		this.accumulatedRecharge = accumulatedRecharge;
	}

	public Integer getFirstDayRegistrations() {
		return firstDayRegistrations;
	}

	public void setFirstDayRegistrations(Integer firstDayRegistrations) {
		this.firstDayRegistrations = firstDayRegistrations;
	}

	@Override
	public String toString() {
		return "BasicPaybackReport [date=" + date + ", totalRechargeAmount="
				+ totalRechargeAmount + ", totalCost=" + totalCost
				+ ", realCost=" + realCost + ", registrations=" + registrations
				+ ", arpu=" + arpu + ", arppu=" + arppu + ", totalRecharges="
				+ totalRecharges + ", registerAccountNum=" + registerAccountNum
				+ ", accumulatedRecharge=" + accumulatedRecharge
				+ ", firstDayRegistrations=" + firstDayRegistrations + "]";
	}
}
