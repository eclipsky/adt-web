package com.dataeye.ad.assistor.module.advertisement.model.tencent;

import java.util.List;

/**
 * @author lingliqi
 * @date 2017-12-23 11:57
 */
public class Product {

    private String product_name;
    private String product_refs_id;
    private String product_type;
    private ProductInfoObject product_info;
    private Integer created_time;
    private Integer last_modified_time;
    private List<Object> subordinate_product_list;

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_refs_id() {
        return product_refs_id;
    }

    public void setProduct_refs_id(String product_refs_id) {
        this.product_refs_id = product_refs_id;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public ProductInfoObject getProduct_info() {
        return product_info;
    }

    public void setProduct_info(ProductInfoObject product_info) {
        this.product_info = product_info;
    }

    public int getCreated_time() {
        return created_time;
    }

    public void setCreated_time(int created_time) {
        this.created_time = created_time;
    }

    public int getLast_modified_time() {
        return last_modified_time;
    }

    public void setLast_modified_time(int last_modified_time) {
        this.last_modified_time = last_modified_time;
    }

    public List<Object> getSubordinate_product_list() {
        return subordinate_product_list;
    }

    public void setSubordinate_product_list(List<Object> subordinate_product_list) {
        this.subordinate_product_list = subordinate_product_list;
    }

    public static class ProductInfoObject {
        private ProductTypeAppleAppStoreObject product_type_apple_app_store;

        private ProductTypeAppAndroidOpenPlatformObject product_type_app_android_open_platform;

        private ProductTypeUnionAppInfo product_type_union_app_info;

        public ProductTypeAppleAppStoreObject getProduct_type_apple_app_store() {
            return product_type_apple_app_store;
        }

        public void setProduct_type_apple_app_store(ProductTypeAppleAppStoreObject product_type_apple_app_store) {
            this.product_type_apple_app_store = product_type_apple_app_store;
        }

        public ProductTypeAppAndroidOpenPlatformObject getProduct_type_app_android_open_platform() {
            return product_type_app_android_open_platform;
        }

        public void setProduct_type_app_android_open_platform(ProductTypeAppAndroidOpenPlatformObject product_type_app_android_open_platform) {
            this.product_type_app_android_open_platform = product_type_app_android_open_platform;
        }

        public ProductTypeUnionAppInfo getProduct_type_union_app_info() {
            return product_type_union_app_info;
        }

        public void setProduct_type_union_app_info(ProductTypeUnionAppInfo product_type_union_app_info) {
            this.product_type_union_app_info = product_type_union_app_info;
        }

        public static class ProductTypeAppleAppStoreObject {
            private String app_property_packname;
            private String app_property_version;
            private String app_property_icon_url;
            private String app_property_icon_url_512;
            private String app_property_average_user_rating;
            private int app_property_package_size_bytes;
            private String app_property_pkg_url;
           // private List<String> app_property_genres;  //暂时屏蔽这个字段，因为这个字段有时候返回的是空字符串

            public String getApp_property_packname() {
                return app_property_packname;
            }

            public void setApp_property_packname(String app_property_packname) {
                this.app_property_packname = app_property_packname;
            }

            public String getApp_property_version() {
                return app_property_version;
            }

            public void setApp_property_version(String app_property_version) {
                this.app_property_version = app_property_version;
            }

            public String getApp_property_icon_url() {
                return app_property_icon_url;
            }

            public void setApp_property_icon_url(String app_property_icon_url) {
                this.app_property_icon_url = app_property_icon_url;
            }

            public String getApp_property_icon_url_512() {
                return app_property_icon_url_512;
            }

            public void setApp_property_icon_url_512(String app_property_icon_url_512) {
                this.app_property_icon_url_512 = app_property_icon_url_512;
            }

            public String getApp_property_average_user_rating() {
                return app_property_average_user_rating;
            }

            public void setApp_property_average_user_rating(String app_property_average_user_rating) {
                this.app_property_average_user_rating = app_property_average_user_rating;
            }

            public int getApp_property_package_size_bytes() {
                return app_property_package_size_bytes;
            }

            public void setApp_property_package_size_bytes(int app_property_package_size_bytes) {
                this.app_property_package_size_bytes = app_property_package_size_bytes;
            }

            public String getApp_property_pkg_url() {
                return app_property_pkg_url;
            }

            public void setApp_property_pkg_url(String app_property_pkg_url) {
                this.app_property_pkg_url = app_property_pkg_url;
            }

         /*   public List<String> getApp_property_genres() {
                return app_property_genres;
            }

            public void setApp_property_genres(List<String> app_property_genres) {
                this.app_property_genres = app_property_genres;
            }*/
        }

        public static class ProductTypeAppAndroidOpenPlatformObject {
            private String app_property_packname;
            private String app_property_version;
            private String app_property_icon_url;
            private String app_property_average_user_rating;
            private int app_property_package_size_bytes;
            private String app_property_pkg_url;
            //private List<String> app_property_genres;

            public String getApp_property_packname() {
                return app_property_packname;
            }

            public void setApp_property_packname(String app_property_packname) {
                this.app_property_packname = app_property_packname;
            }

            public String getApp_property_version() {
                return app_property_version;
            }

            public void setApp_property_version(String app_property_version) {
                this.app_property_version = app_property_version;
            }

            public String getApp_property_icon_url() {
                return app_property_icon_url;
            }

            public void setApp_property_icon_url(String app_property_icon_url) {
                this.app_property_icon_url = app_property_icon_url;
            }

            public String getApp_property_average_user_rating() {
                return app_property_average_user_rating;
            }

            public void setApp_property_average_user_rating(String app_property_average_user_rating) {
                this.app_property_average_user_rating = app_property_average_user_rating;
            }

            public int getApp_property_package_size_bytes() {
                return app_property_package_size_bytes;
            }

            public void setApp_property_package_size_bytes(int app_property_package_size_bytes) {
                this.app_property_package_size_bytes = app_property_package_size_bytes;
            }

            public String getApp_property_pkg_url() {
                return app_property_pkg_url;
            }

            public void setApp_property_pkg_url(String app_property_pkg_url) {
                this.app_property_pkg_url = app_property_pkg_url;
            }

           /* public List<String> getApp_property_genres() {
                return app_property_genres;
            }

            public void setApp_property_genres(List<String> app_property_genres) {
                this.app_property_genres = app_property_genres;
            }*/
        }

        public static class ProductTypeUnionAppInfo {
            private String app_property_packname;
            private String app_property_version;
            private String app_property_icon_url;
            private int app_property_package_size_bytes;
            private String app_property_pkg_md5;
            private String app_property_pkg_url;

            public String getApp_property_packname() {
                return app_property_packname;
            }

            public void setApp_property_packname(String app_property_packname) {
                this.app_property_packname = app_property_packname;
            }

            public String getApp_property_version() {
                return app_property_version;
            }

            public void setApp_property_version(String app_property_version) {
                this.app_property_version = app_property_version;
            }

            public String getApp_property_icon_url() {
                return app_property_icon_url;
            }

            public void setApp_property_icon_url(String app_property_icon_url) {
                this.app_property_icon_url = app_property_icon_url;
            }

            public int getApp_property_package_size_bytes() {
                return app_property_package_size_bytes;
            }

            public void setApp_property_package_size_bytes(int app_property_package_size_bytes) {
                this.app_property_package_size_bytes = app_property_package_size_bytes;
            }

            public String getApp_property_pkg_md5() {
                return app_property_pkg_md5;
            }

            public void setApp_property_pkg_md5(String app_property_pkg_md5) {
                this.app_property_pkg_md5 = app_property_pkg_md5;
            }

            public String getApp_property_pkg_url() {
                return app_property_pkg_url;
            }

            public void setApp_property_pkg_url(String app_property_pkg_url) {
                this.app_property_pkg_url = app_property_pkg_url;
            }
        }
    }
}
