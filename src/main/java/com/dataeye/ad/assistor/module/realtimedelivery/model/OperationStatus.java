package com.dataeye.ad.assistor.module.realtimedelivery.model;

import org.apache.commons.lang.StringUtils;

/**
 * 投放状态
 * Created by huangzehai on 2017/5/15.
 */
public enum OperationStatus {
    Enable(1), Disable(0);

    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    OperationStatus(int value) {
        this.value = value;
    }

    public static OperationStatus parse(String text) {
        for (OperationStatus status : OperationStatus.values()) {
            if (StringUtils.equalsIgnoreCase(status.name(), text)) {
                return status;
            }
        }
        return null;
    }

    public static OperationStatus parse(int value) {
        for (OperationStatus status : OperationStatus.values()) {
            if (status.getValue() == value){
                return status;
            }
        }
        return null;
    }

}
