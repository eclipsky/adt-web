package com.dataeye.ad.assistor.module.advertisement.service.tencent;



import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ConfiguredStatus;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ProductType;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.SpeedMode;
import com.dataeye.ad.assistor.module.advertisement.constants.InterfaceConstants.TencentInterface;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupVO;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.Campaign;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.Filtering;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.HttpUtil;
import com.dataeye.ad.assistor.util.RandomUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


/**
 * 广点通API
 * 推广计划Service
 * */
@Service("adCampaignService")
public class AdCampaignService {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AdCampaignService.class);

	/**
	 * 获取推广计划
	 * @param account_id
	 * @param campaign_id
	 * @param campaign_name  
	 * @return 
	 */
	public List<PlanGroupVO> query(Integer account_id,Integer campaign_id,String accessToken,String campaign_name) {
		//参数校验
		validateParamsByAccount(account_id, accessToken);
		// 1.构建请求参数
		String Tencent_params= StringUtils.substring(TencentInterface.TENCENT_PARAMETERS, 1);
		String params1 = HttpUtil.urlParamReplace(Tencent_params,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));

		StringBuilder params = new StringBuilder("");
		params.append(params1);
		params.append("&account_id="+account_id);
		if(campaign_id != null){
			params.append("&campaign_id="+campaign_id);
		}
		List<Filtering> filterList = new ArrayList<>();
		if(StringUtils.isNotBlank(campaign_name)){
			Filtering filterVo = new Filtering();
			filterVo.setField("campaign_name");
			filterVo.setOperator("EQUALS");
			List<String> filterValues = new ArrayList<>();
			filterValues.add(campaign_name);
			filterVo.setValues(filterValues);
			filterList.add(filterVo);
			
		}
		if(!filterList.isEmpty()){
			params.append("&filtering="+HttpUtil.urlEncode(JSONArray.fromObject(filterList).toString()));
		}                                                                                                                                                                                  
		String getCampaignInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.CAMPAIGN_GET;
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.get(getCampaignInterface,params.toString());
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>查询推广计划API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{getCampaignInterface, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>查询推广计划API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{getCampaignInterface, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
    	JsonObject data=jsonObject.get("data").getAsJsonObject();
    	JsonArray dataJsonObject = data.get("list").getAsJsonArray();
    	List<Campaign>  response = StringUtil.gson.fromJson(dataJsonObject, new TypeToken<List<Campaign>>(){}.getType());
    	List<PlanGroupVO> result = new ArrayList<>();
    	//得到计划信息
    	for (Campaign campaignVo : response) {
    		PlanGroupVO adGroupVO = new PlanGroupVO();
    		adGroupVO.setPlanGroupId(campaignVo.getCampaign_id());
			adGroupVO.setPlanGroupName(campaignVo.getCampaign_name());
			adGroupVO.setPlanGroupStatus(ConfiguredStatus.valueOf(campaignVo.getConfigured_status()).getValue());
			adGroupVO.setProductType(ProductType.valueOf(campaignVo.getProduct_type()).getValue());
			adGroupVO.setDailyBudget(CurrencyUtils.fenToYuan(new BigDecimal(campaignVo.getDaily_budget())));
			adGroupVO.setSpeedMode(SpeedMode.valueOf(campaignVo.getSpeed_mode()).getValue());
			result.add(adGroupVO);
		}
        return result;
	}
	
	/**
	 * 创建推广计划
	 * @param account_id
	 * @param campaign_name
	 * @param campaign_type
	 * @param product_type
	 * @param daily_budget 日消耗限额，单位为分，微信朋友圈广告（ campaign_type = CAMPAIGN_TYPE_WECHAT_MOMENTS ）不可使用，其他广告可使用。
	 * @param configured_status
	 * @param speed_mode
	 * @return campaign_id  推广计划 id
	 */
	public int add(Integer account_id,String campaign_name,String campaign_type,String product_type,Integer daily_budget,String speed_mode,String accessToken) {
		//参数校验
		validateParamsByAdd(account_id, accessToken, campaign_name, product_type);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("campaign_name", campaign_name);
		params.put("campaign_type", campaign_type==null?"CAMPAIGN_TYPE_NORMAL":campaign_type);
		params.put("product_type", product_type);
		params.put("daily_budget", daily_budget+"");
		params.put("configured_status", "AD_STATUS_NORMAL");
		params.put("speed_mode", speed_mode);
		
		String addCampaignInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.CAMPAIGN_ADD;
		String addCampaignInterfaceUrl = HttpUtil.urlParamReplace(addCampaignInterface,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(addCampaignInterfaceUrl, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>创建推广计划API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{addCampaignInterfaceUrl, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>创建推广计划API请求数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{addCampaignInterfaceUrl, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
    	JsonObject data = jsonObject.get("data").getAsJsonObject();    	
        return data.get("campaign_id").getAsInt();
	}
	
	/**
	 * 删除推广计划
	 * @param account_id
	 * @param campaign_name
	 * @return
	 */
	public void delete(Integer account_id,Integer campaign_id,String accessToken) {
		//参数校验
		validateParamsByCampaignId(account_id, accessToken, campaign_id);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("campaign_id", campaign_id+"");
		
		String deleteUrl = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.CAMPAIGN_DELETE;
		String deleteCampaignInterface = HttpUtil.urlParamReplace(deleteUrl,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(deleteCampaignInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>删除推广计划API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{deleteCampaignInterface, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		logger.error("<Tencent>删除推广计划API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{deleteCampaignInterface, params,code, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
	}
	
	/**
	 * 更新推广计划
	 * @param account_id（必填）
	 * @param campaign_id（必填）
	 * @param campaign_name
	 * @param daily_budget 日消耗限额，单位为分，微信朋友圈广告（ campaign_type = CAMPAIGN_TYPE_WECHAT_MOMENTS ）不可使用，其他广告可使用。
	 * @param configured_status
	 * @param speed_mode
	 * @return product_type 
	 */
	public int update(Integer account_id,Integer campaign_id,String campaign_name,Integer daily_budget,String configured_status,String speed_mode,String product_type,String accessToken) {
		validateParamsByCampaignId(account_id, accessToken, campaign_id);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("campaign_id", campaign_id+"");
		if(StringUtils.isNotBlank(campaign_name)){
			params.put("campaign_name", campaign_name);
		}
		if(daily_budget != null ){
			params.put("daily_budget", daily_budget+"");
		}
		if(StringUtils.isNotBlank(configured_status)){
			params.put("configured_status", configured_status);
		}
		if(StringUtils.isNotBlank(speed_mode)){
			params.put("speed_mode", speed_mode);
		}
		if(StringUtils.isNotBlank(product_type)){
			params.put("product_type", product_type);
		}
		String updateCampaignInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.CAMPAIGN_UPDATE;
		String updateCampaignInterfaceUrl = HttpUtil.urlParamReplace(updateCampaignInterface,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(updateCampaignInterfaceUrl, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>更新推广计划API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{updateCampaignInterfaceUrl, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>更新推广计划API请求数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{updateCampaignInterfaceUrl, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
    	JsonObject data = jsonObject.get("data").getAsJsonObject();    	
        return data.get("campaign_id").getAsInt();
	}
	
	
	
	//参数校验
	private void validateParamsByAccount(Integer account_id,String accessToken){
		if (account_id == null || account_id == 0) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCOUNT_ID_IS_NULL);
        }
        if (StringUtils.isBlank(accessToken)) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCESSTOKEN_ID_IS_NULL);
        }
	}
	
	private void validateParamsByAdd(Integer account_id,String accessToken,String campaign_name,String product_type){
		validateParamsByAccount(account_id, accessToken);
		if (StringUtils.isBlank(campaign_name)) {
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_NAME_IS_NULL);
        }
		if (StringUtils.isBlank(product_type) || StringUtils.isBlank(ProductType.valueOf(product_type).name())) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
        }
	}
	
	private void validateParamsByCampaignId(Integer account_id,String accessToken,Integer campaign_id){
		validateParamsByAccount(account_id, accessToken);
		if (campaign_id == null || campaign_id == 0) {
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_ID_IS_NULL);
        }
	}
	
	

}
