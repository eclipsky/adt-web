package com.dataeye.ad.assistor.module.realtimedelivery.model;

import java.util.Date;

/**
 * Created by huangzehai on 2017/4/27.
 */
public class DailyBalance extends Balance {
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
