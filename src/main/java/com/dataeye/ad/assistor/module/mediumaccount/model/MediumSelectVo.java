package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.List;

import com.google.gson.annotations.Expose;

public class MediumSelectVo {
	
	/** 媒体ID */
	@Expose
	private Integer mediumId;
	/** 媒体名称 */
	@Expose
	private String mediumName;
	/** adt媒体对应的渠道id */
	@Expose
	private String channelId;
	/** tracking渠道集合*/
	@Expose
	private List<Channel> list;
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	public List<Channel> getList() {
		return list;
	}
	public void setList(List<Channel> list) {
		this.list = list;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	@Override
	public String toString() {
		return "MediumSelectVo [mediumId=" + mediumId + ", mediumName="
				+ mediumName + ", channelId=" + channelId + ", list=" + list
				+ "]";
	}
	
	
}
