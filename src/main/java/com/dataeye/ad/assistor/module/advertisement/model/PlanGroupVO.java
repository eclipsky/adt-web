package com.dataeye.ad.assistor.module.advertisement.model;

import java.math.BigDecimal;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * 推广计划组
 * Created by ldj
 */
public class PlanGroupVO {

	/**计划组id**/
	@Expose
    private Integer planGroupId;
	/**计划组名称**/
	@Expose
    private String planGroupName;
	/**日限额，单位为元，精确到分**/
	@Expose
    private BigDecimal dailyBudget;
	/**投放速度模式**/
	@Expose
    private Integer speedMode;
	/**客户设置的状态，即开关操作*/
	@Expose
    private Integer planGroupStatus;
	/**标的物类型*/
	@Expose
	private Integer productType;
	
	/**媒体帐号*/
	@Expose
	private String mediumAccountName;
	
	/**产品名称列表*/
	@Expose
	private List<String> productNameList;
	@Expose
	private Integer mediumAccountId;
	@Expose
	private Integer mediumId;
	@Expose
	private String mediumName;
	
	private Integer productId;
	
	private String productNames;
	private String productIds;
	private Integer planGroupType;
	
	/**资金状态*/
	@Expose
	private Integer fund_status;

	public Integer getPlanGroupId() {
		return planGroupId;
	}
	public void setPlanGroupId(Integer planGroupId) {
		this.planGroupId = planGroupId;
	}
	public String getPlanGroupName() {
		return planGroupName;
	}
	public void setPlanGroupName(String planGroupName) {
		this.planGroupName = planGroupName;
	}
	public Integer getSpeedMode() {
		return speedMode;
	}
	public void setSpeedMode(Integer speedMode) {
		this.speedMode = speedMode;
	}
	public Integer getPlanGroupStatus() {
		return planGroupStatus;
	}
	public void setPlanGroupStatus(Integer planGroupStatus) {
		this.planGroupStatus = planGroupStatus;
	}
	public String getMediumAccountName() {
		return mediumAccountName;
	}
	public void setMediumAccountName(String mediumAccountName) {
		this.mediumAccountName = mediumAccountName;
	}
	public List<String> getProductNameList() {
		return productNameList;
	}
	public void setProductNameList(List<String> productNameList) {
		this.productNameList = productNameList;
	}
	
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductNames() {
		return productNames;
	}
	public void setProductNames(String productNames) {
		this.productNames = productNames;
	}
	
	public Integer getProductType() {
		return productType;
	}
	public void setProductType(Integer productType) {
		this.productType = productType;
	}
	public String getProductIds() {
		return productIds;
	}
	public void setProductIds(String productIds) {
		this.productIds = productIds;
	}
	
	public Integer getPlanGroupType() {
		return planGroupType;
	}
	public void setPlanGroupType(Integer planGroupType) {
		this.planGroupType = planGroupType;
	}
	public BigDecimal getDailyBudget() {
		return dailyBudget;
	}
	public void setDailyBudget(BigDecimal dailyBudget) {
		this.dailyBudget = dailyBudget;
	}
	
	public Integer getFund_status() {
		return fund_status;
	}
	public void setFund_status(Integer fund_status) {
		this.fund_status = fund_status;
	}
	@Override
	public String toString() {
		return "PlanGroupVO [ planGroupId=" + planGroupId
				+ ", planGroupName=" + planGroupName + ", dailyBudget="
				+ dailyBudget + ", speedMode=" + speedMode
				+ ", planGroupStatus=" + planGroupStatus + ", productType="
				+ productType + ", mediumAccountName=" + mediumAccountName
				+ ", productNameList=" + productNameList + ", mediumAccountId="
				+ mediumAccountId + ", mediumId=" + mediumId + ", mediumName="
				+ mediumName + ", productId=" + productId + ", productNames="
				+ productNames + ", productIds=" + productIds
				+ ", planGroupType=" + planGroupType + ", fund_status="
				+ fund_status + "]";
	}

	
	
}
