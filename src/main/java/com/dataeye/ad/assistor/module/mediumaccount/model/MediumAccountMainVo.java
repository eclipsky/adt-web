//package com.dataeye.ad.assistor.module.mediumaccount.model;
//
//import com.google.gson.annotations.Expose;
//
///**
// * Created by huangzehai on 2016/12/27.
// * 账号
// */
//public class MediumAccountMainVo {
//    /**
//     * 账号ID.
//     */
//    @Expose
//    private long id;
//    /**
//     * 账号名称.
//     */
//    @Expose
//    private String name;
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    @Override
//    public String toString() {
//        return "MediumAccountMainInfoVo{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                '}';
//    }
//}
