package com.dataeye.ad.assistor.module.company.model;

import java.util.Date;

/**
 * 公司信息
 * @author luzhuyou 2017/05/10
 *
 */
public class Company {

	/** 公司ID */
	private Integer companyId;
	/** 公司名称 */
	private String companyName;
	/** 统一登录用户ID */
	private Integer uid;
	/** 统一登录用户名称 */
	private String userName;
	/** 邮箱，登录账号（统一登录用户:即UserID） */
	private String email;
	/** 电话 */
	private String tel;
	/** QQ */
	private String qq;
	/** 状态：0-正常，1-待商务审核，2-暂停，3-拒绝，4-待运营审核，5-冻结，6-停用 */
	private int status;
	/** 商务处理人ID */
	private int bizHandlerId;
	/** 运营处理人ID */
	private int operHandlerId;
	/** 付费类型：0-试用，1-付费 */
	private int payType;
	/** 接入sdk：0-是，1-否 */
	private int accessSdk;
	/** 到期提醒：0-不提醒，1-提前7天，2-提前14天，3-提前30天 */
	private int expireRemindDays;
	/** 商务审批意见 */
	private String businessView;
	/** 运营审批意见 */
	private String operatorView;
	/** 生效日期 */
	private Date effectDate;
	/** 失效日期 */
	private Date invalidDate;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getBizHandlerId() {
		return bizHandlerId;
	}
	public void setBizHandlerId(int bizHandlerId) {
		this.bizHandlerId = bizHandlerId;
	}
	public int getOperHandlerId() {
		return operHandlerId;
	}
	public void setOperHandlerId(int operHandlerId) {
		this.operHandlerId = operHandlerId;
	}
	public int getPayType() {
		return payType;
	}
	public void setPayType(int payType) {
		this.payType = payType;
	}
	public int getAccessSdk() {
		return accessSdk;
	}
	public void setAccessSdk(int accessSdk) {
		this.accessSdk = accessSdk;
	}
	public int getExpireRemindDays() {
		return expireRemindDays;
	}
	public void setExpireRemindDays(int expireRemindDays) {
		this.expireRemindDays = expireRemindDays;
	}
	public String getBusinessView() {
		return businessView;
	}
	public void setBusinessView(String businessView) {
		this.businessView = businessView;
	}
	public String getOperatorView() {
		return operatorView;
	}
	public void setOperatorView(String operatorView) {
		this.operatorView = operatorView;
	}
	public Date getEffectDate() {
		return effectDate;
	}
	public void setEffectDate(Date effectDate) {
		this.effectDate = effectDate;
	}
	public Date getInvalidDate() {
		return invalidDate;
	}
	public void setInvalidDate(Date invalidDate) {
		this.invalidDate = invalidDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "Company [companyId=" + companyId + ", companyName="
				+ companyName + ", uid=" + uid + ", userName=" + userName
				+ ", email=" + email + ", tel=" + tel + ", qq=" + qq
				+ ", status=" + status + ", bizHandlerId=" + bizHandlerId
				+ ", operHandlerId=" + operHandlerId + ", payType=" + payType
				+ ", accessSdk=" + accessSdk + ", expireRemindDays="
				+ expireRemindDays + ", businessView=" + businessView
				+ ", operatorView=" + operatorView + ", effectDate="
				+ effectDate + ", invalidDate=" + invalidDate + ", remark="
				+ remark + ", createTime=" + createTime + ", updateTime="
				+ updateTime + "]";
	}
}
