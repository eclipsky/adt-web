package com.dataeye.ad.assistor.module.report.model;

import com.google.gson.annotations.Expose;

/**
 * Created by huangzehai on 2017/1/5.
 */
public class Link {
    @Expose
    private String label;
    @Expose
    private LinkID id;
    @Expose
    private String value;
    /**
     * 是否是链接.
     */
    @Expose
    private boolean isLink = false;
    /**
     * 是否弹出窗口
     */
    @Expose
    private boolean isPopup;

    /**
     * 是否汇总数据
     */
    @Expose
    private boolean isTotal = false;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LinkID getId() {
        return id;
    }

    public void setId(LinkID id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isLink() {
        return isLink;
    }

    public void setLink(boolean link) {
        isLink = link;
    }

    public boolean isPopup() {
        return isPopup;
    }

    public void setPopup(boolean popup) {
        isPopup = popup;
    }

    public boolean isTotal() {
        return isTotal;
    }

    public void setTotal(boolean total) {
        isTotal = total;
    }

    @Override
    public String toString() {
        return "Link{" +
                "label='" + label + '\'' +
                ", id=" + id +
                ", value='" + value + '\'' +
                ", isLink=" + isLink +
                ", isPopup=" + isPopup +
                ", isTotal=" + isTotal +
                '}';
    }
}
