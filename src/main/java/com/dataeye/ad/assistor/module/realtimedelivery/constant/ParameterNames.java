package com.dataeye.ad.assistor.module.realtimedelivery.constant;

/**
 * Created by huangzehai on 2017/2/28.
 */
public final class ParameterNames {
    private ParameterNames() {

    }

    public static final String ID = "id";
    public static final String PLAN_ID = "planID";
    public static final String PERIOD = "period";
    public static final String COST_OPERATOR = "costOperator";
    public static final String COST = "cost";
    public static final String CPA_OPERATOR = "cpaOperator";
    public static final String COST_PER_REGISTRATION = "costPerRegistration";
    public static final String CTR_OPERATOR = "ctrOperator";
    public static final String CTR = "ctr";
    public static final String DOWNLOAD_RATE_OPERATOR = "downloadRateOperator";
    public static final String DOWNLOAD_RATE = "downloadRate";
    @Deprecated
    public static final String COST_PER_DOWNLOAD_OPERATOR = "costPerDownloadOperator";
    @Deprecated
    public static final String COST_PER_DOWNLOAD = "costPerDownload";
    public static final String EMAIL_ALERT = "emailAlert";
    //高级告警
    public static final String ADVANCED_ID = "advancedId";
    public static final String ADVANCED_COST_OPERATOR = "advancedCostOperator";
    public static final String ADVANCED_COST = "advancedCost";
    public static final String ADVANCED_CPA_OPERATOR = "advancedCpaOperator";
    public static final String ADVANCED_COST_PER_REGISTRATION = "advancedCostPerRegistration";
    public static final String ADVANCED_CTR_OPERATOR = "advancedCtrOperator";
    public static final String ADVANCED_CTR = "advancedCtr";
    public static final String ADVANCED_DOWNLOAD_RATE_OPERATOR = "advancedDownloadRateOperator";
    public static final String ADVANCED_DOWNLOAD_RATE = "advancedDownloadRate";
    public static final String ADVANCED_COST_PER_DOWNLOAD_OPERATOR = "advancedCostPerDownloadOperator";
    public static final String ADVANCED_COST_PER_DOWNLOAD = "advancedCostPerDownload";
}
