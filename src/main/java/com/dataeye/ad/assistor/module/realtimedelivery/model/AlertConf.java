package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;

/**
 * 告警配置
 *
 * @author luzhuyou
 */
public class AlertConf implements Cloneable {
    /**
     * ID
     */
    @Expose
    private int id;
    /**
     * 计划ID
     */
    @Expose
    private int planID;
    /**
     * 计划名称
     */
    @Expose
    private String planName;
    /**
     * 媒体账号ID
     */
    @Expose
    private int mediumAccountId;
    /**
     * 消耗
     */
    @Expose
    private BigDecimal cost;
    /**
     * CPA
     */
    @Expose
    private BigDecimal costPerRegistration;
    /**
     * CTR
     */
    @Expose
    private BigDecimal ctr;
    /**
     * 下载率
     */
    @Expose
    private BigDecimal downloadRate;
    /**
     * 是否邮件告警
     */
    @Expose
    private int emailAlert;

    /**
     * 下载单价
     */
    @Expose
    private BigDecimal costPerDownload;
    /**
     * 时段.
     */
    @Expose
    private int period;

    /**
     * 告警类型.
     */
    @Expose
    private int type;

    /**
     * 消耗操作符
     */
    @Expose
    private int costOperator;

    /**
     * CPA操作符.
     */
    @Expose
    private int cpaOperator;

    /**
     * CRT操作符.
     */
    @Expose
    private int ctrOperator;

    /**
     * 下载率操作符.
     */
    @Expose
    private int downloadRateOperator;

    /**
     * 下载单价操作符.
     */
    @Expose
    private int costPerDownloadOperator;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlanID() {
        return planID;
    }

    public void setPlanID(int planID) {
        this.planID = planID;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public int getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(int mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getCostPerRegistration() {
        return costPerRegistration;
    }

    public void setCostPerRegistration(BigDecimal costPerRegistration) {
        this.costPerRegistration = costPerRegistration;
    }

    public BigDecimal getCtr() {
        return ctr;
    }

    public void setCtr(BigDecimal ctr) {
        this.ctr = ctr;
    }

    public BigDecimal getDownloadRate() {
        return downloadRate;
    }

    public void setDownloadRate(BigDecimal downloadRate) {
        this.downloadRate = downloadRate;
    }

    public int getEmailAlert() {
        return emailAlert;
    }

    public void setEmailAlert(int emailAlert) {
        this.emailAlert = emailAlert;
    }

    public BigDecimal getCostPerDownload() {
        return costPerDownload;
    }

    public void setCostPerDownload(BigDecimal costPerDownload) {
        this.costPerDownload = costPerDownload;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCostOperator() {
        return costOperator;
    }

    public void setCostOperator(int costOperator) {
        this.costOperator = costOperator;
    }

    public int getCpaOperator() {
        return cpaOperator;
    }

    public void setCpaOperator(int cpaOperator) {
        this.cpaOperator = cpaOperator;
    }

    public int getCtrOperator() {
        return ctrOperator;
    }

    public void setCtrOperator(int ctrOperator) {
        this.ctrOperator = ctrOperator;
    }

    public int getDownloadRateOperator() {
        return downloadRateOperator;
    }

    public void setDownloadRateOperator(int downloadRateOperator) {
        this.downloadRateOperator = downloadRateOperator;
    }

    public int getCostPerDownloadOperator() {
        return costPerDownloadOperator;
    }

    public void setCostPerDownloadOperator(int costPerDownloadOperator) {
        this.costPerDownloadOperator = costPerDownloadOperator;
    }

    @Override
    public AlertConf clone() throws CloneNotSupportedException {
        return (AlertConf) super.clone();
    }

    @Override
    public String toString() {
        return "AlertConf [id=" + id + ", planID=" + planID + ", planName="
                + planName + ", mediumAccountId=" + mediumAccountId + ", cost="
                + cost + ", costPerRegistration=" + costPerRegistration
                + ", ctr=" + ctr + ", downloadRate=" + downloadRate
                + ", emailAlert=" + emailAlert + ", costPerDownload="
                + costPerDownload + ", period=" + period + ", type=" + type
                + ", costOperator=" + costOperator + ", cpaOperator="
                + cpaOperator + ", ctrOperator=" + ctrOperator
                + ", downloadRateOperator=" + downloadRateOperator
                + ", costPerDownloadOperator=" + costPerDownloadOperator + "]";
    }

}
