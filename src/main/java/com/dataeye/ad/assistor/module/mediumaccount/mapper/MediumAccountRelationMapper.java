package com.dataeye.ad.assistor.module.mediumaccount.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountRelation;

import java.util.List;

/**
 * 媒体账号关联关系映射器.
 * Created by luzhuyou 2017/04/20
 */
@MapperScan
public interface MediumAccountRelationMapper {

    /**
     * 新增媒体账号关联关系
     *
     * @param accountRelation
     * @return
     */
    public int add(MediumAccountRelation accountRelation);

    /**
     * 修改媒体账号关联关系信息
     *
     * @param accountRelation
     */
    public int update(MediumAccountRelation accountRelation);
    
    /**
     * 删除媒体账号关联关系信息
     *
     * @param accountRelation
     */
    public int delete(MediumAccountRelation accountRelation);
    
    /**
     * 得到媒体账号关联关系信息
     *
     * @param accountRelation
     */
    public List<MediumAccountRelation> getMediumAccountRelation(MediumAccountRelation accountRelation);
    
    
}
