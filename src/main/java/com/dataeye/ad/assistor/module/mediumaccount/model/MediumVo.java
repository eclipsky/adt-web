package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.List;

import com.google.gson.annotations.Expose;

public class MediumVo {
	
	/** 媒体ID */
	@Expose
	private Integer mediumId;
	/** 媒体名称 */
	@Expose
	private String mediumName;
	/** 爬取页面集合*/
	@Expose
	private List<CrawlerPage> list;
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	public List<CrawlerPage> getList() {
		return list;
	}
	public void setList(List<CrawlerPage> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		return "MediumVo [mediumId=" + mediumId + ", mediumName=" + mediumName
				+ ", list=" + list + "]";
	}
	
	
}
