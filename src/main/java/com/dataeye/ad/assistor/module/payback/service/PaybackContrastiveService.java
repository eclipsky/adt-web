package com.dataeye.ad.assistor.module.payback.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.common.DailyRecord;
import com.dataeye.ad.assistor.common.TrendChart;
import com.dataeye.ad.assistor.module.payback.mapper.PaybackContrastiveMapper;
import com.dataeye.ad.assistor.module.payback.model.PaybackQuery;
import com.dataeye.ad.assistor.module.report.model.Period;
import com.dataeye.ad.assistor.util.TrendChartUtils;


@Service("paybackContrastiveService")
public class PaybackContrastiveService {
   
	@Autowired
	private PaybackContrastiveMapper payBackContrastiveMapper;
	
	/**
	 * 
	 * 回本对比分析--推广计划（按天）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveDailyQueryByPlan(PaybackQuery query) {
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackDailyQueryByPlan(query);
		return TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate() , Period.Day , payBackRateList, new BigDecimal(0.0),true,isDefaultChartGroupPlan(query));
	}
	
	/**
	 * 
	 * 回本对比分析--推广计划（按周）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveWeeklyQueryByPlan(PaybackQuery query) throws Exception{
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackWeekQueryByPlan(query);
		return TrendChartUtils.toTrendChartByWeek(query.getStartDate(), query.getEndDate(), payBackRateList, new BigDecimal(0.0), true,isDefaultChartGroupPlan(query));
	}
	
	/**
	 * 
	 * 回本对比分析--推广计划（按月）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveMonthlyQueryByPlan(PaybackQuery query) {
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackMonthlyQueryByPlan(query);
		return TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate() , Period.Month , payBackRateList, new BigDecimal(0.0),true,isDefaultChartGroupPlan(query));
	}
	
	/**
	 * 
	 * 回本对比分析--媒体帐号（按天）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveDailyQueryByMediumAccount(PaybackQuery query) {
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackDailyQueryByMediumAccount(query);
		return TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate() , Period.Day , payBackRateList, new BigDecimal(0.0),true,isDefaultChartGroupMediumAccount(query));
	}
	
	/**
	 * 
	 * 回本对比分析--媒体帐号（按周）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveWeeklyQueryByMediumAccount(PaybackQuery query) throws Exception{
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackWeekQueryByMediumAccount(query);
		return TrendChartUtils.toTrendChartByWeek(query.getStartDate(), query.getEndDate(), payBackRateList, new BigDecimal(0.0), true,isDefaultChartGroupMediumAccount(query));
	}
	
	/**
	 * 
	 * 回本对比分析--媒体帐号（按月）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveMonthlyQueryByMediumAccount(PaybackQuery query) {
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackMonthlyQueryByMediumAccount(query);
		return TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate() , Period.Month , payBackRateList, new BigDecimal(0.0),true,isDefaultChartGroupMediumAccount(query));
	}

	/**
	 * 
	 * 回本对比分析--媒体（按天）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveDailyQueryByMedium(PaybackQuery query) {
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackDailyQueryByMedium(query);
		return TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate() , Period.Day , payBackRateList, new BigDecimal(0.0),true,isDefaultChartGroupMedium(query));
	}
	
	/**
	 * 
	 * 回本对比分析--媒体（按周）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveWeeklyQueryByMedium(PaybackQuery query)throws Exception {
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackWeekQueryByMedium(query);
		return TrendChartUtils.toTrendChartByWeek(query.getStartDate(), query.getEndDate(), payBackRateList, new BigDecimal(0.0), true,isDefaultChartGroupMedium(query));
	}
	
	/**
	 * 
	 * 回本对比分析--媒体（按月）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveMonthlyQueryByMedium(PaybackQuery query) {
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackMonthlyQueryByMedium(query);
		return TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate() , Period.Month , payBackRateList, new BigDecimal(0.0),true,isDefaultChartGroupMedium(query));
	}
	
	/**
	 * 
	 * 回本对比分析--产品（按天）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveDailyQueryByProduct(PaybackQuery query) {
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackDailyQueryByProduct(query);
		return TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate() , Period.Day , payBackRateList, new BigDecimal(0.0),true,isDefaultChartGroupProduct(query));
	}
	
	/**
	 * 
	 * 回本对比分析--产品（按周）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveWeeklyQueryByProduct(PaybackQuery query)throws Exception {
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackWeekQueryByProduct(query);
		return TrendChartUtils.toTrendChartByWeek(query.getStartDate(), query.getEndDate(), payBackRateList, new BigDecimal(0.0), true,isDefaultChartGroupProduct(query));
	}
	
	/**
	 * 
	 * 回本对比分析--产品（按月）
	 * 
	 * */
	public TrendChart<String, BigDecimal> payBackContrastiveMonthlyQueryByProduct(PaybackQuery query) {
		List<DailyRecord<String, BigDecimal>> payBackRateList = payBackContrastiveMapper.payBackMonthlyQueryByProduct(query);
		return TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate() , Period.Month , payBackRateList, new BigDecimal(0.0),true,isDefaultChartGroupProduct(query));
	}
	
	
	private boolean isDefaultChartGroupPlan(PaybackQuery query) {
        return (query.getPlanIds() == null || query.getPlanIds().isEmpty());
    }

	private boolean isDefaultChartGroupMediumAccount(PaybackQuery query) {
        return (query.getAccountIds() == null || query.getAccountIds().isEmpty());
    }
	private boolean isDefaultChartGroupMedium(PaybackQuery query) {
        return (query.getMediumIds() == null || query.getMediumIds().isEmpty());
    }
	private boolean isDefaultChartGroupProduct(PaybackQuery query) {
        return (query.getProductIds() == null || query.getProductIds().isEmpty());
    }
}
