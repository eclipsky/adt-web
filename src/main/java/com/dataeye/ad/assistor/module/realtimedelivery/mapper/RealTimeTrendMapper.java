package com.dataeye.ad.assistor.module.realtimedelivery.mapper;

import com.dataeye.ad.assistor.module.realtimedelivery.model.*;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * Created by huangzehai on 2017/2/24.
 */
@MapperScan
public interface RealTimeTrendMapper {
    /**
     * 实时趋势
     *
     * @param trendQuery 查询条件
     * @return
     */
    List<RealTimeTrend> realTimeTrend(TrendQuery trendQuery);

    /**
     * 获取落地页指标
     *
     * @param trendQuery
     * @return
     */
    List<PageStat> getPageStatById(TrendQuery trendQuery);

    /**
     * 获取CP指标
     *
     * @param trendQuery
     * @return
     */
    List<PackageStat> getPackageStatById(TrendQuery trendQuery);

    /**
     * 获取最近一段时间的十分钟实时报告，供高级报警使用
     *
     * @param realTimeReportQuery
     * @return
     */
    List<RealTimeReport> realTimePlanStatInTenMinutes(RealTimeReportQuery realTimeReportQuery);

    /**
     * 获取最近一段时间的十分钟落地页指标
     *
     * @param realTimeReportQuery
     * @return
     */
    List<PageStat> realTimePageStatInTenMinutes(RealTimeReportQuery realTimeReportQuery);

    /**
     * 获取最近一段时间的十分钟CP指标
     *
     * @param realTimeReportQuery
     * @return
     */
    List<PackageStat> realTimePackageStatInTenMinutes(RealTimeReportQuery realTimeReportQuery);

    /**
     * 获取指定计划的关联关系
     *
     * @param planId
     * @return
     */
    Association getAssociationByPlan(@Param("planId") int planId);

    List<RealTimeReport> getPlanIndicatorAtTime(RealTimeReportQuery realTimeReportQuery);

    List<PageStat> getPageIndicatorAtTime(RealTimeReportQuery realTimeReportQuery);

    List<PackageStat> getPackageIndicatorAtTime(RealTimeReportQuery realTimeReportQuery);
}
