package com.dataeye.ad.assistor.module.payback.service;

import com.dataeye.ad.assistor.common.Chart;
import com.dataeye.ad.assistor.common.DailyRecord;
import com.dataeye.ad.assistor.common.TrendChart;
import com.dataeye.ad.assistor.module.naturalflow.model.DailyPaybackNaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlowQuery;
import com.dataeye.ad.assistor.module.naturalflow.service.NaturalFlowService;
import com.dataeye.ad.assistor.module.payback.mapper.PaybackReportMapper;
import com.dataeye.ad.assistor.module.payback.model.*;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.NumberUtils;
import com.dataeye.ad.assistor.util.TrendChartUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class LtvServiceImpl implements LtvService {
    /**
     * 折扣率趋势图显示天数.
     */
    private static final int DAYS = 90;

    private static final String LTV = "LTV";
    private static final String CPA = "%s注册CPA";

    private static final long ONE_DAY = 86400;

    @Autowired
    private PaybackReportMapper paybackReportMapper;

    @Autowired
    private NaturalFlowService naturalFlowService;

    /**
     * LTV日报
     *
     * @param query
     * @return
     */
    @Override
    public List<LtvDailyReport> ltvDailyReport(PaybackQuery query) {
        //单位：分
        List<DailyPaybackReport> reports = paybackReportMapper.dailyPaybackLtvReport(query);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            NaturalFlowQuery naturalFlowQuery = new NaturalFlowQuery();
            naturalFlowQuery.setStartDate(query.getStartDate());
            naturalFlowQuery.setEndDate(query.getEndDate());
            naturalFlowQuery.setCompanyId(query.getCompanyId());
            naturalFlowQuery.setProductIds(query.getProductIds());
            List<DailyPaybackNaturalFlow> naturalFlows = naturalFlowService.getDailyPaybackLtvNaturalFlowsGroupByDate(naturalFlowQuery);
            Map<Date, DailyPaybackNaturalFlow> naturalFlowsByDate = groupByDate(naturalFlows);
            for (DailyPaybackReport report : reports) {
                if (naturalFlowsByDate.containsKey(report.getDate())) {
                    DailyPaybackNaturalFlow naturalFlow = naturalFlowsByDate.get(report.getDate());
                    //累计充值
                    report.setRegistrations(NumberUtils.add(report.getRegistrations(), naturalFlow.getRegistrations()));
                    report.setPayAmount1Day(NumberUtils.add(report.getPayAmount1Day(), naturalFlow.getPayAmount1day()));
                    report.setPayAmount2Days(NumberUtils.add(report.getPayAmount2Days(), naturalFlow.getPayAmount2days()));
                    report.setPayAmount3Days(NumberUtils.add(report.getPayAmount3Days(), naturalFlow.getPayAmount3days()));
                    report.setPayAmount4Days(NumberUtils.add(report.getPayAmount4Days(), naturalFlow.getPayAmount4days()));
                    report.setPayAmount5Days(NumberUtils.add(report.getPayAmount5Days(), naturalFlow.getPayAmount5days()));
                    report.setPayAmount6Days(NumberUtils.add(report.getPayAmount6Days(), naturalFlow.getPayAmount6days()));
                    report.setPayAmount7Days(NumberUtils.add(report.getPayAmount7Days(), naturalFlow.getPayAmount7days()));
                    report.setPayAmount15Days(NumberUtils.add(report.getPayAmount15Days(), naturalFlow.getPayAmount15days()));
                    report.setPayAmount30Days(NumberUtils.add(report.getPayAmount30Days(), naturalFlow.getPayAmount30days()));
                    report.setPayAmount60Days(NumberUtils.add(report.getPayAmount60Days(), naturalFlow.getPayAmount60days()));
                    report.setPayAmount90Days(NumberUtils.add(report.getPayAmount90Days(), naturalFlow.getPayAmount90days()));
                }
            }
        }
        return computeLTV(reports);
    }

    @Override
    public TrendChart<String, BigDecimal> ltvTrendChart(PaybackTrendQuery query) {
        //获取指定日期的消耗和注册,消耗单位为分
        Ltv ltv = paybackReportMapper.getLtvByDate(query);
        //获取每日充值金额，单位为分
        List<PaybackRate> dailyPayAmounts = paybackReportMapper.dailyPaybackRateTrend(query);

        //注册数
        Integer registrations = ltv == null ? null : ltv.getRegistrations();
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            NaturalFlowQuery naturalFlowQuery = new NaturalFlowQuery();
            naturalFlowQuery.setDate(query.getDate());
            naturalFlowQuery.setStartDate(query.getDate());
            naturalFlowQuery.setEndDate(query.getDate());
            naturalFlowQuery.setProductIds(query.getProductIds());
            naturalFlowQuery.setCompanyId(query.getCompanyId());
            //添加自然流量充值金额
            dailyPayAmounts = addNaturalFlow(naturalFlowQuery, dailyPayAmounts);
            //添加自然流量注册数
            NaturalFlow naturalFlow = naturalFlowService.getNaturalFlowTotal(naturalFlowQuery);
            if (naturalFlow != null) {
                registrations = NumberUtils.add(registrations, naturalFlow.getRegistrations());
            }
        }

        //LTV趋势图
        BigDecimal accumulativePayAmount = BigDecimal.valueOf(0);
        List<DailyRecord<String, BigDecimal>> records = new ArrayList<>();
        boolean isDefaultLTVChart = false;
        if (dailyPayAmounts != null && !dailyPayAmounts.isEmpty()) {
            for (PaybackRate payAmount : dailyPayAmounts) {
                accumulativePayAmount = NumberUtils.add(accumulativePayAmount, payAmount.getPayAmount());
                DailyRecord<String, BigDecimal> record = new DailyRecord<>();
                record.setDate(payAmount.getPayDate());
                record.setName(LTV);
                record.setValue(NumberUtils.cpa(CurrencyUtils.fenToYuan(accumulativePayAmount), registrations));
                records.add(record);
            }
        } else {
            //添加默认LTV趋势图
            DailyRecord<String, BigDecimal> record = new DailyRecord<>();
            record.setDate(query.getDate());
            record.setName(LTV);
            record.setValue(BigDecimal.valueOf(0));
            records.add(record);
            isDefaultLTVChart = true;
        }

        //CPA
        Date endDate = getEndDate(query.getDate());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(query.getDate());
        BigDecimal cpa;
        if (ltv == null) {
            cpa = BigDecimal.valueOf(0);
        } else {
            cpa = NumberUtils.cpa(CurrencyUtils.fenToYuan(ltv.getCost()), registrations);
        }
        while (!calendar.getTime().after(endDate)) {
            DailyRecord<String, BigDecimal> record = new DailyRecord<>();
            record.setDate(calendar.getTime());
            record.setName(String.format(CPA, DateUtils.format(query.getDate(), DateUtils.MONTH_DATE_FORMAT)));
            record.setValue(cpa);
            records.add(record);
            calendar.add(Calendar.DATE, 1);
        }

        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toAccumulativeTrendChart(query.getDate(), endDate, records, BigDecimal.valueOf(0), false, false);
        //设置标志位，供前端将0替换为“-”
        if (trendChart != null) {
            if (trendChart.getIndicators() != null) {
                for (Chart<String, BigDecimal> chart : trendChart.getIndicators()) {
                    if (StringUtils.equalsIgnoreCase(chart.getTitle(), LTV)) {
                        chart.setFlag(isDefaultLTVChart);
                    }
                }
            }
        }
        return trendChart;
    }

    /**
     * 添加自然流量
     *
     * @param naturalFlowQuery
     * @param dailyPayAmounts
     */
    private List<PaybackRate> addNaturalFlow(NaturalFlowQuery naturalFlowQuery, final List<PaybackRate> dailyPayAmounts) {
        Map<Date, PaybackRate> payAmountsByDate = groupByPayDate(dailyPayAmounts);
        //单位为分
        List<NaturalFlow> naturalFlows = naturalFlowService.dailyNaturalFlowPayAmount(naturalFlowQuery);
        List<PaybackRate> mergedDailyPayAmount = dailyPayAmounts;
        if (naturalFlows != null && !naturalFlows.isEmpty()) {
            for (NaturalFlow naturalFlow : naturalFlows) {
                if (payAmountsByDate.containsKey(naturalFlow.getStatDate())) {
                    PaybackRate payAmount = payAmountsByDate.get(naturalFlow.getStatDate());
                    payAmount.setPayAmount(NumberUtils.add(payAmount.getPayAmount(), naturalFlow.getRecharge()));
                } else {
                    PaybackRate payAmount = new PaybackRate();
                    payAmount.setPayDate(naturalFlow.getStatDate());
                    payAmount.setPayAmount(naturalFlow.getRecharge());
                    payAmountsByDate.put(naturalFlow.getStatDate(), payAmount);
                }
            }
            mergedDailyPayAmount = new ArrayList<>(payAmountsByDate.values());
            Collections.sort(mergedDailyPayAmount, new Comparator<PaybackRate>() {
                @Override
                public int compare(PaybackRate o1, PaybackRate o2) {
                    return o1.getPayDate().compareTo(o2.getPayDate());
                }
            });
        }
        return mergedDailyPayAmount;
    }

    /**
     * 获取付费截止日期
     *
     * @param startDate
     * @return
     */
    private Date getEndDate(Date startDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        //包含截止日期，故减一
        calendar.add(Calendar.DATE, DAYS - 1);
        return calendar.getTime();
    }

    /**
     * 按付费日期分组
     *
     * @param dailyPayAmounts
     * @return
     */
    private Map<Date, PaybackRate> groupByPayDate(List<PaybackRate> dailyPayAmounts) {
        Map<Date, PaybackRate> map = new HashMap<>();
        if (dailyPayAmounts != null) {
            for (PaybackRate dailyPayAmount : dailyPayAmounts) {
                map.put(dailyPayAmount.getPayDate(), dailyPayAmount);
            }
        }
        return map;
    }

    /**
     * 按日期分组
     *
     * @param naturalFlows
     * @return
     */
    private <T extends NaturalFlow> Map<Date, T> groupByDate(List<T> naturalFlows) {
        Map<Date, T> naturalFlowsByDate = new HashMap<>();
        if (naturalFlows != null) {
            for (T naturalFlow : naturalFlows) {
                naturalFlowsByDate.put(naturalFlow.getStatDate(), naturalFlow);
            }
        }
        return naturalFlowsByDate;
    }

    /**
     * 计算LTV
     *
     * @param reports
     */
    private List<LtvDailyReport> computeLTV(List<DailyPaybackReport> reports) {
        List<LtvDailyReport> views = new ArrayList<>();
        long todayTimestamp = DateUtils.getTimestamp000();
        for (DailyPaybackReport report : reports) {
            //计算LTV和累计充值
            LtvDailyReport view = new LtvDailyReport();
            long reportTimestamp = report.getDate().getTime() / 1000;
            long timeDiff = todayTimestamp - reportTimestamp;
            view.setDate(DateUtils.format(report.getDate()));
            view.setRegistrations(report.getRegistrations());
            view.setLtv1Day(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount1Day()), report.getRegistrations()));
            view.setPayAmount1Day(CurrencyUtils.fenToYuan(report.getPayAmount1Day()));
            if (timeDiff > ONE_DAY) {
                view.setLtv2Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount2Days()), report.getRegistrations()));
                view.setPayAmount2Days(CurrencyUtils.fenToYuan(report.getPayAmount2Days()));
            } else {
                view.setLtv2Days(null);
                view.setPayAmount2Days(null);
            }
            if (timeDiff > ONE_DAY * 2) {
                view.setLtv3Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount3Days()), report.getRegistrations()));
                view.setPayAmount3Days(CurrencyUtils.fenToYuan(report.getPayAmount3Days()));
            } else {
                view.setLtv3Days(null);
                view.setPayAmount3Days(null);
            }
            if (timeDiff > ONE_DAY * 3) {
                view.setLtv4Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount4Days()), report.getRegistrations()));
                view.setPayAmount4Days(CurrencyUtils.fenToYuan(report.getPayAmount4Days()));
            } else {
                view.setLtv4Days(null);
                view.setPayAmount4Days(null);
            }
            if (timeDiff > ONE_DAY * 4) {
                view.setLtv5Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount5Days()), report.getRegistrations()));
                view.setPayAmount5Days(CurrencyUtils.fenToYuan(report.getPayAmount5Days()));
            } else {
                view.setLtv5Days(null);
                view.setPayAmount5Days(null);
            }
            if (timeDiff > ONE_DAY * 5) {
                view.setLtv6Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount6Days()), report.getRegistrations()));
                view.setPayAmount6Days(CurrencyUtils.fenToYuan(report.getPayAmount6Days()));
            } else {
                view.setLtv6Days(null);
                view.setPayAmount6Days(null);
            }
            if (timeDiff > ONE_DAY * 6) {
                view.setLtv7Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount7Days()), report.getRegistrations()));
                view.setPayAmount7Days(CurrencyUtils.fenToYuan(report.getPayAmount7Days()));
            } else {
                view.setLtv7Days(null);
                view.setPayAmount7Days(null);
            }
            if (timeDiff > ONE_DAY * 14) {
                view.setLtv15Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount15Days()), report.getRegistrations()));
                view.setPayAmount15Days(CurrencyUtils.fenToYuan(report.getPayAmount15Days()));
            } else {
                view.setLtv15Days(null);
                view.setPayAmount15Days(null);
            }
            if (timeDiff > ONE_DAY * 29) {
                view.setLtv30Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount30Days()), report.getRegistrations()));
                view.setPayAmount30Days(CurrencyUtils.fenToYuan(report.getPayAmount30Days()));
            } else {
                view.setLtv30Days(null);
                view.setPayAmount30Days(null);
            }
            if (timeDiff > ONE_DAY * 59) {
                view.setLtv60Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount60Days()), report.getRegistrations()));
                view.setPayAmount60Days(CurrencyUtils.fenToYuan(report.getPayAmount60Days()));
            } else {
                view.setLtv60Days(null);
                view.setPayAmount60Days(null);
            }
            if (timeDiff > ONE_DAY * 89) {
                view.setLtv90Days(NumberUtils.cpa(CurrencyUtils.fenToYuan(report.getPayAmount90Days()), report.getRegistrations()));
                view.setPayAmount90Days(CurrencyUtils.fenToYuan(report.getPayAmount90Days()));
            } else {
                view.setLtv90Days(null);
                view.setPayAmount90Days(null);
            }
            views.add(view);
        }
        return views;
    }
}
