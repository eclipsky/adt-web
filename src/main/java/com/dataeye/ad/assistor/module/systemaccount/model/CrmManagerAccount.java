package com.dataeye.ad.assistor.module.systemaccount.model;

import java.util.Date;

/**
 * CRM管理人员账号
 * @author luzhuyou
 *
 */
public class CrmManagerAccount {
	/** 账号ID */
	private int accountId;
	/** 邮箱(登录账号) */
	private String email;
	/** 姓名 */
	private String userName;
	/** 密码 */
	private String password;
	/** 状态：0-正常，1-失效 */
	private int status;
	/** 账户角色:0-销售/商务，1-运营，2-开发 */
	private int accountRole;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getAccountRole() {
		return accountRole;
	}
	public void setAccountRole(int accountRole) {
		this.accountRole = accountRole;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "CrmManagerAccount [accountId=" + accountId + ", email=" + email
				+ ", userName=" + userName + ", password=" + password
				+ ", status=" + status + ", accountRole=" + accountRole
				+ ", remark=" + remark + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + "]";
	}
	
}
