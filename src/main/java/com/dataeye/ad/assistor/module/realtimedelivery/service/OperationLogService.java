package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLog;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLogQuery;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLogView;
import com.github.pagehelper.Page;

/**
 * Created by huangzehai on 2017/6/28.
 */
public interface OperationLogService {
    /**
     * Add operation log.
     *
     * @param log
     */
    void addOperationLog(OperationLog log);

    /**
     * List operation logs by account id.
     *
     * @param operationLogQuery
     * @return
     */
    Page<OperationLogView> listOperationLogs(OperationLogQuery operationLogQuery);
}
