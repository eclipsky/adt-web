package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlowQuery;
import com.dataeye.ad.assistor.module.naturalflow.service.NaturalFlowService;
import com.dataeye.ad.assistor.module.naturalflow.util.NaturalFlowUtils;
import com.dataeye.ad.assistor.module.report.mapper.DeliveryKeyIndicatorMapper;
import com.dataeye.ad.assistor.module.report.model.DateRangeQuery;
import com.dataeye.ad.assistor.module.report.model.DeliveryKeyIndicator;
import com.dataeye.ad.assistor.module.report.model.DeliveryKeyIndicatorQuery;
import com.dataeye.ad.assistor.module.result.model.IndicatorWithId;
import com.dataeye.ad.assistor.module.result.util.IndicatorUtils;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.NumberUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by huangzehai on 2017/5/23.
 */
@Service
public class DeliveryKeyIndicatorServiceImpl implements DeliveryKeyIndicatorService {

    @Autowired
    private DeliveryKeyIndicatorMapper deliveryKeyIndicatorMapper;

    @Autowired
    private NaturalFlowService naturalFlowService;

    /**
     * 关键指标汇总
     *
     * @param query
     * @return
     */
    @Override
    public DeliveryKeyIndicator getDeliveryKeyIndicatorTotal(DeliveryKeyIndicatorQuery query) {
        BigDecimal cost = deliveryKeyIndicatorMapper.totalCost(query);
        Integer downloads = deliveryKeyIndicatorMapper.totalDownloads(query);
        Integer activations = deliveryKeyIndicatorMapper.totalActivations(query);
        Integer registrations = deliveryKeyIndicatorMapper.totalRegistrations(query);
        Integer dau = deliveryKeyIndicatorMapper.dau(query);
        //自然流量
        if (!query.isFilterNaturalFlow()) {
            NaturalFlow naturalFlowTotal = naturalFlowService.getNaturalFlowTotal(toNaturalFlowQuery(query));
            if (naturalFlowTotal != null) {
                //激活
                activations = NumberUtils.add(activations, naturalFlowTotal.getActivations());
                //注册
                registrations = NumberUtils.add(registrations, naturalFlowTotal.getRegistrations());
            }

            //获取自然流量DAU
            NaturalFlow naturalFlowForDau = naturalFlowService.getNaturalFlowTotal(toNaturalFlowQuery(query));
            if (naturalFlowForDau != null) {
                dau = NumberUtils.add(dau, naturalFlowForDau.getDau());
            }
        }

        //构建关键指标汇总对象
        DeliveryKeyIndicator deliveryKeyIndicator = new DeliveryKeyIndicator();
        deliveryKeyIndicator.setCost(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(cost)));
        deliveryKeyIndicator.setDownloads(StringUtil.defaultIfNull(downloads));
        deliveryKeyIndicator.setActivations(StringUtil.defaultIfNull(activations));
        deliveryKeyIndicator.setRegistrations(StringUtil.defaultIfNull(registrations));
        deliveryKeyIndicator.setDau(StringUtil.defaultIfNull(dau));
        return deliveryKeyIndicator;
    }

    @Override
    public List<IndicatorWithId> getCostGroupByProduct(DeliveryKeyIndicatorQuery query) {
        List<IndicatorWithId> indicators = deliveryKeyIndicatorMapper.getCostGroupByProduct(query);
        IndicatorUtils.formatIndicators(indicators);
        IndicatorUtils.renameOthers(indicators);
        return indicators;
    }

    @Override
    public List<IndicatorWithId> getCostGroupByMedium(DeliveryKeyIndicatorQuery query) {
        List<IndicatorWithId> indicators = deliveryKeyIndicatorMapper.getCostGroupByMedium(query);
        IndicatorUtils.formatIndicators(indicators);
        return indicators;
    }

    @Override
    public List<IndicatorWithId> getDownloadsGroupByProduct(DeliveryKeyIndicatorQuery query) {
        List<IndicatorWithId> indicators = deliveryKeyIndicatorMapper.getDownloadsGroupByProduct(query);
        IndicatorUtils.renameOthers(indicators);
        return indicators;
    }

    @Override
    public List<IndicatorWithId> getDownloadsGroupByMedium(DeliveryKeyIndicatorQuery query) {
        return deliveryKeyIndicatorMapper.getDownloadsGroupByMedium(query);
    }

    /**
     * 按产品分组统计激活数
     *
     * @param query
     * @return
     */
    @Override
    public List<IndicatorWithId> getActivationsGroupByProduct(DeliveryKeyIndicatorQuery query) {
        List<IndicatorWithId> indicators = deliveryKeyIndicatorMapper.getActivationsGroupByProduct(query);
        IndicatorUtils.renameOthers(indicators);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProduct(toNaturalFlowQuery(query));
            if (naturalFlows != null) {
                for (NaturalFlow naturalFlow : naturalFlows) {
                    IndicatorWithId naturalFlowIndicator = new IndicatorWithId();
                    naturalFlowIndicator.setName(NaturalFlowUtils.formatProductName(naturalFlow.getProductName()));
                    naturalFlowIndicator.setValue(new BigDecimal(naturalFlow.getActivations()));
                    indicators.add(naturalFlowIndicator);
                }
            }
        }
        return indicators;
    }

    /**
     * 按媒体分组统计激活数
     *
     * @param query
     * @return
     */
    @Override
    public List<IndicatorWithId> getActivationsGroupByMedium(DeliveryKeyIndicatorQuery query) {
        List<IndicatorWithId> indicators = deliveryKeyIndicatorMapper.getActivationsGroupByMedium(query);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            NaturalFlow naturalFlow = naturalFlowService.getNaturalFlowTotal(toNaturalFlowQuery(query));
            if (naturalFlow != null) {
                IndicatorWithId naturalFlowIndicator = new IndicatorWithId();
                naturalFlowIndicator.setName(Labels.NATURAL_FLOW_OF_ALL_MEDIUMS);
                naturalFlowIndicator.setValue(new BigDecimal(naturalFlow.getActivations()));
                indicators.add(naturalFlowIndicator);
            }
        }
        return indicators;
    }

    /**
     * 产品分组统计注册数
     *
     * @param query
     * @return
     */
    @Override
    public List<IndicatorWithId> getRegistrationsGroupByProduct(DeliveryKeyIndicatorQuery query) {
        List<IndicatorWithId> indicators = deliveryKeyIndicatorMapper.getRegistrationsGroupByProduct(query);
        IndicatorUtils.renameOthers(indicators);
        if (!query.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProduct(toNaturalFlowQuery(query));
            if (naturalFlows != null) {
                for (NaturalFlow naturalFlow : naturalFlows) {
                    IndicatorWithId naturalFlowIndicator = new IndicatorWithId();
                    naturalFlowIndicator.setName(NaturalFlowUtils.formatProductName(naturalFlow.getProductName()));
                    naturalFlowIndicator.setValue(new BigDecimal(naturalFlow.getRegistrations()));
                    indicators.add(naturalFlowIndicator);
                }
            }
        }
        return indicators;
    }

    @Override
    public List<IndicatorWithId> getRegistrationsGroupByMedium(DeliveryKeyIndicatorQuery query) {
        List<IndicatorWithId> indicators = deliveryKeyIndicatorMapper.getRegistrationsGroupByMedium(query);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            NaturalFlow naturalFlow = naturalFlowService.getNaturalFlowTotal(toNaturalFlowQuery(query));
            if (naturalFlow != null) {
                IndicatorWithId naturalFlowIndicator = new IndicatorWithId();
                naturalFlowIndicator.setName(Labels.NATURAL_FLOW_OF_ALL_MEDIUMS);
                naturalFlowIndicator.setValue(new BigDecimal(naturalFlow.getRegistrations()));
                indicators.add(naturalFlowIndicator);
            }
        }
        return indicators;
    }

    @Override
    public List<IndicatorWithId> getDauGroupByProduct(DeliveryKeyIndicatorQuery query) {
        List<IndicatorWithId> indicators = deliveryKeyIndicatorMapper.getDauGroupByProduct(query);
        IndicatorUtils.renameOthers(indicators);
        if (!query.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProduct(toNaturalFlowQuery(query));
            if (naturalFlows != null) {
                for (NaturalFlow naturalFlow : naturalFlows) {
                    IndicatorWithId naturalFlowIndicator = new IndicatorWithId();
                    naturalFlowIndicator.setName(NaturalFlowUtils.formatProductName(naturalFlow.getProductName()));
                    naturalFlowIndicator.setValue(new BigDecimal(naturalFlow.getDau()));
                    indicators.add(naturalFlowIndicator);
                }
            }
        }
        return indicators;
    }

    /**
     * 生成DAU专用的查询，仅查询所选范围最后一天的DAU.
     *
     * @param query
     * @return
     */
    private DateRangeQuery toDateRangeQuery(DeliveryKeyIndicatorQuery query) {
        DateRangeQuery dateRangeQuery = new DateRangeQuery();
        dateRangeQuery.setStartDate(query.getEndDate());
        dateRangeQuery.setEndDate(query.getEndDate());
        dateRangeQuery.setCompanyId(query.getCompanyId());
        return dateRangeQuery;
    }

    private NaturalFlowQuery toNaturalFlowQuery(DeliveryKeyIndicatorQuery query) {
        NaturalFlowQuery dateRangeQuery = new NaturalFlowQuery();
        dateRangeQuery.setStartDate(query.getEndDate());
        dateRangeQuery.setEndDate(query.getEndDate());
        dateRangeQuery.setCompanyId(query.getCompanyId());
        dateRangeQuery.setPermissionMediumAccountIds(query.getPermissionMediumAccountIds());
        dateRangeQuery.setPermissionSystemAccounts(query.getPermissionSystemAccounts());
        return dateRangeQuery;
    }

    @Override
    public List<IndicatorWithId> getDauGroupByMedium(DeliveryKeyIndicatorQuery query) {
        List<IndicatorWithId> indicators = deliveryKeyIndicatorMapper.getDauGroupByMedium(query);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            NaturalFlow naturalFlow = naturalFlowService.getNaturalFlowTotal(toNaturalFlowQuery(query));
            if (naturalFlow != null) {
                IndicatorWithId naturalFlowIndicator = new IndicatorWithId();
                naturalFlowIndicator.setName(Labels.NATURAL_FLOW_OF_ALL_MEDIUMS);
                naturalFlowIndicator.setValue(new BigDecimal(naturalFlow.getDau()));
                indicators.add(naturalFlowIndicator);
            }
        }
        return indicators;
    }
}
