package com.dataeye.ad.assistor.module.product.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.product.constants.Constants.TrackingInterface;
import com.dataeye.ad.assistor.module.product.model.DebugDevice;
import com.dataeye.ad.assistor.module.product.model.DebugLogData;
import com.dataeye.ad.assistor.module.product.model.Link;
import com.dataeye.ad.assistor.module.systemaccount.model.PtLoginResponse;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


/**
 * 测试设备Service
 * @author ldj 2017/11/01
 *
 */
@Service("debugSdkDataService")
public class DebugSdkDataService {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(DebugSdkDataService.class);

    /**
     *清除测试数据（清除调试日志+统计数据）
     * */
    public String delete(Integer uid,String appid, Integer id) {
		
    	
    	return "";
	}
    
    
	 /**
     * 查询实时日志
     * @param uid	创建人id
	 * @param appid	产品appid
	 * @param status
	 * @param type
     * @author ldj  2017-11-01
     * */
    public List<DebugLogData> queryDebugDevice(Integer uid,String appid,String status,String type) {
    	//1.构建参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("uid", uid+"");
		params.put("appid", appid);
		params.put("status", status);
		params.put("type", type);
		//2.HTTP请求获取数据
		String queryDebugDeviceInterface = ConfigHandler.getProperty(TrackingInterface.QUERY_DEBUG_LOG_KEY, TrackingInterface.QUERY_DEBUG_LOG_URL);
		String httpResponse = HttpRequest.post(queryDebugDeviceInterface, params);
		logger.debug("[httpResponse]:"+httpResponse);
		if(StringUtils.isBlank(httpResponse)) {
    		logger.info("实时日志查询的接口请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
		// 3.结果数据封装，转换为Map
		JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("实时日志查询的接口响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	PtLoginResponse<List<DebugLogData>> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<PtLoginResponse<List<DebugLogData>>>(){}.getType());
    	if(response == null || response.getContent() == null) {
        	logger.info("实时日志查询的接口请求返回数据为空！");
        	return null;
        }
    	return response.getContent();
	}
    
    /**
	 * 查询测试统计日志
	 * @param uid	创建人id
	 * @param appid	产品appid
	 * @return
	 * @author ldj  2017-11-01
	 * */
	public List<Link> queryDebugSummary(Integer uid,String appid) {
		//1.构建参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("uid", uid+"");
		params.put("appid", appid);
		//2.HTTP请求获取数据
		String debugDeviceInterface = ConfigHandler.getProperty(TrackingInterface.QUERY_DEBUG_SUMMARY_KEY, TrackingInterface.QUERY_DEBUG_SUMMARY_URL);
		String httpResponse = HttpRequest.post(debugDeviceInterface, params);
		logger.debug("[httpResponse]:"+httpResponse);
		if(StringUtils.isBlank(httpResponse)) {
    		logger.info("查询测试统计日志的接口请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
		// 3.结果数据封装，转换为Map
		JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("查询测试统计日志的接口响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	PtLoginResponse<List<Link>> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<PtLoginResponse<List<Link>>>(){}.getType());
    	if(response == null || response.getContent() == null) {
        	logger.info("实时日志查询的接口请求返回数据为空！");
        	return null;
        }
    	return response.getContent();
	}
	
	/**
	 * 清除测试数据
	 * @param uid	创建人id
	 * @param appid	产品appid
	 * @return
	 * @author ldj  2017-11-01
	 * */
	public String deleteDebugData(Integer uid,String appid) {
		//1.构建参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("uid", uid+"");
		params.put("appid", appid);
		//2.HTTP请求获取数据
		String addDebugDeviceInterface = ConfigHandler.getProperty(TrackingInterface.DELETE_DEBUG_DATA_KEY, TrackingInterface.DELETE_DEBUG_DATA_URL);
		String httpResponse = HttpRequest.post(addDebugDeviceInterface, params);
		logger.debug("[httpResponse]:"+httpResponse);
		if(StringUtils.isBlank(httpResponse)) {
    		logger.info("清除测试数据的接口请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
		// 3.结果数据封装，转换为Map
		JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("清除测试数据的接口响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	return "SUCCESS";
	}
}
