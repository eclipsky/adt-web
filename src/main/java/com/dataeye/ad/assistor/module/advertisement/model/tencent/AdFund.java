package com.dataeye.ad.assistor.module.advertisement.model.tencent;


/**
 * 资金账户
 * */
public class AdFund {

	/**资金账户类型*/
	private String fund_type;
	/**余额，单位为分*/
	private Integer balance;
	/**资金状态*/
	private String fund_status;
	/**当日花费，单位为分*/
	private Integer realtime_cost;
	public String getFund_type() {
		return fund_type;
	}
	public void setFund_type(String fund_type) {
		this.fund_type = fund_type;
	}
	public Integer getBalance() {
		return balance;
	}
	public void setBalance(Integer balance) {
		this.balance = balance;
	}
	public String getFund_status() {
		return fund_status;
	}
	public void setFund_status(String fund_status) {
		this.fund_status = fund_status;
	}
	public Integer getRealtime_cost() {
		return realtime_cost;
	}
	public void setRealtime_cost(Integer realtime_cost) {
		this.realtime_cost = realtime_cost;
	}
	@Override
	public String toString() {
		return "AdFund [fund_type=" + fund_type + ", balance=" + balance
				+ ", fund_status=" + fund_status + ", realtime_cost="
				+ realtime_cost + "]";
	}
	
}
