package com.dataeye.ad.assistor.module.report.model;

import org.apache.commons.lang.StringUtils;

/**
 * 维度（推广计划、活动组、媒体）,值分别为plan,eventGroup, medium
 * Created by huangzehai on 2017/1/4.
 */
public enum Dimension {
    Plan, Medium, systemAccount;

    public static Dimension parse(String text) {
        for (Dimension dimension : Dimension.values()) {
            if (StringUtils.equalsIgnoreCase(dimension.name(), text)) {
                return dimension;
            }
        }
        return null;
    }
}
