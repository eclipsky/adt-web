package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.model.DeliveryStatus;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.TuiUtils;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2017/5/15.
 */
@Service("tuiPlanSwitchService")
public class TuiPlanSwitchService extends AbstractMediumPlanService<DeliveryStatus> implements PlanSwitchService {
    private static final String URL = "http://tui.qq.com/misapi/adgroup/operation";

    private static final String REFERER_FORMAT = "http://tui.qq.com/xui/%s/adgroup/list?begin_date=%s&data_begin_date=%s&data_end_date=%s&end_date=9999-12-31&page_num=1&page_size=10&price_mode=_all&promotion=_all&sort_key=create_time&sort_type=ASC&source_link=menu&status=ADGROUP_STATUS_UN_DELETED";

    @Override
    public Result updateStatus(DeliveryStatus status) {
        return this.execute(status);
    }

    @Override
    protected String url(String cookie, Long mediumPlanId) {
        return URL;
    }

    /**
     * 设置请求参数.
     *
     * @param cookie 媒体账号Cookie.
     * @param status 出价参数
     * @return
     */
    @Override
    protected List<NameValuePair> parameters(String cookie, DeliveryStatus status) {
        List<NameValuePair> params = new ArrayList<>();
        //计划ID
        params.add(new BasicNameValuePair("adgroup_id", status.getMediumPlanId().toString()));
        //广告主ID
        params.add(new BasicNameValuePair("advertiser_id", TuiUtils.getLoginId(cookie)));
        //操作状态
        params.add(new BasicNameValuePair("operation_status", StringUtils.upperCase(status.getStatus().name())));
        return params;
    }

    /**
     * 设置请求头部.
     *
     * @param request
     * @param cookie
     */
    @Override
    protected void requestHeader(HttpRequestBase request, String cookie) {
        request.setHeader(X_CSRF_TOKEN, TuiUtils.getCsrfToken(cookie));
        request.setHeader(HOST, "tui.qq.com");
        request.setHeader(ORIGIN, "http://tui.qq.com");
        String today = DateUtils.format(new Date());
        request.setHeader(REFERER, String.format(REFERER_FORMAT, TuiUtils.getLoginId(cookie), today, today, today));
    }

    /**
     * 解析响应
     *
     * @param content
     * @return
     */
    @Override
    protected Result response(String content) {
        Gson gson = new Gson();
        Response response = gson.fromJson(content, Response.class);
        Result result = new Result();
        //Code为0是操作成功.
        result.setSuccess(response.getCode() == 0);
        result.setMessage(response.getMsg());
        return result;
    }

    /**
     * 响应对象.
     */
    private class Response {
        private int code;
        private String msg;
//        private Data data;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

//        public Data getData() {
//            return data;
//        }
//
//        public void setData(Data data) {
//            this.data = data;
//        }

        @Override
        public String toString() {
            return "Response{" +
                    "code=" + code +
                    ", msg='" + msg + '\'' +
//                    ", data=" + data +
                    '}';
        }

        private class Data {
            private String status;

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            @Override
            public String toString() {
                return "Data{" +
                        "status='" + status + '\'' +
                        '}';
            }
        }
    }

}
