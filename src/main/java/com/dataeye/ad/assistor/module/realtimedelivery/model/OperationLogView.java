package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.google.gson.annotations.Expose;

/**
 * Created by huangzehai on 2017/6/29.
 */
public class OperationLogView {
    /**
     * 操作Id.
     */
    @Expose
    private long opId;

    @Expose
    private int planId;
    /**
     * 计划名称
     */
    @Expose
    private String planName;

    /**
     * 操作时间
     */
    @Expose
    private String opTime;

    /**
     * ADT账号名称.
     */
    @Expose
    private String accountName;

    /**
     * 操作名称
     */
    @Expose
    private String opName;
    /**
     * 改动后的值
     */
    @Expose
    private String currentValue;
    /**
     * 改动前的值
     */
    @Expose
    private String previousValue;
    /**
     * 结果:1:成功;0:失败.
     */
    @Expose
    private String result;

    @Expose
    private String message;

    public long getOpId() {
        return opId;
    }

    public void setOpId(long opId) {
        this.opId = opId;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getOpTime() {
        return opTime;
    }

    public void setOpTime(String opTime) {
        this.opTime = opTime;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getOpName() {
        return opName;
    }

    public void setOpName(String opName) {
        this.opName = opName;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public String getPreviousValue() {
        return previousValue;
    }

    public void setPreviousValue(String previousValue) {
        this.previousValue = previousValue;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "OperationLogView{" +
                "opId=" + opId +
                ", planId=" + planId +
                ", planName='" + planName + '\'' +
                ", opTime='" + opTime + '\'' +
                ", accountName='" + accountName + '\'' +
                ", opName='" + opName + '\'' +
                ", currentValue='" + currentValue + '\'' +
                ", previousValue='" + previousValue + '\'' +
                ", result='" + result + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
