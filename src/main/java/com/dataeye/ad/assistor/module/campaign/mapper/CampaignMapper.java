package com.dataeye.ad.assistor.module.campaign.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.campaign.model.Campaign;
import com.dataeye.ad.assistor.module.campaign.model.CampaignCreateReqVo;
import com.dataeye.ad.assistor.module.campaign.model.CampaignQueryReqVo;
import com.dataeye.ad.assistor.module.campaign.model.CampaignSelectorVo;
import com.dataeye.ad.assistor.module.campaign.model.CampaignVo;

/**
 * 推广计划映射器.
 * Created by luzhuyou 2017/06/19
 */
@MapperScan
public interface CampaignMapper {

	/**
	 * 查询推广计划列表
	 * @param vo
	 * @return
	 */
	public List<CampaignVo> query(CampaignQueryReqVo vo);

	/**
	 * 下拉框查询推广计划列表
	 * @param vo
	 * @return
	 */
	public List<CampaignSelectorVo> queryForSelector(CampaignQueryReqVo vo);

	/**
	 * 更新推广计划
	 * @param campaign
	 * @return
	 */
	public int update(Campaign campaign);

	/**
	 * 删除推广计划
	 * @param campaignId
	 * @return
	 */
	public int delete(String campaignId);

	/**
	 * 创建推广计划
	 * @param campaignList
	 */
	public void batchInsert(List<CampaignCreateReqVo> campaignList);
	
	/**
	 * 查询appid集合
	 * @param vo
	 * @return
	 */
	public String queryAppIds(CampaignQueryReqVo vo);
	
	/**
	 * 根据短链号，查询Tracking广告活动信息
	 * @param campaignId
	 * */
	public CampaignVo getCampaignById(String campaignId);
}
