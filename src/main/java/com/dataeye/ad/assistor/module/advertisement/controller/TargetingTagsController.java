package com.dataeye.ad.assistor.module.advertisement.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.CharsetUtils;
import com.dataeye.ad.assistor.module.advertisement.model.TargetingTagVO;
import com.dataeye.ad.assistor.module.advertisement.model.TargetingVO;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.TargetingBean;
import com.dataeye.ad.assistor.util.FileUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonElement;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.module.advertisement.service.TargetingTagsService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;


/**
 * Created by ldj
 * 广告投放
 * 定向控制器.
 */
@Controller
public class TargetingTagsController {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(TargetingTagsController.class);

    @Autowired
    private TargetingTagsService targetingTagsService;

    /**
     * 下拉框查询定向列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017/12/06
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amTargetings/queryForSelect.do")
    public Object queryForSelect(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("下拉框查询投放业务定向列表");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        DEParameter parameter = context.getDeParameter();
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);

        }
        return targetingTagsService.queryTargetingSelect(Integer.parseInt(mediumAccountId));
    }

    /**
     * 上传关键词文件文件
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping(value = "/ad/amTargetings/addkwFile.do")
    public Object addkwFile(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        logger.info("添加关键词文件");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);

        }
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile mf = multipartRequest.getFile("kwFile");
        if (mf == null) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_KWFILE_NOT_UPLOAD);
        }
        String fileName = mf.getOriginalFilename();
        // 接受文件类型为txt
        if (!fileName.contains(".txt")) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_NOT_TXT_FILE);
        }
        HashSet<String> strings = new HashSet<>();
        // 解析文件编码
        Charset charset = CharsetUtils.detectCharset(mf.getBytes(), CharsetUtils.CHARSETS);
        if (charset != null) {
            InputStreamReader reader = new InputStreamReader(mf.getInputStream(), charset);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String s = "";
            int rowsCount = 0;
            try {
                while ((s = bufferedReader.readLine()) != null) {
                    // 关键词字节数最大为30
                    if (s.length() > 10) {
                        rowsCount++;
                    } else {
                        strings.add(s);
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                reader.close();
                bufferedReader.close();
            }
            if (rowsCount > 0) {
                ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_TARGETING_KWFILE_WORD_TOO_LONG);
            }
        } else {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_TARGETING_KWFILE_ENCODE_UNRECOGNIZED);
        }
        // 数组最大长度为500
        if (strings.size() > 500) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_TARGETING_KWFILE_TOO_LONG);
        }
        String[] array = new String[strings.size()];
        return strings.toArray(array);
    }

    /**
     * 查询可用定向标签(地域、商业行为、app类目)
     *
     * @param request
     * @param response
     * @return String 可用标签JSON
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amTargetings/queryTargetingTags.do")
    public Object queryTargetTags(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        logger.info("查询投放业务可用定向标签");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        // 读取本地JSON文件
        InputStream inputStream = TargetingTagsService.class.getClassLoader().getResourceAsStream("resources/json/targetTags.json");
        JsonElement tagJson = StringUtil.jsonParser.parse(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
        TargetingTagVO tagVO = StringUtil.gson.fromJson(tagJson, TargetingTagVO.class);
        try {
            inputStream.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_JSONFILE_READER_ERROR);
        }
        return tagVO;
    }

    /**
     * 编辑广告时，查询该广告的定向
     *
     * @param request
     * @param response
     * @return TargetVO
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amTargetings/queryTargeting.do")
    public Object queryTargeting(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        logger.info("查询投放业务定向条件列表");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        String targetingId = parameter.getParameter("targetingId");
        if (StringUtils.isBlank(targetingId)) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_ID_IS_NULL);
        }

        return targetingTagsService.queryTargeting(Integer.parseInt(mediumAccountId), Integer.parseInt(targetingId));
    }


    /**
     * 新增定向
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amTargetings/addTargeting.do")
    public Object addTargeting(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        logger.info("新增投放业务定向包");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        // 1. 参数解析与校验
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        String targetingName = parameter.getParameter("targetingName");
        if (StringUtils.isBlank(targetingName)) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_NAME_IS_NULL);
        }
        TargetingBean targetingBean = null;
        try {
            targetingBean = StringUtil.gson.fromJson(parameter.getParameter("targeting"), TargetingBean.class);
        } catch (Exception e) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_TARGETING_POST_ERROR);
        }
        String description = parameter.getParameter("description");
        if (StringUtils.isBlank(description)) {
            description = null;
        }
        String isPublic = parameter.getParameter("isPublic");
        if (StringUtils.isBlank(isPublic)) {
            isPublic = "0";
        }
        TargetingVO targetingVO = new TargetingVO(targetingName, targetingBean, description);
        return targetingTagsService.addTargeting(Integer.parseInt(mediumAccountId), targetingVO, Integer.parseInt(isPublic));
    }

    /**
     * 编辑定向
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amTargetings/modifyTargeting.do")
    public Object modifyTargeting(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        logger.info("修改投放业务定向包");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        // 1. 参数解析与校验
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        String targetingId = parameter.getParameter("targetingId");
        if (StringUtils.isBlank(targetingId)) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_ID_IS_NULL);
        }
        String targetingName = parameter.getParameter("targetingName");
        if (StringUtils.isBlank(targetingName)) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_NAME_IS_NULL);
        }
        String isPublic = parameter.getParameter("isPublic");
        if (StringUtils.isBlank(isPublic)) {
            isPublic = "0";
        }
        TargetingBean targetingBean = null;
        try {
            targetingBean = StringUtil.gson.fromJson(parameter.getParameter("targeting"), TargetingBean.class);
        } catch (Exception e) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_TARGETING_POST_ERROR);
        }
        String description = parameter.getParameter("description");
        if (StringUtils.isBlank(description)) {
            description = null;
        }
        TargetingVO targetingVO = new TargetingVO(Integer.parseInt(targetingId), targetingName, targetingBean, description);
        targetingVO.setIsPublic(Integer.parseInt(isPublic));
        return targetingTagsService.modifyTargeting(Integer.parseInt(mediumAccountId), targetingVO);
    }

    /**
     * 删除定向
     * **删除定向后，引用这个定向的所有广告将会没有曝光**
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amTargetings/deleteTargeting.do")
    public Object deleteTargeting(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        logger.info("删除投放业务定向包");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        // 1. 参数解析与校验
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        String targetingId = parameter.getParameter("targetingId");
        if (StringUtils.isBlank(targetingId)) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_ID_IS_NULL);
        }
        return targetingTagsService.deleteTargeting(Integer.parseInt(mediumAccountId), Integer.parseInt(targetingId));
    }

    /**
     * 定向包名称校验
     *
     * @param request
     * @param response
     * @return Boolean True - 校验通过；False - 校验失败
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amTargetings/checkName.do")
    public Object checkTargetingName(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("定向名称校验");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        String targetingName = parameter.getParameter("targetingName");
        String targetingIdStr = parameter.getParameter("targetingId");
        Integer targetingId = null;
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_MEDIUM_ACCOUNT_IS_NULL);
        }
        if (StringUtils.isBlank(targetingName)) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_NAME_IS_NULL);
        }
        if (StringUtils.isNotBlank(targetingIdStr)) {
            targetingId = Integer.parseInt(targetingIdStr);
        }
        return targetingTagsService.checkTargetingName(Integer.parseInt(mediumAccountId), targetingName, targetingId);
    }

}
