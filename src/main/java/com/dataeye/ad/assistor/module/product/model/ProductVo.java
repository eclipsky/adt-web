package com.dataeye.ad.assistor.module.product.model;

import java.util.List;

import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountGroupVo;
import com.google.gson.annotations.Expose;


/**
 * 产品信息VO
 * 	增加关联TrackingApp信息（2017/05/05 luzhuyou）
 * @author luzhuyou 2017/02/20
 *
 */
public class ProductVo {

	/** 产品ID */
	@Expose
	private Integer productId;
	/** 公司ID */
	private Integer companyId;
	/** 产品名称 */
	@Expose
	private String productName;
	/** 状态: 0-正常（默认），1-失效*/
	@Expose
	private Integer status;
	/** CP分成率 */
	@Expose
	private Double shareRate = 1.00;
	/** TrackingAppID */
	@Expose
	private String appId;
	/** TrackingApp名称 @Expose*/
	
	private String appName;
	
	/**平台*/
	@Expose
	private Integer osType;
	/**一级产品分类id**/
	@Expose
	private Integer parentCategoryId;
	/**二级产品分类id**/
	@Expose
	private Integer childrenCategoryId;
	
	/**一级产品分类名称**/
	@Expose
	private String parentCategoryName;
	/**二级产品分类名称**/
	@Expose
	private String childrenCategoryName;
	/**投放媒体帐号*/
	@Expose
	private String mediumAccountNames;
	/**投放媒体帐号id，多个用逗号拼接*/
	@Expose
	private String mediumAccountIds;
	
	/** 投放媒体帐号列表(根据媒体名称分组) */
	@Expose
	private List<MediumAccountGroupVo> mediumAccountList;
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Double getShareRate() {
		return shareRate;
	}
	public void setShareRate(Double shareRate) {
		this.shareRate = shareRate;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	public Integer getChildrenCategoryId() {
		return childrenCategoryId;
	}
	public void setChildrenCategoryId(Integer childrenCategoryId) {
		this.childrenCategoryId = childrenCategoryId;
	}
	public String getParentCategoryName() {
		return parentCategoryName;
	}
	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}
	public String getChildrenCategoryName() {
		return childrenCategoryName;
	}
	public void setChildrenCategoryName(String childrenCategoryName) {
		this.childrenCategoryName = childrenCategoryName;
	}
	
	public String getMediumAccountNames() {
		return mediumAccountNames;
	}
	public void setMediumAccountNames(String mediumAccountNames) {
		this.mediumAccountNames = mediumAccountNames;
	}
	
	public String getMediumAccountIds() {
		return mediumAccountIds;
	}
	public void setMediumAccountIds(String mediumAccountIds) {
		this.mediumAccountIds = mediumAccountIds;
	}
	public List<MediumAccountGroupVo> getMediumAccountList() {
		return mediumAccountList;
	}
	public void setMediumAccountList(List<MediumAccountGroupVo> mediumAccountList) {
		this.mediumAccountList = mediumAccountList;
	}
	@Override
	public String toString() {
		return "ProductVo [productId=" + productId + ", companyId=" + companyId
				+ ", productName=" + productName + ", status=" + status
				+ ", shareRate=" + shareRate + ", appId=" + appId
				+ ", appName=" + appName + ", osType=" + osType
				+ ", parentCategoryId=" + parentCategoryId
				+ ", childrenCategoryId=" + childrenCategoryId
				+ ", parentCategoryName=" + parentCategoryName
				+ ", childrenCategoryName=" + childrenCategoryName
				+ ", mediumAccountNames=" + mediumAccountNames
				+ ", mediumAccountIds=" + mediumAccountIds
				+ ", mediumAccountList=" + mediumAccountList + "]";
	}
	
	
}
