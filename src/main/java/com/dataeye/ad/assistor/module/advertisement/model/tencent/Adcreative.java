package com.dataeye.ad.assistor.module.advertisement.model.tencent;

import java.util.List;




/**
 * 广告创意
 * Created by ldj
 */
public class Adcreative {

	
    private Integer campaign_id;
    
    private Integer adcreative_id;

    private String adcreative_name;

    private Integer adcreative_template_id;

    private Object adcreative_elements;
    
    private String destination_url;
    
    private String deep_link;
    
    private List<String> site_set;

    private String product_type;

    private String product_refs_id;

	public Integer getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(Integer campaign_id) {
		this.campaign_id = campaign_id;
	}

	public Integer getAdcreative_id() {
		return adcreative_id;
	}

	public void setAdcreative_id(Integer adcreative_id) {
		this.adcreative_id = adcreative_id;
	}

	public String getAdcreative_name() {
		return adcreative_name;
	}

	public void setAdcreative_name(String adcreative_name) {
		this.adcreative_name = adcreative_name;
	}

	public Integer getAdcreative_template_id() {
		return adcreative_template_id;
	}

	public void setAdcreative_template_id(Integer adcreative_template_id) {
		this.adcreative_template_id = adcreative_template_id;
	}

	public Object getAdcreative_elements() {
		return adcreative_elements;
	}

	public void setAdcreative_elements(Object adcreative_elements) {
		this.adcreative_elements = adcreative_elements;
	}

	public String getDestination_url() {
		return destination_url;
	}

	public void setDestination_url(String destination_url) {
		this.destination_url = destination_url;
	}

	public String getDeep_link() {
		return deep_link;
	}

	public void setDeep_link(String deep_link) {
		this.deep_link = deep_link;
	}

	public List<String> getSite_set() {
		return site_set;
	}

	public void setSite_set(List<String> site_set) {
		this.site_set = site_set;
	}

	public String getProduct_type() {
		return product_type;
	}

	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}

	public String getProduct_refs_id() {
		return product_refs_id;
	}

	public void setProduct_refs_id(String product_refs_id) {
		this.product_refs_id = product_refs_id;
	}

}
