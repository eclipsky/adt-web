package com.dataeye.ad.assistor.module.realtimedelivery.model;

import java.util.List;

/**
 * Created by huangzehai on 2017/3/3.
 */
public class Alerts {
    private List<DeliveryAlertInfoVo> generalAlerts;
    private List<RealTimeReport> advancedAlerts;

    public List<DeliveryAlertInfoVo> getGeneralAlerts() {
        return generalAlerts;
    }

    public void setGeneralAlerts(List<DeliveryAlertInfoVo> generalAlerts) {
        this.generalAlerts = generalAlerts;
    }

    public List<RealTimeReport> getAdvancedAlerts() {
        return advancedAlerts;
    }

    public void setAdvancedAlerts(List<RealTimeReport> advancedAlerts) {
        this.advancedAlerts = advancedAlerts;
    }
}
