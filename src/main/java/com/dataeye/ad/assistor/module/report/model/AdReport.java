package com.dataeye.ad.assistor.module.report.model;

import java.math.BigDecimal;

import com.google.gson.annotations.Expose;

/**
 * 投放日报.
 * Created by huangzehai on 2016/12/26.
 */
public class AdReport {

    private long id;

    /**
     * 日期|日期范围
     */
    private String date;

    /**
     * 日期|日期范围
     */
    private Link dateLink;

    /**
     * 媒体|平台|渠道.
     */
    private String medium;

    /**
     * 媒体|平台|渠道.
     */
    private Link mediumLink;

    /**
     * 媒体|平台|渠道ID.
     */
    private int mediumId;

    /**
     * 账号
     */
    private String account;

    /**
     * 账号别名
     */
    private String accountAlias;

    /**
     * 账号
     */
    private Link accountLink;

    /**
     * 账号ID.
     */
    private int accountId;

    /**
     * 计划|策略
     */
    private String plan;

    /**
     * 计划|策略
     */
    private Link planLink;

    /**
     * 计划|策略ID.
     */
    private Integer planId;

    /**
     * 总消耗|总花费.
     */
    private BigDecimal totalCost;

    /**
     * 曝光数.
     */
    private Integer exposures;
    /**
     * 点击数.
     */
    private Integer clicks;

    /**
     * 未排重点击数（ADT点击数）
     */
    private Integer totalClicks;
    /**
     * 点击率.
     */
    private Double ctr;

    /**
     * CPC.平均每点击花费.
     */
    private BigDecimal cpc;


    /**
     * 到达落地页UV(独立访客)数.
     */
    private Integer landingPageUV;
    /**
     * 到达率.
     */
    private Double reachRate;

    /**
     * 停留时长.
     */
    private Integer retentionTime;

    /**
     * 下载点击数独立访客（UV）
     */
    private Integer downloadUV;

    /**
     * 下载率.
     */
    private Double downloadRate;

    /**
     * 激活数.
     */
    private Integer activations;

    /**
     * 激活率
     */
    private Double activationRate;

    /**
     * 激活CPA.
     */
    private BigDecimal activationCpa;

    /**
     * 注册设备数.
     */
    private Integer registrations;

    /**
     * 注册CPA.
     */
    private BigDecimal costPerRegistration;

    /**
     * 激活注册率
     */
    private Double activationRegistrationRate;

    /**
     * 点击注册率.
     */
    private Double registrationRate;

    /**
     * 次日留存率。
     */
    private Double nextDayRetentionRate;

    /**
     * 三日留存率。
     */
    private Double threeDayRetentionRate;

    /**
     * 七日留存率.
     */
    private Double sevenDayRetentionRate;

    /**
     * 15日留存率.
     */
    private Double fifteenDayRetentionRate;
    
    /**
     * 三十日留存率.
     */
    private Double thirtyDayRetentionRate;


    /**
     * 新增充值人数.
     */
    private Integer newlyIncreasedRecharges;

    /**
     * 新增充值金额.
     */
    private BigDecimal newlyIncreasedRechargeAmount;

    /**
     * 新增用户付费率
     */
    private Double newlyIncreasedPayRate;

    /**
     * 首日回本率.
     */
    private Double firstDayPaybackRate;

    /**
     * 首日充值金额
     */
    private BigDecimal payAmount1Day;

    /**
     * 7日充值金额
     */
    private BigDecimal payAmount7Days;

    /**
     * 30日充值金额
     */
    private BigDecimal payAmount30Days;

    /**
     * 投放包总充值（所有用户（新增用户和老用户）在某一天带来的充值之和)
     */
    private BigDecimal totalRecharges;

    /**
     * 总充值人数.
     */
    private Integer totalNumberOfRecharge;

    /**
     * 累计充值（所有日期的充值之和）
     */
    private BigDecimal accumulatedRecharge;

    /**
     * 付费率.
     */
    private Double payRate;

    /**
     * 日活跃用户DAU.
     */
    private Integer dau;

    /**
     * 是否自然流量
     */
    private boolean isNaturalFlow;
    
    /**
     * 注册帐号数
     */
    private Integer registerAccountNum;  
    
    //add indicator by ldj 2018-03-06
    /**
     * 首日注册设备数
     * */
    private Integer firstDayRegistrations;
    
    /**
     * 首日注册账号数
     * */
    private Integer firstDayRegisterAccountNum;
    
    /**
     * 首日注册CPA(帐号)
     */
    private BigDecimal registrationAccountNumCpa;
    
    /**
     * 子账号名称
     */
    private String systemAccountName;

    /**
     * 账号id
     */
    private Integer systemAccountId;
    
    /**
     * 首日LTV
     */
    private BigDecimal ltv1Day;

    /**
     * 7日LTV
     */
    private BigDecimal ltv7Days;

    /**
     * 30日LTV
     */
    private BigDecimal ltv30Days;
    
    private Integer registerAccountNumD3;
    private Integer registerAccountNumD7;
    private Integer registerAccountNumD15;
    private Integer registerAccountNumD30;
    private Integer retainD1;
    private Integer retainD3;
    private Integer retainD7;
    private Integer retainD14;
    private Integer retainD30;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Link getDateLink() {
        return dateLink;
    }

    public void setDateLink(Link dateLink) {
        this.dateLink = dateLink;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public Link getMediumLink() {
        return mediumLink;
    }

    public void setMediumLink(Link mediumLink) {
        this.mediumLink = mediumLink;
    }

    public int getMediumId() {
        return mediumId;
    }

    public void setMediumId(int mediumId) {
        this.mediumId = mediumId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccountAlias() {
        return accountAlias;
    }

    public void setAccountAlias(String accountAlias) {
        this.accountAlias = accountAlias;
    }

    public Link getAccountLink() {
        return accountLink;
    }

    public void setAccountLink(Link accountLink) {
        this.accountLink = accountLink;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public Link getPlanLink() {
        return planLink;
    }

    public void setPlanLink(Link planLink) {
        this.planLink = planLink;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public Integer getExposures() {
        return exposures;
    }

    public void setExposures(Integer exposures) {
        this.exposures = exposures;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Integer getTotalClicks() {
        return totalClicks;
    }

    public void setTotalClicks(Integer totalClicks) {
        this.totalClicks = totalClicks;
    }

    public Double getCtr() {
        return ctr;
    }

    public void setCtr(Double ctr) {
        this.ctr = ctr;
    }

    public BigDecimal getCpc() {
        return cpc;
    }

    public void setCpc(BigDecimal cpc) {
        this.cpc = cpc;
    }

    public Integer getLandingPageUV() {
        return landingPageUV;
    }

    public void setLandingPageUV(Integer landingPageUV) {
        this.landingPageUV = landingPageUV;
    }

    public Double getReachRate() {
        return reachRate;
    }

    public void setReachRate(Double reachRate) {
        this.reachRate = reachRate;
    }

    public Integer getRetentionTime() {
        return retentionTime;
    }

    public void setRetentionTime(Integer retentionTime) {
        this.retentionTime = retentionTime;
    }

    public Integer getDownloadUV() {
        return downloadUV;
    }

    public void setDownloadUV(Integer downloadUV) {
        this.downloadUV = downloadUV;
    }

    public Double getDownloadRate() {
        return downloadRate;
    }

    public void setDownloadRate(Double downloadRate) {
        this.downloadRate = downloadRate;
    }

    public Integer getActivations() {
        return activations;
    }

    public void setActivations(Integer activations) {
        this.activations = activations;
    }

    public Double getActivationRate() {
        return activationRate;
    }

    public void setActivationRate(Double activationRate) {
        this.activationRate = activationRate;
    }

    public BigDecimal getActivationCpa() {
        return activationCpa;
    }

    public void setActivationCpa(BigDecimal activationCpa) {
        this.activationCpa = activationCpa;
    }

    public Integer getRegistrations() {
        return registrations;
    }

    public void setRegistrations(Integer registrations) {
        this.registrations = registrations;
    }

    public BigDecimal getCostPerRegistration() {
        return costPerRegistration;
    }

    public void setCostPerRegistration(BigDecimal costPerRegistration) {
        this.costPerRegistration = costPerRegistration;
    }

    public Double getActivationRegistrationRate() {
        return activationRegistrationRate;
    }

    public void setActivationRegistrationRate(Double activationRegistrationRate) {
        this.activationRegistrationRate = activationRegistrationRate;
    }

    public Double getRegistrationRate() {
        return registrationRate;
    }

    public void setRegistrationRate(Double registrationRate) {
        this.registrationRate = registrationRate;
    }

    public Double getNextDayRetentionRate() {
        return nextDayRetentionRate;
    }

    public void setNextDayRetentionRate(Double nextDayRetentionRate) {
        this.nextDayRetentionRate = nextDayRetentionRate;
    }

    public Double getThreeDayRetentionRate() {
        return threeDayRetentionRate;
    }

    public void setThreeDayRetentionRate(Double threeDayRetentionRate) {
        this.threeDayRetentionRate = threeDayRetentionRate;
    }

    public Double getSevenDayRetentionRate() {
        return sevenDayRetentionRate;
    }

    public void setSevenDayRetentionRate(Double sevenDayRetentionRate) {
        this.sevenDayRetentionRate = sevenDayRetentionRate;
    }

    public Double getThirtyDayRetentionRate() {
        return thirtyDayRetentionRate;
    }

    public void setThirtyDayRetentionRate(Double thirtyDayRetentionRate) {
        this.thirtyDayRetentionRate = thirtyDayRetentionRate;
    }

    public Integer getNewlyIncreasedRecharges() {
        return newlyIncreasedRecharges;
    }

    public void setNewlyIncreasedRecharges(Integer newlyIncreasedRecharges) {
        this.newlyIncreasedRecharges = newlyIncreasedRecharges;
    }

    public BigDecimal getNewlyIncreasedRechargeAmount() {
        return newlyIncreasedRechargeAmount;
    }

    public void setNewlyIncreasedRechargeAmount(BigDecimal newlyIncreasedRechargeAmount) {
        this.newlyIncreasedRechargeAmount = newlyIncreasedRechargeAmount;
    }

    public Double getNewlyIncreasedPayRate() {
        return newlyIncreasedPayRate;
    }

    public void setNewlyIncreasedPayRate(Double newlyIncreasedPayRate) {
        this.newlyIncreasedPayRate = newlyIncreasedPayRate;
    }

    public Integer getTotalNumberOfRecharge() {
        return totalNumberOfRecharge;
    }

    public void setTotalNumberOfRecharge(Integer totalNumberOfRecharge) {
        this.totalNumberOfRecharge = totalNumberOfRecharge;
    }

    public BigDecimal getAccumulatedRecharge() {
        return accumulatedRecharge;
    }

    public void setAccumulatedRecharge(BigDecimal accumulatedRecharge) {
        this.accumulatedRecharge = accumulatedRecharge;
    }

    public Double getPayRate() {
        return payRate;
    }

    public void setPayRate(Double payRate) {
        this.payRate = payRate;
    }

    public Integer getDau() {
        return dau;
    }

    public void setDau(Integer dau) {
        this.dau = dau;
    }

    public boolean isNaturalFlow() {
        return isNaturalFlow;
    }

    public void setNaturalFlow(boolean naturalFlow) {
        isNaturalFlow = naturalFlow;
    }

    public BigDecimal getPayAmount1Day() {
        return payAmount1Day;
    }

    public void setPayAmount1Day(BigDecimal payAmount1Day) {
        this.payAmount1Day = payAmount1Day;
    }

    public BigDecimal getPayAmount7Days() {
        return payAmount7Days;
    }

    public void setPayAmount7Days(BigDecimal payAmount7Days) {
        this.payAmount7Days = payAmount7Days;
    }

    public BigDecimal getPayAmount30Days() {
        return payAmount30Days;
    }

    public void setPayAmount30Days(BigDecimal payAmount30Days) {
        this.payAmount30Days = payAmount30Days;
    }

    public Double getFirstDayPaybackRate() {
        return firstDayPaybackRate;
    }

    public void setFirstDayPaybackRate(Double firstDayPaybackRate) {
        this.firstDayPaybackRate = firstDayPaybackRate;
    }

    public BigDecimal getTotalRecharges() {
        return totalRecharges;
    }

    public void setTotalRecharges(BigDecimal totalRecharges) {
        this.totalRecharges = totalRecharges;
    }
    public Integer getRegisterAccountNum() {
		return registerAccountNum;
	}
	public void setRegisterAccountNum(Integer registerAccountNum) {
		this.registerAccountNum = registerAccountNum;
	}
	

	public Integer getFirstDayRegistrations() {
		return firstDayRegistrations;
	}

	public void setFirstDayRegistrations(Integer firstDayRegistrations) {
		this.firstDayRegistrations = firstDayRegistrations;
	}

	public Integer getFirstDayRegisterAccountNum() {
		return firstDayRegisterAccountNum;
	}

	public void setFirstDayRegisterAccountNum(Integer firstDayRegisterAccountNum) {
		this.firstDayRegisterAccountNum = firstDayRegisterAccountNum;
	}
	public Double getFifteenDayRetentionRate() {
		return fifteenDayRetentionRate;
	}

	public void setFifteenDayRetentionRate(Double fifteenDayRetentionRate) {
		this.fifteenDayRetentionRate = fifteenDayRetentionRate;
	}

	public BigDecimal getRegistrationAccountNumCpa() {
		return registrationAccountNumCpa;
	}

	public void setRegistrationAccountNumCpa(BigDecimal registrationAccountNumCpa) {
		this.registrationAccountNumCpa = registrationAccountNumCpa;
	}

	public String getSystemAccountName() {
		return systemAccountName;
	}

	public void setSystemAccountName(String systemAccountName) {
		this.systemAccountName = systemAccountName;
	}

	public Integer getSystemAccountId() {
		return systemAccountId;
	}

	public void setSystemAccountId(Integer systemAccountId) {
		this.systemAccountId = systemAccountId;
	}

	public BigDecimal getLtv1Day() {
		return ltv1Day;
	}

	public void setLtv1Day(BigDecimal ltv1Day) {
		this.ltv1Day = ltv1Day;
	}

	public BigDecimal getLtv7Days() {
		return ltv7Days;
	}

	public void setLtv7Days(BigDecimal ltv7Days) {
		this.ltv7Days = ltv7Days;
	}

	public BigDecimal getLtv30Days() {
		return ltv30Days;
	}

	public void setLtv30Days(BigDecimal ltv30Days) {
		this.ltv30Days = ltv30Days;
	}

	public Integer getRegisterAccountNumD3() {
		return registerAccountNumD3;
	}

	public void setRegisterAccountNumD3(Integer registerAccountNumD3) {
		this.registerAccountNumD3 = registerAccountNumD3;
	}

	public Integer getRegisterAccountNumD7() {
		return registerAccountNumD7;
	}

	public void setRegisterAccountNumD7(Integer registerAccountNumD7) {
		this.registerAccountNumD7 = registerAccountNumD7;
	}

	public Integer getRegisterAccountNumD15() {
		return registerAccountNumD15;
	}

	public void setRegisterAccountNumD15(Integer registerAccountNumD15) {
		this.registerAccountNumD15 = registerAccountNumD15;
	}

	public Integer getRegisterAccountNumD30() {
		return registerAccountNumD30;
	}

	public void setRegisterAccountNumD30(Integer registerAccountNumD30) {
		this.registerAccountNumD30 = registerAccountNumD30;
	}

	public Integer getRetainD1() {
		return retainD1;
	}

	public void setRetainD1(Integer retainD1) {
		this.retainD1 = retainD1;
	}

	public Integer getRetainD3() {
		return retainD3;
	}

	public void setRetainD3(Integer retainD3) {
		this.retainD3 = retainD3;
	}

	public Integer getRetainD7() {
		return retainD7;
	}

	public void setRetainD7(Integer retainD7) {
		this.retainD7 = retainD7;
	}

	public Integer getRetainD14() {
		return retainD14;
	}

	public void setRetainD14(Integer retainD14) {
		this.retainD14 = retainD14;
	}

	public Integer getRetainD30() {
		return retainD30;
	}

	public void setRetainD30(Integer retainD30) {
		this.retainD30 = retainD30;
	}

	@Override
	public String toString() {
		return "AdReport [id=" + id + ", date=" + date + ", dateLink="
				+ dateLink + ", medium=" + medium + ", mediumLink="
				+ mediumLink + ", mediumId=" + mediumId + ", account="
				+ account + ", accountAlias=" + accountAlias + ", accountLink="
				+ accountLink + ", accountId=" + accountId + ", plan=" + plan
				+ ", planLink=" + planLink + ", planId=" + planId
				+ ", totalCost=" + totalCost + ", exposures=" + exposures
				+ ", clicks=" + clicks + ", totalClicks=" + totalClicks
				+ ", ctr=" + ctr + ", cpc=" + cpc + ", landingPageUV="
				+ landingPageUV + ", reachRate=" + reachRate
				+ ", retentionTime=" + retentionTime + ", downloadUV="
				+ downloadUV + ", downloadRate=" + downloadRate
				+ ", activations=" + activations + ", activationRate="
				+ activationRate + ", activationCpa=" + activationCpa
				+ ", registrations=" + registrations + ", costPerRegistration="
				+ costPerRegistration + ", activationRegistrationRate="
				+ activationRegistrationRate + ", registrationRate="
				+ registrationRate + ", nextDayRetentionRate="
				+ nextDayRetentionRate + ", threeDayRetentionRate="
				+ threeDayRetentionRate + ", sevenDayRetentionRate="
				+ sevenDayRetentionRate + ", fifteenDayRetentionRate="
				+ fifteenDayRetentionRate + ", thirtyDayRetentionRate="
				+ thirtyDayRetentionRate + ", newlyIncreasedRecharges="
				+ newlyIncreasedRecharges + ", newlyIncreasedRechargeAmount="
				+ newlyIncreasedRechargeAmount + ", newlyIncreasedPayRate="
				+ newlyIncreasedPayRate + ", firstDayPaybackRate="
				+ firstDayPaybackRate + ", payAmount1Day=" + payAmount1Day
				+ ", payAmount7Days=" + payAmount7Days + ", payAmount30Days="
				+ payAmount30Days + ", totalRecharges=" + totalRecharges
				+ ", totalNumberOfRecharge=" + totalNumberOfRecharge
				+ ", accumulatedRecharge=" + accumulatedRecharge + ", payRate="
				+ payRate + ", dau=" + dau + ", isNaturalFlow=" + isNaturalFlow
				+ ", registerAccountNum=" + registerAccountNum
				+ ", firstDayRegistrations=" + firstDayRegistrations
				+ ", firstDayRegisterAccountNum=" + firstDayRegisterAccountNum
				+ ", registrationAccountNumCpa=" + registrationAccountNumCpa
				+ ", systemAccountName=" + systemAccountName
				+ ", systemAccountId=" + systemAccountId + ", ltv1Day="
				+ ltv1Day + ", ltv7Days=" + ltv7Days + ", ltv30Days="
				+ ltv30Days + ", registerAccountNumD3=" + registerAccountNumD3
				+ ", registerAccountNumD7=" + registerAccountNumD7
				+ ", registerAccountNumD15=" + registerAccountNumD15
				+ ", registerAccountNumD30=" + registerAccountNumD30
				+ ", retainD1=" + retainD1 + ", retainD3=" + retainD3
				+ ", retainD7=" + retainD7 + ", retainD14=" + retainD14
				+ ", retainD30=" + retainD30 + "]";
	}
}
