package com.dataeye.ad.assistor.module.realtimedelivery.model;

/**
 * Created by huangzehai on 2017/5/25.
 */
public class TrackingStat extends Time {
    private Integer pageId;
    private Integer activations;

    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

    public Integer getActivations() {
        return activations;
    }

    public void setActivations(Integer activations) {
        this.activations = activations;
    }

    @Override
    public String toString() {
        return "TrackingStat{" +
                "pageId=" + pageId +
                ", activations=" + activations +
                '}';
    }
}
