package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.math.BigDecimal;
import java.util.Arrays;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.google.gson.annotations.Expose;

/**
 * <pre>
 * 媒体账户VO
 * @author luzhuyou 2017/02/20
 */
public class MediumAccountVo extends DataPermissionDomain {

	/** 媒体账户ID */
	@Expose
	private Integer mediumAccountId;
	/** 产品ID,多个用逗号隔开 */
	@Expose
	private String productIds;
	/** 产品名称 */
	@Expose
	private String productNames;
	/** 媒体ID */
	@Expose
	private Integer mediumId;
	/** 媒体名称 */
	@Expose
	private String mediumName;
	/** 媒体主页 */
	@Expose
	private String mediumHomepage;
	/** 媒体账号 */
	@Expose
	private String mediumAccount;
	/** 媒体账号别名 */
	@Expose
	private String mediumAccountAlias;
	/** 密码 */
	private String password;
	/**优化师ADT账号*/
	@Expose
	private String responsibleAccounts;
	/** 系统账号名称列表(至少一个系统账号，多个以逗号分隔) */
	@Expose
	private String followerAccounts;
	/** 折扣率 */
	@Expose
	private Double discountRate;
	/** 登录状态:0-登陆态正常，1-登录态异常 */
	@Expose
	private Integer loginStatus;
	/** 状态：0-正常，1-失效 */
	@Expose
	private Integer status;
	/** 媒体ID列表，仅作为查询参数 */
	private Integer[] mediumIds;
	/** 媒体账号ID列表，仅作为查询参数 */
	private Integer[] mediumAccountIds;
	
	/**1-推广计划，2-推广分组，参考ad_crawler_page.page_type**/
	@Expose
	private Integer pageType;

	/**
	 * Cookie
	 */
	private String cookie;
	/***优化师系统帐号和别名**/
	@Expose
	private String responsibleAccountNameAndAlias;
	/***系统帐号和别名**/
	@Expose
	private String followerAccountNameAndAlias;
	
	//爬虫需要的标识，即媒体帐号的帐号id
	private String skey;
	
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public String getProductIds() {
		return productIds;
	}
	public void setProductIds(String productIds) {
		this.productIds = productIds;
	}
	public String getProductNames() {
		return productNames;
	}
	public void setProductNames(String productNames) {
		this.productNames = productNames;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	public String getMediumHomepage() {
		return mediumHomepage;
	}
	public void setMediumHomepage(String mediumHomepage) {
		this.mediumHomepage = mediumHomepage;
	}
	public String getMediumAccount() {
		return mediumAccount;
	}
	public void setMediumAccount(String mediumAccount) {
		this.mediumAccount = mediumAccount;
	}
	public String getMediumAccountAlias() {
		return mediumAccountAlias;
	}
	public void setMediumAccountAlias(String mediumAccountAlias) {
		this.mediumAccountAlias = mediumAccountAlias;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getResponsibleAccounts() {
		return responsibleAccounts;
	}

	public void setResponsibleAccounts(String responsibleAccounts) {
		this.responsibleAccounts = responsibleAccounts;
	}

	public String getFollowerAccounts() {
		return followerAccounts;
	}
	public void setFollowerAccounts(String followerAccounts) {
		this.followerAccounts = followerAccounts;
	}
	public Double getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(Double discountRate) {
		this.discountRate = discountRate;
	}
	public Integer getLoginStatus() {
		return loginStatus;
	}
	public void setLoginStatus(Integer loginStatus) {
		this.loginStatus = loginStatus;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer[] getMediumIds() {
		return mediumIds;
	}
	public void setMediumIds(Integer[] mediumIds) {
		this.mediumIds = mediumIds;
	}
	public Integer[] getMediumAccountIds() {
		return mediumAccountIds;
	}
	public void setMediumAccountIds(Integer[] mediumAccountIds) {
		this.mediumAccountIds = mediumAccountIds;
	}

	public String getCookie() {
		return cookie;
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	public Integer getPageType() {
		return pageType;
	}
	public void setPageType(Integer pageType) {
		this.pageType = pageType;
	}
	public String getResponsibleAccountNameAndAlias() {
		return responsibleAccountNameAndAlias;
	}
	public void setResponsibleAccountNameAndAlias(
			String responsibleAccountNameAndAlias) {
		this.responsibleAccountNameAndAlias = responsibleAccountNameAndAlias;
	}
	public String getFollowerAccountNameAndAlias() {
		return followerAccountNameAndAlias;
	}
	public void setFollowerAccountNameAndAlias(String followerAccountNameAndAlias) {
		this.followerAccountNameAndAlias = followerAccountNameAndAlias;
	}
	public String getSkey() {
		return skey;
	}
	public void setSkey(String skey) {
		this.skey = skey;
	}
	@Override
	public String toString() {
		return "MediumAccountVo [mediumAccountId=" + mediumAccountId
				+ ", productIds=" + productIds + ", productNames="
				+ productNames + ", mediumId=" + mediumId + ", mediumName="
				+ mediumName + ", mediumHomepage=" + mediumHomepage
				+ ", mediumAccount=" + mediumAccount + ", mediumAccountAlias="
				+ mediumAccountAlias + ", password=" + password
				+ ", responsibleAccounts=" + responsibleAccounts
				+ ", followerAccounts=" + followerAccounts + ", discountRate="
				+ discountRate + ", loginStatus=" + loginStatus + ", status="
				+ status + ", mediumIds=" + Arrays.toString(mediumIds)
				+ ", mediumAccountIds=" + Arrays.toString(mediumAccountIds)
				+ ", pageType=" + pageType + ", cookie=" + cookie
				+ ", responsibleAccountNameAndAlias="
				+ responsibleAccountNameAndAlias
				+ ", followerAccountNameAndAlias="
				+ followerAccountNameAndAlias + ", skey=" + skey + "]";
	}


}
