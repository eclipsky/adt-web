package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.model.Period;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeReport;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeReportQuery;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.Map;

/**
 * Created by huangzehai on 2017/2/27.
 */
public interface RealTimeReportService {

    /**
     * 获取所有用户最近一个小时、最近半小时，最近20分钟，最近10分钟的实时时段指标
     *
     * @return
     */
    Map<Period, Map<Integer, RealTimeReport>> getLastPeriodIndicators();


    /**
     * 获取登录用户最近一个小时、最近半小时，最近20分钟，最近10分钟的实时时段指标
     *
     * @param dataPermissionDomain
     * @return
     */
    Map<Period, Map<Integer, RealTimeReport>> getLastPeriodIndicators(DataPermissionDomain dataPermissionDomain);

    /**
     * 获取指定时间前的指标
     *
     * @param query
     */
    Map<Integer, RealTimeReport> getIndicatorAtTime(RealTimeReportQuery query);
}
