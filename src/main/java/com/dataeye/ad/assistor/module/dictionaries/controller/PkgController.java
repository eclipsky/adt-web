package com.dataeye.ad.assistor.module.dictionaries.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.dictionaries.model.Pkg;
import com.dataeye.ad.assistor.module.dictionaries.service.PkgService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.github.pagehelper.PageHelper;

/**
 * Created by luzhuyou
 * 投放包控制器.
 */
@Controller
public class PkgController {
	
    @Autowired
    private PkgService pkgService;

    /**
     * 下拉框查询投放包
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/pkg/queryPkgForSelector.do")
    public Object queryPkgForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
        
    	int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
    	String pkgName = parameter.getParameter("pkgName");
    	if(StringUtils.isBlank(pkgName)) {
    		pkgName = null;
        }
    	
    	// 对所有公司所有产品，下拉框查询时均显示包
    	List<Pkg> result = pkgService.queryByCompany(companyId,pkgName);
    	// 对优依购（companyId=3），暂时投放包只返回无的数据，优依购的处理逻辑将落地页替换为“短链”，包不需要维护，只显示“无”
//        if(companyId == 1 || companyId == 2) { 
//        	result = pkgService.queryByCompany(companyId);
//        }
        if(result == null) {
        	result = new ArrayList<Pkg>();
		}
        if(StringUtils.isBlank(pkgName)) {
        	// 在包下拉框值中第一行添加“无”的记录
            Pkg pkg = new Pkg();
            pkg.setPkgId(0);
            pkg.setPkgName("无");
            result.add(0, pkg);
        }
        
        
        PageData pageData = PageDataHelper.getPageDataByList(parameter.getPageSize(), parameter.getPageId(), result);
        return pageData;
    }
    
    /**
     * 根据公司id和短链id查询投放包
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/pkg/queryPkgByCpPkgId.do")
    public Object queryPkgByCpPkgId(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
        
    	int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
    	String cpPkgId = parameter.getParameter("cpPkgId");
    	if(StringUtils.isBlank(cpPkgId)) {
    		ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_ID_IS_NULL);
        }
    	List<Pkg> result = pkgService.queryByTrackingInfo(companyId, null, cpPkgId, null);
    	Pkg pkg = new Pkg();
    	if(!result.isEmpty()){
    		pkg = result.get(0);
    	}
        return pkg;
    }

}
