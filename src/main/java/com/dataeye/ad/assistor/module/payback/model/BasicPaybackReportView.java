package com.dataeye.ad.assistor.module.payback.model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;

public class BasicPaybackReportView {

    /**
     * 日期.
     */
    @Expose
    private String date;
    /**
     * 消耗.
     */
    @Expose
    private String totalCost;
    
    /**
     *  注册账号数.
     */
    @Expose
    private Integer registerAccountNum;
    

    /**
     * 充值账号数
     */
    @Expose
    private Integer totalRecharges;
    
    /**
     * 充值金额
     */
    @Expose
    private BigDecimal totalRechargeAmount = new BigDecimal(0);
    

    /**
     * 付费率.
     */
    @Expose
    private String payRate;
    
    /**
     * ARPPU.
     */
    @Expose
    private String arppu;
    
    /**
     * ARPU.
     */
    @Expose
    private String arpu;

    /**
     * 新增注册数.
     
    @Expose
    private Integer registrations;*/

    /**
     * 注册CPA
   
    @Expose
    private String cpa;  */
    
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getTotalRechargeAmount() {
        return totalRechargeAmount;
    }

    public void setTotalRechargeAmount(BigDecimal totalRechargeAmount) {
        this.totalRechargeAmount = totalRechargeAmount;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getArpu() {
        return arpu;
    }

    public void setArpu(String arpu) {
        this.arpu = arpu;
    }

    public Integer getRegisterAccountNum() {
		return registerAccountNum;
	}

	public void setRegisterAccountNum(Integer registerAccountNum) {
		this.registerAccountNum = registerAccountNum;
	}

	public Integer getTotalRecharges() {
		return totalRecharges;
	}

	public void setTotalRecharges(Integer totalRecharges) {
		this.totalRecharges = totalRecharges;
	}

	public String getPayRate() {
		return payRate;
	}

	public void setPayRate(String payRate) {
		this.payRate = payRate;
	}

	public String getArppu() {
		return arppu;
	}

	public void setArppu(String arppu) {
		this.arppu = arppu;
	}

	@Override
	public String toString() {
		return "BasicPaybackReportView [date=" + date + ", totalCost="
				+ totalCost + ", registerAccountNum=" + registerAccountNum
				+ ", totalRecharges=" + totalRecharges
				+ ", totalRechargeAmount=" + totalRechargeAmount + ", payRate="
				+ payRate + ", arppu=" + arppu + ", arpu=" + arpu + "]";
	}
}
