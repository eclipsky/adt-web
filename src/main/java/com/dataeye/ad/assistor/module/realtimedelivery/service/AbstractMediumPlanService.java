package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.mapper.MediumAccountMapper;
import com.dataeye.ad.assistor.module.mediumaccount.model.LoginStatus;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountQuery;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountVo;
import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.model.MediumPlan;
import com.dataeye.ad.assistor.util.HttpUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * 投放状态抽象服务
 * Created by huangzehai on 2017/5/10.
 */
@Service
public abstract class AbstractMediumPlanService<T extends MediumPlan> implements MediumPlanService<T> {

    /**
     * 媒体账号ID不能为空消息.
     */
    private static final String MEDIUM_ACCOUNT_ID_MISSING = "媒体账号ID不能为空";

    /**
     * 媒体账号未登录消息.
     */
    private static final String MEDIUM_ACCOUNT_LOGOUT = "媒体账号未登录";

    /**
     * UTF-8编码
     */
    private static final String UTF8 = "UTF-8";

    /**
     * HTTP成功响应状态码
     */
    private static final int SUCCESS_STATUS_CODE = 200;

    /**
     * 请求失败消息。
     */
    public static final String REQUEST_FAIL = "请求失败";

    /**
     * HTTP头部字段名.
     */
    public static final String HOST = "Host";
    public static final String ORIGIN = "Origin";
    public static final String REFERER = "Referer";
    public static final String X_CSRF_TOKEN = "X-CSRF-TOKEN";
    public static final String HTTPS = "https";


    private Logger logger = LoggerFactory.getLogger(AbstractMediumPlanService.class);

    @Autowired
    private MediumAccountMapper mediumAccountMapper;

    /**
     * 返回请求URL。
     *
     * @return
     */
    protected abstract String url(String cookie, Long mediumPlanId);

    /**
     * 构造请求参数.
     *
     * @param cookie 媒体账号Cookie.
     * @param status 出价参数
     * @return
     */
    protected abstract List<NameValuePair> parameters(String cookie, T status);

    /**
     * 设置请求头部.
     *
     * @param request
     */
    protected abstract void requestHeader(HttpRequestBase request, String cookie);

    /**
     * 设置通用头部
     *
     * @param request
     * @param cookie
     */
    private void setCommonHeader(HttpRequestBase request, String cookie) {
        request.setHeader("Cookie", cookie);
//        request.setHeader("Host", "client.pfp.sina.com.cn");
//        request.setHeader("Origin", "http://client.pfp.sina.com.cn");
//        request.setHeader("Referer", "http://client.pfp.sina.com.cn/main.html");
        request.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");
        request.setHeader("X-Requested-With", "XMLHttpRequest");
        request.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");
        request.setHeader("Accept-Language", "zh-CN,zh;q=0.8");
        request.setHeader("Accept-Encoding", "gzip, deflate");
        request.setHeader("Connection", "keep-alive");
        request.setHeader("Content-Type", "application/x-www-form-urlencoded");
    }

    /**
     * 发起http请求.
     *
     * @param cookie
     * @param status
     * @return
     * @throws IOException
     */
    private HttpResponse request(String cookie, T status) throws IOException {
        String url = url(cookie, status.getMediumPlanId());
        HttpPost httpPost = new HttpPost(url);
        // 设置头字段
        setCommonHeader(httpPost, cookie);
        requestHeader(httpPost, cookie);
        List<NameValuePair> params = parameters(cookie, status);
        // 设置表单参数
        httpPost.setEntity(new UrlEncodedFormEntity(params, UTF8));
        HttpClient httpClient;
        if (StringUtils.startsWithIgnoreCase(url, HTTPS)) {
            httpClient = HttpUtil.getHttpsClient();
        } else {
            httpClient = HttpUtil.getHttpClient();
        }
        logger.info("HTTP POST -> URL:{}, params:{}", httpPost.getURI(), params);
        // 发送请求
        return httpClient.execute(httpPost);
    }

    /**
     * 修改投放状态.
     *
     * @param mediumPlan
     */
    @Override
    public Result execute(T mediumPlan) {
        Result result = new Result();
        if (mediumPlan.getMediumAccountId() == null || mediumPlan.getMediumAccountId() <= 0) {
            result.setSuccess(false);
            result.setMessage(MEDIUM_ACCOUNT_ID_MISSING);
            return result;
        }
        logger.info("媒体改价 -> mediumId:{}, mediumAccountId:{}", mediumPlan.getMediumId(), mediumPlan.getMediumAccountId());
        //根据媒体账号ID获取Cookie
        MediumAccountQuery mediumAccountQuery = new MediumAccountQuery();
        mediumAccountQuery.setMediumId(mediumPlan.getMediumId());
        mediumAccountQuery.setMediumAccountId(mediumPlan.getMediumAccountId());
        MediumAccountVo mediumAccount = mediumAccountMapper.getMediumAccount(mediumAccountQuery);
        //获取该媒体账号的Cookie.
        if (mediumAccount == null || LoginStatus.parse(mediumAccount.getLoginStatus()) != LoginStatus.Login || StringUtils.isBlank(mediumAccount.getCookie())) {
            result.setSuccess(false);
            result.setMessage(MEDIUM_ACCOUNT_LOGOUT);
            return result;
        }
        if(mediumAccount.getSkey() != null){
        	mediumPlan.setMediumUserId(mediumAccount.getSkey());
        }

        try {
            HttpResponse response = request(mediumAccount.getCookie(), mediumPlan);
            result = response(response);
        } catch (IOException e) {
            logger.error(e.getMessage());
            result.setSuccess(false);
            result.setMessage(REQUEST_FAIL);
        }
        return result;
    }

    /**
     * 将响应转化为标准结果.
     *
     * @param response
     * @return
     */
    private Result response(HttpResponse response) {
        int statusCode = response.getStatusLine().getStatusCode();
        String content = HttpUtil.getContent(response.getEntity());
        logger.info(content);
        //检查状态码和响应
        if (statusCode == SUCCESS_STATUS_CODE && StringUtils.isNotBlank(content)) {
            return response(content);
        } else {
            Result result = new Result();
            result.setSuccess(false);
            result.setMessage(REQUEST_FAIL);
            return result;
        }
    }

    /**
     * 解析响应内容.
     *
     * @param content
     * @return
     */
    protected abstract Result response(String content);

}
