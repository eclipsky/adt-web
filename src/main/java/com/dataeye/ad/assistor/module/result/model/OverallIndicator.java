package com.dataeye.ad.assistor.module.result.model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;

/**
 * Created by huangzehai on 2017/4/26.
 */
public class OverallIndicator {
    /**
     * 累计消耗。
     */
    @Expose
    private String cost;

    /**
     * 累计充值.
     */
    @Expose
    private String recharge;

    /**
     * 回本率
     */
    @Expose
    private Double paybackRate;

    /**
     * 媒体账户余额
     */
    @Expose
    private String balance;
    /**
     * 总体CPA
     */
    @Expose
    private BigDecimal cpa;
    /**
     * DAU.
     */
    @Expose
    private Long dau;

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getRecharge() {
        return recharge;
    }

    public void setRecharge(String recharge) {
        this.recharge = recharge;
    }

    public Double getPaybackRate() {
        return paybackRate;
    }

    public void setPaybackRate(Double paybackRate) {
        this.paybackRate = paybackRate;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public BigDecimal getCpa() {
        return cpa;
    }

    public void setCpa(BigDecimal cpa) {
        this.cpa = cpa;
    }

    public Long getDau() {
        return dau;
    }

    public void setDau(Long dau) {
        this.dau = dau;
    }

    @Override
    public String toString() {
        return "OverallIndicator{" +
                "cost='" + cost + '\'' +
                ", recharge='" + recharge + '\'' +
                ", paybackRate=" + paybackRate +
                ", balance='" + balance + '\'' +
                ", cpa=" + cpa +
                ", dau=" + dau +
                '}';
    }
}
