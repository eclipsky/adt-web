package com.dataeye.ad.assistor.module.realtimedelivery.model;

/**
 * 实时投放告警信息VO
 * @author luzhuyou
 *
 */
public class DeliveryAlertInfoVo {
	/** 当前日期 */
	private String currentDate;
	/** 媒体名称 */
	private String medium;
	/** 账户 */
	private String account;
	/** 计划ID. */
	private Integer planId;
	/** 计划名称 */
	private String planName;
	/** 总消耗 */
	private Double consume;
	/** CTR */
	private Double ctr;
	/** 下载率 */
	private Double downloadRate;
	/** CPA */
	private Double costPerRegistration;
	/** 告警邮箱 */
	private String alertEmails;
	/** 消耗告警阈值 */
	private Double totalCostThreshold;

	// 以下CPA相关指标 ： 基于首日注册设备数计算

	/** CPA告警阈值 */
	private Double costPerRegistrationThreshold;
	/** CTR告警阈值 */
	private Double ctrThreshold;
	/** 下载率告警阈值 */
	private Double downloadRateThreshold;
	/** 是否邮件告警 */
	private Integer emailAlert;
	/** 下载单价(元) */
	private Double costPerDownload;
	/** 消耗操作符：0-小于等于, 1-大于等于 */
	private Integer costOperator;
	/** CPA操作符：0-小于等于, 1-大于等于 */
	private Integer cpaOperator;
	/** CTR操作符：0-小于等于, 1-大于等于 */
	private Integer ctrOperator;
	/** 下载率操作符：0-小于等于, 1-大于等于 */
	private Integer downloadRateOperator;
	/** 下载单价操作符：0-小于等于, 1-大于等于 */
	private Integer costPerDownloadOperator;

	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Double getConsume() {
		return consume;
	}
	public void setConsume(Double consume) {
		this.consume = consume;
	}
	public Double getCtr() {
		return ctr;
	}
	public void setCtr(Double ctr) {
		this.ctr = ctr;
	}
	public Double getDownloadRate() {
		return downloadRate;
	}
	public void setDownloadRate(Double downloadRate) {
		this.downloadRate = downloadRate;
	}
	public Double getCostPerRegistration() {
		return costPerRegistration;
	}
	public void setCostPerRegistration(Double costPerRegistration) {
		this.costPerRegistration = costPerRegistration;
	}
	public String getAlertEmails() {
		return alertEmails;
	}
	public void setAlertEmails(String alertEmails) {
		this.alertEmails = alertEmails;
	}
	public Double getTotalCostThreshold() {
		return totalCostThreshold;
	}
	public void setTotalCostThreshold(Double totalCostThreshold) {
		this.totalCostThreshold = totalCostThreshold;
	}
	public Double getCostPerRegistrationThreshold() {
		return costPerRegistrationThreshold;
	}
	public void setCostPerRegistrationThreshold(Double costPerRegistrationThreshold) {
		this.costPerRegistrationThreshold = costPerRegistrationThreshold;
	}
	public Double getCtrThreshold() {
		return ctrThreshold;
	}
	public void setCtrThreshold(Double ctrThreshold) {
		this.ctrThreshold = ctrThreshold;
	}
	public Double getDownloadRateThreshold() {
		return downloadRateThreshold;
	}
	public void setDownloadRateThreshold(Double downloadRateThreshold) {
		this.downloadRateThreshold = downloadRateThreshold;
	}
	public Integer getEmailAlert() {
		return emailAlert;
	}
	public void setEmailAlert(Integer emailAlert) {
		this.emailAlert = emailAlert;
	}
	public Double getCostPerDownload() {
		return costPerDownload;
	}
	public void setCostPerDownload(Double costPerDownload) {
		this.costPerDownload = costPerDownload;
	}
	public Integer getCostOperator() {
		return costOperator;
	}
	public void setCostOperator(Integer costOperator) {
		this.costOperator = costOperator;
	}
	public Integer getCpaOperator() {
		return cpaOperator;
	}
	public void setCpaOperator(Integer cpaOperator) {
		this.cpaOperator = cpaOperator;
	}
	public Integer getCtrOperator() {
		return ctrOperator;
	}
	public void setCtrOperator(Integer ctrOperator) {
		this.ctrOperator = ctrOperator;
	}
	public Integer getDownloadRateOperator() {
		return downloadRateOperator;
	}
	public void setDownloadRateOperator(Integer downloadRateOperator) {
		this.downloadRateOperator = downloadRateOperator;
	}
	public Integer getCostPerDownloadOperator() {
		return costPerDownloadOperator;
	}
	public void setCostPerDownloadOperator(Integer costPerDownloadOperator) {
		this.costPerDownloadOperator = costPerDownloadOperator;
	}
	@Override
	public String toString() {
		return "DeliveryAlertInfoVo [currentDate=" + currentDate + ", medium="
				+ medium + ", account=" + account + ", planId=" + planId
				+ ", planName=" + planName + ", consume=" + consume + ", ctr="
				+ ctr + ", downloadRate=" + downloadRate
				+ ", costPerRegistration=" + costPerRegistration
				+ ", alertEmails=" + alertEmails + ", totalCostThreshold="
				+ totalCostThreshold + ", costPerRegistrationThreshold="
				+ costPerRegistrationThreshold + ", ctrThreshold="
				+ ctrThreshold + ", downloadRateThreshold="
				+ downloadRateThreshold + ", emailAlert=" + emailAlert
				+ ", costPerDownload=" + costPerDownload + ", costOperator="
				+ costOperator + ", cpaOperator=" + cpaOperator
				+ ", ctrOperator=" + ctrOperator + ", downloadRateOperator="
				+ downloadRateOperator + ", costPerDownloadOperator="
				+ costPerDownloadOperator + "]";
	}

}
