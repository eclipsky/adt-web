package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.google.gson.annotations.Expose;

/**
 * Created by huangzehai on 2017/4/28.
 */
public class BalanceAlertConf {
    private int id;
    /**
     * 公司ID.
     */
    private int companyId;
    /**
     * 立即邮件通知:0-否, 1-是
     */
    @Expose
    private int emailAlert;
    /**
     * 负责人系统账户ID.
     */
    private int accountId;
    
    /**
     * 媒体账号余额标红逻辑的默认值，默认为5
     */
    @Expose
    private int balanceRedNum ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getEmailAlert() {
        return emailAlert;
    }

    public void setEmailAlert(int emailAlert) {
        this.emailAlert = emailAlert;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
    public int getBalanceRedNum() {
		return balanceRedNum;
	}
	public void setBalanceRedNum(int balanceRedNum) {
		this.balanceRedNum = balanceRedNum;
	}

	@Override
	public String toString() {
		return "BalanceAlertConf [id=" + id + ", companyId=" + companyId
				+ ", emailAlert=" + emailAlert + ", accountId=" + accountId
				+ ", balanceRedNum=" + balanceRedNum + "]";
	}
}
