package com.dataeye.ad.assistor.module.campaign.constants;

import com.google.gson.annotations.Expose;

/**
 * 预创建推广链接接口响应数据
 * @author luzhuyou 2017/06/21
 *
 */
public class TrackingResTrackUrl {
	@Expose
	private String campaignId;
	private String campaignName;
	@Expose
	private String trackUrl;
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getTrackUrl() {
		return trackUrl;
	}
	public void setTrackUrl(String trackUrl) {
		this.trackUrl = trackUrl;
	}
	@Override
	public String toString() {
		return "TrackingResTrackUrl [campaignId=" + campaignId
				+ ", campaignName=" + campaignName + ", trackUrl=" + trackUrl
				+ "]";
	}
	
}
