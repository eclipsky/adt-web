package com.dataeye.ad.assistor.module.report.mapper;

import com.dataeye.ad.assistor.module.report.model.AdReport;
import com.dataeye.ad.assistor.module.report.model.DeliveryReportQuery;

import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 投放日报
 * Created by huangzehai on 2016/12/27.
 */
@MapperScan
public interface AdReportMapper {

    /**
     * 投放日报-计划维度-汇总数据
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> planSummaryReport(DeliveryReportQuery query);

    /**
     * 投放日报-推广计划-按期间汇总-汇总数据
     *
     * @param query
     * @return
     */
    AdReport planSummaryReportTotal(DeliveryReportQuery query);

    /**
     * 投放日报-计划维度-汇总数据-总记录数
     *
     * @param query 查询条件
     * @return
     */
    int planSummaryReportTotalCount(DeliveryReportQuery query);

    /**
     * 投放日报-计划维度-汇总数据-详细
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> planSummaryReportDetail(DeliveryReportQuery query);

    /**
     * 投放日报-计划维度-按天数据-详细
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> planDailyReportDetail(DeliveryReportQuery query);

    /**
     * 投放日报-计划维度-按天显示数据-汇总
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> planDailyReport(DeliveryReportQuery query);

    List<AdReport> planDailyReportWithNaturalFlow(DeliveryReportQuery query);


    /**
     * 投放日报-计划维度-按天显示数据-总记录数
     *
     * @param query 查询条件
     * @return
     */
    int planDailyReportTotalCount(DeliveryReportQuery query);

    /**
     * @param query
     * @return
     */
    int mediumDailyReportTotalCount(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-按天显示数据-汇总
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumDailyReport(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-按天显示数据-汇总,包含自然流量
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumDailyReportWithNaturalFlow(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-按天显示数据-展开媒体
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumDailyReportExpandMedium(DeliveryReportQuery query);

    /**
     * 获取投放日报-媒体维度-按天显示数据-展开活动组时返回的数据。
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumDailyReportExpandEventGroup(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-按天显示数据-展开账号计划。
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumDailyReportExpandAccountAndPlan(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumSummaryReport(DeliveryReportQuery query);

    /**
     * 投放日报-媒体-按期间汇总-汇总
     *
     * @param query 查询条件
     * @return
     */
    AdReport mediumSummaryReportTotal(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-总记录数
     *
     * @param query 查询条件
     * @return
     */
    int mediumSummaryReportTotalCount(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-展开日期
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumSummaryReportExpandDate(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-展开活动组
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumSummaryReportExpandEventGroup(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-展开账号和计划
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumSummaryReportExpandAccountAndPlan(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-展开活动组-不展开日期
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumSummaryReportExpandEventGroupNotDate(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-展开账号和计划-不展开日期
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> mediumSummaryReportExpandAccountAndPlanNotDate(DeliveryReportQuery query);
    
    /**
     * 投放日报-子账户Tab
     *
     * @param query 查询条件
     * @return
     */
    List<AdReport> queryindicatorByAccount(DeliveryReportQuery query);
    
    /**
     * 投放日报-子账户Tab-总记录数
     *
     * @param query 查询条件
     * @return
     */
    int queryindicatorByAccountTotalCount(DeliveryReportQuery query);

}
