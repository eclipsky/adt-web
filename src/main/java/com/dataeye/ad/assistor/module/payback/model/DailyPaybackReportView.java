package com.dataeye.ad.assistor.module.payback.model;

import java.math.BigDecimal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyPaybackReportView extends BasicPaybackReportView {
    /**
     * DAU.
     */
    @Expose
    private Integer dau;

    /**
     * 3日回本率.
     */
    @Expose
    private Double paybackRateAfter3days;

    /**
     * 3日回本率,供下载表格使用.
     */
    private String paybackRatePercentAfter3days;

    /**
     * 7日回本率.
     */
    @Expose
    private Double paybackRateAfter7days;

    /**
     * 7日回本率,供下载表格使用
     */
    private String paybackRatePercentAfter7days;

    /**
     * 15日回本率.
     */
    @Expose
    private Double paybackRateAfter15days;

    /**
     * 15日回本率，供下载表格使用
     */
    private String paybackRatePercentAfter15days;

    /**
     * 30日回本率.
     */
    @Expose
    private Double paybackRateAfter30days;

    /**
     * 30日回本率，供下载表格使用
     */
    private String paybackRatePercentAfter30days;

    /**
     * 60日回本率.
     */
    @Expose
    @SerializedName("paybackRateAfter60days")
    private Double paybackRateAfter60days;


    /**
     * 60日回本率,供下载表格使用.
     */
    private String paybackRatePercentAfter60days;

    /**
     * 90日回本率.
     */
    @Expose
    @SerializedName("paybackRateAfter90days")
    private Double paybackRateAfter90days;

    /**
     * 90日回本率，供下载表格使用.
     */
    private String paybackRatePercentAfter90days;
    
    /**
     * 首日回本率.
     */
    @Expose
    private Double paybackRateAfter1days;
    
    /**
     *  首日回本率，供下载表格使用
     */
    private String paybackRatePercentAfter1days;
    /**
     * 2日回本率.
     */
    @Expose
    private Double paybackRateAfter2days;
    /**
     * 4日回本率.
     */
    @Expose
    private Double paybackRateAfter4days;
    /**
     * 5日回本率.
     */
    @Expose
    private Double paybackRateAfter5days;
    /**
     * 6日回本率.
     */
    @Expose
    private Double paybackRateAfter6days;

    /**
     *  2日回本率，供下载表格使用
     */
    private String paybackRatePercentAfter2days;
    /**
     *  4日回本率，供下载表格使用
     */
    private String paybackRatePercentAfter4days;
    /**
     *  5日回本率，供下载表格使用
     */
    private String paybackRatePercentAfter5days;
    /**
     *  6日回本率，供下载表格使用
     */
    private String paybackRatePercentAfter6days;
    
    /**
     * 首日充值金额
     */
    @Expose
    private BigDecimal payAmount1Day;
    /**
     * 2日充值金额
     */
    @Expose
    private BigDecimal payAmount2Day;
    /**
     * 3日充值金额
     */
    @Expose
    private BigDecimal payAmount3Day;
    /**
     * 4日充值金额
     */
    @Expose
    private BigDecimal payAmount4Day;
    /**
     * 5日充值金额
     */
    @Expose
    private BigDecimal payAmount5Day;
    /**
     * 6日充值金额
     */
    @Expose
    private BigDecimal payAmount6Day;
    /**
     * 7日充值金额
     */
    @Expose
    private BigDecimal payAmount7Day;
    /**
     * 15日充值金额
     */
    @Expose
    private BigDecimal payAmount15Day;
    /**
     * 30日充值金额
     */
    @Expose
    private BigDecimal payAmount30Day;
    /**
     * 60日充值金额
     */
    @Expose
    private BigDecimal payAmount60Day;
    /**
     * 90日充值金额
     */
    @Expose
    private BigDecimal payAmount90Day;

    /**
     * 累计充值
     */
    @Expose
    private BigDecimal accumulatedRecharge;
    
    /**
     * 注册CPA.(设备)
     */
    @Expose
    private String registrationCpa;
    
    public Integer getDau() {
        return dau;
    }

    public void setDau(Integer dau) {
        this.dau = dau;
    }

    public Double getPaybackRateAfter3days() {
        return paybackRateAfter3days;
    }

    public void setPaybackRateAfter3days(Double paybackRateAfter3days) {
        this.paybackRateAfter3days = paybackRateAfter3days;
    }

    public String getPaybackRatePercentAfter3days() {
        return paybackRatePercentAfter3days;
    }

    public void setPaybackRatePercentAfter3days(String paybackRatePercentAfter3days) {
        this.paybackRatePercentAfter3days = paybackRatePercentAfter3days;
    }

    public Double getPaybackRateAfter7days() {
        return paybackRateAfter7days;
    }

    public void setPaybackRateAfter7days(Double paybackRateAfter7days) {
        this.paybackRateAfter7days = paybackRateAfter7days;
    }

    public String getPaybackRatePercentAfter7days() {
        return paybackRatePercentAfter7days;
    }

    public void setPaybackRatePercentAfter7days(String paybackRatePercentAfter7days) {
        this.paybackRatePercentAfter7days = paybackRatePercentAfter7days;
    }

    public Double getPaybackRateAfter15days() {
        return paybackRateAfter15days;
    }

    public void setPaybackRateAfter15days(Double paybackRateAfter15days) {
        this.paybackRateAfter15days = paybackRateAfter15days;
    }

    public String getPaybackRatePercentAfter15days() {
        return paybackRatePercentAfter15days;
    }

    public void setPaybackRatePercentAfter15days(String paybackRatePercentAfter15days) {
        this.paybackRatePercentAfter15days = paybackRatePercentAfter15days;
    }

    public Double getPaybackRateAfter30days() {
        return paybackRateAfter30days;
    }

    public void setPaybackRateAfter30days(Double paybackRateAfter30days) {
        this.paybackRateAfter30days = paybackRateAfter30days;
    }

    public String getPaybackRatePercentAfter30days() {
        return paybackRatePercentAfter30days;
    }

    public void setPaybackRatePercentAfter30days(String paybackRatePercentAfter30days) {
        this.paybackRatePercentAfter30days = paybackRatePercentAfter30days;
    }

    public Double getPaybackRateAfter60days() {
        return paybackRateAfter60days;
    }

    public void setPaybackRateAfter60days(Double paybackRateAfter60days) {
        this.paybackRateAfter60days = paybackRateAfter60days;
    }

    public String getPaybackRatePercentAfter60days() {
        return paybackRatePercentAfter60days;
    }

    public void setPaybackRatePercentAfter60days(String paybackRatePercentAfter60days) {
        this.paybackRatePercentAfter60days = paybackRatePercentAfter60days;
    }

    public Double getPaybackRateAfter90days() {
        return paybackRateAfter90days;
    }

    public void setPaybackRateAfter90days(Double paybackRateAfter90days) {
        this.paybackRateAfter90days = paybackRateAfter90days;
    }

    public String getPaybackRatePercentAfter90days() {
        return paybackRatePercentAfter90days;
    }

    public void setPaybackRatePercentAfter90days(String paybackRatePercentAfter90days) {
        this.paybackRatePercentAfter90days = paybackRatePercentAfter90days;
    }

	public Double getPaybackRateAfter1days() {
		return paybackRateAfter1days;
	}

	public void setPaybackRateAfter1days(Double paybackRateAfter1days) {
		this.paybackRateAfter1days = paybackRateAfter1days;
	}

	public String getPaybackRatePercentAfter1days() {
		return paybackRatePercentAfter1days;
	}

	public void setPaybackRatePercentAfter1days(String paybackRatePercentAfter1days) {
		this.paybackRatePercentAfter1days = paybackRatePercentAfter1days;
	}

	public Double getPaybackRateAfter2days() {
		return paybackRateAfter2days;
	}

	public void setPaybackRateAfter2days(Double paybackRateAfter2days) {
		this.paybackRateAfter2days = paybackRateAfter2days;
	}

	public Double getPaybackRateAfter4days() {
		return paybackRateAfter4days;
	}

	public void setPaybackRateAfter4days(Double paybackRateAfter4days) {
		this.paybackRateAfter4days = paybackRateAfter4days;
	}

	public Double getPaybackRateAfter5days() {
		return paybackRateAfter5days;
	}

	public void setPaybackRateAfter5days(Double paybackRateAfter5days) {
		this.paybackRateAfter5days = paybackRateAfter5days;
	}

	public Double getPaybackRateAfter6days() {
		return paybackRateAfter6days;
	}

	public void setPaybackRateAfter6days(Double paybackRateAfter6days) {
		this.paybackRateAfter6days = paybackRateAfter6days;
	}

	public String getPaybackRatePercentAfter2days() {
		return paybackRatePercentAfter2days;
	}

	public void setPaybackRatePercentAfter2days(String paybackRatePercentAfter2days) {
		this.paybackRatePercentAfter2days = paybackRatePercentAfter2days;
	}

	public String getPaybackRatePercentAfter4days() {
		return paybackRatePercentAfter4days;
	}

	public void setPaybackRatePercentAfter4days(String paybackRatePercentAfter4days) {
		this.paybackRatePercentAfter4days = paybackRatePercentAfter4days;
	}

	public String getPaybackRatePercentAfter5days() {
		return paybackRatePercentAfter5days;
	}

	public void setPaybackRatePercentAfter5days(String paybackRatePercentAfter5days) {
		this.paybackRatePercentAfter5days = paybackRatePercentAfter5days;
	}

	public String getPaybackRatePercentAfter6days() {
		return paybackRatePercentAfter6days;
	}

	public void setPaybackRatePercentAfter6days(String paybackRatePercentAfter6days) {
		this.paybackRatePercentAfter6days = paybackRatePercentAfter6days;
	}

	public BigDecimal getPayAmount1Day() {
		return payAmount1Day;
	}

	public void setPayAmount1Day(BigDecimal payAmount1Day) {
		this.payAmount1Day = payAmount1Day;
	}

	public BigDecimal getPayAmount2Day() {
		return payAmount2Day;
	}

	public void setPayAmount2Day(BigDecimal payAmount2Day) {
		this.payAmount2Day = payAmount2Day;
	}

	public BigDecimal getPayAmount3Day() {
		return payAmount3Day;
	}

	public void setPayAmount3Day(BigDecimal payAmount3Day) {
		this.payAmount3Day = payAmount3Day;
	}

	public BigDecimal getPayAmount4Day() {
		return payAmount4Day;
	}

	public void setPayAmount4Day(BigDecimal payAmount4Day) {
		this.payAmount4Day = payAmount4Day;
	}

	public BigDecimal getPayAmount5Day() {
		return payAmount5Day;
	}

	public void setPayAmount5Day(BigDecimal payAmount5Day) {
		this.payAmount5Day = payAmount5Day;
	}

	public BigDecimal getPayAmount6Day() {
		return payAmount6Day;
	}

	public void setPayAmount6Day(BigDecimal payAmount6Day) {
		this.payAmount6Day = payAmount6Day;
	}

	public BigDecimal getPayAmount7Day() {
		return payAmount7Day;
	}

	public void setPayAmount7Day(BigDecimal payAmount7Day) {
		this.payAmount7Day = payAmount7Day;
	}

	public BigDecimal getPayAmount15Day() {
		return payAmount15Day;
	}

	public void setPayAmount15Day(BigDecimal payAmount15Day) {
		this.payAmount15Day = payAmount15Day;
	}

	public BigDecimal getPayAmount30Day() {
		return payAmount30Day;
	}

	public void setPayAmount30Day(BigDecimal payAmount30Day) {
		this.payAmount30Day = payAmount30Day;
	}

	public BigDecimal getPayAmount60Day() {
		return payAmount60Day;
	}

	public void setPayAmount60Day(BigDecimal payAmount60Day) {
		this.payAmount60Day = payAmount60Day;
	}

	public BigDecimal getPayAmount90Day() {
		return payAmount90Day;
	}

	public void setPayAmount90Day(BigDecimal payAmount90Day) {
		this.payAmount90Day = payAmount90Day;
	}

	public BigDecimal getAccumulatedRecharge() {
		return accumulatedRecharge;
	}

	public void setAccumulatedRecharge(BigDecimal accumulatedRecharge) {
		this.accumulatedRecharge = accumulatedRecharge;
	}

	public String getRegistrationCpa() {
		return registrationCpa;
	}

	public void setRegistrationCpa(String registrationCpa) {
		this.registrationCpa = registrationCpa;
	}
	
    
}
