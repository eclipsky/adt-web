package com.dataeye.ad.assistor.module.advertisement.service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ConfiguredStatus;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.PlanGroupType;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ProductType;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.SpeedMode;
import com.dataeye.ad.assistor.module.advertisement.mapper.ApiTxCampaignMapper;
import com.dataeye.ad.assistor.module.advertisement.model.AdGroupQueryVo;
import com.dataeye.ad.assistor.module.advertisement.model.MarketingApiAuth;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupCreatePreVO;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupSelectVO;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupVO;
import com.dataeye.ad.assistor.module.advertisement.service.tencent.AdCampaignService;
import com.dataeye.ad.assistor.module.advertisement.service.tencent.AdFundService;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.StringUtil;

@Service("apiTxCampaignService")
public class ApiTxCampaignService {


    @Autowired
    private ApiTxAdGroupService apiTxAdGroupService;
    @Autowired
    private AdCampaignService adCampaignService;
    @Autowired
    private ApiTxCampaignMapper apiTxCampaignMapper;
    @Autowired
    private AdFundService adFundService;

    /**
     * 查询广告计划列表
     *
     * @param planGroupId
     * @param adGroupId
     * @param mediumId
     * @return
     */
    public List<PlanGroupVO> queryAdPlanGroup(AdGroupQueryVo vo) {
        List<PlanGroupVO> result = apiTxCampaignMapper.query(vo);
        for (PlanGroupVO pVo : result) {
            List<String> productNameList = new ArrayList<>();
            PlanGroupVO adRelationVo = apiTxAdGroupService.queryProductInfosByPlanGroupId(vo.getCompanyId(), pVo.getPlanGroupId(), pVo.getMediumAccountId());
            if (adRelationVo != null && adRelationVo.getProductNames() != null) {
                productNameList = StringUtil.stringToListString(adRelationVo.getProductNames());
            }
            pVo.setProductNameList(productNameList);
            /* MarketingApiAuth tAccountVo = adGroupRelationService.getAccessTokey(pVo.getMediumAccountId());
            AdFund fundVo = adFundService.query(tAccountVo.getAccountId(), tAccountVo.getAccessToken());
            if (fundVo != null) {
                pVo.setFund_status(FundStatus.valueOf(fundVo.getFund_status()).getValue());
            }*/
        }
        return result;
    }

    public int modify(PlanGroupCreatePreVO vo) {

        MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(vo.getMediumAccountId());
        String planGroupStatus = null;
        if (vo.getPlanGroupStatus() != null && ConfiguredStatus.parse(vo.getPlanGroupStatus()) != null) {
            planGroupStatus = ConfiguredStatus.parse(vo.getPlanGroupStatus()).name();
        }
        String speedMode = null;
        if (vo.getSpeedMode() != null && SpeedMode.parse(vo.getSpeedMode()) != null) {
            speedMode = SpeedMode.parse(vo.getSpeedMode()).name();
        }
        Integer daily_budget = null;
        if (vo.getDailyBudget() != null) {
            daily_budget = vo.getDailyBudget();
        }
        /*if (vo != null && StringUtils.isNotBlank(vo.getPlanGroupName())) {
            checkAdPlanGroupName(null, tAccountVo.getAccountId(), tAccountVo.getAccessToken(), vo.getPlanGroupName(), vo.getPlanGroupId());
        }*/
        adCampaignService.update(tAccountVo.getAccountId(), vo.getPlanGroupId(), vo.getPlanGroupName(), daily_budget, planGroupStatus, speedMode, null, tAccountVo.getAccessToken());
        vo.setUpdateTime(new Date());
        return apiTxCampaignMapper.update(vo);
    }

    /**
     * 创建推广计划
     *
     * @param planGroupName 推广计划名称
     * @param dailyBudget   日限额，单位为元，精确到分
     * @param productType   标的物类型
     * @param speedMode     投放速度
     * @param mediumAccount
     * @param mAccountId
     * @param accessToken
     */
    public int add(String planGroupName, BigDecimal dailyBudget, Integer productType, Integer speedMode, Integer mediumAccountId, Integer companyId, Integer systemAccountId, Integer mAccountId, String accessToken) {
        if (mediumAccountId != null) {
            MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
            if (tAccountVo != null) {
                mAccountId = tAccountVo.getAccountId();
                accessToken = tAccountVo.getAccessToken();
            }
        }
       // checkAdPlanGroupName(null, mAccountId, accessToken, planGroupName, null);
        Integer planGroupId = adCampaignService.add(mAccountId, planGroupName, null, ProductType.parse(productType).name(), CurrencyUtils.yuanToFen(dailyBudget).intValue(), SpeedMode.parse(speedMode).name(), accessToken);
        PlanGroupCreatePreVO vo = new PlanGroupCreatePreVO();
        vo.setPlanGroupId(planGroupId);
        vo.setPlanGroupName(planGroupName);
        vo.setDailyBudget(CurrencyUtils.yuanToFen(dailyBudget).intValue());
        vo.setPlanGroupStatus(ConfiguredStatus.AD_STATUS_NORMAL.getValue());
        vo.setProductType(productType);
        vo.setSpeedMode(speedMode);
        vo.setPlanGroupType(PlanGroupType.CAMPAIGN_TYPE_NORMAL.getValue());
        vo.setCompanyId(companyId);
        vo.setSystemAccountId(systemAccountId);
        vo.setMediumAccountId(mediumAccountId);
        vo.setCreateTime(new Date());
        vo.setUpdateTime(new Date());
        return apiTxCampaignMapper.add(vo);
    }

    /**
     * 删除推广计划
     *
     * @param planGroupId
     * @param mediumId
     * @param mediumAccountId
     * @return
     */
    public int delete(Integer planGroupId, Integer mediumId, Integer mediumAccountId) {
        //1.删除关联关系
        apiTxAdGroupService.delete(planGroupId, null, mediumId, mediumAccountId);
        MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
        //2.删除媒体平台上的推广计划
        adCampaignService.delete(tAccountVo.getAccountId(), planGroupId, tAccountVo.getAccessToken());
        PlanGroupCreatePreVO vo = new PlanGroupCreatePreVO();
        vo.setMediumAccountId(mediumAccountId);
        vo.setPlanGroupId(planGroupId);
        return apiTxCampaignMapper.delete(vo);
    }

    /**
     * 下拉框查询广告计划列表
     *
     * @param planGroupId
     * @param adGroupId
     * @param mediumId
     * @return
     */
    public List<PlanGroupSelectVO> queryAdPlanGroupSelect(AdGroupQueryVo vo) {
        return apiTxCampaignMapper.queryForSelect(vo);
    }

    /**
     * 检查推广计划名称是否存在，同一账户下不允许重名
     * campaign_name 推广计划名称
     */
    public void checkAdPlanGroupName(Integer mediumAccountId, Integer account_id, String accessToken, String campaign_name, Integer planGroupId) {
        if (mediumAccountId != null) {

            MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
            account_id = tAccountVo.getAccountId();
            accessToken = tAccountVo.getAccessToken();
        }
        List<PlanGroupVO> result = adCampaignService.query(account_id, null, accessToken, campaign_name);
        if (result.size() > 0) {
            if (planGroupId == null || (planGroupId != null && result.get(0).getPlanGroupId().intValue() == planGroupId)) {
                ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_NAME_EXISTS);
            }
        }
    }

    /**
     * 检查推广计划名称是否存在，同一账户下不允许重名
     *
     * @param planGroupId   推广计划ID
     * @param planGroupName 推广计划名称
     */
    public boolean checkAdPlanGroupName(Integer mediumAccountId, String planGroupName, Integer planGroupId) {
        // 名称长度为120字节，在UTF编码下，最多为40字符
        final int maxLength = 40;
        Integer existRecord = apiTxCampaignMapper.getRecordByPlanGroupName(mediumAccountId, planGroupName, planGroupId);
        if (planGroupName.length() <= maxLength && existRecord == null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据计划id和媒体帐号id查询推广计划
     *
     * @param companyId
     * @param planGroupId
     * @param mediumAccountId
     * @return
     */
    public PlanGroupSelectVO getPlanGroup(Integer companyId, Integer planGroupId, Integer mediumAccountId) {
        PlanGroupCreatePreVO vo = new PlanGroupCreatePreVO();
        vo.setCompanyId(companyId);
        vo.setMediumAccountId(mediumAccountId);
        vo.setPlanGroupId(planGroupId);
        return apiTxCampaignMapper.getPlanGroup(vo);
    }
}
