package com.dataeye.ad.assistor.module.report.model;

/**
 * Created by huangzehai on 2017/3/17.
 */
public enum ReportType {
    Plan, Medium, MediumDetail, systemAccount
}
