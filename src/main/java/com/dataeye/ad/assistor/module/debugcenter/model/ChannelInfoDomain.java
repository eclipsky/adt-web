package com.dataeye.ad.assistor.module.debugcenter.model;

import java.util.List;
import java.util.Map;

/**
 * @author lingliqi
 * @date 2018-01-18 11:14
 */
public class ChannelInfoDomain {
    private String channelId;
    private String showName;
    private Integer type;    // 0：系统定义的渠道，1：用户自定义渠道
    private String cbUrl;
    private List<TemplateParams> templateParams;
    private List<ClickParams> clickParams;
    private String createTime;
    private String updateTime;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCbUrl() {
        return cbUrl;
    }

    public void setCbUrl(String cbUrl) {
        this.cbUrl = cbUrl;
    }

    public List<TemplateParams> getTemplateParams() {
        return templateParams;
    }

    public void setTemplateParams(List<TemplateParams> templateParams) {
        this.templateParams = templateParams;
    }

    public List<ClickParams> getClickParams() {
        return clickParams;
    }

    public void setClickParams(List<ClickParams> clickParams) {
        this.clickParams = clickParams;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public static class ClickParams {
        private int platform;
        private String uriParam;
        private int clickSource;

        public int getPlatform() {
            return platform;
        }

        public void setPlatform(int platform) {
            this.platform = platform;
        }

        public String getUriParam() {
            return uriParam;
        }

        public void setUriParam(String uriParam) {
            this.uriParam = uriParam;
        }

        public int getClickSource() {
            return clickSource;
        }

        public void setClickSource(int clickSource) {
            this.clickSource = clickSource;
        }
    }


    public static class TemplateParams {
        private String label;
        private String key;
        private String type;
        private String defaultValue;
        private boolean isRequired;
        private boolean isWritable;
        private List<Map<String, String>> options;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public boolean isRequired() {
            return isRequired;
        }

        public void setRequired(boolean required) {
            isRequired = required;
        }

        public boolean isWritable() {
            return isWritable;
        }

        public void setWritable(boolean writable) {
            isWritable = writable;
        }

        public List<Map<String, String>> getOptions() {
            return options;
        }

        public void setOptions(List<Map<String, String>> options) {
            this.options = options;
        }
    }
}
