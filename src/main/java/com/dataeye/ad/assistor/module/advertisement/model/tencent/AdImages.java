package com.dataeye.ad.assistor.module.advertisement.model.tencent;

import com.google.gson.annotations.Expose;

/**
 * 图片
 * */
public class AdImages {

	/**图片 id*/
	@Expose
	private String image_id;
	/**图片宽度，单位 px*/
	@Expose
	private Integer width;
	/**图片高度，单位 px*/
	@Expose
	private String height;
	/**图片大小 单位 B(byte)*/
	@Expose
	private String file_size;
	/**图片类型*/
	@Expose
	private String type;
	/**图片文件签名，使用图片文件的 md5 值，用于检查上传图片文件的完整性*/
	private String signature;
	/**预览地址*/
	@Expose
	private String preview_url;
	public String getImage_id() {
		return image_id;
	}
	public void setImage_id(String image_id) {
		this.image_id = image_id;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getFile_size() {
		return file_size;
	}
	public void setFile_size(String file_size) {
		this.file_size = file_size;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getPreview_url() {
		return preview_url;
	}
	public void setPreview_url(String preview_url) {
		this.preview_url = preview_url;
	}
	@Override
	public String toString() {
		return "AdImages [image_id=" + image_id + ", width=" + width
				+ ", height=" + height + ", file_size=" + file_size + ", type="
				+ type + ", signature=" + signature + ", preview_url="
				+ preview_url + "]";
	}
	
}
