package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.BidStrategy;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Bid;
import com.dataeye.ad.assistor.util.CookieUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by huangzehai on 2017/5/18.
 */
@Service("baiduBidService")
public class BaiduBidService extends AbstractMediumPlanService<Bid> implements BidService {
    private String URL = "http://feedads.baidu.com/nirvana/request.ajax?path=sirius/MOD/unit";
    private static final String USER_ID_KEY = "SAMPLING_USER_ID";
    private static final String USER_ID_KEY_2 = "__cas__id__3";
    private static final String TOKEN = "__cas__st__3";
    private static final int CPC = 1;
    private static final int OCPC = 3;

    /**
     * OCPC第一阶段
     */
    private static final int PHASE_1 = 0;
    /**
     * OCPC第二阶段
     */
    private static final int PHASE_2 = 1;
    
    /**
     * 成功消息。
     */
    private static final String SUCCESS = "修改出价成功";
    /**
     * 失败消息.
     */
    private static final String FAIL = "修改出价失败";

    @Override
    public Result bid(Bid bid) {
        return execute(bid);
    }

    @Override
    protected String url(String cookie, Long mediumPlanId) {
        return URL;
    }

    @Override
    protected List<NameValuePair> parameters(String cookie, Bid bid) {
        String userId = CookieUtils.get(cookie, USER_ID_KEY);
        if(StringUtils.isEmpty(userId)){
        	userId = CookieUtils.get(cookie, USER_ID_KEY_2);
        }
        String token = CookieUtils.get(cookie, TOKEN);
        List<NameValuePair> params = new ArrayList<>();

        List<Long> mediumPlanIds = new ArrayList<>();
        mediumPlanIds.add(bid.getMediumPlanId());

        params.add(new BasicNameValuePair("userid", userId));
        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("reqid", UUID.randomUUID().toString()));
        params.add(new BasicNameValuePair("path", "sirius/MOD/unit"));
        //计划ID
        params.add(new BasicNameValuePair("eventId", UUID.randomUUID().toString()));
        //操作
        Params param = new Params();
        Items items = new Items();
        //items.setFtype(1);
        if (StringUtils.equalsIgnoreCase(bid.getBidStrategy(), BidStrategy.CPC)) {
            items.setBidtype(CPC);
            items.setBid(bid.getBid().setScale(2, RoundingMode.HALF_UP));
        }else if (StringUtils.equalsIgnoreCase(bid.getBidStrategy(), BidStrategy.OCPC)) {
        	items.setBidtype(OCPC);
        	//需要传递ocpc相关参数内容
        	OcpcItems ocpc=StringUtil.gson.fromJson(bid.getBidExtend(), new TypeToken<OcpcItems>(){}.getType());
        	if(bid.getIsCpaBid() != null && bid.getIsCpaBid() == PHASE_1){
        		items.setBid(bid.getBid().setScale(2, RoundingMode.HALF_UP));
        	}else if (bid.getIsCpaBid() != null && bid.getIsCpaBid() == PHASE_2) {
        		ocpc.setOcpcBid(bid.getBid().setScale(2, RoundingMode.HALF_UP));
			}
        	items.setOcpc(ocpc);
		}
//
        param.setItems(items);
        param.setUnitid(Arrays.asList(bid.getMediumPlanId()));
        Gson gson = new Gson();
        params.add(new BasicNameValuePair("params", gson.toJson(param)));
        return params;
    }

    private class Params {
        private Items items;

        private List<Long> unitid;

        public Items getItems() {
            return items;
        }

        public void setItems(Items items) {
            this.items = items;
        }

        public List<Long> getUnitid() {
            return unitid;
        }

        public void setUnitid(List<Long> unitid) {
            this.unitid = unitid;
        }

        @Override
        public String toString() {
            return "Params{" +
                    "items=" + items +
                    ", unitid=" + unitid +
                    '}';
        }
    }

    private class Items {
        //private int ftype;
        /**
         * 出价类型
         */
        private int bidtype;
        /**
         * 出价金额,单位元.
         */
        private BigDecimal bid;
        
        /**ocpc*/
        private OcpcItems ocpc;

 /*       public int getFtype() {
            return ftype;
        }

        public void setFtype(int ftype) {
            this.ftype = ftype;
        }*/

        public int getBidtype() {
            return bidtype;
        }

        public void setBidtype(int bidtype) {
            this.bidtype = bidtype;
        }

        public BigDecimal getBid() {
            return bid;
        }

        public void setBid(BigDecimal bid) {
            this.bid = bid;
        }

        public OcpcItems getOcpc() {
			return ocpc;
		}

		public void setOcpc(OcpcItems ocpc) {
			this.ocpc = ocpc;
		}

		@Override
		public String toString() {
			return "Items [bidtype=" + bidtype + ", bid="
					+ bid + ", ocpc=" + ocpc + "]";
		}
    }

    @Override
    protected void requestHeader(HttpRequestBase request, String cookie) {
        String userId = CookieUtils.get(cookie, USER_ID_KEY);
        request.setHeader(HOST, "feedads.baidu.com");
        request.setHeader(ORIGIN, "http://feedads.baidu.com");
        request.setHeader(REFERER, String.format("http://feedads.baidu.com/nirvana/main.html?userid=%s", userId));
    }

    @Override
    protected Result response(String content) {
        Gson gson = new Gson();
        Response response = gson.fromJson(content, Response.class);
        Result result = new Result();
        //Code为0是操作成功.
        if (response.getStatus() == 200 && response.getError() == null) {
            result.setSuccess(true);
            result.setMessage(SUCCESS);
        } else {
            result.setSuccess(false);
            result.setMessage(FAIL);
        }
        return result;
    }

    private class Response {
        private int status;
        private Object data;
        private Object error;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }

        public Object getError() {
            return error;
        }

        public void setError(Object error) {
            this.error = error;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "status=" + status +
                    ", data=" + data +
                    ", error=" + error +
                    '}';
        }
    }
    
    private class OcpcItems {
        private BigDecimal ocpcBid;
        private int ocpcLevel;
        private int transType;
        private int transFrom;
        private String lpUrl;
        private String monitorUrl;
        private String naUrl;
        private int appTransId;
        private String passTime;
        private int tjSiteid;
        private int tjTransid;
		public BigDecimal getOcpcBid() {
			return ocpcBid;
		}
		public void setOcpcBid(BigDecimal ocpcBid) {
			this.ocpcBid = ocpcBid;
		}
		public int getOcpcLevel() {
			return ocpcLevel;
		}
		public void setOcpcLevel(int ocpcLevel) {
			this.ocpcLevel = ocpcLevel;
		}
		public int getTransType() {
			return transType;
		}
		public void setTransType(int transType) {
			this.transType = transType;
		}
		public int getTransFrom() {
			return transFrom;
		}
		public void setTransFrom(int transFrom) {
			this.transFrom = transFrom;
		}
		public String getLpUrl() {
			return lpUrl;
		}
		public void setLpUrl(String lpUrl) {
			this.lpUrl = lpUrl;
		}
		public String getMonitorUrl() {
			return monitorUrl;
		}
		public void setMonitorUrl(String monitorUrl) {
			this.monitorUrl = monitorUrl;
		}
		public String getNaUrl() {
			return naUrl;
		}
		public void setNaUrl(String naUrl) {
			this.naUrl = naUrl;
		}
		public int getAppTransId() {
			return appTransId;
		}
		public void setAppTransId(int appTransId) {
			this.appTransId = appTransId;
		}
		public String getPassTime() {
			return passTime;
		}
		public void setPassTime(String passTime) {
			this.passTime = passTime;
		}
		public int getTjSiteid() {
			return tjSiteid;
		}
		public void setTjSiteid(int tjSiteid) {
			this.tjSiteid = tjSiteid;
		}
		public int getTjTransid() {
			return tjTransid;
		}
		public void setTjTransid(int tjTransid) {
			this.tjTransid = tjTransid;
		}
		@Override
		public String toString() {
			return "OcpcItems [ocpcBid=" + ocpcBid + ", ocpcLevel=" + ocpcLevel
					+ ", transType=" + transType + ", transFrom=" + transFrom
					+ ", lpUrl=" + lpUrl + ", monitorUrl=" + monitorUrl
					+ ", naUrl=" + naUrl + ", appTransId=" + appTransId
					+ ", passTime=" + passTime + ", tjSiteid=" + tjSiteid
					+ ", tjTransid=" + tjTransid + "]";
		}
        
    }
}
