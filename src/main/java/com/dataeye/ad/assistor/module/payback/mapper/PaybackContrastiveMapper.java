package com.dataeye.ad.assistor.module.payback.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.common.DailyRecord;
import com.dataeye.ad.assistor.module.payback.model.PaybackQuery;

/**
 * 回本对比
 * 
 * */
@MapperScan
public interface PaybackContrastiveMapper {
	
	/**
	 * 回本对比分析--推广计划（按天）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackDailyQueryByPlan(PaybackQuery query);
	
	/**
	 * 回本对比分析--推广计划（按周）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackWeekQueryByPlan(PaybackQuery query);
	
	/**
	 * 回本对比分析--推广计划（按月）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackMonthlyQueryByPlan(PaybackQuery query);
	
	/**
	 * 回本对比分析--媒体帐号（按天）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackDailyQueryByMediumAccount(PaybackQuery query);
	
	/**
	 * 回本对比分析--媒体帐号（按周）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackWeekQueryByMediumAccount(PaybackQuery query);
	
	/**
	 * 回本对比分析--媒体帐号（按月）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackMonthlyQueryByMediumAccount(PaybackQuery query);
	
	/**
	 * 回本对比分析--媒体（按天）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackDailyQueryByMedium(PaybackQuery query);
	
	/**
	 * 回本对比分析--媒体（按周）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackWeekQueryByMedium(PaybackQuery query);
	
	/**
	 * 回本对比分析--媒体（按月）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackMonthlyQueryByMedium(PaybackQuery query);
	
	/**
	 * 回本对比分析--产品（按天）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackDailyQueryByProduct(PaybackQuery query);
	/**
	 * 回本对比分析--产品（按周）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackWeekQueryByProduct(PaybackQuery query);
	
	/**
	 * 回本对比分析--产品（按月）
	 * */
	public List<DailyRecord<String, BigDecimal>> payBackMonthlyQueryByProduct(PaybackQuery query);
}
