package com.dataeye.ad.assistor.module.advertisement.model;



import com.google.gson.annotations.Expose;


/**
 * 广告组
 * Created by ldj
 */
public class AdvertisementGroupVO {

	/**推广计划**/
	@Expose
    private PlanGroupSelectVO planGroup;
	/**广告组**/
	@Expose
    private AdvertisementGroup adGroup;

	@Expose
	private Integer productId;
	/**产品名称*/
	@Expose
	private String productName;
	@Expose
	private Integer mediumAccountId;
	/**媒体帐号*/
	@Expose
	private String mediumAccountName;
	@Expose
	private Integer mediumId;
	@Expose
	private String mediumName;
	
	
	public PlanGroupSelectVO getPlanGroup() {
		return planGroup;
	}
	public void setPlanGroup(PlanGroupSelectVO planGroup) {
		this.planGroup = planGroup;
	}
	public AdvertisementGroup getAdGroup() {
		return adGroup;
	}
	public void setAdGroup(AdvertisementGroup adGroup) {
		this.adGroup = adGroup;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public String getMediumAccountName() {
		return mediumAccountName;
	}
	public void setMediumAccountName(String mediumAccountName) {
		this.mediumAccountName = mediumAccountName;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	@Override
	public String toString() {
		return "AdvertisementGroupVO [planGroup=" + planGroup + ", adGroup="
				+ adGroup + ", productId=" + productId + ", productName="
				+ productName + ", mediumAccountId=" + mediumAccountId
				+ ", mediumAccountName=" + mediumAccountName + ", mediumId="
				+ mediumId + ", mediumName=" + mediumName + "]";
	}
	
	
}
