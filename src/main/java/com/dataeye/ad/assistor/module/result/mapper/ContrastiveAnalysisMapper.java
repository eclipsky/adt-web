package com.dataeye.ad.assistor.module.result.mapper;

import com.dataeye.ad.assistor.common.Item;
import com.dataeye.ad.assistor.module.product.model.Product;
import com.dataeye.ad.assistor.module.result.model.ContrastiveAnalysisQuery;
import com.dataeye.ad.assistor.module.result.model.DailyIndicator;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccount;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 对比分析Mapper.
 * Created by huangzehai on 2017/4/10.
 */
@MapperScan
public interface ContrastiveAnalysisMapper {

    /**
     * 按优化师分组统计日消耗趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    List<DailyIndicator> dailyCostTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);

    /**
     * 按优化师分组统计日CTR趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    List<DailyIndicator> dailyCtrTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);


    /**
     * 按优化师分组统计日下载率趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    List<DailyIndicator> dailyDownloadRateTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);


    /**
     * 按优化师分组统计日CPA趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    List<DailyIndicator> dailyCpaTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);

    /**
     * 按优化师分组统计七日回本率趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    List<DailyIndicator> dailySevenDayPaybackRateTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);

    /**
     * 获取关联表中的优化师列表.
     * @param dataPermissionDomain
     * @return
     */
    List<SystemAccount> listOptimizers(DataPermissionDomain dataPermissionDomain);

    /**
     *  获取关联表中的产品列表.
     * @param companyId
     * @return
     */
    List<Product> listProducts(@Param("companyId") int companyId);
    
    /**
     * 按优化师分组统计七日留存率趋势.
     *
     * @param contrastiveAnalysisQuery
     * @return
     */
    List<DailyIndicator> dailySevenDayRetentionRateTrendGroupByOptimizer(ContrastiveAnalysisQuery contrastiveAnalysisQuery);
}
