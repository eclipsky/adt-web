package com.dataeye.ad.assistor.module.result.model;

import com.google.gson.annotations.Expose;

/**
 * Created by huangzehai on 2017/5/26.
 */
public class IndicatorWithId extends Indicator {
    @Expose
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "IndicatorWithId{" +
                "id=" + id +
                "} " + super.toString();
    }
}
