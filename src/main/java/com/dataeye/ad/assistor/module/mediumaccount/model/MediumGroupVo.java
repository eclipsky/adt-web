package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * 媒体分组及媒体列表
 * @author luzhuyou 2017/08/10
 *
 */
public class MediumGroupVo<T> {
	/**
	 * 媒体类型：0-ADT对接媒体，1-Tracking通用媒体，2-自定义媒体
	 */
	@Expose
	private int mediumType; 
	
	@Expose
	private List<T> mediumList;

	public int getMediumType() {
		return mediumType;
	}

	public void setMediumType(int mediumType) {
		this.mediumType = mediumType;
	}

	public List<T> getMediumList() {
		return mediumList;
	}

	public void setMediumList(List<T> mediumList) {
		this.mediumList = mediumList;
	}

	@Override
	public String toString() {
		return "MediumGroupVo [mediumType=" + mediumType + ", mediumList="
				+ mediumList + "]";
	}
	
}
