package com.dataeye.ad.assistor.module.report.model;

import org.apache.commons.lang.StringUtils;

/**
 * 表格的链接.
 * Created by huangzehai on 2017/1/4.
 */
public enum LinkID {
    Date(2), Medium(2), Account(3), DateMedium(3), DateAccount(4), Plan(2), DailyMediumTotal(2), DailyMedium(3), DailyAccount(4);

    private int tier;

    public int getTier() {
        return tier;
    }

    public void setTier(int tier) {
        this.tier = tier;
    }

    LinkID(int tier) {
        this.tier = tier;
    }

    public static LinkID parse(String text) {
        for (LinkID link : LinkID.values()) {
            if (StringUtils.equalsIgnoreCase(link.name(), text)) {
                return link;
            }
        }
        return null;
    }
}
