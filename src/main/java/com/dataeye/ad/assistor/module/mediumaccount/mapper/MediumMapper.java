package com.dataeye.ad.assistor.module.mediumaccount.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.mediumaccount.model.Channel;
import com.dataeye.ad.assistor.module.mediumaccount.model.Medium;

/**
 * 媒体映射器.
 * Created by luzhuyou
 */
@MapperScan
public interface MediumMapper {

    /**
     * 查询媒体列表信息
     * @return
     * @author luzhuyou 2017/02/22
     * @update ldj 2017-08-14
     */
	public List<Medium> queryAll(Medium vo);
	
	/**
	 * 获取Tracking媒体ID
	 * @return
	 * @update ldj 2017-08-10 
	 */
	public String getTrackingMediumId(Channel mapping);
	
	/**
     * 根据产品id,查询媒体列表信息
     * @return
     * @author ldj 2017-07-07
     */
	public List<Medium> queryByProductId(Medium vo);

	/**
     * 查询ADT已对接媒体列表信息(id范围：1-999)
     * @return
     * @author luzhuyou 2017/08/10
     */
	public List<Medium> queryAdtMedium();

	/**
     * 查询Tracking通用媒体列表信息（不含ADT已有部分）（id范围：1000-9999）
     * @return
     * @author luzhuyou 2017/08/10
     */
	public List<Medium> queryTrackingCommonMedium();

	/**
     * 查询自定义媒体列表信息（id范围：100000-）
     * @return
     * @author luzhuyou 2017/08/10
     */
	public List<Medium> queryCustomMedium(int companyId);

	/**
	 * 根据媒体名称查询媒体是否存在
	 * @param medium
	 * @return
	 * @author luzhuyou 2017/08/10
	 */
	public Medium get(Medium medium);

	/**
	 * 新增自定义媒体
	 * @param medium
	 * @return
	 * @author luzhuyou 2017/08/10
	 */
	public int addCustomMedium(Medium medium);
	
	
	/**
	 * 得到ADT媒体对应的渠道信息
	 * @return
	 * @author ldj 2017-08-11
	 */
	public List<Channel> getAdtMediumMapping();
	
	
}
