package com.dataeye.ad.assistor.module.company.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.company.model.TrackingCompany;

/**
 * 公司信息映射器.
 * Created by luzhuyou
 */
@MapperScan
public interface TrackingCompanyMapper {
	
	/**
	 * 新增Tracking产品公司信息
	 * @param company
	 * @return
	 */
	public int add(TrackingCompany company);

	/**
	 * 根据Tracking产品公司ID获取Tracking产品公司信息
	 * @param companyId
	 * @return
	 */
	public TrackingCompany get(int companyId);

	/**
	 * 根据邮箱号获取Tracking产品公司信息
	 * @param email
	 * @return
	 */
	public TrackingCompany getByEmail(String email);
	
	/**
	 * 根据UID获取Tracking产品公司信息
	 * @param uid
	 * @return
	 */
	public TrackingCompany getByUid(int uid);
}
