package com.dataeye.ad.assistor.module.campaign.model;

import java.util.Date;
import java.util.List;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

/**
 * 查询推广计划参数VO
 * @author luzhuyou 2017/06/19
 *
 */
public class CampaignQueryReqVo extends DataPermissionDomain {
	
	/** uid */
	private int uid;
	/** TrackingAppID，如果多个，逗号分隔 */
	private String appIds;
	/** Tracking媒体ID，如果多个，逗号分隔*/
	private String trackingMediumIds;
	/** 活动ID（短链活动号），如果多个，逗号分隔*/
	private String trackingCampaignIds;
	/** ADT账号ID，如果多个，逗号分隔 */
	private String adtAccountIds;
	/** 渠道ID，如果多个，逗号分隔 */
	private String channelIds;
	/**
     * 起始日期.
     */
    private Date startDate;
    /**
     * 截止日期.
     */
    private Date endDate;
    /**
     * 媒体ID列表.
     */
    private List<Integer> mediumIds;
    /**
     * 媒体账号ID列表.
     */
    private List<Integer> mediumAccountIds;

    /**
     * 产品ID列表.
     */
    private List<Integer> productIds;

    /**
     * 计划ID列表
     */
    private List<String> campaignIds;

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getAppIds() {
		return appIds;
	}

	public void setAppIds(String appIds) {
		this.appIds = appIds;
	}

	public String getTrackingMediumIds() {
		return trackingMediumIds;
	}

	public void setTrackingMediumIds(String trackingMediumIds) {
		this.trackingMediumIds = trackingMediumIds;
	}

	public String getTrackingCampaignIds() {
		return trackingCampaignIds;
	}

	public void setTrackingCampaignIds(String trackingCampaignIds) {
		this.trackingCampaignIds = trackingCampaignIds;
	}

	public String getAdtAccountIds() {
		return adtAccountIds;
	}

	public void setAdtAccountIds(String adtAccountIds) {
		this.adtAccountIds = adtAccountIds;
	}

	public String getChannelIds() {
		return channelIds;
	}

	public void setChannelIds(String channelIds) {
		this.channelIds = channelIds;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<Integer> getMediumIds() {
		return mediumIds;
	}

	public void setMediumIds(List<Integer> mediumIds) {
		this.mediumIds = mediumIds;
	}

	public List<Integer> getMediumAccountIds() {
		return mediumAccountIds;
	}

	public void setMediumAccountIds(List<Integer> mediumAccountIds) {
		this.mediumAccountIds = mediumAccountIds;
	}

	public List<Integer> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<Integer> productIds) {
		this.productIds = productIds;
	}

	public List<String> getCampaignIds() {
		return campaignIds;
	}

	public void setCampaignIds(List<String> campaignIds) {
		this.campaignIds = campaignIds;
	}

	@Override
	public String toString() {
		return "CampaignQueryReqVo [uid=" + uid + ", appIds=" + appIds
				+ ", trackingMediumIds=" + trackingMediumIds
				+ ", trackingCampaignIds=" + trackingCampaignIds
				+ ", adtAccountIds=" + adtAccountIds + ", channelIds="
				+ channelIds + ", startDate=" + startDate + ", endDate="
				+ endDate + ", mediumIds=" + mediumIds + ", mediumAccountIds="
				+ mediumAccountIds + ", productIds=" + productIds
				+ ", campaignIds=" + campaignIds + "]";
	}

}
