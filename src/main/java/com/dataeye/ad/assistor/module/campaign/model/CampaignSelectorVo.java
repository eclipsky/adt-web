package com.dataeye.ad.assistor.module.campaign.model;

import com.google.gson.annotations.Expose;

/**
 * 推广计划（广告活动）VO
 * @author luzhuyou 2017/06/21
 *
 */
public class CampaignSelectorVo {

	/** 活动ID（短链活动号）*/
	@Expose
	private String campaignId;
	/** 活动名称 */
	@Expose
	private String campaignName;
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	@Override
	public String toString() {
		return "CampaignSelectorVo [campaignId=" + campaignId
				+ ", campaignName=" + campaignName + "]";
	}
}
