package com.dataeye.ad.assistor.module.product.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.dictionaries.mapper.ProductCategoryMapper;
import com.dataeye.ad.assistor.module.dictionaries.model.ProductCategory;
import com.dataeye.ad.assistor.module.mediumaccount.mapper.MediumAccountRelationMapper;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountVo;
import com.dataeye.ad.assistor.module.mediumaccount.service.MediumAccountService;
import com.dataeye.ad.assistor.module.product.constants.Constants.TrackingInterface;
import com.dataeye.ad.assistor.module.product.mapper.ProductMapper;
import com.dataeye.ad.assistor.module.product.model.CpShareRate;
import com.dataeye.ad.assistor.module.product.model.NatureChannelVo;
import com.dataeye.ad.assistor.module.product.model.Product;
import com.dataeye.ad.assistor.module.product.model.ProductSelectorVo;
import com.dataeye.ad.assistor.module.product.model.ProductVo;
import com.dataeye.ad.assistor.module.product.model.RealTimeDataResponseVo;
import com.dataeye.ad.assistor.module.product.model.RealTimeDataVo;
import com.dataeye.ad.assistor.module.systemaccount.model.PtLoginResponse;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

/**
 * 产品Service
 * @author luzhuyou 2017/02/16
 *
 */
@Service("productService")
public class ProductService {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(ProductService.class);

	@Autowired
	private ProductMapper productMapper;
	@Autowired
	private CpShareRateService cpShareRateService;
	@Autowired
	private ProductCategoryMapper productCategoryMapper;
	@Autowired
    private MediumAccountRelationMapper mediumAccountRelationMapper;
	@Autowired
	private MediumAccountService mediumAccountService;
	
	
	/**
	 * 查询产品列表
	 * @param companyId
	 * @return
	 */
	public List<ProductVo> query(Integer companyId) {
		Product product = new Product();
		product.setCompanyId(companyId);
		return productMapper.query(product);
	}
	
	/**
	 * 从ApplicationContext中查询产品列表
	 * @param companyId
	 * @return
	 */
	public List<ProductVo> queryFromCache(Integer companyId) {
        return ApplicationContextContainer.getProductList(companyId);
	}
	
	/**
	 * 从ApplicationContext中查询产品列表
	 * @param companyId
	 * @param productIds
	 * @return
	 */
	public List<ProductVo> queryFromCache(Integer companyId, Set<Integer> productIds) {
        return ApplicationContextContainer.getProductList(companyId, productIds);
	}

	/**
	 * 根据产品名称获取产品信息
	 * @param productName
	 * @param companyId
	 * @return
	 */
	public ProductVo get(String productName, int companyId) {
		return get(productName, companyId, null,null);
	}
	
	/**
	 * 根据产品名称获取产品信息
	 * @param productName
	 * @param companyId
	 * @param appId
	 * @return
	 * @author luzhuyou 2017/05/05
	 */
	public ProductVo get(String productName, int companyId, String appId,Integer productId) {
		Product product = new Product();
		product.setCompanyId(companyId);
		product.setProductName(productName);
		product.setAppId(appId);
		product.setProductId(productId);
		return productMapper.get(product);
	}

	/**
	 * 新增产品信息
	 * @param productName
	 * @param shareRate
	 * @param status
	 * @param companyId
	 * @param appId
	 * @param osType
	 * @param parentCategoryId
	 * @param childrenCategoryId
	 * @param mediumAccountIds 多个用逗号拼接
	 * @return
	 */
	public int add(String productName, double shareRate, int status,
			int companyId, String appId,int osType,int parentCategoryId,int childrenCategoryId,String mediumAccountIds) {
		// 新增产品信息
		Product product = new Product();
		product.setCompanyId(companyId);
		product.setProductName(productName);
		product.setStatus(status);
		product.setCreateTime(new Date());
		product.setUpdateTime(new Date());
		product.setAppId(appId);
		product.setOsType(osType);
		product.setParentCategoryId(parentCategoryId);
		product.setChildrenCategoryId(childrenCategoryId);
		productMapper.add(product);
		
		// 新增CP分成率
		cpShareRateService.add(companyId, product.getProductId(), shareRate);
		
		String parentCategoryName = "";
		if(parentCategoryId != 0){
			ProductCategory category = productCategoryMapper.get(parentCategoryId);
			parentCategoryName = category.getCategoryName();
		}
		String childrenCategoryName = "";
		if(childrenCategoryId != 0){
			ProductCategory category = productCategoryMapper.get(childrenCategoryId);
			childrenCategoryName = category.getCategoryName();
		}
		//增加媒体帐号关联关系
		 if (null != mediumAccountIds) {
			String[] array = mediumAccountIds.split(",");
			for(int i=0; i<array.length; i++) {
				Integer mediumAccountId = Integer.parseInt(array[i]);
				//得到该媒体帐号的信息
				MediumAccountVo mediuAccount = mediumAccountService.getMediumAccount(null, mediumAccountId);
				if(mediuAccount != null && mediuAccount.getMediumId() != null){
					//添加媒体帐号关联关系
					mediumAccountService.addMediumAccountRelation(companyId, mediumAccountId, mediuAccount.getMediumId(), product.getProductId());
				}
			}
		}
		
		
		// 新增产品信息到ApplicationContext中
        ApplicationContextContainer.addProduct(companyId, product.getProductId(), productName.trim(), shareRate,status,appId,osType,parentCategoryId,childrenCategoryId, parentCategoryName, childrenCategoryName);
		return product.getProductId();
	}

	/**
	 * 修改产品信息
	 * @param productId
	 * @param productName
	 * @param shareRate
	 * @param status
	 * @param companyId
	 * @param parentCategoryId
	 * @param childrenCategoryId
	 * @param mediumAccountIds 如果多个，用逗号拼接
	 * @return
	 */
	public int modify(int productId, String productName, double shareRate,
			int status, int companyId,int parentCategoryId,int childrenCategoryId,String mediumAccountIds) {
		// 修改产品信息
		Product product = new Product();
		product.setProductId(productId);
		product.setCompanyId(companyId);
		product.setProductName(productName);
		product.setStatus(status);
		product.setUpdateTime(new Date());
		product.setParentCategoryId(parentCategoryId);
		product.setChildrenCategoryId(childrenCategoryId);
		productMapper.update(product);
		
		// 修改CP分成率
		CpShareRate cpShareRate = new CpShareRate();
		cpShareRate.setProductId(productId);
		cpShareRate.setShareRate(shareRate);
		cpShareRate.setUpdateTime(new Date());
		int result = cpShareRateService.modify(productId, shareRate);
		String parentCategoryName = "";
		if(parentCategoryId != 0){
			ProductCategory category = productCategoryMapper.get(parentCategoryId);
			parentCategoryName = category.getCategoryName();
		}
		String childrenCategoryName = "";
		if(childrenCategoryId != 0){
			ProductCategory category = productCategoryMapper.get(childrenCategoryId);
			childrenCategoryName = category.getCategoryName();
		}
		
		// 修改媒体账号关联关系(先将旧的关系全部删除，再添加新的关系)
		mediumAccountService.deleteMediumAccountRelation(companyId, null, null, productId);
		
		 if (null != mediumAccountIds) {
			String[] array = mediumAccountIds.split(",");
			for(int i=0; i<array.length; i++) {
				Integer mediumAccountId = Integer.parseInt(array[i]);
				//得到该媒体帐号的信息
				MediumAccountVo mediuAccount = mediumAccountService.getMediumAccount(null, mediumAccountId);
				if(mediuAccount != null && mediuAccount.getMediumId() != null){
					//添加媒体帐号关联关系
					mediumAccountService.addMediumAccountRelation(companyId, mediumAccountId, mediuAccount.getMediumId(), product.getProductId());
				}
			}
		}
		// 更新ApplicationContext中的产品信息
        ApplicationContextContainer.modifyProduct(companyId, productId, productName.trim(), shareRate, status,parentCategoryId,childrenCategoryId, parentCategoryName, childrenCategoryName);
        
        return result;
	}

	/**
	 * 下拉框查询产品
	 * @param companyId
	 * @param permissionMediumAccountIds
	 * @param permissionSystemAccounts
	 * @return
	 * @updated luzhuyou 2017/08/16
	 */
	public List<ProductSelectorVo> queryForSelector(int companyId, Integer[] permissionMediumAccountIds, 
			String[] permissionSystemAccounts) {
		ProductSelectorVo vo = new ProductSelectorVo();
		vo.setCompanyId(companyId);
		vo.setPermissionMediumAccountIds(permissionMediumAccountIds);
		vo.setPermissionSystemAccounts(permissionSystemAccounts);
		return productMapper.queryForSelector(vo);
	}
	
	
	/**
	 * 修改trackingApp
	 * @param productId
	 * @param appId
	 * @param appName
	 * @param companyId
	 * @return
	 * @author luzhuyou 2017/05/05
	 */
	public int modifyRefApp(int productId, String appId, String appName, int companyId) {
		
		if(StringUtils.isNotBlank(appId)) {
			ProductVo vo = this.get(null, companyId, appId,null);
			if(vo != null && appId.equals(vo.getAppId())) {
				logger.error("TrackingApp已被产品[" + vo.getProductName() + "]关联，请先解绑.");
				ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ALREADY_USED);
			}
		}
		
		// 更新产品信息中的appId
		Product product = new Product();
		product.setCompanyId(companyId);
		product.setProductId(productId);
		product.setAppId(appId);
		product.setUpdateTime(new Date());
		int result = productMapper.updateRefApp(product);
		
		// 更新ApplicationContext中的产品信息
        ApplicationContextContainer.modifyProductRefApp(companyId, productId, appId, appName);

		return result;
	}
	
	
	/**
	 * 获取新建产品的appid
	 * @param uid	创建人id
	 * @param name	App名称
	 * @param type	游戏  ,默认等于2 
	 * @param category	游戏类型
	 * @param platform	系统类型 (tracking系统类型和adt相反)
	 *  (tracking 类型对应  0-Others, 2-iOS，1-Android)
	 *  (ADT osType类型对应 0-Others，1-iOS， 2-Android)
	 * @param currency	货币英文缩写，默认等于CNY
	 * @param gameEngine 游戏引擎，0：None（应用没有引擎），1：Cocos2d-x，2：U3D，3：Flash air，4：自研引擎，5：其他
	 * @return
	 * @author ldj  2017-06-15
	 * @update 2017-11-24 把系统类型转成tracking类型一致
	 * */
	public String getProductRefAppId(String name, Integer category, Integer osType,Integer uid) {
		//1.构建参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("uid", uid+"");
		params.put("name", name);
		params.put("type", "2");
		params.put("category", category+"");
		//tracking系统类型和adt相反 ,update 2017-11-24
		Integer platform = 0;
		if( osType!=null && osType.intValue() == 1){ 
			platform = 2;
		}
		if( osType!=null && osType.intValue() == 2){ 
			platform = 1;
		}
		params.put("platform", platform+"");
		params.put("currency", "CNY");
		params.put("gameEngine", "5");
		//2.HTTP请求获取数据
		String getAppIdInterface = ConfigHandler.getProperty(TrackingInterface.GET_APPID_KEY, TrackingInterface.GET_APPID);
		String httpResponse = HttpRequest.post(getAppIdInterface, params);
		logger.debug("[httpResponse]:"+httpResponse);
		if(StringUtils.isBlank(httpResponse)) {
    		logger.info("获取新建产品的AppId接口请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
		// 3.结果数据封装，转换为Map
		JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("获取新建产品的AppId接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		if( statusCode == 406 ){  //产品名称已经存在
    			ExceptionHandler.throwParameterException(StatusCode.PROD_PRODUCT_NAME_EXISTS);
        	}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	String appId = jsonObject.get("content").getAsString();
    	// {"ID": "1491536610555543","statusCode":200,"content":"CDAAA134250F6A3C8E9E53E534EFF82F7"}
    	
        return appId;
	}
	
	/**
	 * 获取实时统计数据 （SDK接入测试）
	 * @param appid	Appid
	 * @param uid
	 * @return
	 * @author ldj  2017-06-15
	 * */
	public List<RealTimeDataVo> getRealTimeInfo(String appid,Integer uid) {
		//1.构建参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("appid", appid);
		
		//获取当前时间
		String now = DateUtils.currentDate();
		params.put("uid", uid+"");
		params.put("startDate", now);
		params.put("endDate", now);
		
		//2.HTTP请求获取数据
		String getAppIdInterface = ConfigHandler.getProperty(TrackingInterface.GET_REALTIME_INFO_KEY, TrackingInterface.GET_REALTIME_INFO);
		String httpResponse = HttpRequest.post(getAppIdInterface, params);
		
		if(StringUtils.isBlank(httpResponse)) {
    		logger.info("SDK接入测试，获取实时统计数据接口请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
		// 3.结果数据封装，转换为Map
		JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("SDK接入测试，获取实时统计数据接口响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	PtLoginResponse<List<RealTimeDataResponseVo>> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<PtLoginResponse<List<RealTimeDataResponseVo>>>(){}.getType());
    	
        if(response == null || response.getContent() == null) {
        	logger.info("SDK接入测试，获取实时统计数据为空");
        	return null;
        }
        List<RealTimeDataVo> result = new ArrayList<RealTimeDataVo>();
        List<RealTimeDataResponseVo> list = response.getContent();
        for (RealTimeDataResponseVo datavo : list) {
        	RealTimeDataVo vo = new RealTimeDataVo();
        	vo.setChannelId(datavo.getChannelId());
        	vo.setChannelName(datavo.getChannelName());
        	vo.setClickA(datavo.getClickA());
        	vo.setClickU(datavo.getClickU());
        	vo.setActiveNum(datavo.getActiveU());
        	vo.setActiveNumRate(datavo.getActiveRate());
        	vo.setRegisterNum(datavo.getRegisterU());
        	vo.setRegisterNumRate(datavo.getRegisterRate());
        	vo.setLoginNum(datavo.getActiveUser());
        	vo.setTotalPayAmount(datavo.getRevenue());
        	vo.setRegisterAccountNum(datavo.getRegisterA());
        	result.add(vo);
		}
        return result;
	}
	
	/**
	 * 查询所有产品的自然流量
	 * create by ldj 2017-07-05
	 * @param uid
	 * @param companyId
	 * 1.先查询出所有的产品。
	 * 2.循环产品，得到每个产品的自然流量
	 * **/
	public List<NatureChannelVo> getNatureRealTimeData(Integer companyId , Integer uid){
		List<NatureChannelVo> result = new ArrayList<NatureChannelVo>();
		List<ProductVo> productList = queryFromCache(companyId);
		for (ProductVo po : productList) {
			
			if(!StringUtils.isBlank(po.getAppId())){
				List<RealTimeDataVo> realTimeDateVoList = getRealTimeInfo(po.getAppId(), uid);
				for (RealTimeDataVo realTimeDataVo : realTimeDateVoList) {
					if(realTimeDataVo.getChannelId().equals("nature")){  //只取出自然流量的数据
						NatureChannelVo natureVo = new NatureChannelVo();
						natureVo.setProductName(po.getProductName());
						natureVo.setActiveNum(Integer.parseInt(realTimeDataVo.getActiveNum()));
						natureVo.setRegisterNum(Integer.parseInt(realTimeDataVo.getRegisterNum()));
						natureVo.setRegisterNumRate(realTimeDataVo.getRegisterNumRate());
						natureVo.setTotalPayAmount(realTimeDataVo.getTotalPayAmount());
						result.add(natureVo);
						break;
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * 查询产品列表
	 * @param companyId
	 * @return
	 * @create ldj 2017-08-25
	 */
	public ProductVo getMediumAccountInfoByProudctId(Integer companyId, Integer productId) {
		Product product = new Product();
		product.setCompanyId(companyId);
		product.setProductId(productId);
		return productMapper.getMediumAccountInfoByProudctId(product);
	}
	
	/**
	 * 根据多个产品ID查询产品列表
	 * @param productIds 产品ID，如果多个则以逗号分隔
	 * @return
	 */
	public List<ProductVo> queryByProductIds(String[] productIds) {
		return productMapper.queryByProductIds(productIds);
	}
	
	/**
	 * 修改app
	 * @param uid	创建人id
	 * @param appId
	 * @param name	App名称
	 * @param type	游戏  ,默认等于2 
	 * @param category	游戏类型
	 * @param platform	系统类型
	 * @param currency	货币英文缩写，默认等于CNY
	 * @param gameEngine 游戏引擎，0：None（应用没有引擎），1：Cocos2d-x，2：U3D，3：Flash air，4：自研引擎，5：其他
	 * @return
	 * @author ldj  2017-09-01
	 * */
	public void modifyApp(String appId,String name, Integer category, Integer osType,Integer uid) {
		//1.构建参数
		Map<String,String> params = new HashMap<String,String>();
		params.put("uid", uid+"");
		params.put("appid", appId);
		params.put("name", name);
		params.put("type", "2");
		params.put("category", category+"");
		//tracking系统类型和adt相反 ,update 2017-11-24
		Integer platform = 0;
		if( osType!=null && osType.intValue() == 1){ 
			platform = 2;
		}
		if( osType!=null && osType.intValue() == 2){ 
			platform = 1;
		}
		params.put("platform", platform+"");
		params.put("currency", "CNY");
		params.put("gameEngine", "5");
		//2.HTTP请求获取数据
		String getAppIdInterface = ConfigHandler.getProperty(TrackingInterface.MODIFY_APP_KEY, TrackingInterface.MODIFY_APP_URL);
		String httpResponse = HttpRequest.post(getAppIdInterface, params);
		logger.debug("[httpResponse]:"+httpResponse);
		if(StringUtils.isBlank(httpResponse)) {
    		logger.info("修改app接口请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
		// 3.结果数据封装，转换为Map
		JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.info("修改app接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		if( statusCode == 406 ){  //产品名称已经存在
    			ExceptionHandler.throwParameterException(StatusCode.PROD_PRODUCT_NAME_EXISTS);
        	}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	//String appId = jsonObject.get("content").getAsString();
    	// {"ID": "1491536610555543","statusCode":200,"content":"CDAAA134250F6A3C8E9E53E534EFF82F7"}
    	
      
	}
	
	
	/**
	 * 根据多个产品ID查询产品名称
	 * @param productIds 产品ID，如果多个则以逗号分隔
	 * @return
	 */
	public String getProductNamesByProductIds(String[] productIds) {
		return productMapper.getProductNamesByProductIds(productIds);
	}
}
