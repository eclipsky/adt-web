package com.dataeye.ad.assistor.module.association.model;

import java.util.Arrays;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.google.gson.annotations.Expose;

/**
 * 关联规则对象VO
 * @author luzhuyou 2017/03/02
 *
 */
public class AssociationRuleVo extends DataPermissionDomain {

	/** 媒体ID */
	@Expose
	private Integer mediumId;
	/** 媒体名称 */
	@Expose
	private String mediumName;
	/** 媒体账户ID */
	@Expose
	private Integer mediumAccountId;
	/** 媒体账号 */
	@Expose
	private String mediumAccount;
	/** 产品ID */
	@Expose
	private Integer productId;
	/** 产品名称 */
	@Expose
	private String productName;
	/** 计划ID */
	@Expose
	private Integer planId;
	/** 计划名称 */
	@Expose
	private String planName;
	/** 包ID */
	@Expose
	private Integer pkgId;
	/** 包名称 */
	@Expose
	private String pkgName;
	/** 页面ID */
	@Expose
	private Integer pageId;
	/** 页面名称 */
	@Expose
	private String pageName;
	/** 系统类型：0-Others, 1-iOS, 2-Android */
	@Expose
	private Integer osType;
	/** 媒体ID列表，仅作为查询参数 */
	private Integer[] mediumIds;
	/** 媒体账号ID列表，仅作为查询参数 */
	private Integer[] mediumAccountIds;
	/**
	 *  优化师ADT账号
	 */
	@Expose
	private String optimizerSystemAccount;
	/**
	 * unboundPlan 0-所有计划的关联规则信息，1-未绑定关联规则的计划
	 */
	private Integer unboundPlan;
	
	/** 统计日期 */
	@Expose
	private String statDate;
	
	/** 产品ID列表，仅作为查询参数 */
	private Integer[] productIds;
	
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public String getMediumAccount() {
		return mediumAccount;
	}
	public void setMediumAccount(String mediumAccount) {
		this.mediumAccount = mediumAccount;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Integer getPkgId() {
		return pkgId;
	}
	public void setPkgId(Integer pkgId) {
		this.pkgId = pkgId;
	}
	public String getPkgName() {
		return pkgName;
	}
	public void setPkgName(String pkgName) {
		this.pkgName = pkgName;
	}
	public Integer getPageId() {
		return pageId;
	}
	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}
	public String getPageName() {
		return pageName;
	}
	/**
	 * 设置pageName时，截取url最后一个反斜杠后的内容
	 * @param pageName
	 */
	public void setPageName(String pageName) {
		if(pageName == null) {
			return;
		}
		int lastSlashIndex = pageName.lastIndexOf('/');
		if (lastSlashIndex > 0 && lastSlashIndex != pageName.length() - 1) {
			this.pageName = pageName.substring(lastSlashIndex + 1);
		} else {
			this.pageName = pageName;
		}
	}
	public Integer[] getMediumIds() {
		return mediumIds;
	}
	public void setMediumIds(Integer[] mediumIds) {
		this.mediumIds = mediumIds;
	}
	public Integer[] getMediumAccountIds() {
		return mediumAccountIds;
	}
	public void setMediumAccountIds(Integer[] mediumAccountIds) {
		this.mediumAccountIds = mediumAccountIds;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}

	public String getOptimizerSystemAccount() {
		return optimizerSystemAccount;
	}

	public void setOptimizerSystemAccount(String optimizerSystemAccount) {
		this.optimizerSystemAccount = optimizerSystemAccount;
	}
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public Integer getUnboundPlan() {
		return unboundPlan;
	}
	public void setUnboundPlan(Integer unboundPlan) {
		this.unboundPlan = unboundPlan;
	}
	public String getStatDate() {
		return statDate;
	}
	public void setStatDate(String statDate) {
		this.statDate = statDate;
	}
	public Integer[] getProductIds() {
		return productIds;
	}
	public void setProductIds(Integer[] productIds) {
		this.productIds = productIds;
	}
	@Override
	public String toString() {
		return "AssociationRuleVo [mediumId=" + mediumId + ", mediumName="
				+ mediumName + ", mediumAccountId=" + mediumAccountId
				+ ", mediumAccount=" + mediumAccount + ", productId="
				+ productId + ", productName=" + productName + ", planId="
				+ planId + ", planName=" + planName + ", pkgId=" + pkgId
				+ ", pkgName=" + pkgName + ", pageId=" + pageId + ", pageName="
				+ pageName + ", osType=" + osType + ", mediumIds="
				+ Arrays.toString(mediumIds) + ", mediumAccountIds="
				+ Arrays.toString(mediumAccountIds)
				+ ", optimizerSystemAccount=" + optimizerSystemAccount
				+ ", unboundPlan=" + unboundPlan + ", statDate=" + statDate
				+ ", productIds=" + Arrays.toString(productIds) + "]";
	}

}
