package com.dataeye.ad.assistor.module.realtimedelivery.model;

/**
 * 媒体计划
 * Created by huangzehai on 2017/5/15.
 */
public class MediumPlan {
    /**
     * 媒体ID
     */
    private Integer mediumId;

    /**
     * 媒体账号ID
     */
    private Integer mediumAccountId;

    /**
     * 媒体计划Id。
     */
    private Long mediumPlanId;

    /**
     * 计划ID
     */
    private Integer planId;

    /**
     * 公司ID，供记录操作日志。
     */
    private Integer companyId;

    /**
     * ADT账号ID,供记录操作日志.
     */
    private Integer accountId;
    
    /**
     * 媒体平台的mediumAccountId
     * */
    private String mediumUserId;

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public Long getMediumPlanId() {
        return mediumPlanId;
    }

    public void setMediumPlanId(Long mediumPlanId) {
        this.mediumPlanId = mediumPlanId;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getMediumUserId() {
		return mediumUserId;
	}

	public void setMediumUserId(String mediumUserId) {
		this.mediumUserId = mediumUserId;
	}

	@Override
	public String toString() {
		return "MediumPlan [mediumId=" + mediumId + ", mediumAccountId="
				+ mediumAccountId + ", mediumPlanId=" + mediumPlanId
				+ ", planId=" + planId + ", companyId=" + companyId
				+ ", accountId=" + accountId + ", mediumUserId=" + mediumUserId
				+ "]";
	}
}
