package com.dataeye.ad.assistor.module.mediumaccount.model;

import com.google.gson.annotations.Expose;


/**
 * Tracking渠道表
 * @create ldj 2017-08-10
 *
 */
public class Channel {
	/** 渠道id */
	@Expose
	private String channelId;
	/** 渠道名称 */
	@Expose
	private String showName;
	/** 媒体id */
	private Integer mediumId;
	
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getShowName() {
		return showName;
	}
	public void setShowName(String showName) {
		this.showName = showName;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	
	@Override
	public String toString() {
		return "Channel [channelId=" + channelId + ", showName=" + showName
				+ ", mediumId=" + mediumId + "]";
	}
	
}
