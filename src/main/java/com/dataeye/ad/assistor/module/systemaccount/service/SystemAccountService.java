package com.dataeye.ad.assistor.module.systemaccount.service;

import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccount;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountVo;
import com.dataeye.ad.assistor.module.mediumaccount.service.MediumAccountService;
import com.dataeye.ad.assistor.module.realtimedelivery.service.UpsAndDownsPreferenceService;
import com.dataeye.ad.assistor.module.report.service.IndicatorService;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.AccountRole;
import com.dataeye.ad.assistor.module.systemaccount.mapper.SystemAccountMapper;
import com.dataeye.ad.assistor.module.systemaccount.model.DevAuthorizedProductVo;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccount;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountSelectorVo;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;
import com.dataeye.ad.assistor.util.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 系统账号Service
 *
 * @author luzhuyou 2017/02/16
 */
@Service
public class SystemAccountService {

    @Autowired
    private SystemAccountMapper systemAccountMapper;

    @Autowired
    private SystemAccountGroupService systemAccountGroupService;

    @Autowired
    private MediumAccountService mediumAccountService;

    @Autowired
    private IndicatorService indicatorService;

    @Autowired
    private UpsAndDownsPreferenceService upsAndDownsPreferenceService;
    @Autowired
    private DevAuthorizedProductService devAuthorizedProductService;

    /**
     * 查询系统账号列表
     *
     * @param account   系统账号/账号别名
     * @param companyId
     * @param groupId
     * @return
     */
    public List<SystemAccountVo> query(String account, Integer groupId, int companyId) {
        SystemAccountVo vo = new SystemAccountVo();
        vo.setAccountName(account);
        vo.setAccountAlias(account);
        vo.setGroupId(groupId);
        vo.setCompanyId(companyId);
        return systemAccountMapper.query(vo);
    }

    /**
     * 根据账号名称（邮箱）账号信息
     *
     * @param accountName 系统账号
     * @return
     */
    public SystemAccountVo get(String accountName) {
        SystemAccountVo vo = new SystemAccountVo();
        vo.setAccountName(accountName);
        return systemAccountMapper.get(vo);
    }

    /**
     * 根据账号ID获取账号信息
     *
     * @param accountId 账号ID
     * @return
     */
    public SystemAccountVo get(int accountId) {
    	SystemAccountVo vo = systemAccountMapper.getById(accountId);
    	 if(vo.getAccountRole().intValue() == AccountRole.ENGINEER){
    		 DevAuthorizedProductVo oldDevAuthorizedProduct = devAuthorizedProductService.getDevAuthorizedProduct(accountId);
    		 if(oldDevAuthorizedProduct != null && oldDevAuthorizedProduct.getProductIds() != null){
    			 vo.setProductIds(oldDevAuthorizedProduct.getProductIds());
      		}
         }else{
        	String responsibleAccounts = "";
        	MediumAccount responsibleVo = mediumAccountService.getResponsibleFollowersGroupAccount(vo.getCompanyId(), vo.getAccountName(),null);
        	if(responsibleVo != null && responsibleVo.getMediumAccountIds() != null){
        		responsibleAccounts = responsibleVo.getMediumAccountIds();
        	}
        	String followerAccounts = "";
        	MediumAccount followerVo = mediumAccountService.getResponsibleFollowersGroupAccount(vo.getCompanyId(), null,vo.getAccountName());
        	if(followerVo != null && followerVo.getMediumAccountIds() != null){
        		followerAccounts = followerVo.getMediumAccountIds();
        	}
         	vo.setResponsibleAccounts(responsibleAccounts==null?"":responsibleAccounts);
         	vo.setFollowerAccounts(followerAccounts==null?"":followerAccounts);
         }
        return vo;
    }

    /**
     * 新增系统账号
     *
     * @param accountName
     * @param accountAlias
     * @param password
     * @param mobile
     * @param status
     * @param menuPermission
     * @param companyId
     * @param responsibleAccountIds 负责人ADT账号id
     * @param followerAccountIds 关注者ADT账号id
     * @param productIds 当角色为开发工程师的时候，才会有产品id
     * @return
     * @author luzhuyou 2017/02/17
     */
    public int add(String accountName, String accountAlias,
                   String password, String mobile, int status, int groupId,
                   String menuPermission, int companyId, int accountRole, String indicatorPermission, String responsibleAccountIds, String followerAccountIds, String productIds) {

        SystemAccount systemAccount = new SystemAccount();
        systemAccount.setAccountName(accountName);
        systemAccount.setAccountAlias(accountAlias);
        systemAccount.setPassword(password);
        systemAccount.setMobile(mobile);
        systemAccount.setStatus(status);
        systemAccount.setMenuPermission(menuPermission);
        systemAccount.setAccountRole(accountRole);
        systemAccount.setGroupId(groupId);
        systemAccount.setCompanyId(companyId);
        systemAccount.setCreateTime(new Date());
        systemAccount.setUpdateTime(new Date());
        systemAccount.setIndicatorPermission(indicatorPermission);
        int result = systemAccountMapper.add(systemAccount);

        //添加默认指标偏好
        SystemAccountVo query = new SystemAccountVo();
        query.setCompanyId(companyId);
        query.setAccountName(accountName);
        SystemAccountVo account = systemAccountMapper.get(query);
        indicatorService.addIndicatorPreference(companyId, account.getAccountId(), accountRole, indicatorPermission,null);
        //添加默认涨跌指标显示偏好
        upsAndDownsPreferenceService.addDefaultUpsAndDownsPreference(companyId, account.getAccountId(), accountRole, indicatorPermission);
        
        //增加子账号负责和关注的媒体帐号
        addResponsibleOrFollowerAccounts(companyId, accountName, StringUtil.stringToList(responsibleAccountIds), StringUtil.stringToList(followerAccountIds));
        
        if(accountRole == AccountRole.ENGINEER && StringUtils.isNotBlank(productIds)){
        	devAuthorizedProductService.add(account.getAccountId(), companyId, productIds);
        	ApplicationContextContainer.reloadDevAuthorizedProductMap(companyId);
        }
     // 刷新ApplicationContext中系统账号与媒体账号的映射关系
        ApplicationContextContainer.reloadSystemAccountToMediumAccountMap(companyId);
        return result;
    }

    /**
     * 修改系统账号
     *
     * @param accountId
     * @param accountName
     * @param accountAlias
     * @param mobile
     * @param status
     * @param menuPermission
     * @param companyId
     * @param responsibleAccountIds 负责人ADT账号id
     * @param followerAccountIds 关注者ADT账号id
     * @return
     * @author luzhuyou 2017/02/17
     */
    public int modify(int accountId, String accountName, String accountAlias, int accountRole, String mobile, int status, String menuPermission,
                      String indicatorPermission, int companyId, Integer groupId, String responsibleAccountIds, String followerAccountIds, String productIds) {
        SystemAccount systemAccount = new SystemAccount();
        systemAccount.setAccountId(accountId);
        systemAccount.setAccountName(accountName);
        systemAccount.setAccountAlias(accountAlias);
        systemAccount.setAccountRole(accountRole);
        systemAccount.setMobile(mobile);
        systemAccount.setStatus(status);
        systemAccount.setMenuPermission(menuPermission);
        systemAccount.setIndicatorPermission(indicatorPermission);
        systemAccount.setGroupId(groupId);
        systemAccount.setCompanyId(companyId);
        systemAccount.setUpdateTime(new Date());
        int result = systemAccountMapper.update(systemAccount);
        //更新指标偏好.
        indicatorService.updateIndicatorPreference(companyId, accountId, accountRole, indicatorPermission);
        //更新涨跌指标显示偏好
        upsAndDownsPreferenceService.updateUpsAndDownsPreferenceAsPermissionChanged(companyId, accountId, accountRole, indicatorPermission);
        
        if(accountRole == AccountRole.ENGINEER){
        	
        	if(StringUtils.isNotBlank(productIds)){
        		DevAuthorizedProductVo oldDevAuthorizedProduct = devAuthorizedProductService.getDevAuthorizedProduct(accountId);
        		if(oldDevAuthorizedProduct != null){
        			oldDevAuthorizedProduct.setProductIds(productIds);
        			devAuthorizedProductService.update(accountId, productIds);
        		}else{
        			devAuthorizedProductService.add(accountId, companyId, productIds);
        		}
        	}else{
        		//删除
        		devAuthorizedProductService.delete(accountId);
        	}
        	ApplicationContextContainer.reloadDevAuthorizedProductMap(companyId);
        }
      //更新子账号负责和关注的媒体帐号
        modifyResponsibleOrFollowerAccounts(companyId, accountName, responsibleAccountIds, followerAccountIds);
       // 刷新ApplicationContext中系统账号与媒体账号的映射关系
        ApplicationContextContainer.reloadSystemAccountToMediumAccountMap(companyId);
        return result;
    }

    /**
     * 更新系统账号
     *
     * @param systemAccount
     * @return
     */
    public int updateSystemAccount(SystemAccount systemAccount) {
        int result = systemAccountMapper.update(systemAccount);
        //更新指标偏好.
        indicatorService.updateIndicatorPreference(systemAccount.getCompanyId(), systemAccount.getAccountId(), systemAccount.getAccountRole(), systemAccount.getIndicatorPermission());
        //更新涨跌指标显示偏好
        upsAndDownsPreferenceService.updateUpsAndDownsPreferenceAsPermissionChanged(systemAccount.getCompanyId(), systemAccount.getAccountId(), systemAccount.getAccountRole(), systemAccount.getIndicatorPermission());
        return result;
    }

    /**
     * 修改账号角色
     *
     * @param accountId
     * @param accountName
     * @param accountRole
     * @return
     * @author luzhuyou 2017/02/17
     */
    public int modifyAccountRole(Integer accountId, String accountName, int accountRole) {
        SystemAccount systemAccount = new SystemAccount();
        systemAccount.setAccountId(accountId);
        systemAccount.setAccountName(accountName);
        systemAccount.setAccountRole(accountRole);
        systemAccount.setUpdateTime(new Date());
        return systemAccountMapper.updateAccountRole(systemAccount);
    }

    /**
     * 修改系统账号分组ID
     *
     * @param accountId
     * @param accountName
     * @param groupId
     * @return
     */
    public int modifyGroupId(Integer accountId, String accountName, int groupId) {
        SystemAccount systemAccount = new SystemAccount();
        systemAccount.setAccountId(accountId);
        systemAccount.setAccountName(accountName);
        systemAccount.setGroupId(groupId);
        systemAccount.setUpdateTime(new Date());
        return systemAccountMapper.updateGroupId(systemAccount);
    }

    /**
     * 重置已分组ID为未分组
     *
     * @param groupId
     * @return
     */
    public int resetGroupId(int groupId) {
        SystemAccount systemAccount = new SystemAccount();
        systemAccount.setGroupId(groupId);
        systemAccount.setUpdateTime(new Date());
        return systemAccountMapper.resetGroupId(systemAccount);
    }

    /**
     * 修改系统账号状态
     *
     * @param accountId
     * @param accountName
     * @param status
     * @return
     */
    public int modifyStatus(Integer accountId, String accountName, int status) {
        SystemAccount systemAccount = new SystemAccount();
        systemAccount.setAccountId(accountId);
        systemAccount.setAccountName(accountName);
        systemAccount.setStatus(status);
        systemAccount.setUpdateTime(new Date());
        return systemAccountMapper.updateStatus(systemAccount);
    }

    /**
     * 修改系统账号密码
     *
     * @param accountId
     * @param password
     */
    public int modifyPassword(Integer accountId, String password) {
        SystemAccount systemAccount = new SystemAccount();
        systemAccount.setAccountId(accountId);
        systemAccount.setPassword(password);
        systemAccount.setUpdateTime(new Date());
        return systemAccountMapper.updatePassword(systemAccount);
    }

    /**
     * 下拉框查询系统账号
     *
     * @param groupIds  请求分组ID集合参数
     * @param companyId
     * @return
     */
    public List<SystemAccountSelectorVo> queryForSelector(Integer[] groupIds, int companyId, boolean isGroupLeaderCandidate, boolean isManagerOrFollowerCandidate) {
        SystemAccountSelectorVo vo = new SystemAccountSelectorVo();
        vo.setRequestGroupIds(groupIds);
        vo.setCompanyId(companyId);
        vo.setGroupLeaderCandidate(isGroupLeaderCandidate);
        vo.setManagerOrFollowerCandidate(isManagerOrFollowerCandidate);
        return systemAccountMapper.queryForSelector(vo);
    }

    /**
     * 删除系统账号
     * <br><em>如果该系统账号属于某个分组组长账号，则将该分组组长账号更新为空值。</em>
     * <br><em>将媒体账号关联的关注者ADT账号删除。</em>
     * <br><em>将媒体账号关联的优化师ADT账号删除。</em>
     * <br><em>删除系统账号。</em>
     *
     * @param systemAccountVo
     * @return
     */
    public int delete(SystemAccountVo systemAccountVo) {
   /*     // 如果该系统账号是否组长，则更新组长账号为空
        systemAccountGroupService.resetLeaderAccount(systemAccountVo.getCompanyId(), systemAccountVo.getAccountName());
        // 更新该系统账号为删除状态，将系统账号角色ID修改为未分组ID
//		return this.modify(systemAccountVo.getAccountId(), systemAccountVo.getAccountName(), 
//				systemAccountVo.getAccountAlias(), systemAccountVo.getMobile(), 
//				Constants.UserStatus.DELETE, systemAccountVo.getMenuPermission(), 
//				systemAccountVo.getCompanyId(), Constants.GroupId.NONE);

        // 将媒体账号关联的关注者ADT账号删除
        List<MediumAccountVo> mediumAccountVoForFollowersList = ApplicationContextContainer.queryMediumAccountVoForFollowersList(systemAccountVo.getCompanyId(), systemAccountVo.getAccountName());
        if (mediumAccountVoForFollowersList != null && !mediumAccountVoForFollowersList.isEmpty()) {
            for (MediumAccountVo vo : mediumAccountVoForFollowersList) {
                MediumAccountRelation mediumAccount = new MediumAccountRelation();
                mediumAccount.setMediumAccountId(vo.getMediumAccountId());
                mediumAccount.setProductId(vo.getProductId());
                // 更新关注者ADT账号
                String systemAccountNameList = vo.getFollowerAccounts();
                StringBuilder sb = new StringBuilder("");
                if (StringUtils.isNotBlank(systemAccountNameList)) {
                    String[] systemAccountNameArr = systemAccountNameList.trim().split(",");
                    for (int i = 0; i < systemAccountNameArr.length; i++) {
                        if (systemAccountNameArr[i].equals(systemAccountVo.getAccountName())) {
                            continue;
                        }
                        sb.append(systemAccountNameArr[i]).append(",");
                    }
                    if (sb.toString().endsWith(",")) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                }
                mediumAccount.setFollowerAccounts(sb.toString());
                mediumAccount.setUpdateTime(new Date());
                // 更新媒体账户表对应SystemAccountNameList字段
                mediumAccountService.modifySystemAccountNameList(mediumAccount);
            }
        }

        // 将媒体账号关联的优化师ADT账号删除
        List<MediumAccountVo> mediumAccountVoForOptimizerList = ApplicationContextContainer.queryMediumAccountVoForResponsibleAccountList(systemAccountVo.getCompanyId(), systemAccountVo.getAccountName());
        if (mediumAccountVoForOptimizerList != null && !mediumAccountVoForOptimizerList.isEmpty()) {
            for (MediumAccountVo vo : mediumAccountVoForOptimizerList) {
                MediumAccountRelation mediumAccount = new MediumAccountRelation();
                mediumAccount.setMediumAccountId(vo.getMediumAccountId());
                mediumAccount.setProductId(vo.getProductId());
                // 更新优化师ADT账号
                String optimizerAccountNameList = vo.getResponsibleAccounts();
                StringBuilder sb = new StringBuilder("");
                if (StringUtils.isNotBlank(optimizerAccountNameList)) {
                    String[] optimizerAccountNameArr = optimizerAccountNameList.trim().split(",");
                    for (int i = 0; i < optimizerAccountNameArr.length; i++) {
                        if (optimizerAccountNameArr[i].equals(systemAccountVo.getAccountName())) {
                            continue;
                        }
                        sb.append(optimizerAccountNameArr[i]).append(",");
                    }
                    if (sb.toString().endsWith(",")) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                }
                mediumAccount.setResponsibleAccounts(sb.toString());
                mediumAccount.setUpdateTime(new Date());
                // 更新媒体账户关联关系表对应OptimizerSystemAccount字段
                mediumAccountService.modifyOptimizerSystemAccount(mediumAccount);
            }
        }

        // 直接删除
        int result = systemAccountMapper.delete(systemAccountVo.getAccountId());

        // 刷新ApplicationContext中系统账号与媒体账号的映射关系
        ApplicationContextContainer.reloadSystemAccountToMediumAccountMap();

        // 重新加载组长所对应所有组员系统账号信息
        ApplicationContextContainer.reloadLeaderMemberAccountsMap();
        return result;*/
    	return 0;
    }


    /**
     * 列出所有的系统账号
     *
     * @return
     */
    public List<SystemAccount> listSystemAccounts() {
        return systemAccountMapper.listSystemAccounts();
    }
    
    /**
     * 检验id字符串中是否包含id,包含则返回true，否则返回true
     * 
     * */
    public Boolean checkContainsId(String ids, String id) {
        if (StringUtils.isNotBlank(ids) && StringUtils.isNotBlank(id) && StringUtils.contains(ids, id)) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * 
     * 修改子账户负责和关注的媒体帐号
     * @param companyId
     * @param accountName
     * @param responsibleAccountIds 多个用逗号拼接
     * @param followerAccountIds 多个用逗号拼接
     * @create ldj 2017-08-29
     * **/
    public void modifyResponsibleOrFollowerAccounts(int companyId, String accountName, String responsibleAccountIds, String followerAccountIds) {
    	System.out.println("修改子账户负责和关注的媒体帐号,accountName=="+accountName+",responsibleAccountIds="+responsibleAccountIds+",followerAccountIds="+followerAccountIds);
    	
    	//需要新增的关注或者负责的媒体帐号的集合
    	List<Integer> addRresponsibleList = new ArrayList<>();
    	//需要删除的关注或者负责的媒体帐号的集合
    	List<Integer> deleteRresponsibleList = new ArrayList<>();
    	//1.处理responsibleAccountIds字符串，转成List集合
    	List<Integer> responsibleAccountIdList = StringUtil.stringToList(responsibleAccountIds);
    	if(responsibleAccountIdList == null){
    		responsibleAccountIdList=  new ArrayList<>();
        }
    	//2.先查出系统帐号负责和关注的媒体帐号信息
        List<Integer> oldResponsible = mediumAccountService.getMediumAccountIdByRespOrFollowers(companyId, accountName, null);
        if(oldResponsible == null){
        	oldResponsible=  new ArrayList<>();
        }
        
        List<Integer> result = new ArrayList<>();
      //3.取差集，将需要新增的媒体帐号id抽取出来，放入addRresponsibleList中
        result.addAll(responsibleAccountIdList);
        result.removeAll(oldResponsible);
        addRresponsibleList.addAll(result);
        result.clear();
        
        //4.取差集，将需要删除的媒体帐号id抽取出来，取消对该媒体帐号的负责
        result.addAll(oldResponsible);
        result.removeAll(responsibleAccountIdList);
        deleteRresponsibleList.addAll(result);
        result.clear();
        
        //取消对媒体帐号的负责
        String accountNameStr = Constant.Separator.COMMA+accountName+ Constant.Separator.COMMA;
        for (Integer mediumAccountId : deleteRresponsibleList) {
        	MediumAccountVo oldMediumAccountVo = mediumAccountService.getMediumAccount(null, mediumAccountId);
        	if(oldMediumAccountVo != null){
        		//检验媒体帐号是否存在子账户名称，存在则取消
        		if(StringUtils.isNotBlank(oldMediumAccountVo.getResponsibleAccounts())){
        			
        			String oldResponsibleAccountNames = Constant.Separator.COMMA+oldMediumAccountVo.getResponsibleAccounts()+ Constant.Separator.COMMA;
         		    if(oldResponsibleAccountNames.contains(accountNameStr)){
         		    	oldResponsibleAccountNames = oldResponsibleAccountNames.replace(accountNameStr, ",");
         		    	if(oldResponsibleAccountNames.startsWith(Constant.Separator.COMMA)){
         		    		oldResponsibleAccountNames = oldResponsibleAccountNames.substring(1);
         		    	}
         		    	if(oldResponsibleAccountNames.endsWith(Constant.Separator.COMMA)){
         		    		oldResponsibleAccountNames = oldResponsibleAccountNames.substring(0, oldResponsibleAccountNames.length()-1);
         		    	}
         		    	mediumAccountService.modifyResponsibleOrFollowerAccounts(mediumAccountId, null, oldResponsibleAccountNames);
         		    }
         		    
         		 }
         		
         	 }
		}
        
        /////////////////////////处理关注的媒体帐号//////////////////////////////////////////////////
      //需要新增的关注或者负责的媒体帐号的集合
    	List<Integer> addFollowerList = new ArrayList<>();
    	//需要删除的关注或者负责的媒体帐号的集合
    	List<Integer> deleteFollowerList = new ArrayList<>();
    	//1.处理FollowerAccountIds字符串，转成List集合
    	List<Integer> followerAccountIdList = StringUtil.stringToList(followerAccountIds);
    	if(followerAccountIdList == null){
    		followerAccountIdList=  new ArrayList<>();
        }
    	//2.先查出系统帐号负责和关注的媒体帐号信息
        List<Integer> oldFollower = mediumAccountService.getMediumAccountIdByRespOrFollowers(companyId, null, accountName);
        if(oldFollower == null){
        	oldFollower=  new ArrayList<>();
        }

       //3.取差集，将需要新增的媒体帐号id抽取出来，放入addFollowerList中
        result.addAll(followerAccountIdList);
        result.removeAll(oldFollower);
        addFollowerList.addAll(result);
        result.clear();
        
        //4.取差集，将需要删除的媒体帐号id抽取出来，取消对该媒体帐号的负责
        result.addAll(oldFollower);
        result.removeAll(followerAccountIdList);
        deleteFollowerList.addAll(result);
        result.clear();
        
        //取消对媒体帐号的负责
        for (Integer mediumAccountId : deleteFollowerList) {
        	MediumAccountVo oldMediumAccountVo = mediumAccountService.getMediumAccount(null, mediumAccountId);
        	if(oldMediumAccountVo != null){
        		//检验媒体帐号是否存在子账户名称，存在则取消
        		if(StringUtils.isNotBlank(oldMediumAccountVo.getFollowerAccounts())){
        			
        			String oldFollowerAccountNames = Constant.Separator.COMMA+oldMediumAccountVo.getFollowerAccounts()+ Constant.Separator.COMMA;
         		    if(oldFollowerAccountNames.contains(accountNameStr)){
         		    	oldFollowerAccountNames = oldFollowerAccountNames.replace(accountNameStr, ",");
         		    	if(oldFollowerAccountNames.startsWith(Constant.Separator.COMMA)){
         		    		oldFollowerAccountNames = oldFollowerAccountNames.substring(1);
         		    	}
         		    	if(oldFollowerAccountNames.endsWith(Constant.Separator.COMMA)){
         		    		oldFollowerAccountNames = oldFollowerAccountNames.substring(0, oldFollowerAccountNames.length()-1);
         		    	}
         		    	mediumAccountService.modifyResponsibleOrFollowerAccounts(mediumAccountId, oldFollowerAccountNames, null);
         		    }
         		    
         		 }
         		
         	 }
		}
        
        //2.增加系统帐号负责和关注的媒体帐号信息
        addResponsibleOrFollowerAccounts(companyId, accountName, addRresponsibleList, addFollowerList);
	}
 
    /**
     * 
     * 新增子账户负责和关注的媒体帐号
     * @param companyId
     * @param accountName
     * @param responsibleAccountIdList 
     * @param followerAccountIdList 
     * @create ldj 2017-08-29
     * **/
    public void addResponsibleOrFollowerAccounts(int companyId, String accountName, List<Integer> responsibleAccountIdList, List<Integer> followerAccountIdList) {
    	if(responsibleAccountIdList !=null && responsibleAccountIdList.size()>0){
            for (Integer respId : responsibleAccountIdList) {
				if(respId != null ){
					MediumAccountVo mediumAccountVo = mediumAccountService.getMediumAccount(null, respId);
                 	 if(mediumAccountVo != null){
                 		 String responsibleAccountNames = accountName;
                 		 if(StringUtils.isNotBlank(mediumAccountVo.getResponsibleAccounts())){
                 			 responsibleAccountNames = mediumAccountVo.getResponsibleAccounts()+","+accountName;
                 		 }
                 		 mediumAccountService.modifyResponsibleOrFollowerAccounts(respId, null, responsibleAccountNames);
                 	 }
				}
			}
       }
    	if(followerAccountIdList != null && followerAccountIdList.size()>0){
    		for (Integer follwerId : followerAccountIdList) {
    			if(follwerId != null ){
    				MediumAccountVo mediumAccountVo = mediumAccountService.getMediumAccount(null, follwerId);
                	 if(mediumAccountVo != null){
                		 String followerAccountNames = accountName;
                		 if(StringUtils.isNotBlank(mediumAccountVo.getFollowerAccounts())){
                			followerAccountNames = mediumAccountVo.getFollowerAccounts()+","+accountName;
                		 }
                		 mediumAccountService.modifyResponsibleOrFollowerAccounts(follwerId, followerAccountNames, null);
                	 }
    			}
    		}
    	} 
    }

    /**
     * 修改登录后页面展示消息已提示次数
     *
     * @param accountId
     * @param alreadyPromptTimes
     * @return
     * @author luzhuyou 2017/09/14
     */
    public int modifyPromptTimes(Integer accountId, int alreadyPromptTimes) {
        SystemAccount systemAccount = new SystemAccount();
        systemAccount.setAccountId(accountId);
        systemAccount.setAlreadyPromptTimes(alreadyPromptTimes);
        systemAccount.setUpdateTime(new Date());
        return systemAccountMapper.updatePromptTimes(systemAccount);
    }
    
    /**
     * 下拉框查询子账户
     *
     * @param permissionSystemAccountIds
     * @param companyId
     * @return
     */
    public List<SystemAccountSelectorVo> querySubAccountForSelector(Integer[] permissionSystemAccountIds, int companyId) {
        SystemAccountSelectorVo vo = new SystemAccountSelectorVo();
        vo.setPermissionSystemAccountIds(permissionSystemAccountIds);
        vo.setCompanyId(companyId);
        vo.setManagerOrFollowerCandidate(true);
        return systemAccountMapper.queryForSelector(vo);
    }
}
