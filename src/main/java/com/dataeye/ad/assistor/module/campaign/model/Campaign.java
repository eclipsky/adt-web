package com.dataeye.ad.assistor.module.campaign.model;

import java.util.Date;

/**
 * 推广计划（广告活动）
 * @author luzhuyou 2017/06/15
 *
 */
public class Campaign {

	/** 活动ID（短链活动号）*/
	private String campaignId;
	/** 活动名称 */
	private String campaignName;
	/** 公司ID */
	private Integer companyId;
	/** ADT账号ID */
	private Integer systemAccountId;
	/** 产品ID */
	private Integer productId;
	/** 媒体ID */
	private Integer mediumId;
	/** 媒体账号ID */
	private Integer mediumAccountId;
	/** 系统类型：0-Others, 1-iOS, 2-Android */
	private Integer osType;
	/** 推广链接 */
	private String trackUrl;
	/** 下载地址（落地页URL） */
	private String downloadUrl;
	/** 是否上应用宝:0-否，1-是 */
	private Integer appTreasure;
	/** 广告参数 */
	private String thirdAdParams;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getSystemAccountId() {
		return systemAccountId;
	}
	public void setSystemAccountId(Integer systemAccountId) {
		this.systemAccountId = systemAccountId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	public String getTrackUrl() {
		return trackUrl;
	}
	public void setTrackUrl(String trackUrl) {
		this.trackUrl = trackUrl;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public Integer getAppTreasure() {
		return appTreasure;
	}
	public void setAppTreasure(Integer appTreasure) {
		this.appTreasure = appTreasure;
	}
	public String getThirdAdParams() {
		return thirdAdParams;
	}
	public void setThirdAdParams(String thirdAdParams) {
		this.thirdAdParams = thirdAdParams;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "Campaign [campaignId=" + campaignId + ", campaignName="
				+ campaignName + ", companyId=" + companyId
				+ ", systemAccountId=" + systemAccountId + ", productId="
				+ productId + ", mediumId=" + mediumId + ", mediumAccountId="
				+ mediumAccountId + ", osType=" + osType + ", trackUrl="
				+ trackUrl + ", downloadUrl=" + downloadUrl + ", appTreasure="
				+ appTreasure + ", thirdAdParams=" + thirdAdParams
				+ ", remark=" + remark + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + "]";
	}
}
