package com.dataeye.ad.assistor.module.result.service;

import com.dataeye.ad.assistor.module.result.model.*;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/6.
 */
public interface CoreIndicatorService {
    /**
     * 获取总体核心指标.
     *
     * @return
     */
    OverallIndicator getOverallCoreIndicator(CoreIndicatorQuery dataPermissionDomain);

    /**
     * 按媒体分组统计核心指标.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCostGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计充值.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getRechargeGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计回本率
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getPaybackRateGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计余额
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getBalanceGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计CPA.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCpaGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计DAU
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getDauGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计消耗.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCostGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计充值.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getRechargeGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计回本率
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getPaybackRateGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计余额
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getBalanceGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计CPA.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCpaGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计DAU
     *
     * @param coreIndicatorQuery
     * @returnl
     */
    List<Indicator> getDauGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计消耗.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCostGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计充值.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getRechargeGroupByOs(DataPermissionDomain coreIndicatorQuery);

    /**
     * 按产品分组统计回本率
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getPaybackRateGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计CPA.
     *
     * @param coreIndicatorQuery
     * @return
     */
    List<Indicator> getCpaGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计DAU
     *
     * @param coreIndicatorQuery
     * @returnl
     */
    List<Indicator> getDauGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计日消耗趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyCostTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计日充值趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyRechargeTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计日余额趋势
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyBalanceTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计日CPA趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyCpaTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按媒体分组统计日DAU趋势
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyDauTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日消耗趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyCostTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日充值趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyRechargeTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日余额趋势
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyBalanceTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日CPA趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyCpaTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日DAU趋势
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyDauTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日消耗趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyCostTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日充值趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyRechargeTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日CPA趋势.
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyCpaTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery);

    /**
     * 按产品分组统计日DAU趋势
     *
     * @param coreIndicatorQuery
     * @return
     */
    TrendChart<BigDecimal> dailyDauTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery);
}
