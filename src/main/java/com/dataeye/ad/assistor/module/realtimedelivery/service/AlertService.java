package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeDeliveryFormatVo;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeDeliveryVo;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.List;

/**
 * Created by huangzehai on 2017/2/28.
 */
public interface AlertService {
    /**
     * 给实时统计结果设置告警
     *
     * @param realTimeStats
     */
    void setAlert(List<RealTimeDeliveryFormatVo> realTimeStats, List<RealTimeDeliveryVo> realTimeDeliveryList, DataPermissionDomain dataPermissionDomain, int offset);

    /**
     * 发送高级告警
     *
     * @return
     */
    void sendAdvancedAlerts();

    void sendGeneralAlerts(String date);
}
