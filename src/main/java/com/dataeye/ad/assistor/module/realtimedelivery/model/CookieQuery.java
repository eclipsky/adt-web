package com.dataeye.ad.assistor.module.realtimedelivery.model;

/**
 * Created by huangzehai on 2017/5/12.
 */
public class CookieQuery {
    /**
     * 平台类型，1-媒体平台，2-H5平台，3-CP平台
     */
    private int type;
    /**
     * 媒体ID.
     */
    private int mediumId;

    /**
     * 媒体账号.
     */
    private String mediumAccount;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getMediumId() {
        return mediumId;
    }

    public void setMediumId(int mediumId) {
        this.mediumId = mediumId;
    }

    public String getMediumAccount() {
        return mediumAccount;
    }

    public void setMediumAccount(String mediumAccount) {
        this.mediumAccount = mediumAccount;
    }

    @Override
    public String toString() {
        return "CookieQuery{" +
                "type=" + type +
                ", mediumId=" + mediumId +
                ", mediumAccount='" + mediumAccount + '\'' +
                '}';
    }
}
