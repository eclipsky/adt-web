package com.dataeye.ad.assistor.module.report.model;

import org.apache.commons.lang.StringUtils;

public enum Period {
    Month, Week, Day, Hour(60), HalfAnHour(30), TwentyMinutes(20), TenMinutes(10);

    private Integer intervalInMinutes;

    public Integer getIntervalInMinutes() {
        return intervalInMinutes;
    }

    public void setIntervalInMinutes(Integer intervalInMinutes) {
        this.intervalInMinutes = intervalInMinutes;
    }

    Period(Integer intervalInMinutes) {
        this.intervalInMinutes = intervalInMinutes;
    }

    Period() {
    }

    public static Period parse(String text) {
        for (Period period : Period.values()) {
            if (StringUtils.equalsIgnoreCase(period.name(), text)) {
                return period;
            }
        }
        return null;
    }
}
