package com.dataeye.ad.assistor.module.advertisement.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.advertisement.model.AdGroupQueryVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdvertisementGroupSelectVo;
import com.dataeye.ad.assistor.module.advertisement.model.ApiTxAdGroupVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdcreativeTemplate;
import com.dataeye.ad.assistor.module.advertisement.model.MarketingApiAuth;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupVO;

/**
 * 广告投放
 * 广告关联关系映射器.
 * Created by ldj
 */
@MapperScan
public interface ApiTxAdGroupMapper {

    /**
     * 查询广告关联关系
     *
     * @param vo
     * @return
     */
    public List<ApiTxAdGroupVo> query(AdGroupQueryVo vo);

    /**
     * 下拉框查询广告关联关系
     *
     * @param vo
     * @return
     */
    public List<AdvertisementGroupSelectVo> queryForSelect(AdGroupQueryVo vo);

    /**
     * 更新广告关联关系
     *
     * @param relation
     * @return
     */
	public int update(ApiTxAdGroupVo relation);

    /**
     * 创建广告关联关系
     *
     * @param relation
     */
    public void add(ApiTxAdGroupVo relation);


    /**
     * 删除广告关联关系
     *
     * @param relation
     * @return
     */
    public int delete(ApiTxAdGroupVo relation);

    /**
     * 获得GDT授权账户信息
     */
    public MarketingApiAuth getAccessTokey(Integer mediumAccountId);

    /**
     * 查询广告创意版位
     * productType  (PRODUCT_TYPE_APP_IOS,PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM)
     */
    public List<AdcreativeTemplate> queryAdcreativeTemplate(String productType);

    
    /**
     * 获取广告
     *
     * @param  adGroupId
     * @return
     */
    public ApiTxAdGroupVo getAdGroup(Integer adGroupId);

    /**
     * 查询推广计划下的产品信息
     *
     * @param vo
     * @return
     */
    public PlanGroupVO queryProductInfosByPlanGroupId(AdGroupQueryVo vo);


    /**
     * 根据广告ID删除广告关联关系
     *
     * @param relation
     * @return
     */
    public int deleteByAdGroupId(ApiTxAdGroupVo relation);

    /**
     * 查询数据库是否存在该记录
     *
     * @param mediumAccountId
     * @param adGroupName
     * @param adGroupId
     * @return
     */
    public Integer getRecordByAdGroupName(@Param("mediumAccountId") Integer mediumAccountId,
                                          @Param("adGroupName") String adGroupName, @Param("adGroupId") Integer adGroupId);
}
