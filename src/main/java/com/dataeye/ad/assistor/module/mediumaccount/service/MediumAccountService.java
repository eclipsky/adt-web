package com.dataeye.ad.assistor.module.mediumaccount.service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.common.Item;
import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.constant.Constant.MediumType;
import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.association.mapper.AssociationRuleMapper;
import com.dataeye.ad.assistor.module.association.model.PlanRelation;
import com.dataeye.ad.assistor.module.mediumaccount.constants.Constants.MediumAccountInterface;
import com.dataeye.ad.assistor.module.mediumaccount.mapper.MediumAccountMapper;
import com.dataeye.ad.assistor.module.mediumaccount.mapper.MediumAccountRelationMapper;
import com.dataeye.ad.assistor.module.mediumaccount.mapper.MediumMapper;
import com.dataeye.ad.assistor.module.mediumaccount.model.*;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.AccountRole;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountService;
import com.dataeye.ad.assistor.util.HttpUtil;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 媒体账户业务处理
 */
@Service("mediumAccountService")
public class MediumAccountService {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(MediumAccountService.class);
    @Autowired
    private MediumAccountMapper mediumAccountMapper;
    @Autowired
    private MediumDiscountRateService mediumDiscountRateService;
    @Autowired
    private MediumAccountRelationMapper mediumAccountRelationMapper;
    @Autowired
    private AssociationRuleMapper associationRuleMapper;
    @Autowired
    private MediumMapper mediumMapper;
    @Autowired
    private SystemAccountService systemAccountService;


    /**
     * 查询媒体账号列表信息
     *
     * @return
     */
    public List<MediumAccountVo> query(Integer companyId) {
    	MediumAccountVo mediumAccountVo = new MediumAccountVo();
        mediumAccountVo.setCompanyId(companyId);
        return mediumAccountMapper.query(mediumAccountVo);
    }

    /**
     * 查询媒体账号列表信息
     *
     * @param companyId
     * @param productId
     * @param mediumId
     * @param permissionMediumAccountIds
     * @param mediumIds
     * @param mediumAccountIds
     * @param account 负责人/关注人
     * @return
     */
    public List<MediumAccountVo> query(Integer companyId, Integer mediumId, Integer[] permissionMediumAccountIds, String mediumIds, 
			String mediumAccountIds, String account) {
        MediumAccountVo mediumAccountVo = new MediumAccountVo();
        mediumAccountVo.setCompanyId(companyId);
      //  mediumAccountVo.setProductId(productId);
        mediumAccountVo.setMediumId(mediumId);
        mediumAccountVo.setPermissionMediumAccountIds(permissionMediumAccountIds);
        mediumAccountVo.setResponsibleAccounts(account);
        if (null != mediumIds) {
			String[] array = mediumIds.split(",");
			Integer[] mediumIdsArr = new Integer[array.length];
			for(int i=0; i<array.length; i++) {
				mediumIdsArr[i] = Integer.parseInt(array[i]);
			}
			mediumAccountVo.setMediumIds(mediumIdsArr);
		}
		if (null != mediumAccountIds) {
			String[] array = mediumAccountIds.split(",");
			Integer[] mediumAccountIdsArr = new Integer[array.length];
			for(int i=0; i<array.length; i++) {
				mediumAccountIdsArr[i] = Integer.parseInt(array[i]);
			}
			mediumAccountVo.setMediumAccountIds(mediumAccountIdsArr);
		}
        return mediumAccountMapper.query(mediumAccountVo);
    }

    /**
     * 查询单个媒体账号信息
     *
     * @param companyId
     * @param mediumId
     * @param mediumAccount
     * @return
     */
    public MediumAccountVo get(int companyId, Integer mediumId, String mediumAccount) {
        MediumAccountVo mediumAccountVo = new MediumAccountVo();
        mediumAccountVo.setCompanyId(companyId);
        mediumAccountVo.setMediumId(mediumId);
        mediumAccountVo.setMediumAccount(mediumAccount);
        return mediumAccountMapper.get(mediumAccountVo);
    }

    /**
     * 新增媒体账号信息
     *
     * @param companyId
     * @param productIds
     * @param mediumId
     * @param mediumAccount
     * @param mediumAccountAlias
     * @param password
     * @param discountRate
     * @param followerAccounts
     * @param status
     * @param responsibleAccounts
     * @param pageType
     * @return
     */
    public int add(int companyId, String productIds, int mediumId, String mediumAccount,
                   String mediumAccountAlias, String password, double discountRate,
                   String followerAccounts, int status, String responsibleAccounts, int pageType) {
        // 新增媒体账号
        MediumAccount account = new MediumAccount();
        account.setCompanyId(companyId);
        account.setMediumId(mediumId);
        account.setMediumAccount(mediumAccount);
        account.setMediumAccountAlias(mediumAccountAlias);
        account.setPassword(password);
        account.setStatus(status);
        account.setCreateTime(new Date());
        account.setUpdateTime(new Date());
        account.setPageType(pageType);
        account.setFollowerAccounts(followerAccounts);
        account.setResponsibleAccounts(responsibleAccounts);
        mediumAccountMapper.add(account);

        
        List<Integer>  productIdList = StringUtil.stringToList(productIds);
        if(productIdList != null && productIdList.size()>0){
        	for (Integer productId : productIdList) {
        		//添加媒体帐号关联关系
				addMediumAccountRelation(companyId, account.getMediumAccountId(), mediumId, productId);
			}
        }      

        // 新增媒体折扣率
        mediumDiscountRateService.add(account.getMediumAccountId(), discountRate);

        // 刷新ApplicationContext中系统账号与媒体账号的映射关系
        ApplicationContextContainer.reloadSystemAccountToMediumAccountMap(companyId);

        return account.getMediumAccountId();
    }

    /**
     * 修改媒体账号信息
     *
     * @param companyId
     * @param mediumAccountId
     * @param productIds
     * @param mediumId
     * @param mediumAccount
     * @param mediumAccountAlias
     * @param password
     * @param discountRate
     * @param followerAccounts
     * @param status
     * @param responsibleAccounts
     * @param accountRole
     * @return
     */
    public int modify(int companyId, int mediumAccountId, String productIds, int mediumId,
                      String mediumAccount, String mediumAccountAlias, String password,
                      double discountRate, String followerAccounts, int status, String responsibleAccounts,int accountRole,Integer dailyBudget) {
    	
		// 修改媒体账号
		MediumAccount account = new MediumAccount();
		account.setCompanyId(companyId);
		account.setMediumAccountId(mediumAccountId);
		account.setMediumId(mediumId);
		account.setMediumAccount(mediumAccount);
		account.setMediumAccountAlias(mediumAccountAlias);
		account.setPassword(password);
		account.setStatus(status);
		account.setCreateTime(new Date());
		account.setUpdateTime(new Date());
		account.setFollowerAccounts(followerAccounts);
        account.setResponsibleAccounts(responsibleAccounts);
        if(StringUtils.isNotBlank(password)){
        	account.setLoginStatus(1);  //如果修改密码，则将登录状态改成未登录
        }
        if(dailyBudget != null && dailyBudget > 0 && mediumId == 13){
        	account.setDailyBudget(dailyBudget);
        }
		mediumAccountMapper.update(account);

		int result = 0;
        // 修改媒体账号关联关系(先将旧的关系全部删除，再添加新的关系)
		/*MediumAccountRelation accountRelationQuery = new MediumAccountRelation();
		accountRelationQuery.setCompanyId(companyId);
		accountRelationQuery.setMediumAccountId(mediumAccountId);
		accountRelationQuery.setMediumId(mediumId);*/
		//List<MediumAccountRelation> oldRelationList = mediumAccountRelationMapper.getMediumAccountRelation(accountRelationQuery);
		
		if(accountRole == AccountRole.COMPANY){
			deleteMediumAccountRelation(companyId, mediumAccountId, mediumId, null);
		    List<Integer>  productIdList = StringUtil.stringToList(productIds);
	        if(productIdList != null && productIdList.size()>0){
	        	for (Integer productId : productIdList) {
	        		//添加媒体帐号关联关系
	        		result = addMediumAccountRelation(companyId, mediumAccountId, mediumId, productId);
				}
	        } 
			 //删除负责人时，更新计划关联关系
	        updatePlanRelations(mediumAccountId, responsibleAccounts);
	        // 修改媒体折扣率
	        mediumDiscountRateService.modify(account.getMediumAccountId(), discountRate);
		}
		
        // 刷新ApplicationContext中系统账号与媒体账号的映射关系
        ApplicationContextContainer.reloadSystemAccountToMediumAccountMap(companyId);

        return result;
    }

    /**
     * 更新关联关系表的负责人列表。
     *
     * @param mediumAccountId
     * @param optimizerSystemAccount
     */
    private void updatePlanRelations(int mediumAccountId, String optimizerSystemAccount) {
        List<PlanRelation> planRelations = associationRuleMapper.getPlanRelationsByMediumAccountId(mediumAccountId);
        if (planRelations != null) {
            for (PlanRelation planRelation : planRelations) {
                if (StringUtils.isNotBlank(planRelation.getOptimizerSystemAccount())) {
                    String oldOptimizerSystemAccount = planRelation.getOptimizerSystemAccount();
                    String updatedOptimizerSystemAccount = updateMediumAccountManager(oldOptimizerSystemAccount, optimizerSystemAccount);
                    //如果负责人列表发生改变，则存储到数据库
                    if (!StringUtils.equalsIgnoreCase(oldOptimizerSystemAccount, updatedOptimizerSystemAccount)) {
                        planRelation.setOptimizerSystemAccount(updatedOptimizerSystemAccount);
                        associationRuleMapper.updatePlanRelation(planRelation);
                    }
                }
            }
        }
    }

    /**
     * 或许更新后的负责人列表。
     *
     * @param manager
     * @param managers
     * @return
     */
    private String updateMediumAccountManager(String manager, String managers) {
        if (StringUtils.isBlank(manager) || StringUtils.isBlank(managers) || !StringUtils.contains(managers, manager)) {
            return null;
        } else {
            return manager;
        }
    }

    /**
     * 获取媒体账户
     * <br><tt>产品</tt>
     * <br>如果产品ID为空，则查询该公司下所有产品
     * <br>如果只传一个媒体ID，则返回该公司下该媒体ID对应账户
     * <br>如果媒体ID为包含“,”的多个ID组成的字符串，则返回它们对应的所有账户
     * <tt>媒体</tt>
     * <br>如果媒体ID为空，则查询该公司下所选择的产品对应的所有媒体
     * <br>如果只传一个媒体ID，则返回媒体ID对应账户
     * <br>如果媒体ID为包含“,”的多个ID组成的字符串，则返回它们对应的所有账户
     *
     * @param companyId
     * @param productIds
     * @param mediumIds
     * @param permissionMediumAccountIds 媒体账号名称列表
     * @return 媒体账户列表
     * @author luzhuyou 2017/02/22
     * 
     * @param loginStatus 等于1，则表示只查询媒体授权登录的媒体帐号
     */
    public List<MediumAccountSelectorVo> queryForSelector(int companyId, String productIds, String mediumIds, Integer[] permissionMediumAccountIds,Integer loginStatus) {
        MediumAccountSelectorVo vo = new MediumAccountSelectorVo();
        vo.setCompanyId(companyId);
        if (null != productIds) {
            String[] productIdArr = productIds.split(",");
            vo.setProductIds(productIdArr);
        }
        if (null != mediumIds) {
            String[] mediumIdArr = mediumIds.split(",");
            vo.setMediumIds(mediumIdArr);
        }
        if (null != permissionMediumAccountIds) {
            vo.setPermissionMediumAccountIds(permissionMediumAccountIds);
        }
        if(loginStatus != null && loginStatus.intValue() == 1){
        	vo.setLoginStatus(11);
        }

        return mediumAccountMapper.queryForSelector(vo);
    }

    /**
     * 更新媒体账户表对应responsibleAccounts字段或followerAccounts字段
     * @param mediumAccountId
     * @param followerAccounts
     * @param responsibleAccounts
     * @return
     * @author luzhuyou 2017/02/22
     * @update ldj 2017-08-29
     */
    public void modifyResponsibleOrFollowerAccounts(int mediumAccountId, String followerAccounts, String responsibleAccounts) {
    	// 修改媒体账号
		MediumAccount account = new MediumAccount();
		account.setMediumAccountId(mediumAccountId);
		account.setUpdateTime(new Date());
		account.setFollowerAccounts(followerAccounts);
        account.setResponsibleAccounts(responsibleAccounts);
        mediumAccountMapper.update(account);
    }

    /**
     * 更新媒体账户关联关系表对应OptimizerSystemAccount字段
     *
     * @param accountRelation
     * @return
     * @author luzhuyou 2017/04/24
     */
 /*   public int modifyOptimizerSystemAccount(MediumAccountRelation accountRelation) {
        return mediumAccountRelationMapper.updateOptimizerSystemAccount(accountRelation);
    }*/

    /**
     * 根据媒体账号和产品获取优化师账号
     *
     * @param optimizerQuery
     * @return
     */
    public List<Item<String, String>> getResponsibleAccountsByAccount(OptimizerQuery optimizerQuery) {
        //String optimizers = mediumAccountRelationMapper.getOptimizersByAccountAndProduct(optimizerQuery);
        String optimizers = mediumAccountMapper.getResponsibleAccountsByAccount(optimizerQuery);
        List<Item<String, String>> items = new ArrayList<>();
        if (StringUtils.isNotBlank(optimizers)) {
            for (String optimizer : optimizers.split(Constant.Separator.COMMA)) {
                items.add(new Item<>(optimizer, optimizer));
            }
        }
        return items;
    }
    /**
     * 通过调用web服务进行密码加密
     * @param content  是需要加密的内容，支持GET/POST请求
     * @create ldj 2017-06-20
     * **/
    public String getEncryptPassword(String password) {
		//构建参数
		String params = HttpUtil.urlEncode(password);
		//HTTP请求获取数据
		String getPasswordInterface = ConfigHandler.getProperty(MediumAccountInterface.GET_ENCRYPT_PASSWORD_KEY, MediumAccountInterface.ENCRYPT_ENCODE);
		String httpResponse = HttpUtil.get(HttpUtil.urlParamReplace(getPasswordInterface, params));
		if(httpResponse == null){
			logger.info("调用加密接口请求失败！");
			ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
			return null;
		}
		JsonObject json = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
		String status = json.get("result").getAsString();
		String result = json.get("content").getAsString();
		if(!status.equalsIgnoreCase("success")){
			logger.info("调用加密接口请求异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{status, json});
			ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
			return null;
		}
    	return result;
	}
    
    /**
     * 
     * 添加媒体帐号关联关系
     * */
    public int addMediumAccountRelation(int companyId,int mediumAccountId,int mediumId,int productId) {
    	  MediumAccountRelation accountRelation = new MediumAccountRelation();
		  accountRelation.setCompanyId(companyId);
		  accountRelation.setMediumAccountId(mediumAccountId);
		  accountRelation.setMediumId(mediumId);
		  accountRelation.setProductId(productId);
		  accountRelation.setCreateTime(new Date());
		  accountRelation.setUpdateTime(new Date());
    	return mediumAccountRelationMapper.add(accountRelation);
	}
    
    /**
     * 
     * 删除媒体帐号关联关系
     * */
    public int deleteMediumAccountRelation(int companyId,Integer mediumAccountId,Integer mediumId,Integer productId) {
    	if(mediumAccountId != null || productId != null){
    	  MediumAccountRelation accountRelation = new MediumAccountRelation();
  		  accountRelation.setCompanyId(companyId);
  		  accountRelation.setMediumAccountId(mediumAccountId);
  		  accountRelation.setMediumId(mediumId);
  		  accountRelation.setProductId(productId);
      	 return mediumAccountRelationMapper.delete(accountRelation);
    	}
    	return 0;  
	}
    
   /**
    * 下拉框查询媒体帐号列表信息（包含媒体类型、媒体名称、媒体帐号列表、关注人或负责人）
    * @create ldj 2017-08-25
    * */
    public List<MediumGroupVo<MediumAndAccountVo>> MediumAccountGroup(int companyId,String accountName) {
    	List<MediumGroupVo<MediumAndAccountVo>> mediumGroupList = new ArrayList<MediumGroupVo<MediumAndAccountVo>>();
    	// 1-ADT已对接媒体
		MediumGroupVo<MediumAndAccountVo> adtMedium = new MediumGroupVo<MediumAndAccountVo>(); 
		adtMedium.setMediumType(MediumType.ADT);
		List<Medium> adtMediumList = mediumMapper.queryAdtMedium();
		List<MediumAndAccountVo> AccountVoList = getMediumAccountByMediumList(adtMediumList, companyId, accountName);
		adtMedium.setMediumList(AccountVoList);
		mediumGroupList.add(adtMedium);
		
		// 2-Tracking通用媒体
		MediumGroupVo<MediumAndAccountVo> trackingCommonMedium = new MediumGroupVo<MediumAndAccountVo>(); 
		trackingCommonMedium.setMediumType(MediumType.TRACKING_COMMON);
		List<Medium> trackingCommonMediumList = mediumMapper.queryTrackingCommonMedium();
		List<MediumAndAccountVo> AccountGroupTrackingMediumList = getMediumAccountByMediumList(trackingCommonMediumList, companyId, accountName);
		trackingCommonMedium.setMediumList(AccountGroupTrackingMediumList);
		mediumGroupList.add(trackingCommonMedium);
		
		// 3-自定义媒体
		MediumGroupVo<MediumAndAccountVo> customMedium = new MediumGroupVo<MediumAndAccountVo>(); 
		customMedium.setMediumType(MediumType.CUSTOM);
		List<Medium> customMediumList = mediumMapper.queryCustomMedium(companyId);
		List<MediumAndAccountVo> AccountGroupcustomMediumList = getMediumAccountByMediumList(customMediumList, companyId, accountName);
		customMedium.setMediumList(AccountGroupcustomMediumList);
		mediumGroupList.add(customMedium);
    	return mediumGroupList;
	}
    
    /***
     * 循环媒体列表，根据媒体id和adt帐号查找对应的投放媒体帐号
     * @create ldj 2017-08-25
     * 
     * */
    public List<MediumAndAccountVo> getMediumAccountByMediumList(List<Medium> mediumList,int companyId,String responsibleOrFollowerAccountNames) {
    	List<MediumAndAccountVo> result = new ArrayList<MediumAndAccountVo>(); 
    	for (Medium vo : mediumList) {
    		List<MediumAccount> accountList = getMediumAccountByMediumAndAccount(companyId,vo.getMediumId(), responsibleOrFollowerAccountNames);
    		if(accountList.size()>0){
    			MediumAndAccountVo groupVo = new MediumAndAccountVo();
    			groupVo.setMediumId(vo.getMediumId());
    			groupVo.setMediumName(vo.getMediumName());
    			groupVo.setMediumAccountList(accountList);
    			result.add(groupVo);
    		}
		}
    	return result;
	}

    
    /***
     * 根据媒体id和adt帐号查找对应的投放媒体帐号
     * @create ldj 2017-08-25
     * 
     * */
    public List<MediumAccount> getMediumAccountByMediumAndAccount(int companyId,Integer mediumId,String responsibleOrFollowerAccountNames) {
    	MediumAccount mediumAccount = new MediumAccount();
    	mediumAccount.setCompanyId(companyId);
    	mediumAccount.setMediumId(mediumId);
    	mediumAccount.setResponsibleAccounts(responsibleOrFollowerAccountNames);
    	return mediumAccountMapper.getMediumAccountByMediumAndAccount(mediumAccount);
	}
    

    /**
     * 根据媒体账号 获得负责人或关注人帐号字符串
     * 返回格式为 帐号名称(媒体名称)
     * @param companyId
     * @param responsibleAccounts
     * @param followerAccounts 
     * @return string  格式===   帐号名称(媒体名称)
     * @author ldj 2017-08-28
     */
    public MediumAccount getResponsibleFollowersGroupAccount(int companyId,String responsibleAccounts,String followerAccounts) {
    	MediumAccount mediumAccount = new MediumAccount();
    	mediumAccount.setCompanyId(companyId);
    	mediumAccount.setResponsibleAccounts(responsibleAccounts);
    	mediumAccount.setFollowerAccounts(followerAccounts);
    	return mediumAccountMapper.getResponsibleFollowersGroupAccount(mediumAccount);
	}
    
    /**
	 * 根据媒体账号ID获取媒体账号信息.
	 * @param mediumAccountQuery
	 * @return
	 */
    public MediumAccountVo getMediumAccount(Integer mediumId,Integer mediumAccountId) {
    	MediumAccountQuery mediumAccountQuery = new MediumAccountQuery();
    	mediumAccountQuery.setMediumAccountId(mediumAccountId);
    	mediumAccountQuery.setMediumId(mediumId);
    	return mediumAccountMapper.getMediumAccount(mediumAccountQuery);
	}
    
    /**
     * 根据媒体账号 获得负责人或关注人帐号字符串,媒体信息和媒体帐号信息
     * @param companyId
     * @param accountNames
     * @author ldj 2017-09-04
     */
    public List<MediumAccount> getResponsibleFollowersGroupMedium(int companyId,String responsibleAccounts,String followerAccounts,Integer productId) {
    	MediumAccount mediumAccount = new MediumAccount();
    	mediumAccount.setCompanyId(companyId);
    	mediumAccount.setResponsibleAccounts(responsibleAccounts);
    	mediumAccount.setFollowerAccounts(followerAccounts);
    	mediumAccount.setProductId(productId);
    	return mediumAccountMapper.getResponsibleFollowersGroupMedium(mediumAccount);
    }
    
    /**
     * 根据媒体id，将媒体帐号分组
     * @param List<MediumAccount>
     * @author ldj 2017-09-04
     * **/
    public List<MediumAccountGroupVo> getMediumAccountListGroupMedium(List<MediumAccount> list){
    	List<MediumAccountGroupVo> mediumGroupList = new ArrayList<MediumAccountGroupVo>();
    	for (MediumAccount vo : list) {
    		List<String> accountList = StringUtil.stringToListString(vo.getMediumAccountNames());
    		if(accountList.size()>0){
    			MediumAccountGroupVo mediumAccountGroupVo= new MediumAccountGroupVo();
        		mediumAccountGroupVo.setMediumName(vo.getMediumName());
        		mediumAccountGroupVo.setList(accountList);
        		mediumGroupList.add(mediumAccountGroupVo);
    		}
		}
    	return mediumGroupList;
    }
    
    /**
     * 根据媒体帐号列表，查询负责和关注的系统帐号和别名
     * @param
     * @create ldj 2017-09-04
     * **/
    public List<MediumAccountVo> queryMediumAccountAndAlias(List<MediumAccountVo> list) {
    	//循环媒体帐号列表，查找负责的系统帐号别名和关注的系统帐号别名
    	List<MediumAccountVo> result = new ArrayList<MediumAccountVo>();
		//key---accountName，value----accountNameAndAlias
		Map<String, String> accountNameAndAliasMap = new HashMap<String, String>();
		for (MediumAccountVo vo : list) {
			//负责的系统帐号别名
			String responsibleAccounts = vo.getResponsibleAccounts();
			if(StringUtils.isNotBlank(responsibleAccounts)){
				List<String> responsibleList = StringUtil.stringToListString(responsibleAccounts);
				for (String s : responsibleList) {
					if(StringUtils.isNotBlank(s)){
						String accountNameAndAlias = s;
						if(accountNameAndAliasMap.containsKey(s)){
							accountNameAndAlias = accountNameAndAliasMap.get(s);
						}else{
							//从数据库中查找
							SystemAccountVo accountVo = systemAccountService.get(s);
							if(accountVo != null){
								accountNameAndAlias = s+"("+accountVo.getAccountAlias()+")";
								accountNameAndAliasMap.put(s, accountNameAndAlias);
							}
						}
						responsibleAccounts = responsibleAccounts.replace(s, accountNameAndAlias);
					}
				}
			}
			vo.setResponsibleAccountNameAndAlias(responsibleAccounts==null?"":responsibleAccounts);
			//关注的系统帐号别名
			String followerAccounts = vo.getFollowerAccounts();
			if(StringUtils.isNotBlank(followerAccounts)){
				List<String> followerList = StringUtil.stringToListString(followerAccounts);
				for (String s : followerList) {
					if(StringUtils.isNotBlank(s)){
						String accountNameAndAlias = s;
						if(accountNameAndAliasMap.containsKey(s)){
							accountNameAndAlias = accountNameAndAliasMap.get(s);
						}else{
							//从数据库中查找
							SystemAccountVo accountVo = systemAccountService.get(s);
							if(accountVo != null){
								accountNameAndAlias = s+"("+accountVo.getAccountAlias()+")";
								accountNameAndAliasMap.put(s, accountNameAndAlias);
							}
						}
						followerAccounts = followerAccounts.replace(s, accountNameAndAlias);
					}
				}
			}
			vo.setFollowerAccountNameAndAlias(followerAccounts==null?"":followerAccounts);
			result.add(vo);
		}
		return result;
	}
    
    /**
     * 查询子账户负责或关注的媒体帐号
     * @author ldj 2017-11-23
     * @param responsibleAccounts
     * @paramfollowerAccounts 
     * @return
     */
    public List<Integer> getMediumAccountIdByRespOrFollowers(int companyId,String responsibleAccounts,String followerAccounts) {
    	MediumAccount mediumAccount = new MediumAccount();
    	mediumAccount.setCompanyId(companyId);
    	if(StringUtils.isNotBlank(responsibleAccounts)){
    		mediumAccount.setResponsibleAccounts(responsibleAccounts);
    	}
    	if(StringUtils.isNotBlank(followerAccounts)){
    		mediumAccount.setFollowerAccounts(followerAccounts);
    	}
    	return mediumAccountMapper.getMediumAccountIdByRespOrFollowers(mediumAccount);
    }
}
