package com.dataeye.ad.assistor.module.systemaccount.model;

import com.dataeye.ad.assistor.common.CachedObjects;


/**
 * <pre>
 * 统一登录接口请求完的响应对象
 * @author luzhuyou 2017/05/10
 */
public class PtLoginResponse<T> {
	/** 标识请求的唯一id: 时间戳+随机数 */
	private String id;
	/** 状态码:  200-成功；201-参数错误...（如果有其他异常，则累加）*/
	private int statusCode;
	/** 正文: 将返回的JSON数据保存在该字段内 */
	private T content;

	/**
	 * 把当前对象转换为json格式
	 *  @return  
	 *  @author luzhuyou 2017/03/13
	 */
	public String toJson() {
		return CachedObjects.GSON_ONLY_EXPOSE.toJson(this);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "TrackingResponse [id=" + id + ", statusCode=" + statusCode
				+ ", content=" + content + "]";
	}

}
