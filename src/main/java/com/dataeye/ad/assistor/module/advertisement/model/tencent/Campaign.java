package com.dataeye.ad.assistor.module.advertisement.model.tencent;


/**
 * 推广计划
 * Created by ldj
 */
public class Campaign {

    private Integer campaign_id;

    private String campaign_name;

    private String configured_status;

    private String campaign_type;

    private String product_type;

    private Integer daily_budget;

    private Integer budget_reach_date;

    private Integer created_time;

    private Integer last_modified_time;
    
    private String speed_mode;


    public Integer getCampaign_id() {
        return campaign_id;
    }

    public void setCampaign_id(Integer campaign_id) {
        this.campaign_id = campaign_id;
    }

    public String getCampaign_name() {
        return campaign_name;
    }

    public void setCampaign_name(String campaign_name) {
        this.campaign_name = campaign_name;
    }

    public Integer getDaily_budget() {
        return daily_budget;
    }

    public void setDaily_budget(Integer daily_budget) {
        this.daily_budget = daily_budget;
    }

    public Integer getBudget_reach_date() {
        return budget_reach_date;
    }

    public void setBudget_reach_date(Integer budget_reach_date) {
        this.budget_reach_date = budget_reach_date;
    }

    public Integer getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Integer created_time) {
        this.created_time = created_time;
    }

    public Integer getLast_modified_time() {
        return last_modified_time;
    }

    public void setLast_modified_time(Integer last_modified_time) {
        this.last_modified_time = last_modified_time;
    }

	public String getConfigured_status() {
		return configured_status;
	}

	public void setConfigured_status(String configured_status) {
		this.configured_status = configured_status;
	}

	public String getCampaign_type() {
		return campaign_type;
	}

	public void setCampaign_type(String campaign_type) {
		this.campaign_type = campaign_type;
	}

	public String getProduct_type() {
		return product_type;
	}

	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}

	public String getSpeed_mode() {
		return speed_mode;
	}

	public void setSpeed_mode(String speed_mode) {
		this.speed_mode = speed_mode;
	}
    
    
}
