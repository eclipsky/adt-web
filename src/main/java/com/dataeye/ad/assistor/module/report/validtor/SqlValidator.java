package com.dataeye.ad.assistor.module.report.validtor;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Pattern;

/**
 * Created by huangzehai on 2017/1/9.
 */
public final class SqlValidator {
    private static final String ASC = "asc";
    private static final String DESC = "desc";

    private SqlValidator() {

    }

    private static String FIELD_PATTERN = "^[\\w\\s_.,]+$";

    public static boolean isValidOrderBy(String orderBy) {
        return StringUtils.isBlank(orderBy) || Pattern.matches(FIELD_PATTERN, orderBy);
    }

    public static boolean isValidOrder(String order) {
        return StringUtils.isBlank(order) || StringUtils.equalsIgnoreCase(ASC, order) || StringUtils.equalsIgnoreCase(DESC, order);
    }

}
