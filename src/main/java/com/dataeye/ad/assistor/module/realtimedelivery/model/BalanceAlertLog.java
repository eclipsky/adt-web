package com.dataeye.ad.assistor.module.realtimedelivery.model;

import java.util.Date;

/**
 * Created by huangzehai on 2017/5/3.
 */
public class BalanceAlertLog {
    private String email;
    private long mediumAccountIdCrc;
    private Date updateTime;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getMediumAccountIdCrc() {
        return mediumAccountIdCrc;
    }

    public void setMediumAccountIdCrc(long mediumAccountIdCrc) {
        this.mediumAccountIdCrc = mediumAccountIdCrc;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "BalanceAlertLog{" +
                "email='" + email + '\'' +
                ", mediumAccountIdCrc=" + mediumAccountIdCrc +
                ", updateTime=" + updateTime +
                '}';
    }
}
