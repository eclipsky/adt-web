package com.dataeye.ad.assistor.module.company.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.company.mapper.TrackingCompanyMapper;
import com.dataeye.ad.assistor.module.company.model.TrackingCompany;

/**
 * Tracking公司Service
 * @author luzhuyou 2017/07/21
 *
 */
@Service("trackingCompanyService")
public class TrackingCompanyService {

	@Autowired
	private TrackingCompanyMapper trackingCompanyMapper;

	/**
	 * 新增Tracking产品账号信息
	 * @param company
	 * @return
	 */
	public int add(TrackingCompany company) {
		// 新增公司信息
		company.setCreateTime(new Date());
		company.setUpdateTime(new Date());
		trackingCompanyMapper.add(company);
		
		return company.getCompanyId();
	}

	/**
	 * 根据公司ID获取公司信息
	 * @param companyId
	 * @return
	 */
	public TrackingCompany get(int companyId) {
		TrackingCompany company = trackingCompanyMapper.get(companyId);
		return company;
	}

	/**
	 * 根据邮箱号获取公司信息
	 * @param email
	 * @return
	 */
	public TrackingCompany getByEmail(String email) {
		TrackingCompany company = trackingCompanyMapper.getByEmail(email);
		return company;
	}
	
	/**
	 * 根据UID获取公司信息
	 * @param uid
	 * @return
	 */
	public TrackingCompany getByUid(int uid) {
		TrackingCompany company = trackingCompanyMapper.getByUid(uid);
		return company;
	}

}
