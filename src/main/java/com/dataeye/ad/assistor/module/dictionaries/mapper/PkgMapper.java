package com.dataeye.ad.assistor.module.dictionaries.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.dictionaries.model.Pkg;

/**
 * 包表映射器.
 * Created by luzhuyou 2017/03/02
 */
@MapperScan
public interface PkgMapper {

	/**
	 * 查询包列表
	 * @param pkg
	 * @return
	 */
	public List<Pkg> query(Pkg pkg);

}
