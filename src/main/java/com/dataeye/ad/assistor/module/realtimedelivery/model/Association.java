package com.dataeye.ad.assistor.module.realtimedelivery.model;


import java.util.Date;

/**
 * Created by huangzehai on 2017/3/1.
 */
public class Association {
    private Long planId;
    private Long pageId;
    private Long packageId;
    private Date date;

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public Long getPackageId() {
        return packageId;
    }

    public void setPackageId(Long packageId) {
        this.packageId = packageId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
