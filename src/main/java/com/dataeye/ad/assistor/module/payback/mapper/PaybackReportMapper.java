package com.dataeye.ad.assistor.module.payback.mapper;

import com.dataeye.ad.assistor.module.payback.model.*;
import org.mybatis.spring.annotation.MapperScan;

import java.math.BigDecimal;
import java.util.List;

/**
 * 回本日报Mapper.
 * Created by huangzehai on 2017/1/13.
 */
@MapperScan
public interface PaybackReportMapper {



    /**
     * 回本日报.
     *
     * @param query 查询条件
     * @return
     */
    List<DailyPaybackReport> dailyPaybackReport(PaybackQuery query);

    /**
     * ltv分析中计算总充值
     *
     * @param query 查询条件
     * @return
     */
    List<DailyPaybackReport> dailyPaybackLtvReport(PaybackQuery query);

    /**
     * 日回本率趋势
     *
     * @param query 回本率趋势查询条件
     * @return
     */
    List<PaybackRate> dailyPaybackRateTrend(PaybackTrendQuery query);

    /**
     * 获取指定某一天的广告投放总消耗
     *
     * @param query 回本率趋势查询条件
     * @return
     */
    BigDecimal getTotalCostByDate(PaybackTrendQuery query);

    /**
     * 回本月报.
     *
     * @param query 查询条件
     * @return
     */
    List<MonthlyPaybackReport> monthlyPaybackReport(PaybackQuery query);

    /**
     * 获取某天的消耗和注册数账号数
     *
     * @param query
     * @return
     */
    Ltv getLtvByDate(PaybackTrendQuery query);
}
