package com.dataeye.ad.assistor.module.result.service;

import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlowQuery;
import com.dataeye.ad.assistor.module.naturalflow.service.NaturalFlowService;
import com.dataeye.ad.assistor.module.naturalflow.util.NaturalFlowUtils;
import com.dataeye.ad.assistor.module.result.mapper.CoreIndicatorMapper;
import com.dataeye.ad.assistor.module.result.model.*;
import com.dataeye.ad.assistor.module.result.util.IndicatorUtils;
import com.dataeye.ad.assistor.module.result.util.TrendChartUtils;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/6.
 */
@Service
public class CoreIndicatorServiceImpl implements CoreIndicatorService {


    @Autowired
    private CoreIndicatorMapper coreIndicatorMapper;

    @Autowired
    private NaturalFlowService naturalFlowService;

    /**
     * 获取总体核心指标.
     *
     * @return
     */
    @Override
    public OverallIndicator getOverallCoreIndicator(CoreIndicatorQuery coreIndicatorQuery) {
        CoreIndicator coreIndicator = coreIndicatorMapper.getTotalCost(coreIndicatorQuery);
        //表数据为空时
        if (coreIndicator == null) {
            coreIndicator = new CoreIndicator();
        }

        BigDecimal totalRecharge = coreIndicatorMapper.getTotalRecharge(coreIndicatorQuery);
        BigDecimal realRecharge = coreIndicatorMapper.getRealRecharge(coreIndicatorQuery);
        BigDecimal totalBalance = coreIndicatorMapper.getTotalBalance(coreIndicatorQuery);
        BigDecimal totalRegistrations = coreIndicatorMapper.getTotalRegistrations(coreIndicatorQuery);
        Long dau = coreIndicatorMapper.getDau(coreIndicatorQuery);


        //格式化数据
        if (coreIndicator != null) {
            //总体注册CPA
            if (coreIndicator.getCost() != null && totalRegistrations != null && totalRegistrations.compareTo(new BigDecimal(0)) > 0) {
                coreIndicator.setCpa(coreIndicator.getCost().divide(totalRegistrations, 4, RoundingMode.HALF_UP).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP));
            }

            //累计消耗
            coreIndicator.setCost(CurrencyUtils.fenToYuan(coreIndicator.getCost()));
            //媒体账户余额
            coreIndicator.setBalance(CurrencyUtils.fenToYuan(totalBalance));
            //累计充值
            coreIndicator.setRecharge(CurrencyUtils.fenToYuan(totalRecharge));

            //期间回本率
            coreIndicator.setPaybackRate(NumberUtils.rate(realRecharge, coreIndicator.getRealCost()));

            //DAU
            coreIndicator.setDau(dau);
        }
        return formatCoreIndicator(coreIndicator);
    }

    /**
     * 格式化核心指标
     *
     * @param coreIndicator
     * @return
     */
    private OverallIndicator formatCoreIndicator(CoreIndicator coreIndicator) {
        OverallIndicator overallIndicator = new OverallIndicator();
        overallIndicator.setCpa(coreIndicator.getCpa());
        overallIndicator.setDau(coreIndicator.getDau());
        overallIndicator.setPaybackRate(coreIndicator.getPaybackRate());
        overallIndicator.setCost(CurrencyUtils.yuanToWan(coreIndicator.getCost()));
        overallIndicator.setRecharge(CurrencyUtils.yuanToWan(coreIndicator.getRecharge()));
        overallIndicator.setBalance(CurrencyUtils.yuanToWan(coreIndicator.getBalance()));
        return overallIndicator;
    }


    /**
     * 按媒体分组统计消耗
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getCostGroupByMedium(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getCostGroupByMedium(coreIndicatorQuery);
        IndicatorUtils.formatIndicators(indicators);
        return indicators;
    }

    /**
     * 按媒体分组统计充值
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getRechargeGroupByMedium(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getRechargeGroupByMedium(coreIndicatorQuery);
        //添加自然流量
        if (!coreIndicatorQuery.isFilterNaturalFlow()) {
            BigDecimal naturalFlowRecharge = naturalFlowService.getNaturalFlowRecharge(coreIndicatorQuery);
            if (naturalFlowRecharge != null) {
                Indicator naturalFlowIndicator = new Indicator();
                naturalFlowIndicator.setName(Labels.NATURAL_FLOW_OF_ALL_MEDIUMS);
                naturalFlowIndicator.setValue(naturalFlowRecharge);
                indicators.add(naturalFlowIndicator);
            }
        }
        IndicatorUtils.formatIndicators(indicators);
        IndicatorUtils.renameOthers(indicators);
        return indicators;
    }

    /**
     * 按媒体分组统计回本率
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getPaybackRateGroupByMedium(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getPaybackRateGroupByMedium(coreIndicatorQuery);
        //自然流量
        if (!coreIndicatorQuery.isFilterNaturalFlow()) {
            Double paybackRate = naturalFlowService.getNaturalFlowPaybackRate(coreIndicatorQuery);
            if (paybackRate != null) {
                Indicator indicator = new Indicator();
                indicator.setName(Labels.NATURAL_FLOW_OF_ALL_MEDIUMS);
                indicator.setValue(new BigDecimal(paybackRate));
                indicators.add(indicator);
            }
        }
        return indicators;
    }

    /**
     * 按媒体分组统计余额
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getBalanceGroupByMedium(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getBalanceGroupByMedium(coreIndicatorQuery);
        IndicatorUtils.formatIndicators(indicators);
        return indicators;
    }

    /**
     * 按媒体分组统计CPA
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getCpaGroupByMedium(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getCpaGroupByMedium(coreIndicatorQuery);
        //添加自然流量
        if (!coreIndicatorQuery.isFilterNaturalFlow()) {
            NaturalFlow naturalFlow = naturalFlowService.getNaturalFlowTotal(toNaturalFlowQuery(coreIndicatorQuery));
            BigDecimal totalCost = naturalFlowService.getTotalCost(coreIndicatorQuery);
            if (naturalFlow != null && naturalFlow.getRegistrations() != null && naturalFlow.getRegistrations() > 0 && totalCost != null) {
                Indicator indicator = new Indicator();
                indicator.setName(Labels.NATURAL_FLOW_OF_ALL_MEDIUMS);
                indicator.setValue(totalCost.divide(new BigDecimal(naturalFlow.getRegistrations()), 2, RoundingMode.HALF_UP));
                indicators.add(indicator);
            }
        }
        IndicatorUtils.formatIndicators(indicators);
        return indicators;
    }

    private NaturalFlowQuery toNaturalFlowQuery(CoreIndicatorQuery query) {
        NaturalFlowQuery naturalFlowQuery = new NaturalFlowQuery();
        naturalFlowQuery.setCompanyId(query.getCompanyId());
        naturalFlowQuery.setPermissionMediumAccountIds(query.getPermissionMediumAccountIds());
        naturalFlowQuery.setPermissionSystemAccounts(query.getPermissionSystemAccounts());
        naturalFlowQuery.setStartDate(query.getStartDate());
        naturalFlowQuery.setEndDate(query.getEndDate());
        return naturalFlowQuery;
    }

    /**
     * 按媒体分组统计DAU
     *
     * @param query
     * @return
     */
    @Override
    public List<Indicator> getDauGroupByMedium(CoreIndicatorQuery query) {
        List<Indicator> indicators = coreIndicatorMapper.getDauGroupByMedium(query);
        if (!query.isFilterNaturalFlow()) {
            Integer dau = naturalFlowService.getNaturalFlowDau(query);
            if (dau != null) {
                Indicator indicator = new Indicator();
                indicator.setName(Labels.NATURAL_FLOW_OF_ALL_MEDIUMS);
                indicator.setValue(new BigDecimal(dau));
                indicators.add(indicator);
            }
        }
        //添加自然流量
        return indicators;
    }

    /**
     * 按产品分组统计消耗
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getCostGroupByProduct(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getCostGroupByProduct(coreIndicatorQuery);
        IndicatorUtils.formatIndicators(indicators);
        IndicatorUtils.renameOthers(indicators);
        return indicators;
    }

    /**
     * 按产品分组统计充值
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getRechargeGroupByProduct(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getRechargeGroupByProduct(coreIndicatorQuery);
        //添加自然流量
        if (!coreIndicatorQuery.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowRechargeGroupByProduct(coreIndicatorQuery);
            if (naturalFlows != null) {
                for (NaturalFlow naturalFlow : naturalFlows) {
                    IndicatorWithId naturalFlowIndicator = new IndicatorWithId();
                    naturalFlowIndicator.setName(NaturalFlowUtils.formatProductName(naturalFlow.getProductName()));
                    naturalFlowIndicator.setValue(naturalFlow.getAccumulatedPayingAmount());
                    indicators.add(naturalFlowIndicator);
                }
            }
        }
        IndicatorUtils.formatIndicators(indicators);
        IndicatorUtils.renameOthers(indicators);
        return indicators;
    }

    /**
     * 按产品分组统计回本率.
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getPaybackRateGroupByProduct(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getPaybackRateGroupByProduct(coreIndicatorQuery);
        //添加自然流量
        if (!coreIndicatorQuery.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowPaybackRateGroupByProduct(coreIndicatorQuery);
            if (naturalFlows != null) {
                for (NaturalFlow naturalFlow : naturalFlows) {
                    Indicator naturalFlowIndicator = new Indicator();
                    naturalFlowIndicator.setName(NaturalFlowUtils.formatProductName(naturalFlow.getProductName()));
                    naturalFlowIndicator.setValue(new BigDecimal(naturalFlow.getPaybackRate()));
                    indicators.add(naturalFlowIndicator);
                }
            }
        }
        IndicatorUtils.renameOthers(indicators);
        return indicators;
    }

    /**
     * 按产品分组统计余额
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getBalanceGroupByProduct(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getBalanceGroupByProduct(coreIndicatorQuery);
        IndicatorUtils.formatIndicators(indicators);
        IndicatorUtils.renameOthers(indicators);
        return indicators;
    }

    /**
     * 按产品分组统计CPA
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getCpaGroupByProduct(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getCpaGroupByProduct(coreIndicatorQuery);
        //添加自然流量
        if (!coreIndicatorQuery.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlowCpa = naturalFlowService.getNaturalFlowCpaGroupByProduct(toNaturalFlowQuery(coreIndicatorQuery));
            if (naturalFlowCpa != null) {
                for (NaturalFlow naturalFlow : naturalFlowCpa) {
                    Indicator naturalFlowIndicator = new Indicator();
                    naturalFlowIndicator.setName(NaturalFlowUtils.formatProductName(naturalFlow.getProductName()));
                    naturalFlowIndicator.setValue(naturalFlow.getCpa());
                    indicators.add(naturalFlowIndicator);
                }
            }
        }
        IndicatorUtils.formatIndicators(indicators);
        IndicatorUtils.renameOthers(indicators);
        return indicators;
    }

    /**
     * 按产品分组统计DAU
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getDauGroupByProduct(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getDauGroupByProduct(coreIndicatorQuery);
        //添加自然流量
        if (!coreIndicatorQuery.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowDauGroupByProduct(coreIndicatorQuery);
            if (naturalFlows != null) {
                for (NaturalFlow naturalFlow : naturalFlows) {
                    Indicator naturalFlowIndicator = new Indicator();
                    naturalFlowIndicator.setName(NaturalFlowUtils.formatProductName(naturalFlow.getProductName()));
                    naturalFlowIndicator.setValue(new BigDecimal(naturalFlow.getDau()));
                    indicators.add(naturalFlowIndicator);
                }
            }
        }
        IndicatorUtils.renameOthers(indicators);
        return indicators;
    }

    /**
     * 按平台分组统计消耗
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getCostGroupByOs(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getCostGroupByOs(coreIndicatorQuery);
        replaceWithOsName(indicators);
        IndicatorUtils.formatIndicators(indicators);
        return indicators;
    }

    /**
     * 按平台分组统计充值
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getRechargeGroupByOs(DataPermissionDomain coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getRechargeGroupByOs(coreIndicatorQuery);
        replaceWithOsName(indicators);
        IndicatorUtils.formatIndicators(indicators);
        return indicators;
    }

    /**
     * 按平台分组统计回本率
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getPaybackRateGroupByOs(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getPaybackRateGroupByOs(coreIndicatorQuery);
        replaceWithOsName(indicators);
        return indicators;
    }

    /**
     * 按平台分组统计CPA
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getCpaGroupByOs(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getCpaGroupByOs(coreIndicatorQuery);
        replaceWithOsName(indicators);
        IndicatorUtils.formatIndicators(indicators);
        return indicators;
    }

    /**
     * 按平台分组统计DAU
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public List<Indicator> getDauGroupByOs(CoreIndicatorQuery coreIndicatorQuery) {
        List<Indicator> indicators = coreIndicatorMapper.getDauGroupByOs(coreIndicatorQuery);
        replaceWithOsName(indicators);
        return indicators;
    }

    /**
     * 按媒体分组统计日消耗趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyCostTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyCostTrendGroupByMedium(coreIndicatorQuery);
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, indicators);
    }

    /**
     * 按媒体分组统计充值日趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyRechargeTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyRechargeTrendGroupByMedium(coreIndicatorQuery);
        //添加自然流量
        if (!coreIndicatorQuery.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowRechargeGroupByDate(coreIndicatorQuery);
            if (naturalFlows != null) {
                for (NaturalFlow naturalFlow : naturalFlows) {
                    DailyIndicator indicator = new DailyIndicator();
                    indicator.setName(Labels.NATURAL_FLOW_OF_ALL_MEDIUMS);
                    indicator.setDate(naturalFlow.getStatDate());
                    indicator.setValue(naturalFlow.getAccumulatedPayingAmount());
                    indicators.add(indicator);
                }
            }
        }
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, indicators);
    }

    /**
     * 按媒体分组统计日余额趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyBalanceTrendGroupByMedium(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyBalanceTrendGroupByMedium(coreIndicatorQuery);
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, indicators);
    }

    /**
     * 按媒体分组分组统计日CPA趋势图
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyCpaTrendGroupByMedium(CoreIndicatorQuery query) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyCpaTrendGroupByMedium(query);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            List<DailyIndicator> naturalFlowIndicators = naturalFlowService.getNaturalFlowCpaGroupByDate(toNaturalFlowQuery(query));
            if (naturalFlowIndicators != null) {
                indicators.addAll(naturalFlowIndicators);
            }
        }
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(query, indicators);
    }

    /**
     * 按媒体分组统计冲击DAU日趋势图
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyDauTrendGroupByMedium(CoreIndicatorQuery query) {
        List<DailyIndicator> dailyIndicators = coreIndicatorMapper.dailyDauTrendGroupByMedium(query);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            List<DailyIndicator> naturalFlows = naturalFlowService.getNaturalFlowDauGroupByDate(query);
            if (naturalFlows != null) {
                dailyIndicators.addAll(naturalFlows);
            }
        }
        return TrendChartUtils.toTrendChart(query, dailyIndicators);
    }

    /**
     * 按产品分组统计日消耗趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyCostTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyCostTrendGroupByProduct(coreIndicatorQuery);
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, indicators);
    }

    /**
     * 按产品分组统计日充值趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyRechargeTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyRechargeTrendGroupByProduct(coreIndicatorQuery);
        //添加自然流量
        if (!coreIndicatorQuery.isFilterNaturalFlow()) {
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowRechargeGroupByProductAndDate(coreIndicatorQuery);
            if (naturalFlows != null) {
                for (NaturalFlow naturalFlow : naturalFlows) {
                    DailyIndicator indicator = new DailyIndicator();
                    indicator.setName(NaturalFlowUtils.formatProductName(naturalFlow.getProductName()));
                    indicator.setDate(naturalFlow.getStatDate());
                    indicator.setValue(naturalFlow.getAccumulatedPayingAmount());
                    indicators.add(indicator);
                }
            }
        }
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, indicators);
    }

    /**
     * 按产品分组统计日余额趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyBalanceTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyBalanceTrendGroupByProduct(coreIndicatorQuery);
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, indicators);
    }

    /**
     * 按产品分组统计日CPA趋势图
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyCpaTrendGroupByProduct(CoreIndicatorQuery query) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyCpaTrendGroupByProduct(query);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            List<DailyIndicator> naturalFlowIndicators = naturalFlowService.getNaturalFlowCpaGroupByProductAndDate(query);
            if (naturalFlowIndicators != null) {
                indicators.addAll(naturalFlowIndicators);
            }
        }
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(query, indicators);
    }

    /**
     * 按产品分组统计日DAU趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyDauTrendGroupByProduct(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> dailyIndicators = coreIndicatorMapper.dailyDauTrendGroupByProduct(coreIndicatorQuery);
        //添加自然流量
        if (!coreIndicatorQuery.isFilterNaturalFlow()) {
            List<DailyIndicator> naturalFlows = naturalFlowService.getNaturalFlowDauGroupByProductAndDate(coreIndicatorQuery);
            if (naturalFlows != null) {
                dailyIndicators.addAll(naturalFlows);
            }
        }
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, dailyIndicators);
    }

    /**
     * 按平台分组统计消耗日趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyCostTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyCostTrendGroupByOs(coreIndicatorQuery);
        replaceWithOsName(indicators);
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, indicators);
    }

    /**
     * 按平台分组统计日充值趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyRechargeTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyRechargeTrendGroupByOs(coreIndicatorQuery);
        replaceWithOsName(indicators);
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, indicators);
    }

    /**
     * 按平台分组统计日CPA趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyCpaTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyCpaTrendGroupByOs(coreIndicatorQuery);
        replaceWithOsName(indicators);
        IndicatorUtils.formatIndicators(indicators);
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, indicators);
    }

    /**
     * 按平台分组统计日DAU趋势图
     *
     * @param coreIndicatorQuery
     * @return
     */
    @Override
    public TrendChart<BigDecimal> dailyDauTrendGroupByOs(CoreIndicatorQuery coreIndicatorQuery) {
        List<DailyIndicator> indicators = coreIndicatorMapper.dailyDauTrendGroupByOs(coreIndicatorQuery);
        replaceWithOsName(indicators);
        return TrendChartUtils.toTrendChart(coreIndicatorQuery, indicators);
    }

    /**
     * 重名系统名称
     *
     * @param indicators
     */
    private void replaceWithOsName(List<? extends Indicator> indicators) {
        for (Indicator indicator : indicators) {
            replaceWithOsName(indicator);
        }
    }

    /**
     * 重命名系统名称
     *
     * @param indicator
     */
    private void replaceWithOsName(Indicator indicator) {
        if (indicator != null && indicator.getName() != null) {
            OsType osType = OsType.parse(Integer.valueOf(indicator.getName()));
            if (osType != null) {
                indicator.setName(osType.name());
            }
        }
    }

}
