package com.dataeye.ad.assistor.module.payback.model;

import com.dataeye.ad.assistor.module.report.constant.Defaults;
import com.dataeye.ad.assistor.module.report.model.DateRangeQuery;

import java.util.List;

/**
 * 回本日报查询条件.
 * Created by huangzehai on 2017/1/13.
 */
public class PaybackQuery extends DateRangeQuery {
    /**
     * 媒体ID列表.
     */
    private List<Integer> mediumIds;
    /**
     * 媒体账号ID列表.
     */
    private List<Integer> accountIds;

    /**
     * 产品ID列表.
     */
    private List<Integer> productIds;

    /**
     * 计划ID列表
     *
     * @return
     */
    private List<Integer> planIds;

    /**
     * 限制查询返回的记录数.
     */
    private int size;
    
    /**
     * 指标
     * */
    private String indicator; 
    
    /**
     * 时段.
     */
    private Period period;

    /**
     * 是否过滤自然流量.
     */
    private boolean filterNaturalFlow;
    
    /**
     * 当前页。
     */
    private int currentPage = Defaults.CURRENT_PAGE;
    /**
     * 分页大小。
     */
    private int pageSize = Defaults.PAGE_SIZE;
    
    private String order;

    public List<Integer> getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(List<Integer> mediumIds) {
        this.mediumIds = mediumIds;
    }

    public List<Integer> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<Integer> accountIds) {
        this.accountIds = accountIds;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public List<Integer> getPlanIds() {
        return planIds;
    }

    public void setPlanIds(List<Integer> planIds) {
        this.planIds = planIds;
    }

    public boolean isFilterNaturalFlow() {
        return filterNaturalFlow;
    }

    public void setFilterNaturalFlow(boolean filterNaturalFlow) {
        this.filterNaturalFlow = filterNaturalFlow;
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "PaybackQuery [mediumIds=" + mediumIds + ", accountIds="
				+ accountIds + ", productIds=" + productIds + ", planIds="
				+ planIds + ", size=" + size + ", indicator=" + indicator
				+ ", period=" + period + ", filterNaturalFlow="
				+ filterNaturalFlow + ", currentPage=" + currentPage
				+ ", pageSize=" + pageSize + ", order=" + order + "]";
	}
}
