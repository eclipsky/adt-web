package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 实时操盘指标涨跌
 * Created by huangzehai on 2017/7/4.
 */
public class IndicatorUpsAndDowns {
    /**
     * 消耗
     */
    @Expose
    @SerializedName("totalCost")
    private UpsAndDowns cost;
    /**
     * CTR.
     */
    @Expose
    private UpsAndDowns ctr;
    /**
     * 下载率
     */
    @Expose
    private UpsAndDowns downloadRate;
    /**
     * 激活CPA.
     */
    @Expose
    private UpsAndDowns activationCpa;
    /**
     * 注册Cpa.
     */
    @Expose
    @SerializedName("costPerRegistration")
    private UpsAndDowns registrationCpa;

    public UpsAndDowns getCost() {
        return cost;
    }

    public void setCost(UpsAndDowns cost) {
        this.cost = cost;
    }

    public UpsAndDowns getCtr() {
        return ctr;
    }

    public void setCtr(UpsAndDowns ctr) {
        this.ctr = ctr;
    }

    public UpsAndDowns getDownloadRate() {
        return downloadRate;
    }

    public void setDownloadRate(UpsAndDowns downloadRate) {
        this.downloadRate = downloadRate;
    }

    public UpsAndDowns getActivationCpa() {
        return activationCpa;
    }

    public void setActivationCpa(UpsAndDowns activationCpa) {
        this.activationCpa = activationCpa;
    }

    public UpsAndDowns getRegistrationCpa() {
        return registrationCpa;
    }

    public void setRegistrationCpa(UpsAndDowns registrationCpa) {
        this.registrationCpa = registrationCpa;
    }

    @Override
    public String toString() {
        return "IndicatorUpsAndDowns{" +
                "cost=" + cost +
                ", ctr=" + ctr +
                ", downloadRate=" + downloadRate +
                ", activationCpa=" + activationCpa +
                ", registrationCpa=" + registrationCpa +
                '}';
    }
}
