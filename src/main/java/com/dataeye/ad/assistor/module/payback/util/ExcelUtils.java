package com.dataeye.ad.assistor.module.payback.util;

import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.module.payback.model.DailyPaybackReportView;
import com.dataeye.ad.assistor.module.payback.model.MonthlyPaybackReportView;
import com.dataeye.ad.assistor.util.StringUtil;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.List;

/**
 * Created by huangzehai on 2017/1/16.
 */
public final class ExcelUtils {

    /**
     * 日回本率表头字段名称.
     */
    private static final String[] DAILY_PAYBACK_REPORT_COLUMN_NAMES = {"日期", "消耗(折前)", "注册账号数","首日注册CPA(设备)","充值账号数","充值金额","累计充值","付费率","ARPU", "ARPPU", "首日回本率","首日回本金额","2日回本率","2日回本金额", "3日回本率","3日回本金额","4日回本率","4日回本金额","5日回本率","5日回本金额","6日回本率","6日回本金额", "7日回本率","7日回本金额", "15日回本率","15日回本金额", "30日回本率","30日回本金额", "60日回本率","60日回本金额", "90日回本率","90日回本金额"};
    /**
     * 月回本率表头字段名称.
     */
    private static final String[] MONTHLY_PAYBACK_REPORT_COLUMN_NAMES = {"日期", "累计充值","消耗", "新增注册","APRU","注册CPA", "当月回本率", "前2月回本率", "前3月回本率", "前4月回本率", "前5月回本率", "前6月回本率", "前7月回本率"};

    public static Workbook buildDailyPaybackReport(List<DailyPaybackReportView> reports) {
        HSSFWorkbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("日回本率");
        addHead(sheet, DAILY_PAYBACK_REPORT_COLUMN_NAMES);
        Row row;

        int rowIndex = 1;
        for (DailyPaybackReportView report : reports) {
            row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getDate()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getTotalCost()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getRegisterAccountNum()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getRegistrationCpa()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getTotalRecharges()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getTotalRechargeAmount()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getAccumulatedRecharge()));
            String payRate = Labels.UNKNOWN;
            if(report.getPayRate() != null && !report.getPayRate().equals(Labels.UNKNOWN)){
            	payRate = formatAsPercent(Double.parseDouble(report.getPayRate()));
            }
            row.createCell(columnIndex++).setCellValue(payRate);
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getArpu()));
            
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getArppu()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter1days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount1Day()));
            
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter2days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount2Day()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter3days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount3Day()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter4days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount4Day()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter5days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount5Day()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter6days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount6Day()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter7days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount7Day()));
            //row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getRegistrations()));
            //row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getCpa()));
            //row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getDau()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter15days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount15Day()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter30days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount30Day()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter60days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount60Day()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter90days()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getPayAmount90Day()));
        }
        return wb;
    }

    public static void addHead(Sheet sheet, String[] columnNames) {
        //添加表头
        Row row = sheet.createRow(0);
        int columnIndex = 0;
        for (String columnName : columnNames) {
            row.createCell(columnIndex++).setCellValue(columnName);
        }
    }

    /**
     * 构建月回本报告表格
     *
     * @param reports
     * @return
     */
    public static Workbook buildMonthlyPaybackReport(List<MonthlyPaybackReportView> reports) {
        HSSFWorkbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("月回本率");
        addHead(sheet, MONTHLY_PAYBACK_REPORT_COLUMN_NAMES);
        Row row;

        int rowIndex = 1;
        for (MonthlyPaybackReportView report : reports) {
            row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getDate()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getTotalRechargeAmount()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getTotalCost()));
           // row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getRegistrations()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getArpu()));
            //row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(report.getCpa()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter1Month()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter2Months()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter3Months()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter4Months()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter5Months()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter6Months()));
            row.createCell(columnIndex++).setCellValue(formatAsPercent(report.getPaybackRateAfter7Months()));
        }
        return wb;
    }
    

  /**
   * 格式化为百分比
   *
   * @param rate
   * @return
   */
  private static String formatAsPercent(Double rate) {
      if (rate == null) {
          return Labels.UNKNOWN;
      } else {
          NumberFormat nf = NumberFormat.getPercentInstance();
          //保留两位小数
          nf.setMinimumFractionDigits(2);
          //四舍五入
          nf.setRoundingMode(RoundingMode.HALF_UP);
          return nf.format(rate);
      }
  }
    
}
