//package com.dataeye.ad.assistor.module.mediumaccount.model;
//
//import com.google.gson.annotations.Expose;
//
//import java.util.Date;
//
///**
// * <pre>
// * The type Account info.
// * @author Stran <br>
// * @version 1.0 <br>
// * @since 2016.11.23 19:21:01
// */
//public class MediumAccountOldVo {
//
//    @Expose
//    private int id;
//    @Expose
//    private int accountType;
//    @Expose
//    private String accountTypeName;
//    @Expose
//    private int mediumId;
//    @Expose
//    private String mediumName;
//    @Expose
//    private String mediumHomePage;
//    @Expose
//    private String accountAlias;
//    @Expose
//    private String account;
//    @Expose
//    private int loginStatus;
//    private String password;
//    private String skey;
//    private String extend;
//    @Expose
//    private String alertEmails;
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public int getAccountType() {
//        return accountType;
//    }
//
//    public void setAccountType(int accountType) {
//        this.accountType = accountType;
//    }
//
//    public String getAccountTypeName() {
//        return accountTypeName;
//    }
//
//    public void setAccountTypeName(String accountTypeName) {
//        this.accountTypeName = accountTypeName;
//    }
//
//    public int getMediumId() {
//        return mediumId;
//    }
//
//    public void setMediumId(int mediumId) {
//        this.mediumId = mediumId;
//    }
//
//    public String getMediumName() {
//        return mediumName;
//    }
//
//    public void setMediumName(String mediumName) {
//        this.mediumName = mediumName;
//    }
//
//    public String getMediumHomePage() {
//        return mediumHomePage;
//    }
//
//    public void setMediumHomePage(String mediumHomePage) {
//        this.mediumHomePage = mediumHomePage;
//    }
//
//    public String getAccountAlias() {
//        return accountAlias;
//    }
//
//    public void setAccountAlias(String accountAlias) {
//        this.accountAlias = accountAlias;
//    }
//
//    public String getAccount() {
//        return account;
//    }
//
//    public void setAccount(String account) {
//        this.account = account;
//    }
//
//    public int getLoginStatus() {
//        return loginStatus;
//    }
//
//    public void setLoginStatus(int loginStatus) {
//        this.loginStatus = loginStatus;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getSkey() {
//        return skey;
//    }
//
//    public void setSkey(String skey) {
//        this.skey = skey;
//    }
//
//    public String getExtend() {
//        return extend;
//    }
//
//    public void setExtend(String extend) {
//        this.extend = extend;
//    }
//
//    public String getAlertEmails() {
//        return alertEmails;
//    }
//
//    public void setAlertEmails(String alertEmails) {
//        this.alertEmails = alertEmails;
//    }
//
//}
