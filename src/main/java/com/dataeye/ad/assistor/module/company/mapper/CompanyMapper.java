package com.dataeye.ad.assistor.module.company.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.company.model.Company;

/**
 * 公司信息映射器.
 * Created by luzhuyou
 */
@MapperScan
public interface CompanyMapper {
	
	/**
	 * 新增公司信息
	 * @param company
	 * @return
	 */
	public int add(Company company);

	/**
	 * 根据公司ID获取公司信息
	 * @param companyId
	 * @return
	 */
	public Company get(int companyId);

	/**
	 * 根据邮箱号获取公司信息
	 * @param email
	 * @return
	 */
	public Company getByEmail(String email);
	
	/**
	 * 根据UID获取公司信息
	 * @param uid
	 * @return
	 */
	public Company getByUid(int uid);
}
