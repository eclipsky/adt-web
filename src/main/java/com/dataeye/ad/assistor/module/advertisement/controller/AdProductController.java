package com.dataeye.ad.assistor.module.advertisement.controller;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement;
import com.dataeye.ad.assistor.module.advertisement.service.AppProductService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lingliqi
 * @date 2017-12-23 11:20
 */
@Controller
public class AdProductController {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(AdProductController.class);


    @Autowired
    private AppProductService appProductService;

    /**
     * 拉取推广APP详情
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amProduct/queryProduct.do")
    public Object queryProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("拉取推广APP详情信息");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        String productType = parameter.getParameter("productType");
        String productRefsId = parameter.getParameter("productRefsId");
        String appId = parameter.getParameter("appId");
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        if (StringUtils.isBlank(productType) || Advertisement.ProductType.parse(Integer.parseInt(productType)) == null) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
        }
        if (StringUtils.isBlank(productRefsId) && StringUtils.isBlank(appId)) {
        	ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_PRODUCT_REFS_ID_IS_NULL);
        }
/*        if (StringUtils.isBlank(appId)) {
        	ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ID_ERROR);
        }
*/        
        return appProductService.queryProduct(Integer.parseInt(mediumAccountId), Advertisement.ProductType.parse(Integer.parseInt(productType)).name(), productRefsId, StringUtils.isBlank(appId)?null:appId);


    }
}
