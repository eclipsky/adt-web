package com.dataeye.ad.assistor.module.realtimedelivery.mapper;

import com.dataeye.ad.assistor.common.DailyRecord;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.module.mediumaccount.model.Medium;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountSelectorVo;
import com.dataeye.ad.assistor.module.realtimedelivery.model.*;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.math.BigDecimal;
import java.util.List;

/**
 * 账户余额Mapper.
 * Created by huangzehai on 2017/4/18.
 */
public interface BalanceMapper {
    /**
     * 列出账户余额
     */
    List<Balance> listAccountBalance(BalanceQuery balanceQuery);

    /**
     * 列出账户最近7日平均花费
     */
    List<Cost> listAccountAverageCost(DataPermissionDomain dataPermissionDomain);

    /**
     * 账户余额趋势图
     */
    List<DailyRecord<String, BigDecimal>> balanceTrend(BalanceTrendQuery balanceTrendQuery);

    /**
     * 账户汇总余额趋势图
     */
    List<DailyRecord<String, BigDecimal>> totalBalanceTrend(BalanceTrendQuery balanceTrendQuery);

    /**
     * 获取负责人管理的媒体账户列表
     *
     * @param mediumAccountQuery
     * @return
     */
    List<MediumAccountSelectorVo> listManagingMediumAccounts(MediumAccountQuery mediumAccountQuery);

    /**
     * 获取负责人管理的媒体列表
     * @param mediumQuery
     * @return
     */
    List<Medium> listManagingMediums(UserInSession userInSession);
}
