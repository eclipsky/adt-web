package com.dataeye.ad.assistor.module.dictionaries.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.module.dictionaries.model.Page;
import com.dataeye.ad.assistor.module.dictionaries.service.PageService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.github.pagehelper.PageHelper;

/**
 * Created by luzhuyou
 * 落地页控制器.
 */
@Controller
public class PageController {
	
    @Autowired
    private PageService pageService;

    /**
     * 下拉框查询落地页
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/page/queryPageForSelector.do")
    public Object queryPageForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        
    	int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
    	String pageName = parameter.getParameter("pageName");
    	if(StringUtils.isBlank(pageName)) {
    		pageName = null;
        }
    	
    	//PageHelper.startPage(parameter.getPageId(), parameter.getPageSize());
        List<Page> result = pageService.queryByCompany(companyId,pageName);
        if(result == null) {
        	result = new ArrayList<Page>();
		}
        if(StringUtils.isBlank(pageName)) {
        	// 在落地页下拉框值中第一行添加“无”的记录
            Page page = new Page();
            page.setPageId(0);
            page.setPageName("无");
            result.add(0, page);
        }
        
        PageData pageData = PageDataHelper.getPageDataByList(parameter.getPageSize(), parameter.getPageId(), result);
        return pageData;
    }

}
