package com.dataeye.ad.assistor.module.realtimedelivery.service;

import ch.qos.logback.classic.Logger;
import com.dataeye.ad.assistor.module.realtimedelivery.mapper.AlertConfMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.mapper.RealTimeDeliveryMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.model.*;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.dataeye.ad.assistor.util.MailUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by huangzehai on 2017/2/28.
 */
@Service
public class AlertServiceImpl implements AlertService {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(AlertServiceImpl.class);
    /**
     * 已设置告警.
     */
    private static final int HAS_ALERT = 1;

    private final int RED = 1;

    private final int EMAIL_ALERT_FALSE = 0;

    private final int EMAIL_ALERT_TRUE = 1;

    @Autowired
    private AlertConfMapper alertConfMapper;

    @Autowired
    private RealTimeReportService realTimeReportService;

    @Autowired
    private RealTimeDeliveryMapper realTimeDeliveryMapper;

    /**
     * 给实时统计结果设置告警
     *
     * @param realTimeStats
     * @Param offset 添加自然流量后引起的列表偏移量
     */
    @Override
    public void setAlert(List<RealTimeDeliveryFormatVo> realTimeStats, List<RealTimeDeliveryVo> realTimeDeliveryList, DataPermissionDomain dataPermissionDomain, int offset) {
        if (realTimeDeliveryList != null) {
            Map<Integer, List<AlertConf>> alertConfs = getAlertConfigurations();
            Map<Period, Map<Integer, RealTimeReport>> realTimeReports = realTimeReportService.getLastPeriodIndicators(dataPermissionDomain);
            for (int i = offset; i < realTimeDeliveryList.size(); i++) {
                RealTimeDeliveryFormatVo report = realTimeStats.get(i);
                RealTimeDeliveryVo delivery = realTimeDeliveryList.get(i);
                List<AlertConf> alerts = alertConfs.get(delivery.getPlanId());
                //内部告警
                if (delivery.getTotalCost() != null && delivery.getTotalCost() >= 100 && delivery.getCostPerRegistrationFd() == null) {
                    report.setPlanNameColor(RED);
                    report.setTotalCostColor(RED);
                    report.setRegistrationsColor(RED);
                    report.setCostPerRegistrationColor(RED);
                }

                if (alerts != null) {
                    report.setAlertConfStatus(HAS_ALERT);
                    for (AlertConf alertConf : alerts) {
                        if (alertConf.getType() == AlertType.General.getValue()) {
                            setGeneralAlert(report, delivery, alertConf);
                        } else {
                            Map<Integer, RealTimeReport> reportsOfPeriod = realTimeReports.get(Period.parseByValue(alertConf.getPeriod()));
                            if (reportsOfPeriod != null) {
                                RealTimeReport realTimeReport = reportsOfPeriod.get(delivery.getPlanId());
                                if (hasAdvancedAlert(alertConf, realTimeReport)) {
                                    report.setPlanNameColor(RED);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 设置普通告警
     *
     * @param report
     * @param delivery
     * @param alertConf
     */
    private void setGeneralAlert(RealTimeDeliveryFormatVo report, RealTimeDeliveryVo delivery, AlertConf alertConf) {
        //普通告警
        //消耗
        if (compare(delivery.getTotalCost(), alertConf.getCost(), Operator.parse(alertConf.getCostOperator()))) {
            //CPA
            if (compare(delivery.getCostPerRegistrationFd(), alertConf.getCostPerRegistration(), Operator.parse(alertConf.getCpaOperator()))) {
                report.setCostPerRegistrationColor(RED);
                report.setTotalCostColor(RED);
                report.setPlanNameColor(RED);
            }

            //CTR
            if (compare(delivery.getCtr(), alertConf.getCtr(), Operator.parse(alertConf.getCtrOperator()))) {
                report.setCtrColor(RED);
                report.setTotalCostColor(RED);
                report.setPlanNameColor(RED);
            }

            //下载率
            if (compare(delivery.getDownloadRate(), alertConf.getDownloadRate(), Operator.parse(alertConf.getDownloadRateOperator()))) {
                report.setDownloadRateColor(RED);
                report.setTotalCostColor(RED);
                report.setPlanNameColor(RED);
            }

            if (alertConf.getCostPerRegistration() == null && alertConf.getCtr() == null && alertConf.getDownloadRate() == null) {
                report.setPlanNameColor(RED);
                report.setTotalCostColor(RED);
            }
        }
    }

    private boolean hasGeneralAlert(DeliveryAlertInfoVo delivery, AlertConf alertConf) {
        if (delivery == null || alertConf == null || alertConf.getEmailAlert() == EMAIL_ALERT_FALSE) {
            return false;
        }
        //普通告警
        //消耗
        boolean hasGeneralAlert = false;
        if (compare(delivery.getConsume(), alertConf.getCost(), Operator.parse(alertConf.getCostOperator()))) {
            //CPA
            if (compare(delivery.getCostPerRegistration(), alertConf.getCostPerRegistration(), Operator.parse(alertConf.getCpaOperator()))) {
                hasGeneralAlert = true;
            }

            //CTR
            if (compare(delivery.getCtr(), alertConf.getCtr(), Operator.parse(alertConf.getCtrOperator()))) {
                hasGeneralAlert = true;
            }

            //下载率
            if (compare(delivery.getDownloadRate(), alertConf.getDownloadRate(), Operator.parse(alertConf.getDownloadRateOperator()))) {
                hasGeneralAlert = true;
            }

            if (alertConf.getCostPerRegistration() == null && alertConf.getCtr() == null && alertConf.getDownloadRate() == null) {
                hasGeneralAlert = true;
            }
        }
        return hasGeneralAlert;
    }


    /**
     * 发送高级告警邮件
     *
     * @return
     */
    public void sendAdvancedAlerts() {
        List<AlertConf> alertConfs = alertConfMapper.getAdvancedAlertConfigurations();
        if (alertConfs != null) {
            Map<Period, Map<Integer, RealTimeReport>> realTimeReports = realTimeReportService.getLastPeriodIndicators();
            for (AlertConf alertConf : alertConfs) {
                if (alertConf.getEmailAlert() == EMAIL_ALERT_TRUE) {
//高级告警
                    Map<Integer, RealTimeReport> reportsOfPeriod = realTimeReports.get(Period.parseByValue(alertConf.getPeriod()));
                    if (reportsOfPeriod != null) {
                        RealTimeReport realTimeReport = reportsOfPeriod.get(alertConf.getPlanID());
                        if (hasAdvancedAlert(alertConf, realTimeReport)) {
                            //Sent alert email
                            sentAdvancedAlertEmail(realTimeReport);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void sendGeneralAlerts(String date) {
        //普通告警
        // modify by luzhuyou 2017/03/05 采用新的查询实时告警(SQL)信息处理逻辑
        List<DeliveryAlertInfoVo> realTimeDeliveryList = realTimeDeliveryMapper.queryMonitorInfo(date);

        if (null == realTimeDeliveryList) {
            logger.debug("No delivery data trigger alarm！");
            return;
        }

        Map<Integer, AlertConf> generalAlertConfs = getGeneralAlertConfigurations();
        for (DeliveryAlertInfoVo delivery : realTimeDeliveryList) {
            AlertConf alertConf = generalAlertConfs.get(delivery.getPlanId());
            if (hasGeneralAlert(delivery, alertConf)) {
                //发送邮件
                sentGeneralEmail(delivery, date);
            }
        }
    }

    private void sentGeneralEmail(DeliveryAlertInfoVo vo, String date) {
        String subject = "告警:实时投放[" + vo.getMedium() + "][计划：" + vo.getPlanName() + "]";
        StringBuilder content = new StringBuilder();
        content.append("[").append(date).append("]一条投放计划达到告警阈值，请关注：<br/>");
        content.append(" 媒体：[").append(vo.getMedium()).append("]<br/>");
        content.append(" 计划：[").append(vo.getPlanName()).append("]<br/>");
        content.append(" 消耗：[").append(vo.getConsume()).append("]<br/>");
        content.append(" 注册CPA：[").append(vo.getCostPerRegistration() == null ? "-" : (new BigDecimal(vo.getCostPerRegistration()).setScale(2, RoundingMode.HALF_UP))).append("]<br/>");
        content.append(" CTR：[").append(vo.getCtr() == null ? "-" : (new BigDecimal(vo.getCtr() * 100).setScale(2, RoundingMode.HALF_UP) + "%")).append("]<br/>");
        content.append(" 下载率：[").append(vo.getDownloadRate() == null ? "-" : (new BigDecimal(vo.getDownloadRate() * 100).setScale(2, RoundingMode.HALF_UP) + "%")).append("]<br/>");

        String to = vo.getAlertEmails();
        if (StringUtils.isNotBlank(to)) {
            logger.info("Alert:{[{}]{}. Send to [{}]}", new Object[]{date, subject, to});
            MailUtils.sendAlarmMail(subject, content.toString(), to);
        }
    }

    private void sentAdvancedAlertEmail(RealTimeReport alert) {
//检查收件人邮件是否为空
        if (StringUtils.isBlank(alert.getResponsibleAccounts())) {
            //如果收件人邮件为空,不发送告警邮件
            logger.info("Alert but do not set the recipients!");
            return;
        }
        NumberFormat nf = NumberFormat.getPercentInstance();
        //保留两位小数
        nf.setMinimumFractionDigits(2);
        //四舍五入
        nf.setRoundingMode(RoundingMode.HALF_UP);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String subject = "高级告警:实时投放[" + alert.getMediumName() + "][计划：" + alert.getPlanName() + "]";
        StringBuilder content = new StringBuilder();
        content.append("[").append(df.format(alert.getTime())).append("]一条投放计划达到告警阈值，请关注：<br/>");
        content.append(" 媒体：[").append(alert.getMediumName()).append("]<br/>");
        content.append(" 计划：[").append(alert.getPlanName()).append("]<br/>");
        content.append(" 时段：[").append(alert.getTimeRange()).append("]<br/>");
        content.append(" 时段消耗：[").append(alert.getCost().setScale(2, RoundingMode.HALF_UP)).append("]<br/>");
        content.append(" 时段注册CPA：[").append(alert.getCostPerRegistrationFd() == null ? "-" : (alert.getCostPerRegistrationFd().setScale(2, RoundingMode.HALF_UP))).append("]<br/>");
        content.append(" 时段CTR：[").append(nf.format(alert.getCtr())).append("]<br/>");
        content.append(" 时段下载率：[").append(nf.format(alert.getDownloadRate())).append("]<br/>");
        content.append(" 时段下载单价：[").append(alert.getCostPerDownload() == null ? "-" : (alert.getCostPerDownload().setScale(2, RoundingMode.HALF_UP))).append("]<br/>");

        String to = alert.getResponsibleAccounts();
        logger.info("Advanced Alert:{[{}]{}. Send to [{}]}", new Object[]{df.format(alert.getTime()), subject, to});
        MailUtils.sendAlarmMail(subject, content.toString(), to);
    }

    private boolean hasAdvancedAlert(AlertConf alertConf, RealTimeReport realTimeReport) {
        boolean hasAdvancedAlert = false;
        if (realTimeReport != null) {
            //消耗
            if (compare(realTimeReport.getCost(), alertConf.getCost(), Operator.parse(alertConf.getCostOperator()))) {
                //CPA
                boolean hasCpaAlert = compare(realTimeReport.getCostPerRegistrationFd(), alertConf.getCostPerRegistration(), Operator.parse(alertConf.getCpaOperator()));

                //CTR
                boolean hasCtrAlert = compare(realTimeReport.getCtr(), alertConf.getCtr(), Operator.parse(alertConf.getCtrOperator()));

                //下载率
                boolean hasDownloadRateAlert = compare(realTimeReport.getDownloadRate(), alertConf.getDownloadRate(), Operator.parse(alertConf.getDownloadRateOperator()));

                //下载单价
                boolean hasCostPerDownloadAlert = compare(realTimeReport.getCostPerDownload(), alertConf.getCostPerDownload(), Operator.parse(alertConf.getCostPerDownloadOperator()));

                boolean isOptionsIgnored = alertConf.getCostPerRegistration() == null && alertConf.getCtr() == null && alertConf.getDownloadRate() == null && alertConf.getCostPerDownload() == null;
                if (hasCpaAlert || hasCtrAlert || hasDownloadRateAlert || hasCostPerDownloadAlert || isOptionsIgnored) {
                    hasAdvancedAlert = true;
                }
            }
        }
        return hasAdvancedAlert;
    }

    private boolean compare(Double value1, BigDecimal value2, Operator operator) {
        if (operator == null || value1 == null || value2 == null) {
            return false;
        }
        switch (operator) {
            case LTE:
                return new BigDecimal(value1).compareTo(value2) <= 0;
            case GTE:
                return new BigDecimal(value1).compareTo(value2) >= 0;
            default:
                return false;
        }
    }

    private boolean compare(BigDecimal value1, BigDecimal value2, Operator operator) {
        if (operator == null || value1 == null || value2 == null) {
            return false;
        }
        switch (operator) {
            case LTE:
                return value1.compareTo(value2) <= 0;
            case GTE:
                return value1.compareTo(value2) >= 0;
            default:
                return false;
        }
    }

    private Map<Integer, List<AlertConf>> getAlertConfigurations() {
        List<AlertConf> alertConfs = alertConfMapper.getAllAlertConfigurations();
        if (alertConfs == null) {
            return null;
        }
        //根据计划ID分组
        Map<Integer, List<AlertConf>> alertConfGroupByPlanID = new HashMap<>();
        for (AlertConf alertConf : alertConfs) {
            if (alertConfGroupByPlanID.containsKey(alertConf.getPlanID())) {
                alertConfGroupByPlanID.get(alertConf.getPlanID()).add(alertConf);
            } else {
                List<AlertConf> confs = new ArrayList<>();
                confs.add(alertConf);
                alertConfGroupByPlanID.put(alertConf.getPlanID(), confs);
            }
        }
        return alertConfGroupByPlanID;
    }

    private Map<Integer, AlertConf> getGeneralAlertConfigurations() {
        List<AlertConf> alertConfs = alertConfMapper.getGeneralAlertConfigurations();
        if (alertConfs == null) {
            return null;
        }
        //根据计划ID分组
        Map<Integer, AlertConf> alertConfGroupByPlanID = new HashMap<>();
        for (AlertConf alertConf : alertConfs) {
            alertConfGroupByPlanID.put(alertConf.getPlanID(), alertConf);
        }
        return alertConfGroupByPlanID;
    }

}
