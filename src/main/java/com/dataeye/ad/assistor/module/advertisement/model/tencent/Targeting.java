package com.dataeye.ad.assistor.module.advertisement.model.tencent;



/**
 * @author lingliqi
 * @date 2017-12-05 11:25
 */
public class Targeting {


    private Integer targeting_id;

    private String targeting_name;

    private TargetingBean targeting;

    private String description;

    private Integer created_time;

    private Integer last_modified_time;

    private String ad_lock_status;

    public Integer getTargeting_id() {
        return targeting_id;
    }

    public void setTargeting_id(Integer targeting_id) {
        this.targeting_id = targeting_id;
    }

    public String getTargeting_name() {
        return targeting_name;
    }

    public void setTargeting_name(String targeting_name) {
        this.targeting_name = targeting_name;
    }

    public TargetingBean getTargeting() {
        return targeting;
    }

    public void setTargeting(TargetingBean targeting) {
        this.targeting = targeting;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Integer created_time) {
        this.created_time = created_time;
    }

    public Integer getLast_modified_time() {
        return last_modified_time;
    }

    public void setLast_modified_time(Integer last_modified_time) {
        this.last_modified_time = last_modified_time;
    }

    public String getAd_lock_status() {
        return ad_lock_status;
    }

    public void setAd_lock_status(String ad_lock_status) {
        this.ad_lock_status = ad_lock_status;
    }
}
