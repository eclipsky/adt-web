package com.dataeye.ad.assistor.module.systemaccount.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.systemaccount.model.MenuInterfaceMapping;

/**
 * 菜单接口映射Mapper.
 * Created by luzhuyou
 */
@MapperScan
public interface MenuInterfaceMappingMapper {

	/**
	 * 菜单接口映射信息列表
	 * @return
	 */
    public List<MenuInterfaceMapping> query();

}
