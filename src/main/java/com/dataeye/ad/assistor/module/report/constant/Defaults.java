package com.dataeye.ad.assistor.module.report.constant;

/**
 * 投放日报默认值
 * Created by huangzehai on 2017/1/4.
 */
public class Defaults {
    /**
     * 排序字段.
     */
    public static final String TOTAL_COST = "total_cost";

    /**
     * 账号字段
     */
    public static final String ACCOUNT = "account_name";

    /**
     * 计划字段
     */
    public static final String PLAN = "plan_name";

    /**
     * 账号和计划名称.
     */
    public static final String ACCOUNT_AND_PLAN = "account_name,plan_name";

    /**
     * 排序方式，默认为降序.
     */
    public static final String DESC = "DESC";

    /**
     * 日期显示数据默认排序字段.
     */
    public static final String DATE = "p.stat_date";

    /**
     * 按天显示数据默认排序方式为升序.
     */
    public static final String ASC = "ASC";

    /**
     * 默认当前分页.
     */
    public static final int CURRENT_PAGE = 1;
    /**
     * 默认分页大小.
     */
    public static final int PAGE_SIZE = 10;
    
    /**
     * 子账户名称
     */
    public static final String SYSTEM_ACCOUNT_ID = "system_account_id";

}
