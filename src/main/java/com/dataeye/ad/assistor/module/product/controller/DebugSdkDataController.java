package com.dataeye.ad.assistor.module.product.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.product.constants.Constants.DebugLogType;
import com.dataeye.ad.assistor.module.product.service.DebugSdkDataService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;

/**
 * Created by ldj 2017/10/30
 * 测试日志管理控制器.
 */
@Controller
public class DebugSdkDataController {
	
    private static final Logger logger = (Logger) LoggerFactory.getLogger(DebugSdkDataController.class);

    @Autowired
    private DebugSdkDataService debugSdkDataService;


    /**
     * 实时日志查询
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author ldj 2017/10/30
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/debugSdkData/listDebugLog.do")
    public Object query(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("实时日志查询");
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
        String appId = parameter.getParameter("appId");
        String status = parameter.getParameter("status");
        String type = "total";
        if(StringUtils.isBlank(appId)) {
			ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ID_ERROR);
        }
        if(StringUtils.isBlank(status) || !StringUtils.isNumeric(status)) {
        	status = "0";
        }
        if(!StringUtils.isBlank(parameter.getParameter("type"))){
        	type = DebugLogType.getdebugLogType(parameter.getParameter("type"));
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int uid = userInSession.getCompanyAccountUid();
        
        return debugSdkDataService.queryDebugDevice(uid, appId, status, type);
    }
	
	 /**
     * 查询测试统计日志
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author ldj 2017/10/30
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/debugSdkData/listDebugSummary.do")
    public Object queryDebugSummary(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("查询测试统计日志");
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
        String appId = parameter.getParameter("appId");
        if(StringUtils.isBlank(appId)) {
			ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ID_ERROR);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int uid = userInSession.getCompanyAccountUid();
        return debugSdkDataService.queryDebugSummary(uid, appId);
    }
	
    /**
     * 清除测试数据
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author ldj 2017/10/30
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/debugSdkData/deleteDebugData.do")
    public Object delete(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("清除测试日志.");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String appid = parameter.getParameter("appId");
        
        if(StringUtils.isBlank(appid)) {
			ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ID_ERROR);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
		//获取uid
		int uid = userInSession.getCompanyAccountUid();
        return debugSdkDataService.deleteDebugData(uid, appid);
    }
    
 
  
    
}
