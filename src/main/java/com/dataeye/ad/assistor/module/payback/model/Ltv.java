package com.dataeye.ad.assistor.module.payback.model;

import java.math.BigDecimal;

public class Ltv {
    /**
     * 消耗.
     */
    private BigDecimal cost;
    /**
     * 注册.
     */
    private Integer registrations;

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Integer getRegistrations() {
        return registrations;
    }

    public void setRegistrations(Integer registrations) {
        this.registrations = registrations;
    }

    @Override
    public String toString() {
        return "Cpa{" +
                "cost=" + cost +
                ", registrations=" + registrations +
                '}';
    }
}
