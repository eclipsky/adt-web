package com.dataeye.ad.assistor.module.report.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.Date;

/**
 * 日期范围查询
 * Created by huangzehai on 2017/7/18.
 */
public class DateRangeQuery extends DataPermissionDomain {
    /**
     * 起始日期.
     */
    private Date startDate;
    /**
     * 截止日期.
     */
    private Date endDate;


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "DateRangeQuery{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                "} " + super.toString();
    }
}
