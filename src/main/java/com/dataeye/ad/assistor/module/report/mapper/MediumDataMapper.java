package com.dataeye.ad.assistor.module.report.mapper;

import com.dataeye.ad.assistor.module.report.model.CommonQuery;
import com.dataeye.ad.assistor.module.report.model.MediumData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 媒体数据Mapper.
 */
public interface MediumDataMapper {
    /**
     * 列出媒体数据
     *
     * @param query
     * @return
     */
    List<MediumData> listMediumData(CommonQuery query);

    /**
     * 批量更新媒体数据
     *
     * @param mediumDataList
     * @return
     */
    int updateMediumData(@Param("mediumDataList") List<MediumData> mediumDataList);
}
