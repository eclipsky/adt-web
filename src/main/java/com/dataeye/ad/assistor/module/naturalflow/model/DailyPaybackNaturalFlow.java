package com.dataeye.ad.assistor.module.naturalflow.model;

import java.math.BigDecimal;

public class DailyPaybackNaturalFlow extends NaturalFlow {
//    /**
//     * 首日充值
//     */
//    private BigDecimal payAmount1day;
    /**
     * 3日充值
     */
    private BigDecimal payAmount3days;

//    /**
//     * 7日充值
//     */
//    private BigDecimal payAmount7days;

    /**
     * 15日充值
     */
    private BigDecimal payAmount15days;

//    /**
//     * 30日充值
//     */
//    private BigDecimal payAmount30days;

    /**
     * 60日充值
     */
    private BigDecimal payAmount60days;

    /**
     * 90日充值
     */
    private BigDecimal payAmount90days;
    
    /**
     * 2日充值
     */
    private BigDecimal payAmount2days;
    /**
     * 4日充值
     */
    private BigDecimal payAmount4days;
    /**
     * 5日充值
     */
    private BigDecimal payAmount5days;
    /**
     * 6日充值
     */
    private BigDecimal payAmount6days;

//    public BigDecimal getPayAmount1day() {
//        return payAmount1day;
//    }
//
//    public void setPayAmount1day(BigDecimal payAmount1day) {
//        this.payAmount1day = payAmount1day;
//    }

    public BigDecimal getPayAmount3days() {
        return payAmount3days;
    }

    public void setPayAmount3days(BigDecimal payAmount3days) {
        this.payAmount3days = payAmount3days;
    }

//    public BigDecimal getPayAmount7days() {
//        return payAmount7days;
//    }
//
//    public void setPayAmount7days(BigDecimal payAmount7days) {
//        this.payAmount7days = payAmount7days;
//    }

    public BigDecimal getPayAmount15days() {
        return payAmount15days;
    }

    public void setPayAmount15days(BigDecimal payAmount15days) {
        this.payAmount15days = payAmount15days;
    }

//    public BigDecimal getPayAmount30days() {
//        return payAmount30days;
//    }
//
//    public void setPayAmount30days(BigDecimal payAmount30days) {
//        this.payAmount30days = payAmount30days;
//    }

    public BigDecimal getPayAmount60days() {
        return payAmount60days;
    }

    public void setPayAmount60days(BigDecimal payAmount60days) {
        this.payAmount60days = payAmount60days;
    }

    public BigDecimal getPayAmount90days() {
        return payAmount90days;
    }

    public void setPayAmount90days(BigDecimal payAmount90days) {
        this.payAmount90days = payAmount90days;
    }

    public BigDecimal getPayAmount2days() {
		return payAmount2days;
	}

	public void setPayAmount2days(BigDecimal payAmount2days) {
		this.payAmount2days = payAmount2days;
	}

	public BigDecimal getPayAmount4days() {
		return payAmount4days;
	}

	public void setPayAmount4days(BigDecimal payAmount4days) {
		this.payAmount4days = payAmount4days;
	}

	public BigDecimal getPayAmount5days() {
		return payAmount5days;
	}

	public void setPayAmount5days(BigDecimal payAmount5days) {
		this.payAmount5days = payAmount5days;
	}

	public BigDecimal getPayAmount6days() {
		return payAmount6days;
	}

	public void setPayAmount6days(BigDecimal payAmount6days) {
		this.payAmount6days = payAmount6days;
	}

	@Override
	public String toString() {
		return "DailyPaybackNaturalFlow [payAmount3days=" + payAmount3days
				+ ", payAmount15days=" + payAmount15days + ", payAmount60days="
				+ payAmount60days + ", payAmount90days=" + payAmount90days
				+ ", payAmount2days=" + payAmount2days + ", payAmount4days="
				+ payAmount4days + ", payAmount5days=" + payAmount5days
				+ ", payAmount6days=" + payAmount6days + "]";
	}
}
