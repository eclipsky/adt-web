package com.dataeye.ad.assistor.module.realtimedelivery.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.module.realtimedelivery.mapper.AlertConfMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.mapper.AlertEmailConfMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.mapper.RealTimeDeliveryMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.model.AlertConf;
import com.dataeye.ad.assistor.module.realtimedelivery.model.AlertEmailConf;
import com.dataeye.ad.assistor.module.realtimedelivery.model.AlertQuery;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeDeliveryRequestVo;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeDeliveryVo;
import com.dataeye.ad.assistor.util.DateUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 实时投放业务逻辑
 *
 * @author luzhuyou 2017/01/03
 */
@Service("deliveryService")
public class DeliveryService {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(DeliveryService.class);

    @Autowired
    private AlertConfMapper alertConfMapper;

    @Autowired
    private AlertEmailConfMapper alertEmailConfMapper;

    @Autowired
    private RealTimeDeliveryMapper realTimeDeliveryMapper;

    @Autowired
    private AlertService alertService;

    /**
     * 告警配置
     *
     * @param id                  主键
     * @param planID              计划ID
     * @param consume             消耗
     * @param costPerRegistration CPA
     * @param ctr                 CTR
     * @param downloadRate        下载率
     * @param emailAlert          是否邮件告警
     * @author luzhuyou
     */
    public int confAlert(Integer id, Integer planID, BigDecimal consume,
                         BigDecimal costPerRegistration, BigDecimal ctr, BigDecimal downloadRate,
                         int emailAlert) {
        if (id != null) {
            return modifyAlert(id, consume, costPerRegistration, ctr, downloadRate, emailAlert);
        }
        return addAlert(planID, consume, costPerRegistration, ctr, downloadRate, emailAlert);
    }

    /**
     * 新建告警配置
     *
     * @param planID              计划ID
     * @param consume             消耗
     * @param costPerRegistration CPA
     * @param ctr                 CTR
     * @param downloadRate        下载率
     * @param emailAlert          是否邮件告警
     * @author luzhuyou
     */
    @Deprecated
    public int addAlert(int planID, BigDecimal consume,
                        BigDecimal costPerRegistration, BigDecimal ctr, BigDecimal downloadRate,
                        int emailAlert) {
        AlertConf conf = new AlertConf();
        conf.setPlanID(planID);
        conf.setCost(consume);
        conf.setCostPerRegistration(costPerRegistration);
        conf.setCtr(null == ctr ? null : ctr.divide(new BigDecimal(100)));//传入的值为百分比，保存到数据库后除以100
        conf.setDownloadRate(null == downloadRate ? null : downloadRate.divide(new BigDecimal(100)));//传入的值为百分比，保存到数据库后除以100
        conf.setEmailAlert(emailAlert);
        return alertConfMapper.insert(conf);
    }

    /**
     * 添加警告配置
     *
     * @return
     */
    public int addAlertConfiguration(AlertConf alertConf) {
        //传入的值为百分比，保存到数据库后除以100
        if (alertConf.getCtr() != null) {
            alertConf.setCtr(alertConf.getCtr().divide(new BigDecimal(100)));
        }

        //传入的值为百分比，保存到数据库后除以100
        if (alertConf.getDownloadRate() != null) {
            alertConf.setDownloadRate(alertConf.getDownloadRate().divide(new BigDecimal(100)));
        }
        return alertConfMapper.insert(alertConf);
    }

    /**
     * 批量添加告警配置，并重置已有告警配置。
     *
     * @param alertConf
     * @param planIds
     * @return
     */
    public void addAlertConfigurations(AlertConf alertConf, List<Integer> planIds) {
        //传入的值为百分比，保存到数据库后除以100
        if (alertConf.getCtr() != null) {
            alertConf.setCtr(alertConf.getCtr().divide(new BigDecimal(100)));
        }

        //传入的值为百分比，保存到数据库后除以100
        if (alertConf.getDownloadRate() != null) {
            alertConf.setDownloadRate(alertConf.getDownloadRate().divide(new BigDecimal(100)));
        }

        List<AlertConf> alertConfs = new ArrayList<>();
        for (Integer planId : planIds) {
            try {
                AlertConf alertConfCopy = alertConf.clone();
                alertConfCopy.setPlanID(planId);
                alertConfs.add(alertConfCopy);
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        alertConfMapper.batchInsertAlertConfs(alertConfs);
    }

    /**
     * 删除指定计划的告警配置.
     * @param planIds
     */
    public void deleteAlertConfByPlanIds( List<Integer> planIds){
        alertConfMapper.deleteAlertConfByPlanIds(planIds);
    }

    /**
     * 修改告警配置
     *
     * @param id                  主键
     * @param consume             消耗
     * @param costPerRegistration CPA
     * @param ctr                 CTR
     * @param downloadRate        下载率
     * @param emailAlert          是否邮件告警
     * @author luzhuyou
     */
    @Deprecated
    public int modifyAlert(int id, BigDecimal consume,
                           BigDecimal costPerRegistration, BigDecimal ctr, BigDecimal downloadRate,
                           int emailAlert) {
        AlertConf conf = new AlertConf();
        conf.setId(id);
        conf.setCost(consume);
        conf.setCostPerRegistration(costPerRegistration);
        conf.setCtr(null == ctr ? null : ctr.divide(new BigDecimal(100)));//传入的值为百分比，保存到数据库后除以100
        conf.setDownloadRate(null == downloadRate ? null : downloadRate.divide(new BigDecimal(100)));//传入的值为百分比，保存到数据库后除以100
        conf.setEmailAlert(emailAlert);
        return alertConfMapper.update(conf);
    }

    /**
     * 更新告警配置
     *
     * @param alertConf
     * @return
     */
    public int updateAlertConfiguration(AlertConf alertConf) {
        //传入的值为百分比，保存到数据库后除以100
        if (alertConf.getCtr() != null) {
            alertConf.setCtr(alertConf.getCtr().divide(new BigDecimal(100)));
        }

        //传入的值为百分比，保存到数据库后除以100
        if (alertConf.getDownloadRate() != null) {
            alertConf.setDownloadRate(alertConf.getDownloadRate().divide(new BigDecimal(100)));
        }
        return alertConfMapper.update(alertConf);
    }

    /**
     * 查询告警配置
     *
     * @param alertQuery 警告配置查询条件
     * @author luzhuyou
     */
    public AlertConf getAlert(AlertQuery alertQuery) {
        AlertConf conf = alertConfMapper.get(alertQuery);
        if (conf != null) {
            BigDecimal ctr = conf.getCtr();
            conf.setCtr(null == ctr ? null : ctr.multiply(new BigDecimal(100)));

            BigDecimal downloadRate = conf.getDownloadRate();
            conf.setDownloadRate(null == downloadRate ? null : downloadRate.multiply(new BigDecimal(100)));
        }
        return conf;
    }

    /**
     * 告警邮件设置
     *
     * @param accountID   账户ID
     * @param alertEmails 告警邮箱
     * @author luzhuyou
     */
    public int addOrModifyAlertEmail(int accountID, String alertEmails) {
        AlertEmailConf conf = alertEmailConfMapper.get(accountID);
        int result = 0;
        if (conf == null) {
            logger.info("Add alert email info, accountID=[{}]", accountID);
            conf = new AlertEmailConf();
            conf.setAccountId(accountID);
            conf.setAlertEmails(alertEmails);
            result = alertEmailConfMapper.insert(conf);
        } else {
            logger.info("Modify alert email info, accountID=[{}]", accountID);
            conf.setAlertEmails(alertEmails);
            result = alertEmailConfMapper.update(conf);
        }
        return result;
    }

    /**
     * 实时投放数据查询
     *
     * @param companyId                  公司ID
     * @param mediumIds                  媒体ID数组，多个以逗号分隔
     * @param mediumAccountIds           媒体账号ID数组，多个以逗号分隔
     * @param statDate                   需要查询的统计日期
     * @param permissionMediumAccountIds 具有权限访问的数据的媒体账号ID
     * @param ignoreIsNullCostPlan  过滤媒体数据为空的计划，值为true（过滤）或false.
     * @param planIds 计划列表
     * @author luzhuyou
     */
    public List<RealTimeDeliveryVo> queryDelivery(int companyId, String mediumIds, String mediumAccountIds, String statDate, Integer[] permissionMediumAccountIds, List<Integer> productIds, boolean ignoreIsNullCostPlan,List<Integer> planIds) {

        RealTimeDeliveryRequestVo requestVo = new RealTimeDeliveryRequestVo();
        requestVo.setCompanyId(companyId);
        requestVo.setStatDate(DateUtils.currentDate());
        if (null != mediumIds) {
            String[] mediumIdsStrArr = mediumIds.split(",");
            Integer[] mediumIdArr = new Integer[mediumIdsStrArr.length];
            for (int i = 0; i < mediumIdsStrArr.length; i++) {
                mediumIdArr[i] = Integer.parseInt(mediumIdsStrArr[i]);
            }
            requestVo.setMediumIds(mediumIdArr);
        }
        if (null != mediumAccountIds) {
            String[] mediumAccountIdsStrArr = mediumAccountIds.split(",");
            Integer[] mediumIdsArr = new Integer[mediumAccountIdsStrArr.length];
            for (int i = 0; i < mediumAccountIdsStrArr.length; i++) {
                mediumIdsArr[i] = Integer.parseInt(mediumAccountIdsStrArr[i]);
            }
            requestVo.setMediumAccountIds(mediumIdsArr);
        }
        if (null != permissionMediumAccountIds && permissionMediumAccountIds.length > 0) {
            requestVo.setPermissionMediumAccountIds(permissionMediumAccountIds);
        }

        requestVo.setProductIds(productIds);
        requestVo.setPlanIds(planIds);
        requestVo.setIgnoreIsNullCostPlan(ignoreIsNullCostPlan);

        List<RealTimeDeliveryVo> realTimeDeliveryList = realTimeDeliveryMapper.query(requestVo);
        return realTimeDeliveryList;
    }

    /**
     * 监控实时投放告警
     *
     * @param date 需要查询的日期
     * @author luzhuyou
     */
    public void monitor(String date) {
        //普通告警
        alertService.sendGeneralAlerts(date);
        //高级告警
        alertService.sendAdvancedAlerts();
    }

    private boolean isNull(Object threashold) {
        if (threashold == null) {
            return true;
        } else {
            return false;
        }
    }

}
