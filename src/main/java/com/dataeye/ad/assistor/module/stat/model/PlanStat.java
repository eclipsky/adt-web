package com.dataeye.ad.assistor.module.stat.model;

import java.util.Date;

/**
 * 计划统计（按天/实时）
 * @author luzhuyou 2017/03/16
 *
 */
public class PlanStat {
	/**
	 * 统计日期
	 */
	private String statDate;
	/**
	 * 计划ID
	 */
	private int planId;
	/**
	 * 公司ID
	 */
	private int companyId;
	/**
	 * 落地页ID
	 */
	private int pageId;
	/**
	 * 包ID
	 */
	private int pkgId;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/** 
	 * 系统类型：0-Others, 1-iOS, 2-Android 
	 */
	private Integer osType;
	
	public String getStatDate() {
		return statDate;
	}
	public void setStatDate(String statDate) {
		this.statDate = statDate;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getPageId() {
		return pageId;
	}
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	public int getPkgId() {
		return pkgId;
	}
	public void setPkgId(int pkgId) {
		this.pkgId = pkgId;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	
}
