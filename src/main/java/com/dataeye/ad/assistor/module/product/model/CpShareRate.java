package com.dataeye.ad.assistor.module.product.model;

import java.util.Date;

/**
 * CP分成率
 * @author luzhuyou 2017/02/20
 *
 */
public class CpShareRate {

	/** 公司ID */
	private Integer companyId;
	/** 分成率ID */
	private Integer shareRateId;
	/** 产品ID */
	private Integer productId;
	/** 分成率 */
	private Double shareRate;
	/** 生效日期 */
	private Date effectDate;
	/** 失效日期 */
	private Date invalidDate;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getShareRateId() {
		return shareRateId;
	}
	public void setShareRateId(Integer shareRateId) {
		this.shareRateId = shareRateId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Double getShareRate() {
		return shareRate;
	}
	public void setShareRate(Double shareRate) {
		this.shareRate = shareRate;
	}
	public Date getEffectDate() {
		return effectDate;
	}
	public void setEffectDate(Date effectDate) {
		this.effectDate = effectDate;
	}
	public Date getInvalidDate() {
		return invalidDate;
	}
	public void setInvalidDate(Date invalidDate) {
		this.invalidDate = invalidDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "CpShareRate [companyId=" + companyId + ", shareRateId="
				+ shareRateId + ", productId=" + productId + ", shareRate="
				+ shareRate + ", effectDate=" + effectDate + ", invalidDate="
				+ invalidDate + ", remark=" + remark + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}
	
}
