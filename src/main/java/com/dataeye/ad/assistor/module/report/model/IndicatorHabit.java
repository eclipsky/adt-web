package com.dataeye.ad.assistor.module.report.model;

/**
 * 指标偏好.
 * Created by huangzehai on 2017/6/8.
 */
public class IndicatorHabit{
    private Integer companyId;
    private Integer accountId;
    private String menuId;
    private String indicatorHabit;

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getIndicatorHabit() {
        return indicatorHabit;
    }

    public void setIndicatorHabit(String indicatorHabit) {
        this.indicatorHabit = indicatorHabit;
    }

    @Override
    public String toString() {
        return "IndicatorHabit{" +
                "companyId=" + companyId +
                ", accountId=" + accountId +
                ", menuId='" + menuId + '\'' +
                ", indicatorHabit='" + indicatorHabit + '\'' +
                '}';
    }
}
