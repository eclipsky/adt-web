package com.dataeye.ad.assistor.module.realtimedelivery.mapper;

import com.dataeye.ad.assistor.module.realtimedelivery.model.AlertConf;
import com.dataeye.ad.assistor.module.realtimedelivery.model.AlertQuery;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 计划警告配置映射器
 *
 * @author luzhuyou
 */
@MapperScan
public interface AlertConfMapper {
    int insert(AlertConf conf);

    AlertConf get(AlertQuery alertQuery);

    /**
     * 获取所有的告警配置信息.
     *
     * @return
     */
    List<AlertConf> getAllAlertConfigurations();

    /**
     * 获取所有高级告警配置
     *
     * @return
     */
    List<AlertConf> getAdvancedAlertConfigurations();

    /**
     * 获取所有高级告警配置
     *
     * @return
     */
    List<AlertConf> getGeneralAlertConfigurations();

    AlertConf query();

    int update(AlertConf conf);

    void delete(int id);

    /**
     * 删除指定计划的告警配置.
     *
     * @param planIds
     */
    void deleteAlertConfByPlanIds(@Param("planIds") List<Integer> planIds);

    void batchInsertAlertConfs(@Param("alertConfs")List<AlertConf> alertConfs);

}
