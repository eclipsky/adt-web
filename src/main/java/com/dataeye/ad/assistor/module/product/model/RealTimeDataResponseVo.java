package com.dataeye.ad.assistor.module.product.model;



/**
 * 实时统计数据
 * 
 * */
public class RealTimeDataResponseVo {
	
	private String channelId;  //渠道ID
	
	private String channelName; //渠道名称
	
	private String clickA;  //总点击次数
	
	private String clickU; //有效点击次数
	
	private String activeU; //激活数
	
	private String activeRate; //激活率
	
	private String registerU;  //注册率(设备)
	
	private String registerRate; //注册率
	
	private String activeUser; //今日累计活跃
	
	private String revenue;  //今日累计付费
	
	private String registerA; //注册账号数
	
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getClickA() {
		return clickA;
	}
	public void setClickA(String clickA) {
		this.clickA = clickA;
	}
	public String getClickU() {
		return clickU;
	}
	public void setClickU(String clickU) {
		this.clickU = clickU;
	}
	public String getActiveU() {
		return activeU;
	}
	public void setActiveU(String activeU) {
		this.activeU = activeU;
	}
	public String getActiveRate() {
		return activeRate;
	}
	public void setActiveRate(String activeRate) {
		this.activeRate = activeRate;
	}
	public String getRegisterU() {
		return registerU;
	}
	public void setRegisterU(String registerU) {
		this.registerU = registerU;
	}
	public String getRegisterRate() {
		return registerRate;
	}
	public void setRegisterRate(String registerRate) {
		this.registerRate = registerRate;
	}
	public String getActiveUser() {
		return activeUser;
	}
	public void setActiveUser(String activeUser) {
		this.activeUser = activeUser;
	}
	public String getRevenue() {
		return revenue;
	}
	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}
	public String getRegisterA() {
		return registerA;
	}
	public void setRegisterA(String registerA) {
		this.registerA = registerA;
	}
	@Override
	public String toString() {
		return "RealTimeDataResponseVo [channelId=" + channelId
				+ ", channelName=" + channelName + ", clickA=" + clickA
				+ ", clickU=" + clickU + ", activeU=" + activeU
				+ ", activeRate=" + activeRate + ", registerU=" + registerU
				+ ", registerRate=" + registerRate + ", activeUser="
				+ activeUser + ", revenue=" + revenue + ", registerA="
				+ registerA + "]";
	}
	
}
