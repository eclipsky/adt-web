package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.module.report.model.CommonQuery;
import com.dataeye.ad.assistor.module.report.model.MediumData;
import com.github.pagehelper.Page;

import java.util.List;

public interface MediumDataService {
    /**
     * 列出媒体数据
     *
     * @param query
     * @return
     */
    Page<MediumData> listMediumData(CommonQuery query);

    /**
     * 批量更新媒体数据
     *
     * @param mediumDataList
     * @return
     */
    int updateMediumData(List<MediumData> mediumDataList);
}
