package com.dataeye.ad.assistor.module.payback.service;

import com.dataeye.ad.assistor.common.TrendChart;
import com.dataeye.ad.assistor.module.payback.model.LtvDailyReport;
import com.dataeye.ad.assistor.module.payback.model.PaybackQuery;
import com.dataeye.ad.assistor.module.payback.model.PaybackTrendQuery;

import java.math.BigDecimal;
import java.util.List;

public interface LtvService {
    /**
     * LTV日报
     *
     * @param query
     * @return
     */
    List<LtvDailyReport> ltvDailyReport(PaybackQuery query);

    /**
     * LTV日趋势图
     *
     * @param query
     * @return
     */
    TrendChart<String, BigDecimal> ltvTrendChart(PaybackTrendQuery query);
}
