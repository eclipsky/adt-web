package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;

/**
 * 涨跌
 * Created by huangzehai on 2017/6/29.
 */
public class UpsAndDowns {
    /**
     * 涨跌值.
     */
    @Expose
    private BigDecimal increment;
    /**
     * 涨跌百分比.
     */
    @Expose
    private String incrementPercent;

    /**
     * 是否百分比.
     */
    @Expose
    private boolean isPercent;

    public BigDecimal getIncrement() {
        return increment;
    }

    public void setIncrement(BigDecimal increment) {
        this.increment = increment;
    }

    public String getIncrementPercent() {
        return incrementPercent;
    }

    public void setIncrementPercent(String incrementPercent) {
        this.incrementPercent = incrementPercent;
    }

    public boolean isPercent() {
        return isPercent;
    }

    public void setPercent(boolean percent) {
        isPercent = percent;
    }

    @Override
    public String toString() {
        return "UpsAndDowns{" +
                "increment=" + increment +
                ", incrementPercent='" + incrementPercent + '\'' +
                ", isPercent=" + isPercent +
                '}';
    }
}

