package com.dataeye.ad.assistor.module.campaign.constants;



/**
 * 查询推广计划接口响应数据
 * @author luzhuyou 2017/06/28
 *
 */
public class TrackingResCampaign {
	/** 渠道ID */
	private int channelId;
	/** TrackingAppID */
	private String appId;
	/** 活动ID（短链活动号）*/
	private String campaignId;
	/** 活动名称 */
	private String campaignName;
	/** 推广链接 */
	private String trackUrl;
	/** 下载地址（落地页URL） */
	private String downloadUrl;
	/** 广告参数 */
	private String thirdAdParams;
	/** 修改时间 */
	private String updateTime;
	
	/**监测链接类型**/
	private String type;
	
	/**监测转换指标*/
	private String eventType;
	
	public int getChannelId() {
		return channelId;
	}
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getTrackUrl() {
		return trackUrl;
	}
	public void setTrackUrl(String trackUrl) {
		this.trackUrl = trackUrl;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getThirdAdParams() {
		return thirdAdParams;
	}
	public void setThirdAdParams(String thirdAdParams) {
		this.thirdAdParams = thirdAdParams;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	@Override
	public String toString() {
		return "TrackingResCampaign [channelId=" + channelId + ", appId="
				+ appId + ", campaignId=" + campaignId + ", campaignName="
				+ campaignName + ", trackUrl=" + trackUrl + ", downloadUrl="
				+ downloadUrl + ", thirdAdParams=" + thirdAdParams
				+ ", updateTime=" + updateTime + ", type=" + type
				+ ", eventType=" + eventType + "]";
	}
	
}
