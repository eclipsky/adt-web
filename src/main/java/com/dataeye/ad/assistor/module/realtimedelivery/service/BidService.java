package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Bid;

/**
 * 出价服务接口.
 * Created by huangzehai on 2017/5/10.
 */
public interface BidService {
    /**
     * 出价.
     *
     * @param bid
     */
    Result bid(Bid bid);
}
