package com.dataeye.ad.assistor.module.result.util;

import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.module.result.model.Indicator;
import com.dataeye.ad.assistor.module.result.model.IndicatorWithId;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/11.
 */
public final class IndicatorUtils {


    private IndicatorUtils() {

    }

    /**
     * 格式化指标值，将货币单元由分转成元，并且保留2位小数.
     *
     * @param indicators
     */
    public static void formatIndicators(List<? extends Indicator> indicators) {
        if (indicators != null) {
            for (Indicator indicator : indicators) {
                if (indicator != null && indicator.getValue() != null) {
                    indicator.setValue(CurrencyUtils.fenToYuan(indicator.getValue()));
                }
            }
        }
    }

    /**
     * 将未知的媒体|产品|平台，命名为其他.
     *
     * @param indicators
     */
    public static void renameOthers(List<? extends Indicator> indicators) {
        if (indicators != null) {
            for (Indicator indicator : indicators) {
                if (indicator != null && indicator.getValue() != null) {
                    if (StringUtils.isBlank(indicator.getName())) {
                        indicator.setName(Labels.OTHERS);
                        if (indicator instanceof IndicatorWithId) {
                            ((IndicatorWithId) indicator).setId(Labels.OTHERS_ID);
                        }
                    }
                }
            }
        }
    }
}
