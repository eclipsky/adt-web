package com.dataeye.ad.assistor.module.dictionaries.model;

import java.util.Date;

import com.google.gson.annotations.Expose;


/**
 * 落地页页面表
 * @author luzhuyou 2017/03/02
 *
 */
public class Page {

	/** 页面ID */
	@Expose
	private Integer pageId;
	/** 页面名称 */
	@Expose
	private String pageName;
	/** H5应用ID */
	private String h5AppId;
	/** H5页面ID */
	@Expose
	private String h5PageId;
	/** 公司ID */
	private Integer companyId;
	/** 状态：0-正常，1-删除 */
	private Integer status;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	/** 系统类型：0-Others, 1-iOS, 2-Android */
	private Integer osType;
	
	public Integer getPageId() {
		return pageId;
	}
	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}
	public String getPageName() {
		return pageName;
	}
	/**
	 * 设置pageName时，截取url最后一个反斜杠后的内容
	 * @param pageName
	 */
	public void setPageName(String pageName) {
		if(pageName == null) {
			return;
		}
		int lastSlashIndex = pageName.lastIndexOf('/');
		if (lastSlashIndex > 0 && lastSlashIndex != pageName.length() - 1) {
			this.pageName = pageName.substring(lastSlashIndex + 1);
		} else {
			this.pageName = pageName;
		}
	}
	public String getH5AppId() {
		return h5AppId;
	}
	public void setH5AppId(String h5AppId) {
		this.h5AppId = h5AppId;
	}
	public String getH5PageId() {
		return h5PageId;
	}
	public void setH5PageId(String h5PageId) {
		this.h5PageId = h5PageId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	@Override
	public String toString() {
		return "Page [pageId=" + pageId + ", pageName=" + pageName
				+ ", h5AppId=" + h5AppId + ", h5PageId=" + h5PageId
				+ ", companyId=" + companyId + ", status=" + status
				+ ", remark=" + remark + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", osType=" + osType + "]";
	}
}
