package com.dataeye.ad.assistor.module.naturalflow.service;

import com.dataeye.ad.assistor.module.naturalflow.mapper.NaturalFlowMapper;
import com.dataeye.ad.assistor.module.naturalflow.model.DailyPaybackNaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.MonthlyPaybackNaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlowQuery;
import com.dataeye.ad.assistor.module.naturalflow.util.NaturalFlowUtils;
import com.dataeye.ad.assistor.module.report.model.DateRangeQuery;
import com.dataeye.ad.assistor.module.result.model.DailyIndicator;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * Created by huangzehai on 2017/7/17.
 */
@Service
public class NaturalFlowServiceImpl implements NaturalFlowService {

    @Autowired
    private NaturalFlowMapper naturalFlowMapper;

    /**
     * 查询自然流量
     *
     * @param query
     * @return
     */
    @Override
    public List<NaturalFlow> getNaturalFlowsGroupByProductAndDate(NaturalFlowQuery query) {
        List<NaturalFlow> naturalFlows = naturalFlowMapper.getNaturalFlowsGroupByProductAndDate(query);
        format(naturalFlows);
        return naturalFlows;
    }

    @Override
    public List<NaturalFlow> getNaturalFlowsGroupByDate(NaturalFlowQuery query) {
        List<NaturalFlow> naturalFlows = naturalFlowMapper.getNaturalFlowsGroupByDate(query);
        format(naturalFlows);
        return naturalFlows;
    }

    @Override
    public List<NaturalFlow> getNaturalFlowsGroupByProduct(NaturalFlowQuery query) {
        List<NaturalFlow> naturalFlows = naturalFlowMapper.getNaturalFlowsGroupByProduct(query);
        format(naturalFlows);
        return naturalFlows;
    }

    @Override
    public NaturalFlow getNaturalFlowTotal(NaturalFlowQuery query) {
        NaturalFlow naturalFlow = naturalFlowMapper.getNaturalFlowTotal(query);
        format(naturalFlow);
        return naturalFlow;
    }

    /**
     * 获取自然流量在所选日期范围的累计充值
     *
     * @param query
     * @return
     */
    @Override
    public BigDecimal getNaturalFlowRecharge(DateRangeQuery query) {
        return naturalFlowMapper.getNaturalFlowRecharge(query);
    }

    @Override
    public List<NaturalFlow> getNaturalFlowRechargeGroupByDate(DateRangeQuery query) {
        return naturalFlowMapper.getNaturalFlowRechargeGroupByDate(query);
    }

    /**
     * 获取自然流量在所选日期范围的累计充值,按产品分组.
     *
     * @param query
     * @return
     */
    @Override
    public List<NaturalFlow> getNaturalFlowRechargeGroupByProduct(DateRangeQuery query) {
        return naturalFlowMapper.getNaturalFlowRechargeGroupByProduct(query);
    }

    /**
     * 获取自然流量在所选日期范围的累计充值,按产品和日期分组.
     *
     * @param query
     * @return
     */
    @Override
    public List<NaturalFlow> getNaturalFlowRechargeGroupByProductAndDate(DateRangeQuery query) {
        return naturalFlowMapper.getNaturalFlowRechargeGroupByProductAndDate(query);
    }

    /**
     * 获取自然流量的回本率
     *
     * @param query
     * @return
     */
    @Override
    public Double getNaturalFlowPaybackRate(DateRangeQuery query) {
        BigDecimal cost = naturalFlowMapper.getRealTotalCost(query);
        BigDecimal recharge = naturalFlowMapper.getRealTotalRecharge(query);
        if (cost != null && recharge != null && recharge.compareTo(new BigDecimal(0)) > 0) {
            return recharge.divide(cost, 4, RoundingMode.HALF_UP).doubleValue();
        } else {
            return null;
        }
    }

    /**
     * 获取自然流量回本率，按产品分组
     *
     * @param query
     * @return
     */
    @Override
    public List<NaturalFlow> getNaturalFlowPaybackRateGroupByProduct(DateRangeQuery query) {
        List<NaturalFlow> naturalFlowCosts = naturalFlowMapper.getRealCostGroupByProduct(query);
        List<NaturalFlow> naturalFlowRecharges = naturalFlowMapper.getRealRechargeGroupByProduct(query);
        Map<Integer, NaturalFlow> recharges = groupByProductId(naturalFlowRecharges);
        List<NaturalFlow> naturalFlows = new ArrayList<>();
        if (naturalFlowCosts != null && naturalFlowRecharges != null) {
            for (NaturalFlow naturalFlow : naturalFlowCosts) {
                if (recharges.containsKey(naturalFlow.getProductId())) {
                    NaturalFlow naturalFlowPaybackRate = new NaturalFlow();
                    naturalFlowPaybackRate.setProductId(naturalFlow.getProductId());
                    naturalFlowPaybackRate.setProductName(naturalFlow.getProductName());
                    if (recharges.get(naturalFlow.getProductId()).getRecharge() != null && naturalFlow.getRealCost() != null && naturalFlow.getRealCost().compareTo(new BigDecimal(0)) > 0) {
                        naturalFlowPaybackRate.setPaybackRate(recharges.get(naturalFlow.getProductId()).getRecharge().divide(naturalFlow.getRealCost(), 4, RoundingMode.HALF_UP).doubleValue());
                    } else {
                        naturalFlowPaybackRate.setPaybackRate(0.0);
                    }
                    naturalFlows.add(naturalFlowPaybackRate);
                }
            }
        }
        return naturalFlows;
    }

    /**
     * 获取自然流量CPA，按产品分组
     *
     * @param query
     * @return
     */
    @Override
    public List<NaturalFlow> getNaturalFlowCpaGroupByProduct(NaturalFlowQuery query) {
        List<NaturalFlow> naturalFlowCosts = naturalFlowMapper.getCostGroupByProduct(query);
        List<NaturalFlow> naturalFlowRegistrations = naturalFlowMapper.getNaturalFlowsGroupByProduct(query);
        Map<Integer, NaturalFlow> registrations = groupByProductId(naturalFlowRegistrations);
        List<NaturalFlow> naturalFlows = new ArrayList<>();
        if (naturalFlowCosts != null && naturalFlowRegistrations != null) {
            for (NaturalFlow naturalFlowCost : naturalFlowCosts) {
                if (registrations.containsKey(naturalFlowCost.getProductId())) {
                    NaturalFlow naturalFlowCpa = new NaturalFlow();
                    naturalFlowCpa.setProductId(naturalFlowCost.getProductId());
                    naturalFlowCpa.setProductName(naturalFlowCost.getProductName());
                    naturalFlowCpa.setCpa(naturalFlowCost.getCost().divide(new BigDecimal(registrations.get(naturalFlowCost.getProductId()).getRegistrations()), 2, RoundingMode.HALF_UP));
                    naturalFlows.add(naturalFlowCpa);
                }
            }
        }
        return naturalFlows;
    }

    @Override
    public List<DailyIndicator> getNaturalFlowCpaGroupByDate(NaturalFlowQuery query) {
        List<NaturalFlow> naturalFlowCosts = naturalFlowMapper.getCostGroupByDate(query);
        List<NaturalFlow> naturalFlowRegistrations = naturalFlowMapper.getNaturalFlowsGroupByDate(query);
        Map<Date, NaturalFlow> naturalFlowByDate = groupByDate(naturalFlowRegistrations);
        List<DailyIndicator> dailyIndicators = new ArrayList<>();
        if (naturalFlowCosts != null && naturalFlowRegistrations != null) {
            for (NaturalFlow naturalFlowCost : naturalFlowCosts) {
                if (naturalFlowByDate.containsKey(naturalFlowCost.getStatDate())) {
                    NaturalFlow naturalFlowRegistration = naturalFlowByDate.get(naturalFlowCost.getStatDate());
                    DailyIndicator dailyIndicator = new DailyIndicator();
                    dailyIndicator.setDate(naturalFlowCost.getStatDate());
                    dailyIndicator.setName(NaturalFlowUtils.formatProductName(naturalFlowCost.getProductName()));
                    //CPA
                    dailyIndicator.setValue(naturalFlowCost.getCost().divide(new BigDecimal(naturalFlowRegistration.getRegistrations()), 2, RoundingMode.HALF_UP));
                    dailyIndicators.add(dailyIndicator);
                }
            }
        }
        return dailyIndicators;
    }

    /**
     * 获取自然流量CPA，按产品和日期分组
     *
     * @param query
     * @return
     */
    @Override
    public List<DailyIndicator> getNaturalFlowCpaGroupByProductAndDate(DateRangeQuery query) {
        List<NaturalFlow> naturalFlowCosts = naturalFlowMapper.getCostGroupByProductAndDate(query);
        NaturalFlowQuery naturalFlowQuery = new NaturalFlowQuery();
        naturalFlowQuery.setStartDate(query.getStartDate());
        naturalFlowQuery.setEndDate(query.getEndDate());
        naturalFlowQuery.setCompanyId(query.getCompanyId());
        List<NaturalFlow> naturalFlowRegistrations = naturalFlowMapper.getNaturalFlowsGroupByProductAndDate(naturalFlowQuery);
        Map<Integer, Map<Date, NaturalFlow>> naturalFlowsByProductAndDate = groupByProductIdAndDate(naturalFlowRegistrations);
        List<DailyIndicator> dailyIndicators = new ArrayList<>();
        if (naturalFlowCosts != null && naturalFlowRegistrations != null) {
            for (NaturalFlow naturalFlowCost : naturalFlowCosts) {
                if (naturalFlowsByProductAndDate.containsKey(naturalFlowCost.getProductId()) && naturalFlowsByProductAndDate.get(naturalFlowCost.getProductId()).containsKey(naturalFlowCost.getStatDate())) {
                    NaturalFlow naturalFlowRegistration = naturalFlowsByProductAndDate.get(naturalFlowCost.getProductId()).get(naturalFlowCost.getStatDate());
                    DailyIndicator dailyIndicator = new DailyIndicator();
                    dailyIndicator.setDate(naturalFlowCost.getStatDate());
                    dailyIndicator.setName(NaturalFlowUtils.formatProductName(naturalFlowCost.getProductName()));
                    //CPA
                    dailyIndicator.setValue(naturalFlowCost.getCost().divide(new BigDecimal(naturalFlowRegistration.getRegistrations()), 2, RoundingMode.HALF_UP));
                    dailyIndicators.add(dailyIndicator);
                }
            }
        }
        return dailyIndicators;
    }

    @Override
    public BigDecimal getTotalCost(DateRangeQuery query) {
        return CurrencyUtils.fenToYuan(naturalFlowMapper.getTotalCost(query));
    }

    @Override
    public List<NaturalFlow> getCostGroupByProduct(DateRangeQuery query) {
        List<NaturalFlow> naturalFlows = naturalFlowMapper.getCostGroupByProduct(query);
        format(naturalFlows);
        return naturalFlows;
    }

    @Override
    public Integer getNaturalFlowDau(DateRangeQuery query) {
        return naturalFlowMapper.getNaturalFlowDau(query);
    }

    @Override
    public List<NaturalFlow> getNaturalFlowDauGroupByProduct(DateRangeQuery query) {
        return naturalFlowMapper.getNaturalFlowDauGroupByProduct(query);
    }

    @Override
    public List<DailyIndicator> getNaturalFlowDauGroupByDate(DateRangeQuery query) {
        List<NaturalFlow> naturalFlows = naturalFlowMapper.getNaturalFlowDauGroupByDate(query);
        List<DailyIndicator> dailyIndicators = new ArrayList<>();
        if (naturalFlows != null) {
            for (NaturalFlow naturalFlow : naturalFlows) {
                if (naturalFlow.getDau() != null) {
                    DailyIndicator dailyIndicator = new DailyIndicator();
                    dailyIndicator.setDate(naturalFlow.getStatDate());
                    dailyIndicator.setName(NaturalFlowUtils.formatProductName(naturalFlow.getProductName()));
                    //DAU
                    dailyIndicator.setValue(new BigDecimal(naturalFlow.getDau()));
                    dailyIndicators.add(dailyIndicator);
                }
            }
        }
        return dailyIndicators;
    }

    @Override
    public List<DailyIndicator> getNaturalFlowDauGroupByProductAndDate(DateRangeQuery query) {
        List<DailyIndicator> dailyIndicators = naturalFlowMapper.getNaturalFlowDauGroupByProductAndDate(query);
        if (dailyIndicators != null) {
            for (DailyIndicator dailyIndicator : dailyIndicators) {
                dailyIndicator.setName(NaturalFlowUtils.formatProductName(dailyIndicator.getName()));
            }
        }
        return dailyIndicators;
    }

    @Override
    public List<DailyPaybackNaturalFlow> getDailyPaybackNaturalFlowsGroupByDate(NaturalFlowQuery query) {
        return naturalFlowMapper.getDailyPaybackNaturalFlowsGroupByDate(query);
    }

    @Override
    public List<DailyPaybackNaturalFlow> getDailyPaybackLtvNaturalFlowsGroupByDate(NaturalFlowQuery query) {
        return naturalFlowMapper.getDailyPaybackLtvNaturalFlowsGroupByDate(query);
    }

    @Override
    public List<MonthlyPaybackNaturalFlow> getMonthlyPaybackNaturalFlowsGroupByDate(NaturalFlowQuery query) {
        return naturalFlowMapper.getMonthlyPaybackNaturalFlowsGroupByDate(query);
    }

    /**
     * 获取自然流量每日充值，单位为分
     *
     * @param query
     * @return
     */
    @Override
    public List<NaturalFlow> dailyNaturalFlowPayAmount(NaturalFlowQuery query) {
        return naturalFlowMapper.dailyNaturalFlowPayAmount(query);
    }

    /**
     * 按产品ID分组
     *
     * @param naturalFlows
     * @return
     */
    private Map<Integer, NaturalFlow> groupByProductId(List<NaturalFlow> naturalFlows) {
        Map<Integer, NaturalFlow> map = new HashMap<>();
        if (naturalFlows != null) {
            for (NaturalFlow naturalFlow : naturalFlows) {
                map.put(naturalFlow.getProductId(), naturalFlow);
            }
        }
        return map;
    }

    /**
     * 按日期分组
     *
     * @param naturalFlows
     * @return
     */
    private Map<Date, NaturalFlow> groupByDate(List<NaturalFlow> naturalFlows) {
        Map<Date, NaturalFlow> naturalFlowsByDate = new HashMap<>();
        if (naturalFlows != null) {
            for (NaturalFlow naturalFlow : naturalFlows) {
                naturalFlowsByDate.put(naturalFlow.getStatDate(), naturalFlow);
            }
        }
        return naturalFlowsByDate;
    }

    /**
     * 按产品和日期分组
     *
     * @param naturalFlows
     * @return
     */
    private Map<Integer, Map<Date, NaturalFlow>> groupByProductIdAndDate(List<NaturalFlow> naturalFlows) {
        Map<Integer, Map<Date, NaturalFlow>> naturalFlowsByProduct = new HashMap<>();
        if (naturalFlows != null) {
            for (NaturalFlow naturalFlow : naturalFlows) {
                if (naturalFlowsByProduct.containsKey(naturalFlow.getProductId())) {
                    naturalFlowsByProduct.get(naturalFlow.getProductId()).put(naturalFlow.getStatDate(), naturalFlow);
                } else {
                    //按日期排序，使用TreeMap
                    Map<Date, NaturalFlow> naturalFlowsByDate = new TreeMap<>();
                    naturalFlowsByDate.put(naturalFlow.getStatDate(), naturalFlow);
                    naturalFlowsByProduct.put(naturalFlow.getProductId(), naturalFlowsByDate);
                }
            }
        }
        return naturalFlowsByProduct;
    }

    /**
     * 格式化自然流量指标
     *
     * @param naturalFlows
     */
    private void format(List<NaturalFlow> naturalFlows) {
        if (naturalFlows != null) {
            for (NaturalFlow naturalFlow : naturalFlows) {
                format(naturalFlow);
            }
        }
    }

    /**
     * 格式化自然流量指标
     *
     * @param naturalFlow
     */
    private void format(NaturalFlow naturalFlow) {
        if (naturalFlow != null) {
            naturalFlow.setCpa(CurrencyUtils.fenToYuan(naturalFlow.getCpa()));
            naturalFlow.setCost(CurrencyUtils.fenToYuan(naturalFlow.getCost()));
            naturalFlow.setRealCost(CurrencyUtils.fenToYuan(naturalFlow.getRealCost()));
            naturalFlow.setRecharge(CurrencyUtils.fenToYuan(naturalFlow.getRecharge()));
            naturalFlow.setNewlyIncreasedRechargeAmount(CurrencyUtils.fenToYuan(naturalFlow.getNewlyIncreasedRechargeAmount()));
            naturalFlow.setAccumulatedPayingAmount(CurrencyUtils.fenToYuan(naturalFlow.getAccumulatedPayingAmount()));
            naturalFlow.setTotalPayingAmount(CurrencyUtils.fenToYuan(naturalFlow.getTotalPayingAmount()));
        }
    }
}
