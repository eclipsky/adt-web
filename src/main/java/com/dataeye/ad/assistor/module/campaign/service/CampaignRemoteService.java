package com.dataeye.ad.assistor.module.campaign.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.RemoteInterfaceConstants.Tracking;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.constant.TrackingResponse;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.campaign.constants.TrackingResCampaign;
import com.dataeye.ad.assistor.module.campaign.constants.TrackingResChannelParams;
import com.dataeye.ad.assistor.module.campaign.constants.TrackingResTrackUrl;
import com.dataeye.ad.assistor.module.campaign.model.CampaignCreateReqVo;
import com.dataeye.ad.assistor.module.campaign.model.CampaignQueryReqVo;
import com.dataeye.ad.assistor.module.campaign.model.CampaignVo;
import com.dataeye.ad.assistor.module.systemaccount.model.PtLoginResponse;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

/**
 * 推广计划Service
 * @author luzhuyou 2017/06/19
 *
 */
@Service("campaignRemoteService")
public class CampaignRemoteService {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(CampaignRemoteService.class);
	
	/**
	 * 查询推广计划活动
	 * @param appId
	 * @param uid
	 * @param trackingMediumId
	 * @param campaignId
	 * @param startDate  (tracking接口时间以创建时间去查询)
	 * @param endDate
	 * @return
	 */
	public List<TrackingResCampaign> queryCampaign(CampaignQueryReqVo vo) {
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("appid", vo.getAppIds());
		params.put("uid", vo.getUid()+"");
		params.put("channelId", vo.getTrackingMediumIds() == null ? "":vo.getTrackingMediumIds());
		params.put("campaignId", vo.getTrackingCampaignIds() == null ? "":vo.getTrackingCampaignIds());
		params.put("startDate", vo.getStartDate() == null ? "":DateUtils.format(vo.getStartDate()));
		params.put("endDate", vo.getEndDate() == null ? "":DateUtils.format(vo.getEndDate()));
		params.put("adtAccountId", vo.getAdtAccountIds());
		
		String queryCampaignInterface = ConfigHandler.getProperty(Tracking.QUERY_CAMPAIGN_KEY, Tracking.QUERY_CAMPAIGN);
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(queryCampaignInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("Tracking查询推广计划接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.error("Tracking查询推广计划接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	
    	TrackingResponse<TrackingResCampaign> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<TrackingResponse<TrackingResCampaign>>(){}.getType());
    	
        return response.getContent();
	}
	
	/**
	 * 查询渠道参数
	 * @param appId
	 * @param uid
	 * @param osType
	 * @param trackingMediumId
	 * @return
	 */
	public TrackingResChannelParams getChannelParams(String appId, int uid, int osType, String trackingMediumId) {
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("appid", appId);
		params.put("uid", uid+"");
		params.put("platform", osType+"");
		params.put("channelId", trackingMediumId);
		
		String preCreateCampaignInterface = ConfigHandler.getProperty(Tracking.GET_CHANNEL_PARAMS_KEY, Tracking.GET_CHANNEL_PARAMS);
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(preCreateCampaignInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("Tracking查询渠道参数接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.error("Tracking查询渠道参数接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	PtLoginResponse<TrackingResChannelParams> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<PtLoginResponse<TrackingResChannelParams>>(){}.getType());
    	if(response == null || response.getContent() == null) {
        	logger.info("查询渠道参数失败.");
        	ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
        return response.getContent();
    	
	}
	
	/**
	 * 预创建推广链接
	 * @param vo
	 * @return
	 */
	public TrackingResTrackUrl preCreateCampaign(CampaignCreateReqVo vo) {
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("appid", vo.getAppId());
		params.put("uid", vo.getCompanyAccountUid()+"");
		params.put("campaignName", vo.getCampaignName());
		params.put("platform", vo.getOsType()+"");
		params.put("channelId", vo.getTrackingMediumId()+"");
		params.put("pageUrl", vo.getDownloadUrl());
		params.put("adtAccountId", vo.getSystemAccountId()+"");
		
		String preCreateCampaignInterface = ConfigHandler.getProperty(Tracking.ADD_CAMPAIGN_BEFORE_KEY, Tracking.ADD_CAMPAIGN_BEFORE);
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(preCreateCampaignInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("Tracking预创建推广链接接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.error("Tracking预创建推广链接接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		if( statusCode == 406 ){  //计划名称已经存在
        		ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_NAME_IS_EXISTS);
        	}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	
    	TrackingResponse<TrackingResTrackUrl> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<TrackingResponse<TrackingResTrackUrl>>(){}.getType());
    	
        if(response == null || response.getContent() == null || response.getContent().isEmpty()) {
        	logger.info("预创建推广链接失败.");
        	ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
        return response.getContent().get(0);
	}
	
	/**
	 * 创建推广计划
	 * @param campaign
	 * @return
	 */
	public List<TrackingResTrackUrl> createCampaign(CampaignCreateReqVo campaign) {
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("appid", campaign.getAppId());
		params.put("uid", campaign.getCompanyAccountUid()+"");
		params.put("campaignName", campaign.getCampaignName());
		params.put("platform", campaign.getOsType()+"");
		params.put("channelId", campaign.getTrackingMediumId()+"");
		params.put("pageUrl", campaign.getDownloadUrl());
		params.put("urlCount", campaign.getUrlCount()+"");
		params.put("campaignId", campaign.getCampaignId());
		params.put("trackUrl", campaign.getTrackUrl());
		params.put("thirdAdParams", campaign.getThirdAdParams());
		params.put("adtAccountId", campaign.getSystemAccountId()+"");
		params.put("eventType", campaign.getEventType()==null?"ACTIVE":campaign.getEventType());
		
		String preCreateCampaignInterface = ConfigHandler.getProperty(Tracking.ADD_CAMPAIGN_KEY, Tracking.ADD_CAMPAIGN);
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(preCreateCampaignInterface, params);
    	logger.error("Tracking创建推广计划接口发起请求，地址[{}]，参数[{}]", new Object[]{preCreateCampaignInterface, params});
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("Tracking创建推广计划接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	
    	if(statusCode != 200) {
    		
    		logger.error("Tracking创建推广计划接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		
    		if( statusCode == 406 ){  //计划名称已经存在
        		ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_NAME_IS_EXISTS);
        	}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	
    	TrackingResponse<TrackingResTrackUrl> response = StringUtil.gson.fromJson(httpResponse, new TypeToken<TrackingResponse<TrackingResTrackUrl>>(){}.getType());
    	
        if(response == null || response.getContent() == null || response.getContent().isEmpty()) {
        	logger.info("创建推广计划失败.");
        	ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
        return response.getContent();
	}
	
	/**
	 * 修改推广计划
	 * @param vo
	 * @return
	 */
	public int modifyCampaign(CampaignVo vo) {
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("appid", vo.getAppId());
		params.put("uid", vo.getCompanyAccountUid()+"");
		params.put("campaignId", vo.getCampaignId());
		params.put("campaignName", vo.getCampaignName());
		params.put("pageUrl", vo.getDownloadUrl());
		params.put("thirdAdParams", vo.getThirdAdParams() == null ? "" : vo.getThirdAdParams());
		
		String modifyCampaignInterface = ConfigHandler.getProperty(Tracking.MODIFY_CAMPAIGN_KEY, Tracking.MODIFY_CAMPAIGN);
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(modifyCampaignInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("Tracking修改推广计划接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	
    	if(statusCode != 200) {
    		logger.error("Tracking修改推广计划接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		if( statusCode == 406 ){  //计划名称已经存在
        		ExceptionHandler.throwParameterException(StatusCode.CAMP_CAMPAIGN_NAME_IS_EXISTS);
        	}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	
        return 1;
	}
	
	/**
	 * 删除推广计划
	 * @param appId
	 * @param uid
	 * @param campaignId
	 * @return
	 */
	public int deleteCampaign(String appId, int uid, String campaignId) {
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("appid", appId);
		params.put("uid", uid+"");
		params.put("campaignId", campaignId);
		
		String deleteCampaignInterface = ConfigHandler.getProperty(Tracking.DELETE_CAMPAIGN_KEY, Tracking.DELETE_CAMPAIGN);
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(deleteCampaignInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("Tracking删除推广计划接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.error("Tracking删除推广计划接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	
        return 1;
	}
}
