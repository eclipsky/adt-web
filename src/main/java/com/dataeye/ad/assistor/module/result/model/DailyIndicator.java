package com.dataeye.ad.assistor.module.result.model;

import java.util.Date;

/**
 * Created by huangzehai on 2017/4/7.
 */
public class DailyIndicator extends Indicator {
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
