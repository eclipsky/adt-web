package com.dataeye.ad.assistor.module.debugcenter.controller;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.*;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.debugcenter.model.ChannelDebugDevice;
import com.dataeye.ad.assistor.module.debugcenter.model.DebugCenterParameter;
import com.dataeye.ad.assistor.module.debugcenter.service.DebugCenterService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 调试中心控制器
 *
 * @author lingliqi
 * @date 2018-01-18 10:18
 */
@Controller
public class DebugCenterController {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(DebugCenterController.class);

    @Autowired
    private DebugCenterService debugCenterService;

    /**
     * 清除调试设备信息
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/debugCenter/clearDebugDevice.do")
    public Object clearDebugDevice(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("清除调试设备信息");
        DEContext context = DEContextContainer.getContext(request);
        DebugCenterParameter debugCenterParameter = parseDebugCenterParameter(context);
        return debugCenterService.clearDebugDevice(debugCenterParameter);

    }


    /**
     * 归因查询
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/debugCenter/debugDeviceMatch.do")
    public Object debugDeviceMatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("调试设备归因查询");
        DEContext context = DEContextContainer.getContext(request);
        DebugCenterParameter debugCenterParameter = parseDebugCenterParameter(context);
        return debugCenterService.debugDeviceMatch(debugCenterParameter);
    }


    /**
     * 查询媒体联调测试设备列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/debugCenter/queryChannelDebugDevice.do")
    public Object queryChannelDebugDevice(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("媒体联调-查询测试设备列表");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int uid = userInSession.getCompanyAccountUid();
        List<ChannelDebugDevice> result = debugCenterService.queryChannelDebugDevice(uid);
        PageData pageData = PageDataHelper.getPageDataByList(parameter.getPageSize(), parameter.getPageId(), result);
        return pageData;
    }

    /**
     * 新增媒体联调测试设备
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/debugCenter/addChannelDebugDevice.do")
    public Object addChannelDebugDevice(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("媒体联调-新增联调测试设备");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int uid = userInSession.getCompanyAccountUid();
        String deviceName = parameter.getParameter("deviceName");
        String deviceId = parameter.getParameter("deviceId");
        if (StringUtils.isBlank(deviceName)) {
            ExceptionHandler.throwParameterException(StatusCode.DEBUG_DEVICE_NAME_IS_NULL);
        }
        if (StringUtils.isBlank(deviceId)) {
            ExceptionHandler.throwParameterException(StatusCode.DEBUG_DEVICE_IS_NULL);
        }
        return debugCenterService.addChannelDebugDevice(deviceName, deviceId, uid);
    }

    /**
     * 删除媒体联调测试设备
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/debugCenter/deleteChannelDebugDevice.do")
    public Object deleteChannelDebugDevice(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("媒体联调-删除联调测试设备");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int uid = userInSession.getCompanyAccountUid();
        String deviceId = parameter.getParameter("deviceId");
        if (StringUtils.isBlank(deviceId)) {
            ExceptionHandler.throwParameterException(StatusCode.DEBUG_DEVICE_IS_NULL);
        }
        return debugCenterService.deleteChannelDebugDevice(deviceId, uid);
    }


    public DebugCenterParameter parseDebugCenterParameter(DEContext context) {
        DebugCenterParameter debugCenterParameter = new DebugCenterParameter();
        DEParameter parameter = context.getDeParameter();
        String appid = parameter.getParameter("appid");
        String device = parameter.getParameter("device");
        if (StringUtils.isBlank(appid)) {
            ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ID_ERROR);
        }
        if (StringUtils.isBlank(device)) {
            ExceptionHandler.throwParameterException(StatusCode.DEBUG_DEVICE_IS_NULL);
        }
        debugCenterParameter.setAppid(appid);
        debugCenterParameter.setDevice(device);
        return debugCenterParameter;
    }


}
