package com.dataeye.ad.assistor.module.payback.service;

import com.dataeye.ad.assistor.module.payback.model.*;

import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2017/1/13.
 */
public interface PaybackReportService {
    /**
     * 回本日报.
     *
     * @return
     */
    List<DailyPaybackReportView> dailyPaybackReport(PaybackQuery paybackQuery);

    /**
     * 日回本率趋势
     *
     * @param query
     * @return
     */
    List<PaybackRate> dailyPaybackRateTrend(PaybackTrendQuery query);

    /**
     * 回本月报.
     *
     * @return
     */
    List<MonthlyPaybackReportView> monthlyPaybackReport(PaybackQuery paybackQuery);

}
