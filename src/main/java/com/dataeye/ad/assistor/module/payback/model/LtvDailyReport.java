package com.dataeye.ad.assistor.module.payback.model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;

/**
 * LTV按天人分析
 */
public class LtvDailyReport {
    /**
     * 日期.
     */
    @Expose
    private String date;

    /**
     * 新增注册
     */
    @Expose
    private Integer registrations;

    /**
     * 当日LTV
     */
    @Expose
    private BigDecimal ltv1Day;

    /**
     * 2日LTV
     */
    @Expose
    private BigDecimal ltv2Days;

    /**
     * 3日LTV
     */
    @Expose
    private BigDecimal ltv3Days;

    /**
     * 4日LTV
     */
    @Expose
    private BigDecimal ltv4Days;

    /**
     * 5日LTV
     */
    @Expose
    private BigDecimal ltv5Days;

    /**
     * 6日LTV
     */
    @Expose
    private BigDecimal ltv6Days;

    /**
     * 7日LTV
     */
    @Expose
    private BigDecimal ltv7Days;

    /**
     * 15日LTV
     */
    @Expose
    private BigDecimal ltv15Days;

    /**
     * 30日LTV
     */
    @Expose
    private BigDecimal ltv30Days;

    /**
     * 60日LTV
     */
    @Expose
    private BigDecimal ltv60Days;

    /**
     * 90日LTV
     */
    @Expose
    private BigDecimal ltv90Days;


    /**
     * 首日充值
     */
    @Expose
    private BigDecimal payAmount1Day;

    /**
     * 2日充值
     */
    @Expose
    private BigDecimal payAmount2Days;

    /**
     * 3日充值
     */
    @Expose
    private BigDecimal payAmount3Days;

    /**
     * 4日充值
     */
    @Expose
    private BigDecimal payAmount4Days;

    /**
     * 5日充值
     */
    @Expose
    private BigDecimal payAmount5Days;

    /**
     * 6日充值
     */
    @Expose
    private BigDecimal payAmount6Days;

    /**
     * 7日充值
     */
    @Expose
    private BigDecimal payAmount7Days;

    /**
     * 15日充值
     */
    @Expose
    private BigDecimal payAmount15Days;

    /**
     * 30日充值
     */
    @Expose
    private BigDecimal payAmount30Days;

    /**
     * 60日充值
     */
    @Expose
    private BigDecimal payAmount60Days;

    /**
     * 90日充值
     */
    @Expose
    private BigDecimal payAmount90Days;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getRegistrations() {
        return registrations;
    }

    public void setRegistrations(Integer registrations) {
        this.registrations = registrations;
    }

    public BigDecimal getLtv1Day() {
        return ltv1Day;
    }

    public void setLtv1Day(BigDecimal ltv1Day) {
        this.ltv1Day = ltv1Day;
    }

    public BigDecimal getLtv2Days() {
        return ltv2Days;
    }

    public void setLtv2Days(BigDecimal ltv2Days) {
        this.ltv2Days = ltv2Days;
    }

    public BigDecimal getLtv3Days() {
        return ltv3Days;
    }

    public void setLtv3Days(BigDecimal ltv3Days) {
        this.ltv3Days = ltv3Days;
    }

    public BigDecimal getLtv4Days() {
        return ltv4Days;
    }

    public void setLtv4Days(BigDecimal ltv4Days) {
        this.ltv4Days = ltv4Days;
    }

    public BigDecimal getLtv5Days() {
        return ltv5Days;
    }

    public void setLtv5Days(BigDecimal ltv5Days) {
        this.ltv5Days = ltv5Days;
    }

    public BigDecimal getLtv6Days() {
        return ltv6Days;
    }

    public void setLtv6Days(BigDecimal ltv6Days) {
        this.ltv6Days = ltv6Days;
    }

    public BigDecimal getLtv7Days() {
        return ltv7Days;
    }

    public void setLtv7Days(BigDecimal ltv7Days) {
        this.ltv7Days = ltv7Days;
    }

    public BigDecimal getLtv15Days() {
        return ltv15Days;
    }

    public void setLtv15Days(BigDecimal ltv15Days) {
        this.ltv15Days = ltv15Days;
    }

    public BigDecimal getLtv30Days() {
        return ltv30Days;
    }

    public void setLtv30Days(BigDecimal ltv30Days) {
        this.ltv30Days = ltv30Days;
    }

    public BigDecimal getLtv60Days() {
        return ltv60Days;
    }

    public void setLtv60Days(BigDecimal ltv60Days) {
        this.ltv60Days = ltv60Days;
    }

    public BigDecimal getLtv90Days() {
        return ltv90Days;
    }

    public void setLtv90Days(BigDecimal ltv90Days) {
        this.ltv90Days = ltv90Days;
    }

    public BigDecimal getPayAmount1Day() {
        return payAmount1Day;
    }

    public void setPayAmount1Day(BigDecimal payAmount1Day) {
        this.payAmount1Day = payAmount1Day;
    }

    public BigDecimal getPayAmount2Days() {
        return payAmount2Days;
    }

    public void setPayAmount2Days(BigDecimal payAmount2Days) {
        this.payAmount2Days = payAmount2Days;
    }

    public BigDecimal getPayAmount3Days() {
        return payAmount3Days;
    }

    public void setPayAmount3Days(BigDecimal payAmount3Days) {
        this.payAmount3Days = payAmount3Days;
    }

    public BigDecimal getPayAmount4Days() {
        return payAmount4Days;
    }

    public void setPayAmount4Days(BigDecimal payAmount4Days) {
        this.payAmount4Days = payAmount4Days;
    }

    public BigDecimal getPayAmount5Days() {
        return payAmount5Days;
    }

    public void setPayAmount5Days(BigDecimal payAmount5Days) {
        this.payAmount5Days = payAmount5Days;
    }

    public BigDecimal getPayAmount6Days() {
        return payAmount6Days;
    }

    public void setPayAmount6Days(BigDecimal payAmount6Days) {
        this.payAmount6Days = payAmount6Days;
    }

    public BigDecimal getPayAmount7Days() {
        return payAmount7Days;
    }

    public void setPayAmount7Days(BigDecimal payAmount7Days) {
        this.payAmount7Days = payAmount7Days;
    }

    public BigDecimal getPayAmount15Days() {
        return payAmount15Days;
    }

    public void setPayAmount15Days(BigDecimal payAmount15Days) {
        this.payAmount15Days = payAmount15Days;
    }

    public BigDecimal getPayAmount30Days() {
        return payAmount30Days;
    }

    public void setPayAmount30Days(BigDecimal payAmount30Days) {
        this.payAmount30Days = payAmount30Days;
    }

    public BigDecimal getPayAmount60Days() {
        return payAmount60Days;
    }

    public void setPayAmount60Days(BigDecimal payAmount60Days) {
        this.payAmount60Days = payAmount60Days;
    }

    public BigDecimal getPayAmount90Days() {
        return payAmount90Days;
    }

    public void setPayAmount90Days(BigDecimal payAmount90Days) {
        this.payAmount90Days = payAmount90Days;
    }

    @Override
    public String toString() {
        return "LtvDailyReport{" +
                "date='" + date + '\'' +
                ", registrations=" + registrations +
                ", ltv1Day=" + ltv1Day +
                ", ltv2Days=" + ltv2Days +
                ", ltv3Days=" + ltv3Days +
                ", ltv4Days=" + ltv4Days +
                ", ltv5Days=" + ltv5Days +
                ", ltv6Days=" + ltv6Days +
                ", ltv7Days=" + ltv7Days +
                ", ltv15Days=" + ltv15Days +
                ", ltv30Days=" + ltv30Days +
                ", ltv60Days=" + ltv60Days +
                ", ltv90Days=" + ltv90Days +
                '}';
    }
}
