package com.dataeye.ad.assistor.module.product.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.product.model.CpShareRate;

/**
 * 产品CP分成率映射器.
 * Created by luzhuyou 2017/02/20
 */
@MapperScan
public interface CpShareRateMapper {

	/**
	 * 新增CP分成率
	 * @param cpShareRate
	 * @return
	 */
	public int add(CpShareRate cpShareRate);

	/**
	 * 修改CP分成率
	 * @param cpShareRate
	 * @return
	 */
	public int update(CpShareRate cpShareRate);

}
