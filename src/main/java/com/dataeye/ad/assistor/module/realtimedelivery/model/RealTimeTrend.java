package com.dataeye.ad.assistor.module.realtimedelivery.model;

import java.math.BigDecimal;

/**
 * Created by huangzeahai on 2017/2/24.
 */
public class RealTimeTrend extends Time implements Cloneable {
    /**
     * 消耗
     */
    private BigDecimal cost = new BigDecimal(0);
    /**
     * 曝光
     */
    private long exposures;
    /**
     * 点击数
     */
    private long clicks;
    /**
     * 点击通过率CTR
     */
    private double ctr;

    /**
     * 到达数.
     */
    private long reaches;
    /**
     * 下载数
     */
    private long downloads;
    /**
     * 下载率
     */
    private double downloadRate;
    /**
     * 激活数.
     */
    private long activations;

    /**
     * 激活率.
     */
    private double activationRate;
    /**
     * 激活CPA.
     */
    private BigDecimal activationCpa = new BigDecimal(0);
    /**
     * 首日注册设备数
     */
    private long firstDayRegistrations;
    /**
     * 注册率.
     */
    private Double registrationRateFd;
    /**
     * 首日注册设备CPA
     */
    private BigDecimal costPerRegistrationFd=new BigDecimal(0);

    /**
     * 下载单价
     */
    private BigDecimal costPerDownload = new BigDecimal(0);

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public long getExposures() {
        return exposures;
    }

    public void setExposures(long exposures) {
        this.exposures = exposures;
    }

    public long getClicks() {
        return clicks;
    }

    public void setClicks(long clicks) {
        this.clicks = clicks;
    }

    public double getCtr() {
        return ctr;
    }

    public void setCtr(double ctr) {
        this.ctr = ctr;
    }

    public long getReaches() {
        return reaches;
    }

    public void setReaches(long reaches) {
        this.reaches = reaches;
    }

    public long getDownloads() {
        return downloads;
    }

    public void setDownloads(long downloads) {
        this.downloads = downloads;
    }

    public double getDownloadRate() {
        return downloadRate;
    }

    public void setDownloadRate(double downloadRate) {
        this.downloadRate = downloadRate;
    }

    public long getActivations() {
        return activations;
    }

    public void setActivations(long activations) {
        this.activations = activations;
    }

    public double getActivationRate() {
        return activationRate;
    }

    public void setActivationRate(double activationRate) {
        this.activationRate = activationRate;
    }

    public BigDecimal getActivationCpa() {
        return activationCpa;
    }

    public void setActivationCpa(BigDecimal activationCpa) {
        this.activationCpa = activationCpa;
    }

    public long getFirstDayRegistrations() {
        return firstDayRegistrations;
    }

    public void setFirstDayRegistrations(long firstDayRegistrations) {
        this.firstDayRegistrations = firstDayRegistrations;
    }

    public Double getRegistrationRateFd() {
        return registrationRateFd;
    }

    public void setRegistrationRateFd(Double registrationRateFd) {
        this.registrationRateFd = registrationRateFd;
    }

    public BigDecimal getCostPerRegistrationFd() {
        return costPerRegistrationFd;
    }

    public void setCostPerRegistrationFd(BigDecimal costPerRegistrationFd) {
        this.costPerRegistrationFd = costPerRegistrationFd;
    }

    public BigDecimal getCostPerDownload() {
        return costPerDownload;
    }

    public void setCostPerDownload(BigDecimal costPerDownload) {
        this.costPerDownload = costPerDownload;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return "RealTimeTrend{" +
                "cost=" + cost +
                ", exposures=" + exposures +
                ", clicks=" + clicks +
                ", ctr=" + ctr +
                ", reaches=" + reaches +
                ", downloads=" + downloads +
                ", downloadRate=" + downloadRate +
                ", activations=" + activations +
                ", activationRate=" + activationRate +
                ", activationCpa=" + activationCpa +
                ", firstDayRegistrations=" + firstDayRegistrations +
                ", registrationRateFd=" + registrationRateFd +
                ", costPerRegistrationFd=" + costPerRegistrationFd +
                ", costPerDownload=" + costPerDownload +
                '}';
    }
}
