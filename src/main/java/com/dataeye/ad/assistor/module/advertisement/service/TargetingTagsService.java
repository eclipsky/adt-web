package com.dataeye.ad.assistor.module.advertisement.service;


import com.dataeye.ad.assistor.module.advertisement.mapper.ApiTxAdTargetingMapper;
import com.dataeye.ad.assistor.module.advertisement.model.*;
import com.dataeye.ad.assistor.module.advertisement.service.tencent.TargetingService;
import com.dataeye.ad.assistor.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lingliqi
 * @date 2017-12-01 10:33
 */
@Service("targetTagsService")
public class TargetingTagsService {

    @Autowired
    private ApiTxAdGroupService apiTxAdGroupService;

    @Autowired
    private TargetingService targetingService;

    @Autowired
    private ApiTxAdTargetingMapper apiTxAdTargetingMapper;

    private Logger logger = LoggerFactory.getLogger(TargetingTagsService.class);

    private final static int IS_PUBLIC = 1;


    /**
     * 查询定向条件
     *
     * @param mediumAccountId
     * @param targetingId
     * @return
     */
    public List<TargetingVO> queryTargeting(Integer mediumAccountId, Integer targetingId) {
        List<TargetingVO> responseResult = new ArrayList<>();
        MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
        int mAccountId = 0;
        String accessToken = "";
        if (tAccountVo != null) {
            mAccountId = tAccountVo.getAccountId();
            accessToken = tAccountVo.getAccessToken();
        }
        responseResult = targetingService.query(mAccountId, targetingId, accessToken);
        return responseResult;
    }

    /**
     * 新增定向
     *
     * @param mediumAccountId
     * @param targetingVO
     * @return
     */
    public Integer addTargeting(Integer mediumAccountId, TargetingVO targetingVO, Integer isPublic) {
        MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
        int mAccountId = 0;
        String accessToken = "";
        if (tAccountVo != null) {
            mAccountId = tAccountVo.getAccountId();
            accessToken = tAccountVo.getAccessToken();
        }
        String targetingName = targetingVO.getTargetingName();
        String targeting = null;
        if (targetingVO.getTargeting() != null) {
            targeting = StringUtil.gson.toJson(targetingVO.getTargeting());
        }
        String description = targetingVO.getDescription();
        Integer id = targetingService.add(mAccountId, targetingName, targeting, description, accessToken);
        if (null != id) {
            apiTxAdTargetingMapper.add(new TargetingSelectVo(id, targetingName, isPublic, mediumAccountId, new Date(), new Date()));
        }
        return id;

    }

    /**
     * 编辑定向
     *
     * @param mediumAccountId
     * @param targetingVO
     * @return
     */
    public Integer modifyTargeting(Integer mediumAccountId, TargetingVO targetingVO) {
        MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
        int mAccountId = 0;
        String accessToken = "";
        if (tAccountVo != null) {
            mAccountId = tAccountVo.getAccountId();
            accessToken = tAccountVo.getAccessToken();
        }
        Integer targetingId = targetingVO.getTargetingId();
        String targetingName = targetingVO.getTargetingName();
        String targeting = StringUtil.gson.toJson(targetingVO.getTargeting());
        String description = targetingVO.getDescription();
        int isPublic = targetingVO.getIsPublic();
        Integer id = targetingService.update(mAccountId, targetingId, targetingName, targeting, description, accessToken);
        if (null != id) {
            TargetingSelectVo selectVo = new TargetingSelectVo();
            selectVo.setTargetingId(id);
            selectVo.setTargetingName(targetingName);
            selectVo.setMediumAccountId(mediumAccountId);
            selectVo.setIsPublic(isPublic);
            selectVo.setUpdateTime(new Date());
            apiTxAdTargetingMapper.update(selectVo);
        }
        return id;
    }


    /**
     * 删除定向
     *
     * @param mediumAccountId
     * @param targetingId
     * @return
     */
    public Integer deleteTargeting(Integer mediumAccountId, Integer targetingId) {
        MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
        int mAccountId = 0;
        String accessToken = "";
        if (tAccountVo != null) {
            mAccountId = tAccountVo.getAccountId();
            accessToken = tAccountVo.getAccessToken();
        }
        Integer id = targetingService.delete(mAccountId, targetingId, accessToken);
        if (null != id) {
            apiTxAdTargetingMapper.delete(mediumAccountId, targetingId);
        }
        return id;
    }

    /**
     * 查询定向下拉框
     **/
    public List<TargetingSelectVo> queryTargetingSelect(Integer mediumAccountId) {
        return apiTxAdTargetingMapper.queryForSelect(mediumAccountId, IS_PUBLIC);
    }

    /**
     * 定向名称校验
     *
     * @param mediumAccountId
     * @param targetingName
     * @return
     */
    public boolean checkTargetingName(Integer mediumAccountId, String targetingName, Integer targetingId) {
        // 名称长度为120字节，在UTF编码下，最多为40字符
        final int maxLength = 40;
        Integer existRecord = apiTxAdTargetingMapper.getRecordByTargetingName(mediumAccountId, targetingName, targetingId);
        if (targetingName.length() <= maxLength && null == existRecord) {
            return true;
        } else {
            return false;
        }


    }


}
