package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.List;

import com.google.gson.annotations.Expose;

public class MediumAndAccountVo {
	
	/** 媒体ID */
	@Expose
	private Integer mediumId;
	/** 媒体名称 */
	@Expose
	private String mediumName;
	/** 媒体帐号列表*/
	@Expose
	private List<MediumAccount> mediumAccountList;
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}

	public List<MediumAccount> getMediumAccountList() {
		return mediumAccountList;
	}
	public void setMediumAccountList(List<MediumAccount> mediumAccountList) {
		this.mediumAccountList = mediumAccountList;
	}
	@Override
	public String toString() {
		return "MediumVo [mediumId=" + mediumId + ", mediumName=" + mediumName
				+ ", mediumAccountList=" + mediumAccountList + "]";
	}
	
	
}
