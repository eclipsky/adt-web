package com.dataeye.ad.assistor.module.payback.model;

import java.math.BigDecimal;

/**
 * 回本日报
 * Created by huangzehai on 2017/1/12.
 */
public class DailyPaybackReport extends BasicPaybackReport {

    /**
     * DAU.
     */
    private Integer dau;

    /**
     * 首日充值
     */
    private BigDecimal payAmount1Day;

    /**
     * 3日充值
     */
    private BigDecimal payAmount3Days;

    /**
     * 7日充值
     */
    private BigDecimal payAmount7Days;

    /**
     * 15日充值
     */
    private BigDecimal payAmount15Days;

    /**
     * 30日充值
     */
    private BigDecimal payAmount30Days;

    /**
     * 60日充值
     */
    private BigDecimal payAmount60Days;

    /**
     * 90日充值
     */
    private BigDecimal payAmount90Days;
    /**
     * 2日充值
     */
    private BigDecimal payAmount2Days;
    /**
     * 4日充值
     */
    private BigDecimal payAmount4Days;
    /**
     * 5日充值
     */
    private BigDecimal payAmount5Days;
    /**
     * 6日充值
     */
    private BigDecimal payAmount6Days;
    

    public Integer getDau() {
        return dau;
    }

    public void setDau(Integer dau) {
        this.dau = dau;
    }

    public BigDecimal getPayAmount1Day() {
        return payAmount1Day;
    }

    public void setPayAmount1Day(BigDecimal payAmount1Day) {
        this.payAmount1Day = payAmount1Day;
    }

    public BigDecimal getPayAmount3Days() {
        return payAmount3Days;
    }

    public void setPayAmount3Days(BigDecimal payAmount3Days) {
        this.payAmount3Days = payAmount3Days;
    }

    public BigDecimal getPayAmount7Days() {
        return payAmount7Days;
    }

    public void setPayAmount7Days(BigDecimal payAmount7Days) {
        this.payAmount7Days = payAmount7Days;
    }

    public BigDecimal getPayAmount15Days() {
        return payAmount15Days;
    }

    public void setPayAmount15Days(BigDecimal payAmount15Days) {
        this.payAmount15Days = payAmount15Days;
    }

    public BigDecimal getPayAmount30Days() {
        return payAmount30Days;
    }

    public void setPayAmount30Days(BigDecimal payAmount30Days) {
        this.payAmount30Days = payAmount30Days;
    }

    public BigDecimal getPayAmount60Days() {
        return payAmount60Days;
    }

    public void setPayAmount60Days(BigDecimal payAmount60Days) {
        this.payAmount60Days = payAmount60Days;
    }

    public BigDecimal getPayAmount90Days() {
        return payAmount90Days;
    }

    public void setPayAmount90Days(BigDecimal payAmount90Days) {
        this.payAmount90Days = payAmount90Days;
    }

    public BigDecimal getPayAmount2Days() {
		return payAmount2Days;
	}

	public void setPayAmount2Days(BigDecimal payAmount2Days) {
		this.payAmount2Days = payAmount2Days;
	}

	public BigDecimal getPayAmount4Days() {
		return payAmount4Days;
	}

	public void setPayAmount4Days(BigDecimal payAmount4Days) {
		this.payAmount4Days = payAmount4Days;
	}

	public BigDecimal getPayAmount5Days() {
		return payAmount5Days;
	}

	public void setPayAmount5Days(BigDecimal payAmount5Days) {
		this.payAmount5Days = payAmount5Days;
	}

	public BigDecimal getPayAmount6Days() {
		return payAmount6Days;
	}

	public void setPayAmount6Days(BigDecimal payAmount6Days) {
		this.payAmount6Days = payAmount6Days;
	}

	@Override
	public String toString() {
		return "DailyPaybackReport [dau=" + dau + ", payAmount1Day="
				+ payAmount1Day + ", payAmount3Days=" + payAmount3Days
				+ ", payAmount7Days=" + payAmount7Days + ", payAmount15Days="
				+ payAmount15Days + ", payAmount30Days=" + payAmount30Days
				+ ", payAmount60Days=" + payAmount60Days + ", payAmount90Days="
				+ payAmount90Days + ", payAmount2Days=" + payAmount2Days
				+ ", payAmount4Days=" + payAmount4Days + ", payAmount5Days="
				+ payAmount5Days + ", payAmount6Days=" + payAmount6Days + "]";
	}
}
