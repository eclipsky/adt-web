package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.Arrays;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.google.gson.annotations.Expose;

/**
 * Created by luzhuyou
 * 媒体账号下拉框核心信息
 */
public class MediumAccountSelectorVo extends DataPermissionDomain {
    
    /** 媒体账户ID */
	@Expose
	private Integer mediumAccountId;
	/** 媒体名称 */
	@Expose
	private String mediumAccount;
	/** 产品ID数组 */
	private String[] productIds;
	/** 媒体ID数组 */
	private String[] mediumIds;
	/** 媒体账号别名 */
	@Expose
	private String mediumAccountAlias;
	private Integer loginStatus;
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public String getMediumAccount() {
		return mediumAccount;
	}
	public void setMediumAccount(String mediumAccount) {
		this.mediumAccount = mediumAccount;
	}
	public String[] getProductIds() {
		return productIds;
	}
	public void setProductIds(String[] productIds) {
		this.productIds = productIds;
	}
	public String[] getMediumIds() {
		return mediumIds;
	}
	public void setMediumIds(String[] mediumIds) {
		this.mediumIds = mediumIds;
	}
	public String getMediumAccountAlias() {
		return mediumAccountAlias;
	}
	public void setMediumAccountAlias(String mediumAccountAlias) {
		this.mediumAccountAlias = mediumAccountAlias;
	}
	public Integer getLoginStatus() {
		return loginStatus;
	}
	public void setLoginStatus(Integer loginStatus) {
		this.loginStatus = loginStatus;
	}
	@Override
	public String toString() {
		return "MediumAccountSelectorVo [mediumAccountId=" + mediumAccountId
				+ ", mediumAccount=" + mediumAccount + ", productIds="
				+ Arrays.toString(productIds) + ", mediumIds="
				+ Arrays.toString(mediumIds) + ", mediumAccountAlias="
				+ mediumAccountAlias + ", loginStatus=" + loginStatus + "]";
	}

}
