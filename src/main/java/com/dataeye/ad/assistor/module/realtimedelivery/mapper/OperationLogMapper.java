package com.dataeye.ad.assistor.module.realtimedelivery.mapper;

import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLog;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLogQuery;

import java.util.List;

/**
 * Created by huangzehai on 2017/6/28.
 */
public interface OperationLogMapper {
    /**
     * insert an operation log.
     *
     * @param log Operation Log
     * @return
     */
    int addOperationLog(OperationLog log);

    /**
     * List operation logs by account id.
     *
     * @param operationLogQuery
     * @return
     */
    List<OperationLog> listOperationLogs(OperationLogQuery operationLogQuery);
}
