package com.dataeye.ad.assistor.module.realtimedelivery.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 操作编码
 * Created by huangzehai on 2017/6/29.
 */
public final class OpCode {
    private OpCode() {

    }

    public static final int BID = 1;
    public static final int PLAN_SWITCH = 2;
    private static final Map<Integer, String> OP_NAMES = new HashMap<>();

    static {
        OP_NAMES.put(BID, "修改出价");
        OP_NAMES.put(PLAN_SWITCH, "切换开关");
    }

    public static String getOpName(int opCode) {
        return OP_NAMES.get(opCode);
    }
}
