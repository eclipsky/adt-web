package com.dataeye.ad.assistor.module.realtimedelivery.controller;


import java.util.List;

import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceAlertConf;
import com.dataeye.ad.assistor.module.realtimedelivery.service.BalanceAlertService;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.AccountRole;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by huangzehai on 2017/4/28.
 */
@Controller
public class BalanceAlertController {
    /**
     * 不发送邮件通知.
     */
    private static final int NOT_SENT_EMAIL_WARNING = 0;
    @Autowired
    private BalanceAlertService balanceAlertService;

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/balance/alert/get.do")
    public Object getBalanceAlertConf(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession == null) {
            ExceptionHandler.throwParameterException(StatusCode.LOGI_RE_LOGIN);
        }
        BalanceAlertConf balanceAlertConf = balanceAlertService.getBalanceAlertConfByAccountId(userInSession.getAccountId());
        if (balanceAlertConf == null) {
            balanceAlertConf = new BalanceAlertConf();
            balanceAlertConf.setEmailAlert(NOT_SENT_EMAIL_WARNING);
            //查找出该公司的配置
            List<BalanceAlertConf> alertConfList = balanceAlertService.getBalanceAlertConfByCompanyId(userInSession.getCompanyId());
            if(alertConfList.size()>0 ){
            	balanceAlertConf.setBalanceRedNum(alertConfList.get(0).getBalanceRedNum());
            }
        }
        return balanceAlertConf;
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/balance/alert/save.do")
    public Object saveBalanceAlertConf(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession == null) {
            ExceptionHandler.throwParameterException(StatusCode.LOGI_RE_LOGIN);
        }
        DEParameter parameter = context.getDeParameter();
        String emailAlert = parameter.getParameter(DEParameter.Keys.EMAIL_ALERT);
        if (StringUtils.isBlank(emailAlert) && !StringUtils.isNumeric(emailAlert)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_BALANCE_EMAIL_ALERT_IS_MISSING);
        }
        String balanceRedNum = parameter.getParameter("balanceRedNum");
        if (StringUtils.isBlank(balanceRedNum) && !StringUtils.isNumeric(balanceRedNum) && Integer.parseInt(balanceRedNum) > 100 && Integer.parseInt(balanceRedNum) < 1) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_BALANCE_RED_NUM_IS_ERROR);
        }
        int companyId = userInSession.getCompanyId();
        int accountId = userInSession.getAccountId();
        BalanceAlertConf balanceAlertConf = new BalanceAlertConf();
        balanceAlertConf.setCompanyId(companyId);
        balanceAlertConf.setAccountId(accountId);
        balanceAlertConf.setEmailAlert(Integer.valueOf(emailAlert));
        balanceAlertConf.setBalanceRedNum(Integer.parseInt(balanceRedNum));
        int result = balanceAlertService.saveBalanceAlertConf(balanceAlertConf);
        //主账号可修改媒体账号余额标红逻辑默认值，默认为5
        if(userInSession.getAccountRole() == AccountRole.COMPANY){
        	List<BalanceAlertConf> alertConfList = balanceAlertService.getBalanceAlertConfByCompanyId(companyId);
        	for (BalanceAlertConf b : alertConfList) {
        		balanceAlertService.updateBalanceAlertConf(b.getAccountId(), b.getCompanyId(), Integer.parseInt(balanceRedNum));
			}
        	
        }
        return result;
    }
}
