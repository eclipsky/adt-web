package com.dataeye.ad.assistor.module.mediumaccount.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.mediumaccount.mapper.CrawlerPageMapper;
import com.dataeye.ad.assistor.module.mediumaccount.model.CrawlerPage;

/**
 * 爬虫页面Service
 */
@Service("crawlerPageService")
public class CrawlerPageService {
	
    @Autowired
    private CrawlerPageMapper crawlerPageMapper;

    /**
     * 根据媒体id查询爬虫页面列表
     * @return
     */
	public List<CrawlerPage> query(Integer mediumId) {
		return crawlerPageMapper.query(mediumId);
	}

}
