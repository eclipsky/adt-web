package com.dataeye.ad.assistor.module.report.model;

import com.dataeye.ad.assistor.module.report.constant.Indicators;
import org.apache.commons.lang.StringUtils;

/**
 * Created by huangzehai on 2017/4/27.
 */
public enum DeliveryContrastiveAnalysisIndicator {
    Cost(Indicators.COST, "消耗"), CTR(Indicators.CTR, "CTR"), DownloadRate(Indicators.DOWNLOAD_RATE, "下载率"), REGISTRATION(Indicators.REGISTER_NUM, "注册数"), CPA(Indicators.REGISTER_CPA, "注册CPA"), Recharge(Indicators.TOTAL_PAY_AMOUNT, "总充值");

    private String indicatorId;

    private String label;

    public String getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(String indicatorId) {
        this.indicatorId = indicatorId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    DeliveryContrastiveAnalysisIndicator(String indicatorId, String label) {
        this.indicatorId = indicatorId;
        this.label = label;
    }

    public static DeliveryContrastiveAnalysisIndicator parse(String text) {
        for (DeliveryContrastiveAnalysisIndicator indicator : DeliveryContrastiveAnalysisIndicator.values()) {
            if (StringUtils.equalsIgnoreCase(indicator.name(), text)) {
                return indicator;
            }
        }
        return null;
    }
}
