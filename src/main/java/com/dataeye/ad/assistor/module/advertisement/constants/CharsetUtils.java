package com.dataeye.ad.assistor.module.advertisement.constants;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 识别文本文件编码
 *
 * @author lingliqi
 * @date 2017-12-27 10:00
 * @see http://www.java2s.com/Code/Java/I18N/Howtoautodetectafilesencoding.htm
 */
public class CharsetUtils {

    public static final String[] CHARSETS = {"UTF-8", "GBK", "ISO-8859-7", "US-ASCII", "windows-1253"};

    public static Charset detectCharset(byte[] buff, String[] charsets) {
        Charset charset = null;

        for (String charsetName : charsets) {
            ByteArrayInputStream bais = new ByteArrayInputStream(buff);
            charset = detectCharset(bais, Charset.forName(charsetName));
            if (charset != null) {
                break;
            }
        }

        return charset;
    }

    private static Charset detectCharset(InputStream inputStream, Charset charset) {
        try {
            BufferedInputStream input = new BufferedInputStream(inputStream);
            CharsetDecoder decoder = charset.newDecoder();
            decoder.reset();
            byte[] buffer = new byte[512];
            boolean identified = false;
            while ((input.read(buffer) != -1) && (!identified)) {
                identified = identify(buffer, decoder);
            }

            input.close();

            if (identified) {
                return charset;
            } else {
                return null;
            }

        } catch (Exception e) {
            return null;
        }
    }

    private static boolean identify(byte[] bytes, CharsetDecoder decoder) {
        try {
            decoder.decode(ByteBuffer.wrap(bytes));
        } catch (CharacterCodingException e) {
            return false;
        }
        return true;
    }


}
