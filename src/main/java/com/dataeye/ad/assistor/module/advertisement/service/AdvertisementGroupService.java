package com.dataeye.ad.assistor.module.advertisement.service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ClientException;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.BillingEvent;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ConfiguredStatus;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ProductType;
import com.dataeye.ad.assistor.module.advertisement.mapper.ApiTxAppProductMapper;
import com.dataeye.ad.assistor.module.advertisement.model.AdCreativeRelationVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdGroupQueryVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdProductVO;
import com.dataeye.ad.assistor.module.advertisement.model.ApiTxAdGroupVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdParamsVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdcreativeVO;
import com.dataeye.ad.assistor.module.advertisement.model.AdvertisementGroup;
import com.dataeye.ad.assistor.module.advertisement.model.AdvertisementGroupSelectVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdvertisementGroupVO;
import com.dataeye.ad.assistor.module.advertisement.model.MarketingApiAuth;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupSelectVO;
import com.dataeye.ad.assistor.module.advertisement.model.TargetingVO;
import com.dataeye.ad.assistor.module.advertisement.service.tencent.AdgroupService;
import com.dataeye.ad.assistor.module.advertisement.service.tencent.AdsService;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.product.model.ProductVo;
import com.dataeye.ad.assistor.module.product.service.ProductService;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.OpCode;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Bid;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLog;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationStatus;
import com.dataeye.ad.assistor.module.realtimedelivery.service.OperationLogService;
import com.dataeye.ad.assistor.util.CurrencyUtils;

/***
 * 广告组Service
 * */
@Service("advertisementGroupService")
public class AdvertisementGroupService {

    @Autowired
    private ApiTxAdGroupService apiTxAdGroupService;
    @Autowired
    private ApiTxAdCreativeService apiTxAdCreativeService;
    @Autowired
    private AdgroupService adgroupService;
    @Autowired
    private ApiTxCampaignService apiTxCampaignService;
    @Autowired
    private CreativeService creativeService;
    @Autowired
    private AdsService adsService;
    @Autowired
    private TargetingTagsService targetingTagsService;

    @Autowired
    private ApiTxAppProductMapper apiTxAppProductMapper;
    @Autowired
    private ProductService productService;
    
    @Autowired
    private OperationLogService operationLogService;
    /**
     * 查询广告列表
     *
     * @param planGroupId
     * @param adGoupId
     * @param mediumId
     * @return
     */
    public List<AdvertisementGroupVO> query(AdGroupQueryVo vo) {
        List<AdvertisementGroupVO> result = new ArrayList<>();
        Map<Integer, PlanGroupSelectVO> planMap = new HashMap<>();
        //1.根据查询参数找到入库的广告关联关系列表
        List<ApiTxAdGroupVo> adGroupRelationList = apiTxAdGroupService.query(vo);
        for (ApiTxAdGroupVo adRelationVo : adGroupRelationList) {
            AdvertisementGroupVO adVo = new AdvertisementGroupVO();
            adVo.setMediumId(adRelationVo.getMediumId());
            adVo.setMediumName(adRelationVo.getMediumName());
            adVo.setMediumAccountId(adRelationVo.getMediumAccountId());
            adVo.setMediumAccountName(adRelationVo.getMediumAccountName());
            adVo.setProductName(adRelationVo.getProductName());
            adVo.setProductId(adRelationVo.getProductId());
            //2.根据计划id，查找推广计划
            PlanGroupSelectVO pVo = new PlanGroupSelectVO();
            Integer planGroupId = adRelationVo.getPlanGroupId();
            if (planMap.containsKey(planGroupId)) {
                pVo = planMap.get(planGroupId);
            } else {
                pVo = apiTxCampaignService.getPlanGroup(vo.getCompanyId(), planGroupId, adRelationVo.getMediumAccountId());
                planMap.put(planGroupId, pVo);
            }

            adVo.setPlanGroup(pVo);
            //3.根据广告组id，查询广告组
            Integer adgroup_id = adRelationVo.getAdGroupId();
            AdvertisementGroup adGroup = new AdvertisementGroup();
            adGroup.setAdGroupId(adgroup_id);
            adGroup.setAdcreativeTemplateId(adRelationVo.getAdcreativeTemplateId());
            adVo.setAdGroup(adGroup);
            result.add(adVo);
        }
        return result;
    }


    /**
     * 查询广告列表
     *
     * @param planGroupId
     * @param adGoupId
     * @param mediumId
     * @return
     */
    public List<AdvertisementGroupSelectVo> queryadGroupForSelect(AdGroupQueryVo vo) {
        return apiTxAdGroupService.queryForSelect(vo);
    }

    /**
     * 创建广告
     *
     * @param AdvertisementGroup 广告组
     * @param PlanGroupVO        推广计划
     * @param adcreativeVoList   创意
     *                           PlanGroupVO planVo,
     *                           AdGroupRelationVo relationVo
     * @return
     */
    public int addAdGroup(AdvertisementGroup adVo, ApiTxAdGroupVo relationVo, List<AdcreativeVO> adcreativeVoList, TargetingVO targetingVo) {
        int statusCode = StatusCode.UNKNOWN;
    	Integer mediumAccountId = 0;
        if (relationVo != null) {
            mediumAccountId = relationVo.getMediumAccountId();
        }

        AdParamsVo adParamsVo = new AdParamsVo();
        adParamsVo.setCompanyId(adVo.getCompanyId());
        adParamsVo.setMediumAccountId(mediumAccountId);
        adParamsVo.setMediumId(relationVo.getMediumId());

        Integer mAccountId = null;
        String accessToken = null;
        MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
        if (tAccountVo != null) {
            mAccountId = tAccountVo.getAccountId();
            accessToken = tAccountVo.getAccessToken();
        }
        //检验广告组名称是否重复
      //  checkAdGroupName(null, mAccountId, accessToken, adVo.getAdGroupName(), null);
        Integer productType = 0;
        //获取推广计划id
        Integer planGroupId = 0;
        if (relationVo != null) {
            productType = relationVo.getProductType();
            planGroupId = relationVo.getPlanGroupId();
        }
        try {
            //1.创建定向
            if (adVo.getTargetingId() == null && targetingVo != null) {
                Integer targetingId = targetingTagsService.addTargeting(mediumAccountId, targetingVo, targetingVo.getIsPublic());
                adVo.setTargetingId(targetingId);
                adParamsVo.setTargetingId(targetingId);
            }
            String siteSet = "";
            if (adcreativeVoList != null && adcreativeVoList.size() > 0) {
            	siteSet = adcreativeVoList.get(0).getSiteSet();
            }
            //2.创建广告组，得到广告组id
            Integer adGroupId = 0;
            if (adVo != null) {
                adGroupId = adgroupService.add(mAccountId, planGroupId, adVo.getAdGroupName(), siteSet, ProductType.parse(productType).name(), adVo.getBeginDate(), adVo.getEndDate(), BillingEvent.parse(adVo.getCostType()).name(), CurrencyUtils.yuanToFen(adVo.getBid()).intValue(), adVo.getProductRefsId(), adVo.getTargetingId() + "", adVo.getTimeSeries(), accessToken);
                adParamsVo.setAdGroupId(adGroupId);
            }
            //3.获取创意id，如果创意id为空，则创建广告创意
            
            Integer adcreativeTemplateId = 0;
            List<Integer> adcreativeIdList = new ArrayList<>();
            if (adcreativeVoList != null && adcreativeVoList.size() > 0) {
                for (AdcreativeVO adcreativeVo : adcreativeVoList) {
                    siteSet = adcreativeVo.getSiteSet();
                    adcreativeTemplateId = adcreativeVo.getAdcreativeTemplateId();
                    String adcreativeName = adGroupId+"创意" + adcreativeVo.getAdcreativeName();
                    String adcreativeElements = JSONObject.fromObject(adcreativeVo.getAdcreativeElements()).toString();
                    Integer adcreativeId = creativeService.add(planGroupId, adcreativeName, adcreativeVo.getAdcreativeTemplateId(), adcreativeElements, adcreativeVo.getDestination_url(), adcreativeVo.getSiteSet(), productType, adcreativeVo.getDeepLink(), adVo.getProductRefsId(), mediumAccountId, mAccountId, accessToken);

                    adcreativeIdList.add(adcreativeId);
                }
            }
            adParamsVo.setAdcreativeId(adcreativeIdList);
            //4.保存广告关联关系，
            apiTxAdGroupService.add(adGroupId, adVo.getCompanyId(), adVo.getSystemAccountId(), adVo.getAdGroupName(), planGroupId, relationVo.getMediumId(), relationVo.getProductId(), mediumAccountId, adVo.getTargetingId(), adcreativeTemplateId, siteSet,CurrencyUtils.yuanToFen(adVo.getBid()).intValue());
            //5.创建广告，得到广告id
            String impression_tracking_url = null;
            String click_tracking_url = null;
            String feeds_interaction_enabled = null;
            List<Integer> adsIdList = new ArrayList<>();
            for (Integer adcreativeId : adcreativeIdList) {
                String ad_name = adGroupId+"广告" + adcreativeId;
                Integer adsId = adsService.add(mAccountId, adGroupId, adcreativeId, ad_name, impression_tracking_url, click_tracking_url, feeds_interaction_enabled, accessToken);
                //6.保存广告与创意之间的关联关系
                apiTxAdCreativeService.add(adGroupId, adsId, adcreativeId, relationVo.getMediumId(), mediumAccountId);
                adsIdList.add(adsId);
            }
            adParamsVo.setAdsId(adsIdList);
            //添加产品和标的物应用id的关联
            ProductVo product = productService.get(null, adVo.getCompanyId(), null, relationVo.getProductId());
            if(product != null && product.getAppId()!=null){
            	AdProductVO adProductVO = new AdProductVO();
            	adProductVO.setProductRefsId(adVo.getProductRefsId());
            	adProductVO.setAppId(product.getAppId());
            	adProductVO.setUpdateTime(new Date());
            	adProductVO.setMediumAccountId(mediumAccountId);
            	apiTxAppProductMapper.update(adProductVO);
            }
        } catch (Exception e) {
        	ClientException clientEx = (ClientException)e;
            adGroupRollback(adParamsVo, clientEx.getStatusCode());
        }
         return 1;
    }

    /**
     * 编辑广告
     *
     * @param AdvertisementGroup 广告组
     * @param PlanGroupVO        推广计划
     * @param adcreativeVoList   创意
     * @return
     */
    public int modify(AdvertisementGroup adVo, ApiTxAdGroupVo relationVo, List<AdcreativeVO> adcreativeVoList, TargetingVO targetingVo) {
        Integer mediumAccountId = 0;
        Integer planGroupId = 0;
        if (relationVo != null) {
            mediumAccountId = relationVo.getMediumAccountId();
            planGroupId = relationVo.getPlanGroupId();
        }
        Integer mAccountId = null;
        String accessToken = null;
        MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
        if (tAccountVo != null) {
            mAccountId = tAccountVo.getAccountId();
            accessToken = tAccountVo.getAccessToken();
        }
        //更新定向,如果定向id不为空，则更新，如果为空，则创建
        if (targetingVo != null && targetingVo.getTargetingId() == null) {
            Integer targetingId = targetingTagsService.addTargeting(mediumAccountId, targetingVo, targetingVo.getIsPublic());
            adVo.setTargetingId(targetingId);
        } else if (targetingVo != null && targetingVo.getTargetingId() != null) {
            targetingTagsService.modifyTargeting(mediumAccountId, targetingVo);
            adVo.setTargetingId(targetingVo.getTargetingId());
        }

        if (adVo.getTargetingId() == null) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_ID_IS_NULL);
        }
        //1.更新广告组信息
        Integer adGroupId = 0;
        Integer bid = null;
        if (adVo != null) {
            adGroupId = adVo.getAdGroupId();
            // 更新时，广告组名称非必填，通过原广告组名称校验重名，必定出错
            // checkAdGroupName(null, mAccountId, accessToken, adVo.getAdGroupName(), adGroupId);
            bid = CurrencyUtils.yuanToFen(adVo.getBid()).intValue();
            boolean resultFlag = true;
            try {
            	adgroupService.update(mAccountId, adGroupId, adVo.getAdGroupName(), null,bid, null, adVo.getTargetingId() + "", adVo.getBeginDate(), adVo.getEndDate(), adVo.getTimeSeries(), null, null, accessToken);
			} catch (Exception e) {
				ClientException clientEx = (ClientException)e;
				resultFlag = false;
				throw new ClientException(clientEx.getStatusCode());
			}finally{
	            if(bid != null && bid > 0){
	            	int opCode = OpCode.BID;
	            	Bid bidObject = new Bid();
	            	int oldBid = 0;
	            	bidObject.setAccountId(adVo.getSystemAccountId());
	            	bidObject.setBid(adVo.getBid());
	            	bidObject.setCompanyId(adVo.getCompanyId());
	            	bidObject.setPlanId(adGroupId);
	            	Result result = new Result();
	                result.setSuccess(resultFlag);
	            	ApiTxAdGroupVo oldAdGroup = apiTxAdGroupService.getAdGroup(adGroupId);
	            	if(oldAdGroup != null && oldAdGroup.getDailyBudget() != null){
	            		oldBid = oldAdGroup.getDailyBudget();
	            	}
	            	logging(bidObject, result, opCode,  CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(new BigDecimal(oldBid))),  CurrencyUtils.yuanToText(bidObject.getBid()));
	            }
			}
           
        }
        AdvertisementGroup newAdGroup = adgroupService.query(mAccountId, adGroupId, null, accessToken, null).get(0);

        //2.更新创意，如果创意id为0或者空，则需要新建创意；如果创意id大于0，则更新创意内容
        List<Integer> addadcreativeIdList = new ArrayList<>();
        List<Integer> updateadcreativeIdList = new ArrayList<>();
        if (adcreativeVoList != null && adcreativeVoList.size() > 0) {
            for (AdcreativeVO adcreativeVo : adcreativeVoList) {
                if (adcreativeVo.getAdcreativeId() != null && adcreativeVo.getAdcreativeId().intValue() > 0) {
                    //更新创意
                    Integer adcreativeId = adcreativeVo.getAdcreativeId();
                    String adcreativeElements = JSONObject.fromObject(adcreativeVo.getAdcreativeElements()).toString();
                    creativeService.update(adcreativeId, adcreativeVo.getAdcreativeName(), adcreativeElements, adcreativeVo.getDestination_url(), adcreativeVo.getDeepLink(), null, mAccountId, accessToken);
                    updateadcreativeIdList.add(adcreativeId);
                } else {
                    //创建创意
                    String adcreativeName = adGroupId+"创意" + adcreativeVo.getAdcreativeName();
                    String adcreativeElements = JSONObject.fromObject(adcreativeVo.getAdcreativeElements()).toString();
                    Integer adcreativeId = creativeService.add(planGroupId, adcreativeName, adcreativeVo.getAdcreativeTemplateId(), adcreativeElements, adcreativeVo.getDestination_url(), newAdGroup.getSiteSet(), relationVo.getProductType(), adcreativeVo.getDeepLink(), newAdGroup.getProductRefsId(), null, mAccountId, accessToken);

                    addadcreativeIdList.add(adcreativeId);
                }
            }
        }

        //3.更新推广广告组关系表
        apiTxAdGroupService.update(adGroupId, planGroupId, relationVo.getMediumId(), mediumAccountId, adVo.getAdGroupName(), adVo.getTargetingId(),bid,adVo.getStatus());

        //4.更新广告和创意的关联关系
        List<AdCreativeRelationVo> adsRelations = apiTxAdCreativeService.query(adGroupId, mediumAccountId);
        for (AdCreativeRelationVo adCreativeRelationVo : adsRelations) {
            if (!updateadcreativeIdList.contains(adCreativeRelationVo.getAdcreativeId())) {  //不包含，则删除该关联关系
                apiTxAdCreativeService.delete(adGroupId, adCreativeRelationVo.getAdsId(), adCreativeRelationVo.getAdcreativeId(), mediumAccountId);
                //删除API上的东西
                adsService.delete(mAccountId, adCreativeRelationVo.getAdsId(), accessToken);
                creativeService.delete(adCreativeRelationVo.getAdcreativeId(), mediumAccountId, mAccountId, accessToken);
            }
        }
        String impression_tracking_url = null;
        String click_tracking_url = null;
        String feeds_interaction_enabled = null;

        for (Integer adcreativeId : addadcreativeIdList) {
            String ad_name = adGroupId+"广告" + adcreativeId;
            Integer adsId = adsService.add(mAccountId, adGroupId, adcreativeId, ad_name, impression_tracking_url, click_tracking_url, feeds_interaction_enabled, accessToken);
            //6.保存广告与创意之间的关联关系
            apiTxAdCreativeService.add(adGroupId, adsId, adcreativeId, relationVo.getMediumId(), mediumAccountId);
        }
        return 1;
    }


    /**
     * 删除广告组
     *
     * @param adGroupId
     * @param mediumAccountId
     * @return
     */
    public int delete(Integer adGroupId, Integer mediumAccountId) {
        //1.删除关联关系
        apiTxAdGroupService.deleteByAdGroupId(adGroupId, mediumAccountId);
        Integer mAccountId = null;
        String accessToken = null;
        MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
        if (tAccountVo != null) {
            mAccountId = tAccountVo.getAccountId();
            accessToken = tAccountVo.getAccessToken();
        }
        //2.删除媒体平台上的广告
        adgroupService.delete(tAccountVo.getAccountId(), adGroupId, tAccountVo.getAccessToken());
        //3.删除广告下的创意
        apiTxAdCreativeService.delete(adGroupId, null, null, mediumAccountId);
        List<AdCreativeRelationVo> deleteCreativeList = apiTxAdCreativeService.query(adGroupId, mediumAccountId);
        //删除API上的东西
        for (AdCreativeRelationVo creativeVo : deleteCreativeList) {
        	adsService.delete(mAccountId, creativeVo.getAdsId(), accessToken);
        	creativeService.delete(creativeVo.getAdcreativeId(), mediumAccountId, mAccountId, accessToken);
		}
        return 1;
    }

    /**
     * 编辑广告组开关和出价
     *
     * @param AdvertisementGroup 广告组
     * @param mediumAccountId
     * @return
     */
    public int modifyAdGroupsInfo(AdvertisementGroup adVo, Integer mediumAccountId) {
        Integer mAccountId = null;
        String accessToken = null;
        MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
        if (tAccountVo != null) {
            mAccountId = tAccountVo.getAccountId();
            accessToken = tAccountVo.getAccessToken();
        }

        Integer opCode = null;
        String status = null;
        boolean logFlag = false;
        Integer bid = null;
        if (adVo.getStatus() != null && ConfiguredStatus.parse(adVo.getStatus()) != null) {
            status = ConfiguredStatus.parse(adVo.getStatus()).name();           
        }
        if (adVo.getBid() != null) {
        	bid=CurrencyUtils.yuanToFen(adVo.getBid()).intValue();
        }
        //1.更新广告组信息
        //return adgroupService.update(mAccountId, adVo.getAdGroupId(), adVo.getAdGroupName(), null, CurrencyUtils.yuanToFen(adVo.getBid()).intValue(), null, null, null, null, null, status, null, accessToken);
             
        boolean resultFlag = true;
        ApiTxAdGroupVo oldAdGroup = apiTxAdGroupService.getAdGroup(adVo.getAdGroupId());
        try {
        	adgroupService.update(mAccountId, adVo.getAdGroupId(), adVo.getAdGroupName(), null, CurrencyUtils.yuanToFen(adVo.getBid()).intValue(), null, null, null, null, null, status, null, accessToken);
        	if(oldAdGroup != null){
        		if(oldAdGroup.getDailyBudget() != null && bid != null && oldAdGroup.getDailyBudget().intValue() != bid.intValue()){
        			opCode = OpCode.BID;
        			logFlag = true;
        		}else if (oldAdGroup.getStatus() != null && adVo.getStatus() != null && oldAdGroup.getStatus().intValue() != adVo.getStatus().intValue()) {
        			opCode = OpCode.PLAN_SWITCH;
        			 logFlag = true;
				}
        		apiTxAdGroupService.update(adVo.getAdGroupId(), oldAdGroup.getPlanGroupId(), oldAdGroup.getMediumId(), mediumAccountId, adVo.getAdGroupName(), adVo.getTargetingId(),bid,adVo.getStatus());
        	}
        } catch (Exception e) {
			ClientException clientEx = (ClientException)e;
			resultFlag = false;
			throw new ClientException(clientEx.getStatusCode());
		}finally{
            if(logFlag){
            	
            	Bid bidObject = new Bid();
            	
            	bidObject.setAccountId(oldAdGroup.getSystemAccountId());
            	bidObject.setBid(adVo.getBid());
            	bidObject.setCompanyId(oldAdGroup.getCompanyId());
            	bidObject.setPlanId(oldAdGroup.getAdGroupId());
            	Result result = new Result();
                result.setSuccess(resultFlag);
                String previous_value = "-";
                String current_value = "-";
            	if(opCode == OpCode.BID){
            		if(oldAdGroup != null && oldAdGroup.getDailyBudget() != null){
            			previous_value = CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(new BigDecimal(oldAdGroup.getDailyBudget())));
            		}
            		current_value = CurrencyUtils.yuanToText(bidObject.getBid());
            	}else if (opCode == OpCode.PLAN_SWITCH) {
            		if(oldAdGroup != null && oldAdGroup.getStatus() != null){
            			previous_value = getStatusLabel(oldAdGroup.getStatus());
            		}
            		if(adVo != null && adVo.getStatus() != null){
            			current_value = getStatusLabel(adVo.getStatus());
            		}
				}
                
            	logging(bidObject, result, opCode, previous_value, current_value);
            }
		}
        return 1;
    }

    /**
     * 检查广告组名称是否存在，同一账户下不允许重名
     * adgroup_name 广告组名称
     */
    public void checkAdGroupName(Integer mediumAccountId, Integer account_id, String accessToken, String adgroup_name, Integer adGroupId) {
        if (mediumAccountId != null) {
            MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
            account_id = tAccountVo.getAccountId();
            accessToken = tAccountVo.getAccessToken();
        }
        List<AdvertisementGroup> result = adgroupService.query(account_id, null, null, accessToken, adgroup_name);
        if (!result.isEmpty()) {
            if (adGroupId == null || (adGroupId != null && result.get(0).getAdGroupId().intValue() == adGroupId)) {
                ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_NAME_EXISTS);
            }
        }
    }


    /**
     * 回滚操作
     * 删除广告组等相关操作
     **/
    public void adGroupRollback(AdParamsVo adParamsVo, int statusCode) {
        Integer mediumAccountId = adParamsVo.getMediumAccountId();
        //1.删除定向
        if (adParamsVo != null && adParamsVo.getTargetingId() != null && adParamsVo.getTargetingId().intValue() > 0) {
            targetingTagsService.deleteTargeting(mediumAccountId, adParamsVo.getTargetingId());
        }
        //2.删除广告组包括广告的关联关系
        if (adParamsVo != null && adParamsVo.getAdGroupId() != null && adParamsVo.getAdGroupId().intValue() > 0) {
            delete(adParamsVo.getAdGroupId(), mediumAccountId);
        }
        //3.删除广点通上的广告创意
        if (adParamsVo != null && adParamsVo.getAdcreativeId() != null && adParamsVo.getAdcreativeId().size() > 0) {
            for (Integer adcreativeId : adParamsVo.getAdcreativeId()) {
                creativeService.delete(adcreativeId, mediumAccountId, null, null);
            }
        }
        //4.删除广点通上的广告
        if (adParamsVo != null && adParamsVo.getAdsId() != null && adParamsVo.getAdsId().size() > 0) {
            Integer account_id = 0;
            String accessToken = null;
            if (mediumAccountId != null) {
                MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
                account_id = tAccountVo.getAccountId();
                accessToken = tAccountVo.getAccessToken();
            }
            for (Integer ad_id : adParamsVo.getAdsId()) {
                adsService.delete(account_id, ad_id, accessToken);
            }
        }
        throw new ClientException(statusCode);

    }


    
    /**
     *  添加操作日志
     * @param bid
     * @param oldPlan
     * @param result
     * @param previous_value  改动前的值   CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(new BigDecimal(oldBid)))
     * @param current_value 改动后的值  CurrencyUtils.yuanToText(bid.getBid())
     */
    private void logging(Bid bid, Result result,int opCode,String previous_value,String current_value) {
        OperationLog log = new OperationLog();
        log.setCompanyId(bid.getCompanyId());
        log.setAccountId(bid.getAccountId());
        log.setPlanId(bid.getPlanId());
        log.setOpCode(opCode);
        log.setPreviousValue(previous_value);
        log.setCurrentValue(current_value);
        log.setOpTime(new Date());
        log.setResult(result.isSuccess() ? 1 : 0);
        log.setMessage("广告投放菜单操作:plan_id对应表api_tx_ad_group");
        operationLogService.addOperationLog(log);
    }
    
    /**
     * @param status
     * @return
     */
    private String getStatusLabel(int status) {
        if (ConfiguredStatus.AD_STATUS_NORMAL.getValue() == status) {
            return "开";
        } else if (ConfiguredStatus.AD_STATUS_SUSPEND.getValue() == status) {
			
        	return "关";
		}else {
			return "-";
        }
    }
}
