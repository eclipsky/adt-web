package com.dataeye.ad.assistor.module.systemaccount.model;

import java.util.Date;

/**
 * 系统账户分组
 * @author luzhuyou 2017/02/17
 */
public class SystemAccountGroup {
	/** 分组ID */
	private Integer groupId;
	/** 公司ID */
	private Integer companyId;
	/** 分组名称 */
	private String groupName;
	/** 组长账号 */
	private String leaderAccount;
	/** 组员账号DM5校验码，用于判断组员是否有变更，如有变更，更新具体组员账号 */
	private String memberAccountsMd5;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getLeaderAccount() {
		return leaderAccount;
	}
	public void setLeaderAccount(String leaderAccount) {
		this.leaderAccount = leaderAccount;
	}
	public String getMemberAccountsMd5() {
		return memberAccountsMd5;
	}
	public void setMemberAccountsMd5(String memberAccountsMd5) {
		this.memberAccountsMd5 = memberAccountsMd5;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "SystemAccountGroup [groupId=" + groupId + ", companyId="
				+ companyId + ", groupName=" + groupName + ", leaderAccount="
				+ leaderAccount + ", memberAccountsMd5=" + memberAccountsMd5
				+ ", remark=" + remark + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + "]";
	}
	
}
