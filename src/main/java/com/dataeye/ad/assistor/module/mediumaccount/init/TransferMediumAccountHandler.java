package com.dataeye.ad.assistor.module.mediumaccount.init;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.exception.ServerException;
import com.dataeye.ad.assistor.module.mediumaccount.constants.Constants;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccount;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.EncodeUtils;
import com.xunlei.jdbc.JdbcTemplate;
import com.xunlei.jdbc.RowCallbackHandler;

/**
 * <pre>
 * 媒体账号旧表数据迁移至新表
 * @author luzhuyou 2017/03/04
 */
@Controller
public class TransferMediumAccountHandler {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(TransferMediumAccountHandler.class);

    @Resource(name = "jdbcTemplateAdt")
    private JdbcTemplate jdbcTemplate;

    /**
     * 媒体账号旧表数据迁移至新表
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/03/04
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.Public, write = true)
    @RequestMapping("/ad/mediumaccount/init.do")
    public Object transfer(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("媒体账号旧表数据迁移至新表");
    	return this.transfer();
    }
    
    /**
     * 媒体账号旧表数据迁移至新表
     */
    public boolean transfer() {
    	List<MediumAccount> mediumAccountList = this.query();
    	if(null != mediumAccountList && !mediumAccountList.isEmpty()) {
    		// 初始化媒体账户表
    		this.batchInsert(mediumAccountList);
    		// 初始化媒体折扣率
    		this.insertDiscountRate();
    		System.out.println("transfer done!");
    		return true;
    	}else{
    		System.out.println("no record!");
    		return false;
    	}
    }
    
    /**
     * 查询媒体账号旧表数据
     * @return
     * @throws ServerException
     */
    public List<MediumAccount> query() throws ServerException {
        final List<MediumAccount> mediumAccountList = new ArrayList<MediumAccount>();

        try {
            StringBuilder sql = new StringBuilder("SELECT id, platform, account, password, IFNULL(alias,'') alias, "
            		+ "loginStatus, 0 as status, skey, extend, now(), now() FROM dc_ad_account where platform != 5 and platform != 6");

            jdbcTemplate.query(sql.toString(), new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException {
                	MediumAccount mediumAccount = new MediumAccount();
                	mediumAccount.setMediumAccountId(rs.getInt("id"));
                    mediumAccount.setCompanyId(2);
                    mediumAccount.setMediumId(rs.getInt("platform"));
                    mediumAccount.setMediumAccount(rs.getString("account"));
                    try {
						mediumAccount.setPassword(EncodeUtils.aesEncrypt(rs.getString("password"), Constants.AES_ENCRYPT_KEY));
					} catch (Exception e) {
						e.printStackTrace();
					}
                    mediumAccount.setMediumAccountAlias(rs.getString("alias"));
                    mediumAccount.setLoginStatus(rs.getInt("loginStatus"));
                    mediumAccount.setStatus(0);
                    mediumAccount.setSkey(rs.getString("skey"));
                    mediumAccount.setRemark(rs.getString("extend"));
                    mediumAccountList.add(mediumAccount);
                }
            });
        } catch (Exception e) {
            ExceptionHandler.throwDatabaseException(StatusCode.COMM_DB_SQL_ERROR, e);
        }

        return mediumAccountList;
    }

    /**
	 * 批量插入数据到媒体账号新表
	 * @param mediumAccountList
	 * @return
	 */
	public int batchInsert(List<MediumAccount> mediumAccountList) {
		// String truncateSql = "TRANCATE TABLE ac_medium_account";
		StringBuilder batchInsertSql = new StringBuilder("INSERT INTO ac_medium_account "
				+ "(medium_account_id, company_id, medium_id, medium_account, password, medium_account_alias, "
				+ "login_status, status, skey, remark, create_time, update_time) values ");
		for(MediumAccount account : mediumAccountList){  
            batchInsertSql.append("(")
            	.append(account.getMediumAccountId()).append(",")
            	.append(account.getCompanyId()).append(",")
            	.append(account.getMediumId()).append(",'")
            	.append(account.getMediumAccount()).append("','")
            	.append(account.getPassword()).append("','")
            	.append(account.getMediumAccountAlias()).append("',")
            	.append(account.getLoginStatus()).append(",")
            	.append(account.getStatus()).append(",'")
            	.append(account.getSkey()).append("','")
            	.append(account.getRemark()).append("','")
            	.append(DateUtils.currentTime()).append("','")
            	.append(DateUtils.currentTime()).append("'")
            	.append("),");
        }  
		if(batchInsertSql.toString().endsWith(",")) {
			batchInsertSql.deleteCharAt(batchInsertSql.length()-1);
		}
		try {
			// jdbcTemplate.execute(truncateSql);
			return jdbcTemplate.insertAndReturnUpdateCount(batchInsertSql.toString());

		} catch (Exception e) {
			ExceptionHandler.throwDatabaseException(StatusCode.COMM_DB_SQL_ERROR, e);
		}
		return 0;
	}
	
	/**
	 * 初始化媒体折扣率表
	 * @return
	 */
	public int insertDiscountRate() {
		// String truncateSql = "TRANCATE TABLE ac_medium_discount_rate";
		String sql = new String("INSERT INTO ac_medium_discount_rate "
				+ "(discount_rate_id, medium_account_id, discount_rate, effect_date, invalid_date, create_time, update_time) "
				+ "SELECT medium_account_id, medium_account_id, 0, update_time, '2099-12-31', create_time, create_time  "
				+ "FROM ac_medium_account ");
		try {
			// jdbcTemplate.execute(truncateSql);
			return jdbcTemplate.insertAndReturnUpdateCount(sql);

		} catch (Exception e) {
			ExceptionHandler.throwDatabaseException(StatusCode.COMM_DB_SQL_ERROR, e);
		}
		return 0;
	}

}
