package com.dataeye.ad.assistor.module.report.constant;

import com.dataeye.ad.assistor.common.ColumnInfo;

import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * Created by huangzehai on 2016/12/28.
 */
public class Columns {
    public static final String DATE = "date";
    public static final String MEDIUM = "medium";
    public static final String MEDIUM_ACCOUNT = "mediumAccount";
    public static final String PLAN = "plan";
    public static final String COST = "cost";
    public static final String EXPOSURES = "exposures";
    public static final String TOTAL_CLICKS = "totalClicks";
    public static final String CLICKS = "clicks";
//    public static final String DAILY_UNIQUE_CLICKS = "dailyUniqueClicks";
    public static final String CTR = "ctr";
    public static final String CPC = "cpc";
    public static final String REACHES = "reaches";
    public static final String REACH_RATE = "reachRate";
    public static final String RETENTION_TIME = "retentionTime";
    public static final String DOWNLOADS = "downloads";
    public static final String DOWNLOAD_RATE = "downloadRate";
    public static final String ACTIVATIONS = "activations";
    public static final String ACTIVATION_RATE = "activationRate";
    public static final String ACTIVATION_CPA = "activationCpa";
    public static final String REGISTRATIONS = "registrations";
    public static final String REGISTER_ACCOUNT_NUM = "registerAccountNum";
    public static final String REGISTRATIONS_FD = "registrationsFd";
    public static final String REGISTER_ACCOUNT_NUM_FD = "registerAccountNumFd";
    public static final String REGISTRATION_RATE = "registrationRate";
    public static final String REGISTRATION_CPA = "registrationCpa";
    public static final String REGISTER_ACCOUNT_NUM_CPA = "registerAccountNumCpa";
    public static final String ACTIVATION_REGISTRATION_RATE = "activationRegistrationRate";
    public static final String DAY2_RETENTION_RATE = "day2RetentionRate";
    public static final String DAY3_RETENTION_RATE = "day3RetentionRate";
    public static final String DAY7_RETENTION_RATE = "day7RetentionRate";
    public static final String DAY15_RETENTION_RATE = "day15RetentionRate";
    public static final String DAY30_RETENTION_RATE = "day30RetentionRate";
    public static final String NEWLY_INCREASED_RECHARGES = "newlyIncreasedRecharges";
    public static final String NEWLY_ACCOUNT_PAY_AMOUNT = "new_account_pay_amount";
    public static final String NEWLY_INCREASED_PAY_RATE = "newlyIncreasedPayRate";
    public static final String PAYBACK_RATE_1DAY = "paybackRate1Day";
    public static final String LTV_1DAY = "ltv1Day";
    public static final String LTV_7DAYS = "ltv7Days";
    public static final String LTV_30DAYS = "ltv30Days";
    public static final String TOTAL_RECHARGE_AMOUNT = "totalRechargeAmount";
    public static final String TOTAL_RECHARGES = "totalRecharges";
    public static final String ACCUMULATED_RECHARGE = "accumulatedRecharge";
    public static final String PAY_RATE = "payRate";
    public static final String DAU = "dau";
    public static final String SYSTEMACCOUNT = "systemAccount";

    /**
     * 投放日报-计划维度-表头
     */
    public static final Map<String, Object> PLAN_REPORT = new LinkedHashMap<>();
    /**
     * 投放日报-媒体维度-表头
     */
    public static final Map<String, Object> MEDIUM_REPORT = new LinkedHashMap<>();

    /**
     * 投放日报-媒体维度-详细-表头
     */
    public static final Map<String, Object> MEDIUM_DETAIL_REPORT = new LinkedHashMap<>();

    /**
     * 投放日报-媒体维度-详细-下载-表头
     */
    public static final Map<String, Object> MEDIUM_DETAIL_REPORT_DOWNLOAD = new LinkedHashMap<>();

    /**
     * 投放日报指标名称和指标ID映射
     */
    private static final Map<String, String> INDICATOR_MAPPING = new HashMap<>();
    
    /**
     * 投放日报-子账户-下载-表头
     */
    public static final Map<String, Object> SYSTEM_ACCOUNT_REPORT_DOWNLOAD = new LinkedHashMap<>();

    static {
        initPlanReportColumns();
        initMediumReportColumns();
        initMediumReportDetailColumns();
        initMediumReportDetailDownloadColumns();
        initSystemAccountReportColumns();
        initIndicatorMapping();
    }

    /**
     * 获取授权列
     *
     * @param columns
     * @param indicatorPermission Authorized indicators.
     * @return
     */
    public static final Map<String, Object> getAuthorizedColumns(Map<String, Object> columns, String indicatorPermission) {
        Map<String, Object> authorizedColumns = new LinkedHashMap<>();
        Set<String> notIndicators = new HashSet<>();
        notIndicators.add(DATE);
        notIndicators.add(MEDIUM);
        notIndicators.add(MEDIUM_ACCOUNT);
        notIndicators.add(PLAN);
        notIndicators.add(SYSTEMACCOUNT);
        for (Map.Entry<String, Object> entry : columns.entrySet()) {
            //非指标或者授权指标
            if (notIndicators.contains(entry.getKey()) || StringUtils.containsIgnoreCase(indicatorPermission, INDICATOR_MAPPING.get(entry.getKey()))) {
                authorizedColumns.put(entry.getKey(), entry.getValue());
            }
        }
        return authorizedColumns;
    }

    /**
     * 初始化指标映射
     */
    private static final void initIndicatorMapping() {
        INDICATOR_MAPPING.put(COST, "1-1");
        INDICATOR_MAPPING.put(EXPOSURES, "1-4");
        INDICATOR_MAPPING.put(TOTAL_CLICKS, "1-6");
        INDICATOR_MAPPING.put(CLICKS, "1-5");
//        INDICATOR_MAPPING.put(DAILY_UNIQUE_CLICKS, "1-7");
        INDICATOR_MAPPING.put(CTR, "1-8");
        INDICATOR_MAPPING.put(CPC, "1-10");
        INDICATOR_MAPPING.put(REACHES, "2-1");
        INDICATOR_MAPPING.put(REACH_RATE, "2-2");
//        INDICATOR_MAPPING.put("y15", "");
        INDICATOR_MAPPING.put(DOWNLOADS, "2-3");
        INDICATOR_MAPPING.put(DOWNLOAD_RATE, "2-4");
        INDICATOR_MAPPING.put(ACTIVATIONS, "3-1");
        INDICATOR_MAPPING.put(ACTIVATION_RATE, "3-2");
        INDICATOR_MAPPING.put(ACTIVATION_CPA, "3-3");
        INDICATOR_MAPPING.put(REGISTRATIONS, "3-4");
        INDICATOR_MAPPING.put(REGISTRATION_RATE, "3-5");
        INDICATOR_MAPPING.put(REGISTRATION_CPA, "3-6");
        INDICATOR_MAPPING.put(REGISTER_ACCOUNT_NUM_CPA, "3-11");
        INDICATOR_MAPPING.put(ACTIVATION_REGISTRATION_RATE, "3-7");
        INDICATOR_MAPPING.put(REGISTER_ACCOUNT_NUM, "3-8");
        INDICATOR_MAPPING.put(REGISTRATIONS_FD, "3-9");
        INDICATOR_MAPPING.put(REGISTER_ACCOUNT_NUM_FD, "3-10");
        INDICATOR_MAPPING.put(DAY2_RETENTION_RATE, "4-1");
        INDICATOR_MAPPING.put(DAY3_RETENTION_RATE, "4-2");
        INDICATOR_MAPPING.put(DAY7_RETENTION_RATE, "4-3");
        INDICATOR_MAPPING.put(DAY15_RETENTION_RATE, "4-5");
        INDICATOR_MAPPING.put(DAY30_RETENTION_RATE, "4-4");
        INDICATOR_MAPPING.put(NEWLY_INCREASED_RECHARGES, "5-1");
        INDICATOR_MAPPING.put(NEWLY_ACCOUNT_PAY_AMOUNT, "5-2");
        INDICATOR_MAPPING.put(PAYBACK_RATE_1DAY, "8-1");
        INDICATOR_MAPPING.put(TOTAL_RECHARGE_AMOUNT, "5-4");
        INDICATOR_MAPPING.put(TOTAL_RECHARGES, "5-5");
        INDICATOR_MAPPING.put(ACCUMULATED_RECHARGE, "5-6");
        INDICATOR_MAPPING.put(PAY_RATE, "5-7");
        INDICATOR_MAPPING.put(NEWLY_INCREASED_PAY_RATE, "5-8");
        INDICATOR_MAPPING.put(LTV_1DAY, "7-2");
        INDICATOR_MAPPING.put(LTV_7DAYS, "7-8");
        INDICATOR_MAPPING.put(LTV_30DAYS, "7-10");
        INDICATOR_MAPPING.put(DAU, "6-1");
    }

    /**
     * 初始化推广计划表头
     */
    private static void initPlanReportColumns() {
        PLAN_REPORT.put(DATE, new ColumnInfo("日期", "p.stat_date", false));
        PLAN_REPORT.put(MEDIUM, new ColumnInfo("媒体", "medium_name", false));
        PLAN_REPORT.put(MEDIUM_ACCOUNT, new ColumnInfo("媒体账号", "account_name", false));
        PLAN_REPORT.put(PLAN, new ColumnInfo("计划", "plan_name", false));
        addIndices(PLAN_REPORT);
    }

    /**
     * 添加指标
     *
     * @param columns
     */
    private static void addIndices(Map<String, Object> columns) {
        columns.put(COST, new ColumnInfo("总消耗", "total_cost", true, "消耗"));
        columns.put(EXPOSURES, new ColumnInfo("曝光数", "exposures", true, "曝光"));
        columns.put(CLICKS, new ColumnInfo("媒体点击数", "clicks", true, "点击"));
        columns.put(TOTAL_CLICKS, new ColumnInfo("ADT点击数", "unique_clicks", true, "点击"));
//        columns.put(DAILY_UNIQUE_CLICKS, new ColumnInfo("按天排重点击数", "daily_unique_clicks", true, "点击"));
        columns.put(CTR, new ColumnInfo("CTR", "ctr", true, "点击"));
        columns.put(CPC, new ColumnInfo("CPC", "cpc", true, "点击"));
        columns.put(REACHES, new ColumnInfo("到达数", "landing_page_uv", true, "到达"));
        columns.put(REACH_RATE, new ColumnInfo("到达率", "reach_rate", true, "到达"));
//        columns.put(format(index++), new ColumnInfo("停留时长", "retention_time", true, "到达"));
        columns.put(DOWNLOADS, new ColumnInfo("下载数", "download_uv", true, "下载激活"));
        columns.put(DOWNLOAD_RATE, new ColumnInfo("下载率", "download_rate", true, "下载激活"));
        columns.put(ACTIVATIONS, new ColumnInfo("激活数", "activations", true, "下载激活"));
        columns.put(ACTIVATION_RATE, new ColumnInfo("点击激活率", "activation_rate", true, "下载激活"));
        columns.put(ACTIVATION_CPA, new ColumnInfo("激活CPA", "activation_cpa", true, "下载激活"));
        columns.put(REGISTRATIONS_FD, new ColumnInfo("首日注册设备数", "registrationsFd", true, "下载激活"));
        columns.put(REGISTER_ACCOUNT_NUM_FD, new ColumnInfo("首日注册账号数", "registerAccountNumFd", true, "下载激活"));
        columns.put(REGISTRATIONS, new ColumnInfo("注册设备数", "registrations", true, "注册"));
        columns.put(REGISTER_ACCOUNT_NUM, new ColumnInfo("注册账号数", "registerAccountNum", true, "注册"));
        columns.put(ACTIVATION_REGISTRATION_RATE, new ColumnInfo("激活注册率", "activation_registration_rate", true, "注册"));
        columns.put(REGISTRATION_RATE, new ColumnInfo("点击注册率", "registration_rate", true, "注册"));
        columns.put(REGISTRATION_CPA, new ColumnInfo("首日注册CPA(设备)", "cost_per_registration", true, "注册"));
        columns.put(REGISTER_ACCOUNT_NUM_CPA, new ColumnInfo("首日注册CPA(帐号)", "registerAccountNumCpa", true, "注册"));
        columns.put(DAU, new ColumnInfo("DAU", "dau", true, "活跃"));
        columns.put(NEWLY_INCREASED_RECHARGES, new ColumnInfo("首日充值账号数", "newly_increased_recharges", true, "充值"));
        columns.put(NEWLY_ACCOUNT_PAY_AMOUNT, new ColumnInfo("首日充值金额", "new_account_pay_amount", true, "充值"));
        columns.put(NEWLY_INCREASED_PAY_RATE, new ColumnInfo("首日付费率", "newly_increased_pay_rate", true, "充值"));
        columns.put(TOTAL_RECHARGES, new ColumnInfo("充值账号数", "totalRecharges", true, "充值"));
        columns.put(TOTAL_RECHARGE_AMOUNT, new ColumnInfo("充值金额", "totalRechargeAmount", true, "充值"));
        columns.put(ACCUMULATED_RECHARGE, new ColumnInfo("累计充值", "accumulated_recharge", true, "充值"));
        columns.put(PAY_RATE, new ColumnInfo("付费率", "pay_rate", true, "充值"));
        columns.put(PAYBACK_RATE_1DAY, new ColumnInfo("首日回本率", "paybackRate1Day", true, "充值"));
        columns.put(LTV_1DAY, new ColumnInfo("首日LTV", "pay_amount_1day", true, "充值"));
        columns.put(LTV_7DAYS, new ColumnInfo("7日LTV", "pay_amount_7days", true, "充值"));
        columns.put(LTV_30DAYS, new ColumnInfo("30日LTV", "pay_amount_30days", true, "充值"));
        columns.put(DAY2_RETENTION_RATE, new ColumnInfo("次日留存率", "next_day_retention_rate", true, "留存"));
        columns.put(DAY3_RETENTION_RATE, new ColumnInfo("3日留存率", "three_day_retention_rate", true, "留存"));
        columns.put(DAY7_RETENTION_RATE, new ColumnInfo("7日留存率", "seven_day_retention_rate", true, "留存"));
        columns.put(DAY15_RETENTION_RATE, new ColumnInfo("15日留存率", "fifteen_day_retention_rate", true, "留存"));
        columns.put(DAY30_RETENTION_RATE, new ColumnInfo("30日留存率", "thirty_day_retention_rate", true, "留存"));
        
        
    }

    /**
     * 初始化媒体表头
     */
    private static void initMediumReportColumns() {
        MEDIUM_REPORT.put(DATE, new ColumnInfo("日期", "p.stat_date", false));
        MEDIUM_REPORT.put(MEDIUM, new ColumnInfo("媒体", "medium_name", false));
        MEDIUM_REPORT.put(MEDIUM_ACCOUNT, new ColumnInfo("媒体账号", "account_name", false));
        addIndices(MEDIUM_REPORT);
    }

    /**
     * 初始化投放日报媒体维度弹出框的表头
     */
    private static void initMediumReportDetailColumns() {
        MEDIUM_DETAIL_REPORT.put(PLAN, new ColumnInfo("计划", "plan_name", false));
        addIndices(MEDIUM_DETAIL_REPORT);
    }

    /**
     * 初始化投放日报媒体维度下载表格表头
     */
    private static void initMediumReportDetailDownloadColumns() {
        MEDIUM_DETAIL_REPORT_DOWNLOAD.put(MEDIUM_ACCOUNT, new ColumnInfo("媒体账号", "account_name", false));
        MEDIUM_DETAIL_REPORT_DOWNLOAD.put(PLAN, new ColumnInfo("计划", "plan_name", false));
        addIndices(MEDIUM_DETAIL_REPORT_DOWNLOAD);
    }

    /**
     * 初始化子账户Tab表头
     */
    private static void initSystemAccountReportColumns() {
    	SYSTEM_ACCOUNT_REPORT_DOWNLOAD.put(SYSTEMACCOUNT, new ColumnInfo("子账户", "systemAccount", false));
        addIndices(SYSTEM_ACCOUNT_REPORT_DOWNLOAD);
    }
}
