package com.dataeye.ad.assistor.module.systemaccount.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.systemaccount.mapper.CrmManagerAccountMapper;
import com.dataeye.ad.assistor.module.systemaccount.model.CrmManagerAccount;

/**
 * CRM管理人员账号Service
 * @author luzhuyou 2017/05/11
 *
 */
@Service
public class CrmManagerAccountService {

	@Autowired
	private CrmManagerAccountMapper crmManagerAccountMapper;
	
	/**
	 *  根据账号（邮箱）账号信息
	 * @param accountName 系统账号
	 * @return
	 */
	public CrmManagerAccount get(String accountName) {
		return crmManagerAccountMapper.getByEmail(accountName);
	}

	/**
	 *  根据账号ID获取账号信息
	 * @param accountId 账号ID
	 * @return
	 */
	public CrmManagerAccount get(int accountId) {
		return crmManagerAccountMapper.getById(accountId);
	}
	
}
