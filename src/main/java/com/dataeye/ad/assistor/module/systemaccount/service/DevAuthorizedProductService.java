package com.dataeye.ad.assistor.module.systemaccount.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.product.model.ProductVo;
import com.dataeye.ad.assistor.module.product.service.ProductService;
import com.dataeye.ad.assistor.module.systemaccount.mapper.DevAuthorizedProductMapper;
import com.dataeye.ad.assistor.module.systemaccount.model.DevAuthorizedProductVo;

/**
 * 开发工程师授权产品信息Service
 *
 * @author luzhuyou 2017/08/29
 */
@Service
public class DevAuthorizedProductService {

    @Autowired
    private DevAuthorizedProductMapper devAuthorizedProductMapper;
    @Autowired
    private ProductService productService;
    
    /**
     * 获取开发工程师授权产品信息
     * @param companyId
     * @return
     */
    public List<DevAuthorizedProductVo> query(Integer companyId) {
    	List<DevAuthorizedProductVo> voList = devAuthorizedProductMapper.query(companyId);
    	if(voList == null) {
    		return null;
    	}
    	for(DevAuthorizedProductVo vo : voList) {
    		String productIds = vo.getProductIds();
    		if(StringUtils.isNotBlank(productIds)) {
    			String[] productIdArr = productIds.split(",");
    			List<ProductVo> productList = productService.queryByProductIds(productIdArr);
    			vo.setProductList(productList);
    		}
    	}
    	return voList;
    }
    
    /**
     * 新增开发工程师授权产品信息
     * @param accountId
     * @param companyId
     * @param productIds
     * @return
     */
    public void add(int accountId,Integer companyId,String productIds) {
    	DevAuthorizedProductVo vo = new DevAuthorizedProductVo();
    	vo.setAccountId(accountId);
    	vo.setCompanyId(companyId);
    	vo.setProductIds(productIds);
        vo.setCreateTime(new Date());
        vo.setUpdateTime(new Date());
    	devAuthorizedProductMapper.add(vo);;
    }
    
    /**
     * 修改开发工程师授权产品信息
     * @param accountId
     * @param productIds
     * @return
     */
    public void update(int accountId,String productIds) {
    	DevAuthorizedProductVo vo = new DevAuthorizedProductVo();
    	vo.setAccountId(accountId);
    	vo.setProductIds(productIds);
        vo.setUpdateTime(new Date());
    	devAuthorizedProductMapper.update(vo);;
    }
    
    /**
     * 修改开发工程师授权产品信息
     * @param accountId
     * @param productIds
     * @return
     */
    public DevAuthorizedProductVo getDevAuthorizedProduct(int accountId) {
    	return devAuthorizedProductMapper.getDevAuthorizedProduct(accountId);
    }
    
    /**
     * 删除开发工程师授权产品信息
     * @param accountId
     * @return
     */
    public void delete(int accountId) {
    	devAuthorizedProductMapper.delete(accountId);
    }
}
