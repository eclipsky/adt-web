package com.dataeye.ad.assistor.module.dictionaries.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.module.dictionaries.mapper.ConfigMapper;
import com.dataeye.ad.assistor.module.dictionaries.model.ConfigVo;

@Service("configService")
public class ConfigService {
	
	@Autowired
	private ConfigMapper configMapper;
	
	/**
	 * 查询配置表列表
	 * @param vkey  可为空，如果不传，则查询所有的配置
	 * @return
	 */
	public List<ConfigVo> queryAll() {
		return configMapper.query(null);
	}
	
	/**
	 * 从ApplicationContext中获取对应的value
	 * @return
	 */
	public String queryFromCache(String vkey) {
        return ApplicationContextContainer.getConfigVo(vkey);
	}
	
	/**
	 * 从ApplicationContext中获取对应的value
	 * @return
	 */
	public List<Integer> queryFromCacheByList(String vkey) {
        return ApplicationContextContainer.getConfigVoByList(vkey);
	}
}
