package com.dataeye.ad.assistor.module.association.model;

import java.util.Date;

/**
 * 包关联关系表
 * @author ldj 2017-07-11
 *
 */
public class PkgRelation {
	/** 公司ID */
	private Integer companyId;
	/** 系统类型：0-Others, 1-iOS, 2-Android */
	private Integer osType;
	/** 产品ID */
	private Integer productId;
	/** 包ID */
	private Integer pkgId;
	/** 计划ID */
	private Integer planId;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public Integer getPkgId() {
		return pkgId;
	}
	public void setPkgId(Integer pkgId) {
		this.pkgId = pkgId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	@Override
	public String toString() {
		return "PkgRelation [companyId=" + companyId + ", osType=" + osType
				+ ", productId=" + productId + ", pkgId=" + pkgId + ", planId="
				+ planId + ", createTime=" + createTime + ", updateTime="
				+ updateTime + "]";
	}
	
	
}
