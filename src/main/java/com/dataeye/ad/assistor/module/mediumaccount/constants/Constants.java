package com.dataeye.ad.assistor.module.mediumaccount.constants;

/**
 * 媒体账号管理模块常量表
 * @author luzhuyou 2017/02/20
 */
public class Constants {

	public static final double DEFALUT_MEDIUM_DISCOUNT_RATE = 0; // 默认媒体折扣率
	
	public static final String AES_ENCRYPT_KEY = "SJ2DFIOWE234JL39"; 
	
	/**
	 * 媒体账号状态
	 */
	public class MediumAccountStatus {
		/** 正常 */
		public static final int NORMAL = 0;
		/** 失效  */
		public static final int INVALID = 1;
	}
	/**
	 * 通过调用web服务进行密码加密
	 * @create 2017-06-19
	 * **/
	public static class MediumAccountInterface{
		
		public static final String GET_ENCRYPT_PASSWORD_KEY = "get_encrypt_password";
		public static final String ENCRYPT_ENCODE = "http://10.1.2.215:9079/security/encrypt?content={0}";
	}
}
