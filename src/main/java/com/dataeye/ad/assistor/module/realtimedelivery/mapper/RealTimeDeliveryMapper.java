package com.dataeye.ad.assistor.module.realtimedelivery.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.realtimedelivery.model.DeliveryAlertInfoVo;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeDeliveryRequestVo;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeDeliveryVo;

/**
 * 实时投放数据查询映射器
 * @author luzhuyou
 *
 */
@MapperScan
public interface RealTimeDeliveryMapper {

	/**
	 * 查询实时分析数据
	 * @param requestVo
	 * @return
	 * @author luzhuyou 2017/03/03
	 */
	public List<RealTimeDeliveryVo> query(RealTimeDeliveryRequestVo requestVo);
	/**
	 * 查询实时告警信息
	 * @param date
	 * @return
	 */
	public List<DeliveryAlertInfoVo> queryMonitorInfo(String date);

}
