package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Bid;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.TuiUtils;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 智汇推出价服务.
 * Created by huangzehai on 2017/5/12.
 */
@Service("tuiBidService")
public class TuiBidService extends AbstractMediumPlanService<Bid> implements BidService {

    private static final String URL = "http://tui.qq.com/misapi/adgroup/updatePrice";

    private static final String REFERER_FORMAT = "http://tui.qq.com/xui/%s/adgroup/list?begin_date=%s&data_begin_date=%s&data_end_date=%s&end_date=9999-12-31&page_num=1&page_size=10&price_mode=_all&promotion=_all&sort_key=create_time&sort_type=ASC&source_link=menu&status=ADGROUP_STATUS_UN_DELETED";

    public static final String BID_SUCCESS = "修改出价成功";

    @Override
    protected String url(String cookie, Long mediumPlanId) {
        return URL;
    }

    @Override
    protected List<NameValuePair> parameters(String cookie, Bid bid) {
        List<NameValuePair> params = new ArrayList<>();
        Gson gson = new Gson();
        List<Long> mediumPlanIds = new ArrayList<>();
        mediumPlanIds.add(bid.getMediumPlanId());
        //计划ID
        params.add(new BasicNameValuePair("adgroup_id", gson.toJson(mediumPlanIds)));
        //出价，已分为单位.
        params.add(new BasicNameValuePair("new_price", CurrencyUtils.yuanToFen(bid.getBid()).toString()));
        //广告主ID
        params.add(new BasicNameValuePair("advertiser_id", TuiUtils.getLoginId(cookie)));
        return params;
    }

    @Override
    protected void requestHeader(HttpRequestBase request, String cookie) {
        request.setHeader(X_CSRF_TOKEN, TuiUtils.getCsrfToken(cookie));
        request.setHeader(HOST, "tui.qq.com");
        request.setHeader(ORIGIN, "http://tui.qq.com");
        String today = DateUtils.format(new Date());
        request.setHeader(REFERER, String.format(REFERER_FORMAT, TuiUtils.getLoginId(cookie), today, today, today));
    }

    /**
     * 解析响应
     *
     * @param content
     * @return
     */
    @Override
    protected Result response(String content) {
        Result bidResult = new Result();
        Gson gson = new Gson();
        TuiResponse tuiResponse = gson.fromJson(content, TuiResponse.class);
        if (tuiResponse.getData().getFailedCount() == 0 && tuiResponse.getData().getSuccessCount() > 0) {
            bidResult.setSuccess(true);
            bidResult.setMessage(BID_SUCCESS);
        } else {
            bidResult.setSuccess(false);
            bidResult.setMessage(tuiResponse.getData().getFailedList().get(0).getReason());
        }

        return bidResult;
    }

    @Override
    public Result bid(Bid bid) {
        return execute(bid);
    }

    private class TuiResponse {
        private int code;
        private String msg;
        private Data data;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return "TuiResponse{" +
                    "code=" + code +
                    ", msg='" + msg + '\'' +
                    ", data=" + data +
                    '}';
        }

        private class Data {
            @SerializedName("success_cnt")
            private int successCount;
            @SerializedName("failed_cnt")
            private int failedCount;
            @SerializedName("failed_id_list")
            private List<Fail> failedList;

            public int getSuccessCount() {
                return successCount;
            }

            public void setSuccessCount(int successCount) {
                this.successCount = successCount;
            }

            public int getFailedCount() {
                return failedCount;
            }

            public void setFailedCount(int failedCount) {
                this.failedCount = failedCount;
            }

            public List<Fail> getFailedList() {
                return failedList;
            }

            public void setFailedList(List<Fail> failedList) {
                this.failedList = failedList;
            }

            @Override
            public String toString() {
                return "Data{" +
                        "successCount=" + successCount +
                        ", failedCount=" + failedCount +
                        ", failedList=" + failedList +
                        '}';
            }

            private class Fail {
                @SerializedName("adgroup_id")
                private int adGroupId;
                private String reason;

                public int getAdGroupId() {
                    return adGroupId;
                }

                public void setAdGroupId(int adGroupId) {
                    this.adGroupId = adGroupId;
                }

                public String getReason() {
                    return reason;
                }

                public void setReason(String reason) {
                    this.reason = reason;
                }

                @Override
                public String toString() {
                    return "Fail{" +
                            "adGroupId=" + adGroupId +
                            ", reason='" + reason + '\'' +
                            '}';
                }
            }
        }
    }


}
