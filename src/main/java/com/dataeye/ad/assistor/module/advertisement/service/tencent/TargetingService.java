package com.dataeye.ad.assistor.module.advertisement.service.tencent;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.InterfaceConstants;
import com.dataeye.ad.assistor.module.advertisement.model.TargetingVO;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.Targeting;
import com.dataeye.ad.assistor.util.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 广点通API
 * 定向targetingService
 *
 * @author lingliqi
 * @date 2017-12-04 11:39
 */
@Service("targetingService")
public class TargetingService {

    private static final Logger logger = LoggerFactory.getLogger(TargetingService.class);

    /**
     * 获取定向列表
     *
     * @param account_id(必填) 广告主id
     * @param targeting_id   定向id
     * @param access_token   授权token
     * @return List<TargetVo>
     */
    public List<TargetingVO> query(Integer account_id, Integer targeting_id, String access_token) {
        validateParamsByAccount(account_id, access_token);
        // 1.构建请求参数
        String Tencent_params = StringUtils.substring(InterfaceConstants.TencentInterface.TENCENT_PARAMETERS, 1);
        String params1 = HttpUtil.urlParamReplace(Tencent_params,
                access_token, DateUtils.getSecondTimestamp(new Date()) + "", RandomUtils.getRandomString(32));

        String params = params1 + "&account_id=" + account_id;
        if (targeting_id != null) {
            params += "&targeting_id=" + targeting_id;
        }
        String getTargetingInterface = ConfigHandler.getProperty(
                InterfaceConstants.TencentInterface.TENCENT_URL_KEY, InterfaceConstants.TencentInterface.TENCENT_URL) +
                InterfaceConstants.TencentInterface.TARGETING_GET;
        // 2.HTTP请求获取数据
        String httpResponse = HttpRequest.get(getTargetingInterface, params);
        if (StringUtils.isBlank(httpResponse)) {
            logger.error("<Tencent>获取定向API请求失败！请求url[{}]，具体请求参数[{}]",
                    new Object[]{getTargetingInterface, params});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        // 3.结果数据封装，转换为Map
        JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
        int code = jsonObject.get("code").getAsInt();
        if (code != 0) {
            logger.error("<Tencent>获取定向API数据请求响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]",
                    new Object[]{getTargetingInterface, params, code, jsonObject});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        JsonObject data = jsonObject.get("data").getAsJsonObject();
        JsonArray dataJsonObject = data.get("list").getAsJsonArray();
        List<Targeting> response = StringUtil.gson.fromJson(dataJsonObject, new TypeToken<ArrayList<Targeting>>() {
        }.getType());
        List<TargetingVO> result = new ArrayList<>();
        for (Targeting obj : response) {
            TargetingVO targeting = new TargetingVO();
            result.add(targeting);
            targeting.setTargetingId(obj.getTargeting_id());
            targeting.setTargetingName(obj.getTargeting_name());
            String description = obj.getDescription();
            if (StringUtils.isNotBlank(description)) {
                targeting.setDescription(description);
            }
            if (obj.getTargeting() != null) {
                targeting.setTargeting(obj.getTargeting());
            }
        }
        return result;

    }

    /**
     * 新增定向
     *
     * @param account_id(必填)     广告主id
     * @param targeting_name(必填) 定向包名称
     * @param targeting          定向详细条件json
     * @param description        定向描述
     * @param access_token       授权token
     * @return Integer
     */
    public Integer add(Integer account_id, String targeting_name, String targeting, String description, String access_token) {
        // 1.构建请求参数
        Map<String, String> params = new HashMap<String, String>();
        params.put("account_id", account_id + "");
        params.put("targeting_name", targeting_name);
        if (StringUtils.isNotBlank(targeting)) {
            params.put("targeting", targeting);
        }
        if (StringUtils.isNotBlank(description)) {
            params.put("description", description);
        }
        String addTargetingInterface = ConfigHandler.getProperty(
                InterfaceConstants.TencentInterface.TENCENT_URL_KEY, InterfaceConstants.TencentInterface.TENCENT_URL) +
                InterfaceConstants.TencentInterface.TARGETING_ADD;
        String addTargetingInterfaceUrl = HttpUtil.urlParamReplace(addTargetingInterface,
                access_token, DateUtils.getSecondTimestamp(new Date()) + "", RandomUtils.getRandomString(32));
        // 2.HTTP请求获取数据
        String httpResponse = HttpRequest.post(addTargetingInterfaceUrl, params);
        if (StringUtils.isBlank(httpResponse)) {
            logger.error("<Tencent>请求创建定向API失败！请求url[{}]，具体请求参数[{}]", new Object[]{addTargetingInterfaceUrl, params});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        // 3.结果数据封装，转换为Map
        JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
        int code = jsonObject.get("code").getAsInt();
        if (code != 0) {
            logger.error("<Tencent>请求创建定向API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]",
                    new Object[]{addTargetingInterfaceUrl, params, code, jsonObject});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        String data = jsonObject.get("data").toString();
        JsonObject dataJsonObject = StringUtil.jsonParser.parse(data).getAsJsonObject();
        return dataJsonObject.get("targeting_id").getAsInt();
    }

    /**
     * 更新定向
     *
     * @param account_id(必填)     广告主id
     * @param targeting_id(必填)   定向包id
     * @param targeting_name(必填) 定向包名称
     * @param targeting          定向详细条件json
     * @param description        定向描述
     * @param access_token       授权token
     * @return Integer
     */
    public Integer update(Integer account_id, Integer targeting_id, String targeting_name, String targeting, String description, String access_token) {
        // 1.构建请求参数
        Map<String, String> params = new HashMap<String, String>();
        params.put("account_id", account_id + "");
        params.put("targeting_id", targeting_id + "");
        params.put("targeting_name", targeting_name);
        if (StringUtils.isNotBlank(targeting)) {
            params.put("targeting", targeting);
        }
        if (StringUtils.isNotBlank(description)) {
            params.put("description", description);
        }
        String updateTargetingInterface = ConfigHandler.getProperty(
                InterfaceConstants.TencentInterface.TENCENT_URL_KEY, InterfaceConstants.TencentInterface.TENCENT_URL) +
                InterfaceConstants.TencentInterface.TARGETING_UPDATE;
        String updateTargetingInterfaceUrl = HttpUtil.urlParamReplace(updateTargetingInterface,
                access_token, DateUtils.getSecondTimestamp(new Date()) + "", RandomUtils.getRandomString(32));
        // 2.HTTP请求获取数据
        String httpResponse = HttpRequest.post(updateTargetingInterfaceUrl, params);
        if (StringUtils.isBlank(httpResponse)) {
            logger.error("<Tencent>请求更新定向API失败！请求url[{}]，具体请求参数[{}]", new Object[]{updateTargetingInterfaceUrl, params});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        // 3.结果数据封装，转换为Map
        JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
        int code = jsonObject.get("code").getAsInt();
        if (code != 0) {
            logger.error("<Tencent>请求更新定向API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]",
                    new Object[]{updateTargetingInterfaceUrl, params, code, jsonObject});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        String data = jsonObject.get("data").toString();
        JsonObject dataJsonObject = StringUtil.jsonParser.parse(data).getAsJsonObject();
        return dataJsonObject.get("targeting_id").getAsInt();
    }

    public Integer delete(Integer account_id, Integer targeting_id, String access_token) {
        // 1.构建请求参数
        Map<String, String> params = new HashMap<String, String>();
        params.put("account_id", account_id + "");
        params.put("targeting_id", targeting_id + "");
        String deleteTargetingInterface = ConfigHandler.getProperty(
                InterfaceConstants.TencentInterface.TENCENT_URL_KEY, InterfaceConstants.TencentInterface.TENCENT_URL) +
                InterfaceConstants.TencentInterface.TARGETING_DELETE;
        String deleteTargetingInterfaceUrl = HttpUtil.urlParamReplace(deleteTargetingInterface,
                access_token, DateUtils.getSecondTimestamp(new Date()) + "", RandomUtils.getRandomString(32));
        // 2.HTTP请求获取数据
        String httpResponse = HttpRequest.post(deleteTargetingInterfaceUrl, params);
        if (StringUtils.isBlank(httpResponse)) {
            logger.error("<Tencent>请求删除定向API失败！请求url[{}]，具体请求参数[{}]", new Object[]{deleteTargetingInterfaceUrl, params});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        // 3.结果数据封装，转换为Map
        JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
        int code = jsonObject.get("code").getAsInt();
        if (code != 0) {
            logger.error("<Tencent>请求更新定向API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]",
                    new Object[]{deleteTargetingInterfaceUrl, params, code, jsonObject});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        String data = jsonObject.get("data").toString();
        JsonObject dataJsonObject = StringUtil.jsonParser.parse(data).getAsJsonObject();
        return dataJsonObject.get("targeting_id").getAsInt();

    }

    //参数校验
    private void validateParamsByAccount(Integer account_id, String accessToken) {
        if (account_id == null || account_id == 0) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCOUNT_ID_IS_NULL);
        }
        if (StringUtils.isBlank(accessToken)) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCESSTOKEN_ID_IS_NULL);
        }
    }

}
