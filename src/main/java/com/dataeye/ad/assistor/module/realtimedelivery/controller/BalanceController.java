package com.dataeye.ad.assistor.module.realtimedelivery.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Balance;
import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceQuery;
import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceTrendQuery;
import com.dataeye.ad.assistor.module.realtimedelivery.model.MediumAccountQuery;
import com.dataeye.ad.assistor.module.realtimedelivery.service.BalanceService;
import com.dataeye.ad.assistor.module.report.validtor.SqlValidator;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.StringUtil;

/**
 * 账号余额Controller
 * Created by huangzehai on 2017/4/19.
 */
@Controller
public class BalanceController {
    /**
     * 默认排序字段.
     */
    private static final String DEFAULT_ORDER_BY = "balance";
    /**
     * 默认排序方式.
     */
    private static final String DEFAULT_ORDER = "desc";

    /**
     * 媒体账号排序字段.
     */
    private static final String MEDIUM_ACCOUNT = "medium_account";

    /**
     * 媒体账号字段的代替值.使用中文排序.
     */
    private static final String MEDIUM_ACCOUNT_REPLACEMENT = "CONVERT(medium_account USING gbk)";

    @Autowired
    private BalanceService balanceService;

    /**
     * 账户余额列表.
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/balance/list.do")
    public Object listAccountBalance(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        DEParameter parameter = context.getDeParameter();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
//        if (!userInSession.hasManagerPermission()) {
//            return null;
//        }
        String orderBy = parameter.getOrderBy();
        String order = parameter.getOrder();
        //检验排序方式和排序字段
        if (!SqlValidator.isValidOrderBy(orderBy)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_ORDER_BY_INVALID);
        }
        if (!SqlValidator.isValidOrder(order)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_ORDER_INVALID);
        }

        //修正中文排序
        if (StringUtils.equalsIgnoreCase(orderBy, MEDIUM_ACCOUNT)) {
            orderBy = MEDIUM_ACCOUNT_REPLACEMENT;
        }
        String mediumIds = parameter.getMediumIds();
        String mediumAccountIds = parameter.getParameter(DEParameter.Keys.MEDIUM_ACCOUNT_IDS);

        BalanceQuery balanceQuery = new BalanceQuery();
        balanceQuery.setCompanyId(userInSession.getCompanyId());
        balanceQuery.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        balanceQuery.setOptimizerPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());
        balanceQuery.setOrder(StringUtils.defaultIfEmpty(order, DEFAULT_ORDER));
        balanceQuery.setOrderBy(StringUtils.defaultIfEmpty(orderBy, DEFAULT_ORDER_BY));
        balanceQuery.setMediumIds(StringUtil.stringToList(mediumIds));
        balanceQuery.setMediumAccountIds(StringUtil.stringToList(mediumAccountIds));
        return balanceService.listAccountBalance(balanceQuery);
    }

    /**
     * 账户余额趋势图.
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/balance/trend.do")
    public Object balanceTrend(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        //账户余额仅供负责人查看
//        if (!userInSession.hasManagerPermission()) {
//            return null;
//        }
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }
        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        String mediumIds = parameter.getMediumIds();
        String mediumAccountIds = parameter.getParameter(DEParameter.Keys.MEDIUM_ACCOUNT_IDS);

        // 构建查询条件
        BalanceTrendQuery balanceTrendQuery = new BalanceTrendQuery();
        balanceTrendQuery.setStartDate(startDate);
        balanceTrendQuery.setEndDate(endDate);
        balanceTrendQuery.setMediumIds(StringUtil.stringToList(mediumIds));
        balanceTrendQuery.setMediumAccountIds(StringUtil.stringToList(mediumAccountIds));
        balanceTrendQuery.setCompanyId(userInSession.getCompanyId());
        balanceTrendQuery.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        balanceTrendQuery.setOptimizerPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());
        return balanceService.balanceTrend(balanceTrendQuery);
    }


    /**
     * 获取负责人管理的媒体账号
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/manager/medium-accounts.do")
    public Object listManagingMediumAccounts(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        DEParameter parameter = context.getDeParameter();
        String mediumIds = parameter.getMediumIds();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
//        if (!userInSession.hasManagerPermission()) {
//            return null;
//        }
        MediumAccountQuery mediumAccountQuery = new MediumAccountQuery();
        mediumAccountQuery.setCompanyId(userInSession.getCompanyId());
        mediumAccountQuery.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        mediumAccountQuery.setOptimizerPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());
        mediumAccountQuery.setMediumIds(StringUtil.stringToList(mediumIds));
        return balanceService.listManagingMediumAccounts(mediumAccountQuery);
    }

    /**
     * 获取负责人管理的媒体
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/manager/mediums.do")
    public Object listManagingMediums(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
//        if (!userInSession.hasManagerPermission()) {
//            return null;
//        }
        return balanceService.listManagingMediums(userInSession);
    }

}