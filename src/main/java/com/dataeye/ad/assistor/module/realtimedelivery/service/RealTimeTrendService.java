package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeTrend;
import com.dataeye.ad.assistor.module.realtimedelivery.model.RealTimeTrendChart;
import com.dataeye.ad.assistor.module.realtimedelivery.model.TrendQuery;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by huangzehai on 2017/2/24.
 */
public interface RealTimeTrendService {
    /**
     * 实时趋势
     *
     * @param trendQuery 查询条件
     * @return
     */
    RealTimeTrendChart<BigDecimal, Object> realTimeTrendChart(TrendQuery trendQuery);

    /**
     * 实时趋势
     *
     * @param trendQuery 查询条件
     * @return
     */
    List<RealTimeTrend> realTimeTrend(TrendQuery trendQuery);

}
