package com.dataeye.ad.assistor.module.systemaccount.model;

import com.google.gson.annotations.Expose;

/**
 * 系统账户分组
 * @author luzhuyou 2017/02/17
 */
public class GroupBasicVo {
	/** 公司ID */
	private Integer companyId;
	/** 分组类型: 1-全部，2-具体分组，3-未分组 */
	@Expose
	private Integer groupType;
	/** 分组ID(默认-1：未分组) */
	@Expose
	private Integer groupId;
	/** 分组名称 */
	@Expose
	private String groupName;
	/** 组长账号 */
	private String leaderAccount;
	/** 组员人数 */
	@Expose
	private Integer memberNumber;
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getGroupType() {
		return groupType;
	}
	public void setGroupType(Integer groupType) {
		this.groupType = groupType;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getLeaderAccount() {
		return leaderAccount;
	}
	public void setLeaderAccount(String leaderAccount) {
		this.leaderAccount = leaderAccount;
	}
	public Integer getMemberNumber() {
		return memberNumber;
	}
	public void setMemberNumber(Integer memberNumber) {
		this.memberNumber = memberNumber;
	}
	@Override
	public String toString() {
		return "BasicGroupVo [companyId=" + companyId + ", groupType="
				+ groupType + ", groupId=" + groupId + ", groupName="
				+ groupName + ", leaderAccount=" + leaderAccount
				+ ", memberNumber=" + memberNumber + "]";
	}
	
}
