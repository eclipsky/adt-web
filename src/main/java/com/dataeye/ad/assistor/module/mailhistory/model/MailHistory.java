package com.dataeye.ad.assistor.module.mailhistory.model;

import java.util.Date;

/**
 * 邮件发送历史
 * @author luzhuyou 2017/02/24
 *
 */
public class MailHistory {

	/** ID */
	private Integer id;
	/** 邮件类型：0-创建系统账号，1-密码重置，2-告警邮件 */
	private Integer mailType;
	/** 发件人 */
	private String sender;
	/** 收件人 */
	private String recipients;
	/** 发送状态：0-成功，1-失败 */
	private Integer sendStatus;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getMailType() {
		return mailType;
	}
	public void setMailType(Integer mailType) {
		this.mailType = mailType;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getRecipients() {
		return recipients;
	}
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}
	public Integer getSendStatus() {
		return sendStatus;
	}
	public void setSendStatus(Integer sendStatus) {
		this.sendStatus = sendStatus;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "MailHistory [id=" + id + ", mailType=" + mailType + ", sender="
				+ sender + ", recipients=" + recipients + ", sendStatus="
				+ sendStatus + ", remark=" + remark + ", createTime="
				+ createTime + "]";
	}
}
