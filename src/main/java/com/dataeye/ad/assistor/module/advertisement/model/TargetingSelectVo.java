package com.dataeye.ad.assistor.module.advertisement.model;


import com.google.gson.annotations.Expose;

import java.util.Date;


/**
 * 定向
 * Created by ldj
 */
public class TargetingSelectVo {

    /**
     * 定向 id
     **/
    @Expose
    private Integer targetingId;
    /**
     * 定向名称
     **/
    @Expose
    private String targetingName;

    private int isPublic;

    private Integer mediumAccountId;

    private Date createTime;

    private Date updateTime;

    public Integer getTargetingId() {
        return targetingId;
    }

    public void setTargetingId(Integer targetingId) {
        this.targetingId = targetingId;
    }

    public String getTargetingName() {
        return targetingName;
    }

    public void setTargetingName(String targetingName) {
        this.targetingName = targetingName;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public TargetingSelectVo() {
    }

    public TargetingSelectVo(Integer targetingId, String targetingName, int isPublic, Integer mediumAccountId, Date createTime, Date updateTime) {
        this.targetingId = targetingId;
        this.targetingName = targetingName;
        this.isPublic = isPublic;
        this.mediumAccountId = mediumAccountId;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TargetingSelectVo{" +
                "targetingId=" + targetingId +
                ", targetingName='" + targetingName + '\'' +
                ", isPublic=" + isPublic +
                ", mediumAccountId=" + mediumAccountId +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
