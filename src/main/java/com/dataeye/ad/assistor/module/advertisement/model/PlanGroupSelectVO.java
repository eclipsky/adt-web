package com.dataeye.ad.assistor.module.advertisement.model;

import java.math.BigDecimal;

import com.google.gson.annotations.Expose;


/**
 * 推广计划组下拉框
 * Created by ldj
 */
public class PlanGroupSelectVO {
	
	private Integer Id;

	/**计划组id**/
	@Expose
    private Integer planGroupId;
	/**计划组名称**/
	@Expose
    private String planGroupName;
	/**日限额，单位为元，精确到分**/
	@Expose
    private BigDecimal dailyBudget;
	/**投放速度模式**/
	@Expose
    private Integer speedMode;
	/**客户设置的状态，即开关操作*/
	@Expose
    private Integer planGroupStatus;
	/**标的物类型*/
	@Expose
	private Integer productType;
	
	public Integer getPlanGroupId() {
		return planGroupId;
	}
	public void setPlanGroupId(Integer planGroupId) {
		this.planGroupId = planGroupId;
	}
	public String getPlanGroupName() {
		return planGroupName;
	}
	public void setPlanGroupName(String planGroupName) {
		this.planGroupName = planGroupName;
	}
	public BigDecimal getDailyBudget() {
		return dailyBudget;
	}
	public void setDailyBudget(BigDecimal dailyBudget) {
		this.dailyBudget = dailyBudget;
	}
	public Integer getSpeedMode() {
		return speedMode;
	}
	public void setSpeedMode(Integer speedMode) {
		this.speedMode = speedMode;
	}
	public Integer getPlanGroupStatus() {
		return planGroupStatus;
	}
	public void setPlanGroupStatus(Integer planGroupStatus) {
		this.planGroupStatus = planGroupStatus;
	}
	public Integer getProductType() {
		return productType;
	}
	public void setProductType(Integer productType) {
		this.productType = productType;
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	@Override
	public String toString() {
		return "PlanGroupSelectVO [Id=" + Id + ", planGroupId=" + planGroupId
				+ ", planGroupName=" + planGroupName + ", dailyBudget="
				+ dailyBudget + ", speedMode=" + speedMode
				+ ", planGroupStatus=" + planGroupStatus + ", productType="
				+ productType + "]";
	}
	
	
}
