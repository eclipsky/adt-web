package com.dataeye.ad.assistor.module.mediumaccount.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.Constant.MediumType;
import com.dataeye.ad.assistor.constant.RemoteInterfaceConstants.Tracking;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.mediumaccount.mapper.MediumMapper;
import com.dataeye.ad.assistor.module.mediumaccount.model.Channel;
import com.dataeye.ad.assistor.module.mediumaccount.model.CrawlerPage;
import com.dataeye.ad.assistor.module.mediumaccount.model.Medium;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumGroupVo;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumSelectVo;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumVo;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;

/**
 * 媒体Service
 */
@Service("mediumService")
public class MediumService {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(MediumService.class);

    @Autowired
    private MediumMapper mediumMapper;

    @Autowired
    private CrawlerPageService crawlerPageService;
    /**
     * 查询媒体列表信息
     * @return
     * @update ldj 2017-08-14
     */
	public List<Medium> queryAll(int companyId, String productIds) {
		Medium vo = new Medium();
		vo.setCompanyId(companyId);
		if (null != productIds) {
            String[] productIdArr = productIds.split(",");
            vo.setProductIds(productIdArr);
        }
		return mediumMapper.queryAll(vo);
	}
	
	/**
     * 查询ADT已对接媒体列表信息(id范围：1-999)
     * @return
     */
	public List<Medium> queryAdtMedium() {
		return mediumMapper.queryAdtMedium();
	}
	
	/**
     * 查询Tracking通用媒体列表信息（不含ADT已有部分）（id范围：1000-9999）
     * @return
     */
	public List<Medium> queryTrackingCommonMedium() {
		return mediumMapper.queryTrackingCommonMedium();
	}
	
	 /**
     * 查询自定义媒体列表信息（id范围：100000-）
     * @return
     */
	public List<Medium> queryCustomMedium(int companyId) {
		return mediumMapper.queryCustomMedium(companyId);
	}
	
	/**
	 * 查询媒体列表和媒体下的爬虫页面集合
	 * @param mediumId  媒体id
	 * @return 
	 * @create by ldj 2017-06-23
	 * @updater luzhuyou 2017-08-10
	 * */
	public List<MediumGroupVo<MediumVo>> queryMediumForCrawlerPage(int companyId) {
		List<MediumGroupVo<MediumVo>> mediumGroupList = new ArrayList<MediumGroupVo<MediumVo>>();
		// 1-ADT已对接媒体
		MediumGroupVo<MediumVo> adtMedium = new MediumGroupVo<MediumVo>(); 
		adtMedium.setMediumType(MediumType.ADT);
		adtMedium.setMediumList(queryMediumCrawlerPage(this.queryAdtMedium()));
		mediumGroupList.add(adtMedium);
		
		// 2-Tracking通用媒体
		MediumGroupVo<MediumVo> trackingCommonMedium = new MediumGroupVo<MediumVo>(); 
		trackingCommonMedium.setMediumType(MediumType.TRACKING_COMMON);
		trackingCommonMedium.setMediumList(queryMediumCrawlerPage(this.queryTrackingCommonMedium()));
		mediumGroupList.add(trackingCommonMedium);
		
		// 3-自定义媒体
		MediumGroupVo<MediumVo> customMedium = new MediumGroupVo<MediumVo>(); 
		customMedium.setMediumType(MediumType.CUSTOM);
		customMedium.setMediumList(queryMediumCrawlerPage(this.queryCustomMedium(companyId)));
		mediumGroupList.add(customMedium);
		return mediumGroupList;
	}
	
	/**
	 * 根据媒体列表查询媒体的爬虫计划分组情况
	 * @return
	 */
	private List<MediumVo> queryMediumCrawlerPage(List<Medium> listMedium) {
		List<MediumVo> result = new ArrayList<MediumVo>(); 
		for (Medium m : listMedium) {
			List<CrawlerPage> crawlerPageList = crawlerPageService.query(m.getMediumId());
			if(crawlerPageList.isEmpty()){
				crawlerPageList = new ArrayList<CrawlerPage>(); 
			}
			MediumVo vo = new MediumVo();
			vo.setMediumId(m.getMediumId());
			vo.setMediumName(m.getMediumName());
			vo.setList(crawlerPageList);
			result.add(vo);
		}
		return result;
	}

	/**
	 * 获取Tracking媒体ID
	 * @return
	 * @update ldj 2017-08-10 查询(ac_medium_tracking_mapping) >> 改成>> 查询(ac_channel)
	 * 
	 */
	public String getTrackingMediumId(Channel mapping) {
		return mediumMapper.getTrackingMediumId(mapping);
	}

	/**
	 * 根据媒体名称及公司id获取媒体信息
	 * @param mediumName
	 * @param companyId
	 * @return
	 */
	public Medium get(String mediumName, int companyId) {
		Medium medium = new Medium();
		medium.setCompanyId(companyId);
		medium.setMediumName(mediumName);
		return mediumMapper.get(medium);
	}

	/**
	 * 新增自定义媒体
	 * @param mediumName
	 * @param companyId
	 * @param uid
	 * @return
	 */
	public int addCustomMedium(String mediumName, int companyId, int uid) {
		int mediumId = this.addTrackingChannel(uid, mediumName);
		Medium medium = new Medium();
		medium.setMediumId(mediumId);
		medium.setCompanyId(companyId);
		medium.setMediumName(mediumName);
		medium.setCreateTime(new Date());
		medium.setUpdateTime(new Date());
		return mediumMapper.addCustomMedium(medium);
	}
	
	/**
	 * 新增Tracking渠道
	 * @param uid
	 * @param mediumName 
	 * @return
	 */
	public int addTrackingChannel(int uid, String mediumName) {
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("uid", uid+"");
		params.put("channelName", mediumName);
		
		String addChannelInterface = ConfigHandler.getProperty(Tracking.ADD_CHANNEL_KEY, Tracking.ADD_CHANNEL);
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(addChannelInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("新增渠道接口数据请求失败");
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int statusCode = jsonObject.get("statusCode").getAsInt();
    	if(statusCode != 200) {
    		logger.error("新增渠道接口数据响应异常，异常状态码[{}]，具体响应内容[{}]", new Object[]{statusCode, jsonObject});
    		if( statusCode == 406 ){  //渠道已经存在
        		ExceptionHandler.throwParameterException(StatusCode.MEDI_MEDIUM_IS_EXISTS);
        	}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_SEVER_ERROR);
        }
    	
    	int channelId = jsonObject.get("content").getAsInt();
    	return channelId;
	}

	/**
	 * 
	 * 根据产品id，查询媒体列表和对应的tracking渠道列表 
	 * @author ldj 2017-08-10
	 * @param
	 * */
	public List<MediumSelectVo> queryMediumAndChannelByProductId(int companyId, String productIds) {
		List<MediumSelectVo> result = new ArrayList<>();
		List<Medium> mediumList = queryAll(companyId,  productIds) ;
		for (Medium m : mediumList) {
			Integer mediumId = m.getMediumId();
			String channelId = m.getMediumId()+"";
			Channel channel = new Channel();
			channel.setMediumId(mediumId);
			List<Channel> channelResult = new ArrayList<Channel>(); 
			if(MediumType.getMediumType(mediumId) == MediumType.ADT){
				List<Channel> channelList = queryMediumMappingFromCache(mediumId);
				if(channelList == null || channelList.size() == 0){
					channelId = "";
					System.out.println("mediumId=="+mediumId);
				}else if (channelList.size() == 1) {
					channelId = channelList.get(0).getChannelId();
				}else if(channelList.size() >= 2){
					channelResult.addAll(channelList);
					channelId = "";
				}
			}
			MediumSelectVo vo = new MediumSelectVo();
			vo.setMediumId(mediumId);
			vo.setChannelId(channelId);
			vo.setMediumName(m.getMediumName());
			vo.setList(channelResult);
			result.add(vo);
		}
		return result;
	}
	
	/**
	 * 得到所有的ADT媒体对应的渠道信息
	 * @return
	 * @author ldj 2017-08-11
	 */
	public List<Channel> getAdtMediumMapping() {
		return mediumMapper.getAdtMediumMapping();
	}
	
	/**
	 * 根据mediumId,得到对应的渠道对象
	 *  @author ldj 2017-08-11
	 * **/
	public List<Channel> queryMediumMappingFromCache(Integer mediumId) {
		return ApplicationContextContainer.getAdtMediumMappingList(mediumId);
	}
	
}
