package com.dataeye.ad.assistor.module.dictionaries.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.List;

/**
 * 投放计划VO
 *
 * @author luzhuyou 2017/03/02
 */
public class PlanVo extends DataPermissionDomain {

    /**
     * 计划ID
     */
    private Integer planId;
    /**
     * 计划名称
     */
    private String planName;
    /**
     * 媒体ID
     */
    private Integer mediumId;
    /**
     * 媒体账户ID
     */
    private Integer mediumAccountId;

    /**
     * 产品ID列表
     */
    private List<Integer> productIds;
    /**
     * 媒体ID列表
     */
    private List<Integer> mediumIds;
    /**
     * 媒体账号ID列表
     */
    private List<Integer> mediumAccountIds;

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public List<Integer> getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(List<Integer> mediumIds) {
        this.mediumIds = mediumIds;
    }

    public List<Integer> getMediumAccountIds() {
        return mediumAccountIds;
    }

    public void setMediumAccountIds(List<Integer> mediumAccountIds) {
        this.mediumAccountIds = mediumAccountIds;
    }

    @Override
    public String toString() {
        return "PlanVo{" +
                "planId=" + planId +
                ", planName='" + planName + '\'' +
                ", mediumId=" + mediumId +
                ", mediumAccountId=" + mediumAccountId +
                ", productIds=" + productIds +
                ", mediumIds=" + mediumIds +
                ", mediumAccountIds=" + mediumAccountIds +
                "} " + super.toString();
    }
}
