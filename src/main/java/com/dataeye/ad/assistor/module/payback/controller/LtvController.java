package com.dataeye.ad.assistor.module.payback.controller;

import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.payback.model.LtvDailyReport;
import com.dataeye.ad.assistor.module.payback.model.PaybackQuery;
import com.dataeye.ad.assistor.module.payback.model.PaybackTrendQuery;
import com.dataeye.ad.assistor.module.payback.service.LtvService;
import com.dataeye.ad.assistor.module.payback.util.LtvExcelUtils;
import com.dataeye.ad.assistor.module.report.constant.Constants;
import com.dataeye.ad.assistor.module.report.validtor.SqlValidator;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Controller
public class LtvController {

    private static final String CONTENT_TYPE = "application/msexcel;charset=UTF-8";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String CONTENT_DISPOSITION_VALUE = "attachment;filename=ltv-analytics.xls";

    @Autowired
    private LtvService ltvService;

    /**
     * LTV分析
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/payback/ltv/daily.do")
    public Object ltvDailyReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        // 如果为null，则表示无数据访问权限
        if (userInSession == null) {
            return null;
        }
        PaybackQuery query = parseQuery(context);
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        return ltvService.ltvDailyReport(query);
    }

    /**
     * 投放日报下载当前表格
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/payback/ltv/daily/download.do")
    public void downloadLtvDailyReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<LtvDailyReport> reports = (List<LtvDailyReport>) this.ltvDailyReport(request, response);
        if (reports != null) {
            Workbook wb = LtvExcelUtils.buildDailyPaybackReport(reports);
            // Write the output to a file
            response.setContentType(CONTENT_TYPE);
            response.addHeader(CONTENT_DISPOSITION, CONTENT_DISPOSITION_VALUE);
            ServletOutputStream out = response.getOutputStream();
            wb.write(out);
            out.flush();
            out.close();
        }
    }


    /**
     * 解析按日统计参数
     *
     * @param context
     * @return
     * @throws ParseException
     */
    private PaybackQuery parseQuery(DEContext context) throws ParseException {
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }

        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());


        //解析产品ID列表
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        String filterNaturalFlow = parameter.getParameter(DEParameter.Keys.FILTER_NATURAL_FLOW);

        // 默认只按时间排序
        String order = parameter.getParameter(Constants.ORDER);
        if (!SqlValidator.isValidOrder(order)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_ORDER_INVALID);
        }

        //构建回本查询条件
        PaybackQuery query = new PaybackQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setMediumIds(StringUtil.stringToList(parameter.getMediumIds()));
        query.setAccountIds(StringUtil.stringToList(parameter.getAccountIds()));
        query.setPlanIds(StringUtil.stringToList(parameter.getPlanIds()));
        query.setFilterNaturalFlow(Boolean.valueOf(filterNaturalFlow));
        query.setOrder(null == order ? parameter.getOrder() : order);
        return query;
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/payback/ltv/trend.do")
    public Object ltvDailyTrend(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //参数解析与校验
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (permissionMediumAccountIds == null) {
            return null;
        }

        DEParameter parameter = context.getDeParameter();
        //解析同日期
        String dateString = parameter.getDate();
        if (StringUtils.isBlank(dateString)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_DATE_BLANK);
        }

        Date date = DateUtils.parse(dateString);
        String filterNaturalFlow = parameter.getParameter(DEParameter.Keys.FILTER_NATURAL_FLOW);

        PaybackTrendQuery query = new PaybackTrendQuery();
        query.setCompanyId(companyId);
        query.setPermissionMediumAccountIds(permissionMediumAccountIds);
        query.setDate(date);

        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setPlanIds(StringUtil.stringToList(parameter.getPlanIds()));
        query.setAccountIds(StringUtil.stringToList(parameter.getAccountIds()));
        query.setMediumIds(StringUtil.stringToList(parameter.getMediumIds()));
        query.setFilterNaturalFlow(Boolean.valueOf(filterNaturalFlow));
        return ltvService.ltvTrendChart(query);
    }

}
