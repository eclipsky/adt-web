package com.dataeye.ad.assistor.module.advertisement.model;

import java.util.List;




/**
 * 广告相关参数
 * Created by ldj
 */
public class AdParamsVo {
	
	private Integer planGroupId;
	/**广告组id**/
    private Integer adGroupId;
	/**定向 id**/
    private Integer targetingId;
    private List<Integer> adcreativeId;

    private List<Integer> adsId;
    private Integer mediumId;
	/** 公司ID */
	private Integer companyId;
	/** ADT账号ID */
	private Integer systemAccountId;
	private Integer mediumAccountId;
	public Integer getPlanGroupId() {
		return planGroupId;
	}
	public void setPlanGroupId(Integer planGroupId) {
		this.planGroupId = planGroupId;
	}
	public Integer getAdGroupId() {
		return adGroupId;
	}
	public void setAdGroupId(Integer adGroupId) {
		this.adGroupId = adGroupId;
	}
	public Integer getTargetingId() {
		return targetingId;
	}
	public void setTargetingId(Integer targetingId) {
		this.targetingId = targetingId;
	}
	public List<Integer> getAdcreativeId() {
		return adcreativeId;
	}
	public void setAdcreativeId(List<Integer> adcreativeId) {
		this.adcreativeId = adcreativeId;
	}
	public List<Integer> getAdsId() {
		return adsId;
	}
	public void setAdsId(List<Integer> adsId) {
		this.adsId = adsId;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getSystemAccountId() {
		return systemAccountId;
	}
	public void setSystemAccountId(Integer systemAccountId) {
		this.systemAccountId = systemAccountId;
	}
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	
	
}
