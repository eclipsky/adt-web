package com.dataeye.ad.assistor.module.campaign.constants;

import org.apache.commons.lang.StringUtils;


/**
 * 查询粒度 ，天、周、月
 */
public enum CampaignEventsType {
	ACTIVE, REGISTER, PAY;
	public static CampaignEventsType parse(String text) {
        for (CampaignEventsType events : CampaignEventsType.values()) {
            if (StringUtils.equalsIgnoreCase(events.name(), text)) {
                return events;
            }
        }
        return null;
    }
}
