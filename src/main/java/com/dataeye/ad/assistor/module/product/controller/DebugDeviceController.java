package com.dataeye.ad.assistor.module.product.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.product.model.DebugDevice;
import com.dataeye.ad.assistor.module.product.service.DebugDeviceService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;

/**
 * Created by ldj 2017/10/30
 * 测试设备管理控制器.
 */
@Controller
public class DebugDeviceController {
	
    private static final Logger logger = (Logger) LoggerFactory.getLogger(DebugDeviceController.class);

    @Autowired
    private DebugDeviceService debugDeviceService;


    /**
     * 查询测试设备列表
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author ldj 2017/10/30
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/debugDevice/query.do")
    public Object query(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("查询测试设备列表");
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
        String appId = parameter.getParameter("appId");
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int uid = userInSession.getCompanyAccountUid();
        
        if(StringUtils.isBlank(appId)) {
			ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ID_ERROR);
        }
        List<DebugDevice> result  = debugDeviceService.query(uid, appId);
        
        PageData pageData = PageDataHelper.getPageDataByList(parameter.getPageSize(), parameter.getPageId(), result);
        return pageData;
    }
	
    /**
     * 新增测试设备
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author ldj 2017/10/30
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/debugDevice/add.do")
    public Object add(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("新增测试设备.");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String appid = parameter.getParameter("appId");
        String deviceName = parameter.getParameter("deviceName");
        String osType = parameter.getParameter("osType");
        String device = parameter.getParameter("device");
        
        if(StringUtils.isBlank(appid)) {
			ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ID_ERROR);
        }
        if(StringUtils.isBlank(deviceName)) {
			ExceptionHandler.throwParameterException(StatusCode.DEBUG_DEVICE_NAME_IS_NULL);
        }
        if(StringUtils.isBlank(osType)) {
			ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
        }
        if(StringUtils.isBlank(device)) {
			ExceptionHandler.throwParameterException(StatusCode.DEBUG_DEVICE_ID_IS_NULL);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
		//获取uid
		int uid = userInSession.getCompanyAccountUid();
        return debugDeviceService.add(uid, appid, Integer.parseInt(osType), deviceName, device);
    }
    
 
    /**
     * 删除测试设备
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author ldj  2017/10/30
     */
	@PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/debugDevice/delete.do")
    public Object queryProductForSelectorByResponsible(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
    	String appid = parameter.getParameter("appId");
        String id = parameter.getParameter("id");
        if(StringUtils.isBlank(appid)) {
			ExceptionHandler.throwParameterException(StatusCode.PROD_APP_ID_ERROR);
        }
        if(StringUtils.isBlank(id) && StringUtils.isNumeric(id)) {
			ExceptionHandler.throwParameterException(StatusCode.DEBUG_DEVICE_ID_IS_NULL);
        }
       
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int uid = userInSession.getCompanyAccountUid();
        
        return debugDeviceService.deleteDebugDevice(uid, appid, Integer.parseInt(id));
    }
	
    
    
}
