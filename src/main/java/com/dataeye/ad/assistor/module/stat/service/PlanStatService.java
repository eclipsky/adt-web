package com.dataeye.ad.assistor.module.stat.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.association.model.AssociationRuleHistoryVo;
import com.dataeye.ad.assistor.module.stat.mapper.PlanStatMapper;
import com.dataeye.ad.assistor.module.stat.model.PlanStat;
import com.dataeye.ad.assistor.module.stat.model.PlanStatHisVo;

/**
 * 计划统计服务
 * @author luzhuyou 2017/03/16
 *
 */
@Service
public class PlanStatService {
	
	@Autowired
	private PlanStatMapper planStatMapper;
	
	/**
	 * 修改（按天）计划统计表
	 * @param companyId 公司ID
	 * @param planId 计划ID
	 * @param pageId 落地页ID
	 * @param pkgId 包ID
	 * @param osType 系统类型：0-Others, 1-iOS, 2-Android
	 * @param statDate 统计日期 
	 * @return 
	 */
	public int modifyDayStat(Integer companyId, Integer planId, Integer pageId, Integer pkgId, Integer osType, String statDate) {
		PlanStat planStat = new PlanStat();
		planStat.setStatDate(statDate);
		planStat.setCompanyId(companyId);
		planStat.setPlanId(planId);
		planStat.setPageId(pageId);
		planStat.setPkgId(pkgId);
		planStat.setOsType(osType);
		planStat.setUpdateTime(new Date());
		return planStatMapper.updateDayStat(planStat);
	}
	
	/**
	 * 修改（实时）计划统计表
	 * @param companyId 公司ID
	 * @param planId 计划ID
	 * @param pageId 落地页ID
	 * @param pkgId 包ID
	 * @param osType 系统类型：0-Others, 1-iOS, 2-Android
	 * @param statDate 统计日期 
	 * @return 
	 */
	public int modifyRealTimeStat(Integer companyId, Integer planId, Integer pageId, Integer pkgId, Integer osType, String statDate) {
		PlanStat planStat = new PlanStat();
		planStat.setStatDate(statDate);
		planStat.setCompanyId(companyId);
		planStat.setPlanId(planId);
		planStat.setPageId(pageId);
		planStat.setPkgId(pkgId);
		planStat.setOsType(osType);
		planStat.setUpdateTime(new Date());
		return planStatMapper.updateRealTimeStat(planStat);
		
	}
	
	/**
	 * 查询历史上落地页或包已被绑定的计划关联关系（不包含指定计划）
	 * @return
	 */
	public List<PlanStatHisVo> queryHisPossibleConflictRelation(AssociationRuleHistoryVo vo) {
		return planStatMapper.queryHisPossibleConflictRelation(vo);
	}
}
