package com.dataeye.ad.assistor.module.payback.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by huangzehai on 2017/1/17.
 */
public class PaybackRate {
    /**
     * 付款日期
     */
    private Date payDate;

    /**
     * 付款日期
     */
    @Expose
    @SerializedName("payDate")
    private String payDateText;
    /**
     * 付款金额.
     */
    private BigDecimal payAmount;
    /**
     * 回本率
     */
    @Expose
    private double paybackRate;
    
    /**每日充值金额*/
    private BigDecimal dayPayAmount;
    
    /**每日充值金额*/
    @Expose
    @SerializedName("dayPayAmount")
    private String dayPayAmountText;

    public String getPayDateText() {
        return payDateText;
    }

    public void setPayDateText(String payDateText) {
        this.payDateText = payDateText;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public double getPaybackRate() {
        return paybackRate;
    }

    public void setPaybackRate(double paybackRate) {
        this.paybackRate = paybackRate;
    }

	public BigDecimal getDayPayAmount() {
		return dayPayAmount;
	}

	public void setDayPayAmount(BigDecimal dayPayAmount) {
		this.dayPayAmount = dayPayAmount;
	}

	public String getDayPayAmountText() {
		return dayPayAmountText;
	}

	public void setDayPayAmountText(String dayPayAmountText) {
		this.dayPayAmountText = dayPayAmountText;
	}
	
    
}
