package com.dataeye.ad.assistor.module.realtimedelivery.model;

import java.math.BigDecimal;

/**
 * Created by huangzehai on 2017/5/10.
 */
public class Bid extends MediumPlan {

    /**
     * 出价，单位元
     */
    private BigDecimal bid;

    /**
     * 出价方式.
     */
    private String bidStrategy;

    /**
     * 阶段(仅供今日头条使用,第一阶段：0，第二阶段：1)
     */
    private Integer isCpaBid;
    
    /**扩展字段*/
    private String bidExtend;

    public BigDecimal getBid() {
        return bid;
    }

    public void setBid(BigDecimal bid) {
        this.bid = bid;
    }

    public String getBidStrategy() {
        return bidStrategy;
    }

    public void setBidStrategy(String bidStrategy) {
        this.bidStrategy = bidStrategy;
    }

    public Integer getIsCpaBid() {
        return isCpaBid;
    }

    public void setIsCpaBid(Integer isCpaBid) {
        this.isCpaBid = isCpaBid;
    }

    public String getBidExtend() {
		return bidExtend;
	}

	public void setBidExtend(String bidExtend) {
		this.bidExtend = bidExtend;
	}

	@Override
	public String toString() {
		return "Bid [bid=" + bid + ", bidStrategy=" + bidStrategy
				+ ", isCpaBid=" + isCpaBid + ", bidExtend=" + bidExtend + "]";
	}
}
