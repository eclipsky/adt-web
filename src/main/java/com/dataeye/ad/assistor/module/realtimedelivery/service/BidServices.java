package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.constant.BidStrategy;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.Medium;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by huangzehai on 2017/5/18.
 */
public class BidServices {
    /**
     * 出价服务映射.
     */
    private static final Map<Integer, String> SERVICES = new HashMap<>();

    private static final Map<Integer, List<String>> AVAILABLE_BID_STRATEGIES = new HashMap<>();

    static {
        //映射出价服务
        SERVICES.put(Medium.Toutiao, "toutiaoBidService");
        SERVICES.put(Medium.Tui, "tuiBidService");
        SERVICES.put(Medium.BaiduFeedAds, "baiduBidService");
        SERVICES.put(Medium.Tencent, "tencentBidService");
        SERVICES.put(Medium.Uc, "ucBidService");
        AVAILABLE_BID_STRATEGIES.put(Medium.Toutiao, Arrays.asList(BidStrategy.CPC, BidStrategy.OCPC, BidStrategy.CPM, BidStrategy.CPV, BidStrategy.CPA));
        AVAILABLE_BID_STRATEGIES.put(Medium.Tui, Arrays.asList(BidStrategy.CPC));
        AVAILABLE_BID_STRATEGIES.put(Medium.BaiduFeedAds, Arrays.asList(BidStrategy.CPC,BidStrategy.OCPC));
        AVAILABLE_BID_STRATEGIES.put(Medium.Tencent, Arrays.asList(BidStrategy.CPC, BidStrategy.CPM));
        AVAILABLE_BID_STRATEGIES.put(Medium.Uc, Arrays.asList(BidStrategy.CPC, BidStrategy.CPM));
    }

    public static String getBidService(Integer mediumId) {
        if (mediumId == null) {
            return null;
        }
        return SERVICES.get(mediumId);
    }

    public static boolean isBidAvailable(Integer mediumId, String bidStrategy) {
        //转化为标准出价策略
        String standardBidStrategy = BidStrategy.getStandardBidStrategy(mediumId, bidStrategy);
        List<String> availableBidStrategies = AVAILABLE_BID_STRATEGIES.get(mediumId);
        return mediumId != null && SERVICES.containsKey(mediumId) && StringUtils.isNotBlank(standardBidStrategy) && availableBidStrategies != null && availableBidStrategies.contains(standardBidStrategy);
    }
}
