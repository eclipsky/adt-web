package com.dataeye.ad.assistor.module.result.model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;

/**
 * Created by huangzehai on 2017/4/6.
 */
public class Indicator {
    @Expose
    private String name;
    @Expose
    private BigDecimal value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
