package com.dataeye.ad.assistor.module.association.model;

import java.util.List;

/**
 * 计划历史关联关系(日期，计划，产品，落地页，包关联)
 * @author luzhuyou 2017/08/02
 *
 */
public class AssociationRuleHistoryVo {
	/**
	 * 公司ID
	 */
	private Integer companyId;
	/** 计划ID */
	private Integer planId;
	/** 页面ID */
	private Integer pageId;
	/** 包ID */
	private Integer pkgId;
	/** 统计日期数组 */
	private List<String> statDates;
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public Integer getPageId() {
		return pageId;
	}
	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}
	public Integer getPkgId() {
		return pkgId;
	}
	public void setPkgId(Integer pkgId) {
		this.pkgId = pkgId;
	}
	public List<String> getStatDates() {
		return statDates;
	}
	public void setStatDates(List<String> statDates) {
		this.statDates = statDates;
	}
	@Override
	public String toString() {
		return "AssociationRuleHistoryVo [companyId=" + companyId + ", planId="
				+ planId + ", pageId=" + pageId + ", pkgId=" + pkgId
				+ ", statDates=" + statDates + "]";
	}
}
