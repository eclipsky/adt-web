package com.dataeye.ad.assistor.module.association.service;

import ch.qos.logback.classic.Logger;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.association.mapper.AssociationRuleMapper;
import com.dataeye.ad.assistor.module.association.model.AssociationRuleHistoryVo;
import com.dataeye.ad.assistor.module.association.model.AssociationRuleVo;
import com.dataeye.ad.assistor.module.association.model.PkgRelation;
import com.dataeye.ad.assistor.module.association.model.PlanRelation;
import com.dataeye.ad.assistor.module.dictionaries.service.PlanService;
import com.dataeye.ad.assistor.module.stat.model.PlanStatHisVo;
import com.dataeye.ad.assistor.module.stat.service.PlanStatService;
import com.dataeye.ad.assistor.util.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("associationRuleService")
public class AssociationRuleService {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AssociationRuleService.class);

	@Autowired
	private AssociationRuleMapper associationRuleMapper;
	@Autowired
	private PlanStatService planStatService;
	@Autowired
	private PlanService planService;
	
	/**
	 * 查询关联规则信息列表
	 * @param companyId 公司ID
	 * @param planName 计划名称
	 * @param mediumIds 媒体ID：多个媒体ID，用逗号分隔
	 * @param mediumAccountIds 媒体账号ID：多个媒体账号ID，用逗号分隔
	 * @param permissionMediumAccountIds 具有访问权限的媒体账号ID数组
	 * @param unboundPlan 0-所有计划的关联规则信息，1-未绑定关联规则的计划
	 * @param productIds 产品id，多个产品id，用逗号隔开
	 * @return
	 */
	public List<AssociationRuleVo> query(int companyId, String planName, String mediumIds,
                                         String mediumAccountIds, Integer[] permissionMediumAccountIds, Integer unboundPlan, String productIds) {
		AssociationRuleVo associationRuleVo = new AssociationRuleVo();
		associationRuleVo.setCompanyId(companyId);
		associationRuleVo.setPlanName(planName);
		associationRuleVo.setPermissionMediumAccountIds(permissionMediumAccountIds);
		associationRuleVo.setUnboundPlan(unboundPlan);
		if (null != mediumIds) {
			String[] array = mediumIds.split(",");
			Integer[] mediumIdsArr = new Integer[array.length];
			for(int i=0; i<array.length; i++) {
				mediumIdsArr[i] = Integer.parseInt(array[i]);
			}
			associationRuleVo.setMediumIds(mediumIdsArr);
		}
		if (null != mediumAccountIds) {
			String[] array = mediumAccountIds.split(",");
			Integer[] mediumAccountIdsArr = new Integer[array.length];
			for(int i=0; i<array.length; i++) {
				mediumAccountIdsArr[i] = Integer.parseInt(array[i]);
			}
			associationRuleVo.setMediumAccountIds(mediumAccountIdsArr);
		}
		if (null != productIds) {
			String[] array = productIds.split(",");
			Integer[] productIdsArr = new Integer[array.length];
			for(int i=0; i<array.length; i++) {
				productIdsArr[i] = Integer.parseInt(array[i]);
			}
			associationRuleVo.setProductIds(productIdsArr);
		}
		List<AssociationRuleVo> result = associationRuleMapper.queryAssociationRule(associationRuleVo);
		return result;
	}

	/**
	 * 修改关联规则
	 * @param companyId
	 * @param productId
	 * @param planId
	 * @param pageId
	 * @param pkgId
	 * @param osType 系统类型：0-Others, 1-iOS, 2-Android
	 * @param updateStatus 更新状态 ： 0-初始提交更新，1-强制提交更新
	 * @return
	 */
	public int modify(int companyId, int productId, int planId, int pageId, int pkgId, int osType, String optimizerSystemAccount, int updateStatus) {
		//检查关联规则是否已存在
		PlanRelation relation = new PlanRelation();
		relation.setCompanyId(companyId);
		relation.setPageId(pageId);
		relation.setPkgId(pkgId);
		relation.setOsType(osType);
		PlanRelation oldPlanRelation = associationRuleMapper.getPlanRelation(planId);
		if(oldPlanRelation != null) {
			if(productId == oldPlanRelation.getProductId() && pageId == oldPlanRelation.getPageId() && pkgId == oldPlanRelation.getPkgId() && StringUtils.equalsIgnoreCase(optimizerSystemAccount,oldPlanRelation.getOptimizerSystemAccount())) {
				logger.info("没有进行修改!");
				return 0;
			}
		}
		
		String statDate = DateUtils.currentDate();
		List<PlanRelation> planRelationList = associationRuleMapper.queryPlanRelation(relation);
		if(planRelationList != null && !planRelationList.isEmpty()) {
			int size = planRelationList.size();
			for(int i=0; i<size; i++) {
				PlanRelation confictRecord = planRelationList.get(i);
				if(planId != confictRecord.getPlanId()) {
					// 增加强制提交关联规则逻辑，先强制解绑旧的关联规则，再更新新的关联规则
					int tmpResult = 0;
					int noPageId = 0;
					int noPkgId = 0;
					if(pageId != 0 && pageId == confictRecord.getPageId()) {
						logger.error("落地页已被[{}]计划关联.", confictRecord.getPlanName());
						// 0-初始提交更新
						if(updateStatus != 1) {
							ExceptionHandler.throwGeneralServerException(StatusCode.ASSO_PKG_USED_ERROR, new Object[]{confictRecord.getPlanName()});
						}
						logger.info("强制解绑旧的关联规则计划[{}]的落地页[{}].", new Object[]{confictRecord.getPlanName(), confictRecord.getPageId()});
						// 1-强制提交更新
						confictRecord.setPageId(noPageId);
						confictRecord.setUpdateTime(new Date());
						tmpResult = associationRuleMapper.updatePlanRelation(confictRecord);
						
						if(tmpResult > 0) {
							// 更新（按天）计划统计表
							planStatService.modifyDayStat(confictRecord.getCompanyId(), confictRecord.getPlanId(), noPageId, confictRecord.getPkgId(), confictRecord.getOsType(), statDate);
							// 更新（实时）计划统计表
							planStatService.modifyRealTimeStat(confictRecord.getCompanyId(), confictRecord.getPlanId(), noPageId, confictRecord.getPkgId(), confictRecord.getOsType(), statDate);
						}
					}
					if(pkgId != 0 && pkgId == confictRecord.getPkgId()) {
						logger.error("投放包已被[{}]计划关联.", confictRecord.getPlanName());
						
						// 0-初始提交更新
						if(updateStatus != 1) {
							ExceptionHandler.throwGeneralServerException(StatusCode.ASSO_PAGE_USED_ERROR, new Object[]{confictRecord.getPlanName()});
						}
						logger.info("强制解绑旧的关联规则计划[{}]的包[{}].", new Object[]{confictRecord.getPlanName(), confictRecord.getPkgId()});
						// 1-强制提交更新
						confictRecord.setPkgId(noPkgId);
						confictRecord.setUpdateTime(new Date());
						tmpResult = associationRuleMapper.updatePlanRelation(confictRecord);
						
						if(tmpResult > 0) {
							// 更新（按天）计划统计表
							planStatService.modifyDayStat(confictRecord.getCompanyId(), confictRecord.getPlanId(), confictRecord.getPageId(), noPkgId, confictRecord.getOsType(), statDate);
							// 更新（实时）计划统计表
							planStatService.modifyRealTimeStat(confictRecord.getCompanyId(), confictRecord.getPlanId(), confictRecord.getPageId(), noPkgId, confictRecord.getOsType(), statDate);
						}
					}
				}
			}
		}
		
		// modify by luzhuyou 20170801 关联规则页面开放支付查看和修改所有产品的包 
		// 如果对接了Tracking，则如果落地页更新为“无”，那包也同步更新为“无” add by luzhuyou 2017/05/10
//		if(productId != 1 && productId != 7) {
//			if(pageId == 0) {
//				pkgId = 0;
//			}
//		}
		
		// 修改关联规则
		int result = 0;
		if(oldPlanRelation != null) {
			oldPlanRelation.setProductId(productId);
			oldPlanRelation.setPageId(pageId);
			oldPlanRelation.setPkgId(pkgId);
			oldPlanRelation.setOsType(osType);
			oldPlanRelation.setUpdateTime(new Date());
			oldPlanRelation.setOptimizerSystemAccount(optimizerSystemAccount);
			result = associationRuleMapper.updatePlanRelation(oldPlanRelation);
		} else {
			relation.setProductId(productId);
			relation.setPlanId(planId);
			relation.setUpdateTime(new Date());
			relation.setOptimizerSystemAccount(optimizerSystemAccount);
			result = this.addPlanRelation(relation);
		}
		if(result > 0) {
			// 更新计划表系统类型os_type
			if(companyId != 1 && companyId != 2) {// 只有tracking接入的数据才有osType，CP数据没有
				planService.modifyOsType(planId, osType);
			}
			// 更新（按天）计划统计表
			planStatService.modifyDayStat(companyId, planId, pageId, pkgId, osType, statDate);
			// 更新（实时）计划统计表
			planStatService.modifyRealTimeStat(companyId, planId, pageId, pkgId, osType, statDate);
			
			//包的关联规则处理，（尾量数据）
			if(pkgId > 0){
				PkgRelation oldPkgRelation = associationRuleMapper.getPkgRelation(pkgId);
				if(oldPkgRelation != null){
					oldPkgRelation.setPlanId(planId);
					oldPkgRelation.setCompanyId(companyId);
					oldPkgRelation.setOsType(osType);
					oldPkgRelation.setProductId(productId);
					oldPkgRelation.setUpdateTime(new Date());
					associationRuleMapper.updatePkgRelation(oldPkgRelation);
				}else{
					PkgRelation pkgRelation = new PkgRelation();
					pkgRelation.setPkgId(pkgId);
					pkgRelation.setPlanId(planId);
					pkgRelation.setCompanyId(companyId);
					pkgRelation.setOsType(osType);
					pkgRelation.setProductId(productId);
					pkgRelation.setCreateTime(new Date());
					pkgRelation.setUpdateTime(new Date());
					associationRuleMapper.addPkgRelation(pkgRelation);
				}
			}
		}
		return result;
	}
	
	/**
	 * 新增关联规则
	 * @param relation
	 * @return 
	 * @author luzhuyou 2017/03/06
	 */
	public int addPlanRelation(PlanRelation relation) {
		relation.setCreateTime(new Date());
		return associationRuleMapper.addPlanRelation(relation);
	}

	/**
	 * 查询历史关联规则信息列表
	 * @param planId 计划ID
	 * @param unboundPlan 0-所有计划的关联规则信息，1-未绑定关联规则的计划
	 * @return
	 */
	public List<AssociationRuleVo> queryHistory(int planId, int unboundPlan) {
		AssociationRuleVo associationRuleVo = new AssociationRuleVo();
		associationRuleVo.setPlanId(planId);
		associationRuleVo.setUnboundPlan(unboundPlan);
		List<AssociationRuleVo> result = associationRuleMapper.queryHistoryAssociationRule(associationRuleVo);
		return result;
	}

	/**
	 * 修改历史关联规则
	 * @param hisRule 历史关联规则
	 * @param updateStatus 更新状态 ： 0-初始提交更新，1-强制提交更新
	 * @return
	 */
	public void modifyHistory(AssociationRuleHistoryVo hisRule, int updateStatus) {
		// 1.查询落地页和包在待更新日期范围内被绑定过的按天计划统计表数据
		List<PlanStatHisVo> planStatHisVoList = planStatService.queryHisPossibleConflictRelation(hisRule);
		
		// 2.将查询结果封装为已绑定日期包和落地页的Map对象：Map<date, Map<pkgId, PlanStat>>, Map<date, Map<pkgId, PlanStat>>
		Map<String, Map<Integer, PlanStatHisVo>> datePkgPlanStatMap = new HashMap<String, Map<Integer, PlanStatHisVo>>();
		Map<String, Map<Integer, PlanStatHisVo>> datePagePlanStatMap = new HashMap<String, Map<Integer, PlanStatHisVo>>();
		
		if(planStatHisVoList != null && !planStatHisVoList.isEmpty()) {
			for(PlanStatHisVo planStatHisVo : planStatHisVoList) {
				String statDate = planStatHisVo.getStatDate();
				// 生成pkgPlanStatMap
				Map<Integer, PlanStatHisVo> pkgPlanStatMap = datePkgPlanStatMap.get(statDate);
				if(pkgPlanStatMap == null) {
					pkgPlanStatMap = new HashMap<Integer, PlanStatHisVo>();
					datePkgPlanStatMap.put(statDate, pkgPlanStatMap);
				}
				pkgPlanStatMap.put(planStatHisVo.getPkgId(), planStatHisVo);
				
				// 生成pagePlanStatMap
				Map<Integer, PlanStatHisVo> pagePlanStatMap = datePagePlanStatMap.get(statDate);
				if(pagePlanStatMap == null) {
					pagePlanStatMap = new HashMap<Integer, PlanStatHisVo>();
					datePagePlanStatMap.put(statDate, pagePlanStatMap);
				}
				pagePlanStatMap.put(planStatHisVo.getPageId(), planStatHisVo);
			}
		}
		// 3.循环当前待更新计划的落地页和包师傅在已绑定日期Map对象中已存在，如果存在，则记录到页面响应结果数组，如果否，则记录到待更新数组
		List<String> statDates = new ArrayList<String>();
		List<String> conflictStatDates = new ArrayList<String>();
		Set<PlanStatHisVo> conflictPkgRelationSet = new HashSet<PlanStatHisVo>();
		Set<PlanStatHisVo> conflictPageRelationSet = new HashSet<PlanStatHisVo>();
		StringBuilder conflictTips = new StringBuilder("");
		
		for(String statDate : hisRule.getStatDates()) {
			boolean pageConflictFlag = false;
			boolean pkgConflictFlag = false; 
			PlanStatHisVo pageConflict = null;
			PlanStatHisVo pkgConflict = null;
			int pageId = hisRule.getPageId();
			Map<Integer, PlanStatHisVo> pagePlanStatMap = datePagePlanStatMap.get(statDate);
			// 落地页pageId已被绑定
			if(pagePlanStatMap != null) {
				if(pagePlanStatMap.keySet().contains(pageId)) {
					PlanStatHisVo tmp = pagePlanStatMap.get(pageId);
					conflictPageRelationSet.add(tmp);
					logger.error("落地页 [{}] 已被 [{}] 计划在 [{}] 绑定.", new Object[]{tmp.getPageName(), tmp.getPlanName(), statDate});
					pageConflictFlag = true;
					pageConflict = tmp;
				}
			}

			int pkgId = hisRule.getPkgId();
			Map<Integer, PlanStatHisVo> pkgPlanStatMap = datePkgPlanStatMap.get(statDate);
			// 包pkgId已被绑定
			if(pkgPlanStatMap != null) {
				if(pkgPlanStatMap.keySet().contains(pkgId)) {
					PlanStatHisVo tmp = pkgPlanStatMap.get(pkgId);
					conflictPkgRelationSet.add(tmp);
					logger.error("包 [{}] 已被 [{}] 计划在 [{}] 绑定.", new Object[]{tmp.getPkgName(), tmp.getPlanName(), statDate});
					pkgConflictFlag = true;
					pkgConflict = tmp;
				}
			}
			
			if(pageConflictFlag && pkgConflictFlag) {
				conflictStatDates.add(statDate);
				conflictTips.append("落地页和包在已被计划 [" + pageConflict.getPlanName() + "] 在[" + statDate + "]绑定.");
			} else if(pageConflictFlag && !pkgConflictFlag) {
				conflictStatDates.add(statDate);
				conflictTips.append("落地页已被计划 [" + pageConflict.getPlanName() + "] 在[" + statDate + "]绑定.");
			}else if(!pageConflictFlag && pkgConflictFlag) {
				conflictStatDates.add(statDate);
				conflictTips.append("包已被计划 [" + pkgConflict.getPlanName() + "] 在[" + statDate + "]绑定.");
			}else{
				statDates.add(statDate);
			}
		}
		
 		if(StringUtils.isNotBlank(conflictTips.toString())) {
 			int noPageId = 0;
			int noPkgId = 0; 
 			// 0-初始提交更新
			if(updateStatus != 1) {
				// 抛出异常提示包/落地页已被xx计划在xx日期绑定
				ExceptionHandler.throwGeneralServerException(StatusCode.ASSO_CONFLICT, new Object[]{conflictTips.toString()});
			}
			
			// 1-强制提交更新：强制提交关联规则逻辑，先强制解绑旧的关联规则，再更新新的关联规则
			logger.info("强制解绑存在冲突的落地页和包的历史关联关系.");
			// 解绑冲突的落地页的历史关联关系
			for(PlanStatHisVo pageConfictRecord : conflictPageRelationSet) {
				// 更新（按天）计划统计表
				planStatService.modifyDayStat(pageConfictRecord.getCompanyId(), pageConfictRecord.getPlanId(), noPageId, pageConfictRecord.getPkgId(), null, pageConfictRecord.getStatDate());
				// 更新（实时）计划统计表
				planStatService.modifyRealTimeStat(pageConfictRecord.getCompanyId(), pageConfictRecord.getPlanId(), noPageId, pageConfictRecord.getPkgId(), null, pageConfictRecord.getStatDate());
				
			}
			// 解绑冲突的包的历史关联关系
			for(PlanStatHisVo pkgConfictRecord : conflictPkgRelationSet) {
				// 更新（按天）计划统计表
				planStatService.modifyDayStat(pkgConfictRecord.getCompanyId(), pkgConfictRecord.getPlanId(), pkgConfictRecord.getPageId(), noPkgId, null, pkgConfictRecord.getStatDate());
				// 更新（实时）计划统计表
				planStatService.modifyRealTimeStat(pkgConfictRecord.getCompanyId(), pkgConfictRecord.getPlanId(), pkgConfictRecord.getPageId(), noPkgId, null, pkgConfictRecord.getStatDate());
			}
 		}

 		logger.info("更新历史关联规则：计划[{}],包[{}],落地页[{}].", new Object[]{hisRule.getPlanId(), hisRule.getPkgId(), hisRule.getPageId()});
 		for(String statDate : hisRule.getStatDates()) {
 			logger.info("更新日期为 [{}] 的历史关联规则.", new Object[]{hisRule.getStatDates()});
 			// 更新（按天）计划统计表
 			planStatService.modifyDayStat(hisRule.getCompanyId(), hisRule.getPlanId(), hisRule.getPageId(), hisRule.getPkgId(), null, statDate);
 			// 更新（实时）计划统计表
 			planStatService.modifyRealTimeStat(hisRule.getCompanyId(), hisRule.getPlanId(), hisRule.getPageId(), hisRule.getPkgId(), null, statDate);
 		}
		
	}
	
	public List<Integer> queryPlanIdsByAccountIds(Integer[] systemAccountAccountIds, Integer companyId) {
		AssociationRuleVo associationRuleVo = new AssociationRuleVo();
		associationRuleVo.setCompanyId(companyId);
		associationRuleVo.setPermissionSystemAccountIds(systemAccountAccountIds);
		return associationRuleMapper.queryPlanIdsByAccountIds(associationRuleVo);
	}
}
