package com.dataeye.ad.assistor.module.debugcenter.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * @author lingliqi
 * @date 2018-01-18 11:10
 */
public class KeyValue<K, V> implements Serializable {
    @Expose
    private K key;
    @Expose
    private V value;

    public KeyValue() {
    }

    public KeyValue(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public K getKey() {
        return key;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public V getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "(" + key + "," + value + ")";
    }
}
