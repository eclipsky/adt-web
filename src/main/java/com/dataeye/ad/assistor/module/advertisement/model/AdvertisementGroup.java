package com.dataeye.ad.assistor.module.advertisement.model;

import java.math.BigDecimal;

import com.google.gson.annotations.Expose;


/**
 * 广告组
 * Created by ldj
 */
public class AdvertisementGroup {

	/**广告组id**/
	@Expose
    private Integer adGroupId;
	/**广告组名称**/
	@Expose
    private String adGroupName;
	
	/**计费类型**/
	@Expose
    private Integer costType;
	/**广告出价，单位为元**/
	@Expose
    private BigDecimal bid;
	/**开始投放日期**/
	@Expose
    private String beginDate;
	/**结束投放日期**/
	@Expose
    private String endDate;
	
	/**标的物 id，**/
	@Expose
    private String productRefsId;
	/**定向 id**/
	@Expose
    private Integer targetingId;
	/**客户设置的状态*/
	@Expose
    private Integer status;
	/**标的物类型*/
	@Expose
	private Integer productType;
	/**系统状态*/
	@Expose
	private Integer systemStatus;
	/**审核消息*/
	@Expose
	private String rejectMessage;
	//创意规格 id，
	@Expose
    private Integer adcreativeTemplateId;
	/**站点集合  */
	@Expose
	private String siteSet;
	/**
	 * 投放时间段，格式为 48 * 7 位字符串，且都为 0 和 1，
	 *  0 为不投放， 1 为投放
	 * */
	@Expose
	private String timeSeries;
    
	/** 公司ID */
	private Integer companyId;
	/** ADT账号ID */
	private Integer systemAccountId;
	
	public Integer getAdGroupId() {
		return adGroupId;
	}
	public void setAdGroupId(Integer adGroupId) {
		this.adGroupId = adGroupId;
	}
	public String getAdGroupName() {
		return adGroupName;
	}
	public void setAdGroupName(String adGroupName) {
		this.adGroupName = adGroupName;
	}
	public Integer getCostType() {
		return costType;
	}
	public void setCostType(Integer costType) {
		this.costType = costType;
	}
	public BigDecimal getBid() {
		return bid;
	}
	public void setBid(BigDecimal bid) {
		this.bid = bid;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getProductRefsId() {
		return productRefsId;
	}
	public void setProductRefsId(String productRefsId) {
		this.productRefsId = productRefsId;
	}
	public Integer getTargetingId() {
		return targetingId;
	}
	public void setTargetingId(Integer targetingId) {
		this.targetingId = targetingId;
	}
	public Integer getProductType() {
		return productType;
	}
	public void setProductType(Integer productType) {
		this.productType = productType;
	}
	public Integer getSystemStatus() {
		return systemStatus;
	}
	public void setSystemStatus(Integer systemStatus) {
		this.systemStatus = systemStatus;
	}
	public String getRejectMessage() {
		return rejectMessage;
	}
	public void setRejectMessage(String rejectMessage) {
		this.rejectMessage = rejectMessage;
	}
	public String getSiteSet() {
		return siteSet;
	}
	public void setSiteSet(String siteSet) {
		this.siteSet = siteSet;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getTimeSeries() {
		return timeSeries;
	}
	public void setTimeSeries(String timeSeries) {
		this.timeSeries = timeSeries;
	}
	
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getSystemAccountId() {
		return systemAccountId;
	}
	public void setSystemAccountId(Integer systemAccountId) {
		this.systemAccountId = systemAccountId;
	}
	
	public Integer getAdcreativeTemplateId() {
		return adcreativeTemplateId;
	}
	public void setAdcreativeTemplateId(Integer adcreativeTemplateId) {
		this.adcreativeTemplateId = adcreativeTemplateId;
	}
	@Override
	public String toString() {
		return "AdvertisementGroup [adGroupId=" + adGroupId + ", adGroupName="
				+ adGroupName + ", costType=" + costType + ", bid=" + bid
				+ ", beginDate=" + beginDate + ", endDate=" + endDate
				+ ", productRefsId=" + productRefsId + ", targetingId="
				+ targetingId + ", status=" + status + ", productType="
				+ productType + ", systemStatus=" + systemStatus
				+ ", rejectMessage=" + rejectMessage + ", siteSet=" + siteSet
				+ ", timeSeries=" + timeSeries + ", companyId=" + companyId
				+ ", systemAccountId=" + systemAccountId
				+ ", adcreativeTemplateId=" + adcreativeTemplateId + "]";
	}

}
