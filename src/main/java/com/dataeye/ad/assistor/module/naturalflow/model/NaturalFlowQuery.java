package com.dataeye.ad.assistor.module.naturalflow.model;

import com.dataeye.ad.assistor.module.report.model.DateRangeQuery;

import java.util.Date;
import java.util.List;

public class NaturalFlowQuery extends DateRangeQuery {
    /**
     * 产品ID.
     */
    private Integer productId;

    /**
     * 产品ID列表，供回本日报，多选产品时使用
     */
    private List<Integer> productIds;

    /**
     * 日期，供回本日报，回本趋势图使用。
     */
    private Date date;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "NaturalFlowQuery{" +
                "productId=" + productId +
                ", productIds=" + productIds +
                ", date=" + date +
                "} " + super.toString();
    }
}
