package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.common.DailyRecord;
import com.dataeye.ad.assistor.common.TrendChart;
import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.module.mediumaccount.model.Medium;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountSelectorVo;
import com.dataeye.ad.assistor.module.realtimedelivery.mapper.BalanceAlertMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.mapper.BalanceMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.model.*;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.dataeye.ad.assistor.util.TrendChartUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by huangzehai on 2017/4/19.
 */
@Service
public class BalanceServiceImpl implements BalanceService {

    @Autowired
    private BalanceMapper balanceMapper;
    
    @Autowired
    private BalanceAlertMapper balanceAlertMapper;

    @Override
    public List<Balance> listAccountBalance(BalanceQuery balanceQuery) {
        List<Balance> balances = balanceMapper.listAccountBalance(balanceQuery);
        List<Cost> costs = balanceMapper.listAccountAverageCost(balanceQuery);
        Map<Integer, Cost> costByAccount = listToMap(costs);
        int companyId=0;
        if(balanceQuery!=null && balanceQuery.getCompanyId()!=null){
        	companyId = balanceQuery.getCompanyId();
        }
        formatCurrencyAndSettingWarning(balances, costByAccount,companyId);
        balances.add(totalBalance(balances));
        return balances;
    }

    @Override
    public TrendChart<String, BigDecimal> balanceTrend(BalanceTrendQuery balanceTrendQuery) {
        List<DailyRecord<String, BigDecimal>> balances;
        if (balanceTrendQuery.getMediumAccountIds() == null || balanceTrendQuery.getMediumAccountIds().isEmpty()) {
            balances = balanceMapper.totalBalanceTrend(balanceTrendQuery);
            if (balances != null) {
                for (DailyRecord<String, BigDecimal> balance : balances) {
                    balance.setName(Labels.TOTAL);
                }
            }
        } else {
            balances = balanceMapper.balanceTrend(balanceTrendQuery);
        }

        for (DailyRecord<String, BigDecimal> balance : balances) {
            balance.setValue(CurrencyUtils.fenToYuan(balance.getValue()));
        }
        return TrendChartUtils.toTrendChart(balanceTrendQuery.getStartDate(), balanceTrendQuery.getEndDate(), balances, new BigDecimal(0));
    }

    /**
     * 获取负责人管理的媒体账户列表
     *
     * @param mediumAccountQuery
     * @return
     */
    @Override
    public List<MediumAccountSelectorVo> listManagingMediumAccounts(MediumAccountQuery mediumAccountQuery) {
        return balanceMapper.listManagingMediumAccounts(mediumAccountQuery);
    }

    @Override
    public List<Medium> listManagingMediums(UserInSession userInSession) {
        return balanceMapper.listManagingMediums(userInSession);
    }

    private Map<Integer, Cost> listToMap(List<Cost> costs) {
        Map<Integer, Cost> costByAccount = new HashMap<>();
        for (Cost cost : costs) {
            costByAccount.put(cost.getMediumAccountId(), cost);
        }
        return costByAccount;
    }

    /**
     * 计算余额汇总
     *
     * @param balances
     * @return
     */
    private Balance totalBalance(List<Balance> balances) {
        Balance totalBalance = new Balance();
        totalBalance.setMediumName(Labels.TOTAL);
        totalBalance.setMediumAccount(Labels.UNKNOWN);
        if (balances != null) {
            BigDecimal total = new BigDecimal(0);
            BigDecimal totalYesterdayBalance = new BigDecimal(0);
            for (Balance balance : balances) {
                total = total.add(balance.getBalance());
                totalYesterdayBalance = totalYesterdayBalance.add(balance.getYesterdayBalance());
            }
            totalBalance.setBalance(total);
            totalBalance.setYesterdayBalance(totalYesterdayBalance);
        }
        return totalBalance;
    }

    /**
     * 转换余额单位并且设置告警.
     *
     * @param balances
     * @param costByAccount
     * @param balanceRedNum 媒体账号余额标红逻辑的默认值，默认为5
     */
    private void formatCurrencyAndSettingWarning(List<Balance> balances, Map<Integer, Cost> costByAccount,int companyId) {
    	BigDecimal balanceRedNum = new BigDecimal(5);
    	
        //查找出该公司的配置
    	 Map<Integer, BigDecimal> balanceRedNumMap = new HashMap<>();
    	List<BalanceAlertConf> alertConfList = balanceAlertMapper.getBalanceAlertConfByCompanyId(companyId);
    	for (BalanceAlertConf confVo : alertConfList) {
    		if(!balanceRedNumMap.containsKey(confVo.getCompanyId())){
    			balanceRedNumMap.put(confVo.getCompanyId(), new BigDecimal(confVo.getBalanceRedNum()));
    		}
		}
    	if(companyId!=0 && balanceRedNumMap.get(companyId)!=null){
    		balanceRedNum = balanceRedNumMap.get(companyId);
    	}
        
        for (Balance balance : balances) {
        	if(companyId==0 && balance.getCompanyId()!=null && balanceRedNumMap.get(balance.getCompanyId())!=null){
        		 balanceRedNum = balanceRedNumMap.get(balance.getCompanyId());
        	}
            //当 实时媒体账户余额 <= 过去7日日均媒体账户消耗 * 3 时,设置告警
            Cost avgCost = costByAccount.get(balance.getMediumAccountId());
            if (balance.getBalance() != null && avgCost != null && avgCost.getCost().compareTo(new BigDecimal(0)) > 0 && balance.getBalance().compareTo(avgCost.getCost().multiply(balanceRedNum)) <= 0) {
                balance.setWarning(true);
            }

            //转换货币单位
            balance.setBalance(CurrencyUtils.fenToYuan(balance.getBalance()));
            balance.setYesterdayBalance(CurrencyUtils.fenToYuan(balance.getYesterdayBalance()));
        }
    }
}
