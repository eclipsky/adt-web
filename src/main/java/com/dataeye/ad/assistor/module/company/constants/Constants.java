package com.dataeye.ad.assistor.module.company.constants;

/**
 * 系统账号管理模块常量表
 * @author luzhuyou 2017/02/17
 */
public class Constants {

	/**
	 * 公司状态
	 * 
	 * @author luzhuyou 2017/05/10
	 */
	public static class CompanyStatus {
		// 状态：0-正常，1-待商务审核，2-暂停，3-冻结，4-待运营审核，5-冻结，6-停用
		/** 0-正常 */
		public static final int NORMAL = 0;
		/** 1-待商务审核  */
		public static final int BIZ_AUDIT = 1;
		/** 2-暂停  */
		public static final int PAUSE = 2;
		/** 3-拒绝  */
		public static final int REFUSE = 3;
		/** 4-待运营审核  */
		public static final int OPE_AUDIT = 4;
		/** 5-冻结  */
		public static final int FROZEN = 5;
		/** 6-停用  */
		public static final int DISABLE = 6;

		/** 
		 * 判断是否公司状态
		 * @param status 状态：0-正常，1-待商务审核，2-暂停，3-拒绝，4-待运营审核，5-冻结，6-停用
		 * @return
		 */
		public static boolean isUserStatus(int status) {
			if(status == NORMAL || status == BIZ_AUDIT || status == PAUSE ||status == REFUSE || status == OPE_AUDIT || status == FROZEN || status == DISABLE) {
				return true;
			}
			return false;
		}
		
	}
	
	/**
	 * 付费类型
	 * 
	 * @author luzhuyou 2017/05/10
	 */
	public static class PayType {
		/** 0-试用 */
		public static final int TRIAL = 0;
		/** 1-付费  */
		public static final int PAY = 1;
		/** 
		 * 判断是否付费类型
		 * @param payType
		 * @return
		 */
		public static boolean isAccountRole(int payType) {
			if(payType == TRIAL || payType == PAY) {
				return true;
			}
			return false;
		}
	}
}
