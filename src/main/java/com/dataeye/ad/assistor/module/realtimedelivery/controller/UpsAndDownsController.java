package com.dataeye.ad.assistor.module.realtimedelivery.controller;

import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.realtimedelivery.service.UpsAndDownsPreferenceService;
import com.dataeye.ad.assistor.module.report.model.IndicatorPreferenceQuery;
import com.dataeye.ad.assistor.module.report.model.UpsAndDownsPreference;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 指标涨跌控制器
 * Created by huangzehai on 2017/7/6.
 */
@Controller
public class UpsAndDownsController {

    @Autowired
    private UpsAndDownsPreferenceService upsAndDownsPreferenceService;

    /**
     * 获取登录账号的指标涨跌显示偏好
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/real-time/ups-and-downs-preference.do")
    public Object getRealTimeIndicatorPreference(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        IndicatorPreferenceQuery query = new IndicatorPreferenceQuery();
        query.setAccountId(userInSession.getAccountId());
        query.setAccountRole(userInSession.getAccountRole());
        query.setCompanyId(userInSession.getCompanyId());
        return upsAndDownsPreferenceService.getUpsAndDownsPreference(query);
    }

    /**
     * 更新指标涨跌显示偏好
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/real-time/ups-and-downs-preference/update.do")
    public Object updateRealTimeIndicatorPreference(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        DEParameter parameter = context.getDeParameter();
        String indicatorVisibility = parameter.getParameter(DEParameter.Keys.INDICATOR_VISIBILITY);
        String minutes = parameter.getParameter(DEParameter.Keys.MINUTES);
        if (StringUtils.isBlank(indicatorVisibility)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_INDICATOR_VISIBILITY_MISSING);
        }
        if (StringUtils.isBlank(minutes)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_MINUTES_MISSING);
        }

        UpsAndDownsPreference preference = new UpsAndDownsPreference();
        preference.setMinutes(Integer.valueOf(minutes));
        preference.setIndicatorVisibility(indicatorVisibility);
        preference.setCompanyId(userInSession.getCompanyId());
        preference.setAccountId(userInSession.getAccountId());
        return upsAndDownsPreferenceService.updateUpsAndDownsPreference(preference);
    }
}
