package com.dataeye.ad.assistor.module.result.model;

import org.apache.commons.lang.StringUtils;

/**
 * 对比分析指标.
 * Created by huangzehai on 2017/4/11.
 */
public enum ContrastiveAnalysisIndicator {
    Cost, CTR, DownloadRate, CPA, SevenDayPaybackRate,SevenDayRetentionRate;

    public static ContrastiveAnalysisIndicator parse(String text) {
        for (ContrastiveAnalysisIndicator indicator : ContrastiveAnalysisIndicator.values()) {
            if (StringUtils.equalsIgnoreCase(indicator.name(), text)) {
                return indicator;
            }
        }
        return null;
    }
}
