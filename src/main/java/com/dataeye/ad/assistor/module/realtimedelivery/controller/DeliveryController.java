package com.dataeye.ad.assistor.module.realtimedelivery.controller;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.common.Item;
import com.dataeye.ad.assistor.constant.Constant.MediumType;
import com.dataeye.ad.assistor.constant.Constant.Separator;
import com.dataeye.ad.assistor.constant.Labels;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.*;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlowQuery;
import com.dataeye.ad.assistor.module.naturalflow.service.NaturalFlowService;
import com.dataeye.ad.assistor.module.realtimedelivery.model.*;
import com.dataeye.ad.assistor.module.realtimedelivery.service.*;
import com.dataeye.ad.assistor.module.report.constant.Indicators;
import com.dataeye.ad.assistor.module.report.model.IndicatorPreferenceQuery;
import com.dataeye.ad.assistor.module.report.model.UpsAndDownsPreference;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountService;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.NumberUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.dataeye.ad.assistor.util.UpsAndDownsUtils;
import com.google.gson.Gson;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.*;

/**
 * 实时投放控制器
 *
 * @author luzhuyou 2016/01/02
 */
@Controller
public class DeliveryController {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(DeliveryController.class);

    /**
     * 企业账号
     */
    private static final int BUSINESS_ACCOUNT = 2;

    /**
     * 成功保存告警配置消息.
     */
    private static final String SUCCESS_SAVE_ALERT_CONF = "Successfully save alert configuration";

    /**
     * 仅该计划负责人才有操作权限.
     */
    private static final int NOT_PLAN_MANAGER = 1;

    /**
     * 该媒体暂未开通此功能.
     */
    private static final int MEDIUM_UNSUPPORTED = 2;

    /**
     * 十分钟
     */
    private static final int TEN_MINUTES = 10;

    @Autowired
    private DeliveryService deliveryService;

    @Autowired
    private RealTimeTrendService realTimeTrendService;

    @Autowired
    private AlertService alertService;

    @Autowired
    private SystemAccountService systemAccountService;

    @Autowired
    private RealTimeReportService realTimeReportService;

    @Autowired
    private UpsAndDownsPreferenceService upsAndDownsPreferenceService;

    @Autowired
    private NaturalFlowService naturalFlowService;

    /**
     * 实时投放数据查询
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/realtimedelivery/queryDelivery.do")
    public Object queryDelivery(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.debug("Query Realtime Delivery Data![/ad/realtimedelivery/queryDelivery.do]");
        // 1. 参数解析与校验
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String mediumId = parameter.getParameter("mediumID");
        if (StringUtils.isBlank(mediumId)) {
            mediumId = null;
        }

        String accountId = parameter.getParameter("accountID");
        if (StringUtils.isBlank(accountId)) {
            accountId = null;
        }
        String planIdsText = parameter.getParameter("planIds");
        List<Integer> planIds = StringUtil.stringToList(planIdsText);

        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (permissionMediumAccountIds == null) {
            return null;
        }
        String[] permissionSystemAccounts = userInSession.getPermissionSystemAccounts();

        String productIdsText = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        List<Integer> productIds = StringUtil.stringToList(productIdsText);
        // 涨跌幅对比分钟数
        String minutesText = parameter.getParameter(DEParameter.Keys.MINUTES);
        //假设为涨跌对比的是前10分钟.
        String ignoreNaturalFlow = parameter.getParameter(DEParameter.Keys.FILTER_NATURAL_FLOW);

        int minutes = TEN_MINUTES;
        if (StringUtils.isNotBlank(minutesText) && StringUtils.isNumeric(minutesText)) {
            minutes = Integer.valueOf(minutesText);
        }

        if (StringUtils.isNotBlank(minutesText) && StringUtils.isNumeric(minutesText)) {
            minutes = Integer.valueOf(minutesText);
        } else {
            //如果前端不上传对比分钟数,查看用户的涨跌显示偏好
            IndicatorPreferenceQuery query = new IndicatorPreferenceQuery();
            query.setAccountId(userInSession.getAccountId());
            query.setAccountRole(userInSession.getAccountRole());
            query.setCompanyId(userInSession.getCompanyId());
            UpsAndDownsPreference upsAndDownsPreference = upsAndDownsPreferenceService.getUpsAndDownsPreference(query);
            if (upsAndDownsPreference != null) {
                minutes = upsAndDownsPreference.getMinutes();
            }
        }

        // 2. 查询实时投放数据
            List<RealTimeDeliveryVo> realTimeDeliveryList = deliveryService.queryDelivery(companyId, mediumId, accountId, DateUtils.currentDate(), permissionMediumAccountIds, productIds, parameter.isIgnoreIsNullCostPlan(), planIds);
        if (null == realTimeDeliveryList) {
            return null;
        }

        //查询N分钟前的实时指标
        RealTimeReportQuery query = new RealTimeReportQuery();
        query.setCompanyId(companyId);
        query.setPermissionMediumAccountIds(permissionMediumAccountIds);
        query.setStartTime(DateUtils.getTimeBeforeMinutes(minutes));
        Map<Integer, RealTimeReport> previousIndicators = realTimeReportService.getIndicatorAtTime(query);

        //获取登录账号信息
        SystemAccountVo systemAccount = systemAccountService.get(userInSession.getAccountId());
        //计算涨跌幅
        UpsAndDownsUtils.computeUpsAndDowns(realTimeDeliveryList, previousIndicators, systemAccount.getIndicatorPermission());
        //添加自然流量
        int offset = 0;
        if (!Boolean.valueOf(ignoreNaturalFlow)) {
            NaturalFlowQuery naturalFlowQuery = new NaturalFlowQuery();
            naturalFlowQuery.setCompanyId(userInSession.getCompanyId());
            naturalFlowQuery.setPermissionMediumAccountIds(permissionMediumAccountIds);
            naturalFlowQuery.setPermissionSystemAccounts(permissionSystemAccounts);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            Date today = calendar.getTime();
            naturalFlowQuery.setStartDate(today);
            naturalFlowQuery.setEndDate(today);
            naturalFlowQuery.setProductIds(productIds);
            List<NaturalFlow> naturalFlows = naturalFlowService.getNaturalFlowsGroupByProductAndDate(naturalFlowQuery);
            if (naturalFlows != null) {
                offset = naturalFlows.size();
                addNaturalFlow(realTimeDeliveryList, naturalFlows);
            }
        }

        // 3. 数据格式转换为前端展示格式并累加实时统计数据
        //获取负责人或组长管理的媒体账号ID.
        Integer[] optimizerPermissionMediumAccountIds = userInSession.getOptimizerPermissionMediumAccountIds();
        List<Integer> managingMediumAccountIds;
        if (optimizerPermissionMediumAccountIds == null) {
            managingMediumAccountIds = Collections.EMPTY_LIST;
        } else {
            managingMediumAccountIds = Arrays.asList(optimizerPermissionMediumAccountIds);
        }


        List<RealTimeDeliveryFormatVo> formatResultList = new ArrayList<>();
        RealTimeDeliveryVo sumVo = new RealTimeDeliveryVo();
        for (RealTimeDeliveryVo vo : realTimeDeliveryList) {
            // 3.1 数据格式转换为前端展示格式
            RealTimeDeliveryFormatVo view = new RealTimeDeliveryFormatVo(vo);
            //隐藏未授权指标
            hideUnauthorizedIndicator(view, systemAccount.getIndicatorPermission());
            //判断当前用户是否可以修改出价
            view.setBidAvailable(managingMediumAccountIds.contains(vo.getMediumAccountId()) && BidServices.isBidAvailable(vo.getMediumId(), vo.getBidStrategy()));
            if (!view.isBidAvailable()) {
                //出价不可用具体原因
                if (!BidServices.isBidAvailable(vo.getMediumId(), vo.getBidStrategy())) {
                    view.setBidMessageCode(MEDIUM_UNSUPPORTED);
                } else if (!managingMediumAccountIds.contains(vo.getMediumAccountId())) {
                    view.setBidMessageCode(NOT_PLAN_MANAGER);
                }
            }
            //判断用户是否可以切换计划开关
            view.setPlanSwitchAvailable(managingMediumAccountIds.contains(vo.getMediumAccountId()) && PlanSwitchServices.isPlanSwitchAvailable(vo.getMediumId()));
            if (!view.isPlanSwitchAvailable()) {
                //开关不可用具体原因
                if (!PlanSwitchServices.isPlanSwitchAvailable(vo.getMediumId())) {
                    view.setSwitchMessageCode(MEDIUM_UNSUPPORTED);
                } else if (!managingMediumAccountIds.contains(vo.getMediumAccountId())) {
                    view.setSwitchMessageCode(NOT_PLAN_MANAGER);
                }
            }
            //判断用户是否是计划负责人
            view.setManager((view.getMediumType().intValue() != 0 && view.getMediumType().intValue() == MediumType.ADT) && (BUSINESS_ACCOUNT == userInSession.getAccountRole() || managingMediumAccountIds.contains(vo.getMediumAccountId())));
            //判断用户是否可以编辑媒体数据
            if ((view.getMediumType().intValue() == MediumType.TRACKING_COMMON || view.getMediumType().intValue() == MediumType.CUSTOM) && managingMediumAccountIds.contains(view.getMediumAccountId())) {
                view.setEditable(true);
            }

            formatResultList.add(view);
            // 3.2 累加实时统计数据 (消耗、曝光、点击数、下载数、激活数、首日注册设备数、首日注册账号数)
            sumRealTimeDelivery(vo, sumVo);
        }
        // 4. 计算CTR、CPA汇总 结果值
        RealTimeDeliveryFormatVo countResultVo = calcSumResult(sumVo);
        //隐藏未授权指标
        hideUnauthorizedIndicator(countResultVo, systemAccount.getIndicatorPermission());
        //设置告警标志
        DataPermissionDomain dataPermissionDomain = new DataPermissionDomain();
        dataPermissionDomain.setCompanyId(companyId);
        dataPermissionDomain.setPermissionMediumAccountIds(permissionMediumAccountIds);
        alertService.setAlert(formatResultList, realTimeDeliveryList, dataPermissionDomain, offset);
        Map<String, Object> result = new HashMap<>();
        sort(formatResultList);

        result.put("detail", formatResultList);
        result.put("total", countResultVo);

        return result;
    }

    /**
     * 给计划排序
     *
     * @param formatResultList
     */
    private void sort(List<RealTimeDeliveryFormatVo> formatResultList) {
        //计划排序
        Collections.sort(formatResultList, new Comparator<RealTimeDeliveryFormatVo>() {
            @Override
            public int compare(RealTimeDeliveryFormatVo o1, RealTimeDeliveryFormatVo o2) {
                if (o1.getPlanId() < 0 && o2.getPlanId() < 0) {
                    //自然流量根据产品名称排序
                    return o1.getPlanName().compareTo(o2.getPlanName());
                } else if (o1.getPlanId() < 0) {
                    //自然流量置顶
                    return -1;
                } else if (o2.getPlanId() < 0) {
                    //自然流量置顶
                    return 1;
                } else {
                    //将告警计划置顶,根据告警标志位降序、消耗降序排列,自然流量置顶
                    if (o1.getPlanNameColor() == o2.getPlanNameColor()) {
                        if (!o1.getTotalCost().equals(Separator.BAR) && !o2.getTotalCost().equals(Separator.BAR)) {
                            return new BigDecimal(o2.getTotalCost()).compareTo(new BigDecimal(o1.getTotalCost()));
                        } else if (o1.getTotalCost().equals(Separator.BAR) && !o2.getTotalCost().equals(Separator.BAR)) {
                            return 1;
                        } else if (o2.getTotalCost().equals(Separator.BAR) && !o1.getTotalCost().equals(Separator.BAR)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    } else {
                        return o2.getPlanNameColor() - o1.getPlanNameColor();
                    }
                }
            }
        });
    }

    /**
     * 添加自然流量数据
     *
     * @param realTimeDeliveryList
     * @param naturalFlowList
     */
    private void addNaturalFlow(List<RealTimeDeliveryVo> realTimeDeliveryList, List<NaturalFlow> naturalFlowList) {
        if (naturalFlowList != null && !naturalFlowList.isEmpty()) {
            List<RealTimeDeliveryVo> naturalFlows = new ArrayList<>();
            int index = -1;
            for (NaturalFlow naturalFlow : naturalFlowList) {
                RealTimeDeliveryVo report = new RealTimeDeliveryVo();
                report.setPlanId(index);
                report.setProductName(naturalFlow.getProductName());
                report.setPlanName(Labels.NATURAL_FLOWS);
                report.setActivations(naturalFlow.getActivations());
                // 首日注册设备数
                report.setRegisterNumFd(naturalFlow.getFirstDayRegistrations());
                report.setRegisterAccountNumFd(naturalFlow.getFirstDayRegisterAccountNum());
                naturalFlows.add(report);
                index--;
            }
            realTimeDeliveryList.addAll(0, naturalFlows);
        }
    }

    /**
     * 隐藏未授权的指标
     *
     * @param realTimeDeliveryFormatVo
     * @param indicatorPermission
     */
    private void hideUnauthorizedIndicator(RealTimeDeliveryFormatVo realTimeDeliveryFormatVo, String indicatorPermission) {
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.COST)) {
            realTimeDeliveryFormatVo.setTotalCost(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.SHOW_NUM)) {
            realTimeDeliveryFormatVo.setExposures(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CLICK_NUM)) {
            realTimeDeliveryFormatVo.setClicks(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CTR)) {
            realTimeDeliveryFormatVo.setCtr(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.CPC)) {
            realTimeDeliveryFormatVo.setCpc(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REACH)) {
            realTimeDeliveryFormatVo.setReaches(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REACH_RATE)) {
            realTimeDeliveryFormatVo.setReachRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DOWNLOAD_TIMES)) {
            realTimeDeliveryFormatVo.setDownloadUV(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.DOWNLOAD_RATE)) {
            realTimeDeliveryFormatVo.setDownloadRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVE_NUM)) {
            realTimeDeliveryFormatVo.setActivations(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVE_RATE)) {
            realTimeDeliveryFormatVo.setActivationRate(null);
        }
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.ACTIVE_CPA)) {
            realTimeDeliveryFormatVo.setActivationCpa(null);
        }
        // 首日注册设备数
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTRATIONS_FD)) {
            realTimeDeliveryFormatVo.setFirstDayRegistrations(null);
        }
        // 首日注册账号数
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_ACCOUNT_NUM_FD)) {
            realTimeDeliveryFormatVo.setFirstDayRegisterAccountNum(null);
        }
        // 点击注册率
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_RATE)) {
            realTimeDeliveryFormatVo.setFirstDayRegistrationRate(null);
        }
        // 首日注册设备CPA
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_CPA)) {
            realTimeDeliveryFormatVo.setCostPerFirstDayRegistration(null);
        }
        // 首日注册账号CPA
        if (!StringUtils.containsIgnoreCase(indicatorPermission, Indicators.REGISTER_ACCOUNT_NUM_CPA)) {
            realTimeDeliveryFormatVo.setCostPerFirstDayRegisterAccountNum(null);
        }
    }

    /**
     * 累加实时统计数据
     * <br>消耗、曝光、点击数、下载数、激活数、注册数
     *
     * @param item
     */
    private void sumRealTimeDelivery(RealTimeDeliveryVo item, RealTimeDeliveryVo sum) {
        sum.setTotalCost(NumberUtils.add(sum.getTotalCost(), item.getTotalCost()));
        sum.setExposures(NumberUtils.add(sum.getExposures(), item.getExposures()));
        sum.setClicks(NumberUtils.add(sum.getClicks(), item.getClicks()));
        sum.setTotalClicks(NumberUtils.add(sum.getTotalClicks(), item.getTotalClicks()));
        sum.setReaches(NumberUtils.add(sum.getReaches(), item.getReaches()));
        sum.setRetentionTime(NumberUtils.add(sum.getRetentionTime(), item.getRetentionTime()));
        sum.setDownloadUV(NumberUtils.add(sum.getDownloadUV(), item.getDownloadUV()));
        sum.setActivations(NumberUtils.add(sum.getActivations(), item.getActivations()));
        sum.setRegisterNumFd(NumberUtils.add(sum.getRegisterNumFd(), item.getRegisterNumFd()));
        sum.setRegisterAccountNumFd(NumberUtils.add(sum.getRegisterAccountNumFd(), item.getRegisterAccountNumFd()));
    }


    /**
     * 计算CTR、CPA汇总 结果值
     *
     * @param sumVo
     * @return
     */
    private RealTimeDeliveryFormatVo calcSumResult(RealTimeDeliveryVo sumVo) {
        NumberFormat nf = NumberFormat.getPercentInstance();
        //保留两位小数
        nf.setMinimumFractionDigits(2);
        //四舍五入
        nf.setRoundingMode(RoundingMode.HALF_UP);
        RealTimeDeliveryFormatVo formatVo = new RealTimeDeliveryFormatVo();
        formatVo.setMedium(Labels.TOTAL);
        formatVo.setAccount(Separator.BAR);
        //将汇总行的计划ID设置为0,区别于其他计划
        formatVo.setPlanId(0);
        formatVo.setPlanName(Separator.BAR);
        Double totalCost = sumVo.getTotalCost();
        Integer clicks = sumVo.getClicks();
        Integer exposures = sumVo.getExposures();
        Integer firstDayRegistrations = sumVo.getRegisterNumFd();
        Integer firstDayRegisterAccountNum = sumVo.getRegisterAccountNumFd();
        formatVo.setTotalCost(totalCost == null ? Separator.BAR : (new BigDecimal(totalCost).setScale(2, RoundingMode.HALF_UP)) + "");
        formatVo.setExposures(exposures == null ? Separator.BAR : String.valueOf(exposures));
        formatVo.setClicks(clicks == null ? Separator.BAR : String.valueOf(clicks));
        formatVo.setTotalClicks(StringUtil.defaultIfNull(sumVo.getTotalClicks()));
        formatVo.setReaches(sumVo.getReaches() == null ? Separator.BAR : sumVo.getReaches().toString());

        // 计算CTR：点击数除以曝光数
        if (clicks != null && exposures != null && exposures != 0) {
            formatVo.setCtr(new BigDecimal(1.0 * clicks / exposures * 100).setScale(2, RoundingMode.HALF_UP) + "%");
        } else {
            formatVo.setCtr(Separator.BAR);
        }
        formatVo.setDownloadUV(sumVo.getDownloadUV() == null ? Separator.BAR : String.valueOf(sumVo.getDownloadUV()));
        // 计算下载率：下载数除以到达数
        if (sumVo.getDownloadUV() != null && sumVo.getReaches() != null && sumVo.getReaches() > 0) {
            formatVo.setDownloadRate(nf.format((double) sumVo.getDownloadUV() / sumVo.getReaches()));
        } else {
            formatVo.setDownloadRate(Separator.BAR);
        }
        formatVo.setActivations(sumVo.getActivations() == null ? Separator.BAR : String.valueOf(sumVo.getActivations()));
        // 首日注册设备数
        formatVo.setFirstDayRegistrations(firstDayRegistrations == null ? Separator.BAR : String.valueOf(firstDayRegistrations));
        // 首日注册账号数
        formatVo.setFirstDayRegisterAccountNum(firstDayRegisterAccountNum == null ? Separator.BAR : String.valueOf(firstDayRegisterAccountNum));

        // 计算注册设备CPA：消耗值除以首日注册设备数
        if (totalCost != null && firstDayRegistrations != null && firstDayRegistrations > 0) {
            formatVo.setCostPerFirstDayRegistration(new BigDecimal(1.0 * totalCost / firstDayRegistrations).setScale(2, RoundingMode.HALF_UP) + "");
        } else {
            formatVo.setCostPerFirstDayRegistration(Separator.BAR);
        }
        // 计算注册账号CPA：消耗值除以首日注册账号数
        if (totalCost != null && firstDayRegisterAccountNum != null && firstDayRegisterAccountNum > 0) {
            formatVo.setCostPerFirstDayRegisterAccountNum(new BigDecimal(1.0 * totalCost / firstDayRegisterAccountNum).setScale(2, RoundingMode.HALF_UP) + "");
        } else {
            formatVo.setCostPerFirstDayRegisterAccountNum(Separator.BAR);
        }

        //停留时长
        formatVo.setRetentionTime(sumVo.getRetentionTime() == null ? Separator.BAR : sumVo.getRetentionTime().toString());

        //到达率
        if (sumVo.getReaches() != null && sumVo.getClicks() != null && sumVo.getClicks() > 0) {
            formatVo.setReachRate(nf.format((double) sumVo.getReaches() / sumVo.getClicks()));
        } else {
            formatVo.setReachRate(Separator.BAR);
        }
        //激活率
        if (sumVo.getActivations() != null && sumVo.getClicks() != null  && sumVo.getClicks() > 0) {
            formatVo.setActivationRate(nf.format((double) sumVo.getActivations() / sumVo.getClicks()));
        } else {
            formatVo.setActivationRate(Separator.BAR);
        }

        //注册率
        if (sumVo.getRegisterNumFd() != null && sumVo.getClicks() != null  && sumVo.getClicks() > 0) {
            formatVo.setFirstDayRegistrationRate(nf.format((double) sumVo.getRegisterNumFd() / sumVo.getClicks()));
        } else {
            formatVo.setFirstDayRegistrationRate(Separator.BAR);
        }

        //激活CPA
        if (sumVo.getTotalCost() != null && sumVo.getActivations() != null && sumVo.getActivations() > 0) {
            formatVo.setActivationCpa(new BigDecimal(sumVo.getTotalCost()).divide(new BigDecimal(sumVo.getActivations()), 2, RoundingMode.HALF_UP).toString());
        } else {
            formatVo.setActivationCpa(Separator.BAR);
        }

        //计算CPC
        if (sumVo.getTotalCost() != null && sumVo.getClicks() != null && sumVo.getClicks() > 0) {
            formatVo.setCpc(new BigDecimal(sumVo.getTotalCost()).divide(new BigDecimal(sumVo.getClicks()), 2, RoundingMode.HALF_UP).toString());
        } else {
            formatVo.setCpc(Separator.BAR);
        }

        return formatVo;
    }

    /**
     * 设置告警配置
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.Public, write = true)
    @RequestMapping("/ad/realtimedelivery/confAlert.do")
    public Object confAlert(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Conf Alert Data![/ad/realtimedelivery/confAlert.do]");
        //参数解析与校验
        DEContext context = (DEContext) request.getAttribute("CTX");
        DEParameter parameter = context.getDeParameter();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());

        // 如果为null，则表示无数据访问权限
        if (!userInSession.hasManagerPermission()) {
            logger.info("非当前媒体账号负责人，无权限配置告警");
            ExceptionHandler.throwPermissionException(StatusCode.COMM_NO_OPTIMIZER_PRIVILEGE);
        }
        Set<Integer> permissionIdsSet = new HashSet<>(Arrays.asList(userInSession.getOptimizerPermissionMediumAccountIds()));

        //持久化高级告警配置
        //批量修改时需要上传计划ID列表.
        String planIds = parameter.getParameter(DEParameter.Keys.PLAN_IDS);
        String alertConfsJson = parameter.getParameter(DEParameter.Keys.ALERT_CONFS);
        if (StringUtils.isBlank(alertConfsJson)) {
            logger.info("告警配置不能为空");
            ExceptionHandler.throwParameterException(StatusCode.REAL_ALERT_CONF_MISSING);
        }

        Gson gson = new Gson();
        AlertConfs alertConfs = gson.fromJson(alertConfsJson, AlertConfs.class);
        if (StringUtils.isBlank(planIds)) {
            //保存单个计划告警配置
            saveAlertConf(userInSession, permissionIdsSet, alertConfs);
        } else {
            //计划ID列表不为空，批量修改告警配置，不同计划使用相同的告警配置.
            batchUpdateAlertConfs(alertConfs, StringUtil.stringToList(planIds));
        }
        return SUCCESS_SAVE_ALERT_CONF;
    }

    /**
     * 保存计划的告警配置.
     *
     * @param userInSession
     * @param permissionIdsSet
     * @param alertConfs
     */
    private void saveAlertConf(UserInSession userInSession, Set<Integer> permissionIdsSet, AlertConfs alertConfs) {
        if (alertConfs == null) {
            logger.info("告警配置格式不正确");
            ExceptionHandler.throwParameterException(StatusCode.REAL_ALERT_CONF_FORMAT_INCORRECT);
        }

        if (alertConfs.getAdvanced() != null && alertConfs.getAdvanced().getPlanID() > 0) {
            // 权限判断：如果不是企业账号，且在返回的权限列表中不包含对应的媒体账号ID，也无权限
            if (BUSINESS_ACCOUNT != userInSession.getAccountRole() && !permissionIdsSet.contains(alertConfs.getAdvanced().getMediumAccountId())) {
                logger.info("非当前媒体账号负责人，无权限配置告警");
                ExceptionHandler.throwPermissionException(StatusCode.COMM_NO_OPTIMIZER_PRIVILEGE);
            }
            if (alertConfs.getAdvanced().getId() > 0) {
                //更新告警配置
                deliveryService.updateAlertConfiguration(alertConfs.getAdvanced());
            } else {
                //添加新的告警配置
                deliveryService.addAlertConfiguration(alertConfs.getAdvanced());
            }
        }

        if (alertConfs.getGeneral() != null && alertConfs.getGeneral().getPlanID() > 0) {
            // 权限判断：如果不是企业账号，且在返回的权限列表中不包含对应的媒体账号ID，也无权限
            if (2 != userInSession.getAccountRole() && !permissionIdsSet.contains(alertConfs.getGeneral().getMediumAccountId())) {
                logger.info("非当前媒体账号负责人，无权限配置告警");
                ExceptionHandler.throwPermissionException(StatusCode.COMM_NO_OPTIMIZER_PRIVILEGE);
            }

            if (alertConfs.getGeneral().getId() > 0) {
                //更新告警配置
                deliveryService.updateAlertConfiguration(alertConfs.getGeneral());
            } else {
                //添加新的告警配置
                deliveryService.addAlertConfiguration(alertConfs.getGeneral());
            }
        }
    }

    /**
     * 批量修改告警配置
     *
     * @param alertConfs 通用告警配置
     * @param planIds    计划ID
     */
    private void batchUpdateAlertConfs(AlertConfs alertConfs, List<Integer> planIds) {
        if (alertConfs == null) {
            logger.info("告警配置不能为空");
            ExceptionHandler.throwParameterException(StatusCode.REAL_ALERT_CONF_FORMAT_INCORRECT);
        }

        //严格地，还需要检查计划ID是否被当前用户管理

        //清空现有告警配置
        deliveryService.deleteAlertConfByPlanIds(planIds);

        //添加普通告警配置
        if (alertConfs.getGeneral() != null) {
            deliveryService.addAlertConfigurations(alertConfs.getGeneral(), planIds);
        }

        //添加高级告警配置
        if (alertConfs.getAdvanced() != null) {
            deliveryService.addAlertConfigurations(alertConfs.getAdvanced(), planIds);
        }

    }

    /**
     * 查询告警配置
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.Public, write = false)
    @RequestMapping("/ad/realtimedelivery/getAlert.do")
    public Object getAlert(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Get Alert Conf Data![/ad/realtimedelivery/getAlert.do]");
        //参数解析与校验
        DEContext context = (DEContext) request.getAttribute("CTX");
        DEParameter parameter = context.getDeParameter();
        String planID = parameter.getParameter("planID");
        if (StringUtils.isBlank(planID) || !StringUtils.isNumeric(planID)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR, "planID is empty or error numeric!");
        }

        AlertConf general = deliveryService.getAlert(new AlertQuery(Long.parseLong(planID), AlertType.General));
        AlertConf advanced = deliveryService.getAlert(new AlertQuery(Long.parseLong(planID), AlertType.Advanced));
        return new AlertConfs(general, advanced);
    }

    /**
     * 告警邮件设置
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/realtimedelivery/confAlertEmail.do")
    public Object confAlertEmail(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("Add or Modify Alert Email Conf Data![/ad/realtimedelivery/confAlertEmail.do]");
        //参数解析与校验
        DEContext context = (DEContext) request.getAttribute("CTX");
        DEParameter parameter = context.getDeParameter();
        String accountID = parameter.getParameter("accountID");
        if (StringUtils.isBlank(accountID) || !StringUtils.isNumeric(accountID)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR, "accountId is empty or error numeric!");
        }

        String alertEmails = parameter.getParameter("alertEmails");

        return deliveryService.addOrModifyAlertEmail(Integer.parseInt(accountID), alertEmails);
    }

    /**
     * 实时分析-数据趋势图
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/realtimedelivery/trend.do")
    public Object trend(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute("CTX");
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (permissionMediumAccountIds == null) {
            return null;
        }

        DEParameter parameter = context.getDeParameter();
        //解析参数
        int planId = parameter.getPlanId();
        String periodText = parameter.getPeriod();
        String indicatorText = parameter.getIndicators();
        if (planId <= 0) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_PLAN_ID_MISSING);
        }

        //解析时段参数
        Period period = Period.parse(periodText);
        if (period == null) {
            //如果没有上传时段参数，设置为小时。
            period = Period.Hour;
        }

        //解析指标参数
        //获取登录账号信息
        SystemAccountVo systemAccount = systemAccountService.get(userInSession.getAccountId());
        TrendIndicator[] indicators = null;
        if (StringUtils.isNotBlank(indicatorText)) {
            String[] indicatorArray = indicatorText.split(Separator.COMMA);
            List<TrendIndicator> indicatorList = new ArrayList<>();
            for (String indicator : indicatorArray) {
                TrendIndicator trendIndicator = TrendIndicator.parse(indicator);
                //仅仅返回授权的指标
                if (StringUtils.containsIgnoreCase(systemAccount.getIndicatorPermission(), trendIndicator.getIndicatorId())) {
                    indicatorList.add(trendIndicator);
                }
            }
            indicators = indicatorList.toArray(new TrendIndicator[0]);
        }

        //构建查询条件
        TrendQuery query = new TrendQuery();
        query.setPlanId(planId);
        query.setPeriod(period);
        query.setIndicators(indicators);
        query.setCompanyId(companyId);
        query.setPermissionMediumAccountIds(permissionMediumAccountIds);
        return realTimeTrendService.realTimeTrendChart(query);
    }

    /**
     * 实时分析-数据趋势图-指标列表
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/realtimedelivery/indicators.do")
    public Object indicators(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        SystemAccountVo systemAccount = systemAccountService.get(userInSession.getAccountId());
        List<Item<String, String>> indicators = new ArrayList<>();
        for (TrendIndicator indicator : TrendIndicator.values()) {
            //仅仅返回授权的指标
            if (StringUtils.containsIgnoreCase(systemAccount.getIndicatorPermission(), indicator.getIndicatorId()) && indicator != TrendIndicator.TotalCost) {
                indicators.add(new Item<>(indicator.name(), indicator.getLabel()));
            }
        }
        return indicators;
    }

    /**
     * 实时分析-数据趋势图-指标列表
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/realtimedelivery/period.do")
    public Object period(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<Item<String, String>> periods = new ArrayList<>();
        for (Period period : Period.values()) {
            periods.add(new Item<>(period.name(), period.getLabel()));
        }
        return periods;
    }

}
