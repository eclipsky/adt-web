package com.dataeye.ad.assistor.module.product.model;

import com.google.gson.annotations.Expose;


public class Link {
    @Expose
    private String key;
    @Expose
    private String value;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "Link [key=" + key + ", value=" + value + "]";
	}
    

}
