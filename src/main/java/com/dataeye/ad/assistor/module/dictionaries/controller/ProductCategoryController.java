package com.dataeye.ad.assistor.module.dictionaries.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.module.dictionaries.model.ProductCategoryResult;
import com.dataeye.ad.assistor.module.dictionaries.service.ProductCategoryService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;

/**
 * create by ldj
 * 产品分类映射器
 */
@Controller
public class ProductCategoryController {
	
    @Autowired
    private ProductCategoryService productCategoryService;

    /**
     * 下拉框查询产品分类
     * id =2 只查询游戏类型的产品分类列表 
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/productcategory/query.do")
    public Object query(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	List<ProductCategoryResult> result = productCategoryService.queryFromCache();
        return result;
    }

}
