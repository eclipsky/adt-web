package com.dataeye.ad.assistor.module.realtimedelivery.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLogQuery;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLogView;
import com.dataeye.ad.assistor.module.realtimedelivery.service.OperationLogService;
import com.dataeye.ad.assistor.module.report.constant.Defaults;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * 操作日志控制器
 * Created by huangzehai on 2017/6/29.
 */
@Controller
public class OperationLogController {

    @Autowired
    private OperationLogService operationLogService;

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/realtimedelivery/op-log.do")
    public Object listOperationLogs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        DEParameter parameter = context.getDeParameter();
        String pageNumString = parameter.getParameter(DEParameter.Keys.PAGEID);
        String pageSizeString = parameter.getParameter(DEParameter.Keys.PAGESIZE);
        String planId = parameter.getParameter(DEParameter.Keys.PLAN_ID);
        int pageNum = Defaults.CURRENT_PAGE;
        int pageSize = Defaults.PAGE_SIZE;
        if (StringUtils.isNotBlank(pageNumString)) {
            pageNum = Integer.valueOf(pageNumString);
        }
        if (StringUtils.isNotBlank(pageSizeString)) {
            pageSize = Integer.valueOf(pageSizeString);
        }
        //分页
        PageHelper.startPage(pageNum, pageSize);

        OperationLogQuery query = new OperationLogQuery();
        if (StringUtils.isNotBlank(planId) && NumberUtils.isDigits(planId)) {
            query.setPlanId(Integer.valueOf(planId));
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
//        if (!userInSession.hasManagerOrFollowerPermission()) {
//            return null;
//        }
        query.setCompanyId(userInSession.getCompanyId());
        query.setOptimizerPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());
        Page<OperationLogView> logs = operationLogService.listOperationLogs(query);
        return PageDataHelper.getPageData(logs);
    }
}
