package com.dataeye.ad.assistor.module.dictionaries.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.dictionaries.mapper.PkgMapper;
import com.dataeye.ad.assistor.module.dictionaries.model.Pkg;

@Service("pkgService")
public class PkgService {
	
	@Autowired
	private PkgMapper pkgMapper;
	
	/**
	 * 根据公司ID查询所有包列表
	 * @param companyId
	 * @return
	 */
	public List<Pkg> queryByCompany(Integer companyId,String pkgName) {
		return query(companyId, null, pkgName);
	}
	
	/**
	 * 根据包ID查询单个投放包
	 * @param pkgId
	 * @return
	 */
	public Pkg get(Integer pkgId) {
		List<Pkg> pkgList = query(null, pkgId, null);
		if(pkgList != null && !pkgList.isEmpty()) {
			return pkgList.get(0);
		}
		return null;
	}
	
	/**
	 * 查询投放包列表
	 * @param companyId
	 * @param pkgId
	 * @param pkgName
	 * @return
	 */
	public List<Pkg> query(Integer companyId, Integer pkgId, String pkgName) {
		Pkg pkg = new Pkg();
		pkg.setCompanyId(companyId);
		pkg.setPkgId(pkgId);
		pkg.setPkgName(pkgName);
		return pkgMapper.query(pkg);
	}
	
	/**
	 * 查询投放包列表
	 * @param companyId
	 * @param trackingAppId
	 * @param campaignId
	 * @param pkgName
	 * @return
	 */
	public List<Pkg> queryByTrackingInfo(Integer companyId, String trackingAppId, String campaignId, String pkgName) {
		Pkg pkg = new Pkg();
		pkg.setCompanyId(companyId);
		pkg.setCpAppId(trackingAppId);
		pkg.setCpPkgId(campaignId);
		pkg.setPkgName(pkgName);
		return pkgMapper.query(pkg);
	}
}
