package com.dataeye.ad.assistor.module.result.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.result.model.ContrastiveAnalysisIndicator;
import com.dataeye.ad.assistor.module.result.model.ContrastiveAnalysisQuery;
import com.dataeye.ad.assistor.module.result.model.TrendChart;
import com.dataeye.ad.assistor.module.result.service.ContrastiveAnalysisService;
import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;

/**
 * 对比分析控制器.
 * Created by huangzehai on 2017/4/11.
 */
@Controller
public class ContrastiveAnalysisController {

    @Autowired
    private ContrastiveAnalysisService contrastiveAnalysisService;

    /**
     * 对比分析.
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/contrastive-analysis.do")
    public Object contrastiveAnalysis(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute("CTX");
        DEParameter parameter = context.getDeParameter();
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession == null) {
            return null;
        }
        int companyId = userInSession.getCompanyId();
        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (permissionMediumAccountIds == null) {
            return null;
        }

        String mediumIdString = parameter.getMediumIds();
        String optimizerString = parameter.getParameter(DEParameter.Keys.OPTIMIZERS);
        String indicator = parameter.getParameter(DEParameter.Keys.INDICATOR);
        String productIdsString = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);

        //指标名称不能为空
        if (StringUtils.isBlank(indicator)) {
            ExceptionHandler.throwParameterException(StatusCode.RS_INDICATOR_BLANK);
        }

        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }

        //解析媒体ID列表.
        List<Integer> mediumIds = new ArrayList<>();
        if (StringUtils.isNotBlank(mediumIdString)) {
            for (String mediumId : mediumIdString.split(Constant.Separator.COMMA)) {
                mediumIds.add(Integer.valueOf(mediumId));
            }
        }

        //解析产品ID列表
        List<Integer> productIds = new ArrayList<>();
        if (StringUtils.isNotBlank(productIdsString)) {
            for (String productId : productIdsString.split(Constant.Separator.COMMA)) {
                productIds.add(Integer.valueOf(productId));
            }
        }


        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());

        ContrastiveAnalysisIndicator contrastiveAnalysisIndicator = ContrastiveAnalysisIndicator.parse(indicator);
        if (contrastiveAnalysisIndicator == null) {
            //设置默认指标
            contrastiveAnalysisIndicator = ContrastiveAnalysisIndicator.Cost;
        }

        ContrastiveAnalysisQuery query = new ContrastiveAnalysisQuery();
        query.setMediumIds(mediumIds);
        if (StringUtils.isNotBlank(optimizerString)) {
            query.setOptimizers(Arrays.asList(optimizerString.split(Constant.Separator.COMMA)));
        }
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setCompanyId(companyId);
        query.setPermissionMediumAccountIds(permissionMediumAccountIds);
        query.setProductIds(productIds);

        TrendChart<BigDecimal> chart = null;
        switch (contrastiveAnalysisIndicator) {
            case Cost:
                chart = contrastiveAnalysisService.dailyCostTrendGroupByOptimizer(query);
                break;
            case CTR:
                chart = contrastiveAnalysisService.dailyCtrTrendGroupByOptimizer(query);
                break;
            case DownloadRate:
                chart = contrastiveAnalysisService.dailyDownloadRateTrendGroupByOptimizer(query);
                break;
            case CPA:
                chart = contrastiveAnalysisService.dailyCpaTrendGroupByOptimizer(query);
                break;
            case SevenDayPaybackRate:
                chart = contrastiveAnalysisService.dailySevenDayPaybackRateTrendGroupByOptimizer(query);
                break;
            case SevenDayRetentionRate:
                chart = contrastiveAnalysisService.dailySevenDayRetentionRateTrendGroupByOptimizer(query);
                break;     
        }
        return chart;
    }

    /**
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/contrastive-analysis/optimizers.do")
    public Object listOptimizers(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute("CTX");
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession == null) {
            ExceptionHandler.throwParameterException(StatusCode.LOGI_RE_LOGIN);
        }
        int companyId = userInSession.getCompanyId();
        // 如果为null，则表示无数据访问权限，否则，有权限
//        if (!userInSession.hasManagerOrFollowerPermission()) {
//            return null;
//        }
        DataPermissionDomain dataPermissionDomain = new DataPermissionDomain();
        dataPermissionDomain.setCompanyId(companyId);
        dataPermissionDomain.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        return contrastiveAnalysisService.listOptimizers(dataPermissionDomain);
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/result/contrastive-analysis/products.do")
    public Object listProducts(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute("CTX");
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession == null) {
            ExceptionHandler.throwParameterException(StatusCode.LOGI_RE_LOGIN);
        }
        int companyId = userInSession.getCompanyId();
        return contrastiveAnalysisService.listProducts(companyId);
    }
}
