package com.dataeye.ad.assistor.module.association.mapper;

import com.dataeye.ad.assistor.module.association.model.AssociationRuleVo;
import com.dataeye.ad.assistor.module.association.model.PkgRelation;
import com.dataeye.ad.assistor.module.association.model.PkgVo;
import com.dataeye.ad.assistor.module.association.model.PlanRelation;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 关联规则映射器.
 * Created by luzhuyou 2017/03/02
 */
@MapperScan
public interface AssociationRuleMapper {

    /**
     * 查询关联规则信息列表
     *
     * @param associationRuleVo
     * @return
     */
    public List<AssociationRuleVo> queryAssociationRule(AssociationRuleVo associationRuleVo);

    /**
     * 修改关联规则
     *
     * @param relation
     * @return
     */
    public int updatePlanRelation(PlanRelation relation);

    /**
     * 查询关联规则
     *
     * @param relation
     * @return
     */
    public List<PlanRelation> queryPlanRelation(PlanRelation relation);

    /**
     * 新增关联规则
     *
     * @param relation
     * @return
     */
    public int addPlanRelation(PlanRelation relation);

    /**
     * 根据计划ID查询关联规则
     *
     * @param planId
     * @return
     */
    public PlanRelation getPlanRelation(int planId);

    /**
     * 获取指定媒体账号的计划关联关系.
     *
     * @param mediumAccountId
     * @return
     */
    List<PlanRelation> getPlanRelationsByMediumAccountId(int mediumAccountId);

    /**
     * 新增包关联规则
     *
     * @param relation
     * @return
     * @create ldj 2017-07-11
     */
    public int addPkgRelation(PkgRelation relation);

    /**
     * 修改包的关联规则
     *
     * @param relation
     * @return
     * @create ldj 2017-07-11
     */
    public int updatePkgRelation(PkgRelation relation);

    /**
     * 根据包ID查询包的关联规则
     *
     * @param pkgId
     * @return
     * @create ldj 2017-07-11
     */
    public PkgRelation getPkgRelation(int pkgId);

    /**
     * 查询历史关联规则信息列表
     *
     * @param associationRuleVo
     * @return
     */
    public List<AssociationRuleVo> queryHistoryAssociationRule(
            AssociationRuleVo associationRuleVo);

    /**
     * 查询计划id
     *
     * @param associationRuleVo
     * @return
     */
    public List<Integer> queryPlanIdsByAccountIds(AssociationRuleVo associationRuleVo);


    /**
     * 根据短链号查询关联的落地页id
     *
     * @param campaignId
     * @return
     */
    Integer getPageIdByCampaignId(@Param("campaignId") String campaignId);

    /**
     * 根据短链号查询关联的包id
     *
     * @param campaignId
     * @return
     */
    PkgVo getPkgIdByCampaignId(@Param("campaignId") String campaignId);
}
