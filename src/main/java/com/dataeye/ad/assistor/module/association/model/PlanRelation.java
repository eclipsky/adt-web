package com.dataeye.ad.assistor.module.association.model;

import java.util.Date;

/**
 * 计划关联关系表(计划，产品，落地页，包关联)
 * @author luzhuyou 2017/03/03
 *
 */
public class PlanRelation {
	/** 公司ID */
	private Integer companyId;
	/** 产品ID */
	private Integer productId;
	/** 计划ID */
	private Integer planId;
	/** 计划名称 */
	private String planName;
	/** 页面ID */
	private Integer pageId;
	/** 包ID */
	private Integer pkgId;
	/** 系统类型：0-Others, 1-iOS, 2-Android */
	private Integer osType;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	/**
	 *  优化师ADT账号
	 */
	private String optimizerSystemAccount;
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Integer getPageId() {
		return pageId;
	}
	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}
	public Integer getPkgId() {
		return pkgId;
	}
	public void setPkgId(Integer pkgId) {
		this.pkgId = pkgId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}

	public String getOptimizerSystemAccount() {
		return optimizerSystemAccount;
	}

	public void setOptimizerSystemAccount(String optimizerSystemAccount) {
		this.optimizerSystemAccount = optimizerSystemAccount;
	}

	@Override
	public String toString() {
		return "PlanRelation [companyId=" + companyId + ", productId="
				+ productId + ", planId=" + planId + ", planName=" + planName
				+ ", pageId=" + pageId + ", pkgId=" + pkgId + ", osType="
				+ osType + ", remark=" + remark + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", optimizerSystemAccount=" + optimizerSystemAccount + "]";
	}
	
}
