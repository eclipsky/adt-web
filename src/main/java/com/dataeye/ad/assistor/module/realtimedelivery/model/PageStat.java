package com.dataeye.ad.assistor.module.realtimedelivery.model;

/**
 * Created by huangzehai on 2017/3/2.
 */
public class PageStat  extends Time{
    private Integer pageId;
//    /**
//     * 统计时间
//     */
//    private Date time;

    /**
     *  到达数.
     */
    private Integer reaches;

    /**
     * 下载数
     */
    private Integer downloads;

    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

    public Integer getReaches() {
        return reaches;
    }

    public void setReaches(Integer reaches) {
        this.reaches = reaches;
    }

    public Integer getDownloads() {
        return downloads;
    }

    public void setDownloads(Integer downloads) {
        this.downloads = downloads;
    }

    @Override
    public String toString() {
        return "PageStat{" +
                "pageId=" + pageId +
                ", reaches=" + reaches +
                ", downloads=" + downloads +
                '}';
    }
}
