package com.dataeye.ad.assistor.module.advertisement.constants;



/**
 * 广告投放业务相关枚举值
 * */
public class Advertisement {
	
	/**
     * 推广计划类型
     */
    public enum PlanGroupType {

    	
        CAMPAIGN_TYPE_NORMAL(1),//普通展示广告
        CAMPAIGN_TYPE_WECHAT_OFFICIAL_ACCOUNTS(2), //微信公众号广告
        CAMPAIGN_TYPE_WECHAT_MOMENTS(3);//微信朋友圈广告

        private final int value;

		public int getValue() {
			return value;
		}
		PlanGroupType(int value) {
            this.value = value;
        }
        public static PlanGroupType parse(int text) {
        	switch (text) {
            case 1:
                return PlanGroupType.CAMPAIGN_TYPE_NORMAL;
            case 2:
                return PlanGroupType.CAMPAIGN_TYPE_WECHAT_OFFICIAL_ACCOUNTS;
            case 3:
                return PlanGroupType.CAMPAIGN_TYPE_WECHAT_MOMENTS;    
            default:
                return null;
            }
        }

    }

    /**
     * 标的物类型
     */
    public enum ProductType {

    	PRODUCT_TYPE_APP_IOS(1),
        PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM(2),
        //电商推广
        PRODUCT_TYPE_ECOMMERCE(3),
        //微信品牌页
        PRODUCT_TYPE_LINK_WECHAT(4),
        //微信本地门店推广
        PRODUCT_TYPE_LBS_WECHAT(5),
        //普通链接
        PRODUCT_TYPE_LINK(6);
        

        private final Integer value;

        ProductType(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }

        public static ProductType parse(int text) {
        	switch (text) {
            case 1:
                return ProductType.PRODUCT_TYPE_APP_IOS;
            case 2:
                return ProductType.PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM;
            default:
                return null;
            }
        }

    }

    /**
     * 客户设置的状态
     */
    public enum ConfiguredStatus {

        AD_STATUS_NORMAL(1),
        AD_STATUS_SUSPEND(0),;

        private final Integer value;

        ConfiguredStatus(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
        public static ConfiguredStatus parse(int text) {
        	switch (text) {
            case 1:
                return ConfiguredStatus.AD_STATUS_NORMAL;
            case 0:
                return ConfiguredStatus.AD_STATUS_SUSPEND;
            default:
                return null;
            }
        }
    }
    /**
     * 系统状态 
     */
    public enum SystemStatus {

    	AD_STATUS_FROZEN(0),  //封停 
    	AD_STATUS_NORMAL(1),  //有效
    	AD_STATUS_PENDING(2),  //待审核
    	AD_STATUS_DENIED(3),   // 	审核不通过
    	AD_STATUS_PREPARE(4),;  //准备资源

        private final Integer value;

        SystemStatus(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
        public static SystemStatus parse(int text) {
        	switch (text) {
        	case 0:
        		return SystemStatus.AD_STATUS_FROZEN;
            case 1:
                return SystemStatus.AD_STATUS_NORMAL;
            case 2:
                return SystemStatus.AD_STATUS_PENDING;
            case 3:
                return SystemStatus.AD_STATUS_DENIED;
            case 4:
                return SystemStatus.AD_STATUS_PREPARE;
            default:
                return null;
            }
        }
    }
    
    /**
     * 投放速度类型speed_mode
     */
    public enum SpeedMode {

    	//加速投放
    	SPEED_MODE_FAST(1),
    	//标准投放
    	SPEED_MODE_STANDARD(2),;

        private final Integer value;

        SpeedMode(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
        
        public static SpeedMode parse(int text) {
        	switch (text) {
            case 1:
                return SpeedMode.SPEED_MODE_FAST;
            case 2:
                return SpeedMode.SPEED_MODE_STANDARD;
            default:
                return null;
            }
        }
    }
    
    /**
     * 客户设置的状态
     */
    public enum  BillingEvent {

    	BILLINGEVENT_CLICK(1),
    	BILLINGEVENT_IMPRESSION(4),;

        private final Integer value;

        BillingEvent(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
        public static BillingEvent parse(int text) {
        	switch (text) {
            case 1:
                return BillingEvent.BILLINGEVENT_CLICK;
            case 4:
                return BillingEvent.BILLINGEVENT_IMPRESSION;
            default:
                return null;
            }
        }
    }

    
    /**
     * 资金状态
     */
    public enum   FundStatus {
    	FUND_STATUS_FROZEN(0),  //资金冻结
    	FUND_STATUS_NORMAL(1),  // 	有效
    	FUND_STATUS_NOT_ENOUGH(2),;   //余额不足

        private final Integer value;

        FundStatus(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
        public static FundStatus parse(int text) {
        	switch (text) {
            case 0:
                return FundStatus.FUND_STATUS_FROZEN;
            case 1:
                return FundStatus.FUND_STATUS_NORMAL;
            case 2:
                return FundStatus.FUND_STATUS_NOT_ENOUGH;
            default:
                return null;
            }
        }
    }
    /**
     * 资金账户类型
     */
    public enum   FundType {
    	GENERAL_CASH(0),  // 现金账户
    	;  

        private final Integer value;

        FundType(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
        public static FundType parse(int text) {
        	switch (text) {
            case 0:
                return FundType.GENERAL_CASH;
            default:
                return null;
            }
        }
    }
}
