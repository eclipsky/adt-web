package com.dataeye.ad.assistor.module.webtools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.module.mediumaccount.constants.Constants;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.EncodeUtils;

/**
 * Created by luzhuyou 2017/03/06
 * 加解密工具控制器.
 */
@Controller
public class EncodeController {
	
    /**
     * AES Encrypt
     * @author luzhuyou 2017/03/06
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.Public)
    @RequestMapping("/tools/aesEncrypt.do")
    public Object aesEncrypt(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
        String key = parameter.getParameter("key");
        String decodeValue = null;
        if(StringUtils.isNotBlank(key)) {
        	decodeValue = EncodeUtils.aesEncrypt(key.trim(), Constants.AES_ENCRYPT_KEY);
        }
        return decodeValue;
    }
   
    /**
     * AES Decrypt
     * @author luzhuyou 2017/03/06
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.Public)
    @RequestMapping("/tools/aesDecrypt.do")
    public Object aesDecrypt(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
        String key = parameter.getParameter("key");
        String encodeValue = null;
        if(StringUtils.isNotBlank(key)) {
        	encodeValue = EncodeUtils.aesDecrypt(key.trim(), Constants.AES_ENCRYPT_KEY);
        }
        return encodeValue;
    }
    
    
    /**
     * MD5 Base64 Encode
     * @author luzhuyou 2017/03/06
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.Public)
    @RequestMapping("/tools/md5Base64.do")
    public Object md5Base64(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
        String key = parameter.getParameter("key");
        String encodeValue = null;
        if(StringUtils.isNotBlank(key)) {
        	encodeValue = EncodeUtils.md5Base64Encode(key.trim());
        }
        return encodeValue;
    }
    
    /**
     * MD5 Encode
     * @author luzhuyou 2017/03/06
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.Public)
    @RequestMapping("/tools/md5.do")
    public Object md5(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
        String key = parameter.getParameter("key");
        String encodeValue = null;
        if(StringUtils.isNotBlank(key)) {
        	encodeValue = EncodeUtils.md5Encode(key.trim());
        }
        return encodeValue;
    }

}
