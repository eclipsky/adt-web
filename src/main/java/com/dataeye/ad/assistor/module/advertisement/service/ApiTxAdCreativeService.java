package com.dataeye.ad.assistor.module.advertisement.service;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.advertisement.mapper.ApiTxAdCreativeMapper;
import com.dataeye.ad.assistor.module.advertisement.model.AdCreativeRelationVo;

@Service("apiTxAdCreativeService")
public class ApiTxAdCreativeService {
	
	@Autowired
	private ApiTxAdCreativeMapper apiTxAdCreativeMapper;

	/**
	 *查询广告创意关联关系
	 * @param adGroupId
	 * @param mediumAccountId
	 * @return
	 */
	public List<AdCreativeRelationVo> query(Integer adGroupId,Integer mediumAccountId) {
		AdCreativeRelationVo relation= new AdCreativeRelationVo();
		relation.setAdGroupId(adGroupId);
		relation.setMediumAccountId(mediumAccountId);
		return apiTxAdCreativeMapper.query(relation);
	}
	
	/**
	 * 新建广告创意关联关系
	 * @param adGroupId
	 * @param adsId
	 * @param mediumId
	 * @param mediumAccountId
	 * @return
	 */
	public void add(Integer adGroupId,Integer adsId,Integer adcreativeId,Integer mediumId,Integer mediumAccountId) {
		AdCreativeRelationVo relation= new AdCreativeRelationVo();
		relation.setAdGroupId(adGroupId);
		relation.setAdsId(adsId);
		relation.setAdcreativeId(adcreativeId);
		relation.setMediumAccountId(mediumAccountId);
		relation.setMediumId(mediumId);
		relation.setCreateTime(new Date());
		relation.setUpdateTime(new Date());
		apiTxAdCreativeMapper.add(relation);
	}
	
	/**
	 * 删除广告创意关联关系
	 * @param adGroupId
	 * @return
	 */
	public int delete(Integer adGroupId,Integer adsId,Integer adcreativeId,Integer mediumAccountId) {
		AdCreativeRelationVo relation= new AdCreativeRelationVo();
		relation.setAdGroupId(adGroupId);
		relation.setAdsId(adsId);
		relation.setAdcreativeId(adcreativeId);
		relation.setMediumAccountId(mediumAccountId);
		return apiTxAdCreativeMapper.delete(relation);
	}

}
