package com.dataeye.ad.assistor.module.advertisement.model;


import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * @author lingliqi
 * @date 2017-12-14 19:45
 */
public class TargetingTagVO {

    @Expose
    private List<RegionsBean> regions;
    @Expose
    private List<BusinessInterestBean> businessInterest;
    @Expose
    private List<AppCategoryBean> appCategory;


    public List<RegionsBean> getRegions() {
        return regions;
    }

    public void setRegions(List<RegionsBean> regions) {
        this.regions = regions;
    }

    public List<BusinessInterestBean> getBusinessInterest() {
        return businessInterest;
    }

    public void setBusinessInterest(List<BusinessInterestBean> businessInterest) {
        this.businessInterest = businessInterest;
    }

    public List<AppCategoryBean> getAppCategory() {
        return appCategory;
    }

    public void setAppCategory(List<AppCategoryBean> appCategory) {
        this.appCategory = appCategory;
    }

    public static class RegionsBean {
        @Expose
        private int value;
        @Expose
        private String desc;
        @Expose
        private String name;
        @Expose
        private String parent_id;
        @Expose
        private int not_allow_selected;
        @Expose
        private List<OptionsBeanXX> options;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getParent_id() {
            return parent_id;
        }

        public void setParent_id(String parent_id) {
            this.parent_id = parent_id;
        }

        public int getNot_allow_selected() {
            return not_allow_selected;
        }

        public void setNot_allow_selected(int not_allow_selected) {
            this.not_allow_selected = not_allow_selected;
        }

        public List<OptionsBeanXX> getOptions() {
            return options;
        }

        public void setOptions(List<OptionsBeanXX> options) {
            this.options = options;
        }

        public static class OptionsBeanXX {
            @Expose
            private int value;
            @Expose
            private String desc;
            @Expose
            private String name;
            @Expose
            private int parent_id;
            @Expose
            private List<OptionsBeanX> options;

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getParent_id() {
                return parent_id;
            }

            public void setParent_id(int parent_id) {
                this.parent_id = parent_id;
            }

            public List<OptionsBeanX> getOptions() {
                return options;
            }

            public void setOptions(List<OptionsBeanX> options) {
                this.options = options;
            }

            public static class OptionsBeanX {
                @Expose
                private int value;
                @Expose
                private String desc;
                @Expose
                private String name;
                @Expose
                private int parent_id;
                @Expose
                private List<OptionsBean> options;

                public int getValue() {
                    return value;
                }

                public void setValue(int value) {
                    this.value = value;
                }

                public String getDesc() {
                    return desc;
                }

                public void setDesc(String desc) {
                    this.desc = desc;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public int getParent_id() {
                    return parent_id;
                }

                public void setParent_id(int parent_id) {
                    this.parent_id = parent_id;
                }

                public List<OptionsBean> getOptions() {
                    return options;
                }

                public void setOptions(List<OptionsBean> options) {
                    this.options = options;
                }

                public static class OptionsBean {
                    @Expose
                    private int value;
                    @Expose
                    private String desc;
                    @Expose
                    private String name;
                    @Expose
                    private int parent_id;

                    public int getValue() {
                        return value;
                    }

                    public void setValue(int value) {
                        this.value = value;
                    }

                    public String getDesc() {
                        return desc;
                    }

                    public void setDesc(String desc) {
                        this.desc = desc;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public int getParent_id() {
                        return parent_id;
                    }

                    public void setParent_id(int parent_id) {
                        this.parent_id = parent_id;
                    }
                }
            }
        }
    }

    public static class BusinessInterestBean {
        @Expose
        private int value;
        @Expose
        private String desc;
        @Expose
        private int name;
        @Expose
        private int parent_id;
        @Expose
        private List<OptionsBeanXXX> options;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public int getName() {
            return name;
        }

        public void setName(int name) {
            this.name = name;
        }

        public int getParent_id() {
            return parent_id;
        }

        public void setParent_id(int parent_id) {
            this.parent_id = parent_id;
        }

        public List<OptionsBeanXXX> getOptions() {
            return options;
        }

        public void setOptions(List<OptionsBeanXXX> options) {
            this.options = options;
        }

        public static class OptionsBeanXXX {
            @Expose
            private int value;
            @Expose
            private String desc;
            @Expose
            private int name;
            @Expose
            private String parent_id;
            @Expose
            private List<?> options;

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public int getName() {
                return name;
            }

            public void setName(int name) {
                this.name = name;
            }

            public String getParent_id() {
                return parent_id;
            }

            public void setParent_id(String parent_id) {
                this.parent_id = parent_id;
            }

            public List<?> getOptions() {
                return options;
            }

            public void setOptions(List<?> options) {
                this.options = options;
            }
        }
    }

    public static class AppCategoryBean {
        @Expose
        private String desc;
        @Expose
        private long value;
        @Expose
        private String name;
        @Expose
        private int not_allow_selected;
        @Expose
        private List<OptionsBeanXXXX> options;

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public long getValue() {
            return value;
        }

        public void setValue(long value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getNot_allow_selected() {
            return not_allow_selected;
        }

        public void setNot_allow_selected(int not_allow_selected) {
            this.not_allow_selected = not_allow_selected;
        }

        public List<OptionsBeanXXXX> getOptions() {
            return options;
        }

        public void setOptions(List<OptionsBeanXXXX> options) {
            this.options = options;
        }

        public static class OptionsBeanXXXX {
            @Expose
            private String desc;
            @Expose
            private long value;
            @Expose
            private String name;
            @Expose
            private int not_allow_selected;
            @Expose
            private List<?> options;

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public long getValue() {
                return value;
            }

            public void setValue(long value) {
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getNot_allow_selected() {
                return not_allow_selected;
            }

            public void setNot_allow_selected(int not_allow_selected) {
                this.not_allow_selected = not_allow_selected;
            }

            public List<?> getOptions() {
                return options;
            }

            public void setOptions(List<?> options) {
                this.options = options;
            }
        }
    }
}
