package com.dataeye.ad.assistor.module.product.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 产品管理模块常量表
 * @author luzhuyou 2017/02/20
 */
public class Constants {

	public static final double DEFALUT_CP_SHARE_RATE = 1.00; // 默认CP分成率
	
	/**
	 * 产品状态
	 * 
	 * @author luzhuyou 2017/02/20
	 */
	public class ProductStatus {
		/** 正常 */
		public static final int NORMAL = 0;
		/** 失效  */
		public static final int INVALID = 1;
	}
	
	/**
	 * 1、获取新建产品的appid
	 * 2、获取实时统计数据 （SDK接入测试）
	 * 3、增加、删除、查询测试设备相关接口
	 * 4、删除、查询实时测试日志接口
	 * 
	 * */
	public static class TrackingInterface  {
		/**
		 * 获取新建产品的appid
		 * */
		public static final String GET_APPID_KEY = "get_appid";
		public static final String GET_APPID = "http://10.1.2.196:6940/adt/app/createApp";
		
		/**
		 * 修改app
		 * */
		public static final String MODIFY_APP_KEY = "modify_app";
		public static final String MODIFY_APP_URL = "http://10.1.2.196:6940/adt/app/modifyApp";
		
		/**
		 * 获取实时统计数据 （SDK接入测试）
		 * */
		public static final String GET_REALTIME_INFO_KEY = "get_realtime_info";
		public static final String GET_REALTIME_INFO = "http://10.1.2.196:6940/adt/app/statistics/listChannelRealTimeData";
		
		/**
		 * （SDK接入测试）
		 * 查询测试设备
		 * */
		public static final String QUERY_DEBUG_DEVICE_KEY = "query_debugDevice";
		public static final String QUERY_DEBUG_DEVICE_URL = "http://192.168.1.191:10100/listDebugDevice.do";
		
		/**
		 * （SDK接入测试）
		 * 判断一个设备名称是否已经存在
		 * */
		public static final String QUERY_DEVICENAME_EXISTS_KEY = "query_debugDeviceNameExists";
		public static final String QUERY_DEVICENAME_EXISTS_URL = "http://192.168.1.191:10100/isDeviceNameExists.do";
		
		/**
		 * （SDK接入测试）
		 * 添加一条测试设备信息
		 * */
		public static final String ADD_DEBUG_DEVICE_KEY = "add_debugDevice";
		public static final String ADD_DEBUG_DEVICE_URL = "http://192.168.1.191:10100/addDebugDevice.do";
		
		/**
		 * （SDK接入测试）
		 * 删除一条测试设备信息
		 * */
		public static final String DELETE_DEBUG_DEVICE_KEY = "delete_debugDevice";
		public static final String DELETE_DEBUG_DEVICE_URL = "http://192.168.1.191:10100/deleteDebugDevice.do";
		
		/**
		 * （SDK接入测试）
		 * 实时日志查询
		 * */
		public static final String QUERY_DEBUG_LOG_KEY = "query_debugLog";
		public static final String QUERY_DEBUG_LOG_URL = "http://192.168.1.191:10100/listDebugLog.do";
		
		/**
		 * （SDK接入测试）
		 * 查询调试统计数据
		 * */
		public static final String QUERY_DEBUG_SUMMARY_KEY = "query_debugSummary";
		public static final String QUERY_DEBUG_SUMMARY_URL = "http://192.168.1.191:10100/listDebugSummary.do";
		
		/**
		 * （SDK接入测试）
		 * 清除调试数据
		 * */
		public static final String DELETE_DEBUG_DATA_KEY = "delete_debugData";
		public static final String DELETE_DEBUG_DATA_URL = "http://192.168.1.191:10100/clearDebugData.do";
	}
	
	public static class DebugLogType{
		 /**
	     * 测试日志类型
	     */
	    public static final Map<String, String> debugLogTypeMap = new HashMap<>();

	    public static String getdebugLogType(String key) {
			return debugLogTypeMap.get(key);
		}
		static {
			debugLogTypeMap.put("active","active");  //激活
			debugLogTypeMap.put("register","register");  //注册
			debugLogTypeMap.put("login","login");  //登录
			debugLogTypeMap.put("payment","payment");  //付费
			debugLogTypeMap.put("det","det");  //自定义
			debugLogTypeMap.put("total","total");  //所有的
		}
	}
}
