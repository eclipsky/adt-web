package com.dataeye.ad.assistor.module.advertisement.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.advertisement.model.AdGroupQueryVo;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupCreatePreVO;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupSelectVO;
import com.dataeye.ad.assistor.module.advertisement.model.PlanGroupVO;

/**
 * 广告投放
 * 推广计划映射器.
 * Created by ldj
 */
@MapperScan
public interface ApiTxCampaignMapper {

    /**
     * 查询推广计划
     *
     * @param vo
     * @return
     */
    public List<PlanGroupVO> query(AdGroupQueryVo vo);

    /**
     * 下拉框查询推广计划
     *
     * @param vo
     * @return
     */
    public List<PlanGroupSelectVO> queryForSelect(AdGroupQueryVo vo);

    /**
     * 更新推广计划
     *
     * @param
     * @return
     */
    public int update(PlanGroupCreatePreVO vo);

    /**
     * 创建推广计划
     *
     * @param
     */
    public int add(PlanGroupCreatePreVO vo);

    /**
     * 删除推广计划
     *
     * @param
     * @return
     */
    public int delete(PlanGroupCreatePreVO vo);

    /**
     * 根据计划id和媒体帐号id查询推广计划
     *
     * @param
     * @param vo
     * @return
     */
    public PlanGroupSelectVO getPlanGroup(PlanGroupCreatePreVO vo);

    /**
     * 查询数据库是否存在该记录
     *
     * @param mediumAccountId
     * @param planGroupName
     * @param planGroupId
     * @return
     */
    public Integer getRecordByPlanGroupName(@Param("mediumAccountId") Integer mediumAccountId,
                                            @Param("planGroupName") String planGroupName, @Param("planGroupId") Integer planGroupId);
}
