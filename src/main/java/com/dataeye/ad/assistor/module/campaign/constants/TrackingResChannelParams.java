package com.dataeye.ad.assistor.module.campaign.constants;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.dataeye.ad.assistor.module.payback.model.Period;
import com.google.gson.annotations.Expose;

public class TrackingResChannelParams {
	@Expose
	private List<TemplateParams> templateParams;
	@Expose
	private List<EventType> eventTypeList;
	
	public List<TemplateParams> getTemplateParams() {
		return templateParams;
	}
	public void setTemplateParams(List<TemplateParams> templateParams) {
		this.templateParams = templateParams;
	}
    public List<EventType> getEventTypeList() {
		return eventTypeList;
	}
	public void setEventTypeList(List<EventType> eventTypeList) {
		this.eventTypeList = eventTypeList;
	}
	@Override
	public String toString() {
		return "TrackingResChannelParams [templateParams=" + templateParams
				+ ", eventTypeList=" + eventTypeList + "]";
	}



class TemplateParams {
		@Expose
		private String label;
		@Expose
		private String key;
		@Expose
		private String type;
		@Expose
		private String defaultValue;
		@Expose
		private String isRequired;
		@Expose
		private List<Options> options;
		
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public String getDefaultValue() {
			return defaultValue;
		}
		public void setDefaultValue(String defaultValue) {
			this.defaultValue = defaultValue;
		}
		public String getIsRequired() {
			return isRequired;
		}
		public void setIsRequired(String isRequired) {
			this.isRequired = isRequired;
		}
		public List<Options> getOptions() {
			return options;
		}
		public void setOptions(List<Options> options) {
			this.options = options;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		@Override
		public String toString() {
			return "TemplateParams [label=" + label + ", key=" + key
					+ ", type=" + type + ", defaultValue=" + defaultValue
					+ ", isRequired=" + isRequired + ", options=" + options
					+ "]";
		}

		
	}

	class Options {
		@Expose
		private String label;
		@Expose
		private String value;
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		@Override
		public String toString() {
			return "Options [label=" + label + ", value=" + value + "]";
		}
		
	}
	
	class EventType {
		@Expose
		private String key;
		@Expose
		private String name;
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		@Override
		public String toString() {
			return "EventType [key=" + key + ", name=" + name + "]";
		}
		
	}
	
}