package com.dataeye.ad.assistor.module.debugcenter.service;

import com.dataeye.ad.assistor.module.debugcenter.dao.DebugDeviceDao;
import com.dataeye.ad.assistor.module.debugcenter.model.*;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.EncodeUtils;
import com.dataeye.ad.assistor.util.HbasePool;
import com.dataeye.ad.assistor.util.redis.RedisClusterClient;
import com.qq.jutil.string.StringUtil;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author lingliqi
 * @date 2018-01-18 10:39
 */
@Service
public class DebugCenterService {

    @Autowired
    private DebugDeviceDao debugDeviceDao;


    private static RedisClusterClient redisClusterClient = RedisClusterClient.getInstance();

    private static final String adt_device_action_details = "adt_device_action_details";
    private static final String adt_click_device_info = "adt_click_device_info";


    private static final String KEY_SRC_AD = "ad";
    private static final String KEY_SRC_NATURAL = "n";
    /**
     * key类型为对应的uid去重key
     */
    private static final String KEY_TYPE_UID = "u";
    /**
     * 用户登录key
     */
    private static final String UserLoginInfoKey = "adt:lg:v1:${src}:${type}:${uid}:${appid}";
    /**
     * 用户注册key
     */
    private static final String UserRegisterInfoKey = "adt:rg:v1:${src}:${type}:${uid}:${appid}";
    /**
     * 用户支付key
     */
    private static final String UserPayInfoKey = "adt:py:v1:${src}:${type}:${uid}:${appid}";
    /**
     * 设备激活key
     */
    private static final String DeviceActiveInfoKey = "adt:ac:ipc:v1:${uid}:${appid}";
    /**
     * 用户活跃Key
     */
    public static final String UserActiveInfoKey = "adt:au:v1:${src}:${uid}:${appid}";

    private static final String TABLE_CAMPAIGN_NAME = "adt_campaign.campaign_";

    /**
     * 清除调试设备信息
     *
     * @param parameter
     * @return
     */
    public Map<String, Integer> clearDebugDevice(DebugCenterParameter parameter) throws Exception {
        Map<String, Integer> resultMap = new HashMap<String, Integer>();
        String key = parameter.getDevice().toUpperCase() + "#" + parameter.getAppid();
        HTableInterface table = HbasePool.getConnection().getTable(adt_device_action_details);
        Get get = new Get(Bytes.toBytes(key));
        Result result = table.get(get);
        if (result.isEmpty()) {
            // 没有找到激活信息
            resultMap.put("code", 0);// 0 -- 不存在设备信息
            return resultMap;
        }
        // 根据设备信息查找对应的激活信息
        String uid = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("uid")));
        String idfa = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("idfa")));
        String imei1 = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("imei1")));
        String imei2 = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("imei2")));
        String mac = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("mac")));
        String androidId = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("androidId")));
        String androidAdId = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("androidAdId")));
        // 根据设备信息清除激活信息
        List<Delete> deletes = new ArrayList<Delete>();
        String[] deviceArray = {uid, idfa, imei1, imei2, mac, androidId, androidAdId};
        for (String activeDevice : deviceArray) {
            if (StringUtil.isNotEmpty(activeDevice)) {
                String activeHbaseKey = activeDevice + "#" + parameter.getAppid();
                Delete delete = new Delete(Bytes.toBytes(activeHbaseKey));
                deletes.add(delete);
            }
        }
        if (0 < deletes.size()) {
            table.delete(deletes);
        }
        String activeDeviceKey = DeviceActiveInfoKey.replace("${uid}", uid).replace("${appid}", parameter.getAppid());
        redisClusterClient.del(activeDeviceKey);
        // 根据uid清除注册信息
        String adUserRegisterKey = UserRegisterInfoKey.replace("${src}", KEY_SRC_AD).replace("${type}", KEY_TYPE_UID).replace("${uid}", uid).replace("${appid}", parameter.getAppid());
        String naturalUserRegisterKey = UserRegisterInfoKey.replace("${src}", KEY_SRC_NATURAL).replace("${type}", KEY_TYPE_UID).replace("${uid}", uid).replace("${appid}", parameter.getAppid());
        redisClusterClient.del(adUserRegisterKey);
        redisClusterClient.del(naturalUserRegisterKey);
        // 根据uid清除登录信息
        String adUserLoginKey = UserLoginInfoKey.replace("${src}", KEY_SRC_AD).replace("${type}", KEY_TYPE_UID).replace("${uid}", uid).replace("${appid}", parameter.getAppid());
        String naturalUserLoginKey = UserLoginInfoKey.replace("${src}", KEY_SRC_NATURAL).replace("${type}", KEY_TYPE_UID).replace("${uid}", uid).replace("${appid}", parameter.getAppid());
        redisClusterClient.del(adUserLoginKey);
        redisClusterClient.del(naturalUserLoginKey);
        // 根据uid清除付费信息
        //付费可能不存在uid，所以设备有uid-->idfa-->imei1-->imei2
        String[] payDeviceArray = {uid, idfa, imei1, imei2};
        for (String payDevice : payDeviceArray) {
            if (StringUtil.isNotEmpty(payDevice)) {
                String adUserPaymentKey = UserPayInfoKey.replace("${src}", KEY_SRC_AD).replace("${type}", KEY_TYPE_UID).replace("${uid}", payDevice).replace("${appid}", parameter.getAppid());
                String naturalUserPaymentKey = UserPayInfoKey.replace("${src}", KEY_SRC_NATURAL).replace("${type}", KEY_TYPE_UID).replace("${uid}", payDevice).replace("${appid}", parameter.getAppid());
                redisClusterClient.del(adUserPaymentKey);
                redisClusterClient.del(naturalUserPaymentKey);
            }
        }
        //根据uid清除活跃信息
        String adUserActiveKey = UserActiveInfoKey.replace("${src}", KEY_SRC_AD).replace("${uid}", uid).replace("${appid}", parameter.getAppid());
        String naturalActiveKey = UserActiveInfoKey.replace("${src}", KEY_SRC_NATURAL).replace("${uid}", uid).replace("${appid}", parameter.getAppid());
        redisClusterClient.del(adUserActiveKey);
        redisClusterClient.del(naturalActiveKey);
        resultMap.put("code", 1);//1--存在设备信息且删除成功
        return resultMap;
    }

    /**
     * 归因查询
     *
     * @param parameter
     * @return
     * @throws Exception
     */
    public DebugDeviceMatchDataVo debugDeviceMatch(DebugCenterParameter parameter) throws Exception {
        //初始化返回对象
        DebugDeviceMatchDataVo deviceMatchDataVo = new DebugDeviceMatchDataVo();

        String key = parameter.getDevice().toUpperCase() + "#" + parameter.getAppid();//后台存储的都是大写值

        // HBase操作
        HTableInterface table = HbasePool.getConnection().getTable(adt_device_action_details);
        Get get = new Get(Bytes.toBytes(key));
        Result result = table.get(get);
        if (result.isEmpty()) {
            //没找到激活信息
            return deviceMatchDataVo;
        }
        String uid = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("uid")));
        String channelId = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("channel")));
        String compaignId = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("compaignId")));
        long activeTime = Long.valueOf(Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("activeTime"))));
        String activeIp = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("activeIp")));
        String platform = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("platform")));
        String idfa = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("idfa")));
        String imei1 = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("imei1")));
        String imei2 = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("imei2")));
        String mac = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("mac")));
        String androidId = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("androidId")));
        String androidAdId = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("androidAdId")));

        //
        if (platform.equals("1")) {
            deviceMatchDataVo.deviceInfo.add(new KeyValue<String, String>("UID", uid));
            deviceMatchDataVo.deviceInfo.add(new KeyValue<String, String>("IDFA", idfa));
            if (StringUtil.isNotEmpty(mac)) {
                deviceMatchDataVo.deviceInfo.add(new KeyValue<String, String>("MAC", mac));
            }
        } else if (platform.equals("2")) {
            deviceMatchDataVo.deviceInfo.add(new KeyValue<String, String>("UID", uid));
            deviceMatchDataVo.deviceInfo.add(new KeyValue<String, String>("IMEI1", imei1));
            deviceMatchDataVo.deviceInfo.add(new KeyValue<String, String>("IMEI2", imei2));
            deviceMatchDataVo.deviceInfo.add(new KeyValue<String, String>("MAC", mac));
            deviceMatchDataVo.deviceInfo.add(new KeyValue<String, String>("AndroidId", androidId));
        }

        if (StringUtil.isNotEmpty(compaignId) && !"-".equals(compaignId)) {
            //短链存在，说明是推广流量，需要找到推广流量的来源点击信息
            List<String> validDeviceList = new ArrayList<String>();
            // 转码
            addEncodeDevice(idfa, validDeviceList);
            addEncodeDevice(imei1, validDeviceList);
            addEncodeDevice(imei2, validDeviceList);
            addEncodeDevice(mac, validDeviceList);
            addEncodeDevice(androidId, validDeviceList);
            addEncodeDevice(androidAdId, validDeviceList);
            // 获取点击详情 HBase查询
            Map<String, Object> clickInfoMap = getClickInfo(validDeviceList, activeIp, compaignId);
            // 渠道信息 查询数据库 adt_business.channel
            ChannelInfoDomain channelDomain = debugDeviceDao.getChannelInfoByChannelId(StringUtil.convertInt(channelId, 0));
            String channelName = channelDomain == null ? channelId : channelDomain.getShowName();
            // 查询推广活动
            int tableIndex = Math.abs(parameter.getAppid().hashCode()) % 1000;
            String tableName = TABLE_CAMPAIGN_NAME + tableIndex;
            Map<String, Object> campaignMap = debugDeviceDao.listOneCampaignById(parameter.getAppid(), compaignId, tableName);

            deviceMatchDataVo.matchInfo.add(new KeyValue<String, String>("渠道", channelName));
            deviceMatchDataVo.matchInfo.add(new KeyValue<String, String>("短链", compaignId));
            String campaignName = null != campaignMap && 0 < campaignMap.size() ? campaignMap.get("name") + "" : compaignId;
            deviceMatchDataVo.matchInfo.add(new KeyValue<String, String>("推广活动", campaignName));
            if (null != clickInfoMap && 0 < clickInfoMap.size()) {
                long clickTime = (long) clickInfoMap.get("clickTime");
                deviceMatchDataVo.matchInfo.add(new KeyValue<String, String>("点击时间", DateUtils.format(new Date(1000l * clickTime), "yyyy-MM-dd HH:mm:ss")));
                deviceMatchDataVo.matchInfo.add(new KeyValue<String, String>("点击IP", clickInfoMap.get("clickIp") + ""));
            }
            deviceMatchDataVo.matchInfo.add(new KeyValue<String, String>("激活时间", DateUtils.format(new Date(1000 * activeTime), "yyyy-MM-dd HH:mm:ss")));
            deviceMatchDataVo.matchInfo.add(new KeyValue<String, String>("激活IP", activeIp));

        } else {

            deviceMatchDataVo.matchInfo.add(new KeyValue<String, String>("渠道", "自然流量"));
            deviceMatchDataVo.matchInfo.add(new KeyValue<String, String>("激活时间", DateUtils.format(new Date(1000l * activeTime), "yyyy-MM-dd HH:mm:ss")));
            deviceMatchDataVo.matchInfo.add(new KeyValue<String, String>("激活IP", activeIp));

        }

        return deviceMatchDataVo;
    }

    /**
     * 查询媒体联调测试设备列表
     *
     * @param uid
     * @return
     */
    public List<ChannelDebugDevice> queryChannelDebugDevice(int uid) {
        return debugDeviceDao.queryChannelDebugDevice(uid);
    }

    /**
     * 新增联调测试设备
     *
     * @param deviceName
     * @param deviceId
     * @param uid
     * @return
     */
    public Object addChannelDebugDevice(String deviceName, String deviceId, int uid) {
        Map<String, Integer> result = new HashMap<>();
        debugDeviceDao.addChannelDebugDevice(deviceName, deviceId, uid);
        return "success";
    }

    public Object deleteChannelDebugDevice(String deviceId, int uid) {
        Map<String, Integer> result = new HashMap<>();
        debugDeviceDao.deleteChannelDebugDevice(deviceId, uid);
        return "success";
    }

    /**
     * 根据设备信息及Ip信息查找对应的点击信息
     *
     * @param validDeviceList
     * @param activeIp
     * @param compaignId
     * @return
     * @throws Exception
     */
    private Map<String, Object> getClickInfo(List<String> validDeviceList, String activeIp, String compaignId) throws Exception {
        HTableInterface tableInterface = HbasePool.getConnection().getTable(adt_click_device_info);
        for (String device : validDeviceList) {
            if (StringUtil.isEmpty(device)) {
                continue;
            }
            String rowkey = compaignId + "#" + device;
            Get get = new Get(Bytes.toBytes(rowkey));
            Result result = tableInterface.get(get);
            if (!result.isEmpty()) {//查找到了数据
                Map<String, Object> resMap = new HashMap<>();
                long clickTime = Bytes.toLong(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("ct")));
                String clickIp = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("ci")));
                resMap.put("clickTime", clickTime);
                resMap.put("clickIp", clickIp);
                return resMap;
            }
        }
        if (StringUtil.isNotEmpty(activeIp)) {
            //设备匹配都查找不到，则查找ip信息
            String rowkey = compaignId + "#" + activeIp;
            Get get = new Get(Bytes.toBytes(rowkey));
            Result result = tableInterface.get(get);
            if (!result.isEmpty()) {//查找到了数据
                Map<String, Object> resMap = new HashMap<>();
                long clickTime = Bytes.toLong(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("ct")));
                String clickIp = Bytes.toString(result.getValue(Bytes.toBytes("info"), Bytes.toBytes("ci")));
                resMap.put("clickTime", clickTime);
                resMap.put("clickIp", clickIp);
                return resMap;
            }
        }

        return null;
    }

    private static void addEncodeDevice(String device, List<String> deviceList) {
        if (StringUtil.isNotEmpty(device)) {
            String imei1Md5 = EncodeUtils.md5Upper(device);
            String imei1LowerMd5 = EncodeUtils.md5Upper(device.toLowerCase());
            String imei1UpperMd5 = EncodeUtils.md5Upper(device.toUpperCase());
            deviceList.add(device);
            deviceList.add(imei1Md5);
            deviceList.add(imei1LowerMd5);
            deviceList.add(imei1UpperMd5);
        }
    }
}
