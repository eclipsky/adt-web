package com.dataeye.ad.assistor.module.product.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;
import com.google.gson.annotations.Expose;


/**
 * 下拉框产品信息VO
 * @author luzhuyou 2017/02/23
 *
 */
public class ProductSelectorVo extends DataPermissionDomain {

	/** 产品ID */
	@Expose
	private Integer productId;
	/** 产品名称 */
	@Expose
	private String productName;
	@Expose 
	private String appId;
	@Expose
	private Integer osType;
	private String optimizerSystemAccount;
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	public String getOptimizerSystemAccount() {
		return optimizerSystemAccount;
	}
	public void setOptimizerSystemAccount(String optimizerSystemAccount) {
		this.optimizerSystemAccount = optimizerSystemAccount;
	}
	@Override
	public String toString() {
		return "ProductSelectorVo [productId=" + productId + ", productName="
				+ productName + ", appId=" + appId + ", osType=" + osType
				+ ", optimizerSystemAccount=" + optimizerSystemAccount + "]";
	}
	
}
