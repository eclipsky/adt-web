package com.dataeye.ad.assistor.module.advertisement.mapper;

import java.util.List;

import com.dataeye.ad.assistor.module.advertisement.model.AdProductVO;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

/**
 * @author lingliqi
 * @date 2017-12-23 11:30
 */
@MapperScan
public interface ApiTxAppProductMapper {

/*    *//**
     * 查询推广产品的详情信息
     *
     * @param mediumAccountId
     * @param productType
     * @param productRefsId
     * @return
     *//*
    AdProductVO query(@Param("mediumAccountId") int mediumAccountId,
                      @Param("productType") String productType, @Param("productRefsId") String productRefsId, @Param("appId") String appId);*/

    /**
     * 查询推广产品的详情信息
     *
     * @param adProductVO
     */
    List<AdProductVO> query(AdProductVO adProductVO);
    /**
     * 新增推广产品的详情
     *
     * @param adProductVO
     */
    void add(AdProductVO adProductVO);
    
    /**
     * 修改推广产品的详情
     *
     * @param adProductVO
     */
    void update(AdProductVO adProductVO);
}
