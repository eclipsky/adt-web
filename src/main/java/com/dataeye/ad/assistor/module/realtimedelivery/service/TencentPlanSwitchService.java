package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.PlanSwitchMessages;
import com.dataeye.ad.assistor.module.realtimedelivery.model.DeliveryStatus;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationStatus;
import com.dataeye.ad.assistor.util.TencentCookieUtils;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 广点通计划开关服务.
 * Created by huangzehai on 2017/5/19.
 */
@Service("tencentPlanSwitchService")
public class TencentPlanSwitchService extends AbstractMediumPlanService<DeliveryStatus> implements PlanSwitchService {
    private static final String URL = "http://e.qq.com/ec/api.php?mod=order&act=setstatus&g_tk=%s&d_p=0.30285953200645843&callback=frameElement.callback&script";

    private static final String ENABLE = "1";
    private static final String DISABLE = "10";
    private static final String REFERER_FORMAT = "http://e.qq.com/atlas/%s/report/order";

    private static final String UPDATE_SUCCESS = "修改成功";

    @Override
    public Result updateStatus(DeliveryStatus status) {
        return this.execute(status);
    }

    @Override
    protected String url(String cookie, Long mediumPlanId) {
        String gtk = TencentCookieUtils.getGTK(cookie);
        return String.format(URL, gtk);
    }

    @Override
    protected List<NameValuePair> parameters(String cookie, DeliveryStatus status) {
        List<NameValuePair> params = new ArrayList<>();
        String uid = TencentCookieUtils.getUid(cookie);
        params.add(new BasicNameValuePair("qzreferrer", String.format(REFERER_FORMAT, uid)));
        //计划ID
        params.add(new BasicNameValuePair("aid", status.getMediumPlanId().toString()));
        //操作
        params.add(new BasicNameValuePair("status", status.getStatus() == OperationStatus.Enable ? ENABLE : DISABLE));
        params.add(new BasicNameValuePair("callback", "frameElement.callback&script"));
        params.add(new BasicNameValuePair("owner", uid));
        return params;
    }

    @Override
    protected void requestHeader(HttpRequestBase request, String cookie) {
        String uid = TencentCookieUtils.getUid(cookie);
        request.setHeader(HOST, "e.qq.com");
        request.setHeader(ORIGIN, "http://e.qq.com");
        request.setHeader(REFERER, String.format(REFERER_FORMAT, uid));
    }

    @Override
    protected Result response(String content) {
        String json = StringUtils.substringBetween(content, "callback(", ")");
        Gson gson = new Gson();
        Response response = gson.fromJson(json, Response.class);
        Result result = new Result();
        if (response.getRet() == 0 && StringUtils.equalsIgnoreCase(UPDATE_SUCCESS, response.getMsg())) {
            result.setSuccess(true);
            result.setMessage(PlanSwitchMessages.SUCCESS);
        } else {
            result.setSuccess(false);
            result.setMessage(response.getMsg());
        }
        return result;
    }

    private class Response {
        private int ret;
        private String msg;
//        private Data data;

        public int getRet() {
            return ret;
        }

        public void setRet(int ret) {
            this.ret = ret;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

//        public Data getData() {
//            return data;
//        }
//
//        public void setData(Data data) {
//            this.data = data;
//        }

        @Override
        public String toString() {
            return "Response{" +
                    "ret=" + ret +
                    ", msg='" + msg + '\'' +
//                    ", data=" + data +
                    '}';
        }

        private class Data {
            private int status;

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            @Override
            public String toString() {
                return "Data{" +
                        "status=" + status +
                        '}';
            }
        }
    }
}
