package com.dataeye.ad.assistor.module.report.controller;

import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.report.constant.Defaults;
import com.dataeye.ad.assistor.module.report.model.CommonQuery;
import com.dataeye.ad.assistor.module.report.model.MediumData;
import com.dataeye.ad.assistor.module.report.service.MediumDataService;
import com.dataeye.ad.assistor.privilege.DataPermissionHandler;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.*;

/**
 * 媒体数据控制器
 */
@Controller
public class MediumDataControler {

    private static final String SUCCESS = "保存媒体数据成功!";

    private static final String MEDIUM_DATA_EMPTY = "未改动任何数据!";

    @Autowired
    private MediumDataService mediumDataService;

    /**
     * 投放日报-计划维度-汇总数据
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/medium-data/list.do")
    public Object listMediumData(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        // 仅接触负责人管理的计划，企业账户也不能查看媒体数据
        if (!userInSession.hasManagerPermission() || (userInSession.getOptimizerPermissionMediumAccountIds() != null && userInSession.getOptimizerPermissionMediumAccountIds().length == 0)) {
            return null;
        }
        CommonQuery query = parseQuery(context);
        query.setCompanyId(userInSession.getCompanyId());
        query.setOptimizerPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());
        query.setPermissionSystemAccounts(userInSession.getPermissionSystemAccounts());
        //分页
        paging(context);
        Page<MediumData> mediumDataList = mediumDataService.listMediumData(query);

        //设置是否可以编辑标志位
        List<Integer> managingMediumAccountIds;
        if (userInSession.getOptimizerPermissionMediumAccountIds() == null) {
            managingMediumAccountIds = Collections.EMPTY_LIST;
        } else {
            managingMediumAccountIds = Arrays.asList(userInSession.getOptimizerPermissionMediumAccountIds());
        }

        for (MediumData mediumData : mediumDataList) {
            if (mediumData.getMediumAccountId() != null && managingMediumAccountIds.contains(mediumData.getMediumAccountId())) {
                mediumData.setEditable(true);
            }
        }
        return PageDataHelper.getPageData(mediumDataList);
    }

    /**
     * 分页
     *
     * @param context
     */
    private void paging(DEContext context) {
        //分页
        DEParameter parameter = context.getDeParameter();
        String pageNumString = parameter.getParameter(DEParameter.Keys.PAGEID);
        String pageSizeString = parameter.getParameter(DEParameter.Keys.PAGESIZE);
        int pageNumber = Defaults.CURRENT_PAGE;
        int pageSize = Defaults.PAGE_SIZE;
        if (StringUtils.isNotBlank(pageNumString)) {
            pageNumber = Integer.valueOf(pageNumString);
        }
        if (StringUtils.isNotBlank(pageSizeString)) {
            pageSize = Integer.valueOf(pageSizeString);
        }
        //分页
        PageHelper.startPage(pageNumber, pageSize);
    }

    /**
     * 解析按日统计参数
     *
     * @param context
     * @return
     * @throws ParseException
     */
    private CommonQuery parseQuery(DEContext context) throws ParseException {
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }

        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        //构建查询条件
        CommonQuery query = new CommonQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);

        //解析产品ID列表
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setPlanIds(StringUtil.stringToList(parameter.getPlanIds()));
        query.setAccountIds(StringUtil.stringToList(parameter.getAccountIds()));
        query.setMediumIds(StringUtil.stringToList(parameter.getMediumIds()));
        return query;
    }

    /**
     * 投放日报-计划维度-汇总数据
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/medium-data/update.do")
    public Object updateMediumData(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (userInSession == null) {
            ExceptionHandler.throwParameterException(StatusCode.LOGI_RE_LOGIN);
        }
        DEParameter parameter = context.getDeParameter();
        String mediumData = parameter.getParameter(DEParameter.Keys.MEDIUM_DATA);
        if (StringUtils.isBlank(mediumData)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_MEDIUM_DATA_MISSING);
        }

        Gson gson = new Gson();
        MediumData[] mediumDataArray = gson.fromJson(mediumData, MediumData[].class);

        List<Integer> managingMediumAccountIds;
        Integer[] optimizerPermissionMediumAccountIds = DataPermissionHandler.getOptimizerPermissionMediumAccountIds(userInSession);
        if (optimizerPermissionMediumAccountIds == null) {
            managingMediumAccountIds = Collections.EMPTY_LIST;
        } else {
            managingMediumAccountIds = Arrays.asList(optimizerPermissionMediumAccountIds);
        }
        List<MediumData> mediumDataList = filter(mediumDataArray, managingMediumAccountIds);
        //无权限编辑媒体数据
        if (mediumDataArray != null && mediumDataArray.length > 0 && mediumDataList.isEmpty()) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_NO_PERMISSION_EDIT_MEDIUM_DATA);
        }

        if (mediumDataList == null || mediumDataList.isEmpty()) {
            return MEDIUM_DATA_EMPTY;
        }

        validateMediumData(mediumDataList);
        format(mediumDataList);
        //数据校验
        int updatedRows = mediumDataService.updateMediumData(mediumDataList);
        if (updatedRows <= 0) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_FAIL_TO_SAVE_MEDIUM_DATA);
        }
        return SUCCESS;
    }

    /**
     * 过滤无权限编辑的媒体数据
     *
     * @param mediumDataArray
     * @param managingMediumAccountIds
     * @return
     */
    private List<MediumData> filter(MediumData[] mediumDataArray, List<Integer> managingMediumAccountIds) {
        List<MediumData> managingMediumData = new ArrayList<>();
        if (mediumDataArray != null && managingMediumAccountIds != null) {
            for (MediumData mediumData : mediumDataArray) {
                if (managingMediumAccountIds.contains(mediumData.getMediumAccountId())) {
                    managingMediumData.add(mediumData);
                }
            }
        }
        return managingMediumData;
    }

    /**
     * 检验媒体数据
     *
     * @param mediumDataArray
     */
    private void validateMediumData(List<MediumData> mediumDataArray) {
        if (mediumDataArray == null) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_MEDIUM_DATA_MISSING);
        }

        for (MediumData mediumData : mediumDataArray) {
            if (mediumData.getPlanId() == null || mediumData.getPlanSource() == null || mediumData.getDate() == null) {
                ExceptionHandler.throwParameterException(StatusCode.DELI_MEDIUM_DATA_PROPERTY_MISSING);
            }
            break;
        }
    }

    /**
     * 格式化媒体数据
     *
     * @param mediumDataArray
     * @throws ParseException
     */
    private void format(List<MediumData> mediumDataArray) throws ParseException {
        for (MediumData mediumData : mediumDataArray) {
            mediumData.setStatDate(DateUtils.parse(mediumData.getDate()));
            mediumData.setCost(CurrencyUtils.yuanToFen(mediumData.getCost()));
        }
    }

}
