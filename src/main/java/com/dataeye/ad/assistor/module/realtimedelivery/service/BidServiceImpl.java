package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.dictionaries.mapper.PlanMapper;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.BidStrategy;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.Medium;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.OpCode;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Bid;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLog;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by huangzehai on 2017/5/11.
 */
@Service("bidService")
public class BidServiceImpl implements BidService {

    private static final String UNSUPPORTED_MEDIUM = "不支持该媒体的出价操作";

    private static final String UNSUPPORTED_BID_STRATEGY = "不支持该媒体的出价策略%s";

    /**
     * 今日头条OCPC第二阶段
     */
    private static final String PHASE_2 = "2";

    /**
     * 今日头条OCPC第二阶段请求参数值
     */
    private static final int PHASE_2_PARAM_2 = 1;

    @Autowired
    private PlanMapper planMapper;

    @Autowired
    private OperationLogService operationLogService;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public Result bid(Bid bid) {
        //坚持是否支持出价策略
        if (!BidServices.isBidAvailable(bid.getMediumId(), bid.getBidStrategy())) {
            Result result = new Result();
            result.setSuccess(false);
            result.setMessage(String.format(UNSUPPORTED_BID_STRATEGY, bid.getBidStrategy()));
            return result;
        }

        //获取相应媒体的出价服务。
        BidService bidService = (BidService) applicationContext.getBean(BidServices.getBidService(bid.getMediumId()));
        //检查是否支持该媒体出价
        if (bidService == null) {
            //不支持该媒体的出价操作.
            Result result = new Result();
            result.setSuccess(false);
            result.setMessage(UNSUPPORTED_MEDIUM);
            return result;
        } else {
            //获取计划改动前的状态
            Plan oldPlan = planMapper.getPlanById(bid.getPlanId());
            if(bid.getMediumId() == Medium.BaiduFeedAds && oldPlan.getPlanExtend()!=null){
            	bid.setBidExtend(oldPlan.getPlanExtend());
            }
            Result result = bidService.bid(bid);
            if (result.isSuccess()) {
                //对于今日头条或者百度信息流推广OCPC出价的计划，当前处于第一阶段时修改第二阶段的价格时，不更新ADT的数据.
                if ((bid.getMediumId() == Medium.Toutiao || bid.getMediumId() == Medium.BaiduFeedAds) && StringUtils.equalsIgnoreCase(BidStrategy.OCPC, bid.getBidStrategy()) && bid.getIsCpaBid() != null && bid.getIsCpaBid() == PHASE_2_PARAM_2) {
                    Plan plan = planMapper.getPlanById(bid.getPlanId());
                    String oldPhase = BidStrategy.getBidPhase(bid.getMediumId(), plan.getPlanExtend());
                    if (plan != null && StringUtils.equalsIgnoreCase(PHASE_2, oldPhase)) {
                        updateBid(bid);
                    }
                } else {
                    updateBid(bid);
                }
            }

            //添加操作日志
            logging(bid, oldPlan, result);
            return result;
        }
    }

    /**
     *  添加操作日志
     * @param bid
     * @param oldPlan
     * @param result
     */
    private void logging(Bid bid, Plan oldPlan, Result result) {
        OperationLog log = new OperationLog();
        log.setCompanyId(bid.getCompanyId());
        log.setAccountId(bid.getAccountId());
        log.setPlanId(bid.getPlanId());
        log.setOpCode(OpCode.BID);
        log.setPreviousValue(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(new BigDecimal(oldPlan.getPlanBid()))));
        log.setCurrentValue(CurrencyUtils.yuanToText(bid.getBid()));
        log.setResult(result.isSuccess() ? 1 : 0);
        log.setOpTime(new Date());
        String msg = "实时操盘：";
        if (!result.isSuccess()) {
        	msg += result.getMessage();
        }
        log.setMessage(msg);
        operationLogService.addOperationLog(log);
    }

    /**
     * 切换ADT计划出价.
     *
     * @param bid
     * @return
     */
    private int updateBid(Bid bid) {
        Plan plan = new Plan();
        plan.setPlanId(bid.getPlanId());
        plan.setPlanBid(CurrencyUtils.yuanToFen(bid.getBid()).intValue());
        return planMapper.updateBid(plan);
    }
}
