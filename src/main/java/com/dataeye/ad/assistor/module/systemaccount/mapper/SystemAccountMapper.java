package com.dataeye.ad.assistor.module.systemaccount.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccount;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountSelectorVo;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;

/**
 * 系统账户映射器.
 * Created by luzhuyou
 */
@MapperScan
public interface SystemAccountMapper {

	/**
	 * 查询系统账号列表
	 * @param vo
	 * @return
	 */
    public List<SystemAccountVo> query(SystemAccountVo vo);

    /**
	 *  根据账号名称（邮箱）和公司ID获取账号信息
	 * @param vo
	 * @return
	 */
	public SystemAccountVo get(SystemAccountVo vo);

	/**
	 *  根据账号ID获取账号信息
	 * @param accountId 账号ID
	 * @return
	 */
	public SystemAccountVo getById(int accountId);
	
	/**
	 * 新增系统账号
	 * @param systemAccount
	 * @return
	 */
	public int add(SystemAccount systemAccount);

	/**
	 * 修改系统账号
	 * @param systemAccount
	 * @return
	 */
	public int update(SystemAccount systemAccount);

	/**
	 * 修改账号角色
	 * @param systemAccount
	 * @return
	 */
	public int updateAccountRole(SystemAccount systemAccount);

	/**
	 * 修改系统账号分组ID
	 * @param systemAccount
	 * @return
	 */
	public int updateGroupId(SystemAccount systemAccount);
	
	/**
	 * 重置已分组ID为未分组
	 * @param systemAccount
	 * @return
	 */
	public int resetGroupId(SystemAccount systemAccount);
	
	/**
	 * 修改系统账号状态
	 * @param systemAccount
	 * @return
	 */
	public int updateStatus(SystemAccount systemAccount);

	/**
	 * 修改系统账号密码
	 * @param systemAccount
	 * @return
	 */
	public int updatePassword(SystemAccount systemAccount);
	
	/**
	 * 下拉框查询系统账号
	 * @param systemAccountSelectorVo
	 * @return
	 */
    public List<SystemAccountSelectorVo> queryForSelector(SystemAccountSelectorVo systemAccountSelectorVo);

    /**
	 * 删除系统账号
	 * @param accountId
	 * @return
	 */
	public int delete(int accountId);

	/**
	 *  列出所有的系统账号
	 * @return
	 */
	List<SystemAccount> listSystemAccounts();

	/**
     * 修改登录后页面展示消息已提示次数
     *
     * @param systemAccount
     * @return
     * @author luzhuyou 2017/09/14
     */
	public int updatePromptTimes(SystemAccount systemAccount);

}
