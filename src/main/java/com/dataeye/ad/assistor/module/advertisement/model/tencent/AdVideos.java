package com.dataeye.ad.assistor.module.advertisement.model.tencent;

import com.google.gson.annotations.Expose;

/**
 * 视频
 * */
public class AdVideos {

	/**视频 id*/
	private String video_id;
	/**视频宽度，单位 px*/
	private Integer width;
	/**视频高度，单位 px*/
	private String height;
	/**视频帧数*/
	private Integer video_frames;
	/**视频帧率*/
	private Float video_fps;
	/**视频格式*/
	private String video_codec;
	/**视频码率，单位： b/s*/
	private Integer video_bit_rate;
	/**音频格式*/
	private String audio_codec;
	/**音频码率，单位： b/s*/
	private Integer audio_bit_rate;
	/**视频类型*/
	private String type;
	/**转码状态*/
	private String system_status;
	/**视频文件描述*/
	private String description;
	/**视频大小 单位 B(byte)*/
	@Expose
	private String file_size;
	/**视频文件签名，使用视频文件的 md5 值，用于检查上传视频文件的完整性*/
	private String signature;
	/**预览地址*/
	@Expose
	private String preview_url;
	public String getVideo_id() {
		return video_id;
	}
	public void setVideo_id(String video_id) {
		this.video_id = video_id;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public Integer getVideo_frames() {
		return video_frames;
	}
	public void setVideo_frames(Integer video_frames) {
		this.video_frames = video_frames;
	}
	public Float getVideo_fps() {
		return video_fps;
	}
	public void setVideo_fps(Float video_fps) {
		this.video_fps = video_fps;
	}
	public String getVideo_codec() {
		return video_codec;
	}
	public void setVideo_codec(String video_codec) {
		this.video_codec = video_codec;
	}
	public Integer getVideo_bit_rate() {
		return video_bit_rate;
	}
	public void setVideo_bit_rate(Integer video_bit_rate) {
		this.video_bit_rate = video_bit_rate;
	}
	public String getAudio_codec() {
		return audio_codec;
	}
	public void setAudio_codec(String audio_codec) {
		this.audio_codec = audio_codec;
	}
	public Integer getAudio_bit_rate() {
		return audio_bit_rate;
	}
	public void setAudio_bit_rate(Integer audio_bit_rate) {
		this.audio_bit_rate = audio_bit_rate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSystem_status() {
		return system_status;
	}
	public void setSystem_status(String system_status) {
		this.system_status = system_status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFile_size() {
		return file_size;
	}
	public void setFile_size(String file_size) {
		this.file_size = file_size;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getPreview_url() {
		return preview_url;
	}
	public void setPreview_url(String preview_url) {
		this.preview_url = preview_url;
	}
	@Override
	public String toString() {
		return "AdVideos [video_id=" + video_id + ", width=" + width
				+ ", height=" + height + ", video_frames=" + video_frames
				+ ", video_fps=" + video_fps + ", video_codec=" + video_codec
				+ ", video_bit_rate=" + video_bit_rate + ", audio_codec="
				+ audio_codec + ", audio_bit_rate=" + audio_bit_rate
				+ ", type=" + type + ", system_status=" + system_status
				+ ", description=" + description + ", file_size=" + file_size
				+ ", signature=" + signature + ", preview_url=" + preview_url
				+ "]";
	}
	
}
