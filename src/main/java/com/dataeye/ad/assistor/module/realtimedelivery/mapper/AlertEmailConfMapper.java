package com.dataeye.ad.assistor.module.realtimedelivery.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.realtimedelivery.model.AlertEmailConf;

/**
 * 警告邮件配置映射器
 * @author luzhuyou
 *
 */
@MapperScan
public interface AlertEmailConfMapper {
	public int insert(AlertEmailConf conf);
	
	public AlertEmailConf get(int id);
	
	public AlertEmailConf query();
	
	public int update(AlertEmailConf conf);
	
	public void delete(int id);
	
}
