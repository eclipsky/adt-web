package com.dataeye.ad.assistor.module.advertisement.model;


import com.google.gson.annotations.Expose;


/**
 * 广告创意规格
 * Created by ldj
 */
public class AdcreativeTemplate {

	/**广告创意规格id**/
	@Expose
    private Integer id;
	/**广告创意规格名称**/
	@Expose
    private String name;
	/**广告创意形式**/
	@Expose
    private String style;
	/**广告创意描述**/
	@Expose
    private String description;
	
    private String encodedId;
	/**广告创意尺寸*/
	@Expose
	private String size;
	
	/**广告创意站点*/
	@Expose
	private String site;
	/**商品类型*/
	private String productType;
	
	/**出价类型 1-CPC;4-CPM*/
	@Expose
	private String costType;
	
	/**广告版位*/
	@Expose
	private String position;
	
	/**创意元素*/
	@Expose
	private String element;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEncodedId() {
		return encodedId;
	}

	public void setEncodedId(String encodedId) {
		this.encodedId = encodedId;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getCostType() {
		return costType;
	}

	public void setCostType(String costType) {
		this.costType = costType;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	@Override
	public String toString() {
		return "AdcreativeTemplate [id=" + id + ", name=" + name + ", style="
				+ style + ", description=" + description + ", encodedId="
				+ encodedId + ", size=" + size + ", site=" + site
				+ ", productType=" + productType + ", costType=" + costType
				+ ", position=" + position + ", element=" + element + "]";
	}
	
	
	
}
