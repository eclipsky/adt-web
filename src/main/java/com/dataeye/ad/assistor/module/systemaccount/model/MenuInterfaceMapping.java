package com.dataeye.ad.assistor.module.systemaccount.model;

import java.util.Date;

/**
 * 菜单接口映射
 * @author luzhuyou 2017/02/21
 *
 */
public class MenuInterfaceMapping {
	/** 映射ID */
	private Integer mappingId;
	/** 菜单ID */
	private String menuId;
	/** 接口名称 */
	private String interfaceName;
	/** 接口URL */
	private String interfaceUrl;
	/** 数据权限:0-所有权限，1-负责人&关注人权限，2-负责人权限 */
	private Integer dataPermission;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private String updateTime;
	public Integer getMappingId() {
		return mappingId;
	}
	public void setMappingId(Integer mappingId) {
		this.mappingId = mappingId;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getInterfaceName() {
		return interfaceName;
	}
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}
	public String getInterfaceUrl() {
		return interfaceUrl;
	}
	public void setInterfaceUrl(String interfaceUrl) {
		this.interfaceUrl = interfaceUrl;
	}
	public Integer getDataPermission() {
		return dataPermission;
	}
	public void setDataPermission(Integer dataPermission) {
		this.dataPermission = dataPermission;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "MenuInterfaceMapping [mappingId=" + mappingId + ", menuId="
				+ menuId + ", interfaceName=" + interfaceName
				+ ", interfaceUrl=" + interfaceUrl + ", dataPermission="
				+ dataPermission + ", remark=" + remark + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}
	
}
