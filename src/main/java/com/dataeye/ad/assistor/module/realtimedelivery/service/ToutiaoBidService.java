package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.BidStrategy;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Bid;
import com.dataeye.ad.assistor.util.CookieUtils;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by huangzehai on 2017/5/11.
 */
@Service("toutiaoBidService")
public class ToutiaoBidService extends AbstractMediumPlanService<Bid> implements BidService {

    private static final String URL = "https://ad.toutiao.com/overture/ad/%d/update_bid/";

    private static final String BID_PARAMETER_NAME = "bid";
    private static final String IS_CPA_BID = "is_cpa_bid";
    private static final String CSRF_TOKEN = "csrftoken";
    /**
     * 成功消息。
     */
    private static final String SUCCESS = "修改出价成功";
    /**
     * 失败消息.
     */
    private static final String FAIL = "修改出价失败";
    /**
     * 成功状态.
     */
    private static final String STATUS_SUCCES = "success";

    @Override
    protected String url(String cookie, Long mediumPlanId) {
        return String.format(URL, mediumPlanId);
    }

    @Override
    protected List<NameValuePair> parameters(String cookie, Bid bid) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair(BID_PARAMETER_NAME, bid.getBid().setScale(2, RoundingMode.HALF_UP).toString()));
        if (StringUtils.equalsIgnoreCase(BidStrategy.OCPC, bid.getBidStrategy())) {
            params.add(new BasicNameValuePair(IS_CPA_BID, bid.getIsCpaBid().toString()));
        } else if (StringUtils.equalsIgnoreCase(BidStrategy.CPC, bid.getBidStrategy()) || StringUtils.equalsIgnoreCase(BidStrategy.CPM, bid.getBidStrategy()) || StringUtils.equalsIgnoreCase(BidStrategy.CPV, bid.getBidStrategy()) || StringUtils.equalsIgnoreCase(BidStrategy.CPA, bid.getBidStrategy())) {
            params.add(new BasicNameValuePair(IS_CPA_BID, "0"));
        }
        return params;
    }

    @Override
    protected void requestHeader(HttpRequestBase request, String cookie) {
        request.setHeader(HOST, "ad.toutiao.com");
        request.setHeader(ORIGIN, "https://ad.toutiao.com");
        request.setHeader(REFERER, "https://ad.toutiao.com/overture/data/campaign/ad/?campaign_id=59400913649");
        request.setHeader("X-CSRFToken", CookieUtils.get(cookie, CSRF_TOKEN));
    }

    @Override
    protected Result response(String content) {
        Gson gson = new Gson();
        Response response = gson.fromJson(content, Response.class);
        Result result = new Result();
        //Code为0是操作成功.
        if (StringUtils.equalsIgnoreCase(STATUS_SUCCES, response.getStatus())) {
            result.setSuccess(true);
            result.setMessage(SUCCESS);
        } else {
            result.setSuccess(false);
            result.setMessage(FAIL);
        }
        return result;
    }

    @Override
    public Result bid(Bid bid) {
        return execute(bid);
    }

    /**
     * 响应
     */
    private class Response {
        private String status;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        private class Data {
            private BigDecimal bid;
            @SerializedName("ad_id")
            private long adId;

            public BigDecimal getBid() {
                return bid;
            }

            public void setBid(BigDecimal bid) {
                this.bid = bid;
            }

            public long getAdId() {
                return adId;
            }

            public void setAdId(long adId) {
                this.adId = adId;
            }

            @Override
            public String toString() {
                return "Data{" +
                        "bid=" + bid +
                        ", adId=" + adId +
                        '}';
            }
        }
    }
}
