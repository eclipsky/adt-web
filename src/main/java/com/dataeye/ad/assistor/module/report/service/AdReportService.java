package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.module.report.model.*;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * Created by huangzehai on 2016/12/27.
 */
public interface AdReportService {

    /**
     * 投放日报-计划维度-汇总数据
     *
     * @param query 查询条件
     * @return
     */
    PageData planSummaryReport(DeliveryReportQuery query);

    /**
     * 投放日报-计划维度-汇总数据-详细
     *
     * @param query 查询条件
     * @return
     */
    List<DeliveryReportView> planSummaryReportDetail(DeliveryReportQuery query);

    /**
     * 投放日报-计划维度-按天显示数据-汇总
     *
     * @param query 查询条件
     * @return
     */
    PageData planDailyReport(DeliveryReportQuery query);

    /**
     * 投放日报-计划维度-按天数据-详细
     *
     * @param query 查询条件
     * @return
     */
    List<DeliveryReportView> planDailyReportDetail(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据
     *
     * @param query 查询条件
     * @return
     */
    PageData mediumSummaryReport(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-展开日期
     *
     * @param query 查询条件
     * @return
     */
    List<DeliveryReportView> mediumSummaryReportExpandDate(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-按天显示数据-汇总
     *
     * @param query 查询条件
     * @return
     */
    PageData mediumDailyReport(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-按天显示数据-展开媒体
     *
     * @param query 查询条件
     * @return
     */
    List<DeliveryReportView> mediumDailyReportExpandMedium(DeliveryReportQuery query);

    /**
     * 获取投放日报-媒体维度-按天显示数据-展开活动组时返回的数据。
     *
     * @param query 查询条件
     * @return
     */
    List<DeliveryReportView> mediumDailyReportExpandEventGroup(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-按天显示数据-展开账号计划。
     *
     * @param query 查询条件
     * @return
     */
    InnerTable mediumDailyReportExpandAccountAndPlan(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-展开活动组
     *
     * @param query 查询条件
     * @return
     */
    List<DeliveryReportView> mediumSummaryReportExpandEventGroup(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-展开账号和计划
     *
     * @param query 查询条件
     * @return
     */
    InnerTable mediumSummaryReportExpandAccountAndPlan(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-展开活动组-不展开日期
     *
     * @param query 查询条件
     * @return
     */
    List<DeliveryReportView> mediumSummaryReportExpandEventGroupNotDate(DeliveryReportQuery query);

    /**
     * 投放日报-媒体维度-汇总数据-展开账号和计划-不展开日期
     *
     * @param query 查询条件
     * @return
     */
    InnerTable mediumSummaryReportExpandAccountAndPlanNotDate(DeliveryReportQuery query);

    /**
     * 投放日报汇总
     *
     * @param view      查看
     * @param dimension
     * @param query
     * @return
     */
    PageData adReport(View view, Dimension dimension, DeliveryReportQuery query);

    /**
     * 投放日报详细.
     *
     * @param view      查看
     * @param dimension 维度
     * @param query     查询条件
     * @return
     */
    Object adReportDetail(View view, Dimension dimension, LinkID link, DeliveryReportQuery query);

    /**
     * 生成报告Excel.
     *
     * @param reports    投放日报列表
     * @param reportType 报告类型
     * @param accountId  系统账号ID
     * @return
     */
    Workbook toWorkBook(List<DeliveryReportView> reports, ReportType reportType, int accountId);
    
    /**
     * 投放日报子账户Tab
     *
     * @param query
     * @return
     */
    PageData queryindicatorByAccount(DeliveryReportQuery query);
}
