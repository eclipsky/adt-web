package com.dataeye.ad.assistor.module.result.model;

import java.util.List;

/**
 * Created by huangzehai on 2017/4/10.
 */
public class ContrastiveAnalysisQuery extends CoreIndicatorQuery {
    /**
     * 优化师账号列表
     */
    private List<String> optimizers;
    /**
     * 媒体ID.
     */
    private List<Integer> mediumIds;

    /**
     * 产品ID.
     */
    private List<Integer> productIds;

    public List<String> getOptimizers() {
        return optimizers;
    }

    public void setOptimizers(List<String> optimizers) {
        this.optimizers = optimizers;
    }

    public List<Integer> getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(List<Integer> mediumIds) {
        this.mediumIds = mediumIds;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

}
