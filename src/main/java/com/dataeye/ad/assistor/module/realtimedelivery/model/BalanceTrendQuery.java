package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/27.
 */
public class BalanceTrendQuery extends DataPermissionDomain {
    private Date startDate;
    private Date endDate;
    private List<Integer> mediumIds;
    private List<Integer> mediumAccountIds;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Integer> getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(List<Integer> mediumIds) {
        this.mediumIds = mediumIds;
    }

    public List<Integer> getMediumAccountIds() {
        return mediumAccountIds;
    }

    public void setMediumAccountIds(List<Integer> mediumAccountIds) {
        this.mediumAccountIds = mediumAccountIds;
    }
}
