package com.dataeye.ad.assistor.module.advertisement.service.tencent;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.InterfaceConstants.TencentInterface;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.HttpUtil;
import com.dataeye.ad.assistor.util.RandomUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;


/**
 * 广点通API
 * 广告Service
 * */
@Service("adsService")
public class AdsService {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AdsService.class);

	
	/**
	 * 创建广告
	 * @param account_id(必填)   
	 * @param adgroup_id(必填)
	 * @param adcreative_id(必填)
	 * @param ad_name(必填)
	 * @param configured_status
	 * @param impression_tracking_url
	 * @param click_tracking_url
	 * @param feeds_interaction_enabled 
	 * @return ad_id  广告 id
	 */
	public int add(Integer account_id,Integer adgroup_id,Integer adcreative_id,String ad_name,String impression_tracking_url,String click_tracking_url,String feeds_interaction_enabled,String accessToken) {
		validateParamsByAdd(account_id, accessToken, adgroup_id, adcreative_id);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("adgroup_id", adgroup_id+"");
		params.put("adcreative_id", adcreative_id+"");
		params.put("ad_name", ad_name);
		
		
		if(StringUtils.isNotBlank(impression_tracking_url)){
			params.put("impression_tracking_url", impression_tracking_url);
			
		}
		if(StringUtils.isNotBlank(click_tracking_url)){
			params.put("click_tracking_url", click_tracking_url);
			
		}
		if(StringUtils.isNotBlank(feeds_interaction_enabled)){
			params.put("feeds_interaction_enabled", feeds_interaction_enabled);
		}
		
		String addAdsInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADS_ADD;
		String addAdsInterfaceUrl = HttpUtil.urlParamReplace(addAdsInterface,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(addAdsInterfaceUrl, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>创建广告API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{addAdsInterfaceUrl, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>创建广告API请求数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{addAdsInterfaceUrl, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        
        JsonObject data = jsonObject.get("data").getAsJsonObject();    	
        return data.get("ad_id").getAsInt();
	}
	
	/**
	 * 删除广告
	 * @param account_id（必填）
	 * @param ad_id（必填）
	 * @return
	 */
	public void delete(Integer account_id,Integer ad_id,String accessToken) {
		validateParamsByAccount(account_id, accessToken);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("ad_id", ad_id+"");
		
		String deleteUrl = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADS_DELETE;
		String deleteAdGroupInterface = HttpUtil.urlParamReplace(deleteUrl,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(deleteAdGroupInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>删除广告API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{deleteAdGroupInterface, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>删除广告API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{deleteAdGroupInterface, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
	}
	
	//参数校验
			private void validateParamsByAccount(Integer account_id,String accessToken){
				if (account_id == null || account_id == 0) {
		            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCOUNT_ID_IS_NULL);
		        }
		        if (StringUtils.isBlank(accessToken)) {
		            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCESSTOKEN_ID_IS_NULL);
		        }
			}
			
			private void validateParamsByAdd(Integer account_id,String accessToken,Integer adgroup_id,Integer adcreative_id){
				validateParamsByAccount(account_id, accessToken);
				if (adgroup_id == null || adgroup_id == 0) {
		            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_ID_IS_NULL);
		        }
				if (adcreative_id == null || adcreative_id == 0) {
		            ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_ID_IS_NULL);
		        }

			}
	
}
