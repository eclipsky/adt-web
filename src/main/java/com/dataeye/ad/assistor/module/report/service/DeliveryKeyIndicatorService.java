package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.module.report.model.DeliveryKeyIndicator;
import com.dataeye.ad.assistor.module.report.model.DeliveryKeyIndicatorQuery;
import com.dataeye.ad.assistor.module.result.model.IndicatorWithId;

import java.util.List;

/**
 * Created by huangzehai on 2017/5/23.
 */
public interface DeliveryKeyIndicatorService {
    /**
     * 获取投放关键指标汇总数据
     *
     * @param query
     * @return
     */
    DeliveryKeyIndicator getDeliveryKeyIndicatorTotal(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getCostGroupByProduct(DeliveryKeyIndicatorQuery coreIndicatorQuery);

    List<IndicatorWithId> getCostGroupByMedium(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getDownloadsGroupByProduct(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getDownloadsGroupByMedium(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getActivationsGroupByProduct(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getActivationsGroupByMedium(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getRegistrationsGroupByProduct(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getRegistrationsGroupByMedium(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getDauGroupByProduct(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getDauGroupByMedium(DeliveryKeyIndicatorQuery query);
}
