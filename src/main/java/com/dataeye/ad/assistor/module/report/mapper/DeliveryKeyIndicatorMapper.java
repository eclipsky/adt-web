package com.dataeye.ad.assistor.module.report.mapper;

import com.dataeye.ad.assistor.module.report.model.DeliveryKeyIndicatorQuery;
import com.dataeye.ad.assistor.module.result.model.IndicatorWithId;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by huangzehai on 2017/5/23.
 */
public interface DeliveryKeyIndicatorMapper {
    /**
     * 累计消耗。
     *
     * @param query
     * @return
     */
    BigDecimal totalCost(DeliveryKeyIndicatorQuery query);

    /**
     * 累计充值
     *
     * @param query
     * @return
     */
    Integer totalDownloads(DeliveryKeyIndicatorQuery query);

    /**
     * 累计活跃数
     *
     * @param query
     * @return
     */
    Integer totalActivations(DeliveryKeyIndicatorQuery query);

    /**
     * 累计注册数
     *
     * @param query
     * @return
     */
    Integer totalRegistrations(DeliveryKeyIndicatorQuery query);

    /**
     * DAU.
     *
     * @param query
     * @return
     */
    Integer dau(DeliveryKeyIndicatorQuery query);

    /**
     * 按产品分组统计消耗.
     *
     * @param query
     * @return
     */
    List<IndicatorWithId> getCostGroupByProduct(DeliveryKeyIndicatorQuery query);


    /**
     * 按媒体分组统计消耗.
     *
     * @param query
     * @return
     */
    List<IndicatorWithId> getCostGroupByMedium(DeliveryKeyIndicatorQuery query);


    List<IndicatorWithId> getDownloadsGroupByProduct(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getDownloadsGroupByMedium(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getActivationsGroupByProduct(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getActivationsGroupByMedium(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getRegistrationsGroupByProduct(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getRegistrationsGroupByMedium(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getDauGroupByProduct(DeliveryKeyIndicatorQuery query);

    List<IndicatorWithId> getDauGroupByMedium(DeliveryKeyIndicatorQuery query);

}
