package com.dataeye.ad.assistor.module.realtimedelivery.constant;

/**
 * Created by huangzehai on 2017/5/17.
 */
public class Medium {
    /**
     * 今日头条.
     */
    public static final int Toutiao = 2;
    /**
     * 智汇推
     */
    public static final int Tui = 3;
    /**
     * UC头条
     */
    public static final int Uc = 9;


    /**
     * 广点通.
     */
    public static final int Tencent = 13;

    /**
     * 百度信息流推广.
     */
    public static final int BaiduFeedAds = 14;
    
    /**
     * 新浪超级粉丝通
     */
    public static final int AdWeiboNew = 25;

}
