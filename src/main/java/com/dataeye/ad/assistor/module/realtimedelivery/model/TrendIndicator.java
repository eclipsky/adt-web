package com.dataeye.ad.assistor.module.realtimedelivery.model;

import com.dataeye.ad.assistor.module.report.constant.Indicators;
import org.apache.commons.lang.StringUtils;

/**
 * 趋势指标
 * Created by huangzehai on 2017/2/22.
 */
public enum TrendIndicator {
    /**
     * 消耗
     */TotalCost(Indicators.COST, "消耗(￥)"),
    /**
     * 曝光
     */
    Exposures(Indicators.SHOW_NUM, "曝光"),
    /**
     * 点击数
     */Clicks(Indicators.CLICK_NUM, "媒体点击数"),
    /**
     * 点击通过率(
     **/
    CTR(Indicators.CTR, "CTR"),
    /**
     * 下载数
     */
    Downloads(Indicators.DOWNLOAD_TIMES, "下载数"),
    /**
     * 下载率
     */
    DownloadRate(Indicators.DOWNLOAD_RATE, "下载率"),
    /**
     * 激活
     */
    Activations(Indicators.ACTIVE_NUM, "激活数"),
    /**
     * 激活率.
     */
    ActivationRate(Indicators.ACTIVE_RATE, "点击激活率"),
    /**
     * 激活CPA.
     */
    ActivationCPA(Indicators.ACTIVE_CPA, "激活CPA"),
    /**
     * 注册
     */
    Registrations(Indicators.REGISTRATIONS_FD, "首日注册设备数"),
    /**
     * 注册率
     */
    RegistrationRate(Indicators.REGISTER_RATE, "点击注册率"),
    /**
     * CPA
     */
    CPA(Indicators.REGISTER_CPA, "首日注册CPA（设备）");


    /**
     * 指标ID.
     */
    private String indicatorId;

    /**
     * 指标Label.
     */
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(String indicatorId) {
        this.indicatorId = indicatorId;
    }

    TrendIndicator(String indicatorId, String label) {
        this.indicatorId = indicatorId;
        this.label = label;
    }

    public static TrendIndicator parse(String text) {
        for (TrendIndicator indicator : TrendIndicator.values()) {
            if (StringUtils.equalsIgnoreCase(indicator.name(), text)) {
                return indicator;
            }
        }
        return null;
    }
}
