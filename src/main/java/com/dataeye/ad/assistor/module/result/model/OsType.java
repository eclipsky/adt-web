package com.dataeye.ad.assistor.module.result.model;

/**
 * Created by huangzehai on 2017/4/11.
 */
public enum OsType {
    Others(0), iOS(1), Android(2);
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    OsType(int value) {
        this.value = value;
    }

    public static OsType parse(int value) {
        for (OsType osType : OsType.values()) {
            if (osType.getValue() == value) {
                return osType;
            }
        }
        return null;
    }
}
