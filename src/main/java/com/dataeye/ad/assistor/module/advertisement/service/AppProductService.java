package com.dataeye.ad.assistor.module.advertisement.service;

import com.dataeye.ad.assistor.module.advertisement.mapper.ApiTxAppProductMapper;
import com.dataeye.ad.assistor.module.advertisement.model.AdProductVO;
import com.dataeye.ad.assistor.module.advertisement.model.MarketingApiAuth;
import com.dataeye.ad.assistor.module.advertisement.service.tencent.AdProductService;
import com.dataeye.ad.assistor.util.RandomUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.lang.model.element.Element;

/**
 * @author lingliqi
 * @date 2017-12-23 11:22
 */
@Service("appProductService")
public class AppProductService {

    @Autowired
    private ApiTxAppProductMapper apiTxAppProductMapper;
    @Autowired
    private AdProductService adProductService;

    @Autowired
    private ApiTxAdGroupService apiTxAdGroupService;

    /**
     * 查询推广APP信息
     *
     * @param mediumAccountId
     * @param productType
     * @param productRefsId
     * @return
     */
    public AdProductVO queryProduct(Integer mediumAccountId, String productType, String productRefsId,String appId) {
    	AdProductVO paramsVo = new AdProductVO();
    	paramsVo.setAppId(appId);
    	paramsVo.setMediumAccountId(mediumAccountId);
    	paramsVo.setProductRefsId(productRefsId);
    	paramsVo.setProductType(productType);
        List<AdProductVO> adProductList = apiTxAppProductMapper.query(paramsVo);
        AdProductVO adProductVO = new AdProductVO();
        // 数据库不存在该APP信息，先登记后拉取
        if (adProductList.isEmpty()) {
            MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(mediumAccountId);
            int mAccountId = 0;
            String accessToken = "";
            final String productName = "ADT";
            if (tAccountVo != null) {
                mAccountId = tAccountVo.getAccountId();
                accessToken = tAccountVo.getAccessToken();
                Integer refsId = adProductService.add(mAccountId, productName, productType, productRefsId + "", accessToken);
                if (null != refsId && refsId > 0) {
                    adProductVO = adProductService.query(mAccountId, productType, refsId + "", accessToken);
                    adProductVO.setCreateTime(new Date());
                    adProductVO.setUpdateTime(new Date());
                    adProductVO.setMediumAccountId(mediumAccountId);
                    apiTxAppProductMapper.add(adProductVO);
                }
            }
        }else{
        	adProductVO = adProductList.get(0);
        }
        return adProductVO;
    }

}
