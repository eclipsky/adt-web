package com.dataeye.ad.assistor.module.mediumaccount.model;

import com.google.gson.annotations.Expose;

/**
 * 出价结果
 * Created by huangzehai on 2017/5/11.
 */
public class Result {
    @Expose
    private boolean success;
    @Expose
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "BidResult{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
