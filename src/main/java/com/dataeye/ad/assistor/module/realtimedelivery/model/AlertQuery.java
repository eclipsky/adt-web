package com.dataeye.ad.assistor.module.realtimedelivery.model;

/**
 * Created by huangzehai on 2017/2/28.
 */
public class AlertQuery {
    /**
     * 计划ID.
     */
    private Long planId;
    /**
     * 告警类型.
     */
    private AlertType alertType;

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public AlertType getAlertType() {
        return alertType;
    }

    public void setAlertType(AlertType alertType) {
        this.alertType = alertType;
    }

    public AlertQuery(Long planId, AlertType alertType) {
        this.planId = planId;
        this.alertType = alertType;
    }

    @Override
    public String toString() {
        return "AlertQuery{" +
                "planId=" + planId +
                ", alertType=" + alertType +
                '}';
    }
}
