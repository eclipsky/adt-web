package com.dataeye.ad.assistor.module.advertisement.service.tencent;



import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ProductType;
import com.dataeye.ad.assistor.module.advertisement.constants.InterfaceConstants.TencentInterface;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.HttpUtil;
import com.dataeye.ad.assistor.util.RandomUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;


/**
 * 广点通API
 * 广告主Service
 * */
@Service("advertiserService")
public class AdvertiserService {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AdvertiserService.class);

	/**
	 * 获取广告主
	 * @param account_id
	 * @param Advertiser_id
	 * @param Advertiser_name  
	 * @return 
	 */
	/*public List<PlanGroupVO> query(Integer account_id,String accessToken) {
		//参数校验
		validateParamsByAccount(account_id, accessToken);
		// 1.构建请求参数
		String Tencent_params= StringUtils.substring(TencentInterface.TENCENT_PARAMETERS, 1);
		String params1 = HttpUtil.urlParamReplace(Tencent_params,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));

		StringBuilder params = new StringBuilder("");
		params.append(params1);
		params.append("&account_id="+account_id);                                                                                                                                                                               
		String getAdvertiserInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADVERTISER_GET;
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.get(getAdvertiserInterface,params.toString());
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>查询广告主API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{getAdvertiserInterface, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>查询广告主API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{getAdvertiserInterface, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
    	JsonObject data=jsonObject.get("data").getAsJsonObject();

        return result;
	}*/
	
	
	/**
	 * 更新广告主
	 * @param account_id（必填）
	 * @param daily_budget 日消耗限额，单位为分，微信朋友圈广告（ Advertiser_type = Advertiser_TYPE_WECHAT_MOMENTS ）不可使用，其他广告可使用。朋友圈广告不受帐号日限额影响和控制\
	 * 预算需介于 5,000 分-1,000,000,000 分之间（ 50 元-10,000,000 元，单位为人民币）；
	 * 每次修改幅度不能低于帐号今日消耗加上 5,000 分（ 50 元，单位为人民币）；
	 * 每次修改幅度不能低于 5,000 分（ 50 元，单位为人民币）；每天每帐号最多修改 5 次；
	 */
	public void update(Integer account_id,Integer daily_budget,String accessToken) {
		validateParamsByAdvertiserId(account_id, accessToken, daily_budget);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		if(daily_budget != null ){
			params.put("daily_budget", daily_budget+"");
		}
		String updateAdvertiserInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADVERTISER_UPDATE;
		String updateAdvertiserInterfaceUrl = HttpUtil.urlParamReplace(updateAdvertiserInterface,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(updateAdvertiserInterfaceUrl, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>更新广告主API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{updateAdvertiserInterfaceUrl, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>更新广告主API请求数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{updateAdvertiserInterfaceUrl, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
	}
	
	
	
	//参数校验
	private void validateParamsByAccount(Integer account_id,String accessToken){
		if (account_id == null || account_id == 0) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCOUNT_ID_IS_NULL);
        }
        if (StringUtils.isBlank(accessToken)) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCESSTOKEN_ID_IS_NULL);
        }
	}
	
	private void validateParamsByAdd(Integer account_id,String accessToken,String Advertiser_name,String product_type){
		validateParamsByAccount(account_id, accessToken);
		if (StringUtils.isBlank(Advertiser_name)) {
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_NAME_IS_NULL);
        }
		if (StringUtils.isBlank(product_type) || StringUtils.isBlank(ProductType.valueOf(product_type).name())) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
        }
	}
	
	private void validateParamsByAdvertiserId(Integer account_id,String accessToken,Integer daily_budget){
		validateParamsByAccount(account_id, accessToken);
		//预算需介于 5,000 分-1,000,000,000 分之间
		if (daily_budget == null || daily_budget < 5000 || daily_budget >1000000000 ) {
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_DAILY_BUDGET_IS_NULL);
        }
	}
	
	

}
