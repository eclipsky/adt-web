package com.dataeye.ad.assistor.module.debugcenter.model;

import com.google.gson.annotations.Expose;

/**
 * @author lingliqi
 * @date 2018-02-01 15:02
 */
public class ChannelDebugDevice {

    /**
     * 测试设备名称
     */
    @Expose
    private String deviceName;

    /**
     * 测试设备号
     * <p>
     * android - IMEI | ios - IDFA
     */
    @Expose
    private String deviceId;

    /**
     * 主账号id
     */
    private Integer uid;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }
}
