package com.dataeye.ad.assistor.module.advertisement.service.tencent;


import java.util.Date;
import java.util.List;




import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.FundStatus;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.FundType;
import com.dataeye.ad.assistor.module.advertisement.constants.InterfaceConstants.TencentInterface;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.AdFund;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.HttpUtil;
import com.dataeye.ad.assistor.util.RandomUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


/**
 * 广点通API
 * 资金账户Service
 * */
@Service("adFundService")
public class AdFundService {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AdFundService.class);

	/**
	 * 获取资金账户信息
	 * @param account_id
	 * @return 
	 */
	public AdFund query(Integer account_id,String accessToken) {
		//参数校验
		validateParamsByAccount(account_id, accessToken);
		// 1.构建请求参数
		String Tencent_params= StringUtils.substring(TencentInterface.TENCENT_PARAMETERS, 1);
		String params1 = HttpUtil.urlParamReplace(Tencent_params,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));

		StringBuilder params = new StringBuilder("");
		params.append(params1);
		params.append("&account_id="+account_id);
		                                                                                                                                                                                  
		String getFundInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.FUND_GET;
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.get(getFundInterface,params.toString());
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>获取资金账户信息API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{getFundInterface, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>获取资金账户信息API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{getFundInterface, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
    	JsonObject data=jsonObject.get("data").getAsJsonObject();
    	JsonArray dataJsonObject = data.get("list").getAsJsonArray();
    	List<AdFund>  response = StringUtil.gson.fromJson(dataJsonObject, new TypeToken<List<AdFund>>(){}.getType());
    	AdFund fVo = new AdFund();
   	//得到计划信息
    	for (AdFund fundVo : response) {
    		if(fundVo.getFund_type().equals(FundType.GENERAL_CASH.name())){  // 现金账户
    			fVo.setBalance(fundVo.getBalance());
    			fVo.setFund_status(fundVo.getFund_status());
    			fVo.setFund_type(fundVo.getFund_type());
    			fVo.setRealtime_cost(fundVo.getRealtime_cost());
    		}
		}
        return fVo;
	}
	
	
	//参数校验
	private void validateParamsByAccount(Integer account_id,String accessToken){
		if (account_id == null || account_id == 0) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCOUNT_ID_IS_NULL);
        }
        if (StringUtils.isBlank(accessToken)) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCESSTOKEN_ID_IS_NULL);
        }
	}

}
