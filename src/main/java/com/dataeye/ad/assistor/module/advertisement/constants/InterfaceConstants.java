/**
 *
 */
package com.dataeye.ad.assistor.module.advertisement.constants;

/**
 * @author ldj
 */
public class InterfaceConstants {


    /**
     * 广点通广告投放相关接口
     */
    public static class TencentInterface {
        public static final String TENCENT_URL_KEY = "tencentUrl";
        public static final String TENCENT_URL = "https://api.e.qq.com/v1.0/";
        public static final String TENCENT_PARAMETERS = "?access_token={0}&timestamp={1}&nonce={2}";

        /**
         * 推广计划相关接口地址
         */
        public static final String CAMPAIGN_ADD = "campaigns/add" + TENCENT_PARAMETERS;
        public static final String CAMPAIGN_UPDATE = "campaigns/update" + TENCENT_PARAMETERS;
        public static final String CAMPAIGN_GET = "campaigns/get";
        public static final String CAMPAIGN_DELETE = "campaigns/delete" + TENCENT_PARAMETERS;
        /**
         * 广告组相关接口地址
         */
        public static final String ADGROUPS_ADD = "adgroups/add" + TENCENT_PARAMETERS;
        public static final String ADGROUPS_UPDATE = "adgroups/update" + TENCENT_PARAMETERS;
        public static final String ADGROUPS_GET = "adgroups/get";
        public static final String ADGROUPS_DELETE = "adgroups/delete" + TENCENT_PARAMETERS;

        /**
         * 广告创意相关接口地址
         */
        public static final String ADCREATIVE_ADD = "adcreatives/add" + TENCENT_PARAMETERS;
        public static final String ADCREATIVE_UPDATE = "adcreatives/update" + TENCENT_PARAMETERS;
        public static final String ADCREATIVE_GET = "adcreatives/get";
        public static final String ADCREATIVE_DELETE = "adcreatives/delete" + TENCENT_PARAMETERS;

        /**
         * 广告（Ad）相关接口地址
         */
        public static final String ADS_ADD = "ads/add" + TENCENT_PARAMETERS;
        public static final String ADS_UPDATE = "ads/update" + TENCENT_PARAMETERS;
        public static final String ADS_GET = "ads/get";
        public static final String ADS_DELETE = "ads/delete" + TENCENT_PARAMETERS;

        /**
         * 图片相关接口地址
         */
        public static final String IMAGES_ADD = "images/add" + TENCENT_PARAMETERS;
        public static final String IMAGES_GET = "images/get";

        /**
         * 定向（Targeting）相关接口
         */
        public static final String TARGETING_ADD = "targetings/add" + TENCENT_PARAMETERS;
        public static final String TARGETING_UPDATE = "targetings/update" + TENCENT_PARAMETERS;
        public static final String TARGETING_GET = "targetings/get";
        public static final String TARGETING_DELETE = "targetings/delete" + TENCENT_PARAMETERS;

        /**
         * 视频相关接口地址
         */
        public static final String VIDEOS_ADD = "videos/add" + TENCENT_PARAMETERS;
        public static final String VIDEOS_GET = "videos/get";

        /**
         * 资金账户相关接口地址
         */
        public static final String FUND_GET = "funds/get";

        /**
         * 推广标的物接口地址
         */
        public static final String PRODUCT_GET = "products/get";
        public static final String PRODUCT_ADD = "products/add" + TENCENT_PARAMETERS;

        /**
         * 广告帐号 的物接口地址
         */
        public static final String ADVERTISER_GET = "advertiser/get";
        public static final String ADVERTISER_UPDATE = "advertiser/update" + TENCENT_PARAMETERS;

    }

}
