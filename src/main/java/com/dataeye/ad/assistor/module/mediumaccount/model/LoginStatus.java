package com.dataeye.ad.assistor.module.mediumaccount.model;

/**
 * Created by huangzehai on 2017/5/10.
 */
public enum LoginStatus {
    Login(0), LogOut(1), Loading(2), MobileVerification(3), WrongPassword(4);
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    LoginStatus(int value) {
        this.value = value;
    }

    public static LoginStatus parse(int value) {
        for (LoginStatus loginStatus : LoginStatus.values()) {
            if (loginStatus.getValue() == value) {
                return loginStatus;
            }
        }
        return null;
    }
}
