package com.dataeye.ad.assistor.module.stat.model;

import java.util.List;

/**
 * 历史计划关联关系VO
 * @author luzhuyou 2017/08/03
 *
 */
public class PlanStatHisReqVo {
	
	/**
	 * 统计日期
	 */
	private List<String> statDates;
	/**
	 * 计划ID
	 */
	private int planId;
	/**
	 * 公司ID
	 */
	private int companyId;
	/**
	 * 落地页ID
	 */
	private int pageId;
	/**
	 * 包ID
	 */
	private int pkgId;
	public List<String> getStatDates() {
		return statDates;
	}
	public void setStatDates(List<String> statDates) {
		this.statDates = statDates;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getPageId() {
		return pageId;
	}
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	public int getPkgId() {
		return pkgId;
	}
	public void setPkgId(int pkgId) {
		this.pkgId = pkgId;
	}
	@Override
	public String toString() {
		return "PlanStatHisReqVo [planId=" + planId + ", companyId="
				+ companyId + ", pageId=" + pageId + ", pkgId=" + pkgId + "]";
	}
	
}
