package com.dataeye.ad.assistor.module.report.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.Date;
import java.util.List;

/**
 * Created by huangzeahai on 2017/5/4.
 */
public class PlanQuery extends DataPermissionDomain {
    /**
     * 起始日期.
     */
    private Date startDate;
    /**
     * 截止日期.
     */
    private Date endDate;

    /**
     * 媒体ID.
     */
    private List<Integer> productIds;

    /**
     * 媒体账号ID
     */
    private List<Integer> mediumAccountIds;
    /**
     * 计划ID
     */
    private List<Integer> mediumIds;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public List<Integer> getMediumAccountIds() {
        return mediumAccountIds;
    }

    public void setMediumAccountIds(List<Integer> mediumAccountIds) {
        this.mediumAccountIds = mediumAccountIds;
    }

    public List<Integer> getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(List<Integer> mediumIds) {
        this.mediumIds = mediumIds;
    }

    @Override
    public String toString() {
        return "PlanQuery{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", productIds=" + productIds +
                ", mediumAccountIds=" + mediumAccountIds +
                ", mediumIds=" + mediumIds +
                "} " + super.toString();
    }
}
