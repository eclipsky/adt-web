package com.dataeye.ad.assistor.module.report.model;

import org.apache.commons.lang.StringUtils;

/**
 * Created by lenovo on 2017/1/4.
 */
public enum Action {
    Detail, Download;

    public static Action parse(String text) {
        for (Action action : Action.values()) {
            if (StringUtils.equalsIgnoreCase(action.name(), text)) {
                return action;
            }
        }
        return null;
    }
}
