package com.dataeye.ad.assistor.module.advertisement.service.tencent;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.InterfaceConstants;
import com.dataeye.ad.assistor.module.advertisement.model.AdProductVO;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.Product;
import com.dataeye.ad.assistor.util.*;
import com.google.gson.JsonObject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lingliqi
 * @date 2017-12-23 11:52
 */
@Service("adProductService")
public class AdProductService {

    private static final Logger logger = LoggerFactory.getLogger(AdProductService.class);

    private final static int CODE_PRODUCT_EXIST = 32710;

    private final static int CODE_PRODUCT_NOT_FOUND = 32706;

    private final static int CODE_SUB_PRODUCT_NOT_FOUND = 32708;

    private final static int CODE_PRODUCT_ADD_TIMES_TOO_MANY = 32702;

    public Integer add(Integer account_id, String product_name, String product_type, String product_refs_id, String access_token) {
        // 1.构建请求参数
        Map<String, String> params = new HashMap<String, String>();
        params.put("account_id", account_id + "");
        params.put("product_name", product_name);
        params.put("product_type", product_type);
        params.put("product_refs_id", product_refs_id);
        String addProductInterface = ConfigHandler.getProperty(
                InterfaceConstants.TencentInterface.TENCENT_URL_KEY, InterfaceConstants.TencentInterface.TENCENT_URL) +
                InterfaceConstants.TencentInterface.PRODUCT_ADD;
        String addProductInterfaceUrl = HttpUtil.urlParamReplace(addProductInterface,
                access_token, DateUtils.getSecondTimestamp(new Date()) + "", RandomUtils.getRandomString(32));
        // 2.HTTP请求获取数据
        String httpResponse = HttpRequest.post(addProductInterfaceUrl, params);
        if (StringUtils.isBlank(httpResponse)) {
            logger.error("<Tencent>请求创建推广标的物API失败！请求url[{}]，具体请求参数[{}]", new Object[]{addProductInterfaceUrl, params});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        // 3.结果数据封装，转换为Map
        JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
        int code = jsonObject.get("code").getAsInt();
        // 标的物已存在,直接返回输入参数product_refs_id
        if (code == CODE_PRODUCT_EXIST) {
            return Integer.parseInt(product_refs_id);
        } else if (code == CODE_PRODUCT_NOT_FOUND || code == CODE_SUB_PRODUCT_NOT_FOUND || code == CODE_PRODUCT_ADD_TIMES_TOO_MANY) {
            // 标的物ID无效
            return null;
        } else if (code != 0) {
            logger.error("<Tencent>请求创建推广标的物API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]",
                    new Object[]{addProductInterfaceUrl, params, code, jsonObject});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        String data = jsonObject.get("data").toString();
        JsonObject dataJsonObject = StringUtil.jsonParser.parse(data).getAsJsonObject();
        return dataJsonObject.get("product_refs_id").getAsInt();

    }

    public AdProductVO query(Integer account_id, String product_type, String product_refs_id, String access_token) {
        AdProductVO adProductVO = new AdProductVO();
        // 1.构建请求参数
        String Tencent_params = StringUtils.substring(InterfaceConstants.TencentInterface.TENCENT_PARAMETERS, 1);
        String params1 = HttpUtil.urlParamReplace(Tencent_params, access_token, DateUtils.getSecondTimestamp(new Date()) + "", RandomUtils.getRandomString(32));
        StringBuilder params = new StringBuilder("");
        params.append(params1);
        params.append("&account_id=" + account_id);
        params.append("&product_type=" + product_type);
        params.append("&product_refs_id=" + product_refs_id);
        String getProductInterface = ConfigHandler.getProperty(
                InterfaceConstants.TencentInterface.TENCENT_URL_KEY, InterfaceConstants.TencentInterface.TENCENT_URL) +
                InterfaceConstants.TencentInterface.PRODUCT_GET;
        String getProductInterfaceUrl = HttpUtil.urlParamReplace(getProductInterface,
                access_token, DateUtils.getSecondTimestamp(new Date()) + "", RandomUtils.getRandomString(32));
        // 2.HTTP请求获取数据
        String httpResponse = HttpRequest.get(getProductInterfaceUrl, params.toString());
        if (StringUtils.isBlank(httpResponse)) {
            logger.error("<Tencent>请求获取推广标的物API失败！请求url[{}]，具体请求参数[{}]", new Object[]{getProductInterfaceUrl, params});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        // 3.结果数据封装，转换为Map
        JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
        int code = jsonObject.get("code").getAsInt();
        if (code != 0) {
            logger.error("<Tencent>请求获取推广标的物API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]",
                    new Object[]{getProductInterfaceUrl, params, code, jsonObject});
            ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
        String data = jsonObject.get("data").toString();
        Product product = StringUtil.gson.fromJson(data, Product.class);
        adProductVO.setProductName(product.getProduct_name());
        adProductVO.setProductRefsId(product_refs_id);
        adProductVO.setProductType(product_type);
        if (product_type.equals("PRODUCT_TYPE_APP_IOS")) {
            adProductVO.setProductIconUrl(product.getProduct_info().getProduct_type_apple_app_store().getApp_property_icon_url());
        } else if (product_type.equals("PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM")) {
            adProductVO.setProductIconUrl(product.getProduct_info().getProduct_type_app_android_open_platform().getApp_property_icon_url());
        }
        return adProductVO;

    }
}
