package com.dataeye.ad.assistor.module.dictionaries.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.module.dictionaries.mapper.ProductCategoryMapper;
import com.dataeye.ad.assistor.module.dictionaries.model.ProductCategory;
import com.dataeye.ad.assistor.module.dictionaries.model.ProductCategoryResult;


@Service("productCategoryService")
public class ProductCategoryService {
	
	@Autowired
	private ProductCategoryMapper productCategoryMapper;
	
	/**
	 * @param parent_id
	 * **/
	public List<ProductCategory> query(Integer parentId){	
		return productCategoryMapper.query(parentId);
	}
	/**
	 * @param id
	 * **/
	public ProductCategory get(Integer categoryId){	
		return productCategoryMapper.get(categoryId);
	}
	
	/**
	 * 从ApplicationContext中查询产品列表
	 * @return
	 */
	public List<ProductCategoryResult> queryFromCache() {
        return ApplicationContextContainer.getProductCategoryList();
	}

}
