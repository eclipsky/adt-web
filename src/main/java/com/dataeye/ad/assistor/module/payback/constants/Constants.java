package com.dataeye.ad.assistor.module.payback.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 回本对比分析模块常量表
 * @author ldj 2017-07-27
 */
public class Constants {

	public static class PayBackIndicator{
		 /**
	     * 按天统计指标
	     */
	    public static final Map<Integer, String> dailyIndicatorMap = new HashMap<>();
	    /**
	     * 按月统计指标
	     */
	   // public static final Map<Integer, String> monthlyIndicatorMap = new HashMap<>();
	    
	    public static String getdailyIndicator(int key) {
			return dailyIndicatorMap.get(key);
		}
	   /* public static String getMonthlyIndicator(int key) {
			return monthlyIndicatorMap.get(key);
		}*/
		static {
			//按天统计指标
			dailyIndicatorMap.put(1,"amount_d1");  //首日回本金额(按分)
			dailyIndicatorMap.put(3,"amount_d3");  //3日回本金额(按分)
			dailyIndicatorMap.put(7,"amount_d7");  //7日回本金额(按分)
			dailyIndicatorMap.put(15,"amount_d15");  //15日回本金额(按分)
			dailyIndicatorMap.put(30,"amount_d30");  //30日回本金额(按分)
			dailyIndicatorMap.put(60,"amount_d60");  //60日回本金额(按分)
			dailyIndicatorMap.put(90,"amount_d90");  //90日回本金额(按分)
			
			/*//按月统计指标
			monthlyIndicatorMap.put(1,"amount_m1");  //首月回本金额(按分)
			monthlyIndicatorMap.put(2,"amount_m2");  //次月回本金额(按分)
			monthlyIndicatorMap.put(3,"amount_m3");  //3月回本金额(按分)
			monthlyIndicatorMap.put(4,"amount_m4");  //4月回本金额(按分)
			monthlyIndicatorMap.put(5,"amount_m5");  //5月回本金额(按分)
			monthlyIndicatorMap.put(6,"amount_m6");  //6月回本金额(按分)
			monthlyIndicatorMap.put(7,"amount_m7");  //7月回本金额(按分)
*/		}
	}

	
}
