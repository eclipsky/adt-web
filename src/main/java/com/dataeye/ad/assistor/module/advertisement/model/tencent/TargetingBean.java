package com.dataeye.ad.assistor.module.advertisement.model.tencent;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 *
 */
public class TargetingBean {

    @Expose
    private List<String> age;

    @Expose
    private List<String> gender;

    @Expose
    private List<String> education;

    @Expose
    private List<String> relationship_status;

    @Expose
    private List<String> living_status;

    @Expose
    private List<Integer> business_interest;

    @Expose
    private KeyWord keyword;

    @Expose
    private List<Integer> location;

    @Expose
    private List<Integer> region;

    @Expose
    private GeoLocation geo_location;

    @Expose
    private List<String> network_type;

    @Expose
    private List<String> network_operator;

    @Expose
    private List<String> app_install_status;

    @Expose
    private AppBehavior app_behavior;

    @Expose
    private List<Integer> customized_audience;

    @Expose
    private List<String> shopping_capability;

    @Expose
    private List<String> residential_community_price;

    @Expose
    private List<String> online_scenario;

    @Expose
    private List<String> paying_user_type;

    @Expose
    private List<Integer> custom_audience;

    @Expose
    private List<Integer> excluded_custom_audience;

    public static class KeyWord {
        @Expose
        private List<String> words;

        public List<String> getWords() {
            return words;
        }

        public void setWords(List<String> words) {
            this.words = words;
        }
    }

    public static class GeoLocation {
        @Expose
        private List<Integer> regions;
        @Expose
        private List<String> location_types;

        public List<Integer> getRegions() {
            return regions;
        }

        public void setRegions(List<Integer> regions) {
            this.regions = regions;
        }

        public List<String> getLocation_types() {
            return location_types;
        }

        public void setLocation_types(List<String> location_types) {
            this.location_types = location_types;
        }
    }

    public static class AppBehavior {

        @Expose
        private String object_type;
        @Expose
        private List<Long> object_id_list;
        @Expose
        private Integer time_window;
        @Expose
        private List<String> act_id_list;

        public String getObject_type() {
            return object_type;
        }

        public void setObject_type(String object_type) {
            this.object_type = object_type;
        }

        public List<Long> getObject_id_list() {
            return object_id_list;
        }

        public void setObject_id_list(List<Long> object_id_list) {
            this.object_id_list = object_id_list;
        }

        public Integer getTime_window() {
            return time_window;
        }

        public void setTime_window(Integer time_window) {
            this.time_window = time_window;
        }

        public List<String> getAct_id_list() {
            return act_id_list;
        }

        public void setAct_id_list(List<String> act_id_list) {
            this.act_id_list = act_id_list;
        }
    }


    public List<String> getAge() {
        return age;
    }

    public void setAge(List<String> age) {
        this.age = age;
    }

    public List<String> getGender() {
        return gender;
    }

    public void setGender(List<String> gender) {
        this.gender = gender;
    }

    public List<String> getEducation() {
        return education;
    }

    public void setEducation(List<String> education) {
        this.education = education;
    }

    public List<String> getRelationship_status() {
        return relationship_status;
    }

    public void setRelationship_status(List<String> relationship_status) {
        this.relationship_status = relationship_status;
    }

    public List<String> getLiving_status() {
        return living_status;
    }

    public void setLiving_status(List<String> living_status) {
        this.living_status = living_status;
    }

    public List<Integer> getBusiness_interest() {
        return business_interest;
    }

    public void setBusiness_interest(List<Integer> business_interest) {
        this.business_interest = business_interest;
    }

    public KeyWord getKeyword() {
        return keyword;
    }

    public void setKeyword(KeyWord keyword) {
        this.keyword = keyword;
    }

    public List<Integer> getLocation() {
        return location;
    }

    public void setLocation(List<Integer> location) {
        this.location = location;
    }

    public List<Integer> getRegion() {
        return region;
    }

    public void setRegion(List<Integer> region) {
        this.region = region;
    }

    public GeoLocation getGeo_location() {
        return geo_location;
    }

    public void setGeo_location(GeoLocation geo_location) {
        this.geo_location = geo_location;
    }

    public List<String> getNetwork_type() {
        return network_type;
    }

    public void setNetwork_type(List<String> network_type) {
        this.network_type = network_type;
    }

    public List<String> getNetwork_operator() {
        return network_operator;
    }

    public void setNetwork_operator(List<String> network_operator) {
        this.network_operator = network_operator;
    }

    public List<String> getApp_install_status() {
        return app_install_status;
    }

    public void setApp_install_status(List<String> app_install_status) {
        this.app_install_status = app_install_status;
    }

    public AppBehavior getApp_behavior() {
        return app_behavior;
    }

    public void setApp_behavior(AppBehavior app_behavior) {
        this.app_behavior = app_behavior;
    }

    public List<Integer> getCustomized_audience() {
        return customized_audience;
    }

    public void setCustomized_audience(List<Integer> customized_audience) {
        this.customized_audience = customized_audience;
    }

    public List<String> getShopping_capability() {
        return shopping_capability;
    }

    public void setShopping_capability(List<String> shopping_capability) {
        this.shopping_capability = shopping_capability;
    }

    public List<String> getResidential_community_price() {
        return residential_community_price;
    }

    public void setResidential_community_price(List<String> residential_community_price) {
        this.residential_community_price = residential_community_price;
    }

    public List<String> getOnline_scenario() {
        return online_scenario;
    }

    public void setOnline_scenario(List<String> online_scenario) {
        this.online_scenario = online_scenario;
    }

    public List<String> getPaying_user_type() {
        return paying_user_type;
    }

    public void setPaying_user_type(List<String> paying_user_type) {
        this.paying_user_type = paying_user_type;
    }

    public List<Integer> getCustom_audience() {
        return custom_audience;
    }

    public void setCustom_audience(List<Integer> custom_audience) {
        this.custom_audience = custom_audience;
    }

    public List<Integer> getExcluded_custom_audience() {
        return excluded_custom_audience;
    }

    public void setExcluded_custom_audience(List<Integer> excluded_custom_audience) {
        this.excluded_custom_audience = excluded_custom_audience;
    }
}