package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.constant.Medium;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by huangzeahi on 2017/5/18.
 */
public final class PlanSwitchServices {

    /**
     * 出价服务映射.
     */
    private static final Map<Integer, String> SERVICES = new HashMap<>();

    static {
        //映射出价服务
        SERVICES.put(Medium.Toutiao, "toutiaoPlanSwitchService");
        SERVICES.put(Medium.Tui, "tuiPlanSwitchService");
        SERVICES.put(Medium.BaiduFeedAds, "baiduPlanSwitchService");
        SERVICES.put(Medium.Tencent, "tencentPlanSwitchService");
        SERVICES.put(Medium.Uc, "ucPlanSwitchService");
    }

    public static boolean isPlanSwitchAvailable(Integer mediumId) {
        return SERVICES.containsKey(mediumId);
    }

    public static String getPlanSwitchService(Integer mediumId) {
        if (mediumId == null) {
            return null;
        }
        return SERVICES.get(mediumId);
    }
}
