package com.dataeye.ad.assistor.module.realtimedelivery.model;

import java.util.List;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

/**
 * Created by huangzehai on 2017/5/9.
 */
public class BalanceQuery extends DataPermissionDomain {
    private String orderBy;
    private String order;
    private List<Integer> mediumIds;
    private List<Integer> mediumAccountIds;

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

	public List<Integer> getMediumIds() {
		return mediumIds;
	}

	public void setMediumIds(List<Integer> mediumIds) {
		this.mediumIds = mediumIds;
	}

	public List<Integer> getMediumAccountIds() {
		return mediumAccountIds;
	}

	public void setMediumAccountIds(List<Integer> mediumAccountIds) {
		this.mediumAccountIds = mediumAccountIds;
	}
    
}
