package com.dataeye.ad.assistor.module.mediumaccount.model;

/**
 * Created by huangzhai on 2017/5/12.
 */
public class MediumAccountQuery {
    /**
     * 媒体ID.
     */
    private Integer mediumId;
    /**
     * 媒体账号ID.
     */
    private Integer mediumAccountId;

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    @Override
    public String toString() {
        return "MediumAccountQuery{" +
                "mediumId=" + mediumId +
                ", mediumAccountId=" + mediumAccountId +
                '}';
    }
}
