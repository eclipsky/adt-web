package com.dataeye.ad.assistor.module.advertisement.service.tencent;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.InterfaceConstants.TencentInterface;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.AdVideos;
import com.dataeye.ad.assistor.util.DateUtils;

import org.apache.http.entity.mime.content.StringBody;

import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.HttpUtil;
import com.dataeye.ad.assistor.util.RandomUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


/**
 * 广点通API
 * 视频Service
 * */
@Service("adVideoService")
public class AdVideoService {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AdVideoService.class);

	
	/**
	 * 添加视频文件
	 * @param account_id(必填)   
	 * @param file(必填)
	 * @param signature(必填)
	 * @return 
	 */
	public Integer add(Integer account_id,File file,String signature,String accessToken,String description)throws Exception {
		validateParamsByAdd(account_id, accessToken, file);
		// 1.构建请求参数
		Map<String, ContentBody> params = new HashMap<String, ContentBody>();
		params.put("account_id", new StringBody(account_id+"",Charset.forName("UTF-8")));
		params.put("video_file", new FileBody(file));
		params.put("signature", new StringBody(signature+"",Charset.forName("UTF-8")));
		if(StringUtils.isNotBlank(description)){
			params.put("description", new StringBody(description+"",Charset.forName("UTF-8")));
		}
		
		String addVideosInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.VIDEOS_ADD;
		String addVideosInterfaceUrl = HttpUtil.urlParamReplace(addVideosInterface,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.postFile(addVideosInterfaceUrl, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>创建视频API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{addVideosInterfaceUrl, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>创建视频API数据请求响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{addVideosInterfaceUrl, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
    	
    	JsonObject data = jsonObject.get("data").getAsJsonObject();
        return data.get("video_id").getAsInt();
	}
	
	/**
	 * 获取视频
	 * @param account_id（必填）
	 * @param video_id（必填）
	 * @return preview_url 预览地址
	 */
	public AdVideos get(Integer account_id,Integer video_id,String accessToken) {
		validateParamsByAccount(account_id, accessToken);
		// 1.构建请求参数
		String Tencent_params= StringUtils.substring(TencentInterface.TENCENT_PARAMETERS, 1);
		String params1 = HttpUtil.urlParamReplace(Tencent_params,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));

        if (video_id == null || video_id == 0) {
            ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_VIDEOS_ID_IS_NULL);
        }
        String params=params1+"&account_id="+account_id+"&video_id="+video_id;
		
		String getVideosInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.VIDEOS_GET;
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.get(getVideosInterface,params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>获取视频API失败！请求url[{}]，具体请求参数[{}]", new Object[]{getVideosInterface, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>获取视频API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{getVideosInterface, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
    	JsonObject data=jsonObject.get("data").getAsJsonObject();
    	//JsonArray dataJsonObject = data.get("list").getAsJsonArray();
        return StringUtil.gson.fromJson(data, new TypeToken<AdVideos>(){}.getType());
	}
	
	//参数校验
	private void validateParamsByAccount(Integer account_id,String accessToken){
		if (account_id == null || account_id == 0) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCOUNT_ID_IS_NULL);
        }
        if (StringUtils.isBlank(accessToken)) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCESSTOKEN_ID_IS_NULL);
        }
	}
	
	private void validateParamsByAdd(Integer account_id,String accessToken,File file){
		validateParamsByAccount(account_id, accessToken);
		if (file == null) {
			ExceptionHandler.throwParameterException(StatusCode.COMM_FILE_NOT_UPLOAD);
        }
	}
	
}
