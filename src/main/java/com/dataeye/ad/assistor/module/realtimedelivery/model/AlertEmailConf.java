package com.dataeye.ad.assistor.module.realtimedelivery.model;

/**
 * 告警邮箱配置
 * @author luzhuyou
 *
 */
public class AlertEmailConf {
	/** 账户ID */
	private int accountId;
	/** 告警邮箱 */
	private String alertEmails;
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public String getAlertEmails() {
		return alertEmails;
	}
	public void setAlertEmails(String alertEmails) {
		this.alertEmails = alertEmails;
	}
	@Override
	public String toString() {
		return "AlertEmailConf [accountId=" + accountId + ", alertEmails="
				+ alertEmails + "]";
	}
	
}
