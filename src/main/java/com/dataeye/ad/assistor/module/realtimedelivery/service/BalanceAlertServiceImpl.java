package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.module.mediumaccount.mapper.MediumAccountMapper;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccount;
import com.dataeye.ad.assistor.module.realtimedelivery.mapper.BalanceAlertLogMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.mapper.BalanceAlertMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Balance;
import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceAlertConf;
import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceAlertLog;
import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceQuery;
import com.dataeye.ad.assistor.util.MailUtils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import java.util.*;
import java.util.zip.CRC32;

/**
 * Created by huangzehai on 2017/4/28.
 */
@Service("balanceAlertService")
public class BalanceAlertServiceImpl implements BalanceAlertService {
    private Logger logger = LoggerFactory.getLogger(BalanceAlertServiceImpl.class);
    private static final int NORMAL = 0;

    private Map<String, Date> alertLogsCache = new HashMap<>();

    @Autowired
    private BalanceAlertMapper balanceAlertMapper;

    @Autowired
    private BalanceService balanceService;

    @Autowired
    private MediumAccountMapper mediumAccountMapper;

    @Autowired
    private BalanceAlertLogMapper balanceAlertLogMapper;

    @Override
    public int saveBalanceAlertConf(BalanceAlertConf balanceAlertConf) {
        return balanceAlertMapper.saveBalanceAlertConf(balanceAlertConf);
    }

    @Override
    public BalanceAlertConf getBalanceAlertConfByAccountId(int accountId) {
        return balanceAlertMapper.getBalanceAlertConfByAccountId(accountId);
    }

    @PostConstruct
    public void init() {
        logger.info("Cache balance alert logs");
        List<BalanceAlertLog> balanceAlertLogs = balanceAlertLogMapper.listBalanceAlertLogs();
        if (balanceAlertLogs != null) {
            for (BalanceAlertLog balanceAlertLog : balanceAlertLogs) {
                String key = balanceAlertLog.getEmail() + balanceAlertLog.getMediumAccountIdCrc();
                alertLogsCache.put(key, balanceAlertLog.getUpdateTime());
            }
        }
    }

    /**
     * 账户余额邮件通知
     */
    @Override
    public void sentBalanceWarningEmail() {
        //获取所有媒体账号的余额
        List<Balance> balances = balanceService.listAccountBalance(new BalanceQuery());
        Map<Integer, Balance> alertMap = new HashMap<>();
        List<MediumAccount> relations = mediumAccountMapper.listMediumAccountRelation();
        Map<String, Set<Integer>> mediumAccountIdsByManager = relationsToMap(relations);
        for (Balance balance : balances) {
            //仅给正常状态的用户发送邮件
            if (balance.isWarning() && balance.getStatus() == NORMAL) {
                alertMap.put(balance.getMediumAccountId(), balance);
            }
        }
        sendMail(mediumAccountIdsByManager, alertMap);
    }

    /**
     * 发送邮件
     *
     * @param mediumAccountIdsByManager
     * @param warningBalances
     */
    private void sendMail(Map<String, Set<Integer>> mediumAccountIdsByManager, Map<Integer, Balance> warningBalances) {
        List<String> waringEmails = balanceAlertMapper.getWaringEmails();
        for (Map.Entry<String, Set<Integer>> entry : mediumAccountIdsByManager.entrySet()) {
            List<Balance> warningBalanceList = getWarningBalancesByMediumAccountIds(warningBalances, entry.getValue());
            //仅给选中了立即发送邮件的负责人发送邮件.
            if (waringEmails != null && waringEmails.contains(entry.getKey()) && warningBalanceList != null && !warningBalanceList.isEmpty()) {
                //检查在当前是否已经发送过邮件
                BalanceAlertLog balanceAlertLog = generateBalanceAlertLog(entry.getKey(), warningBalanceList);
                //一天只告警一次.
                if (!hasSentEmailToday(balanceAlertLog)) {
                    sentMail(entry.getKey(), warningBalanceList);
                    cacheAlertLog(balanceAlertLog);
                    balanceAlertLogMapper.saveBalanceAlertLog(balanceAlertLog);
                }
            }
        }
    }

    /**
     * 判断是否是否已经发送过告警邮件
     *
     * @return
     */
    private boolean hasSentEmailToday(BalanceAlertLog balanceAlertLog) {
        String key = balanceAlertLog.getEmail() + balanceAlertLog.getMediumAccountIdCrc();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return alertLogsCache.containsKey(key) && alertLogsCache.get(key).after(calendar.getTime());
    }

    /**
     * 缓存告警日志
     *
     * @param balanceAlertLog
     */
    private void cacheAlertLog(BalanceAlertLog balanceAlertLog) {
        String key = balanceAlertLog.getEmail() + balanceAlertLog.getMediumAccountIdCrc();
        alertLogsCache.put(key, new Date());
    }

    /**
     * 生成告警日志
     *
     * @param email
     * @param balances
     * @return
     */
    private BalanceAlertLog generateBalanceAlertLog(String email, List<Balance> balances) {
        BalanceAlertLog log = new BalanceAlertLog();
        StringBuilder mediumIds = new StringBuilder();
        for (Balance balance : balances) {
            mediumIds.append(balance.getMediumAccountId());
        }
        CRC32 crc = new CRC32();
        crc.update(mediumIds.toString().getBytes());
        log.setEmail(email);
        log.setUpdateTime(new Date());
        log.setMediumAccountIdCrc(crc.getValue());
        return log;
    }


    /**
     * 发送邮件
     *
     * @param mailTo
     * @param warningBalanceList
     */
    private void sentMail(String mailTo, List<Balance> warningBalanceList) {
        String subject = "媒体账户余额告警";
        StringBuilder contentBuilder = new StringBuilder();
        contentBuilder.append("<table border=\"1px solid black\">");
        contentBuilder.append("<thead><tr><th>媒体</th><th>媒体账号</th><th>余额</th></tr></thead>");
        contentBuilder.append("<tbody>");
        for (Balance balance : warningBalanceList) {
            contentBuilder.append("<tr>");
            contentBuilder.append("<td>").append(balance.getMediumName()).append("</td>");
            contentBuilder.append("<td>").append(balance.getMediumAccount()).append("</td>");
            contentBuilder.append("<td>").append(balance.getBalance()).append("</td>");
            contentBuilder.append("</tr>");
        }
        contentBuilder.append("</tbody>");
        contentBuilder.append("</table>");
        MailUtils.sendAlarmMail(subject, contentBuilder.toString(), mailTo);
        logger.info("Send balance email to " + mailTo + " and detail is  " + warningBalanceList);
    }


    /**
     * 获取待告警列表
     *
     * @param warningBalances
     * @param mediumAccountIds
     * @return
     */
    private List<Balance> getWarningBalancesByMediumAccountIds(Map<Integer, Balance> warningBalances, Set<Integer> mediumAccountIds) {
        List<Balance> balances = new ArrayList<>();
        for (Integer mediumAccountId : mediumAccountIds) {
            if (warningBalances.containsKey(mediumAccountId)) {
                balances.add(warningBalances.get(mediumAccountId));
            }
        }
        return balances;
    }

    /**
     * 生成负责人和媒体账号ID的关系
     *
     * @param relations
     * @return
     */
    private Map<String, Set<Integer>> relationsToMap(List<MediumAccount> relations) {
        Map<String, Set<Integer>> mediumAccountIdsByManager = new HashMap<>();
        if (relations != null) {
            for (MediumAccount relation : relations) {
                if (StringUtils.isNotBlank(relation.getResponsibleAccounts())) {
                    String[] managers = relation.getResponsibleAccounts().split(Constant.Separator.COMMA);
                    for (String manager : managers) {
                        if (mediumAccountIdsByManager.containsKey(manager)) {
                            mediumAccountIdsByManager.get(manager).add(relation.getMediumAccountId());
                        } else {
                            Set<Integer> set = new HashSet<>();
                            set.add(relation.getMediumAccountId());
                            mediumAccountIdsByManager.put(manager, set);
                        }
                    }
                }
            }
        }
        return mediumAccountIdsByManager;
    }
    
	@Override
	public List<BalanceAlertConf> getBalanceAlertConfByCompanyId(int companyId) {
		return balanceAlertMapper.getBalanceAlertConfByCompanyId(companyId);
	}

	@Override
	public int updateBalanceAlertConf(int accountId, int companyId, int balanceRedNum) {
		BalanceAlertConf balanceAlertConf = new BalanceAlertConf();
        balanceAlertConf.setCompanyId(companyId);
        balanceAlertConf.setAccountId(accountId);
        balanceAlertConf.setBalanceRedNum(balanceRedNum);
		return balanceAlertMapper.updateBalanceAlertConf(balanceAlertConf);
	}
}
