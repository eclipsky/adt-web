package com.dataeye.ad.assistor.module.product.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * 测试设备
 * @author ldj 2017/10/30
 *
 */
public class DebugDevice {

	/** id */
	@Expose
	private Integer id;
	/**测试设备名称 */
	@Expose
	private String deviceName;
	/** 测试设备号 */
	@Expose
	private String device;
	/** 平台 */
	@Expose
	private String platform;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	@Override
	public String toString() {
		return "DebugDevice [id=" + id + ", deviceName=" + deviceName
				+ ", device=" + device + ", platform=" + platform + "]";
	}
	
	
}
