package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.Date;

/**
 * 媒体
 * @author luzhuyou 2017/06/23
 *
 */
public class MediumTrackingMapping {
	/** 媒体ID */
	private Integer mediumId;
	/** 系统类型：0-Others，1-iOS， 2-Android */
	private Integer osType;
	/** Tracking媒体ID */
	private Integer trackingMediumId;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	/**Tracking数据接入类型：0-媒体对接，1-外链*/
	private Integer trackingAccessType;
	
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	public Integer getTrackingMediumId() {
		return trackingMediumId;
	}
	public void setTrackingMediumId(Integer trackingMediumId) {
		this.trackingMediumId = trackingMediumId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	public Integer getTrackingAccessType() {
		return trackingAccessType;
	}
	public void setTrackingAccessType(Integer trackingAccessType) {
		this.trackingAccessType = trackingAccessType;
	}
	@Override
	public String toString() {
		return "MediumTrackingMapping [mediumId=" + mediumId + ", osType="
				+ osType + ", trackingMediumId=" + trackingMediumId
				+ ", remark=" + remark + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", trackingAccessType="
				+ trackingAccessType + "]";
	}
}
