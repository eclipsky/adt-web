package com.dataeye.ad.assistor.module.report.mapper;

import com.dataeye.ad.assistor.common.DailyRecord;
import com.dataeye.ad.assistor.common.WeeklyRecord;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import com.dataeye.ad.assistor.module.report.model.DeliveryTrendQuery;
import com.dataeye.ad.assistor.module.report.model.PlanQuery;
import org.mybatis.spring.annotation.MapperScan;

import java.math.BigDecimal;
import java.util.List;

/**
 * 投放日报对比分析Mapper.
 * Created by huangzehai on 2017/4/27.
 */
@MapperScan
public interface DeliveryContrastiveAnalysisMapper {
    /**
     * 消耗日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 消耗周趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> costWeeklyTrendGroupByMediumAccount(DeliveryTrendQuery query);

    /**
     * 消耗月趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costMonthlyTrendGroupByMediumAccount(DeliveryTrendQuery query);

    /**
     * 消耗时段趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costMinutelyTrendGroupByMediumAccount(DeliveryTrendQuery query);

    /**
     * CTR日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> ctrTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Double>> ctrWeeklyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR按媒体账号分组月趋势图
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> ctrMonthlyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 点击数按媒体账号分组时段趋势图
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> clickMinutelyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 曝光数按媒体账号分组时段趋势图
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> exposureMinutelyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> downloadRateTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Double>> downloadRateWeeklyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> downloadRateMonthlyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载数10分钟趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> downloadTimesMinutelyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 到达数10分钟趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> reachesMinutelyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Integer>> registrationWeeklyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationMonthlyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationMinutelyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> cpaTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> cpaWeeklyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> cpaMonthlyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> rechargeWeeklyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeMonthlyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeMinutelyTrendGroupByMediumAccount(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 消耗日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 消耗周趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> costWeeklyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 消耗月趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costMonthlyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 消耗分钟趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costMinutelyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> ctrTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR周趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Double>> ctrWeeklyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR月趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> ctrMonthlyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);


    /**
     * 点击数时段趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> clickMinutelyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 曝光数时段趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> exposureMinutelyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> downloadRateTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率周趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Double>> downloadRateWeeklyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率月趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> downloadRateMonthlyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载数10分钟趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> downloadTimesMinutelyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 到达数10分钟趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> reachesMinutelyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数周趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Integer>> registrationWeeklyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数月趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationMonthlyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数月趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationMinutelyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> cpaTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA周趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> cpaWeeklyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA月趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> cpaMonthlyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值日趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> rechargeWeeklyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeMonthlyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按计划分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeMinutelyTrendGroupByPlan(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 获取计划列表.
     *
     * @param planQuery
     * @return
     */
    List<Plan> listPlans(PlanQuery planQuery);


    /**
     * 获取默认显示的媒体ID列表.
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<Integer> getDefaultMediumIds(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 获取默认显示的媒体账号ID列表.
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<Integer> getDefaultMediumAccountIds(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 获取默认显示的计划ID列表.
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<Integer> getDefaultPlanIds(DeliveryTrendQuery deliveryTrendQuery);

    List<Integer> getDefaultProductIds(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 消耗日趋势图，按媒体分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 消耗周趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> costWeeklyTrendGroupByMedium(DeliveryTrendQuery query);

    /**
     * 消耗月趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costMonthlyTrendGroupByMedium(DeliveryTrendQuery query);

    /**
     * 消耗时段趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costMinutelyTrendGroupByMedium(DeliveryTrendQuery query);

    /**
     * CTR日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> ctrTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Double>> ctrWeeklyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR按媒体账号分组月趋势图
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> ctrMonthlyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 点击数按媒体账号分组时段趋势图
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> clickMinutelyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 曝光数按媒体账号分组时段趋势图
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> exposureMinutelyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> downloadRateTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Double>> downloadRateWeeklyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> downloadRateMonthlyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载数10分钟趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> downloadTimesMinutelyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 到达数10分钟趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> reachesMinutelyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Integer>> registrationWeeklyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationMonthlyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationMinutelyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> cpaTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> cpaWeeklyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> cpaMonthlyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> rechargeWeeklyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeMonthlyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeMinutelyTrendGroupByMedium(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 消耗日趋势图，按媒体分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 消耗周趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> costWeeklyTrendGroupByProduct(DeliveryTrendQuery query);

    /**
     * 消耗月趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costMonthlyTrendGroupByProduct(DeliveryTrendQuery query);

    /**
     * 消耗时段趋势图，按媒体分组
     *
     * @param query
     * @return
     */
    List<DailyRecord<String, BigDecimal>> costMinutelyTrendGroupByProduct(DeliveryTrendQuery query);

    /**
     * CTR日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> ctrTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Double>> ctrWeeklyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CTR按媒体账号分组月趋势图
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> ctrMonthlyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 点击数按媒体账号分组时段趋势图
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> clickMinutelyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 曝光数按媒体账号分组时段趋势图
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> exposureMinutelyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> downloadRateTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Double>> downloadRateWeeklyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载率月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Double>> downloadRateMonthlyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 下载数10分钟趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> downloadTimesMinutelyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 到达数10分钟趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> reachesMinutelyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, Integer>> registrationWeeklyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationMonthlyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 注册数月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, Integer>> registrationMinutelyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> cpaTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> cpaWeeklyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * CPA月趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> cpaMonthlyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值日趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<WeeklyRecord<String, BigDecimal>> rechargeWeeklyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeMonthlyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 充值周趋势图，按媒体账号分组
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<DailyRecord<String, BigDecimal>> rechargeMinutelyTrendGroupByProduct(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 获取指定媒体账号ID的媒体账号名称
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<String> listMediumAccounts(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 获取指定计划ID的计划名称列表
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<String> listPlanNames(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 获取指定媒体ID的媒体名称列表
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<String> listMediums(DeliveryTrendQuery deliveryTrendQuery);

    /**
     * 获取指定产品ID的产品名称列表
     *
     * @param deliveryTrendQuery
     * @return
     */
    List<String> listProducts(DeliveryTrendQuery deliveryTrendQuery);
}
