package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.realtimedelivery.mapper.RealTimeTrendMapper;
import com.dataeye.ad.assistor.module.realtimedelivery.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 实时趋势图服务实现类。为了提高查询性能，采用分别查询计划、落地页、包的数据，然后在应用层实现联结和计算比率。
 * Created by huangzehai on 2017/2/24.
 */
@Service
public class RealTimeTrendServiceImpl implements RealTimeTrendService {

    private static final String DATE_FORMAT = "MM-dd HH:mm";

    @Autowired
    private RealTimeTrendMapper realTimeTrendMapper;

    /**
     * 生成实时趋势图.
     *
     * @param trendQuery 查询条件
     * @return
     */
    @Override
    public RealTimeTrendChart<BigDecimal, Object> realTimeTrendChart(TrendQuery trendQuery) {
        List<RealTimeTrend> trends = realTimeTrend(trendQuery);
        List<String> times = new ArrayList<>();
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        List<BigDecimal> costs = new ArrayList<>();
        for (RealTimeTrend trend : trends) {
            times.add(df.format(trend.getTime()));
            costs.add(trend.getCost());
        }
        RealTimeTrendChart chart = new RealTimeTrendChart();
        chart.setTimes(times);
        //消耗
        Chart<BigDecimal> costChart = new Chart<>();
        costChart.setTitle(TrendIndicator.TotalCost.getLabel());
        costChart.setValues(costs);
        chart.setTotalCosts(costChart);
        //其他指标
        if (trendQuery.getIndicators() != null && trendQuery.getIndicators().length > 0) {
            List<Chart<Object>> indicatorCharts = new ArrayList<>();
            for (TrendIndicator indicator : trendQuery.getIndicators()) {
                Chart<Object> indicatorChart = new Chart<>();
                indicatorChart.setTitle(indicator.getLabel());
                List<Object> values = new ArrayList<>();
                for (RealTimeTrend trend : trends) {
                    switch (indicator) {
                        case Exposures:
                            values.add(trend.getExposures());
                            break;
                        case Clicks:
                            values.add(trend.getClicks());
                            break;
                        case CTR:
                            values.add(trend.getCtr());
                            indicatorChart.setPercent(true);
                            break;
                        case Downloads:
                            values.add(trend.getDownloads());
                            break;
                        case DownloadRate:
                            values.add(trend.getDownloadRate());
                            indicatorChart.setPercent(true);
                            break;
                        case Activations:
                            values.add(trend.getActivations());
                            break;
                        case ActivationRate:
                            values.add(trend.getActivationRate());
                            indicatorChart.setPercent(true);
                            break;
                        case ActivationCPA:
                            values.add(trend.getActivationCpa());
                            break;
                        case Registrations:
                            values.add(trend.getFirstDayRegistrations());
                            break;
                        case RegistrationRate:
                            values.add(trend.getRegistrationRateFd());
                            indicatorChart.setPercent(true);
                            break;
                        case CPA:
                            values.add(trend.getCostPerRegistrationFd());
                            break;
                    }
                }
                indicatorChart.setValues(values);
                indicatorCharts.add(indicatorChart);
            }
            chart.setIndicators(indicatorCharts);
        }
        return chart;
    }


    /**
     * 获取实时趋势列表.
     *
     * @param trendQuery
     * @return
     */
    private List<RealTimeTrend> getTrend(TrendQuery trendQuery) {
        //联结表.
        List<RealTimeTrend> trends = realTimeTrendMapper.realTimeTrend(trendQuery);
        trends = getReportsByTenMinuteReport(trends, trendQuery.getPeriod());
        //根据时间分组
        Map<Date, RealTimeTrend> realTimeTrendsByTime = groupTrendByTime(trends);
        Map<Date, PageStat> pageStatsByTime = null;
        Map<Date, PackageStat> packageStatsByTime = null;
        //提前获取关联关系，优化MySQL子查询.
        Association association = realTimeTrendMapper.getAssociationByPlan(trendQuery.getPlanId());
        if (association != null) {
            //查询落地页相关指标
            if (association.getPageId() != null && association.getPageId() > 0) {
                trendQuery.setPageId(association.getPageId());
                List<PageStat> pageStatList = realTimeTrendMapper.getPageStatById(trendQuery);
                pageStatList = getReportsByTenMinuteReport(pageStatList, trendQuery.getPeriod());
                pageStatsByTime = groupPageStatByTime(pageStatList);
            }

            //查询包相关指标
            if (association.getPackageId() != null && association.getPackageId() > 0) {
                trendQuery.setPackageId(association.getPackageId());
                List<PackageStat> packageStatList = realTimeTrendMapper.getPackageStatById(trendQuery);
                packageStatList = getReportsByTenMinuteReport(packageStatList, trendQuery.getPeriod());
                packageStatsByTime = groupPackageStatByTime(packageStatList);
            }
        }

        //获取计划、落地页、包的所有统计时间.
        Set<Date> times = getAllStatTime(realTimeTrendsByTime, pageStatsByTime, packageStatsByTime);
        List<RealTimeTrend> trendList = join(realTimeTrendsByTime, pageStatsByTime, packageStatsByTime, times);
        fillMissingIndicator(trendList);

        return trendList;
    }

    /**
     * 获取计划、落地页、包的所有统计时间.
     *
     * @param realTimeTrendsByTime
     * @param pageStatsByTime
     * @param packageStatsByTime
     * @return
     */
    private Set<Date> getAllStatTime(Map<Date, RealTimeTrend> realTimeTrendsByTime, Map<Date, PageStat> pageStatsByTime, Map<Date, PackageStat> packageStatsByTime) {
        Set<Date> times = new TreeSet<>();
        if (realTimeTrendsByTime != null) {
            times.addAll(realTimeTrendsByTime.keySet());
        }
        if (pageStatsByTime != null) {
            times.addAll(pageStatsByTime.keySet());
        }

        if (packageStatsByTime != null) {
            times.addAll(packageStatsByTime.keySet());
        }
        return times;
    }

    /**
     * 填充缺失的指标，使用前一个时段的指标值.
     *
     * @param trendList
     */
    private void fillMissingIndicator(List<RealTimeTrend> trendList) {
        //补充缺失值：将当前设置的指标值设置为前一个指标值.
        if (trendList != null && !trendList.isEmpty()) {
            RealTimeTrend previousTrend = new RealTimeTrend();
            for (RealTimeTrend trend : trendList) {
                if (trend.getCost().compareTo(new BigDecimal(0)) > 0) {
                    previousTrend.setCost(trend.getCost());
                } else {
                    trend.setCost(previousTrend.getCost());
                }

                if (trend.getExposures() > 0) {
                    previousTrend.setExposures(trend.getExposures());
                } else {
                    trend.setExposures(previousTrend.getExposures());
                }


                if (trend.getClicks() > 0) {
                    previousTrend.setClicks(trend.getClicks());
                } else {
                    trend.setClicks(previousTrend.getClicks());
                }

                if (trend.getReaches() > 0) {
                    previousTrend.setReaches(trend.getReaches());
                } else {
                    trend.setReaches(previousTrend.getReaches());
                }

                if (trend.getDownloads() > 0) {
                    previousTrend.setDownloads(trend.getDownloads());
                } else {
                    trend.setDownloads(previousTrend.getDownloads());
                }

                if (trend.getActivations() > 0) {
                    previousTrend.setActivations(trend.getActivations());
                } else {
                    trend.setActivations(previousTrend.getActivations());
                }
                if (trend.getFirstDayRegistrations() > 0) {
                    previousTrend.setFirstDayRegistrations(trend.getFirstDayRegistrations());
                } else {
                    trend.setFirstDayRegistrations(previousTrend.getFirstDayRegistrations());
                }

            }
        }
    }

    /**
     * 联结计划、落地页和包数据
     *
     * @param realTimeTrendsByTime
     * @param pageStatsByTime
     * @param packageStatsByTime
     * @param times
     * @return
     */
    private List<RealTimeTrend> join(Map<Date, RealTimeTrend> realTimeTrendsByTime, Map<Date, PageStat> pageStatsByTime,  Map<Date, PackageStat> packageStatsByTime, Set<Date> times) {
        //联结计划、落地页和包数据
        List<RealTimeTrend> trendList = new ArrayList<>();
        for (Date time : times) {
            if (realTimeTrendsByTime.containsKey(time)) {
                //如果存在该时段的计划数据
                RealTimeTrend trend = realTimeTrendsByTime.get(time);
                if (pageStatsByTime != null && pageStatsByTime.containsKey(time)) {
                    PageStat pageStat = pageStatsByTime.get(time);
                    trend.setReaches(pageStat.getReaches());
                    trend.setDownloads(pageStat.getDownloads());
                }

                if (packageStatsByTime != null && packageStatsByTime.containsKey(time)) {
                    PackageStat packageStat = packageStatsByTime.get(time);
//                    trend.setDownloads(packageStat.getDownloads());
                    trend.setActivations(packageStat.getActivations());
                    trend.setFirstDayRegistrations(packageStat.getFirstDayRegistrations());
                }

                trendList.add(trend);
            } else if (pageStatsByTime != null && pageStatsByTime.containsKey(time)) {
                //如果该时段没有计划数据，但是存在落地页数据和包数据
                RealTimeTrend trend = new RealTimeTrend();
                trend.setTime(time);
                PageStat pageStat = pageStatsByTime.get(time);
                trend.setReaches(pageStat.getReaches());
                trend.setDownloads(pageStat.getDownloads());

                //可能存在该时段的包数据
                if (packageStatsByTime != null && packageStatsByTime.containsKey(time)) {
                    PackageStat packageStat = packageStatsByTime.get(time);
//                    trend.setDownloads(packageStat.getDownloads());
                    trend.setActivations(packageStat.getActivations());
                    trend.setFirstDayRegistrations(packageStat.getFirstDayRegistrations());
                }
                trendList.add(trend);
            }  else {
                //该时段没有计划数据和落地页数据，仅仅存在包数据.
                RealTimeTrend trend = new RealTimeTrend();
                trend.setTime(time);
                PackageStat packageStat = packageStatsByTime.get(time);
//                trend.setDownloads(packageStat.getDownloads());
                trend.setActivations(packageStat.getActivations());
                trend.setFirstDayRegistrations(packageStat.getFirstDayRegistrations());
                trendList.add(trend);
            }
        }
        return trendList;
    }

    /**
     * 根据十分钟间隔的实时报告获取20分钟、半小时、小时间隔统计数据
     *
     * @param reportsInTenMinutes
     * @param period
     * @return
     */
    private <T extends Time> List<T> getReportsByTenMinuteReport(List<T> reportsInTenMinutes, Period period) {
        if (Period.TenMinutes == period || reportsInTenMinutes == null) {
            return reportsInTenMinutes;
        }
        //根据整点时段分组
        Map<Date, T> periodReports = new TreeMap<>();
        //这里将最靠近时段的统计作为该时段的统计。
        for (int i = reportsInTenMinutes.size() - 1; i >= 0; i--) {
            T report = reportsInTenMinutes.get(i);
            //时间“小”的统计报告会覆盖时间“大”的统计报告.
            Date time = periodTime(report.getTime(), period);
            report.setTime(time);
            periodReports.put(time, report);
        }
        return new ArrayList<>(periodReports.values());
    }

    /**
     * 将时间转为时段.
     *
     * @param time
     * @param period
     * @return
     */
    private Date periodTime(Date time, Period period) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        int minute = calendar.get(Calendar.MINUTE);
        int min = minute / period.getIntervalInMinutes() * period.getIntervalInMinutes();
        calendar.set(Calendar.MINUTE, min);
        return calendar.getTime();
    }

    @Override
    public List<RealTimeTrend> realTimeTrend(TrendQuery trendQuery) {
        List<RealTimeTrend> trends = getTrend(trendQuery);

        //补全时间
        //设置起始时间为今天零点
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.set(Calendar.HOUR_OF_DAY, 0);
        endCalendar.set(Calendar.MINUTE, 0);
        endCalendar.set(Calendar.SECOND, 0);
        endCalendar.set(Calendar.MILLISECOND, 0);
        endCalendar.add(Calendar.MINUTE, trendQuery.getPeriod().getIntervalInMinutes());
        List<RealTimeTrend> fullTrends = new ArrayList<>();


        int index = 0, first = 0;
        //如果存在，则跳过零点,认为0点的指标值为0.
        if (trends != null && !trends.isEmpty() && trends.get(0).getTime().equals(calendar.getTime())) {
            index = first = 1;
        }
        while (endCalendar.getTime().before(now)) {

            if (index < trends.size() && endCalendar.getTime().equals(trends.get(index).getTime())) {
                //获得该时间点的统计数据
                //计算差值
                //计算指标
                if (index == first) {
                    //第一个时段，取第一点的值。
                    RealTimeTrend trend = trends.get(index);
                    trend.setTime(calendar.getTime());
                    fullTrends.add(computeRate(trend));
                } else {
                    //其他时段，取时段起始点的差值
                    RealTimeTrend previousTrend = trends.get(index - 1);
                    RealTimeTrend currentTrend = trends.get(index);
                    RealTimeTrend trend = diff(currentTrend, previousTrend);
                    trend.setTime(calendar.getTime());
                    fullTrends.add(computeRate(trend));
                }
                index++;
            } else {
                //没有零点统计数据，将零点统计数据初始化为0
                //该时间点的统计缺失,将当前时间点数据设置为上一个时间点的数据
                fullTrends.add(defaultRealTimeTrend(calendar.getTime()));
            }
            calendar.add(Calendar.MINUTE, trendQuery.getPeriod().getIntervalInMinutes());
            endCalendar.add(Calendar.MINUTE, trendQuery.getPeriod().getIntervalInMinutes());
        }

        return fullTrends;
    }

    /**
     * 根据时间分组统计计划相关指标.
     *
     * @param realTimeTrends
     * @return
     */
    private Map<Date, RealTimeTrend> groupTrendByTime(List<RealTimeTrend> realTimeTrends) {
        if (realTimeTrends == null) {
            return null;
        }
        Map<Date, RealTimeTrend> trendsByTime = new HashMap<>();
        for (RealTimeTrend realTimeTrend : realTimeTrends) {
            trendsByTime.put(realTimeTrend.getTime(), realTimeTrend);
        }
        return trendsByTime;
    }

    /**
     * 根据时间分组统计落地页相关指标.
     *
     * @param pageStatsList
     * @return
     */
    private Map<Date, PageStat> groupPageStatByTime(List<PageStat> pageStatsList) {
        if (pageStatsList == null) {
            return null;
        }
        Map<Date, PageStat> pageStatByTime = new HashMap<>();
        for (PageStat pageStat : pageStatsList) {
            pageStatByTime.put(pageStat.getTime(), pageStat);
        }
        return pageStatByTime;
    }

    private <T extends Time> Map<Date, T> groupByTime(List<T> list) {
        if (list == null) {
            return null;
        }
        Map<Date, T> pageStatByTime = new HashMap<>();
        for (T item : list) {
            pageStatByTime.put(item.getTime(), item);
        }
        return pageStatByTime;
    }

    /**
     * 根据时间分组统计包相关指标.
     *
     * @param packageStatsList
     * @return
     */
    private Map<Date, PackageStat> groupPackageStatByTime(List<PackageStat> packageStatsList) {
        if (packageStatsList == null) {
            return null;
        }
        Map<Date, PackageStat> packageStatByTime = new HashMap<>();
        for (PackageStat packageStat : packageStatsList) {
            packageStatByTime.put(packageStat.getTime(), packageStat);
        }
        return packageStatByTime;
    }

    /**
     * 计算时段差值.
     *
     * @param currentTrend
     * @param previousTrend
     * @return
     */
    private RealTimeTrend diff(RealTimeTrend currentTrend, RealTimeTrend previousTrend) {
        RealTimeTrend diffTrend = new RealTimeTrend();
//        diffTrend.setTime(previousTrend.getTime());
        diffTrend.setCost(currentTrend.getCost().subtract(previousTrend.getCost()));
        diffTrend.setExposures(currentTrend.getExposures() - previousTrend.getExposures());
        diffTrend.setClicks(currentTrend.getClicks() - previousTrend.getClicks());
        diffTrend.setDownloads(currentTrend.getDownloads() - previousTrend.getDownloads());
        diffTrend.setReaches(currentTrend.getReaches() - previousTrend.getReaches());
        diffTrend.setActivations(currentTrend.getActivations() - previousTrend.getActivations());
        diffTrend.setFirstDayRegistrations(currentTrend.getFirstDayRegistrations() - previousTrend.getFirstDayRegistrations());
        return diffTrend;
    }

    /**
     * 计算指标.
     *
     * @param trend
     * @return
     */
    private RealTimeTrend computeRate(RealTimeTrend trend) {
        if (trend.getExposures() != 0) {
            //CTR
            trend.setCtr((double) trend.getClicks() / trend.getExposures());
        }

        if (trend.getReaches() != 0) {
            //下载率
            trend.setDownloadRate((double) trend.getDownloads() / trend.getReaches());
        }

        if (trend.getFirstDayRegistrations() != 0) {
            //注册CPA
            trend.setCostPerRegistrationFd(trend.getCost().divide(new BigDecimal(trend.getFirstDayRegistrations()), 2, RoundingMode.HALF_UP));
        }

        if (trend.getActivations() != 0) {
            //激活CPA
            trend.setActivationCpa(trend.getCost().divide(new BigDecimal(trend.getActivations()), 2, RoundingMode.HALF_UP));
        }

        if (trend.getDownloads() != 0) {
            //下载单价
            trend.setCostPerDownload(trend.getCost().divide(new BigDecimal(trend.getDownloads()), 2, RoundingMode.HALF_UP));
        }

        if (trend.getClicks() != 0) {
            //激活率
            trend.setActivationRate((double) trend.getActivations() / trend.getClicks());
            //注册率
            trend.setRegistrationRateFd((double) trend.getFirstDayRegistrations() / trend.getClicks());
        }
        return trend;
    }

    /**
     * 生成默认指标.
     *
     * @param time
     * @return
     */
    private RealTimeTrend defaultRealTimeTrend(Date time) {
        RealTimeTrend trend = new RealTimeTrend();
        trend.setTime(time);
        trend.setCost(new BigDecimal(0));
        trend.setExposures(0L);
        trend.setClicks(0L);
        trend.setCtr(0.0);
        trend.setDownloads(0L);
        trend.setDownloadRate(0.0);
        trend.setCostPerDownload(new BigDecimal(0));
        trend.setActivations(0L);
        trend.setActivationRate(0.0);
        trend.setActivationCpa(new BigDecimal(0));
        trend.setFirstDayRegistrations(0);
        trend.setRegistrationRateFd(0.0);
        trend.setCostPerRegistrationFd(new BigDecimal(0));
        return trend;
    }
}
