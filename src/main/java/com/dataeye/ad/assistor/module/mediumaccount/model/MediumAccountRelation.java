package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.Date;

/**
 * 媒体账号关联关系
 * @author luzhuyou 2017/04/20
 *
 */
public class MediumAccountRelation {
	/** 媒体账号关联关系ID */
	private Integer relationId;
	/** 媒体账户ID */
	private Integer mediumAccountId;
	/** 公司ID */
	private Integer companyId;
	/** 产品ID */
	private Integer productId;
	/** 媒体ID */
	private Integer mediumId;
	/** 关注者ADT账号名称列表(至少一个系统账号，多个以逗号分隔) */
	private String followerAccounts;
	/** 优化师ADT账号名称 */
	private String responsibleAccounts;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	public Integer getRelationId() {
		return relationId;
	}
	public void setRelationId(Integer relationId) {
		this.relationId = relationId;
	}
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public String getFollowerAccounts() {
		return followerAccounts;
	}
	public void setFollowerAccounts(String followerAccounts) {
		this.followerAccounts = followerAccounts;
	}
	public String getResponsibleAccounts() {
		return responsibleAccounts;
	}
	public void setResponsibleAccounts(String responsibleAccounts) {
		this.responsibleAccounts = responsibleAccounts;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "MediumAccountRelation [relationId=" + relationId
				+ ", mediumAccountId=" + mediumAccountId + ", companyId="
				+ companyId + ", productId=" + productId + ", mediumId="
				+ mediumId + ", followerAccounts=" + followerAccounts
				+ ", responsibleAccounts=" + responsibleAccounts
				+ ", remark=" + remark + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + "]";
	}
}
