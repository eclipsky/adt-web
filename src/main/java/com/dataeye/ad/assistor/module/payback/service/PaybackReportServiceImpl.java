package com.dataeye.ad.assistor.module.payback.service;

import com.dataeye.ad.assistor.module.naturalflow.model.DailyPaybackNaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.MonthlyPaybackNaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlowQuery;
import com.dataeye.ad.assistor.module.naturalflow.service.NaturalFlowService;
import com.dataeye.ad.assistor.module.payback.mapper.PaybackReportMapper;
import com.dataeye.ad.assistor.module.payback.model.*;
import com.dataeye.ad.assistor.module.report.constant.Constants;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.NumberUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 回本日报服务实现类.
 * Created by huangzehai on 2017/1/13.
 */
@Service
public class PaybackReportServiceImpl implements PaybackReportService {
    /**
     * 折扣率趋势图显示天数.
     */
    private static final int DAYS = 90;

    @Autowired
    private PaybackReportMapper paybackReportMapper;

    @Autowired
    private NaturalFlowService naturalFlowService;

    /**
     * 回本日报.
     *
     * @return
     */
    @Override
    public List<DailyPaybackReportView> dailyPaybackReport(PaybackQuery query) {
        List<DailyPaybackReport> reports = paybackReportMapper.dailyPaybackReport(query);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            NaturalFlowQuery naturalFlowQuery = toNaturalFlowQuery(query);
            List<DailyPaybackNaturalFlow> naturalFlows = naturalFlowService.getDailyPaybackNaturalFlowsGroupByDate(naturalFlowQuery);
            Map<Date, DailyPaybackReport> reportsByDate = groupByDate(reports);
            for (DailyPaybackNaturalFlow naturalFlow : naturalFlows) {
                if (reportsByDate.containsKey(naturalFlow.getStatDate())) {
                    DailyPaybackReport report = reportsByDate.get(naturalFlow.getStatDate());
                    //充值金额
                    report.setTotalRechargeAmount(NumberUtils.add(report.getTotalRechargeAmount(), naturalFlow.getTotalPayingAmount()));
                    report.setTotalRecharges(NumberUtils.add(report.getTotalRecharges(), naturalFlow.getTotalPayingUsers()));
                    report.setRegistrations(NumberUtils.add(report.getRegistrations(), naturalFlow.getRegistrations()));
                    report.setRegisterAccountNum(NumberUtils.add(report.getRegisterAccountNum(), naturalFlow.getRegisterAccountNum()));
                    report.setDau(NumberUtils.add(report.getDau(), naturalFlow.getDau()));
                    report.setPayAmount3Days(NumberUtils.add(report.getPayAmount3Days(), naturalFlow.getPayAmount3days()));
                    report.setPayAmount7Days(NumberUtils.add(report.getPayAmount7Days(), naturalFlow.getPayAmount7days()));
                    report.setPayAmount15Days(NumberUtils.add(report.getPayAmount15Days(), naturalFlow.getPayAmount15days()));
                    report.setPayAmount30Days(NumberUtils.add(report.getPayAmount30Days(), naturalFlow.getPayAmount30days()));
                    report.setPayAmount60Days(NumberUtils.add(report.getPayAmount60Days(), naturalFlow.getPayAmount60days()));
                    report.setPayAmount90Days(NumberUtils.add(report.getPayAmount90Days(), naturalFlow.getPayAmount90days()));
                    report.setPayAmount1Day(NumberUtils.add(report.getPayAmount1Day(), naturalFlow.getPayAmount1day()));
                    report.setPayAmount2Days(NumberUtils.add(report.getPayAmount2Days(), naturalFlow.getPayAmount2days()));
                    report.setPayAmount4Days(NumberUtils.add(report.getPayAmount4Days(), naturalFlow.getPayAmount4days()));
                    report.setPayAmount5Days(NumberUtils.add(report.getPayAmount5Days(), naturalFlow.getPayAmount5days()));
                    report.setPayAmount6Days(NumberUtils.add(report.getPayAmount6Days(), naturalFlow.getPayAmount6days()));
                    //累计充值
                    report.setAccumulatedRecharge(NumberUtils.add(report.getAccumulatedRecharge(), CurrencyUtils.fenToYuan(naturalFlow.getAccumulatedPayingAmount())));
                    report.setFirstDayRegistrations(NumberUtils.add(report.getFirstDayRegistrations(), naturalFlow.getFirstDayRegistrations()));
                } else {
                    DailyPaybackReport report = new DailyPaybackReport();
                    report.setDate(naturalFlow.getStatDate());
                    report.setTotalRechargeAmount(naturalFlow.getTotalPayingAmount());
                    report.setTotalRecharges(naturalFlow.getTotalPayingUsers());
                    report.setRegisterAccountNum(naturalFlow.getRegisterAccountNum());
                    report.setRegistrations(naturalFlow.getRegistrations());
                    report.setDau(naturalFlow.getDau());
                    report.setPayAmount1Day(naturalFlow.getPayAmount1day());
                    report.setPayAmount3Days(naturalFlow.getPayAmount3days());
                    report.setPayAmount7Days(naturalFlow.getPayAmount7days());
                    report.setPayAmount15Days(naturalFlow.getPayAmount15days());
                    report.setPayAmount30Days(naturalFlow.getPayAmount30days());
                    report.setPayAmount60Days(naturalFlow.getPayAmount60days());
                    report.setPayAmount90Days(naturalFlow.getPayAmount90days());
                    report.setPayAmount2Days(naturalFlow.getPayAmount2days());
                    report.setPayAmount4Days(naturalFlow.getPayAmount4days());
                    report.setPayAmount5Days(naturalFlow.getPayAmount5days());
                    report.setPayAmount6Days(naturalFlow.getPayAmount6days());
                    report.setAccumulatedRecharge(CurrencyUtils.fenToYuan(naturalFlow.getAccumulatedPayingAmount()));
                    report.setFirstDayRegistrations(naturalFlow.getFirstDayRegistrations());
                    reportsByDate.put(naturalFlow.getStatDate(), report);
                }
            }
            reports = new ArrayList<>(reportsByDate.values());
            Collections.sort(reports, new Comparator<DailyPaybackReport>() {
                @Override
                public int compare(DailyPaybackReport o1, DailyPaybackReport o2) {
                    return o2.getDate().compareTo(o1.getDate());
                }
            });
        }
        return formatData(reports);
    }

    private NaturalFlowQuery toNaturalFlowQuery(PaybackQuery query) {
        NaturalFlowQuery naturalFlowQuery = new NaturalFlowQuery();
        naturalFlowQuery.setStartDate(query.getStartDate());
        naturalFlowQuery.setEndDate(query.getEndDate());
        naturalFlowQuery.setCompanyId(query.getCompanyId());
        naturalFlowQuery.setPermissionMediumAccountIds(query.getPermissionMediumAccountIds());
        naturalFlowQuery.setPermissionSystemAccounts(query.getPermissionSystemAccounts());
        naturalFlowQuery.setProductIds(query.getProductIds());
        return naturalFlowQuery;
    }

    /**
     * 按日期分组
     *
     * @param payAmounts
     * @return
     */
    private Map<Date, DailyPaybackReport> groupByDate(List<DailyPaybackReport> payAmounts) {
        Map<Date, DailyPaybackReport> map = new HashMap<>();
        if (payAmounts != null) {
            for (DailyPaybackReport payAmount : payAmounts) {
                map.put(payAmount.getDate(), payAmount);
            }
        }
        return map;
    }

    /**
     * 日回本率趋势
     *
     * @param query 回本率趋势查询
     * @return
     */
    @Override
    public List<PaybackRate> dailyPaybackRateTrend(PaybackTrendQuery query) {
        //消耗单位为分
        BigDecimal totalCost = paybackReportMapper.getTotalCostByDate(query);
        //每日充值，单位为分
        List<PaybackRate> dailyPayAmounts = paybackReportMapper.dailyPaybackRateTrend(query);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            Map<Date, PaybackRate> payAmountsByDate = groupByPayDate(dailyPayAmounts);
            NaturalFlowQuery naturalFlowQuery = toNaturalFlowQuery(query);
            //单位为分
            List<NaturalFlow> naturalFlows = naturalFlowService.dailyNaturalFlowPayAmount(naturalFlowQuery);
            if (naturalFlows != null && !naturalFlows.isEmpty()) {
                for (NaturalFlow naturalFlow : naturalFlows) {
                    if (payAmountsByDate.containsKey(naturalFlow.getStatDate())) {
                        PaybackRate payAmount = payAmountsByDate.get(naturalFlow.getStatDate());
                        payAmount.setPayAmount(NumberUtils.add(payAmount.getPayAmount(), naturalFlow.getRecharge()));
                        payAmount.setDayPayAmount(NumberUtils.add(payAmount.getDayPayAmount(), naturalFlow.getDayPayAmount()));
                    } else {
                        PaybackRate payAmount = new PaybackRate();
                        payAmount.setPayDate(naturalFlow.getStatDate());
                        payAmount.setPayAmount(naturalFlow.getRecharge());
                        payAmount.setDayPayAmount(naturalFlow.getDayPayAmount());
                        payAmountsByDate.put(naturalFlow.getStatDate(), payAmount);
                    }
                }
                dailyPayAmounts = new ArrayList<>(payAmountsByDate.values());
                Collections.sort(dailyPayAmounts, new Comparator<PaybackRate>() {
                    @Override
                    public int compare(PaybackRate o1, PaybackRate o2) {
                        return o1.getPayDate().compareTo(o2.getPayDate());
                    }
                });
            }
        }
        //补全日期
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        calendar.setTime(query.getDate());
        List<PaybackRate> fullPaybackRates = new ArrayList<>();
        int dateCount = 0;
        int index = 0;
        while (dateCount < DAYS && calendar.getTime().before(now)) {
            if (index < dailyPayAmounts.size() && calendar.getTime().equals(dailyPayAmounts.get(index).getPayDate())) {
                PaybackRate paybackRate = dailyPayAmounts.get(index);
                Double pbRate = NumberUtils.rate(paybackRate.getPayAmount(), totalCost);
                if (pbRate == null) {
                    pbRate = 0.0;
                }
                paybackRate.setPaybackRate(pbRate);
                paybackRate.setPayDateText(DateUtils.format(paybackRate.getPayDate(),DateUtils.MONTH_DATE_FORMAT));
                paybackRate.setDayPayAmountText(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(paybackRate.getDayPayAmount())));
                fullPaybackRates.add(paybackRate);
                index++;
            } else {
                //补全缺失的日期
                PaybackRate missingPaybackRate = new PaybackRate();
                missingPaybackRate.setPayDateText(DateUtils.format(calendar.getTime(),DateUtils.MONTH_DATE_FORMAT));
                missingPaybackRate.setPaybackRate(0);
                missingPaybackRate.setDayPayAmountText("0");
                fullPaybackRates.add(missingPaybackRate);
            }
            dateCount++;
            //日期也同步增加一天
            calendar.add(Calendar.DATE, 1);
        }
        return fullPaybackRates;
    }

    private NaturalFlowQuery toNaturalFlowQuery(PaybackTrendQuery query) {
        NaturalFlowQuery naturalFlowQuery = new NaturalFlowQuery();
        naturalFlowQuery.setDate(query.getDate());
        naturalFlowQuery.setProductIds(query.getProductIds());
        naturalFlowQuery.setCompanyId(query.getCompanyId());
        naturalFlowQuery.setPermissionMediumAccountIds(query.getPermissionMediumAccountIds());
        naturalFlowQuery.setPermissionSystemAccounts(query.getPermissionSystemAccounts());
        return naturalFlowQuery;
    }

    /**
     * 按付费日期分组
     *
     * @param dailyPayAmounts
     * @return
     */
    private Map<Date, PaybackRate> groupByPayDate(List<PaybackRate> dailyPayAmounts) {
        Map<Date, PaybackRate> map = new HashMap<>();
        if (dailyPayAmounts != null) {
            for (PaybackRate dailyPayAmount : dailyPayAmounts) {
                map.put(dailyPayAmount.getPayDate(), dailyPayAmount);
            }
        }
        return map;
    }

    /**
     * @param payAmounts
     * @return
     */
    private Map<Date, MonthlyPaybackReport> groupMonthlyPayAmountByDate(List<MonthlyPaybackReport> payAmounts) {
        Map<Date, MonthlyPaybackReport> map = new HashMap<>();
        if (payAmounts != null) {
            for (MonthlyPaybackReport payAmount : payAmounts) {
                map.put(payAmount.getDate(), payAmount);
            }
        }
        return map;
    }

    /**
     * 回本月报.
     *
     * @return
     */
    @Override
    public List<MonthlyPaybackReportView> monthlyPaybackReport(PaybackQuery query) {
        List<MonthlyPaybackReport> reports = this.paybackReportMapper.monthlyPaybackReport(query);
        //添加自然流量
        if (!query.isFilterNaturalFlow()) {
            Map<Date, MonthlyPaybackReport> payAmountsByDate = groupMonthlyPayAmountByDate(reports);
            NaturalFlowQuery naturalFlowQuery = toNaturalFlowQuery(query);
            List<MonthlyPaybackNaturalFlow> naturalFlows = naturalFlowService.getMonthlyPaybackNaturalFlowsGroupByDate(naturalFlowQuery);
            if (naturalFlows != null && !naturalFlows.isEmpty()) {
                for (MonthlyPaybackNaturalFlow naturalFlow : naturalFlows) {
                    if (payAmountsByDate.containsKey(naturalFlow.getStatDate())) {
                        MonthlyPaybackReport report = payAmountsByDate.get(naturalFlow.getStatDate());
                        //
                        report.setTotalRechargeAmount(NumberUtils.add(report.getTotalRechargeAmount(), naturalFlow.getAccumulatedPayingAmount()));
                        report.setRegistrations(NumberUtils.add(report.getRegistrations(), naturalFlow.getRegistrations()));
                        report.setPayAmount1Month(NumberUtils.add(report.getPayAmount1Month(), naturalFlow.getPayAmount1Month()));
                        report.setPayAmount2Months(NumberUtils.add(report.getPayAmount2Months(), naturalFlow.getPayAmount2Months()));
                        report.setPayAmount3Months(NumberUtils.add(report.getPayAmount3Months(), naturalFlow.getPayAmount3Months()));
                        report.setPayAmount4Months(NumberUtils.add(report.getPayAmount4Months(), naturalFlow.getPayAmount4Months()));
                        report.setPayAmount5Months(NumberUtils.add(report.getPayAmount5Months(), naturalFlow.getPayAmount5Months()));
                        report.setPayAmount6Months(NumberUtils.add(report.getPayAmount6Months(), naturalFlow.getPayAmount6Months()));
                        report.setPayAmount7Months(NumberUtils.add(report.getPayAmount7Months(), naturalFlow.getPayAmount7Months()));
                    } else {
                        MonthlyPaybackReport report = new MonthlyPaybackReport();
                        report.setDate(naturalFlow.getStatDate());
                        report.setTotalRechargeAmount(naturalFlow.getAccumulatedPayingAmount());
                        report.setRegistrations(naturalFlow.getRegistrations());
                        report.setPayAmount1Month(naturalFlow.getPayAmount1Month());
                        report.setPayAmount2Months(naturalFlow.getPayAmount2Months());
                        report.setPayAmount3Months(naturalFlow.getPayAmount3Months());
                        report.setPayAmount4Months(naturalFlow.getPayAmount4Months());
                        report.setPayAmount5Months(naturalFlow.getPayAmount5Months());
                        report.setPayAmount6Months(naturalFlow.getPayAmount6Months());
                        report.setPayAmount7Months(naturalFlow.getPayAmount7Months());
                        payAmountsByDate.put(naturalFlow.getStatDate(), report);
                    }
                }
                reports = new ArrayList<>(payAmountsByDate.values());
                Collections.sort(reports, new Comparator<MonthlyPaybackReport>() {
                    @Override
                    public int compare(MonthlyPaybackReport o1, MonthlyPaybackReport o2) {
                        return o2.getDate().compareTo(o1.getDate());
                    }
                });
            }
        }
        return formatMonthlyData(reports, query);
    }

    /**
     * 格式化数据
     *
     * @param reports
     */
    private List<DailyPaybackReportView> formatData(List<DailyPaybackReport> reports) {
        List<DailyPaybackReportView> views = new ArrayList<>();
        //汇总行数据准备
        DailyPaybackReport total = new DailyPaybackReport();
        if(reports.size() > 0){
             DailyPaybackReportView totalViews = new DailyPaybackReportView();
             totalViews.setDate("汇总");
             views.add(totalViews);
        }
        Map<String,BigDecimal> realCostMap = new HashMap<String, BigDecimal>();
        realCostMap.put("realCost1Days", null);
        realCostMap.put("realCost2Days", null);
        realCostMap.put("realCost3Days", null);
        realCostMap.put("realCost4Days", null);
        realCostMap.put("realCost5Days", null);
        realCostMap.put("realCost6Days", null);
        realCostMap.put("realCost7Days", null);
        realCostMap.put("realCost15Days", null);
        realCostMap.put("realCost30Days", null);
        realCostMap.put("realCost60Days", null);
        realCostMap.put("realCost90Days", null);

        for (DailyPaybackReport report : reports) {
            //格式化回本率百分比
            DailyPaybackReportView view = new DailyPaybackReportView();
            view.setPaybackRateAfter1days(paybackRate(report.getPayAmount1Day(), report.getRealCost()));
            view.setPaybackRateAfter2days(paybackRate(report.getPayAmount2Days(), report.getRealCost()));
            view.setPaybackRateAfter3days(paybackRate(report.getPayAmount3Days(), report.getRealCost()));
            view.setPaybackRateAfter4days(paybackRate(report.getPayAmount4Days(), report.getRealCost()));
            view.setPaybackRateAfter5days(paybackRate(report.getPayAmount5Days(), report.getRealCost()));
            view.setPaybackRateAfter6days(paybackRate(report.getPayAmount6Days(), report.getRealCost()));
            view.setPaybackRateAfter7days(paybackRate(report.getPayAmount7Days(), report.getRealCost()));
            view.setPaybackRateAfter15days(paybackRate(report.getPayAmount15Days(), report.getRealCost()));
            view.setPaybackRateAfter30days(paybackRate(report.getPayAmount30Days(), report.getRealCost()));
            view.setPaybackRateAfter60days(paybackRate(report.getPayAmount60Days(), report.getRealCost()));
            view.setPaybackRateAfter90days(paybackRate(report.getPayAmount90Days(), report.getRealCost()));
           // view.setCpa(CurrencyUtils.yuanToText(computeCpa(report)));
            view.setTotalCost(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(report.getTotalCost())));
            //ARPU
            if (report.getTotalRechargeAmount() != null && report.getDau() != null && report.getDau() > 0) {
                view.setArpu(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(report.getTotalRechargeAmount().divide(new BigDecimal(report.getDau()), 4, RoundingMode.HALF_UP))));
            }else{
            	view.setArpu(Constants.UNKNOWN);
            }
            //ARPPU
            if (report.getTotalRechargeAmount() != null && report.getTotalRecharges() != null && report.getTotalRecharges() > 0) {
                view.setArppu(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(report.getTotalRechargeAmount().divide(new BigDecimal(report.getTotalRecharges()), 4, RoundingMode.HALF_UP))));
            }else{
            	view.setArppu(Constants.UNKNOWN);
            }
            //付费率
            if (report.getTotalRecharges() != null && report.getDau() != null && report.getDau() > 0) {
                view.setPayRate(new BigDecimal(report.getTotalRecharges()).divide(new BigDecimal(report.getDau()), 4, RoundingMode.HALF_UP).toString());
            }else{
            	view.setPayRate(Constants.UNKNOWN);
            }
            //首日注册CPA(设备)
            if (report.getTotalCost() != null && report.getFirstDayRegistrations() != null && report.getFirstDayRegistrations() > 0) {
            	view.setRegistrationCpa(CurrencyUtils.fenToYuan(report.getTotalCost()).divide(new BigDecimal(report.getFirstDayRegistrations()), 2, RoundingMode.HALF_UP).toString());
            }else{
            	view.setRegistrationCpa(Constants.UNKNOWN);
            }
            view.setDate(DateUtils.format(report.getDate()));
            //充值金额
            view.setTotalRechargeAmount(CurrencyUtils.fenToYuan(report.getTotalRechargeAmount()));
            view.setTotalRecharges(report.getTotalRecharges());
            view.setRegisterAccountNum(report.getRegisterAccountNum());
            //view.setRegistrations(report.getRegistrations());
            view.setDau(report.getDau());
            view.setPayAmount1Day(CurrencyUtils.fenToYuan(report.getPayAmount1Day()));
            view.setPayAmount2Day(CurrencyUtils.fenToYuan(report.getPayAmount2Days()));
            view.setPayAmount3Day(CurrencyUtils.fenToYuan(report.getPayAmount3Days()));
            view.setPayAmount4Day(CurrencyUtils.fenToYuan(report.getPayAmount4Days()));
            view.setPayAmount5Day(CurrencyUtils.fenToYuan(report.getPayAmount5Days()));
            view.setPayAmount6Day(CurrencyUtils.fenToYuan(report.getPayAmount6Days()));
            view.setPayAmount7Day(CurrencyUtils.fenToYuan(report.getPayAmount7Days()));
            view.setPayAmount15Day(CurrencyUtils.fenToYuan(report.getPayAmount15Days()));
            view.setPayAmount30Day(CurrencyUtils.fenToYuan(report.getPayAmount30Days()));
            view.setPayAmount60Day(CurrencyUtils.fenToYuan(report.getPayAmount60Days()));
            view.setPayAmount90Day(CurrencyUtils.fenToYuan(report.getPayAmount90Days()));
            //累计充值
            BigDecimal accumulatedRecharge = new BigDecimal(0.00);
            if(report.getAccumulatedRecharge() != null){
            	accumulatedRecharge = report.getAccumulatedRecharge().setScale(2, RoundingMode.HALF_UP);
            }
            view.setAccumulatedRecharge(accumulatedRecharge);
            
            views.add(view);

          ///////////////计算汇总行的数据//////////////////////////////////////////////
          total.setTotalRechargeAmount(NumberUtils.add(report.getTotalRechargeAmount(), total.getTotalRechargeAmount()));
          total.setTotalRecharges(NumberUtils.add(report.getTotalRecharges(), total.getTotalRecharges()));
          total.setTotalCost(NumberUtils.add(report.getTotalCost(), total.getTotalCost()));
          total.setRegisterAccountNum(NumberUtils.add(report.getRegisterAccountNum(), total.getRegisterAccountNum()));
          total.setDau(NumberUtils.add(report.getDau(), total.getDau()));
          //total.setRegistrations(NumberUtils.add(report.getRegistrations(),total.getRegistrations()));
          //total.setRealCost(NumberUtils.add(report.getRealCost(), total.getRealCost()));
          //累计充值
          total.setAccumulatedRecharge(NumberUtils.add(report.getAccumulatedRecharge(), total.getAccumulatedRecharge()));
          total.setFirstDayRegistrations(NumberUtils.add(report.getFirstDayRegistrations(), total.getFirstDayRegistrations()));
          
          String nowDate = DateUtils.now();
          
          if (comparePayDate(report.getDate(), nowDate, 1)){
      		realCostMap.put("realCost1Days", NumberUtils.add(realCostMap.get("realCost1Days"), report.getRealCost()));
      		total.setPayAmount1Day(NumberUtils.add(report.getPayAmount1Day(), total.getPayAmount1Day()));
          }
          if (comparePayDate(report.getDate(), nowDate, 2)){
        		realCostMap.put("realCost2Days", NumberUtils.add(realCostMap.get("realCost2Days"), report.getRealCost()));
        		total.setPayAmount2Days(NumberUtils.add(report.getPayAmount2Days(), total.getPayAmount2Days()));
           }
         //如果计算3日充值，如果当前日期还没有3日充值，则当天的消耗不添加到总消耗中，如果当前日期到了3日充值的时间，只不过没有3日充值金额，则当前日期的消耗也汇总到总消耗中。
          if (comparePayDate(report.getDate(), nowDate, 3)){
      		realCostMap.put("realCost3Days", NumberUtils.add(realCostMap.get("realCost3Days"), report.getRealCost()));
      		total.setPayAmount3Days(NumberUtils.add(report.getPayAmount3Days(), total.getPayAmount3Days()));
          }
          if (comparePayDate(report.getDate(), nowDate, 4)){
      		realCostMap.put("realCost4Days", NumberUtils.add(realCostMap.get("realCost4Days"), report.getRealCost()));
      		total.setPayAmount4Days(NumberUtils.add(report.getPayAmount4Days(), total.getPayAmount4Days()));
         }
          if (comparePayDate(report.getDate(), nowDate, 5)){
      		realCostMap.put("realCost5Days", NumberUtils.add(realCostMap.get("realCost5Days"), report.getRealCost()));
      		total.setPayAmount5Days(NumberUtils.add(report.getPayAmount5Days(), total.getPayAmount5Days()));
         }
          if (comparePayDate(report.getDate(), nowDate, 6)){
      		realCostMap.put("realCost6Days", NumberUtils.add(realCostMap.get("realCost6Days"), report.getRealCost()));
      		total.setPayAmount6Days(NumberUtils.add(report.getPayAmount6Days(), total.getPayAmount6Days()));
         }
          if (comparePayDate(report.getDate(), nowDate, 7)){
      		realCostMap.put("realCost7Days", NumberUtils.add(realCostMap.get("realCost7Days"), report.getRealCost()));
      		total.setPayAmount7Days(NumberUtils.add(report.getPayAmount7Days(), total.getPayAmount7Days()));
          }
          if (comparePayDate(report.getDate(), nowDate, 15)){
      		realCostMap.put("realCost15Days", NumberUtils.add(realCostMap.get("realCost15Days"), report.getRealCost()));
      		total.setPayAmount15Days(NumberUtils.add(report.getPayAmount15Days(), total.getPayAmount15Days()));
          }
          if (comparePayDate(report.getDate(), nowDate, 30)){
      		realCostMap.put("realCost30Days", NumberUtils.add(realCostMap.get("realCost30Days"), report.getRealCost()));
      		total.setPayAmount30Days(NumberUtils.add(report.getPayAmount30Days(), total.getPayAmount30Days()));
          }
          if (comparePayDate(report.getDate(), nowDate, 60)){
      		realCostMap.put("realCost60Days", NumberUtils.add(realCostMap.get("realCost60Days"), report.getRealCost()));
      		total.setPayAmount60Days(NumberUtils.add(report.getPayAmount60Days(), total.getPayAmount60Days()));
          }
          if (comparePayDate(report.getDate(), nowDate, 90)){
      		realCostMap.put("realCost90Days", NumberUtils.add(realCostMap.get("realCost90Days"), report.getRealCost()));
      		total.setPayAmount90Days(NumberUtils.add(report.getPayAmount90Days(), total.getPayAmount90Days()));
          }
        }
        //填充汇总行数据
        if(reports.size() > 0){
        	DailyPaybackReportView totalViews = views.get(0);
        	if(totalViews.getDate().equals("汇总")){

        		totalViews.setTotalRechargeAmount(CurrencyUtils.fenToYuan(total.getTotalRechargeAmount()));
        		totalViews.setTotalRecharges(total.getTotalRecharges());
                totalViews.setRegisterAccountNum(total.getRegisterAccountNum());
        		totalViews.setTotalCost(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(total.getTotalCost())));
        		//totalViews.setRegistrations(total.getRegistrations());
        		//累计充值
        		BigDecimal accumulatedRecharge = new BigDecimal(0.00);
                if(total.getAccumulatedRecharge() != null){
                	accumulatedRecharge = total.getAccumulatedRecharge().setScale(2, RoundingMode.HALF_UP);
                }
                totalViews.setAccumulatedRecharge(accumulatedRecharge);
        		 //首日注册CPA(设备)
        		if (total.getTotalCost() != null && total.getFirstDayRegistrations() != null && total.getFirstDayRegistrations() > 0) {
                	totalViews.setRegistrationCpa(CurrencyUtils.fenToYuan(total.getTotalCost()).divide(new BigDecimal(total.getFirstDayRegistrations()), 2, RoundingMode.HALF_UP).toString());
                }else{
                	totalViews.setRegistrationCpa(Constants.UNKNOWN);
                }
              //ARPU
                if (total.getTotalRechargeAmount() != null && total.getDau() != null && total.getDau() > 0) {
                	totalViews.setArpu(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(total.getTotalRechargeAmount().divide(new BigDecimal(total.getDau()), 4, RoundingMode.HALF_UP))));
                }else{
                	totalViews.setArpu(Constants.UNKNOWN);
                }
                //ARPPU
                if (total.getTotalRechargeAmount() != null && total.getTotalRecharges() != null && total.getTotalRecharges() > 0) {
                	totalViews.setArppu(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(total.getTotalRechargeAmount().divide(new BigDecimal(total.getTotalRecharges()), 4, RoundingMode.HALF_UP))));
                }else{
                	totalViews.setArppu(Constants.UNKNOWN);
                }
                //付费率
                if (total.getTotalRecharges() != null && total.getDau() != null && total.getDau() > 0) {
                	totalViews.setPayRate(new BigDecimal(total.getTotalRecharges()).divide(new BigDecimal(total.getDau()), 4, RoundingMode.HALF_UP).toString());
                }else{
                	totalViews.setPayRate(Constants.UNKNOWN);
                }
                
                //totalViews.setCpa(CurrencyUtils.yuanToText(computeCpa(total)));
                totalViews.setPaybackRateAfter1days(paybackRate(total.getPayAmount1Day(), realCostMap.get("realCost1Days")));
                totalViews.setPaybackRateAfter3days(paybackRate(total.getPayAmount3Days(), realCostMap.get("realCost3Days")));
        		totalViews.setPaybackRateAfter7days(paybackRate(total.getPayAmount7Days(), realCostMap.get("realCost7Days")));
        		totalViews.setPaybackRateAfter15days(paybackRate(total.getPayAmount15Days(), realCostMap.get("realCost15Days")));
        		totalViews.setPaybackRateAfter30days(paybackRate(total.getPayAmount30Days(), realCostMap.get("realCost30Days")));
        		totalViews.setPaybackRateAfter60days(paybackRate(total.getPayAmount60Days(), realCostMap.get("realCost60Days")));
                totalViews.setPaybackRateAfter90days(paybackRate(total.getPayAmount90Days(), realCostMap.get("realCost90Days")));
                totalViews.setPaybackRateAfter2days(paybackRate(total.getPayAmount2Days(), realCostMap.get("realCost2Days")));
                totalViews.setPaybackRateAfter4days(paybackRate(total.getPayAmount4Days(), realCostMap.get("realCost4Days")));
                totalViews.setPaybackRateAfter5days(paybackRate(total.getPayAmount5Days(), realCostMap.get("realCost5Days")));
                totalViews.setPaybackRateAfter6days(paybackRate(total.getPayAmount6Days(), realCostMap.get("realCost6Days")));
                totalViews.setPayAmount1Day(CurrencyUtils.fenToYuan(total.getPayAmount1Day()));
                totalViews.setPayAmount2Day(CurrencyUtils.fenToYuan(total.getPayAmount2Days()));
                totalViews.setPayAmount3Day(CurrencyUtils.fenToYuan(total.getPayAmount3Days()));
                totalViews.setPayAmount4Day(CurrencyUtils.fenToYuan(total.getPayAmount4Days()));
                totalViews.setPayAmount5Day(CurrencyUtils.fenToYuan(total.getPayAmount5Days()));
                totalViews.setPayAmount6Day(CurrencyUtils.fenToYuan(total.getPayAmount6Days()));
                totalViews.setPayAmount7Day(CurrencyUtils.fenToYuan(total.getPayAmount7Days()));
                totalViews.setPayAmount15Day(CurrencyUtils.fenToYuan(total.getPayAmount15Days()));
                totalViews.setPayAmount30Day(CurrencyUtils.fenToYuan(total.getPayAmount30Days()));
                totalViews.setPayAmount60Day(CurrencyUtils.fenToYuan(total.getPayAmount60Days()));
                totalViews.setPayAmount90Day(CurrencyUtils.fenToYuan(total.getPayAmount90Days()));

        	}
        }

        return views;
    }

    private Double paybackRate(BigDecimal payAmount, BigDecimal realCost) {
        if (payAmount != null && realCost != null && realCost.compareTo(new BigDecimal(0)) > 0) {
            return payAmount.divide(realCost, 4, RoundingMode.HALF_UP).doubleValue();
        } else {
            return null;
        }
    }

    /**
     * 计算CPA.
     *
     * @param report
     */
    private <T extends BasicPaybackReport> BigDecimal computeCpa(T report) {
        //格式化CPA
        if (report.getRegistrations() != null && report.getTotalCost() != null && report.getRegistrations() == 0 && report.getTotalCost().compareTo(new BigDecimal(5000)) > 0) {
            //当注册数为0且消耗大于50元时，注册CPA设为消耗
            return CurrencyUtils.fenToYuan(report.getTotalCost());
        } else if (report.getTotalCost() != null && report.getRegistrations() != null && report.getRegistrations() > 0) {
            return CurrencyUtils.fenToYuan(report.getTotalCost().divide(new BigDecimal(report.getRegistrations()), 4, RoundingMode.HALF_UP));
        } else {
            return null;
        }
    }

//    /**
//     * 格式化为百分比
//     *
//     * @param rate
//     * @return
//     */
//    private String formatAsPercent(Double rate) {
//        if (rate == null) {
//            return Constants.UNKNOWN;
//        } else {
//            NumberFormat nf = NumberFormat.getPercentInstance();
//            //保留两位小数
//            nf.setMinimumFractionDigits(2);
//            //四舍五入
//            nf.setRoundingMode(RoundingMode.HALF_UP);
//            return nf.format(rate);
//        }
//    }


    /**
     * 格式化数据
     *
     * @param reports
     */
    private List<MonthlyPaybackReportView> formatMonthlyData(List<MonthlyPaybackReport> reports, PaybackQuery query) {
        List<MonthlyPaybackReportView> views = new ArrayList<>();
        for (MonthlyPaybackReport report : reports) {
            //格式化回本率百分比
            MonthlyPaybackReportView view = new MonthlyPaybackReportView();

            view.setPaybackRateAfter1Month(paybackRate(report.getPayAmount1Month(), report.getRealCost()));
            view.setPaybackRateAfter2Months(paybackRate(report.getPayAmount2Months(), report.getRealCost()));
            view.setPaybackRateAfter3Months(paybackRate(report.getPayAmount3Months(), report.getRealCost()));
            view.setPaybackRateAfter4Months(paybackRate(report.getPayAmount4Months(), report.getRealCost()));
            view.setPaybackRateAfter5Months(paybackRate(report.getPayAmount5Months(), report.getRealCost()));
            view.setPaybackRateAfter6Months(paybackRate(report.getPayAmount6Months(), report.getRealCost()));
            view.setPaybackRateAfter7Months(paybackRate(report.getPayAmount7Months(), report.getRealCost()));

//            view.setPaybackRatePercentAfter1Month(formatAsPercent(report.getPaybackRateAfter1Month()));
//            view.setPaybackRatePercentAfter2Months(formatAsPercent(report.getPaybackRateAfter2Months()));
//            view.setPaybackRatePercentAfter3Months(formatAsPercent(report.getPaybackRateAfter3Months()));
//            view.setPaybackRatePercentAfter4Months(formatAsPercent(report.getPaybackRateAfter4Months()));
//            view.setPaybackRatePercentAfter5Months(formatAsPercent(report.getPaybackRateAfter5Months()));
//            view.setPaybackRatePercentAfter6Months(formatAsPercent(report.getPaybackRateAfter6Months()));
//            view.setPaybackRatePercentAfter7Months(formatAsPercent(report.getPaybackRateAfter7Months()));
            //view.setCpa(CurrencyUtils.yuanToText(computeCpa(report)));
            view.setTotalCost(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(report.getTotalCost())));
            if (report.getTotalRechargeAmount() != null && report.getRegistrations() != null && report.getRegistrations() > 0) {
                view.setArpu(CurrencyUtils.yuanToText(CurrencyUtils.fenToYuan(report.getTotalRechargeAmount().divide(new BigDecimal(report.getRegistrations()), 4, RoundingMode.HALF_UP))));
            }
            view.setDate(dateRange(report.getDate(), query.getStartDate(), query.getEndDate()));
            view.setTotalRechargeAmount(CurrencyUtils.fenToYuan(report.getTotalRechargeAmount()));
           // view.setRegistrations(report.getRegistrations());
            views.add(view);
        }
        return views;
    }

    /**
     * 获取月回本统计日期范围.
     *
     * @param date
     * @param startDate
     * @param endDate
     * @return
     */
    private String dateRange(Date date, Date startDate, Date endDate) {
        //起始日期.
        Date sDate = date;
        if (startDate.after(date)) {
            sDate = startDate;
        }

        //截止日期
        Date eDate = DateUtils.lastDayOfMonth(date);
        if (endDate.before(eDate)) {
            eDate = endDate;
        }
        StringBuilder dateRange = new StringBuilder();
        dateRange.append(DateUtils.format(sDate)).append(Constants.TO).append(DateUtils.format(eDate));
        return dateRange.toString();
    }

    /**
     * 计算投放日期是否具有X日充值
     * @param statDate 投放日期
     * @param nowDate 当前日期
     * @param num  如果输入3，则表示计算statDate是否具有3日回本，如果具备3日充值的时间，则返回true，否则返回false,num-1是投放的那一天也算一天
     * @return
     * **/
    private boolean comparePayDate(Date statDate,String nowDate,Integer num){
    	num = num-1;
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(statDate);
    	cal.add(Calendar.DATE, num);
    	String statDateStr = DateUtils.format(cal.getTime());
    	DateFormat df = new SimpleDateFormat(DateUtils.DATE_FORMAT);
    	if(statDate != null && num >= 0 && DateUtils.after(nowDate,statDateStr , df)){
    		return true;
    	}
    	return false;
    }
}
