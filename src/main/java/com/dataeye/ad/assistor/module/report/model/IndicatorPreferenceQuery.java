package com.dataeye.ad.assistor.module.report.model;

/**
 * 指标偏好查询
 * Created by huangzehai on 2017/6/8.
 */
public class IndicatorPreferenceQuery {
    private int accountId;
    private String menuId;
    /**
     * 仅供判断是否是企业账号，然后给企业账号初始化指标偏好
     */
    private int accountRole;

    /**
     * 公司ID. 仅用于初始化企业账号指标偏好
     */
    private int companyId;

    /**
     * 是否已经初始化指标偏好.
     */
    private boolean isInitialized;

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public int getAccountRole() {
        return accountRole;
    }

    public void setAccountRole(int accountRole) {
        this.accountRole = accountRole;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    public void setInitialized(boolean initialized) {
        isInitialized = initialized;
    }

    @Override
    public String toString() {
        return "IndicatorPreferenceQuery{" +
                "accountId=" + accountId +
                ", menuId='" + menuId + '\'' +
                ", accountRole=" + accountRole +
                ", companyId=" + companyId +
                ", isInitialized=" + isInitialized +
                '}';
    }
}
