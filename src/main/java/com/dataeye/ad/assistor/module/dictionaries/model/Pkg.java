package com.dataeye.ad.assistor.module.dictionaries.model;

import java.util.Date;

import com.google.gson.annotations.Expose;

/**
 * 包表
 * @author luzhuyou 2017/03/02
 *
 */
public class Pkg {

	/** 包ID */
	@Expose
	private Integer pkgId;
	/** 包名称 */
	@Expose
	private String pkgName;
	/** CP应用ID */
	private String cpAppId;
	/** CP包ID */
	@Expose
	private String cpPkgId;
	/** 公司ID */
	private Integer companyId;
	/** 状态：0-正常，1-删除 */
	private Integer status;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	public Integer getPkgId() {
		return pkgId;
	}
	public void setPkgId(Integer pkgId) {
		this.pkgId = pkgId;
	}
	public String getPkgName() {
		return pkgName;
	}
	public void setPkgName(String pkgName) {
		this.pkgName = pkgName;
	}
	public String getCpAppId() {
		return cpAppId;
	}
	public void setCpAppId(String cpAppId) {
		this.cpAppId = cpAppId;
	}
	public String getCpPkgId() {
		return cpPkgId;
	}
	public void setCpPkgId(String cpPkgId) {
		this.cpPkgId = cpPkgId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "Pkg [pkgId=" + pkgId + ", pkgName=" + pkgName + ", cpAppId="
				+ cpAppId + ", cpPkgId=" + cpPkgId + ", companyId=" + companyId
				+ ", status=" + status + ", remark=" + remark + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}
}
