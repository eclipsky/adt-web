package com.dataeye.ad.assistor.module.realtimedelivery.model;

import org.apache.commons.lang.StringUtils;

/**
 * 趋势图时段
 * Created by huangzehai on 2017/2/22.
 */
public enum Period {
    Hour(1, 60, "小时"), HalfAnHour(2, 30, "三十分钟"), TwentyMinutes(3, 20, "二十分钟"), TenMinutes(4, 10, "十分钟");

    private int intervalInMinutes;
    private String label;
    private int value;

    public int getIntervalInMinutes() {
        return intervalInMinutes;
    }

    public void setIntervalInMinutes(int intervalInMinutes) {
        this.intervalInMinutes = intervalInMinutes;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    Period(int value, int intervalsInMinutes, String label) {
        this.value = value;
        this.intervalInMinutes = intervalsInMinutes;
        this.label = label;
    }

    public static Period parse(String text) {
        for (Period period : Period.values()) {
            if (StringUtils.equalsIgnoreCase(period.name(), text)) {
                return period;
            }
        }
        return null;
    }

    public static Period parseByValue(int value) {
        for (Period period : Period.values()) {
            if (period.getValue() == value) {
                return period;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Period{" +
                "intervalInMinutes=" + intervalInMinutes +
                ", label='" + label + '\'' +
                ", value=" + value +
                '}';
    }
}
