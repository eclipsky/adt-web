package com.dataeye.ad.assistor.module.realtimedelivery.controller;

import static com.dataeye.ad.assistor.module.realtimedelivery.constant.Constants.ENABLE_BID_SERVICE;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.BidStrategy;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Bid;
import com.dataeye.ad.assistor.module.realtimedelivery.service.BidService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;

/**
 * 出价控制器.
 * Created by huangzehai on 2017/5/15.
 */
@Controller
public class BidController {
    private Logger logger = LoggerFactory.getLogger(BidController.class);
    /**
     * 今日头条OCPC第一阶段
     */
    private static final String PHASE_1 = "1";
    /**
     * 今日头条OCPC第二阶段
     */
    private static final String PHASE_2 = "2";

    private static final String BID_SUCCESS_MESSAGE = "修改出价成功!";

    @Autowired
    @Qualifier("bidService")
    private BidService bidService;

    /**
     * 出价.
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/realtimedelivery/bid.do")
    public Object bid(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        DEParameter parameter = context.getDeParameter();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
//        if (!userInSession.hasManagerPermission()) {
//            return null;
//        }
        String b = parameter.getParameter(DEParameter.Keys.BID);
        int mediumId = parameter.getMediumId();
        String mediumAccountId = parameter.getParameter(DEParameter.Keys.MEDIUM_ACCOUNT_ID);
        String mediumPlanId = parameter.getParameter(DEParameter.Keys.MEDIUM_PLAN_ID);
        String planId = parameter.getParameter(DEParameter.Keys.PLAN_ID);
        String bidStrategy = parameter.getParameter(DEParameter.Keys.BID_STRATEGY);
        String phase = parameter.getParameter(DEParameter.Keys.PHASE);

        //校验参数
        if (StringUtils.isBlank(b) || !NumberUtils.isNumber(b)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_BID_MISSING);
        }

        if (mediumId <= 0) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_MEDIUM_ID_MISSING);
        }

        if (StringUtils.isBlank(mediumAccountId) || !NumberUtils.isDigits(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }

        if (StringUtils.isBlank(mediumPlanId) || !NumberUtils.isDigits(mediumPlanId)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_MEDIUM_PLAN_ID_MISSING);
        }

        if (StringUtils.isBlank(planId) || !NumberUtils.isDigits(planId)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_PLAN_ID_MISSING);
        }

        if (StringUtils.isBlank(bidStrategy)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_BID_STRATEGY_MISSING);
        }

        if (StringUtils.equalsIgnoreCase(BidStrategy.OCPC, bidStrategy) && StringUtils.isBlank(phase)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_BID_PHASE_MISSING);
        }

        //检查用户是否拥有修改出价的权限
        List<Integer> managedMediumAccountIds = Arrays.asList(userInSession.getOptimizerPermissionMediumAccountIds());
        if (!managedMediumAccountIds.contains(Integer.valueOf(mediumAccountId))) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_NO_PRIVILEGE_OF_BID);
        }

        //构建出价对象.
        Bid bid = new Bid();
        bid.setMediumId(mediumId);
        bid.setMediumAccountId(Integer.valueOf(mediumAccountId));
        bid.setMediumPlanId(Long.valueOf(mediumPlanId));
        bid.setBid(new BigDecimal(b));
        bid.setBidStrategy(bidStrategy);
        bid.setPlanId(Integer.valueOf(planId));
        bid.setAccountId(userInSession.getAccountId());
        bid.setCompanyId(userInSession.getCompanyId());
        //今日头条OCPC出价
        if (StringUtils.isNotBlank(phase)) {
            if (StringUtils.equalsIgnoreCase(PHASE_1, phase)) {
                bid.setIsCpaBid(0);
            } else if (StringUtils.equalsIgnoreCase(PHASE_2, phase)) {
                bid.setIsCpaBid(1);
            }
        }


        String enableBidService = ConfigHandler.getProperty(ENABLE_BID_SERVICE, "false");
        if (Boolean.valueOf(enableBidService)) {
            //启用出价服务时
            logger.info("Update bid: {}", bid);
            return bidService.bid(bid);
        } else {
            //禁用出价服务时
            logger.info("Not update bid as bid service has been disabled.");
            Result result = new Result();
            result.setSuccess(true);
            result.setMessage(BID_SUCCESS_MESSAGE);
            return result;
        }
    }
}
