package com.dataeye.ad.assistor.module.mediumaccount.model;

import com.dataeye.ad.assistor.common.Item;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by huangzehai on 2017/4/25.
 */
public class Optimizers {
    @Expose
    private List<Item<String, String>> optimizers;
    @Expose
    private Item<String, String> defaultOptimizer;

    public List<Item<String, String>> getOptimizers() {
        return optimizers;
    }

    public void setOptimizers(List<Item<String, String>> optimizers) {
        this.optimizers = optimizers;
    }

    public Item<String, String> getDefaultOptimizer() {
        return defaultOptimizer;
    }

    public void setDefaultOptimizer(Item<String, String> defaultOptimizer) {
        this.defaultOptimizer = defaultOptimizer;
    }

    @Override
    public String toString() {
        return "Optimizers{" +
                "optimizers=" + optimizers +
                ", defaultOptimizer=" + defaultOptimizer +
                '}';
    }
}
