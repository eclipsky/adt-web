package com.dataeye.ad.assistor.module.report.model;

import com.google.gson.annotations.Expose;

import java.util.List;
import java.util.Map;

/**
 * Created by huangzehai on 2017/1/4.
 * 内部表格.
 */
public class InnerTable {
    /**
     * 列名
     */
    @Expose
    private Map<String, Object> column;

    /**
     * 当前页数据
     */
    @Expose
    private List<DeliveryReportView> content;

    public Map<String, Object> getColumn() {
        return column;
    }

    public void setColumn(Map<String, Object> column) {
        this.column = column;
    }

    public List<DeliveryReportView> getContent() {
        return content;
    }

    public void setContent(List<DeliveryReportView> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "InnerTable{" +
                "column=" + column +
                ", content=" + content +
                '}';
    }
}
