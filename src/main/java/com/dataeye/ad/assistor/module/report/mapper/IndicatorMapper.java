package com.dataeye.ad.assistor.module.report.mapper;

import com.dataeye.ad.assistor.module.report.model.Indicator;
import com.dataeye.ad.assistor.module.report.model.IndicatorHabit;
import com.dataeye.ad.assistor.module.report.model.IndicatorPreferenceQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by huangzehai on 2017/6/8.
 */
public interface IndicatorMapper {
    /**
     * 列出所有的指标.
     *
     * @return
     */
    List<Indicator> listIndicators();

    /**
     * 获取用户的某个菜单的指标偏好.
     *
     * @param query
     * @return
     */
    String getIndicatorPreference(IndicatorPreferenceQuery query);

    /**
     * 判断是否存在指定系统账号的指标偏好
     *
     * @param accountId
     * @return
     */
    boolean hasIndicatorPreference(@Param("accountId") int accountId);

    /**
     * 插入指标偏好
     *
     * @param indicatorHabit
     * @return
     */
    int insertIndicatorPreference(IndicatorHabit indicatorHabit);

    /**
     * 更新用户的指标偏好.
     *
     * @param indicatorHabit
     * @return
     */
    int updateIndicatorPreference(IndicatorHabit indicatorHabit);

    /**
     * 删除系统账号的指标偏好.
     *
     * @param accountId
     * @return
     */
    int deleteIndicatorPreferenceByAccountId(@Param("accountId") int accountId);
}
