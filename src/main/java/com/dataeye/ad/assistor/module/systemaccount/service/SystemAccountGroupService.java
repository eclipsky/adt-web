package com.dataeye.ad.assistor.module.systemaccount.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.context.ApplicationContextContainer;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants;
import com.dataeye.ad.assistor.module.systemaccount.mapper.SystemAccountGroupMapper;
import com.dataeye.ad.assistor.module.systemaccount.model.GroupBasicVo;
import com.dataeye.ad.assistor.module.systemaccount.model.GroupDetailVo;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountGroup;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;

/**
 * 系统账号分组Service
 * @author luzhuyou 2017/02/17
 *
 */
@Service("systemAccountGroupService")
public class SystemAccountGroupService {
	
	@Autowired
	private SystemAccountGroupMapper systemAccountGroupMapper;
	@Autowired
    private SystemAccountService systemAccountService;
	
	/**
	 * 查询基本分组信息列表
	 * @param companyId
	 * @return
	 */
	public List<GroupBasicVo> queryBasicGroup(int companyId) {
		List<GroupBasicVo> basicGroup = systemAccountGroupMapper.queryBasicGroup(companyId);
		GroupBasicVo sumGroup = new GroupBasicVo();
		sumGroup.setCompanyId(companyId);
		sumGroup.setGroupId(Constants.GroupId.ALL);
		sumGroup.setGroupName(Constants.DEFALUT_ALL_GROUP_NAME);
		sumGroup.setGroupType(Constants.GroupType.ALL);
		sumGroup.setMemberNumber(0);
		
		if(basicGroup != null) {
			for(GroupBasicVo vo : basicGroup) {
				if(vo.getGroupId() == -1) {
					vo.setGroupType(Constants.GroupType.NONE);
					vo.setGroupName(Constants.DEFALUT_NONE_GROUP_NAME);
				} else {
					vo.setGroupType(Constants.GroupType.DETAIL);
				}
				sumGroup.setMemberNumber(sumGroup.getMemberNumber() + vo.getMemberNumber());
			}
		} else {
			basicGroup = new ArrayList<GroupBasicVo>();
		}
		
		basicGroup.add(0, sumGroup);
		return basicGroup;
	}

	/**
	 * 查询分组系统账户列表
	 * @param groupId
	 * @param companyId
	 * @return
	 */
	public List<GroupDetailVo> query(Integer groupId, Integer companyId) {
		SystemAccountGroup group = new SystemAccountGroup();
		group.setCompanyId(companyId);
		group.setGroupId(groupId);
		List<GroupDetailVo> detailVoList = systemAccountGroupMapper.query(group);
		if(detailVoList == null || detailVoList.isEmpty()) {
			return detailVoList;
		}
		// 根据组长账号查询对应系统账号信息
		String leaderAccountName  = detailVoList.get(0).getLeaderAccount();
		int oriSize = detailVoList.size();
		// 如果没有组员的情况下，会查询出一条分组信息，再判断分组信息中组长是否存在，如果不存在，则移除该条记录，否则，移除该条记录并添加一条组长记录
		if(StringUtils.isNotBlank(leaderAccountName)) {
			for(int i=0; i< oriSize; i++) {
				GroupDetailVo detailVo = detailVoList.get(i);
				// 当小组中没有组员只有组长的情况，detailVo.getAccountName()会为空，该条信息只是分组信息，只为了获取组长账号，所以如果为空，需要在List中移除该条分组信息，而后再重新添加组长对应系统账号
				if(detailVo.getAccountName() != null) {
					if(detailVo.getAccountName().equals(leaderAccountName)) {
						detailVo.setAccountRole(Constants.AccountRole.TEAM_LEADER);
						break;
					}
				} else {
					detailVoList.remove(i);
				}
				
				// 如果在最后一个仍然没有匹配到，则添加一条组长账号记录到List第一个index
				if(i == oriSize-1) {
					SystemAccountVo systemAccountVo = systemAccountService.get(leaderAccountName);
					// 封装组长账号信息并添加到分组系统账号列表
					GroupDetailVo groupDetailVo = new GroupDetailVo();
					groupDetailVo.setAccountId(systemAccountVo.getAccountId());
					groupDetailVo.setAccountName(systemAccountVo.getAccountName());
					groupDetailVo.setAccountRole(systemAccountVo.getAccountRole());
					groupDetailVo.setCompanyId(systemAccountVo.getCompanyId());
					groupDetailVo.setAccountAlias(systemAccountVo.getAccountAlias());
					groupDetailVo.setGroupId(systemAccountVo.getGroupId());
					groupDetailVo.setGroupName(systemAccountVo.getGroupName());
					groupDetailVo.setLeaderAccount(leaderAccountName);
					detailVoList.add(0, groupDetailVo);
					break;
				}
				
				// 前面remove了一个值，这里减一
				if(detailVo.getAccountName() == null) {
					oriSize -= 1;
				}
			}
		} else {
			if(oriSize == 1) {
				// 当小组中没组员和组长都没有的情况下
				if(detailVoList.get(0).getAccountName() == null) {
					detailVoList.remove(0);
				}
			}
		}
		
		return detailVoList;
	}

	/**
	 * 根据名称查询分组系统账号
	 * @param groupName
	 * @param companyId
	 * @return
	 */
	public SystemAccountGroup get(Integer groupId, String groupName, int companyId) {
		SystemAccountGroup group = new SystemAccountGroup();
		group.setCompanyId(companyId);
		group.setGroupId(groupId);
		group.setGroupName(groupName);
		return systemAccountGroupMapper.get(group);
	}

	/**
	 * 创建新分组
	 * @param groupName
	 * @param leaderAccount
	 * @param memberAccounts
	 * @param companyId
	 * @return
	 */
	public int add(String groupName, String leaderAccount,
			String memberAccounts, int companyId, String memberAccountsMd5) {
		SystemAccountGroup group = new SystemAccountGroup();
		group.setGroupName(groupName); 
		group.setLeaderAccount(leaderAccount);
		group.setMemberAccountsMd5(memberAccountsMd5);
		group.setCompanyId(companyId);
		group.setCreateTime(new Date());
		group.setUpdateTime(new Date());
		systemAccountGroupMapper.add(group);
		
		if(StringUtils.isNotBlank(leaderAccount)) {
			systemAccountService.modifyAccountRole(null, leaderAccount, Constants.AccountRole.TEAM_LEADER);
		}
		if(StringUtils.isNotBlank(memberAccounts)) {
			String[] accounts = memberAccounts.split(",");
			for(String accountName : accounts){
				systemAccountService.modifyGroupId(null, accountName, group.getGroupId());
			}
		}
		if(StringUtils.isNotBlank(leaderAccount)) {
			// 重新加载组长所对应所有组员系统账号信息
			ApplicationContextContainer.reloadLeaderMemberAccountsMap(companyId);
		}
		return group.getGroupId();
	}

	/**
	 * 修改分组
	 * @param groupId
	 * @param groupName
	 * @param leaderAccount
	 * @param memberAccounts
	 * @param companyId
	 * @param memberAccountsMd5  组员账号对应的MD5
	 * @param isMemberAccountsModify 组员账号是否有修改，false-没有，true-有修改
	 * @return
	 */
	public int modify(int groupId, String groupName, String leaderAccount, 
			String memberAccounts, int companyId, String memberAccountsMd5, boolean isMemberAccountsModify) {
		SystemAccountGroup group = new SystemAccountGroup();
		group.setCompanyId(companyId);
		group.setGroupId(groupId);
		group.setGroupName(groupName);
		group.setLeaderAccount(leaderAccount);
		group.setMemberAccountsMd5(memberAccountsMd5);
		group.setUpdateTime(new Date());
		
		// 组员账号更新：判断组员账号是否有修改
		if(isMemberAccountsModify) {
			// 先将原系统账号分组ID重置为未分组
			systemAccountService.resetGroupId(groupId);

			if(StringUtils.isNotBlank(memberAccounts)) {
				String[] accounts = memberAccounts.split(",");
				for(String accountName : accounts){
					// 更新未分组ID为已分组ID
					systemAccountService.modifyGroupId(null, accountName, groupId);
				}
			}
		}
		
		String oldLeaderAccount = null;
		// 组长账号更新：判断组长账号是否有修改
		// 检查该组长账号是否同时属于多个分组组长
		SystemAccountGroup paramGroup = new SystemAccountGroup();
		paramGroup.setCompanyId(companyId);
		paramGroup.setGroupId(groupId);
		List<SystemAccountGroup> groupList = systemAccountGroupMapper.querySameLeaderAccountByGroupId(paramGroup);
		if(groupList != null && groupList.size() == 1) {
			SystemAccountGroup accountGroup = groupList.get(0);
			oldLeaderAccount = accountGroup.getLeaderAccount();
			if(StringUtils.isNotBlank(oldLeaderAccount) && !leaderAccount.equals(oldLeaderAccount)) {
				// 将旧组长账号角色更新为普通系统账号
				systemAccountService.modifyAccountRole(null, oldLeaderAccount, Constants.AccountRole.NORMAL);
			}
		}
		
		// 将新组长账号角色更新为组长
		systemAccountService.modifyAccountRole(null, leaderAccount, Constants.AccountRole.TEAM_LEADER);
		
		// 修改分组
		int result = systemAccountGroupMapper.update(group);
		
		// 重新加载组长所对应所有组员系统账号信息
		ApplicationContextContainer.reloadLeaderMemberAccountsMap(companyId);
		
		return result;
	}

	/**
	 * 解散分组，将分组信息删除，同时把所有该分组中的组员改为未分组
	 * @param groupId
	 * @param companyId
	 * @return
	 */
	public int delete(int groupId, int companyId) {
		// 先将原系统账号分组ID重置为未分组
		systemAccountService.resetGroupId(groupId);
		
		// 检查该组长账号是否同时属于多个分组组长
		SystemAccountGroup paramGroup = new SystemAccountGroup();
		paramGroup.setCompanyId(companyId);
		paramGroup.setGroupId(groupId);
		List<SystemAccountGroup> groupList = systemAccountGroupMapper.querySameLeaderAccountByGroupId(paramGroup);
		if(groupList != null && groupList.size() == 1) {
			SystemAccountGroup group = groupList.get(0);
			String leaderAccount = group.getLeaderAccount();
			if(StringUtils.isNotBlank(leaderAccount)) {
				// 将组长角色更新为普通系统账号
				systemAccountService.modifyAccountRole(null, leaderAccount, Constants.AccountRole.NORMAL);
			}
		}
		
		// 删除分组ID对应分组信息
		int result = systemAccountGroupMapper.delete(groupId);
		
		// 重新加载组长所对应所有组员系统账号信息
		ApplicationContextContainer.reloadLeaderMemberAccountsMap(companyId);
		
		return result;
	}

	/**
	 * 删除分组某个组员，将组员变为未分组，如果是删除组长，并且该组长只担任一个组的组长，则组长角色更新为普通系统账号
	 * @param groupId
	 * @param accountName
	 * @param accountRole
	 * @param companyId
	 * @return
	 */
	public int deleteGroupMember(int groupId, String accountName, int accountRole, int companyId) {
		
		// 普通系统账号，更新该账号分组ID为未分组ID：-1
		if(accountRole == Constants.AccountRole.NORMAL) {
			systemAccountService.modifyGroupId(null, accountName, Constants.GroupId.NONE);

			// 更新分组表组长账号为空
			SystemAccountGroup group = new SystemAccountGroup();
			group.setCompanyId(companyId);
			group.setGroupId(groupId);
			group.setMemberAccountsMd5("0");// 有组员账号发生变更，则更新组员账号MD5为0
			group.setUpdateTime(new Date());
			systemAccountGroupMapper.update(group);
		} 
		else { // 如果是组长账号
			// 检查该组长账号是否同时属于多个分组组长
			List<SystemAccountGroup> groupList = querySameLeaderAccountByLeaderAccount(companyId, accountName);
			if(groupList == null || groupList.isEmpty()) {
				return 0;
			}
			if(groupList.size() == 1) { //如果只担任一个组组长
				// 将组长角色更新为普通系统账号
				systemAccountService.modifyAccountRole(null, accountName, Constants.AccountRole.NORMAL);
			}
			
			SystemAccountGroup group = new SystemAccountGroup();
			// 判断是否该组长账号同时也属于组员账号，如果是则更新系统账号对应分组ID为未分组
			SystemAccountVo systemAccountVo = systemAccountService.get(accountName);
			if(systemAccountVo != null) {
				// 如果是同一个分组，则更新分组ID为空
				if(systemAccountVo.getGroupId() == groupId) {
					systemAccountService.modifyGroupId(null, accountName, Constants.GroupId.NONE);
					group.setMemberAccountsMd5("0");// 有组员账号发生变更，则更新组员账号MD5为0
				}
			}
			
			// 更新分组表组长账号为空
			group.setCompanyId(companyId);
			group.setGroupId(groupId);
			group.setLeaderAccount("");
			group.setUpdateTime(new Date());
			systemAccountGroupMapper.update(group);
		}
		
		// 重新加载组长所对应所有组员系统账号信息
		ApplicationContextContainer.reloadLeaderMemberAccountsMap(companyId);
		
		return 1;
	}

	/**
	 * 根据组长账号查询相同组长账号的记录
	 * @param paramGroup
	 * @return
	 */
	public List<SystemAccountGroup> querySameLeaderAccountByLeaderAccount(int companyId, String accountName) {
		SystemAccountGroup paramGroup = new SystemAccountGroup();
		paramGroup.setCompanyId(companyId);
		paramGroup.setLeaderAccount(accountName);
		List<SystemAccountGroup> groupList = systemAccountGroupMapper.querySameLeaderAccountByLeaderAccount(paramGroup);
		return groupList;
	}
	
	/**
	 * 重置组长账号为空
	 * @param companyId
	 * @param leaderAccount
	 * @return
	 */
	public int resetLeaderAccount(int companyId, String leaderAccount) {
		SystemAccountGroup paramGroup = new SystemAccountGroup();
		paramGroup.setCompanyId(companyId);
		paramGroup.setLeaderAccount(leaderAccount);
		paramGroup.setUpdateTime(new Date());
		return systemAccountGroupMapper.updateLeaderAccount(paramGroup);
	}
}
