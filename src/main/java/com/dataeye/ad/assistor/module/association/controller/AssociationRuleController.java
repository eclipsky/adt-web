package com.dataeye.ad.assistor.module.association.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dataeye.ad.assistor.module.association.mapper.AssociationRuleMapper;
import com.dataeye.ad.assistor.module.association.model.PkgVo;
import com.dataeye.ad.assistor.module.association.model.PlanRelation;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.association.model.AssociationRuleHistoryVo;
import com.dataeye.ad.assistor.module.association.model.AssociationRuleVo;
import com.dataeye.ad.assistor.module.association.service.AssociationRuleService;
import com.dataeye.ad.assistor.module.dictionaries.service.PageService;
import com.dataeye.ad.assistor.module.dictionaries.service.PkgService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.StringUtil;
import com.github.pagehelper.PageHelper;

/**
 * Created by luzhuyou
 * 关联规则控制器.
 */
@Controller
public class AssociationRuleController {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(AssociationRuleController.class);

    @Autowired
    private AssociationRuleService associationRuleService;
    @Autowired
    private PageService pageService;
    @Autowired
    private PkgService pkgService;
    @Autowired
    private AssociationRuleMapper associationRuleMapper;

    /**
     * 查询关联规则信息列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author luzhuyou 2017/03/02
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/association/query.do")
    public Object query(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("查询关联规则信息列表");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String planName = parameter.getParameter("planName");
        String mediumIds = parameter.getParameter("mediumIds");
        String mediumAccountIds = parameter.getParameter("mediumAccountIds");
        String unboundPlan = parameter.getParameter("unboundPlan");
        String productIds = parameter.getParameter("productIds");
        if (StringUtils.isBlank(planName)) {
            planName = null;
        } else {
            planName = planName.trim();
        }
        if (StringUtils.isBlank(mediumIds)) {
            mediumIds = null;
        }
        if (StringUtils.isBlank(mediumAccountIds)) {
            mediumAccountIds = null;
        }
        if (StringUtils.isBlank(mediumAccountIds)) {
            mediumAccountIds = null;
        }
        if (StringUtils.isBlank(unboundPlan) || !StringUtils.isNumeric(unboundPlan) || !"1".equals(unboundPlan.trim())) {
            unboundPlan = "0";//0-所有计划的关联规则信息，1-未绑定关联规则的计划
        }
        if (StringUtils.isBlank(productIds)) {
            productIds = null;
        }
        UserInSession userInSession = (UserInSession) SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();

        // 如果无负责人权限，则直接返回空的分页信息
        if (!userInSession.hasManagerPermission()) {
            return PageDataHelper.getEmptyPageData(parameter.getPageId(), parameter.getPageSize(), new ArrayList<AssociationRuleVo>());
        }

        PageHelper.startPage(parameter.getPageId(), parameter.getPageSize());
        List<AssociationRuleVo> result = associationRuleService.query(companyId, planName, mediumIds, mediumAccountIds, userInSession.getOptimizerPermissionMediumAccountIds(), Integer.parseInt(unboundPlan.trim()), productIds);
        for (AssociationRuleVo vo : result) {
//        	if(vo.getProductId() == null || vo.getProductId() == 0 || vo.getProductId() == 1 || vo.getProductId() == 7) {// 如果为产品“塔王之王”或“ADT-Demo产品”，则需要采用配置的包，否则：所传入的落地页ID即为活动ID（短链的后六位活动号），这时候，返回的包为无（包ID为0）
//        		continue;
//        	}
//        	if(null != vo.getPkgId() && vo.getPkgId() != 0) {
//        		vo.setPkgId(0);
//        		vo.setPkgName("无");
//        	}
            if (vo.getProductId() == null || vo.getProductId() == 0) {
                continue;
            }
            if (null != vo.getPkgId() && vo.getPkgId() == 0) {
                vo.setPkgId(0);
                vo.setPkgName("无");
            }
            if (null != vo.getPageId() && vo.getPageId() == 0) {
                vo.setPageId(0);
                vo.setPageName("无");
            }
        }
        PageData pageData = PageDataHelper.getPageData((com.github.pagehelper.Page) result);
        return pageData;
    }

    /**
     * 修改关联规则
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author luzhuyou 2017/03/03
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/association/modify.do")
    public Object modify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("修改关联规则");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String updateStatus = parameter.getParameter("updateStatus"); // 0-初始提交更新，1-强制提交更新
        String productId = parameter.getParameter("productId");
        String planId = parameter.getParameter("planId");
        String pageId = parameter.getParameter("pageId");
        String pkgId = parameter.getParameter("pkgId");
        String optimizerSystemAccount = parameter.getParameter("optimizerSystemAccount");
        if (StringUtils.isBlank(updateStatus) || !StringUtils.isNumeric(updateStatus) || !updateStatus.equals("1")) {
            updateStatus = "0";
        }
        if (StringUtils.isBlank(productId) || !StringUtils.isNumeric(productId)) {
            logger.error("产品不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ASSO_PRODUCT_IS_NULL);
        }
        if (StringUtils.isBlank(planId) || !StringUtils.isNumeric(planId)) {
            logger.error("计划不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ASSO_PLAN_IS_NULL);
        }
        if (StringUtils.isBlank(pageId) || !StringUtils.isNumeric(pageId)) {
            logger.error("落地页不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ASSO_PAGE_IS_NULL);
        }
        if (StringUtils.isBlank(pkgId) || !StringUtils.isNumeric(pkgId)) {
            logger.error("包不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ASSO_PKG_IS_NULL);
        }
        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
        int osType = 0;
        int product = Integer.parseInt(productId.trim());
        // modify by luzhuyou 20170801 关联规则页面开放支付查看和修改所有产品的包
//        if(product != 1 && product != 7) { // 如果为产品“塔王之王”或“ADT-Demo产品”，则需要采用配置的包，否则：所传入的落地页ID即为活动ID（短链的后六位活动号），这时候，传入的包为无（包ID为0），而实际上需要将落地页ID和包ID都设置为相同短链活动号对应的包和落地页的ID
//        	if(Integer.parseInt(pageId) != 0) {
//        		Page page = pageService.get(Integer.parseInt(pageId));
//        		osType = page.getOsType();
//        		String pageName = page.getPageName();
//        		pageName = pageName.contains("(") ? pageName.substring(0, pageName.indexOf("(")) : pageName;
//        		List<Pkg> pkgList = pkgService.queryByTrackingInfo(companyId, page.getH5AppId(), page.getH5PageId(), pageName);
//        		if(pkgList != null && !pkgList.isEmpty()) {
//        			pkgId = pkgList.get(0).getPkgId().toString();
//        		}
//        	}
//        }
        // 修改计划关联关系
        return associationRuleService.modify(companyId, product, Integer.parseInt(planId), Integer.parseInt(pageId), Integer.parseInt(pkgId), osType, optimizerSystemAccount, Integer.parseInt(updateStatus.trim()));
    }

    /**
     * 查询历史关联规则信息列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author luzhuyou 2017/08/02
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/association/query-his.do")
    public Object queryHistory(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String planId = parameter.getParameter("planId");
        String unboundPlan = parameter.getParameter("unboundPlan");
        if (StringUtils.isBlank(planId) || !StringUtils.isNumeric(planId)) {
            planId = "-1"; // 错误的计划ID，查不到数据
        }
        if (StringUtils.isBlank(unboundPlan) || !StringUtils.isNumeric(unboundPlan) || !"1".equals(unboundPlan.trim())) {
            unboundPlan = "0";//0-所有计划的关联规则信息，1-未绑定关联规则的计划
        }
        List<AssociationRuleVo> result = associationRuleService.queryHistory(Integer.parseInt(planId), Integer.parseInt(unboundPlan.trim()));
        for (AssociationRuleVo vo : result) {
            if (vo.getProductId() == null || vo.getProductId() == 0) {
                continue;
            }
            if (null != vo.getPkgId() && vo.getPkgId() == 0) {
                vo.setPkgId(0);
                vo.setPkgName("无");
            }
            if (null != vo.getPageId() && vo.getPageId() == 0) {
                vo.setPageId(0);
                vo.setPageName("无");
            }
        }
        return result;
    }

    /**
     * 修改历史关联规则
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author luzhuyou 2017/08/02
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/association/modify-his.do")
    public Object modifyHistory(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("修改历史关联规则");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String updateStatus = parameter.getParameter("updateStatus"); // 0-初始提交更新，1-强制提交更新
        String statDates = parameter.getParameter("statDates");
        String planId = parameter.getParameter("planId");
        String pageId = parameter.getParameter("pageId");
        String pkgId = parameter.getParameter("pkgId");
        if (StringUtils.isBlank(updateStatus) || !StringUtils.isNumeric(updateStatus) || !updateStatus.equals("1")) {
            updateStatus = "0";
        }
        if (StringUtils.isBlank(statDates)) {
            logger.error("统计日期不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ASSO_STAT_DATE_IS_NULL);
        }
        if (StringUtils.isBlank(planId) || !StringUtils.isNumeric(planId)) {
            logger.error("计划不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ASSO_PLAN_IS_NULL);
        }
        if (StringUtils.isBlank(pageId) || !StringUtils.isNumeric(pageId)) {
            logger.error("落地页不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ASSO_PAGE_IS_NULL);
        }
        if (StringUtils.isBlank(pkgId) || !StringUtils.isNumeric(pkgId)) {
            logger.error("包不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ASSO_PKG_IS_NULL);
        }

        int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
        AssociationRuleHistoryVo hisRule = new AssociationRuleHistoryVo();
        hisRule.setCompanyId(companyId);
        hisRule.setPlanId(Integer.parseInt(planId));
        hisRule.setPageId(Integer.parseInt(pageId));
        hisRule.setPkgId(Integer.parseInt(pkgId));
        hisRule.setStatDates(StringUtil.stringToListString(statDates));
        // 修改计划历史关联关系
        associationRuleService.modifyHistory(hisRule, Integer.parseInt(updateStatus.trim()));
        return null;
    }

    /**
     * 批量更新关联规则
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/association/batchModify.do")
    public Object batchUpdateRelation(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("根据短链号批量更新关联规则");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String listStr = parameter.getParameter("list");
        List<PlanRelation> relationList = new ArrayList<>();
        try {
            relationList = StringUtil.gson.fromJson(listStr, new TypeToken<ArrayList<PlanRelation>>() {
            }.getType());
        } catch (Exception e) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_PARAMETER_ERROR);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        String systemAccount = userInSession.getAccountName();
        int osType = 0;
        if (null != relationList) {
            for (PlanRelation relation : relationList) {
                // 通过计划名获取短链号；通过短链号匹配已经关联的pageId，pkgId和productId
                String planName = relation.getPlanName();
                String[] str = planName.split("-|_");
                int length = str.length;
                if (length > 1) {
                    String campaignId = str[length - 1];
                    Integer pageId = associationRuleMapper.getPageIdByCampaignId(campaignId);
                    PkgVo pkgVo = associationRuleMapper.getPkgIdByCampaignId(campaignId);
                    if (null != pageId && null != pkgVo && null != pkgVo.getPkgId() && null != pkgVo.getProductId()) {
                        Integer pkgId = pkgVo.getPkgId();
                        Integer productId = pkgVo.getProductId();
                        associationRuleService.modify(companyId, productId, relation.getPlanId(),
                                pageId, pkgId, osType, systemAccount, 1);
                    }
                }
            }
        }
        return "success";

    }


}
