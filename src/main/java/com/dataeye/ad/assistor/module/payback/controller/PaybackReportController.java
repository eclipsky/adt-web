package com.dataeye.ad.assistor.module.payback.controller;

import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.payback.model.DailyPaybackReportView;
import com.dataeye.ad.assistor.module.payback.model.MonthlyPaybackReportView;
import com.dataeye.ad.assistor.module.payback.model.PaybackQuery;
import com.dataeye.ad.assistor.module.payback.model.PaybackTrendQuery;
import com.dataeye.ad.assistor.module.payback.service.PaybackReportService;
import com.dataeye.ad.assistor.module.payback.util.ExcelUtils;
import com.dataeye.ad.assistor.module.report.constant.Defaults;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.StringUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * 回本日报控制器
 * Created by huangzehai on 2017/1/14.
 */

@Controller
public class PaybackReportController {

	private static final String CONTENT_TYPE = "application/msexcel;charset=UTF-8";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String CONTENT_DISPOSITION_VALUE = "attachment;filename=";
    
    /**
     * 回本日报服务.
     */
    @Autowired
    private PaybackReportService paybackReportService;

    /**
     * 回本日报.
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/payback/daily.do")
    public Object dailyPaybackReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());

        PaybackQuery query = parseQuery(context);
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        query.setPermissionSystemAccounts(userInSession.getPermissionSystemAccounts());
        return paybackReportService.dailyPaybackReport(query);
    }

    /**
     * 解析按日统计参数
     *
     * @param context
     * @return
     * @throws ParseException
     */
    private PaybackQuery parseQuery(DEContext context) throws ParseException {
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }

        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        //构建回本查询条件
        PaybackQuery query = new PaybackQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);

        //解析产品ID列表
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        String filterNaturalFlow = parameter.getParameter(DEParameter.Keys.FILTER_NATURAL_FLOW);
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setPlanIds(StringUtil.stringToList(parameter.getPlanIds()));
        query.setAccountIds(StringUtil.stringToList(parameter.getAccountIds()));
        query.setMediumIds(StringUtil.stringToList(parameter.getMediumIds()));
        query.setSize(parameter.getSize());
        query.setFilterNaturalFlow(Boolean.valueOf(filterNaturalFlow));
        if (parameter.getPageId() > 0) {
            query.setCurrentPage(parameter.getPageId());
        } else {
            query.setCurrentPage(Defaults.CURRENT_PAGE);
        }

        if (parameter.getPageSize() > 0) {
            query.setPageSize(parameter.getPageSize());
        } else {
            query.setPageSize(Defaults.PAGE_SIZE);
        }
        return query;
    }

    /**
     * 解析按月统计参数
     *
     * @param context
     * @return
     * @throws ParseException
     */
    private PaybackQuery parseMonthlyQuery(DEContext context) throws ParseException {
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }

        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());

        //不统计当前月份及未来月份
        Date firstDateOfThisMonth = DateUtils.firstDayOfThisMonth();
        if (!endDate.before(firstDateOfThisMonth)) {
            endDate = DateUtils.lastDayOfPreviousMonth();
        }

        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        String filterNaturalFlow = parameter.getParameter(DEParameter.Keys.FILTER_NATURAL_FLOW);
        //构建回本查询条件
        PaybackQuery query = new PaybackQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setPlanIds(StringUtil.stringToList(parameter.getPlanIds()));
        query.setAccountIds(StringUtil.stringToList(parameter.getAccountIds()));
        query.setMediumIds(StringUtil.stringToList(parameter.getMediumIds()));
        query.setSize(parameter.getSize());
        query.setFilterNaturalFlow(Boolean.valueOf(filterNaturalFlow));
        return query;
    }

    /**
     * 下载回本日报.
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/payback/daily/download.do")
    public void downloadDailyPaybackReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute("CTX");
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        PaybackQuery query = parseQuery(context);
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        query.setPermissionSystemAccounts(userInSession.getPermissionSystemAccounts());

        List<DailyPaybackReportView> reports = paybackReportService.dailyPaybackReport(query);
        if(reports != null){
            Workbook wb = ExcelUtils.buildDailyPaybackReport(reports);
            output(response, wb, "dayly_payback.xls");
        }
    }


    /**
     * 日回本率趋势
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/payback/daily/trend.do")
    public Object dailyPaybackRateTrend(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //参数解析与校验
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        // 回本日报供负责人或关注者查看

        DEParameter parameter = context.getDeParameter();
        //解析同日期
        String dateString = parameter.getDate();
        if (StringUtils.isBlank(dateString)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_DATE_BLANK);
        }

        Date date = DateUtils.parse(dateString);
        String filterNaturalFlow = parameter.getParameter(DEParameter.Keys.FILTER_NATURAL_FLOW);

        PaybackTrendQuery query = new PaybackTrendQuery();
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        query.setPermissionSystemAccounts(userInSession.getPermissionSystemAccounts());
        query.setDate(date);

        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setPlanIds(StringUtil.stringToList(parameter.getPlanIds()));
        query.setAccountIds(StringUtil.stringToList(parameter.getAccountIds()));
        query.setMediumIds(StringUtil.stringToList(parameter.getMediumIds()));
        query.setFilterNaturalFlow(Boolean.valueOf(filterNaturalFlow));

        return this.paybackReportService.dailyPaybackRateTrend(query);
    }

    /**
     * 回本日报.
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/payback/monthly.do")
    public Object monthlyPaybackReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());

        PaybackQuery query = parseMonthlyQuery(context);
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        query.setPermissionSystemAccounts(userInSession.getPermissionSystemAccounts());
        return paybackReportService.monthlyPaybackReport(query);
    }

    /**
     * 下载回本月报.
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/payback/monthly/download.do")
    public void downloadMonthlyPaybackReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        PaybackQuery query = parseMonthlyQuery(context);
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        query.setPermissionSystemAccounts(userInSession.getPermissionSystemAccounts());
        List<MonthlyPaybackReportView> reports = paybackReportService.monthlyPaybackReport(query);
        if(reports != null){
            Workbook wb = ExcelUtils.buildMonthlyPaybackReport(reports);
            output(response, wb, "monthly_payback.xls");
        }
    }
    
    /**
     * 向输出流写入文件
     *
     * @param response
     * @param wb
     * @param fileName
     * @throws IOException
     */
    private void output(HttpServletResponse response, Workbook wb, String fileName) throws IOException {
        // Write the output to a file
        response.setContentType(CONTENT_TYPE);
        response.addHeader(CONTENT_DISPOSITION, CONTENT_DISPOSITION_VALUE+fileName);
        ServletOutputStream out = response.getOutputStream();
        wb.write(out);
        out.flush();
        out.close();
    }
}
