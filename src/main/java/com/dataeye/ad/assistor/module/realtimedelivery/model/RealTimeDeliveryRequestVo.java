package com.dataeye.ad.assistor.module.realtimedelivery.model;

import java.util.Arrays;
import java.util.List;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;


/**
 * 实时投放对象请求参数VO
 *
 * @author luzhuyou 2017/03/03
 */
public class RealTimeDeliveryRequestVo extends DataPermissionDomain {

    /**
     * 统计日期
     */
    private String statDate;
    /**
     * 媒体账户ID
     */
    private Integer[] mediumAccountIds;
    /**
     * 媒体ID数组
     */
    private Integer[] mediumIds;
    private List<Integer> productIds;
    /**
     * 过滤媒体数据为空的计划
     */
    private boolean ignoreIsNullCostPlan = false;
    
    private List<Integer> planIds;

    public String getStatDate() {
        return statDate;
    }

    public void setStatDate(String statDate) {
        this.statDate = statDate;
    }

    public Integer[] getMediumAccountIds() {
        return mediumAccountIds;
    }

    public void setMediumAccountIds(Integer[] mediumAccountIds) {
        this.mediumAccountIds = mediumAccountIds;
    }

    public Integer[] getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(Integer[] mediumIds) {
        this.mediumIds = mediumIds;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }
    
    public boolean isIgnoreIsNullCostPlan() {
		return ignoreIsNullCostPlan;
	}

	public void setIgnoreIsNullCostPlan(boolean ignoreIsNullCostPlan) {
		this.ignoreIsNullCostPlan = ignoreIsNullCostPlan;
	}

	public List<Integer> getPlanIds() {
		return planIds;
	}

	public void setPlanIds(List<Integer> planIds) {
		this.planIds = planIds;
	}

	@Override
	public String toString() {
		return "RealTimeDeliveryRequestVo [statDate=" + statDate
				+ ", mediumAccountIds=" + Arrays.toString(mediumAccountIds)
				+ ", mediumIds=" + Arrays.toString(mediumIds) + ", productIds="
				+ productIds + ", ignoreIsNullCostPlan=" + ignoreIsNullCostPlan
				+ ", planIds=" + planIds + "]";
	}
}
