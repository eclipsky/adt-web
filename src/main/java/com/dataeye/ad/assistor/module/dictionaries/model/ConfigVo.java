package com.dataeye.ad.assistor.module.dictionaries.model;

import java.util.Date;



/**
 * 配置表
 * @author ldj 2018/03/29
 *
 */
public class ConfigVo {

	
	private String vkey;
	private String value;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	

	public String getVkey() {
		return vkey;
	}


	public void setVkey(String vkey) {
		this.vkey = vkey;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	public Date getUpdateTime() {
		return updateTime;
	}


	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}


	@Override
	public String toString() {
		return "ConfigVo [vkey=" + vkey + ", value=" + value + ", remark="
				+ remark + ", createTime=" + createTime + ", updateTime="
				+ updateTime + "]";
	}
}
