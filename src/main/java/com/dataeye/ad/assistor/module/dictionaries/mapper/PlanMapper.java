package com.dataeye.ad.assistor.module.dictionaries.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.campaign.model.CampaignCreateReqVo;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import com.dataeye.ad.assistor.module.dictionaries.model.PlanVo;

/**
 * 投放计划映射器.
 * Created by luzhuyou 2017/03/02
 */
@MapperScan
public interface PlanMapper {

    /**
     * 查询投放计划列表
     *
     * @param planVo
     * @return
     */
    public List<Plan> query(PlanVo planVo);

    /**
     * 更新投放计划系统类型
     *
     * @param plan
     * @return
     */
    public int updateOsType(Plan plan);

    /**
     * 更新计划开关
     *
     * @param plan
     * @return
     */
    int updatePlanSwitch(Plan plan);

    /**
     * 修改计划出价
     *
     * @param plan
     * @return
     */
    int updateBid(Plan plan);

    /**
     * 根据计划ID获取计划
     *
     * @param planId
     * @return
     */
    Plan getPlanById(@Param("planId") int planId);
    
	/**
	 * 创建投放计划
	 * @param planList
	 * 
	 */
	public void batchInsert(List<Plan> planList);
	
	/**
     * 根据媒体平台计划ID获取计划
     * @param mPlanId
     * @return
     */
    Plan getPlanBymPlanId(Plan plan);

    /**
     * 更新计划名称
     *
     * @param plan
     * @return
     */
    int updatePlanName(Plan plan);
}
