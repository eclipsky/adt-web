package com.dataeye.ad.assistor.module.systemaccount.model;

import java.util.Date;


/**
 * 系统账号实体类
 * 
 * @author luzhuyou 2017-02-16
 */
public class SystemAccount {
	/** 账号ID */
	private Integer accountId;
	/** 公司ID */
	private Integer companyId;
	/** 账号名称（邮箱，登录账号） */
	private String accountName;
	/** 账号别名 */
	private String accountAlias;
	/** 密码 */
	private String password;
	/** 状态：0-正常，1-失效，2-删除 */
	private Integer status;
	/** 账户角色:0-普通系统账户，1-组长，2-企业账户 */
	private Integer accountRole;
	/** 菜单权限：多个用逗号分隔 */
	private String menuPermission;
	/**
	 *  指标权限
	 */
	private String indicatorPermission;
	/** 手机 */
	private String mobile;
	/** 分组ID */
	private Integer groupId;
	/** 登录提示，需要提示次数 */
	private Integer loginPromptTimes;
	/** 登录提示，当前已提示次数 */
	private Integer alreadyPromptTimes;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountAlias() {
		return accountAlias;
	}
	public void setAccountAlias(String accountAlias) {
		this.accountAlias = accountAlias;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getAccountRole() {
		return accountRole;
	}
	public void setAccountRole(Integer accountRole) {
		this.accountRole = accountRole;
	}
	public String getMenuPermission() {
		return menuPermission;
	}
	public void setMenuPermission(String menuPermission) {
		this.menuPermission = menuPermission;
	}

	public String getIndicatorPermission() {
		return indicatorPermission;
	}

	public void setIndicatorPermission(String indicatorPermission) {
		this.indicatorPermission = indicatorPermission;
	}

	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public Integer getLoginPromptTimes() {
		return loginPromptTimes;
	}
	public void setLoginPromptTimes(Integer loginPromptTimes) {
		this.loginPromptTimes = loginPromptTimes;
	}
	public Integer getAlreadyPromptTimes() {
		return alreadyPromptTimes;
	}
	public void setAlreadyPromptTimes(Integer alreadyPromptTimes) {
		this.alreadyPromptTimes = alreadyPromptTimes;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "SystemAccount [accountId=" + accountId + ", companyId="
				+ companyId + ", accountName=" + accountName
				+ ", accountAlias=" + accountAlias + ", password=" + password
				+ ", status=" + status + ", accountRole=" + accountRole
				+ ", menuPermission=" + menuPermission
				+ ", indicatorPermission=" + indicatorPermission + ", mobile="
				+ mobile + ", groupId=" + groupId + ", loginPromptTimes="
				+ loginPromptTimes + ", alreadyPromptTimes="
				+ alreadyPromptTimes + ", remark=" + remark + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}

}
