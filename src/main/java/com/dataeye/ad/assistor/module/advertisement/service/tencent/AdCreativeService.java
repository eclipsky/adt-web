package com.dataeye.ad.assistor.module.advertisement.service.tencent;



import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ProductType;
import com.dataeye.ad.assistor.module.advertisement.constants.InterfaceConstants.TencentInterface;
import com.dataeye.ad.assistor.module.advertisement.model.AdcreativeVO;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.AdImages;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.Adcreative;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.HttpRequest;
import com.dataeye.ad.assistor.util.HttpUtil;
import com.dataeye.ad.assistor.util.RandomUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;


/**
 * 广点通API
 * 广告创意Service
 * */
@Service("adCreativeService")
public class AdCreativeService {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AdCreativeService.class);
	
	@Autowired
	private ImageService imageService;

	/**
	 * 获取广告创意
	 * @param account_id(必填)
	 * @param adcreative_id
	 * @return 
	 */
	public List<AdcreativeVO> query(Integer account_id,Integer adcreative_id,String accessToken) {
		//参数校验
		validateParamsByAccount(account_id, accessToken);
		// 1.构建请求参数
		String Tencent_params= StringUtils.substring(TencentInterface.TENCENT_PARAMETERS, 1);
		String params1 = HttpUtil.urlParamReplace(Tencent_params,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));

		String params=params1+"&account_id="+account_id;
		if(adcreative_id != null){
			params+="&adcreative_id="+adcreative_id;
		}
		
		String getAdcreativeInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADCREATIVE_GET;
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.get(getAdcreativeInterface,params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>查询广告创意API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{getAdcreativeInterface, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>查询广告创意API请求数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{getAdcreativeInterface, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
    	JsonObject data=jsonObject.get("data").getAsJsonObject();
    	JsonArray dataJsonObject = data.get("list").getAsJsonArray();
    	List<Adcreative>  response = StringUtil.gson.fromJson(dataJsonObject, new TypeToken<List<Adcreative>>(){}.getType());
    	List<AdcreativeVO> result = new ArrayList<>();
    	//处理创意元素，如果创意元素包含
    	for (Adcreative acVo : response) {
    		AdcreativeVO adCreative = new AdcreativeVO();
    		String elementObjectStr = acVo.getAdcreative_elements().toString(); 
			if(StringUtils.isNotBlank(elementObjectStr)){
				elementObjectStr = JSONObject.fromObject(acVo.getAdcreative_elements()).toString();
				//String elementObjectStr="{\"element_story\":[{\"image\":\"4232591:14e65d8b079a5db248e11340b4c090aa\"},{\"image\":\"4232591:2fdffe0ac81dece3e49d06b38fc5d1b5\"},{\"image\":\"4232591:04e0e74d6a542eb21d0bb87296b9cc13\"}],\"title\":\"ferfere\",\"corporate\":{\"corporate_name\":\"腾讯新闻\"}}";
				JsonObject morejson = StringUtil.jsonParser.parse(elementObjectStr).getAsJsonObject();
				if(morejson.get("element_story") != null && morejson.get("element_story").getAsJsonArray().size() > 0 ){
					JsonArray imgarray = morejson.get("element_story").getAsJsonArray();
				
					for (JsonElement imgElement : imgarray) {
						JsonObject imgjsonObject = imgElement.getAsJsonObject();
						if(imgjsonObject.get("image") != null && imgjsonObject.get("image").getAsString().length() > 0){
							String imgId = imgjsonObject.get("image").getAsString();
							List<AdImages> adImage = imageService.get(account_id, imgId, accessToken);
							if(adImage.size() > 0){
								String image_url = adImage.get(0).getPreview_url();
								imgjsonObject.addProperty("image_url", image_url);
							}
						}
					}
					//elementObjectStr = morejson.toString();
					adCreative.setAdcreativeElements(morejson.toString());
				}else{
					adCreative.setAdcreativeElements(JSONObject.fromObject(acVo.getAdcreative_elements()).toString());
				}
				
			}
			
			adCreative.setAdcreativeId(acVo.getAdcreative_id());
			adCreative.setAdcreativeName(acVo.getAdcreative_name());
			adCreative.setAdcreativeTemplateId(acVo.getAdcreative_template_id());
			adCreative.setDestination_url(acVo.getDestination_url());
			adCreative.setSiteSet(acVo.getSite_set().get(0));
			adCreative.setPlanGroupId(acVo.getCampaign_id());
			result.add(adCreative);
		}
        return result;
	}
	
	/**
	 * 创建广告创意
	 * @param account_id(必填)
	 * @param campaign_id(必填)
	 * @param adcreative_name(必填)
	 * @param adcreative_template_id(必填)
	 * @param adcreative_elements(必填)
	 * @param destination_url(必填) 落地页 url
	 * @param site_set(必填)
	 * @param product_type(必填)
	 * @param deep_link 应用直达页 URL
	 * @param product_refs_id  标的物 id，
	 * @return adcreative_id  广告创意 id
	 */
	public int add(Integer account_id,Integer campaign_id,String adcreative_name,Integer adcreative_template_id,Object adcreative_elements,String destination_url,
			String site_set,String product_type,String deep_link,String product_refs_id,String accessToken) {
		//校验参数
		validateParamsByAdd(account_id, accessToken, campaign_id, adcreative_name, adcreative_template_id, adcreative_elements, site_set, product_type,product_refs_id);
		
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("campaign_id", campaign_id+"");
		params.put("adcreative_name", adcreative_name);
		
		params.put("adcreative_template_id", adcreative_template_id+"");
		params.put("adcreative_elements", JSONObject.fromObject(adcreative_elements).toString());
		params.put("destination_url", destination_url==null?"https://www.dataeye.com":destination_url);
		String site =  "['"+site_set+"']";
		params.put("site_set", JSONArray.fromObject(site).toString());
		params.put("product_type", product_type);
		if(StringUtils.isNotBlank(deep_link)){
			params.put("deep_link", deep_link);
		}
		if(StringUtils.isNotBlank(product_refs_id)){
			params.put("product_refs_id", product_refs_id);
		}
		
		String addAdCreativeInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADCREATIVE_ADD;
		String addAdCreativeInterfaceUrl = HttpUtil.urlParamReplace(addAdCreativeInterface,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(addAdCreativeInterfaceUrl, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>创建广告创意API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{addAdCreativeInterfaceUrl, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>创建广告创意API请求数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{addAdCreativeInterfaceUrl, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }

        JsonObject data = jsonObject.get("data").getAsJsonObject();    	
        return data.get("adcreative_id").getAsInt();
	}
	
	/**
	 * 删除广告创意
	 * @param account_id (必填)
	 * @param adcreative_id(必填)
	 * @return
	 */
	public void delete(Integer account_id,Integer adcreative_id,String accessToken) {
		validateParamsByAdcreativeId(account_id, accessToken, adcreative_id);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("adcreative_id", adcreative_id+"");
		
		String deleteUrl = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADCREATIVE_DELETE;
		String deleteAdCreativeInterface = HttpUtil.urlParamReplace(deleteUrl,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(deleteAdCreativeInterface, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>删除广告创意API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{deleteAdCreativeInterface, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>删除广告创意API数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{deleteAdCreativeInterface, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }
	}
	
	/**
	 * 更新广告创意
	 * @param account_id（必填）
	 * @param adcreative_id（必填）
	 * @param adcreative_name
	 * @param adcreative_elements
	 * @param destination_url
	 * @param deep_link
	 */
	public int update(Integer account_id,Integer adcreative_id,String adcreative_name,Object adcreative_elements,String destination_url,String deep_link,String accessToken) {
		
		validateParamsByAdcreativeId(account_id, accessToken, adcreative_id);
		// 1.构建请求参数
		Map<String, String> params = new HashMap<String, String>();
		params.put("account_id", account_id+"");
		params.put("adcreative_id", adcreative_id+"");
		if(StringUtils.isNotBlank(adcreative_name)){
			params.put("adcreative_name", adcreative_name);
			
		}
		if(adcreative_elements != null){
			params.put("adcreative_elements", JSONObject.fromObject(adcreative_elements).toString());
			
		}
		if(StringUtils.isNotBlank(destination_url)){
			params.put("destination_url", destination_url);
			
		}
		if(StringUtils.isNotBlank(deep_link)){
			params.put("deep_link", deep_link);
			
		}
		String updateAdCreativeInterface = ConfigHandler.getProperty(TencentInterface.TENCENT_URL_KEY, TencentInterface.TENCENT_URL)+TencentInterface.ADCREATIVE_UPDATE;
		String updateAdCreativeInterfaceUrl = HttpUtil.urlParamReplace(updateAdCreativeInterface,accessToken, DateUtils.getSecondTimestamp(new Date())+"",RandomUtils.getRandomString(32));
		// 2.HTTP请求获取数据
    	String httpResponse = HttpRequest.post(updateAdCreativeInterfaceUrl, params);
    	
    	if(StringUtils.isBlank(httpResponse)) {
    		logger.error("<Tencent>编辑广告创意API请求失败！请求url[{}]，具体请求参数[{}]", new Object[]{updateAdCreativeInterfaceUrl, params});
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
    	}
    	
    	// 3.结果数据封装，转换为Map
    	JsonObject jsonObject = StringUtil.jsonParser.parse(httpResponse).getAsJsonObject();
    	int code = jsonObject.get("code").getAsInt();
    	if(code != 0) {
    		logger.error("<Tencent>编辑广告创意API请求数据响应异常，请求url[{}]，具体请求参数[{}]，异常状态码[{}]，具体响应内容[{}]", new Object[]{updateAdCreativeInterfaceUrl, params,code, jsonObject});
    		if(code == 11000 || code == 11002){  //token失效
    			ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_TOKEN_EXPIRED);
    		}
    		ExceptionHandler.throwParameterException(StatusCode.COMM_API_INTERFACE_ERROR);
        }

        JsonObject data = jsonObject.get("data").getAsJsonObject();    	
        return data.get("adcreative_id").getAsInt();
	}
	
	//参数校验
		private void validateParamsByAccount(Integer account_id,String accessToken){
			if (account_id == null || account_id == 0) {
	            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCOUNT_ID_IS_NULL);
	        }
	        if (StringUtils.isBlank(accessToken)) {
	            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_MACCESSTOKEN_ID_IS_NULL);
	        }
		}
		private void validateParamsByAdd(Integer account_id,String accessToken,Integer campaign_id,String adcreative_name,Integer adcreative_template_id,
				Object adcreative_elements,String site_set,String product_type,String product_refs_id){
			validateParamsByAccount(account_id, accessToken);
			if (campaign_id == null || campaign_id == 0) {
	            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_ID_IS_NULL);
	        }
			if (StringUtils.isBlank(adcreative_name)) {
	            ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_NAME_ID_IS_NULL);
	        }
			if(adcreative_template_id == null || adcreative_template_id == 0){
		        ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_TEMPLATE_ID_IS_NULL);
		    }
	        if(StringUtils.isBlank(site_set)){
	        	ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_SITESET_IS_NULL);
	        }
	        if(adcreative_elements == null){
	        	ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_ELEMENTS_IS_NULL);
	        }
			if (StringUtils.isBlank(product_type) || StringUtils.isBlank(ProductType.valueOf(product_type).name())) {
	            ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
	        }
			if(StringUtils.isBlank(product_refs_id)){
		       ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_PRODUCT_REFS_ID_IS_NULL);
		    }
		}
		
		private void validateParamsByAdcreativeId(Integer account_id,String accessToken,Integer adcreative_id){
			validateParamsByAccount(account_id, accessToken);
			if (adcreative_id == null || adcreative_id == 0) {
	            ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_ID_IS_NULL);
	        }
		}
	

}
