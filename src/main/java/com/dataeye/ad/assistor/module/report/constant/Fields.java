package com.dataeye.ad.assistor.module.report.constant;

/**
 * 可点击的字段
 * Created by huangzehai on 2017/1/4.
 */
public final class Fields {
    public static final String DATE = "date";
    public static final String COST = "total_cost";
    public static final String EXPOSURES = "exposures";
    public static final String CLICKS = "clicks";
    public static final String TOTAL_CLICKS = "total_clicks";
    public static final String CTR = "ctr";
    public static final String CPC = "cpc";
    public static final String REACHES = "landing_page_uv";
    public static final String REACH_RATE = "reach_rate";
    public static final String RETENTION_TIME = "retention_time";
    public static final String DOWNLOADS = "download_uv";
    public static final String DOWNLOAD_RATE = "download_rate";
    public static final String ACTIVATIONS = "activations";
    public static final String ACTIVATION_RATE = "activation_rate";
    public static final String ACTIVATION_CPA = "activation_cpa";
    public static final String REGISTRATIONS = "registrations";
    public static final String REGISTRATION_RATE = "registration_rate";
    public static final String REGISTRATION_CPA = "cost_per_registration";
    public static final String ACTIVATION_REGISTRATION_RATE = "activation_registration_rate";
   /* public static final String DAY2_RETENTION_RATE = "next_day_retention_rate";
    public static final String DAY3_RETENTION_RATE = "three_day_retention_rate";
    public static final String DAY7_RETENTION_RATE = "seven_day_retention_rate";
    public static final String DAY30_RETENTION_RATE = "thirty_day_retention_rate";*/
    public static final String NEWLY_INCREASED_RECHARGES = "newly_increased_recharges";
    public static final String NEWLY_ACCOUNT_PAY_AMOUNT = "new_account_pay_amount";
    public static final String NEWLY_INCREASED_PAY_RATE = "newly_increased_pay_rate";
    public static final String FIRST_DAY_PAYBACK_RATE = "first_day_payback_rate";
    public static final String LTV_1DAY = "ltv_1day";
    public static final String LTV_7DAYS = "ltv_7days";
    public static final String LTV_15DAYS = "ltv_15days";
    public static final String LTV_30DAYS = "ltv_30days";
    public static final String REGISTERACCOUNTNUM = "register_account_num";
    public static final String FIRSTDAYREGISTRATIONS = "first_day_registrations";
    public static final String FIRSTDAYREGISTERACCOUNTNUM = "first_day_register_account_num";
    /**
     * 总充值
     */
    public static final String TOTAL_RECHARGE_AMOUNT = "total_recharges";
    /**
     * 总充值用户数
     */
    public static final String TOTAL_RECHARGES = "total_pay_num";
    public static final String ACCUMULATED_RECHARGE = "accumulated_recharge";
    public static final String PAY_RATE = "pay_rate";
    public static final String DAU = "dau";
    
}
