package com.dataeye.ad.assistor.module.systemaccount.constants;

/**
 * 系统账号管理模块常量表
 * @author luzhuyou 2017/02/17
 */
public class Constants {

//	public static final int DEFALUT_NONE_GROUP_ID = -1; 
//	public static final int DEFALUT_ALL_GROUP_ID = -2; 
	public static final String DEFALUT_ALL_GROUP_NAME = "所有子账号"; // "所有ADT账号"分组名称
	public static final String DEFALUT_NONE_GROUP_NAME = "未分组"; // "未分组"分组名称
	public static final int INITIAL_PWD_LENGTH = 12; // 初始密码长度

	public static final String EMAIL_REGEX = "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$" ;

	/**
	 * 分组ID
	 */
	public class GroupId {
		public static final int NONE = -1;// 未分组系统账号分组ID
		public static final int ALL = -2;// 所有系统账号分组ID
	}
	
	/**
	 * 分组类型
	 */
	public static class GroupType {
		/** 所有 */
		public static final int ALL = 1;
		/** 具体分组 */
		public static final int DETAIL = 2;
		/** 未分组 */
		public static final int NONE = 3;

		/** 
		 * 判断是否分组类型
		 * @param groupType 分组类型：1-所有 2-具体分组 3-未分组
		 * @return
		 */
		public static boolean isGroupType(int groupType) {
			if(groupType == ALL || groupType == DETAIL || groupType == NONE) {
				return true;
			}
			return false;
		}
	}
	
	/**
	 * 系统账号状态
	 * 
	 * @author luzhuyou 2017/02/17
	 */
	public static class UserStatus {
		/** 正常 */
		public static final int NORMAL = 0;
		/** 失效  */
		public static final int INVALID = 1;

		/** 
		 * 判断是否系统账号
		 * @param status 状态：0-正常，1-失效，2-删除
		 * @return
		 */
		public static boolean isUserStatus(int status) {
			if(status == NORMAL || status == INVALID) {
				return true;
			}
			return false;
		}
		
	}
	
	/**
	 * 系统账号角色
	 * 
	 * @author luzhuyou 2017/02/17
	 */
	public static class AccountRole {
		/** 0-普通系统账户（默认） */
		public static final int NORMAL = 0;
		/** 1-组长  */
		public static final int TEAM_LEADER = 1;
		/** 2-企业账户 */
		public static final int COMPANY = 2;
		/**
		 * 开发工程师
		 */
		public static final int ENGINEER = 3;
		/**
		 * 代理
		 */
		public static final int PROXY  = 4;
		/** 
		 * 判断是否系统账号
		 * @param accountRole
		 * @return
		 */
		public static boolean isAccountRole(int accountRole) {
			if(accountRole == NORMAL || accountRole == TEAM_LEADER || accountRole == COMPANY) {
				return true;
			}
			return false;
		}
	}
	
	/**
	 * 运营审核账号KEY:properties资源文件中的key值
	 */
	public static String OPER_AUDIT_ACCOUNT_KEY = "oper_audit_account";
	/**
	 * 默认运营审核账号
	 */
	public static String DEFAULT_OPER_AUDIT_ACCOUNT = "mengying@dataeye.com";
	/**
	 * 企业账号菜单权限 properties key
	 */
	public static final String COMPANY_MENU_PERMISSION_KEY = "company_menu_permission";
	/**
	 * 企业账号默认菜单权限
	 */
	public static final String DEFAULT_COMPANY_MENU_PERMISSION = "1,2,3-1,3-2,4-1,4-2,5,6,8-1,8-2";

	/**
	 * 企业账号指标权限 properties key
	 */
	public static final String COMPANY_INDICATOR_PERMISSION_KEY = "company_indicator_permission";

	/**
	 *  企业账号默认指标权限
	 */
	public static final String DEFAULT_COMPANY_INDICATOR_PERMISSION = "1,1-1,1-2,1-3,1-4,1-5,1-6,1-7,1-8,1-9,1-10,2,2-1,2-2,2-3,2-4,3,3-1,3-2,3-3,3-4,3-5,3-6,4,4-1,4-2,4-3,4-4,5,5-1,5-2,5-3,5-4,5-5,5-6,5-7,6,6-1";
	/**
	 * 默认系统账号密码
	 */
	public static final String DEFAULT_ENCODE_PWD = "e99a18c428cb38d5f260853678922e03";
	
	/**
	 * CRM管理账号角色
	 * 
	 * @author luzhuyou 2017/05/11
	 */
	public static class CrmAccountRole {
		/** 0-商务/销售 */
		public static final int BIZ = 0;
		/** 1-运营  */
		public static final int OPE = 1;
		/** 2-开发 */
		public static final int OTHER = 2;
		/** 
		 * 判断是否系统账号
		 * @param accountRole
		 * @return
		 */
		public static boolean isAccountRole(int accountRole) {
			if(accountRole == BIZ || accountRole == OPE || accountRole == OTHER) {
				return true;
			}
			return false;
		}
	}
	
	/**
	 * 统一登陆接口
	 * @author luzhuyou 2017/05/19
	 *
	 */
	public static class PtLoginInterface {
		/**
		 * 获取token用户信息
		 */
		public static final String GET_TOKEN_INFO_KEY = "get_token_info";
		public static final String GET_TOKEN_INFO = "http://192.168.1.207:48080/ptlogin/innerapi/getTokenInfo.do";
		
		/**
		 * 获取用户密码
		 */
		public static final String GET_PWD_KEY = "get_pwd";
		public static final String GET_PWD = "http://192.168.1.207:48080/ptlogin/innerapi/getUserPassword.do";
		
		/**
		 * 增加ADT账号
		 */
		public static final String ADD_ADT_ACCOUNT_KEY = "add_adt_account";
		public static final String ADD_ADT_ACCOUNT = "http://192.168.1.207:48080/ptlogin/innerapi/addAdtAccount.do";
	}
}
