package com.dataeye.ad.assistor.module.product.model;

import com.google.gson.annotations.Expose;

/**
 * 自然流量
 * */
public class NatureChannelVo {
	
	/** 产品名称 */
	@Expose
	private String productName;
	/**激活数*/
	@Expose
	private Integer activeNum;
	/**注册数*/
	@Expose
	private Integer registerNum;
	/**注册率*/
	@Expose
	private String registerNumRate;
	/**总充值*/
	@Expose
	private String totalPayAmount;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getActiveNum() {
		return activeNum;
	}
	public void setActiveNum(Integer activeNum) {
		this.activeNum = activeNum;
	}
	public Integer getRegisterNum() {
		return registerNum;
	}
	public void setRegisterNum(Integer registerNum) {
		this.registerNum = registerNum;
	}
	public String getRegisterNumRate() {
		return registerNumRate;
	}
	public void setRegisterNumRate(String registerNumRate) {
		this.registerNumRate = registerNumRate;
	}
	public String getTotalPayAmount() {
		return totalPayAmount;
	}
	public void setTotalPayAmount(String totalPayAmount) {
		this.totalPayAmount = totalPayAmount;
	}
	@Override
	public String toString() {
		return "NatureChannelVo [productName=" + productName + ", activeNum="
				+ activeNum + ", registerNum=" + registerNum
				+ ", registerNumRate=" + registerNumRate + ", totalPayAmount="
				+ totalPayAmount + "]";
	}
	
	
}
