package com.dataeye.ad.assistor.module.report.constant;

/**
 * Created by huangzehai on 2017/1/4.
 */
public final class Constants {
    private Constants() {

    }

    /**
     * 未知值.
     */
    public static final String UNKNOWN = "-";

    /**
     * 未知值ID.
     */
    public static final int UNKNOWN_ID = -1;

    /**
     * 日期范围
     */
    public static final String TO = "到";

    /**
     * 汇总
     */
    public static final String TOTAL = "汇总";

    /**
     * 排序方式参数名.
     */
    public static final String ORDER = "order";

}
