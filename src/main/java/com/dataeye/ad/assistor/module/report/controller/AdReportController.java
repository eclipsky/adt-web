package com.dataeye.ad.assistor.module.report.controller;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.association.service.AssociationRuleService;
import com.dataeye.ad.assistor.module.report.constant.Constants;
import com.dataeye.ad.assistor.module.report.constant.Defaults;
import com.dataeye.ad.assistor.module.report.model.*;
import com.dataeye.ad.assistor.module.report.service.AdReportService;
import com.dataeye.ad.assistor.module.report.validtor.SqlValidator;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.StringUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2016/12/26.
 * 投放日报控制器.
 */
@Controller
public class AdReportController {

    private static final String CONTENT_TYPE = "application/msexcel;charset=UTF-8";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String CONTENT_DISPOSITION_VALUE = "attachment;filename=report.xls";

    @Autowired
    private AdReportService adReportService;
    @Autowired
    private AssociationRuleService associationRuleService;

    /**
     * 投放日报-计划维度-汇总数据
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/report/adReport.do")
    public Object adReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //参数解析与校验
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        if (!userInSession.hasManagerOrFollowerPermission()) {
            return null;
        }

        //解析参数
        DEParameter parameter = context.getDeParameter();
        LinkID link = LinkID.parse((parameter.getLink()));
        //参数处理
        View view = View.parse(parameter.getView());
        Dimension dimension = Dimension.parse(parameter.getDimension());

        if (view == null) {
            view = View.Total;
        }

        if (dimension == null) {
            dimension = Dimension.Plan;
        }
        DeliveryReportQuery query = parseQuery(parameter);
        query.setCompanyId(userInSession.getCompanyId());
        query.setPermissionMediumAccountIds(userInSession.getPermissionMediumAccountIds());
        query.setPermissionSystemAccounts(userInSession.getPermissionSystemAccounts());
        query.setAccountId(userInSession.getAccountId());

        if (link == null) {
            //外层表格
            return adReportService.adReport(view, dimension, query);
        } else {
            //内嵌表格
            return adReportService.adReportDetail(view, dimension, link, query);
        }
    }

    /**
     * 解析请求参数，转化为查询对象.
     *
     * @param parameter
     * @return
     * @throws ParseException
     */
    private DeliveryReportQuery parseQuery(DEParameter parameter) throws ParseException {
        String startDateString;
        String endDateString;
        if (StringUtils.isBlank(parameter.getDate()) || StringUtils.contains(parameter.getDate(), Constants.TO)) {
            if (StringUtils.isBlank(parameter.getStartDate())) {
                ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
            }

            if (StringUtils.isBlank(parameter.getEndDate())) {
                ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
            }

            //处理日期范围参数
            parameter.checkDateRangeParamsValid(parameter.getStartDate(), parameter.getEndDate());
            startDateString = parameter.getStartDate();
            endDateString = parameter.getEndDate();
        } else {
            startDateString = parameter.getDate();
            endDateString = parameter.getDate();
        }
        Date startDate = DateUtils.parse(startDateString);
        Date endDate = DateUtils.parse(endDateString);

        String orderBy = parameter.getOrderBy();
        String order = parameter.getParameter(Constants.ORDER);
        //检验排序方式和排序字段
        if (!SqlValidator.isValidOrderBy(orderBy)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_ORDER_BY_INVALID);
        }
        if (!SqlValidator.isValidOrder(order)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_ORDER_INVALID);
        }
        
        //构建查询对象
        DeliveryReportQuery query = new DeliveryReportQuery();
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        query.setOrderBy(orderBy);
        query.setOrder(order);//不需要参数默认值.

        query.setIgnoreUnboundPlan(parameter.isIgnoreUnboundPlan());
        query.setIgnoreZeroCostPlan(parameter.isIgnoreZeroCostPlan());
        query.setIgnoreIsNullCostPlan(parameter.isIgnoreIsNullCostPlan());

        //解析媒体ID.
        //点击账号链接时，前端上传单个媒体ID，其参数名为mediumId；其他情况为用户选择媒体下拉列表，可以选择多个媒体，上传的参数名为mediumId.
        String mediumIdText;
        if (parameter.getMediumId() > 0) {
            mediumIdText = String.valueOf(parameter.getMediumId());
        } else {
            mediumIdText = parameter.getMediumIds();
        }

        if (StringUtils.isNotBlank(mediumIdText) && !StringUtils.equalsIgnoreCase(parameter.getMediumIds(), String.valueOf(Constants.UNKNOWN_ID))) {
            //将媒体ID转化为列表.
            List<Integer> mediumIds = new ArrayList<>();
            for (String mediumId : mediumIdText.split(Constant.Separator.COMMA)) {
                mediumIds.add(Integer.valueOf(mediumId));
            }
            query.setMediumIds(mediumIds);
        }

        //点击账号链接时，前端上传单个账号ID，其参数名为accountId；其他情况为用户选择账号下拉列表，可以选择多个账号，上传的参数名为accountIds.
        String accountIdsText;
        if (StringUtils.isNotBlank(parameter.getAccountId()) && !StringUtils.equalsIgnoreCase(parameter.getAccountId(), String.valueOf(Constants.UNKNOWN_ID))) {
            accountIdsText = parameter.getAccountId();
        } else {
            accountIdsText = parameter.getAccountIds();
        }
        if (StringUtils.isNotBlank(accountIdsText) && !StringUtils.equalsIgnoreCase(parameter.getAccountIds(), String.valueOf(Constants.UNKNOWN_ID))) {
            //将账号ID转化为列表
            List<Integer> accountIds = new ArrayList<>();
            for (String accountId : accountIdsText.split(Constant.Separator.COMMA)) {
                accountIds.add(Integer.valueOf(accountId));
            }
            query.setAccountIds(accountIds);
        }
        String planId = parameter.getParameter(DEParameter.Keys.PLAN_ID);
        if (StringUtils.isNotBlank(planId) && StringUtils.isNumeric(planId)) {
            query.setPlanId(Integer.valueOf(planId));
        }
        query.setQuery(parameter.getQuery());
        if (parameter.getPageId() > 0) {
            query.setCurrentPage(parameter.getPageId());
        } else {
            query.setCurrentPage(Defaults.CURRENT_PAGE);
        }

        if (parameter.getPageSize() > 0) {
            query.setPageSize(parameter.getPageSize());
        } else {
            query.setPageSize(Defaults.PAGE_SIZE);
        }

        //解析产品ID列表
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        query.setProductIds(StringUtil.stringToList(productIds));

        String planIdsText = parameter.getParameter("planIds");
        List<Integer> planIds = StringUtil.stringToList(planIdsText);
        query.setPlanIds(planIds);
        //过滤自然流量
        String filterNaturalFlow = parameter.getParameter(DEParameter.Keys.FILTER_NATURAL_FLOW);
        query.setFilterNaturalFlow(Boolean.valueOf(filterNaturalFlow));
        //是否自然流量
        String isNaturalFlow = parameter.getParameter(DEParameter.Keys.IS_NATURAL_FLOW);
        query.setNaturalFlow(Boolean.valueOf(isNaturalFlow));
        return query;
    }


    /**
     * 投放日报下载当前表格
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/report/downloadTable.do")
    public void downloadTable(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        DEParameter parameter = context.getDeParameter();
        Dimension dimension = Dimension.parse(parameter.getDimension());
        LinkID link = LinkID.parse((parameter.getLink()));
        //下载表格时不分页，一次获取所有数据
        parameter.setPageSize(Integer.MAX_VALUE);
        parameter.setPageId(Defaults.CURRENT_PAGE);
        //判断报告类型
        ReportType reportType = getReportType(dimension, link);
        List<DeliveryReportView> reports = new ArrayList<>();
        if(reportType != null && Dimension.systemAccount.equals(dimension)){
        	
        	Object adReports = queryindicatorByAccount(request, response);
        	PageData pageData = (PageData) adReports;
            reports = (List<DeliveryReportView>) pageData.getContent();
        }else{
        	reports = getDeliveryReportViews(request, response);
        }
        //返回文件
        if (reportType != null) {
            output(response, context, reportType, reports);
        }
    }

    /**
     * 获取投放日报视图
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    private List<DeliveryReportView> getDeliveryReportViews(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<DeliveryReportView> reports = null;
        Object adReports = adReport(request, response);
        if (adReports != null) {
            if (adReports instanceof PageData) {
                PageData pageData = (PageData) adReports;
                reports = (List<DeliveryReportView>) pageData.getContent();
            } else if (adReports instanceof List) {
                reports = (List<DeliveryReportView>) adReports;
            } else if (adReports instanceof InnerTable) {
                InnerTable innerTable = (InnerTable) adReports;
                reports = innerTable.getContent();
            }
        }
        return reports;
    }

    /**
     * 获取报告类型
     *
     * @param dimension
     * @param link
     * @return
     */
    private ReportType getReportType(Dimension dimension, LinkID link) {
        ReportType reportType;
        if (Dimension.Plan.equals(dimension)) {
            //推广计划维度
            reportType = ReportType.Plan;
        } else if (Dimension.Medium.equals(dimension) && (LinkID.Account.equals(link) || LinkID.DateAccount.equals(link) || LinkID.DailyAccount.equals(link))) {
            //媒体维度弹出表格
            reportType = ReportType.MediumDetail;
        }else if (Dimension.systemAccount.equals(dimension)) {
            //子账户tab
            reportType = ReportType.systemAccount;
        } else {
            //媒体维度
            reportType = ReportType.Medium;
        }
        return reportType;
    }

    /**
     * 向输出流写入文件
     *
     * @param response
     * @param context
     * @param reportType
     * @param reports
     * @throws IOException
     */
    private void output(HttpServletResponse response, DEContext context, ReportType reportType, List<DeliveryReportView> reports) throws IOException {
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        Workbook wb = adReportService.toWorkBook(reports, reportType, userInSession.getAccountId());
        // Write the output to a file
        response.setContentType(CONTENT_TYPE);
        response.addHeader(CONTENT_DISPOSITION, CONTENT_DISPOSITION_VALUE);
        ServletOutputStream out = response.getOutputStream();
        wb.write(out);
        out.flush();
        out.close();
    }
    
    /**
     * 投放日报子账户Tab
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/report/queryindicatorByAccount.do")
    public Object queryindicatorByAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        //获取公司ID和授权媒体ID列表
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        Integer[] permissionMediumAccountIds = userInSession.getPermissionMediumAccountIds();
        // 如果为null，则表示无数据访问权限，否则，有权限
        if (permissionMediumAccountIds == null) {
            return null;
        }
        //参数解析与校验
        DeliveryReportQuery query = parseQueryGroupByAccount(parameter);
        // 如果为null，则表示开始时间大于等于当前时间，则没有回本率数据
        if (query == null) {
            return null;
        }
        query.setCompanyId(companyId);
        query.setPermissionMediumAccountIds(permissionMediumAccountIds);
        query.setPermissionSystemAccountIds(userInSession.getPermissionSystemAccountIds());
        query.setAccountId(userInSession.getAccountId());
        return adReportService.queryindicatorByAccount(query);
    }

    
    /**
     * 解析统计参数
     *
     * @param parameter
     * @return
     * @throws ParseException
     */
    private DeliveryReportQuery parseQueryGroupByAccount(DEParameter parameter) throws ParseException {
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }
        String mediumAccountIds = parameter.getParameter("mediumAccountIds");
        String systemAccountIds = parameter.getParameter("systemAccountIds");
        
        DeliveryReportQuery query = new DeliveryReportQuery();
        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        query.setStartDate(startDate);
        query.setEndDate(endDate);
        //解析产品ID列表
        String productIds = parameter.getParameter(DEParameter.Keys.PRODUCT_IDS);
        query.setProductIds(StringUtil.stringToList(productIds));
        query.setPlanIds(StringUtil.stringToList(parameter.getPlanIds()));
        query.setAccountIds(StringUtil.stringToList(mediumAccountIds));
        query.setMediumIds(StringUtil.stringToList(parameter.getMediumIds()));
        query.setSystemAccountIds(StringUtil.stringToList(systemAccountIds));
        if (parameter.getPageId() > 0) {
            query.setCurrentPage(parameter.getPageId());
        } else {
            query.setCurrentPage(Defaults.CURRENT_PAGE);
        }
        if (parameter.getPageSize() > 0) {
            query.setPageSize(parameter.getPageSize());
        } else {
            query.setPageSize(Defaults.PAGE_SIZE);
        }
        String orderBy = parameter.getOrderBy();
        String order = parameter.getParameter(Constants.ORDER);
        //检验排序方式和排序字段
        if (!SqlValidator.isValidOrderBy(orderBy)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_ORDER_BY_INVALID);
        }
        if (!SqlValidator.isValidOrder(order)) {
            ExceptionHandler.throwParameterException(StatusCode.DELI_ORDER_INVALID);
        }
        query.setOrderBy(orderBy);
        query.setOrder(order);
        return query;
    }
    
}
