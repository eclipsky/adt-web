package com.dataeye.ad.assistor.module.mediumaccount.model;

import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 媒体账户
 * @author luzhuyou 2017/02/20
 *
 */
public class MediumAccount {
	/** 媒体账户ID */
	@Expose
	private Integer mediumAccountId;
	/** 公司ID */
	private Integer companyId;
	/** 媒体ID */
	private Integer mediumId;
	/** 媒体账号 */
	@Expose
	@SerializedName("mediumAccountName")
	private String mediumAccount;
	/** 媒体账号别名 */
	@Expose
	private String mediumAccountAlias;
	/** 密码 */
	private String password;
	/** 登录状态:0-登陆态正常，1-登录态异常 */
	private Integer loginStatus;
	/** 状态：0-正常，1-失效 */
	private Integer status;
	/** 爬虫所需标识 */
	private String skey;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	/**1-推广计划，2-推广分组，参考ad_crawler_page.page_type*/
	private Integer pageType;
	
	/** 关注者ADT账号名称列表(至少一个系统账号，多个以逗号分隔) */
	private String followerAccounts;
	/** 负责人账号名称 */
	private String responsibleAccounts;
	
	/**帐号类型:负责人—0,关注人---1*/
	@Expose
	private Integer candidatesType;
	
	private String mediumName;
	
	private String mediumAccountIds;
	private String mediumAccountNames;
	private Integer productId;
	
	//日消耗限额
	private Integer dailyBudget;
	
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public String getMediumAccount() {
		return mediumAccount;
	}
	public void setMediumAccount(String mediumAccount) {
		this.mediumAccount = mediumAccount;
	}
	public String getMediumAccountAlias() {
		return mediumAccountAlias;
	}
	public void setMediumAccountAlias(String mediumAccountAlias) {
		this.mediumAccountAlias = mediumAccountAlias;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getLoginStatus() {
		return loginStatus;
	}
	public void setLoginStatus(Integer loginStatus) {
		this.loginStatus = loginStatus;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getSkey() {
		return skey;
	}
	public void setSkey(String skey) {
		this.skey = skey;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getPageType() {
		return pageType;
	}
	public void setPageType(Integer pageType) {
		this.pageType = pageType;
	}
	public String getFollowerAccounts() {
		return followerAccounts;
	}
	public void setFollowerAccounts(String followerAccounts) {
		this.followerAccounts = followerAccounts;
	}
	public String getResponsibleAccounts() {
		return responsibleAccounts;
	}
	public void setResponsibleAccounts(String responsibleAccounts) {
		this.responsibleAccounts = responsibleAccounts;
	}
	
	public Integer getCandidatesType() {
		return candidatesType;
	}
	public void setCandidatesType(Integer candidatesType) {
		this.candidatesType = candidatesType;
	}
	
	public String getMediumAccountIds() {
		return mediumAccountIds;
	}
	public void setMediumAccountIds(String mediumAccountIds) {
		this.mediumAccountIds = mediumAccountIds;
	}
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	
	public String getMediumAccountNames() {
		return mediumAccountNames;
	}
	public void setMediumAccountNames(String mediumAccountNames) {
		this.mediumAccountNames = mediumAccountNames;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	public Integer getDailyBudget() {
		return dailyBudget;
	}
	public void setDailyBudget(Integer dailyBudget) {
		this.dailyBudget = dailyBudget;
	}
	@Override
	public String toString() {
		return "MediumAccount [mediumAccountId=" + mediumAccountId
				+ ", companyId=" + companyId + ", mediumId=" + mediumId
				+ ", mediumAccount=" + mediumAccount + ", mediumAccountAlias="
				+ mediumAccountAlias + ", password=" + password
				+ ", loginStatus=" + loginStatus + ", status=" + status
				+ ", skey=" + skey + ", remark=" + remark + ", createTime="
				+ createTime + ", updateTime=" + updateTime + ", pageType="
				+ pageType + ", followerAccounts=" + followerAccounts
				+ ", responsibleAccounts=" + responsibleAccounts
				+ ", candidatesType=" + candidatesType + ", mediumName="
				+ mediumName + ", mediumAccountIds=" + mediumAccountIds
				+ ", mediumAccountNames=" + mediumAccountNames + ", productId="
				+ productId + ", dailyBudget=" + dailyBudget + "]";
	}
	
}
