package com.dataeye.ad.assistor.module.mediumaccount.model;

import com.google.gson.annotations.Expose;



public class CrawlerPage {
	
	/** 媒体ID */
	private Integer mediumId;
	/**页面类型*/
	@Expose
	private Integer pageType;
	/**页面名称（媒体）*/
	@Expose
	private String pageName;
	
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public Integer getPageType() {
		return pageType;
	}
	public void setPageType(Integer pageType) {
		this.pageType = pageType;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	@Override
	public String toString() {
		return "CrawlerPage [mediumId=" + mediumId + ", pageType=" + pageType
				+ ", pageName=" + pageName + "]";
	}
	
	
	

}
