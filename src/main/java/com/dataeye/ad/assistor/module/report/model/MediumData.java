package com.dataeye.ad.assistor.module.report.model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 媒体（其他媒体或自定义媒体）数据
 */
public class MediumData {
    /**
     * 日期
     */
    private Date statDate;

    @Expose
    private String date;
    /**
     * 产品名称
     */
    @Expose
    private String productName;
    /**
     * 媒体
     */
    @Expose
    private String mediumName;

    /**
     * 媒体账号ID
     */
    @Expose
    private Integer mediumAccountId;

    /**
     * 媒体账号
     */
    @Expose
    private String mediumAccount;
    /**
     * 计划名称
     */
    @Expose
    private String planName;

    /**
     * 计划ID.
     */
    @Expose
    private Integer planId;

    /**
     * 计划来源：0-来自爬虫，非0-表示包尾量补全（等于pkg_id
     */
    @Expose
    private Integer planSource;

    /**
     * 消耗,单位分
     */
    @Expose
    private BigDecimal cost;

    /**
     * 曝光数.
     */
    @Expose
    private Integer exposures;

    /**
     * 点击数.
     */
    @Expose
    private Integer clicks;

    /**
     * 是否可以编辑.
     */
    @Expose
    private boolean isEditable;

    public Date getStatDate() {
        return statDate;
    }

    public void setStatDate(Date statDate) {
        this.statDate = statDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getMediumName() {
        return mediumName;
    }

    public void setMediumName(String mediumName) {
        this.mediumName = mediumName;
    }

    public String getMediumAccount() {
        return mediumAccount;
    }

    public void setMediumAccount(String mediumAccount) {
        this.mediumAccount = mediumAccount;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public Integer getPlanSource() {
        return planSource;
    }

    public void setPlanSource(Integer planSource) {
        this.planSource = planSource;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Integer getExposures() {
        return exposures;
    }

    public void setExposures(Integer exposures) {
        this.exposures = exposures;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    @Override
    public String toString() {
        return "MediumData{" +
                "statDate=" + statDate +
                ", date='" + date + '\'' +
                ", productName='" + productName + '\'' +
                ", mediumName='" + mediumName + '\'' +
                ", mediumAccountId=" + mediumAccountId +
                ", mediumAccount='" + mediumAccount + '\'' +
                ", planName='" + planName + '\'' +
                ", planId=" + planId +
                ", planSource=" + planSource +
                ", cost=" + cost +
                ", exposures=" + exposures +
                ", clicks=" + clicks +
                ", isEditable=" + isEditable +
                '}';
    }
}
