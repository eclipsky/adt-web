package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.BidStrategy;
import com.dataeye.ad.assistor.module.realtimedelivery.model.Bid;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.UcUtils;
import com.dataeye.ad.assistor.util.XXTEAUtil;
import com.google.gson.Gson;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ldj on 2018/01/10.
 */
@Service("ucBidService")
public class UcBidService extends AbstractMediumPlanService<Bid> implements BidService {
    private String URL = "https://e.uc.cn/ad/web/main/unit/v2/batchModifyBid";
    
    /**
     * 成功消息。
     */
    private static final String SUCCESS = "修改出价成功";
    /**
     * 失败消息.
     */
    private static final String FAIL = "修改出价失败";

    @Override
    public Result bid(Bid bid) {
        return execute(bid);
    }

    @Override
    protected String url(String cookie, Long mediumPlanId) {
        return URL;
    }

    @Override
    protected List<NameValuePair> parameters(String cookie, Bid bid) {
    	String userId = bid.getMediumUserId();
    	String a_c_d = XXTEAUtil.encryptToBase64String((DateUtils.winTimestamp()) + "", userId + "");
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("id", bid.getMediumPlanId()+""));
        if (StringUtils.equalsIgnoreCase(bid.getBidStrategy(), BidStrategy.CPC) || StringUtils.equalsIgnoreCase(bid.getBidStrategy(), BidStrategy.CPM)) {
        	params.add(new BasicNameValuePair("bidStage", "1"));
        }
        params.add(new BasicNameValuePair("bid", bid.getBid().setScale(2, RoundingMode.HALF_UP)+""));
        params.add(new BasicNameValuePair("userId", userId));
        params.add(new BasicNameValuePair("a_c_d", a_c_d));
        return params;
    }



    @Override
    protected void requestHeader(HttpRequestBase request, String cookie) {
        request.setHeader(HOST, "e.uc.cn");
        request.setHeader(ORIGIN, "https://e.uc.cn");
        request.setHeader(REFERER, "https://e.uc.cn/ad/static/index.html");
        request.setHeader("uc-csrf-token", UcUtils.getCsrfToken(cookie));
        request.setHeader("x-requested-with", "XMLHttpRequest");
    }

    @Override
    protected Result response(String content) {
        Gson gson = new Gson();
        Response response = gson.fromJson(content, Response.class);
        Result result = new Result();
        //Code为0是操作成功.
        if (response.getStatus() == 0 && response.getMessage().trim().equals("")) {
            result.setSuccess(true);
            result.setMessage(SUCCESS);
        } else {
            result.setSuccess(false);
            result.setMessage(FAIL);
        }
        return result;
    }

    private class Response {
        private int status;
        private String message;
        private Object data;
        private String extend;
        

        public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}
		
		public String getMessage() {
			return message;
		}
		
		public void setMessage(String message) {
			this.message = message;
		}
		
		public Object getData() {
			return data;
		}
		
		public void setData(Object data) {
			this.data = data;
		}
		
		public String getExtend() {
			return extend;
		}
		
		public void setExtend(String extend) {
			this.extend = extend;
		}


		@Override
		public String toString() {
			return "Response [status=" + status + ", message=" + message
					+ ", data=" + data + ", extend=" + extend + "]";
		}
    }

}
