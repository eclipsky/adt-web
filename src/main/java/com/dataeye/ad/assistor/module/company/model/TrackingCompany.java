package com.dataeye.ad.assistor.module.company.model;


/**
 * Tracking公司账号信息
 * @author luzhuyou 2017/07/21
 *
 */
public class TrackingCompany extends Company {

	/** 套餐点击类型:0-限制点击,1-不限制点击 */
	private int clickPackageType;
	/** 套餐总点击次数，当限制点击时，该值必须大于0 */
	private int clickPackageCount;
	public int getClickPackageType() {
		return clickPackageType;
	}
	public void setClickPackageType(int clickPackageType) {
		this.clickPackageType = clickPackageType;
	}
	public int getClickPackageCount() {
		return clickPackageCount;
	}
	public void setClickPackageCount(int clickPackageCount) {
		this.clickPackageCount = clickPackageCount;
	}
}
