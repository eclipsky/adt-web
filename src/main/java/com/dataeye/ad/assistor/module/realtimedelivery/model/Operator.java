package com.dataeye.ad.assistor.module.realtimedelivery.model;

import org.apache.commons.lang.StringUtils;

/**
 * Created by huangzehai on 2017/3/1.
 */
public enum Operator {
    LTE(0), GTE(1);
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    Operator(int value) {
        this.value = value;
    }

    public static Operator parse(int value) {
        for (Operator operator : Operator.values()) {
            if (operator.getValue() == value) {
                return operator;
            }
        }
        return null;
    }
}
