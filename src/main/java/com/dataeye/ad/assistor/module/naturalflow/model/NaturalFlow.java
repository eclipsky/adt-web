package com.dataeye.ad.assistor.module.naturalflow.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 自然流量
 * Created by huangzehai on 2017/7/14.
 */
public class NaturalFlow {
    /**
     * 统计日期
     */
    private Date statDate;
    /**
     * 公司ID
     */
    private Integer companyId;
    /**
     * 产品ID.
     */
    private Integer productId;
    /**
     * 产品名称.
     */
    private String productName;

    /**
     * 激活数
     */
    private Integer activations;
    /**
     * 注册数
     */
    private Integer registrations;

    /**
     * 付费设备数
     */
    private Integer totalPayingUsers;
    /**
     * 总充值（新老用户）
     */
    private BigDecimal totalPayingAmount;

    /**
     * 新增充值金额
     */
    private BigDecimal newlyIncreasedRechargeAmount;

    /**
     * 累计充值(选定投放计划的所有充值)
     */
    private BigDecimal accumulatedPayingAmount;
    /**
     * DAU
     */
    private Integer dau;

    /**
     * 真实消耗.
     */
    private BigDecimal realCost;

    /**
     * 消耗
     */
    private BigDecimal cost;
    /**
     * 充值
     */
    private BigDecimal recharge;

    /**
     * 回本率.
     */
    private Double paybackRate;

    /**
     * 注册CPA.
     */
    private BigDecimal cpa;

    /**每日充值金额*/
    private BigDecimal dayPayAmount;

    /**
     * 首日充值金额
     */
    private BigDecimal payAmount1day;

    /**
     * 7日充值金额
     */
    private BigDecimal payAmount7days;

    /**
     * 30日充值金额
     */
    private BigDecimal payAmount30days;
    
    /**
     * 注册帐号数
     */
    private Integer registerAccountNum; 
    /**
     * 次日留存率。
     */
    private Double nextDayRetentionRate;

    /**
     * 三日留存率。
     */
    private Double threeDayRetentionRate;

    /**
     * 七日留存率.
     */
    private Double sevenDayRetentionRate;

    /**
     * 三十日留存率.
     */
    private Double thirtyDayRetentionRate;
    
    /**
     * 15日留存率.
     */
    private Double fifteenDayRetentionRate;
    
    /**
     * 首日注册设备数
     * */
    private Integer firstDayRegistrations;
    
    /**
     * 首日注册账号数
     * */
    private Integer firstDayRegisterAccountNum;
    /**
     * 首日LTV
     */
    private BigDecimal ltv1Day;

    /**
     * 7日LTV
     */
    private BigDecimal ltv7Days;

    /**
     * 30日LTV
     */
    private BigDecimal ltv30Days;
    
    //
    private Integer registerAccountNumD3;
    private Integer registerAccountNumD7;
    private Integer registerAccountNumD15;
    private Integer registerAccountNumD30;
    private Integer retainD1;
    private Integer retainD3;
    private Integer retainD7;
    private Integer retainD14;
    private Integer retainD30;

    public Date getStatDate() {
        return statDate;
    }

    public void setStatDate(Date statDate) {
        this.statDate = statDate;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getActivations() {
        return activations;
    }

    public void setActivations(Integer activations) {
        this.activations = activations;
    }

    public Integer getRegistrations() {
        return registrations;
    }

    public void setRegistrations(Integer registrations) {
        this.registrations = registrations;
    }

    public Integer getTotalPayingUsers() {
        return totalPayingUsers;
    }

    public void setTotalPayingUsers(Integer totalPayingUsers) {
        this.totalPayingUsers = totalPayingUsers;
    }

    public BigDecimal getTotalPayingAmount() {
        return totalPayingAmount;
    }

    public void setTotalPayingAmount(BigDecimal totalPayingAmount) {
        this.totalPayingAmount = totalPayingAmount;
    }

    public BigDecimal getAccumulatedPayingAmount() {
        return accumulatedPayingAmount;
    }

    public void setAccumulatedPayingAmount(BigDecimal accumulatedPayingAmount) {
        this.accumulatedPayingAmount = accumulatedPayingAmount;
    }

    public Integer getDau() {
        return dau;
    }

    public void setDau(Integer dau) {
        this.dau = dau;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getRecharge() {
        return recharge;
    }

    public void setRecharge(BigDecimal recharge) {
        this.recharge = recharge;
    }

    public Double getPaybackRate() {
        return paybackRate;
    }

    public void setPaybackRate(Double paybackRate) {
        this.paybackRate = paybackRate;
    }

    public BigDecimal getRealCost() {
        return realCost;
    }

    public void setRealCost(BigDecimal realCost) {
        this.realCost = realCost;
    }

    public BigDecimal getCpa() {
        return cpa;
    }

    public void setCpa(BigDecimal cpa) {
        this.cpa = cpa;
    }

    public BigDecimal getNewlyIncreasedRechargeAmount() {
        return newlyIncreasedRechargeAmount;
    }

    public void setNewlyIncreasedRechargeAmount(BigDecimal newlyIncreasedRechargeAmount) {
        this.newlyIncreasedRechargeAmount = newlyIncreasedRechargeAmount;
    }

    public BigDecimal getDayPayAmount() {
        return dayPayAmount;
    }

    public void setDayPayAmount(BigDecimal dayPayAmount) {
        this.dayPayAmount = dayPayAmount;
    }

    public BigDecimal getPayAmount1day() {
        return payAmount1day;
    }

    public void setPayAmount1day(BigDecimal payAmount1day) {
        this.payAmount1day = payAmount1day;
    }

    public BigDecimal getPayAmount7days() {
        return payAmount7days;
    }

    public void setPayAmount7days(BigDecimal payAmount7days) {
        this.payAmount7days = payAmount7days;
    }

    public BigDecimal getPayAmount30days() {
        return payAmount30days;
    }

    public void setPayAmount30days(BigDecimal payAmount30days) {
        this.payAmount30days = payAmount30days;
    }
    public Integer getRegisterAccountNum() {
		return registerAccountNum;
	}
	public void setRegisterAccountNum(Integer registerAccountNum) {
		this.registerAccountNum = registerAccountNum;
	}
	
	public Double getNextDayRetentionRate() {
		return nextDayRetentionRate;
	}

	public void setNextDayRetentionRate(Double nextDayRetentionRate) {
		this.nextDayRetentionRate = nextDayRetentionRate;
	}

	public Double getThreeDayRetentionRate() {
		return threeDayRetentionRate;
	}

	public void setThreeDayRetentionRate(Double threeDayRetentionRate) {
		this.threeDayRetentionRate = threeDayRetentionRate;
	}

	public Double getSevenDayRetentionRate() {
		return sevenDayRetentionRate;
	}

	public void setSevenDayRetentionRate(Double sevenDayRetentionRate) {
		this.sevenDayRetentionRate = sevenDayRetentionRate;
	}

	public Double getThirtyDayRetentionRate() {
		return thirtyDayRetentionRate;
	}

	public void setThirtyDayRetentionRate(Double thirtyDayRetentionRate) {
		this.thirtyDayRetentionRate = thirtyDayRetentionRate;
	}

	public Double getFifteenDayRetentionRate() {
		return fifteenDayRetentionRate;
	}

	public void setFifteenDayRetentionRate(Double fifteenDayRetentionRate) {
		this.fifteenDayRetentionRate = fifteenDayRetentionRate;
	}

	public Integer getFirstDayRegistrations() {
		return firstDayRegistrations;
	}

	public void setFirstDayRegistrations(Integer firstDayRegistrations) {
		this.firstDayRegistrations = firstDayRegistrations;
	}

	public Integer getFirstDayRegisterAccountNum() {
		return firstDayRegisterAccountNum;
	}

	public void setFirstDayRegisterAccountNum(Integer firstDayRegisterAccountNum) {
		this.firstDayRegisterAccountNum = firstDayRegisterAccountNum;
	}

	public BigDecimal getLtv1Day() {
		return ltv1Day;
	}

	public void setLtv1Day(BigDecimal ltv1Day) {
		this.ltv1Day = ltv1Day;
	}

	public BigDecimal getLtv7Days() {
		return ltv7Days;
	}

	public void setLtv7Days(BigDecimal ltv7Days) {
		this.ltv7Days = ltv7Days;
	}

	public BigDecimal getLtv30Days() {
		return ltv30Days;
	}

	public void setLtv30Days(BigDecimal ltv30Days) {
		this.ltv30Days = ltv30Days;
	}

	public Integer getRegisterAccountNumD3() {
		return registerAccountNumD3;
	}

	public void setRegisterAccountNumD3(Integer registerAccountNumD3) {
		this.registerAccountNumD3 = registerAccountNumD3;
	}

	public Integer getRegisterAccountNumD7() {
		return registerAccountNumD7;
	}

	public void setRegisterAccountNumD7(Integer registerAccountNumD7) {
		this.registerAccountNumD7 = registerAccountNumD7;
	}

	public Integer getRegisterAccountNumD15() {
		return registerAccountNumD15;
	}

	public void setRegisterAccountNumD15(Integer registerAccountNumD15) {
		this.registerAccountNumD15 = registerAccountNumD15;
	}

	public Integer getRegisterAccountNumD30() {
		return registerAccountNumD30;
	}

	public void setRegisterAccountNumD30(Integer registerAccountNumD30) {
		this.registerAccountNumD30 = registerAccountNumD30;
	}

	public Integer getRetainD1() {
		return retainD1;
	}

	public void setRetainD1(Integer retainD1) {
		this.retainD1 = retainD1;
	}

	public Integer getRetainD3() {
		return retainD3;
	}

	public void setRetainD3(Integer retainD3) {
		this.retainD3 = retainD3;
	}

	public Integer getRetainD7() {
		return retainD7;
	}

	public void setRetainD7(Integer retainD7) {
		this.retainD7 = retainD7;
	}

	public Integer getRetainD14() {
		return retainD14;
	}

	public void setRetainD14(Integer retainD14) {
		this.retainD14 = retainD14;
	}

	public Integer getRetainD30() {
		return retainD30;
	}

	public void setRetainD30(Integer retainD30) {
		this.retainD30 = retainD30;
	}

	@Override
	public String toString() {
		return "NaturalFlow [statDate=" + statDate + ", companyId=" + companyId
				+ ", productId=" + productId + ", productName=" + productName
				+ ", activations=" + activations + ", registrations="
				+ registrations + ", totalPayingUsers=" + totalPayingUsers
				+ ", totalPayingAmount=" + totalPayingAmount
				+ ", newlyIncreasedRechargeAmount="
				+ newlyIncreasedRechargeAmount + ", accumulatedPayingAmount="
				+ accumulatedPayingAmount + ", dau=" + dau + ", realCost="
				+ realCost + ", cost=" + cost + ", recharge=" + recharge
				+ ", paybackRate=" + paybackRate + ", cpa=" + cpa
				+ ", dayPayAmount=" + dayPayAmount + ", payAmount1day="
				+ payAmount1day + ", payAmount7days=" + payAmount7days
				+ ", payAmount30days=" + payAmount30days
				+ ", registerAccountNum=" + registerAccountNum
				+ ", nextDayRetentionRate=" + nextDayRetentionRate
				+ ", threeDayRetentionRate=" + threeDayRetentionRate
				+ ", sevenDayRetentionRate=" + sevenDayRetentionRate
				+ ", thirtyDayRetentionRate=" + thirtyDayRetentionRate
				+ ", fifteenDayRetentionRate=" + fifteenDayRetentionRate
				+ ", firstDayRegistrations=" + firstDayRegistrations
				+ ", firstDayRegisterAccountNum=" + firstDayRegisterAccountNum
				+ ", ltv1Day=" + ltv1Day + ", ltv7Days=" + ltv7Days
				+ ", ltv30Days=" + ltv30Days + ", registerAccountNumD3="
				+ registerAccountNumD3 + ", registerAccountNumD7="
				+ registerAccountNumD7 + ", registerAccountNumD15="
				+ registerAccountNumD15 + ", registerAccountNumD30="
				+ registerAccountNumD30 + ", retainD1=" + retainD1
				+ ", retainD3=" + retainD3 + ", retainD7=" + retainD7
				+ ", retainD14=" + retainD14 + ", retainD30=" + retainD30 + "]";
	}
}
