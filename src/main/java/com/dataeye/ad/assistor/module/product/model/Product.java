package com.dataeye.ad.assistor.module.product.model;

import java.util.Date;

/**
 * 产品信息
 * @author luzhuyou 2017/02/20
 *
 */
public class Product {

	/** 产品ID */
	private Integer productId;
	/** 公司ID */
	private Integer companyId;
	/** 产品名称 */
	private String productName;
	/** 状态: 0-正常（默认），1-失效*/
	private Integer status;
	/** 备注 */
	private String remark;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	/** TrackingAppID */
	private String appId;
	
	/**平台*/
	private Integer osType;
	/**一级产品分类id**/
	private Integer parentCategoryId;
	/**二级产品分类id**/
	private Integer childrenCategoryId;
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	public Integer getChildrenCategoryId() {
		return childrenCategoryId;
	}
	public void setChildrenCategoryId(Integer childrenCategoryId) {
		this.childrenCategoryId = childrenCategoryId;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", companyId=" + companyId
				+ ", productName=" + productName + ", status=" + status
				+ ", remark=" + remark + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", appId=" + appId
				+ ", osType=" + osType + ", parentCategoryId="
				+ parentCategoryId + ", childrenCategoryId="
				+ childrenCategoryId + "]";
	}
	
	
}
