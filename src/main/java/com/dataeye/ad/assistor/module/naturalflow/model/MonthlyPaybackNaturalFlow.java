package com.dataeye.ad.assistor.module.naturalflow.model;

import java.math.BigDecimal;

public class MonthlyPaybackNaturalFlow extends NaturalFlow{
    /**
     * 当月充值金额.
     */
    private BigDecimal payAmount1Month;

    /**
     * 前2月充值金额.
     */
    private BigDecimal payAmount2Months;

    /**
     * 前3月充值金额.
     */
    private BigDecimal payAmount3Months;

    /**
     * 前4月充值金额.
     */
    private BigDecimal payAmount4Months;

    /**
     * 前5月充值金额.
     */
    private BigDecimal payAmount5Months;

    /**
     * 前6月充值金额.
     */
    private BigDecimal payAmount6Months;

    /**
     * 前7月充值金额.
     */
    private BigDecimal payAmount7Months;

    public BigDecimal getPayAmount1Month() {
        return payAmount1Month;
    }

    public void setPayAmount1Month(BigDecimal payAmount1Month) {
        this.payAmount1Month = payAmount1Month;
    }

    public BigDecimal getPayAmount2Months() {
        return payAmount2Months;
    }

    public void setPayAmount2Months(BigDecimal payAmount2Months) {
        this.payAmount2Months = payAmount2Months;
    }

    public BigDecimal getPayAmount3Months() {
        return payAmount3Months;
    }

    public void setPayAmount3Months(BigDecimal payAmount3Months) {
        this.payAmount3Months = payAmount3Months;
    }

    public BigDecimal getPayAmount4Months() {
        return payAmount4Months;
    }

    public void setPayAmount4Months(BigDecimal payAmount4Months) {
        this.payAmount4Months = payAmount4Months;
    }

    public BigDecimal getPayAmount5Months() {
        return payAmount5Months;
    }

    public void setPayAmount5Months(BigDecimal payAmount5Months) {
        this.payAmount5Months = payAmount5Months;
    }

    public BigDecimal getPayAmount6Months() {
        return payAmount6Months;
    }

    public void setPayAmount6Months(BigDecimal payAmount6Months) {
        this.payAmount6Months = payAmount6Months;
    }

    public BigDecimal getPayAmount7Months() {
        return payAmount7Months;
    }

    public void setPayAmount7Months(BigDecimal payAmount7Months) {
        this.payAmount7Months = payAmount7Months;
    }

    @Override
    public String toString() {
        return "MonthlyPaybackNaturalFlow{" +
                "payAmount1Month=" + payAmount1Month +
                ", payAmount2Months=" + payAmount2Months +
                ", payAmount3Months=" + payAmount3Months +
                ", payAmount4Months=" + payAmount4Months +
                ", payAmount5Months=" + payAmount5Months +
                ", payAmount6Months=" + payAmount6Months +
                ", payAmount7Months=" + payAmount7Months +
                "} " + super.toString();
    }
}
