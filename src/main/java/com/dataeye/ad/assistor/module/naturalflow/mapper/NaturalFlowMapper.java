package com.dataeye.ad.assistor.module.naturalflow.mapper;

import com.dataeye.ad.assistor.module.naturalflow.model.DailyPaybackNaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.MonthlyPaybackNaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlow;
import com.dataeye.ad.assistor.module.naturalflow.model.NaturalFlowQuery;
import com.dataeye.ad.assistor.module.report.model.DateRangeQuery;
import com.dataeye.ad.assistor.module.result.model.DailyIndicator;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by huangzehai on 2017/7/14.
 */
public interface NaturalFlowMapper {
    /**
     * 查询自然流量,按产品和日期分组
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getNaturalFlowsGroupByProductAndDate(NaturalFlowQuery query);

    /**
     * 查询自然流量,按日期分组
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getNaturalFlowsGroupByDate(NaturalFlowQuery query);

    /**
     * 查询自然流量,按产品分组
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getNaturalFlowsGroupByProduct(NaturalFlowQuery query);

    /**
     * 获取自然流量汇总数据
     *
     * @param query
     * @return
     */
    NaturalFlow getNaturalFlowTotal(NaturalFlowQuery query);

    /**
     * 获取自然流量在所选日期范围的累计充值
     *
     * @param query
     * @return
     */
    BigDecimal getNaturalFlowRecharge(DateRangeQuery query);


    /**
     * 获取自然流量在所选日期范围的累计充值,按产品分组.
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getNaturalFlowRechargeGroupByDate(DateRangeQuery query);


    /**
     * 获取自然流量在所选日期范围的累计充值,按产品分组.
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getNaturalFlowRechargeGroupByProduct(DateRangeQuery query);

    /**
     * 获取自然流量在所选日期范围的累计充值,按产品和日期分组.
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getNaturalFlowRechargeGroupByProductAndDate(DateRangeQuery query);

    /**
     * 获取指定日期范围的某公司的真实总消耗
     *
     * @param query
     * @return
     */
    BigDecimal getRealTotalCost(DateRangeQuery query);

    /**
     * 获取指定日期范围的某公司的真实总充值
     *
     * @param query
     * @return
     */
    BigDecimal getRealTotalRecharge(DateRangeQuery query);

    /**
     * 获取某个公司的真实消耗，按产品分组统计
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getRealCostGroupByProduct(DateRangeQuery query);

    /**
     * 获取某个公司的真实充值，按产品分组统计
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getRealRechargeGroupByProduct(DateRangeQuery query);

    /**
     * 获取指定公司在所选时期的总消耗
     *
     * @param query
     * @return
     */
    BigDecimal getTotalCost(DateRangeQuery query);

    /**
     * 获取指定公司在所选时期的总消耗，按产品分组
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getCostGroupByProduct(DateRangeQuery query);

    /**
     * 获取指定公司在所选时期的总消耗，按日期分组
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getCostGroupByDate(DateRangeQuery query);

    /**
     * 获取指定公司在所选时期的总消耗，按产品和日期分组
     *
     * @param query
     * @return
     */
    List<NaturalFlow> getCostGroupByProductAndDate(DateRangeQuery query);

    /**
     * 获取指定公司时间范围最后一天的DAU.
     */
    Integer getNaturalFlowDau(DateRangeQuery query);

    /**
     * 获取指定公司时间范围最后一天的DAU, 按产品分组
     */
    List<NaturalFlow> getNaturalFlowDauGroupByProduct(DateRangeQuery query);

    /**
     * 获取指定公司时间范围最后一天的DAU, 按日期分组
     */
    List<NaturalFlow> getNaturalFlowDauGroupByDate(DateRangeQuery query);

    /**
     * 获取指定公司时间范围最后一天的DAU, 按产品和日期分组
     */
    List<DailyIndicator> getNaturalFlowDauGroupByProductAndDate(DateRangeQuery query);

    /**
     * 获取指定公司时间范围内每日回本自然流量
     *
     * @param query
     * @return
     */
    List<DailyPaybackNaturalFlow> getDailyPaybackNaturalFlowsGroupByDate(NaturalFlowQuery query);


    /**
     * 获取指定公司时间范围内每日回本自然流量 (LTV)
     *
     * @param query
     * @return
     */
    List<DailyPaybackNaturalFlow> getDailyPaybackLtvNaturalFlowsGroupByDate(NaturalFlowQuery query);

    /**
     * 获取指定公司时间范围内每月回本自然流量
     *
     * @param query
     * @return
     */
    List<MonthlyPaybackNaturalFlow> getMonthlyPaybackNaturalFlowsGroupByDate(NaturalFlowQuery query);

    /**
     * 获取指定公司指定产品自投放日开始90天内每天的充值金额
     *
     * @param query
     * @return
     */
    List<NaturalFlow> dailyNaturalFlowPayAmount(NaturalFlowQuery query);
}
