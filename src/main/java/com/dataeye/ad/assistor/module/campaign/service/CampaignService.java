package com.dataeye.ad.assistor.module.campaign.service;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.Constant.MediumType;
import com.dataeye.ad.assistor.constant.Constant.ServerCfg;
import com.dataeye.ad.assistor.constant.RemoteInterfaceConstants.Tracking;
import com.dataeye.ad.assistor.module.campaign.constants.TrackingResCampaign;
import com.dataeye.ad.assistor.module.campaign.constants.TrackingResChannelParams;
import com.dataeye.ad.assistor.module.campaign.constants.TrackingResTrackUrl;
import com.dataeye.ad.assistor.module.campaign.mapper.CampaignMapper;
import com.dataeye.ad.assistor.module.campaign.model.*;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import com.dataeye.ad.assistor.module.dictionaries.service.ConfigService;
import com.dataeye.ad.assistor.module.dictionaries.service.PlanService;
import com.dataeye.ad.assistor.module.mediumaccount.service.MediumService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 推广计划Service
 *
 * @author luzhuyou 2017/06/19
 */
@Service("campaignService")
public class CampaignService {

    @Autowired
    private CampaignMapper campaignMapper;
    @Autowired
    private CampaignRemoteService campaignRemoteService;
    @Autowired
    private MediumService mediumService;
    @Autowired
    private PlanService planService;
    
    @Autowired
    private ConfigService configService;

    /**
     * 查询推广计划
     *
     * @param vo
     * @return
     */
    public List<CampaignVo> query(CampaignQueryReqVo vo) {
        // 查询根据条件筛选出来的短链活动ID
        List<CampaignVo> campaignList = campaignMapper.query(vo);
        if (vo != null && StringUtils.isBlank(vo.getAppIds())) {
            vo.setAppIds(campaignMapper.queryAppIds(vo));
        }

        if (campaignList.size() > 0 && vo.getAppIds() != null && !vo.getAppIds().trim().equals("")) {

            // 查询tracking的推广计划
            List<TrackingResCampaign> trackingCampaignList = campaignRemoteService.queryCampaign(vo);

            // 基于短链活动ID在tracking的推广计划中查询匹配的活动，组装数据
            for (CampaignVo campaignvo : campaignList) {

                for (TrackingResCampaign trackingvo : trackingCampaignList) {

                    if (campaignvo.getCampaignId().equals(trackingvo.getCampaignId())) {
                        campaignvo.setCampaignName(trackingvo.getCampaignName());
                        campaignvo.setTrackUrl(trackingvo.getTrackUrl());
                        campaignvo.setDownloadUrl(trackingvo.getDownloadUrl());
                        campaignvo.setThirdAdParams(trackingvo.getThirdAdParams());

                        //拼接下载监测地址和到达监测链接
                        String monitorUrl = ConfigHandler.getProperty(Tracking.MONITOR_URL_KEY, Tracking.MONITOR_URL);
/*                        //如果监测链接地址返回的是https开头，则monitorUrl也使用https,返回http，则monitorUrl使用http  ,update ldj by 2018-01-19
                        if (trackingvo.getTrackUrl() != null && trackingvo.getTrackUrl().startsWith("https")) {
                            monitorUrl = "https" + monitorUrl.substring("http".length());
                        }*/
                        String downloadMonitorUrl = monitorUrl + campaignvo.getCampaignId() + "?event=download";
                        String arrivalMonitorUrl = monitorUrl + campaignvo.getCampaignId() + "?event=arrival";
                        campaignvo.setDownloadMonitorUrl(downloadMonitorUrl);
                        campaignvo.setArrivalMonitorUrl(arrivalMonitorUrl);
                        campaignvo.setType(trackingvo.getType());
                        campaignvo.setEventType(trackingvo.getEventType());
                        break;
                    }
                }
            }
        }

        return campaignList;
    }

    /**
     * 下拉框查询推广计划
     *
     * @param vo
     * @return
     */
    public List<CampaignSelectorVo> queryForSelector(CampaignQueryReqVo vo) {
        List<CampaignSelectorVo> campaignList = campaignMapper.queryForSelector(vo);

        if (vo != null && StringUtils.isBlank(vo.getAppIds())) {

            vo.setAppIds(campaignMapper.queryAppIds(vo));
        }
        if (campaignList.size() > 0 && vo.getAppIds() != null && !vo.getAppIds().trim().equals("")) {

            // 查询tracking的推广计划
            List<TrackingResCampaign> trackingCampaignList = campaignRemoteService.queryCampaign(vo);

            // 基于短链活动ID在tracking的推广计划中查询匹配的活动，组装数据
            for (CampaignSelectorVo campaignvo : campaignList) {

                for (TrackingResCampaign trackingvo : trackingCampaignList) {

                    if (campaignvo.getCampaignId().equals(trackingvo.getCampaignId())) {
                        campaignvo.setCampaignName(trackingvo.getCampaignName());
                        break;
                    }
                }
            }
        }


        return campaignList;
    }

    /**
     * 更新推广计划名称或落地页Url
     *
     * @param vo
     * @return
     */
    public int modify(CampaignVo vo) {
        int result = 0;
        String campaignId = vo.getCampaignId();
        campaignRemoteService.modifyCampaign(vo);
        Campaign campaign = new Campaign();
        campaign.setCampaignId(campaignId);
        campaign.setUpdateTime(new Date());
        result = campaignMapper.update(campaign);

        
        //根据短链号，查询活动信息，如果媒体为非adt媒体，则向plan表插入记录
        CampaignVo oldCampaign = campaignMapper.getCampaignById(campaignId);
        if(oldCampaign != null){
        	//查询配置表，监测链接中，投放媒体是广点通，是否在ad_plan添加一条记录
        	String planName = vo.getCampaignName() + ServerCfg.SEPARATOR_UNDERLINE + campaignId;
        	boolean flag = false ;
        	
        	String configValue = configService.queryFromCache("gdt_campaign_as_plan");
        	List<Integer> companysConfig = configService.queryFromCacheByList("gdt_active_company_ids");
        	List<Integer> mediumAccountIdsConfig = configService.queryFromCacheByList("gdt_active_user_ids");
        	if(configValue != null && configValue.trim().equals("1") && companysConfig.size() > 0 && mediumAccountIdsConfig.size() > 0){
        		//验证改公司或者该用户是否有权限，如果存在，则表示有，
        		if((companysConfig.contains(vo.getCompanyId()) || mediumAccountIdsConfig.contains(campaign.getMediumAccountId())) && oldCampaign.getMediumId() == 13){
        			flag = true;
        			planName = "NULL_" + vo.getCampaignName() + ServerCfg.SEPARATOR_UNDERLINE + vo.getCampaignId();
        		}
        	}
        	if (MediumType.getMediumType(oldCampaign.getMediumId()) == MediumType.TRACKING_COMMON || MediumType.getMediumType(oldCampaign.getMediumId()) == MediumType.CUSTOM || flag) {
        		Plan oldPlan = planService.getPlanBymPlanId(oldCampaign.getCompanyId(), oldCampaign.getMediumId(), oldCampaign.getMediumAccountId(), campaignId);
        		if (oldPlan != null && !oldPlan.getPlanName().equals(planName)) {
        			result = planService.modifyPlanName(planName, oldPlan.getPlanId());
        		}
        	}
        }
        return result;
    }

    /**
     * 删除更新推广计划
     *
     * @param appId
     * @param companyAccountUid
     * @param campaignId
     * @return
     */
    public int delete(String appId, int companyAccountUid, String campaignId) {
        campaignRemoteService.deleteCampaign(appId, companyAccountUid, campaignId);
        return campaignMapper.delete(campaignId);
    }

    /**
     * 查询Tracking渠道参数
     *
     * @param appId
     * @param companyAccountUid
     * @param mediumId
     * @param osType
     * @param channelId
     * @return
     */
    public TrackingResChannelParams getChannelParams(String appId, int companyAccountUid,
                                                     int mediumId, int osType, String channelId) {
        /*
		// 获取Tracking媒体ID
		String trackingMediumId = getTrackingMediumId(mediumId);*/
        TrackingResChannelParams channelParams = campaignRemoteService.getChannelParams(appId, companyAccountUid, osType, channelId);
        //List<TemplateParams> channelParams = campaignRemoteService.getChannelParams(appId, companyAccountUid, osType, trackingMediumId);
        return channelParams;
    }

    /**
     * 预创建推广链接
     *
     * @param vo
     * @return
     */
    public TrackingResTrackUrl preCreate(CampaignCreateReqVo vo) {
		/*String trackingMediumId = getTrackingMediumId(vo.getMediumId());
		vo.setTrackingMediumId(NumberUtils.toInt(trackingMediumId));*/
        return campaignRemoteService.preCreateCampaign(vo);
    }

    /**
     * 创建推广活动
     *
     * @param vo
     * @return
     */
    public void create(CampaignCreateReqVo vo) {
		
		/*String trackingMediumId = getTrackingMediumId(vo.getMediumId());
		vo.setTrackingMediumId(NumberUtils.toInt(trackingMediumId));*/
        List<TrackingResTrackUrl> trackUrlList = campaignRemoteService.createCampaign(vo);
        List<CampaignCreateReqVo> campaignList = new ArrayList<CampaignCreateReqVo>();
        List<Plan> planList = new ArrayList<Plan>();

        Date now = new Date();
        CampaignCreateReqVo campaign = null;
        for (TrackingResTrackUrl trackUrl : trackUrlList) {
            campaign = new CampaignCreateReqVo();
            campaign = campaign.copy(vo);
            campaign.setCampaignId(trackUrl.getCampaignId());
            campaign.setTrackUrl(trackUrl.getTrackUrl());
            campaign.setCampaignName(trackUrl.getCampaignName());
            campaign.setCreateTime(now);
            campaign.setUpdateTime(now);
            campaignList.add(campaign);
            //如果媒体为非adt媒体，则向plan表插入记录
            int mediumId = campaign.getMediumId();
            int mediumType = MediumType.getMediumType(mediumId);
            
          //查询配置表，监测链接中，投放媒体是广点通，是否在ad_plan添加一条记录
        	String planName = trackUrl.getCampaignName() + ServerCfg.SEPARATOR_UNDERLINE + trackUrl.getCampaignId();
        	boolean flag = false ;
        	
        	String configValue = configService.queryFromCache("gdt_campaign_as_plan");
        	List<Integer> companysConfig = configService.queryFromCacheByList("gdt_active_company_ids");
        	List<Integer> mediumAccountIdsConfig = configService.queryFromCacheByList("gdt_active_user_ids");
        	if(configValue != null && configValue.trim().equals("1") && companysConfig.size() > 0 && mediumAccountIdsConfig.size() > 0){
        		//验证改公司或者该用户是否有权限，如果存在，则表示有，
        		if((companysConfig.contains(vo.getCompanyId()) || mediumAccountIdsConfig.contains(campaign.getMediumAccountId())) && mediumId == 13){
        			flag = true;
        			planName = "NULL_" + trackUrl.getCampaignName() + ServerCfg.SEPARATOR_UNDERLINE + trackUrl.getCampaignId();
        		}
        	}
        	
            if (mediumType == MediumType.TRACKING_COMMON || mediumType == MediumType.CUSTOM || flag) {
                int companyId = campaign.getCompanyId();
                int mediumAccountId = campaign.getMediumAccountId();
                String mPlanId = campaign.getCampaignId();
                Plan oldPlan = planService.getPlanBymPlanId(companyId, mediumId, mediumAccountId, mPlanId);
                if (oldPlan == null) {
                    Plan plan = new Plan();
                    plan.setPlanName(planName);
                    plan.setmPlanId(mPlanId);
                    plan.setMediumId(mediumId);
                    plan.setMediumAccountId(mediumAccountId);
                    plan.setCompanyId(companyId);
                    plan.setOsType(campaign.getOsType());
                    plan.setStatus(0);
                    plan.setCreateTime(now);
                    plan.setUpdateTime(now);
                    planList.add(plan);
                }
            }
        }
        campaignMapper.batchInsert(campaignList);

        if (planList.size() > 0) {

            //如果媒体为非adt媒体，则向plan表插入记录
            planService.batchInsert(planList);
        }

    }

    /**
     * 获取tracking媒体ID
     * @param mediumId
     * @return
     */
/*	private String getTrackingMediumId(int mediumId) {
		Channel channel = new Channel();
		channel.setMediumId(mediumId);
		// 获取Tracking媒体ID
		String trackingMediumId = mediumService.getTrackingMediumId(channel);
		if(trackingMediumId == null || trackingMediumId.equals("")) {
			ExceptionHandler.throwGeneralServerException(StatusCode.CAMP_NO_TRACKING_MEDIUM_ID);
		}
		return trackingMediumId;
	}*/
}
