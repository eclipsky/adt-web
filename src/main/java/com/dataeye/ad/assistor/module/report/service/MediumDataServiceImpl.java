package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.module.report.mapper.MediumDataMapper;
import com.dataeye.ad.assistor.module.report.model.CommonQuery;
import com.dataeye.ad.assistor.module.report.model.MediumData;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.DateUtils;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MediumDataServiceImpl implements MediumDataService {
    @Autowired
    private MediumDataMapper mediumDataMapper;

    @Override
    public Page<MediumData> listMediumData(CommonQuery query) {
        Page<MediumData> mediumDataList = (Page<MediumData>) mediumDataMapper.listMediumData(query);
        format(mediumDataList);
        return mediumDataList;
    }

    /**
     * 格式化媒体数据
     *
     * @param mediumDataList
     */
    private void format(Page<MediumData> mediumDataList) {
        if (mediumDataList != null) {
            for (MediumData mediumData : mediumDataList) {
                mediumData.setDate(DateUtils.format(mediumData.getStatDate()));
                mediumData.setCost(CurrencyUtils.fenToYuan(mediumData.getCost()));
            }
        }
    }

    @Override
    public int updateMediumData(List<MediumData> mediumDataList) {
        return mediumDataMapper.updateMediumData(mediumDataList);
    }
}
