package com.dataeye.ad.assistor.module.campaign.util;

import com.dataeye.ad.assistor.module.campaign.model.CampaignVo;
import com.dataeye.ad.assistor.util.StringUtil;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * Created by ldj on 2018/02/24.
 */
public final class ExcelUtils {

    /**
     * 监测链接表头字段名称.
     */
    private static final String[] CAMPAIGN_COLUMN_NAMES = {"监测链接", "类型", "广告名称", "目标地址","媒体帐号", "推广媒体", "推广产品", "平台"};

    public static Workbook buildWorkBookForCampaign(List<CampaignVo> result) {
        HSSFWorkbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("监测链接");
        addHead(sheet, CAMPAIGN_COLUMN_NAMES);
        Row row;

        int rowIndex = 1;
        for (CampaignVo campaignvo : result) {
            row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(campaignvo.getTrackUrl()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(campaignvo.getType()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(campaignvo.getCampaignName()+"_"+campaignvo.getCampaignId()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(campaignvo.getDownloadUrl()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(campaignvo.getMediumAccount()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(campaignvo.getMediumName()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(campaignvo.getProductName()));
            row.createCell(columnIndex++).setCellValue(StringUtil.defaultIfNull(findOsType(campaignvo.getOsType())));
        }
        return wb;
    }

    public static void addHead(Sheet sheet, String[] columnNames) {
        //添加表头
        Row row = sheet.createRow(0);
        int columnIndex = 0;
        for (String columnName : columnNames) {
            row.createCell(columnIndex++).setCellValue(columnName);
        }
    }
    
    /**
     *  系统类型：0-Others, 1-iOS, 2-Android 
     * osType
     * */
    public static String findOsType(Integer osType) {
        if(osType == 1){
        	return "iOS";
        }else if (osType == 2) {
        	return "Android";
		}else {
			return "-";
		}
    }
}
