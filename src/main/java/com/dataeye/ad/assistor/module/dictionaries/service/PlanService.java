package com.dataeye.ad.assistor.module.dictionaries.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.dictionaries.mapper.PlanMapper;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import com.dataeye.ad.assistor.module.dictionaries.model.PlanVo;

@Service("planService")
public class PlanService {
	
	@Autowired
	private PlanMapper planMapper;
	
	/**
	 * 根据公司ID查询所有投放计划列表
	 * @param companyId
	 * @return
	 */
	public List<Plan> queryByCompany(Integer companyId) {
		return query(companyId, null, null);
	}
	
	/**
	 * 根据计划ID查询所有投放计划列表
	 * @param companyId
	 * @return
	 */
	public Plan get(Integer planId) {
		List<Plan> planList = query(null, planId, null);
		if(planList != null && !planList.isEmpty()) {
			return planList.get(0);
		}
		return null;
	}
	
	/**
	 * 查询投放计划列表
	 * @param companyId
	 * @param planId
	 * @param planName
	 * @param permissionMediumAccountIds
	 * @return
	 */
	public List<Plan> query(Integer companyId, Integer planId, Integer[] permissionMediumAccountIds) {
		PlanVo plan = new PlanVo();
		plan.setCompanyId(companyId);
		plan.setPlanId(planId);
		plan.setPermissionMediumAccountIds(permissionMediumAccountIds);
		return planMapper.query(plan);
	}

	/**
	 * 查询投放计划列表
	 * @param query
	 * @return
	 */
	public List<Plan> query(PlanVo query){
		return planMapper.query(query);
	}
	
	/**
	 * 更新投放计划系统类型
	 * @param planId
	 * @param osType
	 * @return
	 */
	public int modifyOsType(int planId, int osType) {
		Plan plan = new Plan();
		plan.setPlanId(planId);
		plan.setOsType(osType);
		plan.setUpdateTime(new Date());
		return planMapper.updateOsType(plan);
	}
	
	/**
	 * 根据公司ID查询所有投放计划列表
	 * @param companyId
	 * @return
	 */
	public void batchInsert(List<Plan> planList) {
		 planMapper.batchInsert(planList);
	}
	
	/**
	 * 根据公司ID查询所有投放计划列表
	 * @param companyId
	 * @param mediumId
	 * @param mediumAccountId
	 * @param mPlanId
	 * @return
	 */
	public Plan getPlanBymPlanId(int companyId, int mediumId, int mediumAccountId, String mPlanId) {
		Plan plan = new Plan();
		plan.setCompanyId(companyId);
		plan.setMediumId(mediumId);
		plan.setMediumAccountId(mediumAccountId);
		plan.setmPlanId(mPlanId);
		return planMapper.getPlanBymPlanId(plan);
	}
	
	/**
	 * 更新投放计划的计划名称
	 * @param planName
	 * @param planId
	 * @return
	 */
	public int modifyPlanName(String planName, Integer planId) {
		Plan plan = new Plan();
		plan.setPlanName(planName);
		plan.setPlanId(planId);
		plan.setUpdateTime(new Date());
		return planMapper.updatePlanName(plan);
	}
}
