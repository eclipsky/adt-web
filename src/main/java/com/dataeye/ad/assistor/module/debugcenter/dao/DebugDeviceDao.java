package com.dataeye.ad.assistor.module.debugcenter.dao;

import com.dataeye.ad.assistor.module.debugcenter.model.ChannelDebugDevice;
import com.dataeye.ad.assistor.module.debugcenter.model.ChannelInfoDomain;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;
import java.util.Map;

/**
 * @author lingliqi
 * @date 2018-01-18 14:17
 */
@MapperScan
public interface DebugDeviceDao {
    Map<String, Object> listOneCampaignById(@Param("appid") String appid, @Param("campaign") String campaign,
                                            @Param("tableName") String tableName);

    ChannelInfoDomain getChannelInfoByChannelId(@Param("channelId") int channelId);

    List<ChannelDebugDevice> queryChannelDebugDevice(@Param("uid") int uid);

    Integer addChannelDebugDevice(@Param("deviceName") String deviceName, @Param("deviceId") String deviceId, @Param("uid") int uid);

    Integer deleteChannelDebugDevice(@Param("deviceId") String deviceId, @Param("uid") int uid);
}
