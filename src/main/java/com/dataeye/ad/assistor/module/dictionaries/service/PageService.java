package com.dataeye.ad.assistor.module.dictionaries.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.module.dictionaries.mapper.PageMapper;
import com.dataeye.ad.assistor.module.dictionaries.model.Page;

@Service("pageService")
public class PageService {
	
	@Autowired
	private PageMapper pageMapper;
	
	/**
	 * 根据公司ID查询所有落地页列表
	 * @param companyId
	 * @return
	 */
	public List<Page> queryByCompany(Integer companyId,String pageName) {
		return query(companyId, null,pageName);
	}
	
	/**
	 * 根据页面ID查询单个落地页
	 * @param pageId
	 * @return
	 */
	public Page get(Integer pageId) {
		List<Page> pageList = query(null, pageId,null);
		if(pageList != null && !pageList.isEmpty()) {
			return pageList.get(0);
		}
		return null;
	}
	
	/**
	 * 查询落地页列表
	 * @param companyId
	 * @param pageId
	 * @param pageName
	 * @return
	 */
	public List<Page> query(Integer companyId, Integer pageId,String pageName) {
		Page page = new Page();
		page.setCompanyId(companyId);
		page.setPageId(pageId);
		page.setPageName(pageName);
		return pageMapper.query(page);
	}
}
