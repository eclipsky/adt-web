package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.constant.AccountRole;
import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.module.report.constant.Indicators;
import com.dataeye.ad.assistor.module.report.mapper.UpsAndDownsPreferenceMapper;
import com.dataeye.ad.assistor.module.report.model.IndicatorPreferenceQuery;
import com.dataeye.ad.assistor.module.report.model.UpsAndDownsPreference;
import com.dataeye.ad.assistor.module.report.service.IndicatorServiceImpl;
import com.dataeye.ad.assistor.util.IndicatorUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by huangzehai on 2017/7/6.
 */
@Service
public class UpsAndDownsPreferenceServiceImpl implements UpsAndDownsPreferenceService {
    private Logger logger = LoggerFactory.getLogger(UpsAndDownsPreferenceServiceImpl.class);

    /**
     * 开发工程师
     */
    private static final int DEVELOPER = 3;

    /**
     * 操作成功
     */
    private static final int SUCCESS = 1;

    /**
     * 不可见.
     */
    private static final int INVISIBLE = 0;

    @Autowired
    private UpsAndDownsPreferenceMapper upsAndDownsPreferenceMapper;

    @Override
    public UpsAndDownsPreference getUpsAndDownsPreference(IndicatorPreferenceQuery query) {
        logger.info("Get ups and downs preference for query {}", query);
        UpsAndDownsPreference preference = upsAndDownsPreferenceMapper.getUpsAndDownsPreference(query.getAccountId());
        if (preference == null && query.getAccountRole() == AccountRole.BUSINESS_ACCOUNT && !query.isInitialized()) {
            logger.info("Initialize update ups and downs preference for account {}", query.getAccountId());
            String indicatorPermission = StringUtils.join(Indicators.BUSINESS_ACCOUNT_INDICATORS, Constant.Separator.COMMA);
            query.setInitialized(true);
            //创建涨跌幅偏好
            this.addDefaultUpsAndDownsPreference(query.getCompanyId(), query.getAccountId(), query.getAccountRole(), indicatorPermission);
            //再次查询涨跌偏好
            preference = this.getUpsAndDownsPreference(query);
        }
        return preference;
    }

    /**
     * 更新指标涨跌偏好
     *
     * @param upsAndDownsPreference
     * @return
     */
    @Override
    public int updateUpsAndDownsPreference(UpsAndDownsPreference upsAndDownsPreference) {
        return upsAndDownsPreferenceMapper.updateUpsAndDownsPreference(upsAndDownsPreference);
    }

    @Override
    public boolean hasUpsAndDownsPreference(int accountId) {
        return upsAndDownsPreferenceMapper.hasUpsAndDownsPreference(accountId);
    }

    @Override
    public int insertUpsAndDownsPreference(UpsAndDownsPreference upsAndDownsPreference) {
        return upsAndDownsPreferenceMapper.insertUpsAndDownsPreference(upsAndDownsPreference);
    }

    @Override
    public int deleteUpsAndDownsPreference(int accountId) {
        return upsAndDownsPreferenceMapper.deleteUpsAndDownsPreference(accountId);
    }

    @Override
    public int addDefaultUpsAndDownsPreference(int companyId, int accountId, int accountRole, String indicatorPermission) {
        //如果是开发工程师，不添加指标偏好
        if (accountRole == DEVELOPER) {
            return SUCCESS;
        }

        UpsAndDownsPreference preference = new UpsAndDownsPreference();
        preference.setCompanyId(companyId);
        preference.setAccountId(accountId);
        preference.setMinutes(10);
        preference.setIndicatorVisibility(generateDefaultIndicatorVisibility(indicatorPermission, Indicators.UPS_AND_DOWNS_INDICATORS));
        return upsAndDownsPreferenceMapper.insertUpsAndDownsPreference(preference);
    }

    /**
     * 由于指标权限发生改变，更新指标涨跌显示偏好
     *
     * @param companyId
     * @param accountId
     * @param accountRole
     * @param indicatorPermission
     */
    @Override
    public void updateUpsAndDownsPreferenceAsPermissionChanged(int companyId, int accountId, int accountRole, String indicatorPermission) {
        if (accountRole == DEVELOPER) {
            //账号角色由优化师或代理转为开发工程师,删除该账号的指标偏好
            upsAndDownsPreferenceMapper.deleteUpsAndDownsPreference(accountId);
        } else {
            if (upsAndDownsPreferenceMapper.hasUpsAndDownsPreference(accountId)) {
                UpsAndDownsPreference preference = upsAndDownsPreferenceMapper.getUpsAndDownsPreference(accountId);
                preference.setIndicatorVisibility(updateIndicatorVisibility(indicatorPermission, preference.getIndicatorVisibility(), Indicators.UPS_AND_DOWNS_INDICATORS));
                upsAndDownsPreferenceMapper.updateUpsAndDownsPreference(preference);
            } else {
                //原来是开发工程师，现在变成了优化师或者代理
                this.addDefaultUpsAndDownsPreference(companyId, accountId, accountRole, indicatorPermission);
            }
        }
    }

    /**
     * 更新涨跌指标可见性.
     *
     * @param indicatorPermission
     * @param oldIndicatorPreference
     * @param indicators
     * @return
     */
    private String updateIndicatorVisibility(String indicatorPermission, String oldIndicatorPreference, List<String> indicators) {
        List<String> authorizedIndicators = getAuthorizedIndicators(indicatorPermission, indicators);
        StringBuilder indicatorBuilder = new StringBuilder();
        Map<String, Integer> oldPrefs = IndicatorUtils.parseIndicatorPreference(oldIndicatorPreference);
        int index = 0;
        for (String indicator : authorizedIndicators) {
            indicatorBuilder.append(indicator);
            indicatorBuilder.append(Constant.Separator.COLON);
            //对已授权的指标，使用用户的偏好设定
            if (oldPrefs.containsKey(indicator)) {
                indicatorBuilder.append(oldPrefs.get(indicator));
            } else {
                //对新增的授权指标，使用默认的偏好设定——不可见
                indicatorBuilder.append(INVISIBLE);
            }
            if (index < authorizedIndicators.size() - 1) {
                indicatorBuilder.append(Constant.Separator.COMMA);
            }
            index++;
        }
        return indicatorBuilder.toString();
    }

    /**
     * 生成默认指标可见性.
     *
     * @param indicatorPermission
     * @param indicators
     * @return
     */
    private String generateDefaultIndicatorVisibility(String indicatorPermission, List<String> indicators) {
        List<String> authorizedIndicators = getAuthorizedIndicators(indicatorPermission, indicators);
        StringBuilder indicatorBuilder = new StringBuilder();
        int index = 0;
        for (String indicator : authorizedIndicators) {
            indicatorBuilder.append(indicator);
            indicatorBuilder.append(Constant.Separator.COLON);
            indicatorBuilder.append(INVISIBLE);
            if (index < authorizedIndicators.size() - 1) {
                indicatorBuilder.append(Constant.Separator.COMMA);
            }
            index++;
        }
        return indicatorBuilder.toString();
    }

    /**
     * 获取授权访问的指标列表
     *
     * @param indicatorPermission
     * @param indicators
     * @return
     */
    private List<String> getAuthorizedIndicators(String indicatorPermission, List<String> indicators) {
        List<String> authorizedIndicators = new ArrayList<>();
        if (StringUtils.isNotBlank(indicatorPermission)) {
            for (String indicator : indicators) {
                if (StringUtils.contains(indicatorPermission, indicator)) {
                    authorizedIndicators.add(indicator);
                }
            }
        }
        return authorizedIndicators;
    }
}
