package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.dictionaries.mapper.PlanMapper;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.constant.OpCode;
import com.dataeye.ad.assistor.module.realtimedelivery.model.DeliveryStatus;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationLog;
import com.dataeye.ad.assistor.module.realtimedelivery.model.OperationStatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by huangzehai on 2017/5/15.
 */
@Service("planSwitchService")
public class PlanSwitchServiceImpl implements PlanSwitchService {

    /**
     * 不支持消息.
     */
    private static final String NOT_SUPPORT = "不支持该媒体的更新计划开关操作";

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private PlanMapper planMapper;

    @Autowired
    private OperationLogService operationLogService;

    @Override
    public Result updateStatus(DeliveryStatus status) {
        //获取相应媒体的出价服务。
        PlanSwitchService service = (PlanSwitchService) applicationContext.getBean(PlanSwitchServices.getPlanSwitchService(status.getMediumId()));
        if (service == null) {
            //不支持该媒体的出价操作.
            Result result = new Result();
            result.setSuccess(false);
            result.setMessage(NOT_SUPPORT);
            return result;
        } else {
            //获取计划改动前的状态
            Plan oldPlan = planMapper.getPlanById(status.getPlanId());

            //切换媒体的计划开关
            Result result = service.updateStatus(status);
            if (result.isSuccess()) {
                //切换ADT的计划开关
                updatePlanSwitch(status);
            }

            //添加操作日志
            logging(status, oldPlan, result);
            return result;
        }
    }

    /**
     *  添加操作日志
     * @param status
     * @param oldPlan
     * @param result
     */
    private void logging(DeliveryStatus status, Plan oldPlan, Result result) {
        OperationLog log = new OperationLog();
        log.setCompanyId(status.getCompanyId());
        log.setAccountId(status.getAccountId());
        log.setPlanId(status.getPlanId());
        log.setOpCode(OpCode.PLAN_SWITCH);
        log.setPreviousValue(getStatusLabel(OperationStatus.parse(oldPlan.getPlanSwitch())));
        log.setCurrentValue(getStatusLabel(status.getStatus()));
        log.setResult(result.isSuccess() ? 1 : 0);
        log.setOpTime(new Date());
        String msg = "实时操盘：";
        if (!result.isSuccess()) {
        	msg += result.getMessage();
        }
        log.setMessage(msg);
        operationLogService.addOperationLog(log);
    }

    /**
     * @param status
     * @return
     */
    private String getStatusLabel(OperationStatus status) {
        if (status == OperationStatus.Enable) {
            return "开";
        } else {
            return "关";
        }
    }

    /**
     * 切换ADT计划开关.
     *
     * @param status
     * @return
     */
    private int updatePlanSwitch(DeliveryStatus status) {
        Plan plan = new Plan();
        plan.setPlanId(status.getPlanId());
        plan.setPlanSwitch(status.getStatus().getValue());
        return planMapper.updatePlanSwitch(plan);
    }

}
