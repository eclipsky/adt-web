package com.dataeye.ad.assistor.module.report.controller;

import java.text.ParseException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.Parameters;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.report.constant.Indicators;
import com.dataeye.ad.assistor.module.report.model.DeliveryKeyIndicator;
import com.dataeye.ad.assistor.module.report.model.DeliveryKeyIndicatorQuery;
import com.dataeye.ad.assistor.module.report.service.DeliveryKeyIndicatorService;
import com.dataeye.ad.assistor.module.systemaccount.model.SystemAccountVo;
import com.dataeye.ad.assistor.module.systemaccount.service.SystemAccountService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;

/**
 * 投放关键指标控制器
 * Created by huangzehai on 2017/5/25.
 */
@Controller
public class DeliveryKeyIndicatorController {
    @Autowired
    private DeliveryKeyIndicatorService deliveryKeyIndicatorService;

    @Autowired
    private SystemAccountService systemAccountService;

    private DeliveryKeyIndicatorQuery getDeliveryKeyIndicatorQuery(HttpServletRequest request) throws ParseException {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
//        if (!userInSession.hasManagerPermission()) {
//            return null;
//        }
        DEParameter parameter = context.getDeParameter();
        //校验起始日期是否为空
        if (StringUtils.isBlank(parameter.getStartDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }

        //校验截止日期是否为空
        if (StringUtils.isBlank(parameter.getEndDate())) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }
        //转化起始截止日期
        Date startDate = DateUtils.parse(parameter.getStartDate());
        Date endDate = DateUtils.parse(parameter.getEndDate());
        String mediumId = parameter.getParameter(DEParameter.Keys.MEDIUM_ID);
        String productId = parameter.getParameter(DEParameter.Keys.PRODUCT_ID);
        String filterNaturalFlow = parameter.getParameter(DEParameter.Keys.FILTER_NATURAL_FLOW);

        DeliveryKeyIndicatorQuery query = new DeliveryKeyIndicatorQuery();
        query.setCompanyId(userInSession.getCompanyId());
        query.setOptimizerPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());
        //自然流量权限控制
        query.setPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());
        query.setPermissionSystemAccounts(userInSession.getPermissionSystemAccounts());
        query.setStartDate(startDate);
        query.setEndDate(endDate);

        //productId为-1是表示其他产品.
        if (StringUtils.isNotBlank(productId) && NumberUtils.isNumber(productId)) {
            query.setProductId(Integer.valueOf(productId));
        }

        if (StringUtils.isNotBlank(mediumId) && NumberUtils.isDigits(mediumId)) {
            query.setMediumId(Integer.valueOf(mediumId));
        }

        query.setFilterNaturalFlow(Boolean.valueOf(filterNaturalFlow));
        return query;
    }


    private void validateIndicatorAuthentication(HttpServletRequest request, String indicatorId) {
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        SystemAccountVo systemAccount = systemAccountService.get(userInSession.getAccountId());
        if (!StringUtils.containsIgnoreCase(systemAccount.getIndicatorPermission(), indicatorId)) {
            ExceptionHandler.throwGeneralServerException(StatusCode.LOGI_NO_PRIVILEGE);
        }
    }

    /**
     * 投放关键指标汇总数据
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/total.do")
    public Object total(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        DeliveryKeyIndicator keyIndicator = new DeliveryKeyIndicator();
        if(query.getOptimizerPermissionMediumAccountIds() == null){
        	keyIndicator.setCost(Constant.Separator.BAR);
        	keyIndicator.setDownloads(Constant.Separator.BAR);
        	keyIndicator.setActivations(Constant.Separator.BAR);
        	keyIndicator.setRegistrations(Constant.Separator.BAR);
        	keyIndicator.setDau(Constant.Separator.BAR);
        }else{
        	keyIndicator = this.deliveryKeyIndicatorService.getDeliveryKeyIndicatorTotal(query);
        }
        //检查指标权限
        DEContext context = (DEContext) request.getAttribute(Parameters.CONTEXT);
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        SystemAccountVo systemAccount = systemAccountService.get(userInSession.getAccountId());
        //隐藏未授权指标
        if (!StringUtils.containsIgnoreCase(systemAccount.getIndicatorPermission(), Indicators.COST)) {
            keyIndicator.setCost(null);
        }
        if (!StringUtils.containsIgnoreCase(systemAccount.getIndicatorPermission(), Indicators.DOWNLOAD_TIMES)) {
            keyIndicator.setDownloads(null);
        }
        if (!StringUtils.containsIgnoreCase(systemAccount.getIndicatorPermission(), Indicators.ACTIVE_NUM)) {
            keyIndicator.setActivations(null);
        }
        if (!StringUtils.containsIgnoreCase(systemAccount.getIndicatorPermission(), Indicators.REGISTER_NUM)) {
            keyIndicator.setRegistrations(null);
        }
        if (!StringUtils.containsIgnoreCase(systemAccount.getIndicatorPermission(), Indicators.DAU)) {
            keyIndicator.setDau(null);
        }
        return keyIndicator;
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/cost/product.do")
    public Object costGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateIndicatorAuthentication(request, Indicators.COST);
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        return this.deliveryKeyIndicatorService.getCostGroupByProduct(query);
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/cost/medium.do")
    public Object costGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateIndicatorAuthentication(request, Indicators.COST);
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        return this.deliveryKeyIndicatorService.getCostGroupByMedium(query);
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/download/product.do")
    public Object downloadsGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateIndicatorAuthentication(request, Indicators.DOWNLOAD_TIMES);
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        return this.deliveryKeyIndicatorService.getDownloadsGroupByProduct(query);
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/download/medium.do")
    public Object downloadsGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateIndicatorAuthentication(request, Indicators.DOWNLOAD_TIMES);
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        return this.deliveryKeyIndicatorService.getDownloadsGroupByMedium(query);
    }


    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/activation/product.do")
    public Object activationsGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateIndicatorAuthentication(request, Indicators.ACTIVE_NUM);
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        return this.deliveryKeyIndicatorService.getActivationsGroupByProduct(query);
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/activation/medium.do")
    public Object activationsGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateIndicatorAuthentication(request, Indicators.ACTIVE_NUM);
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        return this.deliveryKeyIndicatorService.getActivationsGroupByMedium(query);
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/registration/product.do")
    public Object registrationsGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateIndicatorAuthentication(request, Indicators.REGISTER_NUM);
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        return this.deliveryKeyIndicatorService.getRegistrationsGroupByProduct(query);
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/registration/medium.do")
    public Object registrationsGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateIndicatorAuthentication(request, Indicators.REGISTER_NUM);
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        return this.deliveryKeyIndicatorService.getRegistrationsGroupByMedium(query);
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/dau/product.do")
    public Object dauGroupByProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateIndicatorAuthentication(request, Indicators.DAU);
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        return this.deliveryKeyIndicatorService.getDauGroupByProduct(query);
    }

    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/delivery/key-indicator/dau/medium.do")
    public Object dauGroupByMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
        validateIndicatorAuthentication(request, Indicators.DAU);
        DeliveryKeyIndicatorQuery query = getDeliveryKeyIndicatorQuery(request);
        if (query == null) return null;
        return this.deliveryKeyIndicatorService.getDauGroupByMedium(query);
    }

}
