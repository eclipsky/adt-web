package com.dataeye.ad.assistor.module.advertisement.controller;


import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.common.PageData;
import com.dataeye.ad.assistor.common.PageDataHelper;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ConfiguredStatus;
import com.dataeye.ad.assistor.module.advertisement.constants.Advertisement.ProductType;
import com.dataeye.ad.assistor.module.advertisement.model.AdGroupQueryVo;
import com.dataeye.ad.assistor.module.advertisement.model.ApiTxAdGroupVo;
import com.dataeye.ad.assistor.module.advertisement.model.AdcreativeVO;
import com.dataeye.ad.assistor.module.advertisement.model.AdvertisementGroup;
import com.dataeye.ad.assistor.module.advertisement.model.AdvertisementGroupVO;
import com.dataeye.ad.assistor.module.advertisement.model.MarketingApiAuth;
import com.dataeye.ad.assistor.module.advertisement.model.TargetingVO;
import com.dataeye.ad.assistor.module.advertisement.model.tencent.TargetingBean;
import com.dataeye.ad.assistor.module.advertisement.service.ApiTxAdGroupService;
import com.dataeye.ad.assistor.module.advertisement.service.AdvertisementGroupService;
import com.dataeye.ad.assistor.module.advertisement.service.tencent.AdgroupService;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.StringUtil;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;


/**
 * Created by ldj
 * 广告投放
 * 广告组控制器.
 */
@Controller
public class AdvertisementGroupController {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(AdvertisementGroupController.class);

    @Autowired
    private ApiTxAdGroupService apiTxAdGroupService;
    @Autowired
    private AdvertisementGroupService advertisementGroupService;
    @Autowired
    private AdgroupService adgroupService;

    private static Integer ADD_TYPE = 1;
    private static Integer MODIFY_TYPE = 2;

    /**
     * 查询广告版位列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017-11-28
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amAdcreativeTemplate/query.do")
    public Object queryAdcreativeTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("查询广告版位列表");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String productType = parameter.getParameter("productType");
        if (StringUtils.isBlank(productType) || ProductType.parse(Integer.parseInt(productType)) == null) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
        }
        return apiTxAdGroupService.queryAdcreativeTemplate(ProductType.parse(Integer.parseInt(productType)).name());
    }


    /**
     * 查询广告组信息列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017-11-28
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amAdvertisementGroup/query.do")
    public Object query(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("查询投放广告组列表");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        AdGroupQueryVo vo = parseQuery(context);

        // 如果为null，则表示无数据访问权限，否则，有权限
        if (null == vo.getPermissionMediumAccountIds()) {
            return null;
        }
        DEParameter parameter = context.getDeParameter();

        List<AdvertisementGroupVO> result = advertisementGroupService.query(vo);
        PageData pageData = PageDataHelper.getPageDataByList(parameter.getPageSize(), parameter.getPageId(), result);
        List<AdvertisementGroupVO> voss = (List<AdvertisementGroupVO>)pageData.getContent();
        for (AdvertisementGroupVO sss : voss) {
        	 Integer mAccountId = null;
             String accessToken = null;
             MarketingApiAuth tAccountVo = apiTxAdGroupService.getAccessTokey(sss.getMediumAccountId());
             if (tAccountVo != null) {
                 mAccountId = tAccountVo.getAccountId();
                 accessToken = tAccountVo.getAccessToken();
             }
        	Integer adgroup_id = sss.getAdGroup().getAdGroupId();
            AdvertisementGroup adGroup = sss.getAdGroup();
            if(adgroup_id != null && adgroup_id != 0){
            	List<AdvertisementGroup> adGroupResponse = adgroupService.query(mAccountId, adgroup_id, null, accessToken, null);
            	if (adGroupResponse != null && adGroupResponse.size() > 0) {
            		AdvertisementGroup adVo = adGroupResponse.get(0);
            		adGroup.setAdGroupName(adVo.getAdGroupName());
            		adGroup.setSiteSet(adVo.getSiteSet());
            		adGroup.setCostType(adVo.getCostType());
            		adGroup.setBid(adVo.getBid());
            		adGroup.setProductRefsId(adVo.getProductRefsId());
            		adGroup.setStatus(adVo.getStatus());
            		adGroup.setSystemStatus(adVo.getSystemStatus());
            		adGroup.setTargetingId(adVo.getTargetingId());
            		adGroup.setRejectMessage(adVo.getRejectMessage());
            		adGroup.setBeginDate(adVo.getBeginDate());
            		adGroup.setEndDate(adVo.getEndDate());
            		adGroup.setTimeSeries(adVo.getTimeSeries());
            	}
            }
		}
        return pageData;
    }

    /**
     * 创建广告组
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017-11-28
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amAdvertisementGroup/addAdGroups.do")
    public Object addAdGroups(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("创建广告组");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        AdvertisementGroup vo = parseAdGroup(context, ADD_TYPE);
        ApiTxAdGroupVo relationVo = parseAdRelationVo(context);
        //创意
        List<AdcreativeVO> adcreativeVoList = parseAdcreative(context);
        DEParameter parameter = context.getDeParameter();
        String targetingId = parameter.getParameter("targetingId");
        TargetingVO targetingVo = null;
        if (StringUtils.isNotBlank(targetingId) && NumberUtils.isNumber(targetingId) && Integer.parseInt(targetingId) != 0) {
            vo.setTargetingId(Integer.parseInt(targetingId));
        } else {
            targetingVo = parseTargeting(context);
        }
        return advertisementGroupService.addAdGroup(vo, relationVo, adcreativeVoList, targetingVo);
    }

    /**
     * 编辑广告组
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017-12-01
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amAdvertisementGroup/modifyAdGroups.do")
    public Object modifyAdGroups(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("编辑广告");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        AdvertisementGroup adVo = parseAdGroup(context, MODIFY_TYPE);
        ApiTxAdGroupVo relationVo = parseAdRelationVo(context);
        //创意
        List<AdcreativeVO> adcreativeVoList = parseAdcreative(context);
        DEParameter parameter = context.getDeParameter();
        String targetingId = parameter.getParameter("targetingId");
        TargetingVO targetingVo = null;
        if (StringUtils.isNotBlank(targetingId) && NumberUtils.isNumber(targetingId) && Integer.parseInt(targetingId) != 0) {
            adVo.setTargetingId(Integer.parseInt(targetingId));
        }else{
        	targetingVo = parseTargeting(context);
        	String targetingNotPkgId = parameter.getParameter("targetingNotPkgId");
        	if (StringUtils.isNotBlank(targetingNotPkgId) && NumberUtils.isNumber(targetingNotPkgId) && Integer.parseInt(targetingNotPkgId) != 0) {
        		targetingVo.setTargetingId(Integer.parseInt(targetingNotPkgId));
        	}
        }
        return advertisementGroupService.modify(adVo, relationVo, adcreativeVoList, targetingVo);
    }

    /**
     * 下拉框查询广告组信息列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017-11-29
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amAdvertisementGroup/queryForSelect.do")
    public Object queryForSelect(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("下拉框查询投放广告组列表");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        AdGroupQueryVo vo = parseQuerySelect(context);

        // 如果为null，则表示无数据访问权限，否则，有权限
        if (null == vo.getPermissionMediumAccountIds()) {
            return null;
        }
        return advertisementGroupService.queryadGroupForSelect(vo);
    }


    /**
     * 删除广告列表
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017/11/30
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amAdvertisementGroup/deleteAdGroups.do")
    public Object delete(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("删除广告列表(投放业务)");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String adGroupId = parameter.getParameter("adGroupId");
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        //校验参数
        if (StringUtils.isBlank(adGroupId)) {
            logger.error("投放广告组ID不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_ID_IS_NULL);
        }
        if (StringUtils.isBlank(mediumAccountId)) {
            logger.error("投放媒体帐号ID不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        return advertisementGroupService.delete(Integer.parseInt(adGroupId), Integer.parseInt(mediumAccountId));
    }

    /**
     * 编辑广告组开关和出价
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     * @author ldj 2017-12-01
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amAdvertisementGroup/modifyAdGroupsBid.do")
    public Object modifyAdGroupsBid(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("编辑广告组开关和出价");
        DEContext context = DEContextContainer.getContext(request);
        // 1. 参数解析与校验
        //AdvertisementGroup adVo = parseAdGroup(context,MODIFY_TYPE);

        AdvertisementGroup adVo = new AdvertisementGroup();
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        String mediumId = parameter.getParameter("mediumId");
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        String adGroupId = parameter.getParameter("adGroupId");
        String adGroupName = parameter.getParameter("adGroupName");
        String bid = parameter.getParameter("bid");
        String status = parameter.getParameter("status");  //0-正常，1-失效
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        if (StringUtils.isBlank(adGroupId)) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_ID_IS_NULL);
        }
        if (StringUtils.isBlank(bid) || !NumberUtils.isNumber(bid)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_BID_MISSING);
        }
        if (StringUtils.isBlank(adGroupName)) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_NAME_IS_NULL);
        }
        if (StringUtils.isNotBlank(status) && ConfiguredStatus.parse(Integer.parseInt(status)) != null) {
            adVo.setStatus(Integer.parseInt(status));
        }

        adVo.setAdGroupId(Integer.parseInt(adGroupId));
        adVo.setBid(new BigDecimal(bid));
        adVo.setAdGroupName(adGroupName);
        
        return advertisementGroupService.modifyAdGroupsInfo(adVo, Integer.parseInt(mediumAccountId));
    }


    /**
     * 广告组名称校验
     *
     * @param request
     * @param response
     * @return Boolean True - 校验通过；False - 校验失败
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/amAdvertisementGroup/checkName.do")
    public Object checkAdGroupName(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("广告组名称校验");
        DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        String adGroupName = parameter.getParameter("adGroupName");
        String adGroupIdStr = parameter.getParameter("adGroupId");
        Integer adGroupId = null;
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_MEDIUM_ACCOUNT_IS_NULL);
        }
        if (StringUtils.isBlank(adGroupName)) {
            ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_NAME_IS_NULL);
        }
        if (StringUtils.isNotBlank(adGroupIdStr)) {
            adGroupId = Integer.parseInt(adGroupIdStr);
        }
        return apiTxAdGroupService.checkAdGroupName(Integer.parseInt(mediumAccountId), adGroupName, adGroupId);
    }

    /**
     * 解析请求参数
     *
     * @param context
     * @return
     * @throws ParseException
     * @author ldj 2017/11/28
     */
    public AdGroupQueryVo parseQuery(DEContext context) throws ParseException {
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();

        //构建推广计划查询条件
        AdGroupQueryVo queryVo = new AdGroupQueryVo();


        queryVo.setProductIds(StringUtil.stringToList(parameter.getParameter("productIds")));
        queryVo.setMediumAccountIds(StringUtil.stringToList(parameter.getParameter("mediumAccountIds")));
        queryVo.setMediumIds(StringUtil.stringToList(parameter.getParameter("mediumIds")));
        queryVo.setAdGroupIds(StringUtil.stringToList(parameter.getParameter("adGroupIds")));

        String planGroupId = parameter.getParameter("planGroupId");
        if (StringUtils.isNotBlank(planGroupId)) {
            queryVo.setPlanGroupId(Integer.parseInt(planGroupId));
        } else {
            //校验起始日期是否为空
            if (StringUtils.isBlank(parameter.getStartDate())) {
                ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
            }

            //校验截止日期是否为空
            if (StringUtils.isBlank(parameter.getEndDate())) {
                ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
            }

            //转化起始截止日期
            Date startDate = DateUtils.parse(parameter.getStartDate());
            Date endDate = DateUtils.parse(parameter.getEndDate());
            queryVo.setStartDate(startDate);
            queryVo.setEndDate(endDate);
        }

        // 解析媒体账号权限
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();

        queryVo.setCompanyId(companyId);
        queryVo.setPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());

        //解析系统帐号权限
        Integer[] permissionSystemAccountIds = userInSession.getPermissionSystemAccountIds();
        if (permissionSystemAccountIds != null && permissionSystemAccountIds.length > 0) {
            queryVo.setPermissionSystemAccountIds(permissionSystemAccountIds);
        }
        return queryVo;
    }


    /**
     * 解析创建推广计划参数
     *
     * @param context
     * @return
     * @throws ParseException
     * @author ldj 2017/11/28
     */
    public ApiTxAdGroupVo parseAdRelationVo(DEContext context) throws ParseException {
        ApiTxAdGroupVo vo = new ApiTxAdGroupVo();
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        String planGroupId = parameter.getParameter("planGroupId");
        String mediumId = parameter.getParameter("mediumId");
        String productId = parameter.getParameter("productId");
        String productType = parameter.getParameter("productType");
        String mediumAccountId = parameter.getParameter("mediumAccountId");
        if (StringUtils.isBlank(planGroupId)) {
            ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_ID_IS_NULL);
        }
        if (StringUtils.isBlank(mediumId)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_MEDIUM_ID_MISSING);
        }
        if (StringUtils.isBlank(mediumAccountId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_MEDIUM_ACCOUNT_ID_IS_NULL);
        }
        if (StringUtils.isBlank(productId)) {
            ExceptionHandler.throwParameterException(StatusCode.MEAC_DISCOUNT_PRODUCT_ID_IS_NULL);
        }
        if (StringUtils.isBlank(productType) || ProductType.parse(Integer.parseInt(productType)) == null) {
            ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
        }
        vo.setPlanGroupId(Integer.parseInt(planGroupId));
        vo.setProductType(Integer.parseInt(productType));
        vo.setMediumId(Integer.parseInt(mediumId));
        vo.setMediumAccountId(Integer.parseInt(mediumAccountId));
        vo.setProductId(Integer.parseInt(productId));
        return vo;
    }

    /**
     * 解析创建广告组参数
     *
     * @param context
     * @param type    (1是添加，2是编辑)
     * @return
     * @throws ParseException
     * @author ldj 2017/11/28
     */
    public AdvertisementGroup parseAdGroup(DEContext context, Integer type) throws ParseException {
        AdvertisementGroup vo = new AdvertisementGroup();
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        int accountId = userInSession.getAccountId();
        vo.setCompanyId(companyId);
        vo.setSystemAccountId(accountId);
        String adGroupName = parameter.getParameter("adGroupName");
        String beginDate = parameter.getParameter("beginDate");
        String endDate = parameter.getParameter("endDate");
        String timeSeries = parameter.getParameter("timeSeries");
        String bid = parameter.getParameter("bid");
        if (StringUtils.isBlank(beginDate)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_START_DATE_BLANK);
        }
        if (StringUtils.isBlank(endDate)) {
            ExceptionHandler.throwParameterException(StatusCode.COMM_END_DATE_BLANK);
        }
        if (StringUtils.isBlank(bid) || !NumberUtils.isNumber(bid)) {
            ExceptionHandler.throwParameterException(StatusCode.REAL_BID_MISSING);
        }
        if (StringUtils.isNotBlank(timeSeries)) {
            vo.setTimeSeries(timeSeries);
        }

        if (type == ADD_TYPE) {  //解析添加广告的相关参数
            String productRefsId = parameter.getParameter("productRefsId");
            String costType = parameter.getParameter("costType");
            String productType = parameter.getParameter("productType");
            if (StringUtils.isBlank(adGroupName)) {
                ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_NAME_IS_NULL);
            }
            if (StringUtils.isBlank(productRefsId)) {
                ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_PRODUCT_REFS_ID_IS_NULL);
            }
            if (StringUtils.isBlank(costType)) {
                ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_COST_TYPE_IS_NULL);
            }
            if (StringUtils.isBlank(productType) || ProductType.parse(Integer.parseInt(productType)) == null) {
                ExceptionHandler.throwParameterException(StatusCode.CAMP_OS_TYPE_IS_ERROR);
            }
            vo.setProductRefsId(productRefsId);
            vo.setCostType(Integer.parseInt(costType));
            vo.setProductType(Integer.parseInt(productType));

        } else if (type == MODIFY_TYPE) {  //解析编辑广告的相关参数
            String planGroupId = parameter.getParameter("planGroupId");
            String adGroupId = parameter.getParameter("adGroupId");
            String siteSet = parameter.getParameter("siteSet");
            String adcreativeTemplateId = parameter.getParameter("adcreativeTemplateId");
            if (StringUtils.isBlank(planGroupId)) {
                ExceptionHandler.throwParameterException(StatusCode.ADPLAN_GROUP_ID_IS_NULL);
            }
            if (StringUtils.isBlank(adGroupId)) {
                ExceptionHandler.throwParameterException(StatusCode.ADVERTISING_GROUP_ID_IS_NULL);
            }
            if (StringUtils.isBlank(siteSet)) {
                ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_SITESET_IS_NULL);
            }
            if (StringUtils.isBlank(adcreativeTemplateId)) {
                ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_TEMPLATE_ID_IS_NULL);
            }
            vo.setAdGroupId(Integer.parseInt(adGroupId));
            vo.setSiteSet(siteSet);
        }
        vo.setAdGroupName(adGroupName.trim());
        vo.setBeginDate(beginDate);
        vo.setEndDate(endDate);
        vo.setBid(new BigDecimal(bid));

        return vo;
    }

    /**
     * 解析创建广告创意参数
     *
     * @param context
     * @return
     * @throws ParseException
     * @author ldj 2017/11/29
     */
    public List<AdcreativeVO> parseAdcreative(DEContext context) throws ParseException {
        List<AdcreativeVO> result = new ArrayList<>();
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();

        String adcreativeTemplateId = parameter.getParameter("adcreativeTemplateId");
        String siteSet = parameter.getParameter("siteSet");
        //String destination_url = parameter.getParameter("destination_url");
        if (StringUtils.isBlank(adcreativeTemplateId)) {
            ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_TEMPLATE_ID_IS_NULL);
        }
        if (StringUtils.isBlank(siteSet)) {
            ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_SITESET_IS_NULL);
        }
        String adcreativeList = parameter.getParameter("adcreativeList");
        if (StringUtils.isBlank(adcreativeList)) {
            ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_ELEMENTS_IS_NULL);
        }
        JsonArray adcreativeArray = StringUtil.jsonParser.parse(adcreativeList).getAsJsonArray();
        if (adcreativeArray.size() <= 0) {
            ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_ELEMENTS_IS_NULL);
        }
        List<AdcreativeVO> response = StringUtil.gson.fromJson(adcreativeArray, new TypeToken<List<AdcreativeVO>>() {
        }.getType());
        for (AdcreativeVO vo : response) {
            if (vo.getAdcreativeElements() == null || vo.getAdcreativeElements().equals("")) {
                ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_ELEMENTS_IS_NULL);
            }
            if (StringUtils.isBlank(vo.getAdcreativeName())) {
                ExceptionHandler.throwParameterException(StatusCode.ADCREATIVE_NAME_ID_IS_NULL);
            }
            vo.setAdcreativeTemplateId(Integer.parseInt(adcreativeTemplateId));
            vo.setSiteSet(siteSet);
            //vo.setDestination_url(destination_url);
            result.add(vo);
        }
        return result;
    }


    /**
     * 解析请求参数
     *
     * @param context
     * @return
     * @throws ParseException
     * @author ldj 2017/11/29
     */
    public AdGroupQueryVo parseQuerySelect(DEContext context) throws ParseException {
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        //构建推广计划查询条件
        AdGroupQueryVo queryVo = new AdGroupQueryVo();

        queryVo.setProductIds(StringUtil.stringToList(parameter.getParameter("productIds")));
        queryVo.setMediumAccountIds(StringUtil.stringToList(parameter.getParameter("mediumAccountIds")));
        queryVo.setMediumIds(StringUtil.stringToList(parameter.getParameter("mediumIds")));
        queryVo.setAdGroupIds(StringUtil.stringToList(parameter.getParameter("adGroupIds")));

        // 解析媒体账号权限
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();

        queryVo.setCompanyId(companyId);
        queryVo.setPermissionMediumAccountIds(userInSession.getOptimizerPermissionMediumAccountIds());

        //解析系统帐号权限
        Integer[] permissionSystemAccountIds = userInSession.getPermissionSystemAccountIds();
        if (permissionSystemAccountIds != null && permissionSystemAccountIds.length > 0) {
            queryVo.setPermissionSystemAccountIds(permissionSystemAccountIds);
        }
        return queryVo;
    }

    /**
     * 解析请求参数
     *
     * @param context
     * @return
     * @throws ParseException
     * @author ldj 2017/12/20
     */
    public TargetingVO parseTargeting(DEContext context) throws ParseException {
        //参数解析与校验
        DEParameter parameter = context.getDeParameter();
        String targetingName = parameter.getParameter("targetingName");
        if (StringUtils.isBlank(targetingName)) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_NAME_IS_NULL);
        }
        TargetingBean targetingBean = null;
        try {
            targetingBean = StringUtil.gson.fromJson(parameter.getParameter("targeting"), TargetingBean.class);
        } catch (Exception e) {
            ExceptionHandler.throwParameterException(StatusCode.TARGETTAGS_TARGETING_POST_ERROR);
        }
        String description = parameter.getParameter("description");
        if (StringUtils.isBlank(description)) {
            description = null;
        }
        String isPublic = parameter.getParameter("isPublic");
        if (StringUtils.isBlank(isPublic)) {
            isPublic = "0";
        }

        TargetingVO targetingVO = new TargetingVO();
        targetingVO.setIsPublic(Integer.parseInt(isPublic));
        targetingVO.setTargetingName(targetingName);
        targetingVO.setTargeting(targetingBean);
        targetingVO.setDescription(description);
        return targetingVO;
    }
}
