package com.dataeye.ad.assistor.module.systemaccount.model;

import java.util.List;

import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccount;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumAccountGroupVo;
import com.google.gson.annotations.Expose;

/**
 * 系统账号列表页面参数Vo
 * 
 * @author luzhuyou 2017-02-16
 */
public class SystemAccountVo {
	/** 账号ID */
	@Expose
	private Integer accountId;
	/** 公司ID */
	@Expose
	private Integer companyId;
	/** 账号名称（邮箱，登录账号） */
	@Expose
	private String accountName;
	/** 账号别名 */
	@Expose
	private String accountAlias;
	/** 密码 */
	private String password;
	/** 状态：0-正常，1-失效，2-删除 */
	@Expose
	private Integer status;
	/** 账户角色:0-普通系统账户，1-组长，2-企业账户 */
	@Expose
	private Integer accountRole;
	/** 菜单权限：多个用逗号分隔 */
	@Expose
	private String menuPermission;
	@Expose
	private String indicatorPermission;
	/** 手机 */
	@Expose
	private String mobile;
	/** 分组ID */
	@Expose
	private Integer groupId;
	/** 分组名称 */
	@Expose
	private String groupName;
	/** 企业账号UID */
	@Expose
	private Integer companyAccountUid;
	/** 登录提示，需要提示次数 */
	private Integer loginPromptTimes;
	/** 登录提示，当前已提示次数 */
	private Integer alreadyPromptTimes;
	
	/**优化师ADT账号*/
	@Expose
	private String responsibleAccounts;
	/** 系统账号名称列表(至少一个系统账号，多个以逗号分隔) */
	@Expose
	private String followerAccounts;
	
	/***开发工程师可以查看的投放产品*/
	@Expose
	private String productIds;
	
	/**优化师ADT账号,根据媒体名称分组*/
	@Expose
	private List<MediumAccountGroupVo> responsibleAccountList;
	/** 系统账号名称列表(根据媒体名称分组) */
	@Expose
	private List<MediumAccountGroupVo> followerAccountList;
	
	/***开发工程师可以查看的投放产品*/
	@Expose
	private String productNames;
	
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountAlias() {
		return accountAlias;
	}
	public void setAccountAlias(String accountAlias) {
		this.accountAlias = accountAlias;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getAccountRole() {
		return accountRole;
	}
	public void setAccountRole(Integer accountRole) {
		this.accountRole = accountRole;
	}
	public String getMenuPermission() {
		return menuPermission;
	}
	public void setMenuPermission(String menuPermission) {
		this.menuPermission = menuPermission;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getIndicatorPermission() {
		return indicatorPermission;
	}

	public void setIndicatorPermission(String indicatorPermission) {
		this.indicatorPermission = indicatorPermission;
	}

	public Integer getCompanyAccountUid() {
		return companyAccountUid;
	}
	public void setCompanyAccountUid(Integer companyAccountUid) {
		this.companyAccountUid = companyAccountUid;
	}
	public String getResponsibleAccounts() {
		return responsibleAccounts;
	}
	public void setResponsibleAccounts(String responsibleAccounts) {
		this.responsibleAccounts = responsibleAccounts;
	}
	public String getFollowerAccounts() {
		return followerAccounts;
	}
	public void setFollowerAccounts(String followerAccounts) {
		this.followerAccounts = followerAccounts;
	}
	
	public String getProductIds() {
		return productIds;
	}
	public void setProductIds(String productIds) {
		this.productIds = productIds;
	}
	public List<MediumAccountGroupVo> getResponsibleAccountList() {
		return responsibleAccountList;
	}
	public void setResponsibleAccountList(List<MediumAccountGroupVo> responsibleAccountList) {
		this.responsibleAccountList = responsibleAccountList;
	}
	public List<MediumAccountGroupVo> getFollowerAccountList() {
		return followerAccountList;
	}
	public void setFollowerAccountList(List<MediumAccountGroupVo> followerAccountList) {
		this.followerAccountList = followerAccountList;
	}
	
	public String getProductNames() {
		return productNames;
	}
	public void setProductNames(String productNames) {
		this.productNames = productNames;
	}
	public Integer getLoginPromptTimes() {
		return loginPromptTimes;
	}
	public void setLoginPromptTimes(Integer loginPromptTimes) {
		this.loginPromptTimes = loginPromptTimes;
	}
	public Integer getAlreadyPromptTimes() {
		return alreadyPromptTimes;
	}
	public void setAlreadyPromptTimes(Integer alreadyPromptTimes) {
		this.alreadyPromptTimes = alreadyPromptTimes;
	}
	@Override
	public String toString() {
		return "SystemAccountVo [accountId=" + accountId + ", companyId="
				+ companyId + ", accountName=" + accountName
				+ ", accountAlias=" + accountAlias + ", password=" + password
				+ ", status=" + status + ", accountRole=" + accountRole
				+ ", menuPermission=" + menuPermission
				+ ", indicatorPermission=" + indicatorPermission + ", mobile="
				+ mobile + ", groupId=" + groupId + ", groupName=" + groupName
				+ ", companyAccountUid=" + companyAccountUid
				+ ", loginPromptTimes=" + loginPromptTimes
				+ ", alreadyPromptTimes=" + alreadyPromptTimes
				+ ", responsibleAccounts=" + responsibleAccounts
				+ ", followerAccounts=" + followerAccounts + ", productIds="
				+ productIds + ", responsibleAccountList="
				+ responsibleAccountList + ", followerAccountList="
				+ followerAccountList + ", productNames=" + productNames + "]";
	}
	
}
