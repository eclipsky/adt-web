package com.dataeye.ad.assistor.module.debugcenter.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lingliqi
 * @date 2018-01-18 10:21
 */

public class DebugCenterParameter {
    private static final Logger logger = LoggerFactory.getLogger(DebugCenterParameter.class);

    private String appid;

    private String device;

    public DebugCenterParameter() {
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    @Override
    public String toString() {
        return "DebugCenterParameter{" +
                "appid='" + appid + '\'' +
                ", device='" + device + '\'' +
                '}';
    }
}
