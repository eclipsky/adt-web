package com.dataeye.ad.assistor.module.systemaccount.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.systemaccount.model.DevAuthorizedProductVo;

/**
 * 开发工程师授权产品信息映射器.
 * Created by luzhuyou
 */
@MapperScan
public interface DevAuthorizedProductMapper {

	/**
	 * 获取开发工程师授权产品信息
	 * @param companyId
	 */
	public List<DevAuthorizedProductVo> query(Integer companyId);
	
	/**
	 * 获取开发工程师授权产品信息
	 * @param 
	 */
	public void add(DevAuthorizedProductVo vo);
	
	/**
	 * 修改开发工程师授权产品信息
	 * @param 
	 */
	public void update(DevAuthorizedProductVo vo);
	
	/**
	 * 根据开发工程师id,得到开发工程师授权产品信息
	 * @param accountId
	 */
	public DevAuthorizedProductVo getDevAuthorizedProduct(Integer accountId);
	
	/**
	 * 删除开发工程师授权产品信息
	 * @param 
	 */
	public void delete(Integer accountId);

}
