package com.dataeye.ad.assistor.module.payback.model;

import com.dataeye.ad.assistor.privilege.DataPermissionDomain;

import java.util.Date;
import java.util.List;

/**
 * 回本率趋势查询.
 * Created by huangzehai on 2017/2/17.
 */
public class PaybackTrendQuery extends DataPermissionDomain {
    /**
     * 统计日期.
     */
    private Date date;
    /**
     * 媒体ID列表.
     */
    private List<Integer> mediumIds;
    /**
     * 媒体账号ID列表.
     */
    private List<Integer> accountIds;

    /**
     * 产品ID列表.
     */
    private List<Integer> productIds;

    /**
     * 计划ID列表
     *
     * @return
     */
    private List<Integer> planIds;

    /**
     * 是否过滤自然流量.
     */
    private boolean filterNaturalFlow;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Integer> getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(List<Integer> mediumIds) {
        this.mediumIds = mediumIds;
    }

    public List<Integer> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<Integer> accountIds) {
        this.accountIds = accountIds;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public List<Integer> getPlanIds() {
        return planIds;
    }

    public void setPlanIds(List<Integer> planIds) {
        this.planIds = planIds;
    }

    public boolean isFilterNaturalFlow() {
        return filterNaturalFlow;
    }

    public void setFilterNaturalFlow(boolean filterNaturalFlow) {
        this.filterNaturalFlow = filterNaturalFlow;
    }

    @Override
    public String toString() {
        return "PaybackTrendQuery{" +
                "date=" + date +
                ", mediumIds=" + mediumIds +
                ", accountIds=" + accountIds +
                ", productIds=" + productIds +
                ", planIds=" + planIds +
                ", filterNaturalFlow=" + filterNaturalFlow +
                "} " + super.toString();
    }
}
