package com.dataeye.ad.assistor.module.systemaccount.mapper;

import org.mybatis.spring.annotation.MapperScan;

import com.dataeye.ad.assistor.module.systemaccount.model.AccountLoginInfo;

/**
 * 系统账号登陆信息映射器.
 * Created by luzhuyou
 */
@MapperScan
public interface AccountLoginInfoMapper {


	/**
	 * 新增系统账号登陆信息
	 * @param accountLoginInfo
	 * @return
	 */
	public int add(AccountLoginInfo accountLoginInfo);

}
