package com.dataeye.ad.assistor.module.mediumaccount.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.classic.Logger;

import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEContextContainer;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.SessionContainer;
import com.dataeye.ad.assistor.context.UserInSession;
import com.dataeye.ad.assistor.exception.ExceptionHandler;
import com.dataeye.ad.assistor.module.mediumaccount.model.Medium;
import com.dataeye.ad.assistor.module.mediumaccount.model.MediumSelectVo;
import com.dataeye.ad.assistor.module.mediumaccount.service.MediumService;
import com.dataeye.ad.assistor.module.systemaccount.constants.Constants.AccountRole;
import com.dataeye.ad.assistor.privilege.PrivilegeControl;

/**
 * Created by luzhuyou
 * 媒体控制器.
 */
@Controller
public class MediumController {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(MediumController.class);
    @Autowired
    private MediumService mediumService;

    /**
     * 下拉框获取媒体
     * @update ldj 2017-08-14
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/medium/queryMediumForSelector.do")
    public Object queryMediumForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DEContext context = DEContextContainer.getContext(request);
    	DEParameter parameter = context.getDeParameter();
    	UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
    	int companyId = userInSession.getCompanyId();
    	String productIds = parameter.getParameter("productIds");
        if (StringUtils.isBlank(productIds)) {
        	 productIds = null;
        }
        List<Medium> mediumList = mediumService.queryAll(companyId, productIds);
        if(userInSession.getAccountRole() == AccountRole.COMPANY) {
        	return mediumList;
        }
        // 筛选ADT账号对应具有权限查询的媒体
        Map<Integer, String> permissionMedium = userInSession.getPermissionMedium();
        if(permissionMedium == null || permissionMedium.isEmpty()) {
        	return null;
        }else{
        	List<Medium> result = new ArrayList<Medium>();
        	for(Medium medium : mediumList) {
        		if(permissionMedium.containsKey(medium.getMediumId())) {
        			result.add(medium);
        		}
        	}
        	return result;
        }
    }

    /**
     * 下拉框获取媒体，并查询出每个媒体下面对应的爬虫页面
     * @create by ldj 2017-06-23
     * */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin , write = false)
    @RequestMapping("/ad/medium/queryMediumForCrawlerPage.do")
    public Object queryMediumForCrawlerPage(HttpServletRequest request, HttpServletResponse response)throws Exception{
    	DEContext context = DEContextContainer.getContext(request);
    	int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
    	return mediumService.queryMediumForCrawlerPage(companyId);
    }
    
    /**
     * 根据产品id,下拉框获取媒体
     * @param productId
     * @create by ldj 2017-07-07
     * @update ldj 2017-08-14  废弃掉
     * 
     * **/
/*    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/medium/queryMediumForSelectorById.do")
    public Object queryMediumForSelectorById(HttpServletRequest request, HttpServletResponse response) throws Exception {
         DEContext context = DEContextContainer.getContext(request);
         DEParameter parameter = context.getDeParameter();
         int companyId = SessionContainer.getCurrentUser(context.getSession()).getCompanyId();
         String productIds = parameter.getParameter("productIds");
         if (StringUtils.isBlank(productIds)) {
        	 productIds = null;
         }
        List<Medium> mediumList = mediumService.queryByProductId(companyId, productIds);
        return mediumList;
    }*/
    
    /**
     * 根据产品id，下拉框查询媒体列表和对应的tracking渠道列表 
     * @param productId
     * @create by ldj 2017-08-10
     * 
     * **/
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = false)
    @RequestMapping("/ad/medium/queryMediumMappingForSelector.do")
    public Object queryMediumMappingForSelector(HttpServletRequest request, HttpServletResponse response) throws Exception {
         DEContext context = DEContextContainer.getContext(request);
         DEParameter parameter = context.getDeParameter();
         UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
     	int companyId = userInSession.getCompanyId();
         String productIds = parameter.getParameter("productIds");
         if (StringUtils.isBlank(productIds)) {
        	 productIds = null;
         }
//        return mediumService.queryMediumAndChannelByProductId(companyId, productIds);
        List<MediumSelectVo> mediumList = mediumService.queryMediumAndChannelByProductId(companyId, productIds);
        if(userInSession.getAccountRole() == AccountRole.COMPANY) {
        	return mediumList;
        }
        // 筛选ADT账号对应具有权限查询的媒体
        Map<Integer, String> permissionResponsibleMedium = userInSession.getPermissionResponsibleMedium();
        if(permissionResponsibleMedium == null || permissionResponsibleMedium.isEmpty()) {
        	return null;
        }else{
        	List<MediumSelectVo> result = new ArrayList<MediumSelectVo>();
        	for(MediumSelectVo medium : mediumList) {
        		if(permissionResponsibleMedium.containsKey(medium.getMediumId())) {
        			result.add(medium);
        		}
        	}
        	return result;
        }
    }
    /**
     * 新增自定义媒体
     *
     * @param request
     * @param response
     * @return object
     * @throws Exception
     * @author luzhuyou 2017/08/10
     */
    @PrivilegeControl(scope = PrivilegeControl.Scope.AfterLogin, write = true)
    @RequestMapping("/ad/medium/add-custom.do")
    public Object addCustomMedium(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("新增自定义媒体.");
    	DEContext context = DEContextContainer.getContext(request);
        DEParameter parameter = context.getDeParameter();
        String mediumName = parameter.getParameter("mediumName");
        if(StringUtils.isBlank(mediumName)) {
        	logger.error("媒体名称不允许为空.");
            ExceptionHandler.throwParameterException(StatusCode.MEDI_MEDIUM_NAME_IS_EXISTS);
        }
        UserInSession userInSession = SessionContainer.getCurrentUser(context.getSession());
        int companyId = userInSession.getCompanyId();
        
        // 校验媒体是否已存在
        Medium account = mediumService.get(mediumName.trim(), companyId);
		if (account != null) {
			logger.error("媒体已存在.");
			ExceptionHandler.throwParameterException(StatusCode.MEDI_MEDIUM_IS_EXISTS);
		}
		
		//获取uid
		int uid = userInSession.getCompanyAccountUid();
		
		// 新增自定义媒体
        return mediumService.addCustomMedium(mediumName.trim(), companyId, uid);
    }
}
