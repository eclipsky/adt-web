package com.dataeye.ad.assistor.module.realtimedelivery.model;

/**
 * Created by lenovo on 2017/2/28.
 */
public enum AlertType {
    General(0), Advanced(1);

    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    AlertType(int value) {
        this.value = value;
    }
}
