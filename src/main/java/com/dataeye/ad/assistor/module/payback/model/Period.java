package com.dataeye.ad.assistor.module.payback.model;

import org.apache.commons.lang.StringUtils;


/**
 * 查询粒度 ，天、周、月
 */
public enum Period {
	Month, Week, Day;
	public static Period parse(String text) {
        for (Period period : Period.values()) {
            if (StringUtils.equalsIgnoreCase(period.name(), text)) {
                return period;
            }
        }
        return null;
    }
}
