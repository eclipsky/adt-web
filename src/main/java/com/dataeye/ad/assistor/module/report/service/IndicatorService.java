package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.module.report.model.Indicator;
import com.dataeye.ad.assistor.module.report.model.IndicatorHabit;
import com.dataeye.ad.assistor.module.report.model.IndicatorPreferenceQuery;

import java.util.List;

/**
 * Created by huangzehai on 2017/6/8.
 */
public interface IndicatorService {
    /**
     * 更新指标偏好.
     *
     * @param indicatorHabit
     */
    int updateIndicatorPreference(IndicatorHabit indicatorHabit);

    /**
     * 获取用户的某个菜单的指标偏好.
     *
     * @param query
     * @return
     */
    List<Indicator> getIndicatorPreference(IndicatorPreferenceQuery query);

    /**
     * 列出所有的指标
     *
     * @return
     */
    List<Indicator> listIndicators();

    /**
     * 添加用户的指标偏好
     *
     * @param companyId
     * @param accountId
     * @param accountRole
     * @param indicatorPermission
     * @param menuId
     * @return
     */
    int addIndicatorPreference(int companyId, int accountId, int accountRole, String indicatorPermission,String menuId);

    /**
     * 因为指标偏好权限发生变动后更改用户的指标偏好
     *
     * @param companyId
     * @param accountId
     * @param accountRole
     * @param indicatorPermission
     * @return
     */
    void updateIndicatorPreference(int companyId, int accountId, int accountRole, String indicatorPermission);
}
