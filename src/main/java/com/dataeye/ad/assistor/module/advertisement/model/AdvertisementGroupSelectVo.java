package com.dataeye.ad.assistor.module.advertisement.model;


import com.google.gson.annotations.Expose;


/**
 * 广告组
 * Created by ldj
 */
public class AdvertisementGroupSelectVo {

	/**广告组id**/
	@Expose
    private Integer adGroupId;
	/**广告组名称**/
	@Expose
    private String adGroupName;
	public Integer getAdGroupId() {
		return adGroupId;
	}
	public void setAdGroupId(Integer adGroupId) {
		this.adGroupId = adGroupId;
	}
	public String getAdGroupName() {
		return adGroupName;
	}
	public void setAdGroupName(String adGroupName) {
		this.adGroupName = adGroupName;
	}
	@Override
	public String toString() {
		return "AdvertisementGroupSelectVo [adGroupId=" + adGroupId
				+ ", adGroupName=" + adGroupName + "]";
	}
	
}
