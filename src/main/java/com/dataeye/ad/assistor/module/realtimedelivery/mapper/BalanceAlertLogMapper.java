package com.dataeye.ad.assistor.module.realtimedelivery.mapper;

import com.dataeye.ad.assistor.module.realtimedelivery.model.BalanceAlertLog;

import java.util.List;

/**
 * Created by huangzehai on 2017/5/3.
 */
public interface BalanceAlertLogMapper {

    List<BalanceAlertLog> listBalanceAlertLogs();

    void saveBalanceAlertLog(BalanceAlertLog balanceAlertLog);
}
