package com.dataeye.ad.assistor.module.report.service;

import com.dataeye.ad.assistor.common.Chart;
import com.dataeye.ad.assistor.common.DailyRecord;
import com.dataeye.ad.assistor.common.TrendChart;
import com.dataeye.ad.assistor.module.dictionaries.model.Plan;
import com.dataeye.ad.assistor.module.report.mapper.DeliveryContrastiveAnalysisMapper;
import com.dataeye.ad.assistor.module.report.model.DeliveryTrendQuery;
import com.dataeye.ad.assistor.module.report.model.PlanQuery;
import com.dataeye.ad.assistor.util.CurrencyUtils;
import com.dataeye.ad.assistor.util.TrendChartUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by huangzehai on 2017/4/27.
 */
@Service
public class DeliveryContrastiveAnalysisServiceImpl implements DeliveryContrastiveAnalysisService {
    @Autowired
    private DeliveryContrastiveAnalysisMapper deliveryContrastiveAnalysisMapper;

    /**
     * 消耗趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, BigDecimal> costTrendGroupByMediumAccount(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.MediumAccount);
        if (isDefaultChart) {
            addMediumAccountDefaultFilter(query);
        }
        List<? extends DailyRecord<String, BigDecimal>> costs;
        switch (query.getPeriod()) {
            case Day:
                costs = deliveryContrastiveAnalysisMapper.costTrendGroupByMediumAccount(query);
                break;
            case Week:
                costs = deliveryContrastiveAnalysisMapper.costWeeklyTrendGroupByMediumAccount(query);
                break;
            case Month:
                costs = deliveryContrastiveAnalysisMapper.costMonthlyTrendGroupByMediumAccount(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                costs = deliveryContrastiveAnalysisMapper.costMinutelyTrendGroupByMediumAccount(query);
                break;
            default:
                costs = deliveryContrastiveAnalysisMapper.costTrendGroupByMediumAccount(query);
                break;
        }
        //货币单元转换
        for (DailyRecord<String, BigDecimal> cost : costs) {
            cost.setValue(CurrencyUtils.fenToYuan(cost.getValue()));
        }
        //转化为趋势图
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), costs, new BigDecimal(0), false, isDefaultChart);
        return mediumAccountChartCompletion(query, trendChart, BigDecimal.valueOf(0));

    }

    /**
     * 媒体账号趋势图补全
     *
     * @param query
     * @param trendChart
     * @param defaultValue
     * @param <V>
     * @return
     */
    private <V> TrendChart<String, V> mediumAccountChartCompletion(DeliveryTrendQuery query, TrendChart<String, V> trendChart, V defaultValue) {
        //趋势图补全
        if (query.getMediumAccountIds() != null && !query.getMediumAccountIds().isEmpty() && (trendChart == null || trendChart.getIndicators() == null || query.getMediumAccountIds().size() > trendChart.getIndicators().size())) {
            //获取查询的媒体账号名称
            List<String> names = deliveryContrastiveAnalysisMapper.listMediumAccounts(query);
            return chartCompletion(trendChart, names, defaultValue, trendChart.getDates().size());
        }
        return trendChart;
    }

    private <V> TrendChart<String, V> planChartCompletion(DeliveryTrendQuery query, TrendChart<String, V> trendChart, V defaultValue) {
        //趋势图补全
        if (query.getPlanIds() != null && !query.getPlanIds().isEmpty() && (trendChart == null || trendChart.getIndicators() == null || query.getPlanIds().size() > trendChart.getIndicators().size())) {
            //获取查询的媒体账号名称
            List<String> names = deliveryContrastiveAnalysisMapper.listPlanNames(query);
            return chartCompletion(trendChart, names, defaultValue, trendChart.getDates().size());
        }
        return trendChart;
    }

    private <V> TrendChart<String, V> mediumChartCompletion(DeliveryTrendQuery query, TrendChart<String, V> trendChart, V defaultValue) {
        //趋势图补全
        if (query.getMediumIds() != null && !query.getMediumIds().isEmpty() && (trendChart == null || trendChart.getIndicators() == null || query.getMediumIds().size() > trendChart.getIndicators().size())) {
            //获取查询的媒体账号名称
            List<String> names = deliveryContrastiveAnalysisMapper.listMediums(query);
            return chartCompletion(trendChart, names, defaultValue, trendChart.getDates().size());
        }
        return trendChart;
    }

    private <V> TrendChart<String, V> productChartCompletion(DeliveryTrendQuery query, TrendChart<String, V> trendChart, V defaultValue) {
        //趋势图补全
        if (query.getProductIds() != null && !query.getProductIds().isEmpty() && (trendChart == null || trendChart.getIndicators() == null || query.getProductIds().size() > trendChart.getIndicators().size())) {
            //获取查询的媒体账号名称
            List<String> names = deliveryContrastiveAnalysisMapper.listProducts(query);
            return chartCompletion(trendChart, names, defaultValue, trendChart.getDates().size());
        }
        return trendChart;
    }

    /**
     * 趋势图补全
     *
     * @param trendChart
     * @param names
     */
    private <N, V> TrendChart<N, V> chartCompletion(TrendChart<N, V> trendChart, List<N> names, V defaultValue, int size) {
        if (names == null || names.isEmpty()) {
            return trendChart;
        }

        if (trendChart == null) {
            trendChart = new TrendChart<>();
        }

        List<N> existingNames = new ArrayList<>();
        for (Chart<N, V> chart : trendChart.getIndicators()) {
            existingNames.add(chart.getTitle());
        }

        for (N name : names) {
            if (!existingNames.contains(name)) {
                Chart<N, V> chart = new Chart<>();
                chart.setTitle(name);
                chart.setValues(defaultValues(defaultValue, size));
                trendChart.getIndicators().add(chart);
            }
        }
        return trendChart;
    }

    private <V> List<V> defaultValues(V defaultValue, int size) {
        int index = 0;
        List<V> values = new ArrayList<>();
        while (index < size) {
            values.add(defaultValue);
            index++;
        }
        return values;
    }

    /**
     * 获取时段统计查询的截止时间
     *
     * @param startDate
     * @return
     */
    private Date getEndTime(Date startDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    /**
     * 添加默认媒体账号过滤器
     *
     * @param query
     */
    private void addMediumAccountDefaultFilter(DeliveryTrendQuery query) {
        List<Integer> mediumAccountIds = deliveryContrastiveAnalysisMapper.getDefaultMediumAccountIds(query);
        query.setMediumAccountIds(mediumAccountIds);
    }

    /**
     * 添加默认媒体过滤器
     *
     * @param query
     */
    private void addMediumDefaultFilter(DeliveryTrendQuery query) {
        List<Integer> mediumIds = deliveryContrastiveAnalysisMapper.getDefaultMediumIds(query);
        query.setMediumIds(mediumIds);
    }

    /**
     * 添加默认计划过滤器
     *
     * @param query
     */
    private void addPlanDefaultFilter(DeliveryTrendQuery query) {
        List<Integer> planIds = deliveryContrastiveAnalysisMapper.getDefaultPlanIds(query);
        query.setPlanIds(planIds);
    }

    /**
     * 添加默认产品过滤器
     *
     * @param query
     */
    private void addProductDefaultFilter(DeliveryTrendQuery query) {
        List<Integer> productIds = deliveryContrastiveAnalysisMapper.getDefaultProductIds(query);
        query.setPlanIds(productIds);
    }

    @Override
    public TrendChart<String, Double> ctrTrendGroupByMediumAccount(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.MediumAccount);
        if (isDefaultChart) {
            addMediumAccountDefaultFilter(query);
        }
        List<? extends DailyRecord<String, Double>> ctrList;
        switch (query.getPeriod()) {
            case Day:
                ctrList = deliveryContrastiveAnalysisMapper.ctrTrendGroupByMediumAccount(query);
                break;
            case Week:
                ctrList = deliveryContrastiveAnalysisMapper.ctrWeeklyTrendGroupByMediumAccount(query);
                break;
            case Month:
                ctrList = deliveryContrastiveAnalysisMapper.ctrMonthlyTrendGroupByMediumAccount(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, Integer>> clicks = deliveryContrastiveAnalysisMapper.clickMinutelyTrendGroupByMediumAccount(query);
                List<DailyRecord<String, Integer>> exposures = deliveryContrastiveAnalysisMapper.exposureMinutelyTrendGroupByMediumAccount(query);
                TrendChart<String, Integer> clickChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), clicks, 0, true, isDefaultChart);
                TrendChart<String, Integer> exposureChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), exposures, 0, true, isDefaultChart);
                return TrendChartUtils.toPeriodCtrTrendChart(clickChart, exposureChart);
            default:
                ctrList = deliveryContrastiveAnalysisMapper.ctrTrendGroupByMediumAccount(query);
        }
        TrendChart<String, Double> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), ctrList, 0.0, true, isDefaultChart);
        return mediumAccountChartCompletion(query, trendChart, 0.0);
    }

    /**
     * 下载率日趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, Double> downloadRateTrendGroupByMediumAccount(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.MediumAccount);
        if (isDefaultChart) {
            addMediumAccountDefaultFilter(query);
        }

        List<? extends DailyRecord<String, Double>> downloadRates;
        switch (query.getPeriod()) {
            case Day:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateTrendGroupByMediumAccount(query);
                break;
            case Week:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateWeeklyTrendGroupByMediumAccount(query);
                break;
            case Month:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateMonthlyTrendGroupByMediumAccount(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, Integer>> downloadTimes = deliveryContrastiveAnalysisMapper.downloadTimesMinutelyTrendGroupByMediumAccount(query);
                List<DailyRecord<String, Integer>> reaches = deliveryContrastiveAnalysisMapper.reachesMinutelyTrendGroupByMediumAccount(query);
                TrendChart<String, Integer> downloadTimesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), downloadTimes, 0, true, isDefaultChart);
                TrendChart<String, Integer> reachesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), reaches, 0, true, isDefaultChart);
                return TrendChartUtils.toPeriodRateTrendChart(downloadTimesChart, reachesChart);
            default:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateTrendGroupByMediumAccount(query);
        }
        TrendChart<String, Double> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), downloadRates, 0.0, true, isDefaultChart);
        return mediumAccountChartCompletion(query, trendChart, 0.0);
    }

    /**
     * 注册数趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, Integer> registrationTrendGroupByMediumAccount(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.MediumAccount);
        if (isDefaultChart) {
            addMediumAccountDefaultFilter(query);
        }
        List<? extends DailyRecord<String, Integer>> registrations;
        switch (query.getPeriod()) {
            case Day:
                registrations = deliveryContrastiveAnalysisMapper.registrationTrendGroupByMediumAccount(query);
                break;
            case Week:
                registrations = deliveryContrastiveAnalysisMapper.registrationWeeklyTrendGroupByMediumAccount(query);
                break;
            case Month:
                registrations = deliveryContrastiveAnalysisMapper.registrationMonthlyTrendGroupByMediumAccount(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                registrations = deliveryContrastiveAnalysisMapper.registrationMinutelyTrendGroupByMediumAccount(query);
                break;
            default:
                registrations = deliveryContrastiveAnalysisMapper.registrationTrendGroupByMediumAccount(query);
        }
        TrendChart<String, Integer> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), registrations, 0, false, isDefaultChart);
        return mediumAccountChartCompletion(query, trendChart, 0);
    }

    /**
     * CPA趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, BigDecimal> cpaTrendGroupByMediumAccount(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.MediumAccount);
        if (isDefaultChart) {
            addMediumAccountDefaultFilter(query);
        }
        List<? extends DailyRecord<String, BigDecimal>> cpaList;
        switch (query.getPeriod()) {
            case Day:
                cpaList = deliveryContrastiveAnalysisMapper.cpaTrendGroupByMediumAccount(query);
                break;
            case Week:
                cpaList = deliveryContrastiveAnalysisMapper.cpaWeeklyTrendGroupByMediumAccount(query);
                break;
            case Month:
                cpaList = deliveryContrastiveAnalysisMapper.cpaMonthlyTrendGroupByMediumAccount(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, BigDecimal>> costs = deliveryContrastiveAnalysisMapper.costMinutelyTrendGroupByMediumAccount(query);
                //货币单元转换
                for (DailyRecord<String, BigDecimal> cost : costs) {
                    cost.setValue(CurrencyUtils.fenToYuan(cost.getValue()));
                }
                List<DailyRecord<String, Integer>> registrations = deliveryContrastiveAnalysisMapper.registrationMinutelyTrendGroupByMediumAccount(query);
                TrendChart<String, BigDecimal> downloadTimesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), costs, new BigDecimal(0), false, isDefaultChart);
                TrendChart<String, Integer> reachesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), registrations, 0, false, isDefaultChart);
                return TrendChartUtils.toPeriodCpaTrendChart(downloadTimesChart, reachesChart);
            default:
                cpaList = deliveryContrastiveAnalysisMapper.cpaTrendGroupByMediumAccount(query);
        }

        for (DailyRecord<String, BigDecimal> cpa : cpaList) {
            cpa.setValue(CurrencyUtils.fenToYuan(cpa.getValue()));
        }
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), cpaList, new BigDecimal(0), false, isDefaultChart);
        return mediumAccountChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    /**
     * 总充值趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, BigDecimal> rechargeTrendGroupByMediumAccount(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.MediumAccount);
        if (isDefaultChart) {
            addMediumAccountDefaultFilter(query);
        }

        List<? extends DailyRecord<String, BigDecimal>> recharges;
        switch (query.getPeriod()) {
            case Day:
                recharges = deliveryContrastiveAnalysisMapper.rechargeTrendGroupByMediumAccount(query);
                break;
            case Week:
                recharges = deliveryContrastiveAnalysisMapper.rechargeWeeklyTrendGroupByMediumAccount(query);
                break;
            case Month:
                recharges = deliveryContrastiveAnalysisMapper.rechargeMonthlyTrendGroupByMediumAccount(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                recharges = deliveryContrastiveAnalysisMapper.rechargeMinutelyTrendGroupByMediumAccount(query);
                break;
            default:
                recharges = deliveryContrastiveAnalysisMapper.rechargeTrendGroupByMediumAccount(query);
        }

        for (DailyRecord<String, BigDecimal> recharge : recharges) {
            recharge.setValue(CurrencyUtils.fenToYuan(recharge.getValue()));
        }
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), recharges, new BigDecimal(0), false, isDefaultChart);
        return mediumAccountChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    @Override
    public TrendChart<String, BigDecimal> costTrendGroupByPlan(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Plan);
        if (isDefaultChart) {
            addPlanDefaultFilter(query);
        }
        List<? extends DailyRecord<String, BigDecimal>> costs;
        switch (query.getPeriod()) {
            case Day:
                costs = deliveryContrastiveAnalysisMapper.costTrendGroupByPlan(query);
                break;
            case Week:
                costs = deliveryContrastiveAnalysisMapper.costWeeklyTrendGroupByPlan(query);
                break;
            case Month:
                costs = deliveryContrastiveAnalysisMapper.costMonthlyTrendGroupByPlan(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                costs = deliveryContrastiveAnalysisMapper.costMinutelyTrendGroupByPlan(query);
                break;
            default:
                costs = deliveryContrastiveAnalysisMapper.costTrendGroupByPlan(query);
                break;
        }
        //货币单元转换
        for (DailyRecord<String, BigDecimal> cost : costs) {
            cost.setValue(CurrencyUtils.fenToYuan(cost.getValue()));
        }
        //转化为趋势图
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), costs, new BigDecimal(0), false, isDefaultChart);
        return planChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    /**
     * 按计划分组CTR趋势图
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, Double> ctrTrendGroupByPlan(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Plan);
        if (isDefaultChart) {
            addPlanDefaultFilter(query);
        }
        List<? extends DailyRecord<String, Double>> ctrList;
        switch (query.getPeriod()) {
            case Day:
                ctrList = deliveryContrastiveAnalysisMapper.ctrTrendGroupByPlan(query);
                break;
            case Week:
                ctrList = deliveryContrastiveAnalysisMapper.ctrWeeklyTrendGroupByPlan(query);
                break;
            case Month:
                ctrList = deliveryContrastiveAnalysisMapper.ctrMonthlyTrendGroupByPlan(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, Integer>> clicks = deliveryContrastiveAnalysisMapper.clickMinutelyTrendGroupByPlan(query);
                List<DailyRecord<String, Integer>> exposures = deliveryContrastiveAnalysisMapper.exposureMinutelyTrendGroupByPlan(query);
                TrendChart<String, Integer> clickChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), clicks, 0, true, isDefaultChart);
                TrendChart<String, Integer> exposureChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), exposures, 0, true, isDefaultChart);
                return TrendChartUtils.toPeriodCtrTrendChart(clickChart, exposureChart);
            default:
                ctrList = deliveryContrastiveAnalysisMapper.ctrTrendGroupByPlan(query);
                break;
        }
        //转化为趋势图
        TrendChart<String, Double> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), ctrList, 0.0, true, isDefaultChart);
        return planChartCompletion(query, trendChart, 0.0);
    }

    /**
     * 下载率趋势图，按计划分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, Double> downloadRateTrendGroupByPlan(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Plan);
        if (isDefaultChart) {
            addPlanDefaultFilter(query);
        }

        List<? extends DailyRecord<String, Double>> downloadRates;
        switch (query.getPeriod()) {
            case Day:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateTrendGroupByPlan(query);
                break;
            case Week:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateWeeklyTrendGroupByPlan(query);
                break;
            case Month:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateMonthlyTrendGroupByPlan(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, Integer>> downloadTimes = deliveryContrastiveAnalysisMapper.downloadTimesMinutelyTrendGroupByPlan(query);
                List<DailyRecord<String, Integer>> reaches = deliveryContrastiveAnalysisMapper.reachesMinutelyTrendGroupByPlan(query);
                TrendChart<String, Integer> downloadTimesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), downloadTimes, 0, true, isDefaultChart);
                TrendChart<String, Integer> reachesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), reaches, 0, true, isDefaultChart);
                return TrendChartUtils.toPeriodRateTrendChart(downloadTimesChart, reachesChart);
            default:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateTrendGroupByPlan(query);
        }
        TrendChart<String, Double> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), downloadRates, 0.0, true, isDefaultChart);
        return planChartCompletion(query, trendChart, 0.0);
    }

    @Override
    public TrendChart<String, Integer> registrationTrendGroupByPlan(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Plan);
        if (isDefaultChart) {
            addPlanDefaultFilter(query);
        }
        List<? extends DailyRecord<String, Integer>> registrations;
        switch (query.getPeriod()) {
            case Day:
                registrations = deliveryContrastiveAnalysisMapper.registrationTrendGroupByPlan(query);
                break;
            case Week:
                registrations = deliveryContrastiveAnalysisMapper.registrationWeeklyTrendGroupByPlan(query);
                break;
            case Month:
                registrations = deliveryContrastiveAnalysisMapper.registrationMonthlyTrendGroupByPlan(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                registrations = deliveryContrastiveAnalysisMapper.registrationMinutelyTrendGroupByPlan(query);
                break;
            default:
                registrations = deliveryContrastiveAnalysisMapper.registrationTrendGroupByPlan(query);
        }
        TrendChart<String, Integer> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), registrations, 0, false, isDefaultChart);
        return planChartCompletion(query, trendChart, 0);
    }

    /**
     * CPA趋势图，按计划分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, BigDecimal> cpaTrendGroupByPlan(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Plan);
        if (isDefaultChart) {
            addPlanDefaultFilter(query);
        }
        List<? extends DailyRecord<String, BigDecimal>> cpaList;
        switch (query.getPeriod()) {
            case Day:
                cpaList = deliveryContrastiveAnalysisMapper.cpaTrendGroupByPlan(query);
                break;
            case Week:
                cpaList = deliveryContrastiveAnalysisMapper.cpaWeeklyTrendGroupByPlan(query);
                break;
            case Month:
                cpaList = deliveryContrastiveAnalysisMapper.cpaMonthlyTrendGroupByPlan(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, BigDecimal>> costs = deliveryContrastiveAnalysisMapper.costMinutelyTrendGroupByPlan(query);
                //货币单元转换
                for (DailyRecord<String, BigDecimal> cost : costs) {
                    cost.setValue(CurrencyUtils.fenToYuan(cost.getValue()));
                }
                List<DailyRecord<String, Integer>> registrations = deliveryContrastiveAnalysisMapper.registrationMinutelyTrendGroupByPlan(query);
                TrendChart<String, BigDecimal> downloadTimesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), costs, new BigDecimal(0), false, isDefaultChart);
                TrendChart<String, Integer> reachesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), registrations, 0, false, isDefaultChart);
                return TrendChartUtils.toPeriodCpaTrendChart(downloadTimesChart, reachesChart);
            default:
                cpaList = deliveryContrastiveAnalysisMapper.cpaTrendGroupByPlan(query);
        }

        for (DailyRecord<String, BigDecimal> cpa : cpaList) {
            cpa.setValue(CurrencyUtils.fenToYuan(cpa.getValue()));
        }
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), cpaList, new BigDecimal(0), false, isDefaultChart);
        return planChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    @Override
    public TrendChart<String, BigDecimal> rechargeTrendGroupByPlan(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Plan);
        if (isDefaultChart) {
            addPlanDefaultFilter(query);
        }

        List<? extends DailyRecord<String, BigDecimal>> recharges;
        switch (query.getPeriod()) {
            case Day:
                recharges = deliveryContrastiveAnalysisMapper.rechargeTrendGroupByPlan(query);
                break;
            case Week:
                recharges = deliveryContrastiveAnalysisMapper.rechargeWeeklyTrendGroupByPlan(query);
                break;
            case Month:
                recharges = deliveryContrastiveAnalysisMapper.rechargeMonthlyTrendGroupByPlan(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                recharges = deliveryContrastiveAnalysisMapper.rechargeMinutelyTrendGroupByPlan(query);
                break;
            default:
                recharges = deliveryContrastiveAnalysisMapper.rechargeTrendGroupByPlan(query);
        }

        for (DailyRecord<String, BigDecimal> recharge : recharges) {
            recharge.setValue(CurrencyUtils.fenToYuan(recharge.getValue()));
        }
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), recharges, new BigDecimal(0), false, isDefaultChart);
        return planChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    /**
     * 判断是否是默认图(没有任何过滤条件)
     *
     * @param query
     * @return
     */
    private boolean isDefaultChart(DeliveryTrendQuery query, GroupBy groupBy) {
        boolean noPlanFilter = query.getPlanIds() == null || query.getPlanIds().isEmpty();
        boolean noMediumAccountFilter = query.getMediumAccountIds() == null || query.getMediumAccountIds().isEmpty();
        boolean noMediumFilter = query.getMediumIds() == null || query.getMediumIds().isEmpty();
        boolean noProductFilter = query.getProductIds() == null || query.getProductIds().isEmpty();
        switch (groupBy) {
            case Plan:
                return noPlanFilter;
            case MediumAccount:
                return noMediumAccountFilter;
            case Medium:
                return noMediumFilter;
            case Product:
                return noProductFilter;
            default:
                return false;
        }
    }

    @Override
    public List<Plan> listPlans(PlanQuery planQuery) {
        return deliveryContrastiveAnalysisMapper.listPlans(planQuery);
    }

    /**
     * 消耗趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, BigDecimal> costTrendGroupByMedium(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Medium);
        if (isDefaultChart) {
            addMediumDefaultFilter(query);
        }
        List<? extends DailyRecord<String, BigDecimal>> costs;
        switch (query.getPeriod()) {
            case Day:
                costs = deliveryContrastiveAnalysisMapper.costTrendGroupByMedium(query);
                break;
            case Week:
                costs = deliveryContrastiveAnalysisMapper.costWeeklyTrendGroupByMedium(query);
                break;
            case Month:
                costs = deliveryContrastiveAnalysisMapper.costMonthlyTrendGroupByMedium(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                costs = deliveryContrastiveAnalysisMapper.costMinutelyTrendGroupByMedium(query);
                break;
            default:
                costs = deliveryContrastiveAnalysisMapper.costTrendGroupByMedium(query);
                break;
        }
        //货币单元转换
        for (DailyRecord<String, BigDecimal> cost : costs) {
            cost.setValue(CurrencyUtils.fenToYuan(cost.getValue()));
        }
        //转化为趋势图
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), costs, new BigDecimal(0), false, isDefaultChart);
        return mediumChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    @Override
    public TrendChart<String, Double> ctrTrendGroupByMedium(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Medium);
        if (isDefaultChart) {
            addMediumDefaultFilter(query);
        }
        List<? extends DailyRecord<String, Double>> ctrList;
        switch (query.getPeriod()) {
            case Day:
                ctrList = deliveryContrastiveAnalysisMapper.ctrTrendGroupByMedium(query);
                break;
            case Week:
                ctrList = deliveryContrastiveAnalysisMapper.ctrWeeklyTrendGroupByMedium(query);
                break;
            case Month:
                ctrList = deliveryContrastiveAnalysisMapper.ctrMonthlyTrendGroupByMedium(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, Integer>> clicks = deliveryContrastiveAnalysisMapper.clickMinutelyTrendGroupByMedium(query);
                List<DailyRecord<String, Integer>> exposures = deliveryContrastiveAnalysisMapper.exposureMinutelyTrendGroupByMedium(query);
                TrendChart<String, Integer> clickChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), clicks, 0, true, isDefaultChart);
                TrendChart<String, Integer> exposureChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), exposures, 0, true, isDefaultChart);
                return TrendChartUtils.toPeriodCtrTrendChart(clickChart, exposureChart);
            default:
                ctrList = deliveryContrastiveAnalysisMapper.ctrTrendGroupByMedium(query);
        }
        TrendChart<String, Double> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), ctrList, 0.0, true, isDefaultChart);
        return mediumChartCompletion(query, trendChart, 0.0);
    }

    /**
     * 下载率日趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, Double> downloadRateTrendGroupByMedium(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Medium);
        if (isDefaultChart) {
            addMediumDefaultFilter(query);
        }

        List<? extends DailyRecord<String, Double>> downloadRates;
        switch (query.getPeriod()) {
            case Day:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateTrendGroupByMedium(query);
                break;
            case Week:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateWeeklyTrendGroupByMedium(query);
                break;
            case Month:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateMonthlyTrendGroupByMedium(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, Integer>> downloadTimes = deliveryContrastiveAnalysisMapper.downloadTimesMinutelyTrendGroupByMedium(query);
                List<DailyRecord<String, Integer>> reaches = deliveryContrastiveAnalysisMapper.reachesMinutelyTrendGroupByMedium(query);
                TrendChart<String, Integer> downloadTimesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), downloadTimes, 0, true, isDefaultChart);
                TrendChart<String, Integer> reachesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), reaches, 0, true, isDefaultChart);
                return TrendChartUtils.toPeriodRateTrendChart(downloadTimesChart, reachesChart);
            default:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateTrendGroupByMedium(query);
        }
        TrendChart<String, Double> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), downloadRates, 0.0, true, isDefaultChart);
        return mediumChartCompletion(query, trendChart, 0.0);
    }

    /**
     * 注册数趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, Integer> registrationTrendGroupByMedium(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Medium);
        if (isDefaultChart) {
            addMediumDefaultFilter(query);
        }
        List<? extends DailyRecord<String, Integer>> registrations;
        switch (query.getPeriod()) {
            case Day:
                registrations = deliveryContrastiveAnalysisMapper.registrationTrendGroupByMedium(query);
                break;
            case Week:
                registrations = deliveryContrastiveAnalysisMapper.registrationWeeklyTrendGroupByMedium(query);
                break;
            case Month:
                registrations = deliveryContrastiveAnalysisMapper.registrationMonthlyTrendGroupByMedium(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                registrations = deliveryContrastiveAnalysisMapper.registrationMinutelyTrendGroupByMedium(query);
                break;
            default:
                registrations = deliveryContrastiveAnalysisMapper.registrationTrendGroupByMedium(query);
        }
        TrendChart<String, Integer> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), registrations, 0, false, isDefaultChart);
        return mediumChartCompletion(query, trendChart, 0);
    }

    /**
     * CPA趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, BigDecimal> cpaTrendGroupByMedium(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Medium);
        if (isDefaultChart) {
            addMediumDefaultFilter(query);
        }
        List<? extends DailyRecord<String, BigDecimal>> cpaList;
        switch (query.getPeriod()) {
            case Day:
                cpaList = deliveryContrastiveAnalysisMapper.cpaTrendGroupByMedium(query);
                break;
            case Week:
                cpaList = deliveryContrastiveAnalysisMapper.cpaWeeklyTrendGroupByMedium(query);
                break;
            case Month:
                cpaList = deliveryContrastiveAnalysisMapper.cpaMonthlyTrendGroupByMedium(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, BigDecimal>> costs = deliveryContrastiveAnalysisMapper.costMinutelyTrendGroupByMedium(query);
                //货币单元转换
                for (DailyRecord<String, BigDecimal> cost : costs) {
                    cost.setValue(CurrencyUtils.fenToYuan(cost.getValue()));
                }
                List<DailyRecord<String, Integer>> registrations = deliveryContrastiveAnalysisMapper.registrationMinutelyTrendGroupByMedium(query);
                TrendChart<String, BigDecimal> downloadTimesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), costs, new BigDecimal(0), false, isDefaultChart);
                TrendChart<String, Integer> reachesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), registrations, 0, false, isDefaultChart);
                return TrendChartUtils.toPeriodCpaTrendChart(downloadTimesChart, reachesChart);
            default:
                cpaList = deliveryContrastiveAnalysisMapper.cpaTrendGroupByMedium(query);
        }

        for (DailyRecord<String, BigDecimal> cpa : cpaList) {
            cpa.setValue(CurrencyUtils.fenToYuan(cpa.getValue()));
        }
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), cpaList, new BigDecimal(0), false, isDefaultChart);
        return mediumChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    @Override
    public TrendChart<String, BigDecimal> rechargeTrendGroupByMedium(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Medium);
        if (isDefaultChart) {
            addMediumDefaultFilter(query);
        }

        List<? extends DailyRecord<String, BigDecimal>> recharges;
        switch (query.getPeriod()) {
            case Day:
                recharges = deliveryContrastiveAnalysisMapper.rechargeTrendGroupByMedium(query);
                break;
            case Week:
                recharges = deliveryContrastiveAnalysisMapper.rechargeWeeklyTrendGroupByMedium(query);
                break;
            case Month:
                recharges = deliveryContrastiveAnalysisMapper.rechargeMonthlyTrendGroupByMedium(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                recharges = deliveryContrastiveAnalysisMapper.rechargeMinutelyTrendGroupByMedium(query);
                break;
            default:
                recharges = deliveryContrastiveAnalysisMapper.rechargeTrendGroupByMedium(query);
        }

        for (DailyRecord<String, BigDecimal> recharge : recharges) {
            recharge.setValue(CurrencyUtils.fenToYuan(recharge.getValue()));
        }
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), recharges, new BigDecimal(0), false, isDefaultChart);
        return mediumChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    /**
     * 消耗趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, BigDecimal> costTrendGroupByProduct(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Product);
        if (isDefaultChart) {
            addProductDefaultFilter(query);
        }
        List<? extends DailyRecord<String, BigDecimal>> costs;
        switch (query.getPeriod()) {
            case Day:
                costs = deliveryContrastiveAnalysisMapper.costTrendGroupByProduct(query);
                break;
            case Week:
                costs = deliveryContrastiveAnalysisMapper.costWeeklyTrendGroupByProduct(query);
                break;
            case Month:
                costs = deliveryContrastiveAnalysisMapper.costMonthlyTrendGroupByProduct(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                costs = deliveryContrastiveAnalysisMapper.costMinutelyTrendGroupByProduct(query);
                break;
            default:
                costs = deliveryContrastiveAnalysisMapper.costTrendGroupByProduct(query);
                break;
        }
        //货币单元转换
        for (DailyRecord<String, BigDecimal> cost : costs) {
            cost.setValue(CurrencyUtils.fenToYuan(cost.getValue()));
        }
        //转化为趋势图
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), costs, new BigDecimal(0), false, isDefaultChart);
        return productChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    @Override
    public TrendChart<String, Double> ctrTrendGroupByProduct(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Product);
        if (isDefaultChart) {
            addProductDefaultFilter(query);
        }
        List<? extends DailyRecord<String, Double>> ctrList;
        switch (query.getPeriod()) {
            case Day:
                ctrList = deliveryContrastiveAnalysisMapper.ctrTrendGroupByProduct(query);
                break;
            case Week:
                ctrList = deliveryContrastiveAnalysisMapper.ctrWeeklyTrendGroupByProduct(query);
                break;
            case Month:
                ctrList = deliveryContrastiveAnalysisMapper.ctrMonthlyTrendGroupByProduct(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, Integer>> clicks = deliveryContrastiveAnalysisMapper.clickMinutelyTrendGroupByProduct(query);
                List<DailyRecord<String, Integer>> exposures = deliveryContrastiveAnalysisMapper.exposureMinutelyTrendGroupByProduct(query);
                TrendChart<String, Integer> clickChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), clicks, 0, true, isDefaultChart);
                TrendChart<String, Integer> exposureChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), exposures, 0, true, isDefaultChart);
                return TrendChartUtils.toPeriodCtrTrendChart(clickChart, exposureChart);
            default:
                ctrList = deliveryContrastiveAnalysisMapper.ctrTrendGroupByProduct(query);
        }
        TrendChart<String, Double> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), ctrList, 0.0, true, isDefaultChart);
        return productChartCompletion(query, trendChart, 0.0);
    }

    /**
     * 下载率日趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, Double> downloadRateTrendGroupByProduct(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Product);
        if (isDefaultChart) {
            addProductDefaultFilter(query);
        }

        List<? extends DailyRecord<String, Double>> downloadRates;
        switch (query.getPeriod()) {
            case Day:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateTrendGroupByProduct(query);
                break;
            case Week:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateWeeklyTrendGroupByProduct(query);
                break;
            case Month:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateMonthlyTrendGroupByProduct(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, Integer>> downloadTimes = deliveryContrastiveAnalysisMapper.downloadTimesMinutelyTrendGroupByProduct(query);
                List<DailyRecord<String, Integer>> reaches = deliveryContrastiveAnalysisMapper.reachesMinutelyTrendGroupByProduct(query);
                TrendChart<String, Integer> downloadTimesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), downloadTimes, 0, true, isDefaultChart);
                TrendChart<String, Integer> reachesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), reaches, 0, true, isDefaultChart);
                return TrendChartUtils.toPeriodRateTrendChart(downloadTimesChart, reachesChart);
            default:
                downloadRates = deliveryContrastiveAnalysisMapper.downloadRateTrendGroupByProduct(query);
        }
        TrendChart<String, Double> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), downloadRates, 0.0, true, isDefaultChart);
        return productChartCompletion(query, trendChart, 0.0);
    }

    /**
     * 注册数趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, Integer> registrationTrendGroupByProduct(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Product);
        if (isDefaultChart) {
            addProductDefaultFilter(query);
        }
        List<? extends DailyRecord<String, Integer>> registrations;
        switch (query.getPeriod()) {
            case Day:
                registrations = deliveryContrastiveAnalysisMapper.registrationTrendGroupByProduct(query);
                break;
            case Week:
                registrations = deliveryContrastiveAnalysisMapper.registrationWeeklyTrendGroupByProduct(query);
                break;
            case Month:
                registrations = deliveryContrastiveAnalysisMapper.registrationMonthlyTrendGroupByProduct(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                registrations = deliveryContrastiveAnalysisMapper.registrationMinutelyTrendGroupByProduct(query);
                break;
            default:
                registrations = deliveryContrastiveAnalysisMapper.registrationTrendGroupByProduct(query);
        }
        TrendChart<String, Integer> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), registrations, 0, false, isDefaultChart);
        return productChartCompletion(query, trendChart, 0);
    }

    /**
     * CPA趋势图，按媒体账号分组
     *
     * @param query
     * @return
     */
    @Override
    public TrendChart<String, BigDecimal> cpaTrendGroupByProduct(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Product);
        if (isDefaultChart) {
            addProductDefaultFilter(query);
        }
        List<? extends DailyRecord<String, BigDecimal>> cpaList;
        switch (query.getPeriod()) {
            case Day:
                cpaList = deliveryContrastiveAnalysisMapper.cpaTrendGroupByProduct(query);
                break;
            case Week:
                cpaList = deliveryContrastiveAnalysisMapper.cpaWeeklyTrendGroupByProduct(query);
                break;
            case Month:
                cpaList = deliveryContrastiveAnalysisMapper.cpaMonthlyTrendGroupByProduct(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                List<DailyRecord<String, BigDecimal>> costs = deliveryContrastiveAnalysisMapper.costMinutelyTrendGroupByProduct(query);
                //货币单元转换
                for (DailyRecord<String, BigDecimal> cost : costs) {
                    cost.setValue(CurrencyUtils.fenToYuan(cost.getValue()));
                }
                List<DailyRecord<String, Integer>> registrations = deliveryContrastiveAnalysisMapper.registrationMinutelyTrendGroupByProduct(query);
                TrendChart<String, BigDecimal> downloadTimesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), costs, new BigDecimal(0), false, isDefaultChart);
                TrendChart<String, Integer> reachesChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), registrations, 0, false, isDefaultChart);
                return TrendChartUtils.toPeriodCpaTrendChart(downloadTimesChart, reachesChart);
            default:
                cpaList = deliveryContrastiveAnalysisMapper.cpaTrendGroupByProduct(query);
        }

        for (DailyRecord<String, BigDecimal> cpa : cpaList) {
            cpa.setValue(CurrencyUtils.fenToYuan(cpa.getValue()));
        }
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), cpaList, new BigDecimal(0), false, isDefaultChart);
        return productChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    @Override
    public TrendChart<String, BigDecimal> rechargeTrendGroupByProduct(DeliveryTrendQuery query) {
        boolean isDefaultChart = isDefaultChart(query, GroupBy.Product);
        if (isDefaultChart) {
            addProductDefaultFilter(query);
        }

        List<? extends DailyRecord<String, BigDecimal>> recharges;
        switch (query.getPeriod()) {
            case Day:
                recharges = deliveryContrastiveAnalysisMapper.rechargeTrendGroupByProduct(query);
                break;
            case Week:
                recharges = deliveryContrastiveAnalysisMapper.rechargeWeeklyTrendGroupByProduct(query);
                break;
            case Month:
                recharges = deliveryContrastiveAnalysisMapper.rechargeMonthlyTrendGroupByProduct(query);
                break;
            case Hour:
            case HalfAnHour:
            case TwentyMinutes:
            case TenMinutes:
                query.setIntervalInMinutes(query.getPeriod().getIntervalInMinutes());
                query.setEndDate(getEndTime(query.getStartDate()));
                recharges = deliveryContrastiveAnalysisMapper.rechargeMinutelyTrendGroupByProduct(query);
                break;
            default:
                recharges = deliveryContrastiveAnalysisMapper.rechargeTrendGroupByProduct(query);
        }

        for (DailyRecord<String, BigDecimal> recharge : recharges) {
            recharge.setValue(CurrencyUtils.fenToYuan(recharge.getValue()));
        }
        TrendChart<String, BigDecimal> trendChart = TrendChartUtils.toTrendChart(query.getStartDate(), query.getEndDate(), query.getPeriod(), recharges, new BigDecimal(0), false, isDefaultChart);
        return productChartCompletion(query, trendChart, BigDecimal.valueOf(0));
    }

    private enum GroupBy {
        Plan, MediumAccount, Medium, Product
    }

}
