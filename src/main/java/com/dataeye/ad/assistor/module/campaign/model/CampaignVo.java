package com.dataeye.ad.assistor.module.campaign.model;


import com.google.gson.annotations.Expose;

/**
 * 推广计划（广告活动）VO
 * @author luzhuyou 2017/06/15
 *
 */
public class CampaignVo {

	/** 企业账号uid */
	private int companyAccountUid;
	/** 活动ID（短链活动号）*/
	@Expose
	private String campaignId;
	/** 活动名称 */
	@Expose
	private String campaignName;
	/** 公司ID */
	@Expose
	private Integer companyId;
	/** 产品ID */
	@Expose
	private Integer productId;
	/** TrackingAppID */
	@Expose
	private String appId;
	/** 产品名称 */
	@Expose
	private String productName;
	/** 媒体ID */
	@Expose
	private Integer mediumId;
	/** 媒体名称 */
	@Expose
	private String mediumName;
	/** 媒体账号ID */
	@Expose
	private Integer mediumAccountId;
	/** 媒体账号 */
	@Expose
	private String mediumAccount;
	/** 系统类型：0-Others, 1-iOS, 2-Android */
	@Expose
	private Integer osType;
	/** 推广链接 */
	@Expose
	private String trackUrl;
	/** 下载地址（落地页URL） */
	@Expose
	private String downloadUrl;
	/** 广告参数 */
	@Expose
	private String thirdAdParams;
	/** 修改时间 */
	@Expose
	private String updateTime;
	//private Date updateTime;
	
	/**下载监测地址*/
	@Expose
	private String downloadMonitorUrl;
	
	/**到达监测地址*/
	@Expose
	private String arrivalMonitorUrl;
	
	/**Tracking数据接入类型：0-媒体对接，1-外链
	@Expose
	private String trackingAccessType;*/
	
	/**渠道id*/
	@Expose
	private Integer channelId;
	
	/**监测链接类型**/
	@Expose
	private String type;
	
	/**监测转换指标*/
	@Expose
	private String eventType;
	
	/**推广方式，0--落地页，1--应用宝，2--联盟推广**/
	@Expose
	private Integer appTreasure;
	
	private String createTime;
	
	private Integer status;
	
	public int getCompanyAccountUid() {
		return companyAccountUid;
	}
	public void setCompanyAccountUid(int companyAccountUid) {
		this.companyAccountUid = companyAccountUid;
	}
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getMediumId() {
		return mediumId;
	}
	public void setMediumId(Integer mediumId) {
		this.mediumId = mediumId;
	}
	public String getMediumName() {
		return mediumName;
	}
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}
	public Integer getMediumAccountId() {
		return mediumAccountId;
	}
	public void setMediumAccountId(Integer mediumAccountId) {
		this.mediumAccountId = mediumAccountId;
	}
	public String getMediumAccount() {
		return mediumAccount;
	}
	public void setMediumAccount(String mediumAccount) {
		this.mediumAccount = mediumAccount;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	public String getTrackUrl() {
		return trackUrl;
	}
	public void setTrackUrl(String trackUrl) {
		this.trackUrl = trackUrl;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getThirdAdParams() {
		return thirdAdParams;
	}
	public void setThirdAdParams(String thirdAdParams) {
		this.thirdAdParams = thirdAdParams;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	public String getDownloadMonitorUrl() {
		return downloadMonitorUrl;
	}
	public void setDownloadMonitorUrl(String downloadMonitorUrl) {
		this.downloadMonitorUrl = downloadMonitorUrl;
	}
	public String getArrivalMonitorUrl() {
		return arrivalMonitorUrl;
	}
	public void setArrivalMonitorUrl(String arrivalMonitorUrl) {
		this.arrivalMonitorUrl = arrivalMonitorUrl;
	}
	
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	public Integer getAppTreasure() {
		return appTreasure;
	}
	public void setAppTreasure(Integer appTreasure) {
		this.appTreasure = appTreasure;
	}
	
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "CampaignVo [companyAccountUid=" + companyAccountUid
				+ ", campaignId=" + campaignId + ", campaignName="
				+ campaignName + ", companyId=" + companyId + ", productId="
				+ productId + ", appId=" + appId + ", productName="
				+ productName + ", mediumId=" + mediumId + ", mediumName="
				+ mediumName + ", mediumAccountId=" + mediumAccountId
				+ ", mediumAccount=" + mediumAccount + ", osType=" + osType
				+ ", trackUrl=" + trackUrl + ", downloadUrl=" + downloadUrl
				+ ", thirdAdParams=" + thirdAdParams + ", updateTime="
				+ updateTime + ", downloadMonitorUrl=" + downloadMonitorUrl
				+ ", arrivalMonitorUrl=" + arrivalMonitorUrl + ", channelId="
				+ channelId + ", type=" + type + ", eventType=" + eventType
				+ ", appTreasure=" + appTreasure + ", createTime=" + createTime
				+ ", status=" + status + "]";
	}
}
