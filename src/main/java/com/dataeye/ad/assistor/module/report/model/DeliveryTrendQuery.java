package com.dataeye.ad.assistor.module.report.model;

import java.util.List;

/**
 * Created by huangzehai on 2017/4/27.
 */
public class DeliveryTrendQuery extends DateRangeQuery {

    /**
     * 产品ID列表.
     */
    private List<Integer> productIds;
    /**
     * 媒体ID列表.
     */
    private List<Integer> mediumIds;
    /**
     * 媒体账号ID列表.
     */
    private List<Integer> mediumAccountIds;
    /**
     * 计划ID列表.
     */
    private List<Integer> planIds;
    /**
     * 时段.
     */
    private Period period;

    /**
     * 时间间隔分钟数，仅供时段统计使用.
     */
    private Integer intervalInMinutes;

    public List<Integer> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public List<Integer> getMediumIds() {
        return mediumIds;
    }

    public void setMediumIds(List<Integer> mediumIds) {
        this.mediumIds = mediumIds;
    }

    public List<Integer> getMediumAccountIds() {
        return mediumAccountIds;
    }

    public void setMediumAccountIds(List<Integer> mediumAccountIds) {
        this.mediumAccountIds = mediumAccountIds;
    }

    public List<Integer> getPlanIds() {
        return planIds;
    }

    public void setPlanIds(List<Integer> planIds) {
        this.planIds = planIds;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Integer getIntervalInMinutes() {
        return intervalInMinutes;
    }

    public void setIntervalInMinutes(Integer intervalInMinutes) {
        this.intervalInMinutes = intervalInMinutes;
    }

    @Override
    public String toString() {
        return "DeliveryTrendQuery{" +
                ", productIds=" + productIds +
                ", mediumIds=" + mediumIds +
                ", mediumAccountIds=" + mediumAccountIds +
                ", planIds=" + planIds +
                ", period=" + period +
                ", intervalInMinutes=" + intervalInMinutes +
                "} " + super.toString();
    }
}
