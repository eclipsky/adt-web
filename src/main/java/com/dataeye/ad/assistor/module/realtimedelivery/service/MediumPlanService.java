package com.dataeye.ad.assistor.module.realtimedelivery.service;

import com.dataeye.ad.assistor.module.mediumaccount.model.Result;
import com.dataeye.ad.assistor.module.realtimedelivery.model.MediumPlan;

/**
 * Created by huangzehai on 2017/5/15.
 */
public interface MediumPlanService<T extends MediumPlan> {

    /**
     * 修改投放状态
     *
     * @param mediumPlan
     */
    Result execute(T mediumPlan);
}
