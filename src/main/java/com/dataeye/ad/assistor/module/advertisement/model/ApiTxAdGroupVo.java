package com.dataeye.ad.assistor.module.advertisement.model;

import java.util.Date;

import com.google.gson.annotations.Expose;

/**
 * 广告关联关系
 *
 * @author ldj
 */
public class ApiTxAdGroupVo {

    /**
     * 广告组ID
     **/
    private Integer adGroupId;
    /**
     * 推广计划ID
     **/
    private Integer planGroupId;
    /**
     * 公司ID
     **/
    private Integer companyId;
    /**
     * ADT账号ID
     **/
    private Integer systemAccountId;
    /**
     * 广告组名称
     */
    private String adGroupName;
    /**
     * 媒体ID
     **/
    private Integer mediumId;
    /**
     * 产品ID
     **/
    private Integer productId;
    /**
     * 媒体帐号ID
     **/
    private Integer mediumAccountId;
    /**
     * 定向ID
     **/
    private Integer targetingId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 媒体帐号名称
     **/
    private String mediumAccountName;
    /**
     * 媒体名称
     **/
    private String mediumName;
    /**
     * 产品名称
     **/
    private String productName;
    /**
     * 产品类型
     **/
    private Integer productType;

    private Integer mAccountId;

    private String accessToken;

    //创意规格 id，
    private Integer adcreativeTemplateId;

    private String siteSet;
    
    //出价
    private Integer dailyBudget;
    
    /**客户设置的状态，开关*/
    private Integer status;


    public Integer getPlanGroupId() {
        return planGroupId;
    }

    public void setPlanGroupId(Integer planGroupId) {
        this.planGroupId = planGroupId;
    }

    public Integer getAdGroupId() {
        return adGroupId;
    }

    public void setAdGroupId(Integer adGroupId) {
        this.adGroupId = adGroupId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getSystemAccountId() {
        return systemAccountId;
    }

    public void setSystemAccountId(Integer systemAccountId) {
        this.systemAccountId = systemAccountId;
    }

    public String getAdGroupName() {
        return adGroupName;
    }

    public void setAdGroupName(String adGroupName) {
        this.adGroupName = adGroupName;
    }

    public Integer getMediumId() {
        return mediumId;
    }

    public void setMediumId(Integer mediumId) {
        this.mediumId = mediumId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getMediumAccountId() {
        return mediumAccountId;
    }

    public void setMediumAccountId(Integer mediumAccountId) {
        this.mediumAccountId = mediumAccountId;
    }

    public Integer getTargetingId() {
        return targetingId;
    }

    public void setTargetingId(Integer targetingId) {
        this.targetingId = targetingId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getMediumAccountName() {
        return mediumAccountName;
    }

    public void setMediumAccountName(String mediumAccountName) {
        this.mediumAccountName = mediumAccountName;
    }

    public String getMediumName() {
        return mediumName;
    }

    public void setMediumName(String mediumName) {
        this.mediumName = mediumName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public Integer getmAccountId() {
        return mAccountId;
    }

    public void setmAccountId(Integer mAccountId) {
        this.mAccountId = mAccountId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getAdcreativeTemplateId() {
        return adcreativeTemplateId;
    }

    public void setAdcreativeTemplateId(Integer adcreativeTemplateId) {
        this.adcreativeTemplateId = adcreativeTemplateId;
    }

    public String getSiteSet() {
        return siteSet;
    }

    public void setSiteSet(String siteSet) {
        this.siteSet = siteSet;
    }
    

    public Integer getDailyBudget() {
		return dailyBudget;
	}

	public void setDailyBudget(Integer dailyBudget) {
		this.dailyBudget = dailyBudget;
	}
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ApiTxAdGroupVo [adGroupId=" + adGroupId + ", planGroupId="
				+ planGroupId + ", companyId=" + companyId
				+ ", systemAccountId=" + systemAccountId + ", adGroupName="
				+ adGroupName + ", mediumId=" + mediumId + ", productId="
				+ productId + ", mediumAccountId=" + mediumAccountId
				+ ", targetingId=" + targetingId + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", mediumAccountName="
				+ mediumAccountName + ", mediumName=" + mediumName
				+ ", productName=" + productName + ", productType="
				+ productType + ", mAccountId=" + mAccountId + ", accessToken="
				+ accessToken + ", adcreativeTemplateId="
				+ adcreativeTemplateId + ", siteSet=" + siteSet
				+ ", dailyBudget=" + dailyBudget + ", status=" + status + "]";
	}


}
