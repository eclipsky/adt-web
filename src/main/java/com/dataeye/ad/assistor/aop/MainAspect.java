package com.dataeye.ad.assistor.aop;

import java.text.MessageFormat;
import java.util.Collections;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.config.ResourceHandler;
import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.constant.HttpStatusCode;
import com.dataeye.ad.assistor.constant.StatusCode;
import com.dataeye.ad.assistor.context.DEContext;
import com.dataeye.ad.assistor.context.DEParameter;
import com.dataeye.ad.assistor.context.DEResponse;
import com.dataeye.ad.assistor.exception.ClientException;
import com.dataeye.ad.assistor.exception.ServerException;
import com.dataeye.ad.assistor.privilege.PrivilegeHandler;
import com.dataeye.ad.assistor.util.DateUtils;
import com.dataeye.ad.assistor.util.LogUtils;
import com.dataeye.ad.assistor.util.ServerUtils;

/**
 * <pre>
 * 切面逻辑,环绕每个接口方法
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:38:28
 */
@Aspect
@Service
public class MainAspect {

    /**
     * 给接口请求加切面
     *
     * @param jp
     * @throws Throwable
     * @author Stran
     * @since 2016.09.30 16:38:20
     */
    @Around(value = "execution(public java.lang.Object com.dataeye.ad.assistor.module..*.*(javax.servlet.http"
            + ".HttpServletRequest,javax.servlet.http.HttpServletResponse) throws java.lang.Exception)")
    public void aroundMethod(ProceedingJoinPoint jp) throws Throwable {
        long startTime = DateUtils.winTimestamp();
        DEContext context = new DEContext(jp);
        DEResponse deResponse = context.getDeResponse();

        try {
        	// 先检查权限
			boolean hasPermission = PrivilegeHandler.checkPrivilege(context);
			if(hasPermission) {
				Object obj = jp.proceed();
				obj = obj == null ? Collections.EMPTY_LIST : obj;
				deResponse.setContent(obj);
			} else {
				deResponse.setContent(Collections.EMPTY_LIST);
			}
        } catch (ClientException clientException) {
            deResponse.setStatusCode(clientException.getStatusCode());
            String content = (clientException.getCustomMessage() == null ? "" : clientException.getCustomMessage()) +
            		MessageFormat.format(ResourceHandler.getProperties(deResponse.getStatusCode() + "", Constant.LANG.ZH), clientException.getPropertiesPlaceHolderMessage());
            deResponse.setContent(content);
        } catch (ServerException serverException) {
            deResponse.setStatusCode(serverException.getStatusCode());
            String content = (serverException.getCustomMessage() == null ? "" : serverException.getCustomMessage()) +
            		MessageFormat.format(ResourceHandler.getProperties(deResponse.getStatusCode() + "", Constant.LANG.ZH), serverException.getPropertiesPlaceHolderMessage());
            deResponse.setContent(content);
        } catch (Throwable t) {
            t.printStackTrace();
            deResponse.setHttpStatusCode(HttpStatusCode.SERVER_EXCEPTION);
            deResponse.setStatusCode(StatusCode.COMM_SEVER_ERROR);
        } finally {// 将DEResponse对象返回
            String responseJson = deResponse.toJson();
            ServerUtils.writeToResponse(context.getResponse(), deResponse.getHttpStatusCode(), responseJson);
            DEParameter p = context.getDeParameter();
            LogUtils.logRequest(context.getID(), p.getDoName(), context.getClassName() + "." + context.getMethod(),
                    p.getUrl(), responseJson, DateUtils.winTimestamp() - startTime);
        }
    }

    /**
     * 给自定义响应的接口构建DEContext
     *
     * @param jp
     * @throws Throwable
     */
    @Around(value = "execution(public void com.dataeye.ad.assistor.module..*.*(javax.servlet.http"
            + ".HttpServletRequest,javax.servlet.http.HttpServletResponse) throws java.lang.Exception)")
    public void buildContext(ProceedingJoinPoint jp) throws Throwable {
        new DEContext(jp);
        jp.proceed();
    }

}