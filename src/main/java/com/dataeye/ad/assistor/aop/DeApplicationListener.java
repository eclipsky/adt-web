package com.dataeye.ad.assistor.aop;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import com.dataeye.ad.assistor.config.ConfigHandler;
import com.dataeye.ad.assistor.config.FileWatcher;
import com.dataeye.ad.assistor.config.ResourceHandler;
import com.dataeye.ad.assistor.privilege.PrivilegeHandler;
import com.dataeye.ad.assistor.schedule.Jobs;

/**
 * <pre>
 * 容器启动以后执行
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.10.10 15:38:41
 */
@Service
public class DeApplicationListener implements ApplicationListener {
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        // 容器启动完成以后，开启所有的定时任务
        if (event instanceof ContextRefreshedEvent) {

            ContextRefreshedEvent refreshedEvent = (ContextRefreshedEvent) event;
            // root application context
            if (refreshedEvent.getApplicationContext().getParent() != null) {
                try {
                	PrivilegeHandler.start2ScanPrivileges();
                    // 开启Quartz定时任务
                    Jobs.start();
                    // 初始化语言文件
                    ResourceHandler.init();
                    // 初始化配置文件
                    ConfigHandler.init();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        // 容器关闭的时候，把相关的定时任务也关闭
        if (event instanceof ContextClosedEvent) {
            try {
                // 定时任务
                Jobs.shutdown();
                FileWatcher.shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
