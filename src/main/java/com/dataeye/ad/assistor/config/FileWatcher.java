package com.dataeye.ad.assistor.config;

import com.dataeye.ad.assistor.constant.Constant;
import com.dataeye.ad.assistor.util.FileUtils;

import java.nio.file.*;
import java.util.Vector;

/**
 * <pre>
 * 文件监控器
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.10.10 15:41:37
 */
public class FileWatcher {
    /**
     * 是否需要关闭
     */
    private static volatile boolean stop;
    /**
     * 保存当前线程
     */
    private static Thread currentThread;

    private final static Runnable monitor = new Runnable() {
        @Override
        public void run() {
            currentThread = Thread.currentThread();
            try {
                WatchService watchService = FileSystems.getDefault().newWatchService();
                // 监控classpath:resources目录
                Path resourcePath = Paths.get(FileUtils.getPathOfClasspathResource(Constant.Resource.DIR));
                resourcePath.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY,
                        StandardWatchEventKinds.ENTRY_CREATE);
                // 监控classpath:config目录
                Path configPath = Paths.get(FileUtils.getPathOfClasspathResource(Constant.Config.DIR));
                configPath.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY,
                        StandardWatchEventKinds.ENTRY_CREATE);
                while (!stop) {
                    WatchKey key = watchService.take();
                    for (WatchEvent<?> watchEvent : key.pollEvents()) {
                        Path file = (Path) watchEvent.context();
                        notice(file.toFile().getName());
                    }
                    key.reset();
                }
            } catch (Exception t) {
                t.printStackTrace();
            }
        }

    };

    private static Vector<FileUpdate> noticeList = new Vector<FileUpdate>();

    public static void registe(FileUpdate fileUpdate) {
        noticeList.add(fileUpdate);
    }

    private synchronized static void notice(String fileName) {
        if (noticeList != null) {
            for (FileUpdate fileUpdate : noticeList) {
                fileUpdate.fileChanged(fileName);
            }
        }
    }

    /**
     * 开启当前线程
     */
    static {
        try {
            Thread thread = new Thread(monitor);
            thread.start();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * 关闭当前线程
     *
     * @author Stran
     * @since 2016.10.10 15:41:30
     */
    public static void shutdown() {
        stop = true;
        if (currentThread != null) {
            currentThread.interrupt();
        }

    }
}
