package com.dataeye.ad.assistor.config;

import com.dataeye.ad.assistor.constant.Constant;
import org.apache.commons.lang.StringUtils;

import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <pre>
 * 配置文件处理器
 * @author Stran          <br>
 * @date 2016年2月25日 下午1:43:34
 * <br>
 */
public class ConfigHandler {
    /**
     * 存放tracker配置文件的键值
     */
    public static Map<String, String> TRACKER;

    /**
     * <pre>
     * 初始化加载配置文件
     * @author Stran<br>
     * @date 2016年2月25日 下午1:43:34
     * <br>
     */
    public static void init() {
        reloadConfig();
        registe();
    }

    /**
     * <pre>
     * 处理文件改变
     * @author Stran<br>
     * @date 2016年2月25日 下午1:43:34
     * <br>
     */
    public static void registe() {
        FileWatcher.registe(new FileUpdate() {
            @Override
            public void fileChanged(String fileName) {
                if (Constant.Config.CONFIG_PROPERTIES.endsWith(fileName)) {
                    reloadConfig();
                }
            }
        });
    }

    /**
     * <pre>
     * 加载中文语言文件
     * @author Stran<br>
     * @date 2016年2月25日 下午1:43:34
     * <br>
     */
    private static synchronized void reloadConfig() {
        System.err.println("reload " + Constant.Config.CONFIG_PROPERTIES + " >>>>>>>>>");
        Map<String, String> config = new ConcurrentHashMap<>();
        Properties properties = new Properties();
        try {
            properties.load(ConfigHandler.class.getClassLoader().getResourceAsStream(Constant.Config.CONFIG_PROPERTIES));
            Enumeration<?> kvs = properties.propertyNames(); // 得到配置文件的名字
            while (kvs.hasMoreElements()) {
                String k = (String) kvs.nextElement();
                String v = properties.getProperty(k);
                config.put(k, v);
            }
            TRACKER = config;
            System.err.println("reload " + Constant.Config.CONFIG_PROPERTIES + " done, size=" + config.size());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    /**
     * <pre>
     * 根据key取对应的值
     * @param key
     * @return
     * @author Stran<br>
     * @date 2016年2月25日 下午1:43:34
     * <br>
     */
    public static String getProperty(String key) {
        return TRACKER.get(key);
    }

    public static String getProperty(String key, String defaultValue) {
        if (StringUtils.isEmpty(TRACKER.get(key)))
            return defaultValue;
        return TRACKER.get(key);
    }

    /**
     * <pre>
     * 根据key取对应的值
     * @param key
     * @return
     * @author Stran<br>
     * @date 2016年2月25日 下午1:43:34
     * <br>
     */
    public static int getPropertyInt(String key) {
        try {
            return Integer.parseInt(TRACKER.get(key));
        } catch (Exception e) {
        }

        return Constant.ServerCfg.INVALID_NUMBER;
    }

}

