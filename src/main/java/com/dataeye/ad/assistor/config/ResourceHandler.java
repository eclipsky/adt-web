package com.dataeye.ad.assistor.config;

import com.dataeye.ad.assistor.constant.Constant;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * <pre>
 * 语言文件处理器
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:48:08
 */
public class ResourceHandler {
	/**
	 * 存放中文的键值
	 */
	private static Map<String, String> ZH = new HashMap<>();
	/**
	 * 存放英文的键值
	 */
	private static Map<String, String> EN = new HashMap<>();
	/**
	 * 存放繁体的键值
	 */
	private static Map<String, String> FT = new HashMap<>();

	/**
	 * 初始化语言资源文件
	 *
	 * @author Stran
	 * @since 2016.09.30 16:48:14
	 */
	public static void init() {
		reload(Constant.Resource.ZH_FILE, ZH);
		reload(Constant.Resource.EN_FILE, EN);
		reload(Constant.Resource.FT_FILE, FT);
		registe();
	}

	/**
	 * 处理文件改变
	 *
	 * @author Stran
	 * @since 2016.09.30 16:48:20
	 */
	public static void registe() {
		FileWatcher.registe(new FileUpdate() {
			@Override
			public void fileChanged(String fileName) {
				if (Constant.Resource.ZH_FILE.endsWith(fileName)) {
					reload(Constant.Resource.ZH_FILE, ZH);
				}
				if (Constant.Resource.EN_FILE.endsWith(fileName)) {
					reload(Constant.Resource.EN_FILE, EN);
				}
				if (Constant.Resource.FT_FILE.endsWith(fileName)) {
					reload(Constant.Resource.FT_FILE, FT);
				}
			}
		});
	}

	/**
	 * 加载语言文件
	 *
	 * @param fileName
	 * @param res
	 * @author Stran
	 * @since 2016.09.30 16:48:58
	 */
	private static synchronized void reload(String fileName, Map<String, String> res) {
		System.err.println("reload " + fileName + " >>>>>>>>>");
		try {
			Properties properties = new Properties();
			properties.load(ResourceHandler.class.getClassLoader().getResourceAsStream(fileName));
			Enumeration<?> kvs = properties.propertyNames();
			while (kvs.hasMoreElements()) {
				String k = (String) kvs.nextElement();
				String v = properties.getProperty(k);
				res.put(k, v);
			}
			System.err.println("reload " + fileName + " done,size=" + res.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 根据key取对应语言的属性
	 *
	 * @param key
	 * @param lang
	 * @return string
	 * @author Stran
	 * @since 2016.09.30 16:49:13
	 */
	public static String getProperties(String key, String lang) {
		if (Constant.LANG.ZH.equals(lang)) {
			return ZH.get(key);
		}
		if (Constant.LANG.EN.equals(lang)) {
			return EN.get(key);
		}
		if (Constant.LANG.FT.equals(lang)) {
			return FT.get(key);
		}
		return null;
	}

	public static String getProperties(String key) {
		return ZH.get(key);
	}

	public static String getProperties(int key) {
		return ZH.get(key + "");
	}

	/**
	 * 根据key1, key2取对应语言的属性,并连接在一块
	 *
	 * @param key1
	 * @param key2
	 * @param lang
	 * @return string
	 * @author Stran
	 * @since 2016.09.30 16:49:19
	 */
	public static String getProperties(String key1, String key2, String lang) {
		if (Constant.LANG.ZH.equals(lang)) {
			return ZH.get(key1) + ZH.get(key2);
		}
		if (Constant.LANG.EN.equals(lang)) {
			return EN.get(key1) + " " + ZH.get(key2);
		}
		if (Constant.LANG.FT.equals(lang)) {
			return FT.get(key1) + ZH.get(key2);
		}
		return null;
	}
}
