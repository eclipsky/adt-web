package com.dataeye.ad.assistor.exception;


/**
 * <pre>
 * 自定义异常父类
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:49:50
 */
public abstract class AbstractDataEyeException extends RuntimeException {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	/** 描述一个异常的错误码 */
	private int statusCode;
	/** 描述自定义的异常信息 */
	private String customMessage;
	/** 
	 * Properties资源文件动态参数占位符填充信息 add by luzhuyou 2017/03/10 
	 */
	private Object[] propertiesPlaceHolderMessage;

	public AbstractDataEyeException(int statusCode) {
		this.statusCode = statusCode;
	}
	
	/**
	 * @param statusCode 异常状态码
	 * @param propertiesPlaceHolderMessage Properties资源文件动态参数占位符填充信息 
	 * @author luzhuyou 2017/03/10 
	 */
	public AbstractDataEyeException(int statusCode, Object[] propertiesPlaceHolderMessage) {
		this.statusCode = statusCode;
		this.propertiesPlaceHolderMessage = propertiesPlaceHolderMessage;
	}

	public AbstractDataEyeException(int statusCode, String message) {
		this.statusCode = statusCode;
		this.customMessage = message;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getCustomMessage() {
		return customMessage;
	}

	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}

	public Object[] getPropertiesPlaceHolderMessage() {
		return propertiesPlaceHolderMessage;
	}

	public void setPropertiesPlaceHolderMessage(
			Object[] propertiesPlaceHolderMessage) {
		this.propertiesPlaceHolderMessage = propertiesPlaceHolderMessage;
	}

}