package com.dataeye.ad.assistor.exception;

/**
 * <pre>
 * 抽象的服务端异常
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:50:24
 */
public class ServerException extends AbstractDataEyeException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public ServerException(int statusCode) {
		super(statusCode);
	}

	public ServerException(int statusCode, String message) {
		super(statusCode, message);
	}
	
	/**
	 * @param statusCode 异常状态码
	 * @param propertiesPlaceHolderMessage Properties资源文件动态参数占位符填充信息 
	 * @author luzhuyou 2017/03/10 
	 */
	public ServerException(int statusCode, Object[] propertiesPlaceHolderMessage) {
		super(statusCode, propertiesPlaceHolderMessage);
	}

}
