package com.dataeye.ad.assistor.exception;

/**
 * <pre>
 * 通用的serverexception
 * @author Ivan          <br>
 * @date 2015年3月3日 下午5:44:33
 * <br>
 *
 */
public class GeneralServerException extends ServerException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public GeneralServerException(int statusCode) {
		super(statusCode);
	}

	public GeneralServerException(int statusCode, String message) {
		super(statusCode, message);
	}
	
	/**
	 * @param statusCode 异常状态码
	 * @param propertiesPlaceHolderMessage Properties资源文件动态参数占位符填充信息 
	 * @author luzhuyou 2017/03/10 
	 */
	public GeneralServerException(int statusCode, Object[] propertiesPlaceHolderMessage) {
		super(statusCode, propertiesPlaceHolderMessage);
	}
}
