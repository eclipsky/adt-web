package com.dataeye.ad.assistor.exception;

/**
 * <pre>
 * 客户端异常的父类
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:49:56
 */
public class ClientException extends AbstractDataEyeException {
    /**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public ClientException(int statusCode) {
        super(statusCode);
    }

    public ClientException(int statusCode, String message) {
        super(statusCode, message);
    }

    /**
	 * @param statusCode 异常状态码
	 * @param propertiesPlaceHolderMessage Properties资源文件动态参数占位符填充信息 
	 * @author luzhuyou 2017/03/10 
	 */
	public ClientException(int statusCode, Object[] propertiesPlaceHolderMessage) {
		super(statusCode, propertiesPlaceHolderMessage);
	}
	
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}