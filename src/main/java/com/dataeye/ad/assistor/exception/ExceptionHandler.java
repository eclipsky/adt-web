package com.dataeye.ad.assistor.exception;



/**
 * <pre>
 * 异常处理器
 * @author Stran <br>
 * @version 1.0 <br>
 * @since 2016.09.30 16:50:02
 */
public class ExceptionHandler {

    /**
     * 抛出参数错误的异常
     *
     * @param statusCode
     * @throws ClientException
     * @author Stran
     * @since 2016.09.30 16:50:05
     */
    public static void throwParameterException(int statusCode) throws ClientException {
        throw new ClientException(statusCode);
    }

    public static void throwParameterException(int statusCode, String message) throws ClientException {
        throw new ClientException(statusCode, message);
    }

    /**
	 * 
	 * <pre>
	 * 服务端其他异常
	 *  @param statusCode
	 *  @throws GeneralServerException  
	 *  @author Ivan<br>
	 *  @date 2015年3月8日 下午3:32:44
	 * <br>
	 */
	 public static void throwGeneralServerException(int statusCode) throws GeneralServerException {
		 throw new GeneralServerException(statusCode);
	 }

	/**
	 * 
	 * <pre>
	 * 服务端其他异常
	 * @param statusCode
	 * @param message
	 * @throws GeneralServerException  
	 * @author Ivan<br>
	 * @date 2015年3月8日 下午3:32:24
	 * <br>
	 */
	public static void throwGeneralServerException(int statusCode, String message) throws GeneralServerException {
		throw new GeneralServerException(statusCode, message);
	}
	
	/**
	 * 
	 * <pre>
	 * 服务端其他异常
	 * @param statusCode 异常状态码
	 * @param propertiesPlaceHolderMessage Properties资源文件动态参数占位符填充信息 
	 * @throws GeneralServerException  
	 * @author luzhuyou 2017/03/10 
	 */
	public static void throwGeneralServerException(int statusCode, Object[] propertiesPlaceHolderMessage) throws GeneralServerException {
		throw new GeneralServerException(statusCode, propertiesPlaceHolderMessage);
	}

    /**
     * <pre>
     * 抛出权限错误的异常
     *
     * @param statusCode
     * @throws ClientException
     * @author Ivan<br>
     * @date 2015年4月3日 下午3:35:20 <br>
     */
    public static void throwPermissionException(int statusCode) throws ClientException {
        throw new ClientException(statusCode);
    }

    /**
     * 抛出数据库错误的异常
     *
     * @param statusCode
     * @throws ServerException
     * @author Stran
     * @since 2016.09.30 16:50:14
     */
    public static void throwDatabaseException(int statusCode) throws ServerException {
        throw new ServerException(statusCode, "");
    }

    public static void throwDatabaseException(int statusCode, Throwable th) throws ServerException {
        throw new ServerException(statusCode, "");
    }

    public static void throwDatabaseException(int statusCode, String message) throws ServerException {
        throw new ServerException(statusCode, message);
    }

    /**
     * 抛出类错误异常
     *
     * @param statusCode
     * @param message
     * @throws ServerException
     * @author Stran
     * @since 2016.09.30 16:50:18
     */
    public static void throwClassException(int statusCode, String message) throws ServerException {
        throw new ServerException(statusCode, message);
    }

    public static void throwHttpClientException(int statusCode, Throwable th) throws ServerException {
        throw new ServerException(statusCode, th.getMessage());
    }

    public static void throwHttpClientException(int statusCode, String message) throws ServerException {
        throw new ServerException(statusCode, message);
    }

    /**
	 * 抛出邮件发送异常
	 * 
	 * <pre>
	 * @param statusCode
	 * @throws MailSendException
	 * @author Hayden<br>
	 * @date 2015年3月4日 上午10:40:11
	 * <br>
	 */
	public static void throwMailSendException(int statusCode, String message) throws MailSendException {
		throw new MailSendException(statusCode, message);
	}
}
