package com.dataeye.ad.assistor.exception;

/**
 * <pre>
 * 客户端传递的参数有错误
 * @author Ivan          <br>
 * @date 2015年3月3日 下午5:34:51
 * <br>
 *
 */
public class ParameterException extends ClientException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public ParameterException(int statusCode) {
		super(statusCode);
	}

	/**
	 * @param statusCode 异常状态码
	 * @param propertiesPlaceHolderMessage Properties资源文件动态参数占位符填充信息 
	 * @author luzhuyou 2017/03/10 
	 */
	public ParameterException(int statusCode, Object[] propertiesPlaceHolderMessage) {
		super(statusCode, propertiesPlaceHolderMessage);
	}
}
