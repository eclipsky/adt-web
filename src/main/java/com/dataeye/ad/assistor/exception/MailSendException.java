package com.dataeye.ad.assistor.exception;

/**
 * 邮件发送异常
 * 
 * <pre>
 * @author Hayden<br>
 * @date 2015年3月4日 上午10:39:34
 * <br>
 */
public class MailSendException extends ServerException {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public MailSendException(int statusCode) {
		super(statusCode);
	}

	public MailSendException(int statusCode, String message) {
		super(statusCode, message);
	}
	
	/**
	 * @param statusCode 异常状态码
	 * @param propertiesPlaceHolderMessage Properties资源文件动态参数占位符填充信息 
	 * @author luzhuyou 2017/03/10 
	 */
	public MailSendException(int statusCode, Object[] propertiesPlaceHolderMessage) {
		super(statusCode, propertiesPlaceHolderMessage);
	}
}
