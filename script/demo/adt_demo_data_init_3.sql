-- 表数据梳理：
-- ac_product --陕西产品信息
-- ac_medium_account	-- 筛选优点下的两个媒体账号
	-- ac_medium_account_relation 根据媒体账号获取对应关联关系
	-- ac_medium_account_stat_day 根据媒体账号获取账户余额
-- ac_medium_discount_rate	--根据媒体账号获取对应媒体折扣率
-- ad_plan	-- 获取优点两个指定媒体账号对应的所有计划
-- ad_plan_stat_day	--获取优点两个指定媒体账号对应的最近2个月计划统计数据
-- ad_plan_stat_realtime	--获取优点两个指定媒体账号对应的最近1周计划实时统计数据
-- ad_plan_relation -- 获取如上计划对应的关联规则
-- ad_page	--获取优点下所有落地页
-- ad_page_stat_day	-- 获取有点下最近2个月所有落地页按天统计数据
-- ad_page_stat_realtime	-- 获取最近1周所有落地页实时统计数据
-- ad_pkg	-- 获取优点所有包数据
	-- ad_pkg_relation 获取产品对应包的关联关系数据
-- ad_pkg_stat_day	--获取优点最近2个月所有包统计数据
-- ad_pkg_stat_realtime	--获取优点最近1周所有包实时统计数据
-- ad_pay_stat
-- ad_payback_day	--获取优点最近2个月的数据
-- ad_payback_month		-- 获取优点最近两个月的数据
-- ac_medium_custom --自定义媒体

 待选公司：company_id = 14
 待选媒体及账号：今日头条（2:279,337,338,295），智汇推（3:290,375,493），UC头条（9:371），爱奇艺（10:345,363,364），网易易效（12:491），广点通（13:432），百度信息流推广（14:383,397,438,488）， 新浪扶翼（32:489,492）
 待选产品：创世仙缘（52）,权御三国-安卓（81）,君临城下-安卓（82）,三国群英志-安卓（83）,全民仙逆-ios（84）,三国志大皇帝-ios（85），军令-ios（190）
 ADT-Demo待保存数据产品：ADT-Demo-产品4（52->54），ADT-Demo-产品5（81->56），ADT-Demo-产品6（82->86），ADT-Demo-产品7（83->89），ADT-Demo-产品8（84->90）
 						，ADT-Demo-产品9（85->91），ADT-Demo-产品10（190->92）


-- dump数据库 de_adt 中所有表的结构到 de_adt.sql 文件中
mysqldump -udc_crawler -pcrawler -h192.168.1.215 -P3306 -d de_adt > de_adt.sql
-- dump除指定table外所有表中的数据追加到 de_adt.sql 文件中
mysqldump -udc_crawler -pcrawler -h192.168.1.215 -P3306 -t --ignore-table=de_adt.ac_medium_account_stat_realtime --ignore-table=de_adt.ad_page_stat_realtime --ignore-table=de_adt.ad_pkg_stat_realtime --ignore-table=de_adt.ad_plan_stat_realtime --ignore-table=de_adt.ad_tracking_day --ignore-table=de_adt.ad_tracking_realtime  de_adt >> de_adt.sql
-- dump指定表中的数据
mysqldump -udc_crawler -pcrawler -h192.168.1.215 -P3306 -t de_adt ad_plan_stat_realtime > de_adt_plan_stat_realtime.sql

-- 将待选产品信息转换到ADT-Demo待保存数据产品信息中
update ac_product t1,ac_product t2 
set t1.app_id = t2.app_id, t1.os_type = t2.os_type, t1.parent_category_Id = t2.parent_category_Id, t1.children_category_Id = t2.children_category_Id
where t1.product_id = 54 and t2.product_id = 52;

update ac_product t1,ac_product t2 
set t1.app_id = t2.app_id, t1.os_type = t2.os_type, t1.parent_category_Id = t2.parent_category_Id, t1.children_category_Id = t2.children_category_Id
where t1.product_id = 56 and t2.product_id = 81;

update ac_product t1,ac_product t2 
set t1.app_id = t2.app_id, t1.os_type = t2.os_type, t1.parent_category_Id = t2.parent_category_Id, t1.children_category_Id = t2.children_category_Id
where t1.product_id = 86 and t2.product_id = 82;

update ac_product t1,ac_product t2 
set t1.app_id = t2.app_id, t1.os_type = t2.os_type, t1.parent_category_Id = t2.parent_category_Id, t1.children_category_Id = t2.children_category_Id
where t1.product_id = 89 and t2.product_id = 83;

update ac_product t1,ac_product t2 
set t1.os_type = t2.os_type, t1.parent_category_Id = t2.parent_category_Id, t1.children_category_Id = t2.children_category_Id
where t1.product_id = 90 and t2.product_id = 84;

update ac_product t1,ac_product t2 
set t1.os_type = t2.os_type, t1.parent_category_Id = t2.parent_category_Id, t1.children_category_Id = t2.children_category_Id
where t1.product_id = 91 and t2.product_id = 85;

update ac_product t1,ac_product t2 
set t1.os_type = t2.os_type, t1.parent_category_Id = t2.parent_category_Id, t1.children_category_Id = t2.children_category_Id
where t1.product_id = 92 and t2.product_id = 190;



update ac_product set product_name = 'Demo产品4' where product_id = 54;
update ac_product set product_name = 'Demo产品5' where product_id = 56;
update ac_product set product_name = 'Demo产品6' where product_id = 86;
update ac_product set product_name = 'Demo产品7' where product_id = 89;
update ac_product set product_name = 'Demo产品8' where product_id = 90;
update ac_product set product_name = 'Demo产品9' where product_id = 91;
update ac_product set product_name = 'Demo产品10' where product_id = 92;


-- ac_medium_account 用remark字段保留旧的medium_account_id (remark保存的是旧的medium_account_id)
delete from ac_medium_account where company_id = 1 and remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492);

INSERT IGNORE INTO ac_medium_account (company_id, medium_id, medium_account, password, medium_account_alias, login_status, cookie, status, skey, page_type, remark, create_time, update_time)
select 1 as company_id, t1.medium_id, concat('demo-',t1.medium_id,'-',t1.medium_account_id) medium_account, t1.password, '' medium_account_alias, 1 as login_status, t1.cookie, 1 as status, t1.skey, t1.page_type, t1.medium_account_id as remark, t1.create_time, t2.update_time 
from ac_medium_account t1 join ac_medium_account_relation t2 on t1.medium_account_id = t2.medium_account_id
where t2.product_id in(52,81,82,83,84,85,190) and t1.medium_account_id in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492);

-- ac_medium_account_relation 根据媒体账号获取对应关联关系 (remark保存的是旧的medium_account_id)
delete from ac_medium_account_relation where company_id = 1 and remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492);

INSERT IGNORE INTO ac_medium_account_relation (company_id, product_id, medium_id, medium_account_id, system_account_name_list, optimizer_system_account, remark, create_time, update_time) 
SELECT t1.company_id, (case t2.product_id when 52 then 54 when 81 then 56 when 82 then 86 when 83 then 89 when 84 then 90 when 85 then 91 else 92 end) as product_id, t1.medium_id, t1.medium_account_id, '' system_account_name_list, '' optimizer_system_account, t1.remark, t2.create_time, t2.update_time 
FROM ac_medium_account t1 join ac_medium_account_relation t2 on t1.remark = t2.medium_account_id
where t1.company_id = 1 and t2.product_id in(52,81,82,83,84,85,190) and t2.medium_account_id in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492);


-- ac_medium_account_stat_day 根据媒体账号获取账户余额 (remark保存的是旧的medium_account_id)
delete from ac_medium_account_stat_day where company_id = 1 and remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492);

insert ignore into ac_medium_account_stat_day (stat_date, medium_id, medium_account_id, company_id, balance, cost_today, remark, update_time) 
select t2.stat_date, t2.medium_id, t1.medium_account_id, t1.company_id, t2.balance, t2.cost_today, t1.remark, t2.update_time  from ac_medium_account t1
join ac_medium_account_stat_day t2 on t1.remark = t2.medium_account_id
where t1.company_id = 1 and t2.medium_account_id in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492);


-- ac_medium_discount_rate 用新的媒体账号remark字段保留旧的medium_account_id与媒体折扣率表的旧的medium_account_id关联，得到新的medium_account_id
delete from ac_medium_discount_rate where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492);

INSERT IGNORE INTO ac_medium_discount_rate (medium_account_id, discount_rate, effect_date, invalid_date, remark, create_time, update_time) 
SELECT t1.medium_account_id, t2.discount_rate, t2.effect_date, t2.invalid_date, t1.remark, t2.create_time, t2.update_time FROM ac_medium_account t1
inner join ac_medium_discount_rate t2 on t1.remark = t2.medium_account_id
where t1.company_id = 1 and t2.medium_account_id in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492);


-- ad_plan 用remark字段保留旧的plan_id
delete from ad_plan where company_id = 1 and medium_account_id in(select medium_account_id from ac_medium_account where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492));

INSERT IGNORE INTO ad_plan (plan_name, m_plan_id, group_id, group_name, plan_switch, plan_status, plan_bid, plan_bid_strategy, plan_extend, landing_page, medium_id, medium_account_id, company_id, os_type, status, remark, create_time, update_time)
SELECT concat('demo-plan-', t2.plan_id) as plan_name, t2.m_plan_id, t2.group_id, concat('demo-plan-group-', t2.group_id) as group_name, t2.plan_switch, t2.plan_status, t2.plan_bid, t2.plan_bid_strategy, t2.plan_extend, '' landing_page, t2.medium_id, t1.medium_account_id, 1 as company_id, t2.os_type, t2.status, t2.plan_id, t2.create_time, t2.update_time FROM ac_medium_account t1
INNER JOIN ad_plan t2 ON t1.remark = t2.medium_account_id
WHERE t1.company_id = 1 and t2.medium_account_id in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492);


-- ad_plan_stat_day 获取优点两个指定媒体账号对应的最近2个月计划统计数据
delete from ad_plan_stat_day where company_id = 1 and medium_account_id in(select medium_account_id from ac_medium_account where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492));

INSERT IGNORE INTO ad_plan_stat_day (stat_date, plan_id, medium_id, medium_account_id, company_id, os_type, page_id, pkg_id, ad_id, cost, show_num, click_num, bid, discount_rate, plan_source, update_time) 
SELECT t2.stat_date, t1.plan_id, t2.medium_id, t1.medium_account_id, t1.company_id, t2.os_type, t2.page_id, t2.pkg_id, t2.ad_id, t2.cost, t2.show_num, t2.click_num, t2.bid, t2.discount_rate, t2.plan_source, t2.update_time FROM ad_plan t1
INNER JOIN ad_plan_stat_day t2 ON t1.remark = t2.plan_id
WHERE t1.company_id = 1 and t2.page_id > 0 and t2.stat_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH) 
and t1.medium_account_id in(select medium_account_id from ac_medium_account where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492));


-- ad_plan_stat_realtime
delete from ad_plan_stat_realtime where company_id = 1 and medium_account_id in(select medium_account_id from ac_medium_account where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492));

INSERT IGNORE INTO ad_plan_stat_realtime (stat_time, plan_id, medium_id, medium_account_id, company_id, os_type, page_id, pkg_id, ad_id, cost, show_num, click_num, bid, discount_rate, update_time) 
SELECT t2.stat_time, t1.plan_id, t2.medium_id, t1.medium_account_id, t1.company_id, t2.os_type, t2.page_id, t2.pkg_id, t2.ad_id, t2.cost, t2.show_num, t2.click_num, t2.bid, t2.discount_rate, t2.update_time FROM ad_plan t1
INNER JOIN ad_plan_stat_realtime t2 ON t1.remark = t2.plan_id
WHERE t1.company_id = 1 and t2.stat_time >= DATE_ADD(CURDATE(), INTERVAL -2 DAY) 
and t1.medium_account_id in(select medium_account_id from ac_medium_account where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492));


-- ad_plan_relation（remark字段保留旧的plan_id）
delete from ad_plan_relation where company_id = 1 and plan_id in(
	select plan_id from ad_plan where company_id = 1 and medium_account_id in(select medium_account_id from ac_medium_account where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492))
);

INSERT IGNORE INTO ad_plan_relation (company_id, os_type, product_id, plan_id, page_id, pkg_id, remark, create_time, update_time)
SELECT t1.company_id, t2.os_type, (case t2.product_id when 52 then 54 when 81 then 56 when 82 then 86 when 83 then 89 when 84 then 90 when 85 then 91 else 92 end) as product_id, t1.plan_id, t2.page_id, t2.pkg_id, t1.remark, t2.create_time, t2.update_time FROM ad_plan t1
INNER JOIN ad_plan_relation t2 ON t1.remark = t2.plan_id 
WHERE t1.company_id = 1 and t1.medium_account_id in(select medium_account_id from ac_medium_account where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492));


-- ad_page 用remark字段保留旧的page_id
delete from ad_page where company_id = 1 and remark in(select distinct page_id from ad_plan_relation where company_id = 1 and product_id in(54,56,86,89,90,91,92));

INSERT IGNORE INTO ad_page (page_name, h5_app_id, h5_page_id, company_id, os_type, status, remark, create_time, update_time)
SELECT concat('demo-page-', t2.page_id) as page_name, t2.h5_app_id, t2.h5_page_id, t1.company_id, t2.os_type, t2.status, t1.page_id, t2.create_time, t2.update_time FROM ad_plan_relation t1 
INNER JOIN ad_page t2 ON t1.page_id = t2.page_id 
WHERE t1.company_id = 1 and t1.product_id in(54,56,86,89,90,91,92);


-- ad_page_stat_day 获取有点下最近2个月所有落地页按天统计数据
delete from ad_page_stat_day where company_id = 1 and page_id in (
	select distinct t1.page_id from ad_page t1 join ad_plan_relation t2 on t1.remark = t2.page_id where t1.company_id = 1 and t2.product_id in(54,56,86,89,90,91,92)
);

INSERT IGNORE INTO ad_page_stat_day (stat_date, page_id, company_id, os_type, total_click_num, unique_click_num, pv, uv, download_times, update_time)
SELECT t2.stat_date, t1.page_id, t1.company_id, t2.os_type, t2.total_click_num, t2.unique_click_num, t2.pv, t2.uv, t2.download_times, t2.update_time FROM ad_page t1
INNER JOIN ad_page_stat_day t2 ON t1.remark = t2.page_id
WHERE t1.company_id = 1 and t2.stat_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH) 
	and t2.page_id in(select distinct page_id from ad_plan_relation where company_id = 1 and product_id in(54,56,86,89,90,91,92))
	and t1.remark not in('channel', 'tracking');

-- ad_page_stat_realtime
delete from ad_page_stat_realtime where company_id = 1 and page_id in (
	select distinct t1.page_id from ad_page t1 join ad_plan_relation t2 on t1.remark = t2.page_id where t1.company_id = 1 and t2.product_id in(54,56,86,89,90,91,92)
);

INSERT IGNORE INTO ad_page_stat_realtime (stat_time, page_id, company_id, os_type, total_click_num, unique_click_num, pv, uv, download_times, update_time)
SELECT t2.stat_time, t1.page_id, t1.company_id, t2.os_type, t2.total_click_num, t2.unique_click_num, t2.pv, t2.uv, t2.download_times, now() FROM ad_page t1
INNER JOIN ad_page_stat_realtime t2 ON t1.remark = t2.page_id
WHERE t1.company_id = 1 and t2.stat_time >= DATE_ADD(CURDATE(), INTERVAL -2 DAY) 
	and t2.page_id in(select distinct page_id from ad_plan_relation where company_id = 1 and product_id in(54,56,86,89,90,91,92))
	and t1.remark not in('channel', 'tracking');


-- ad_pkg 用remark字段保留旧的pkg_id
delete from ad_pkg where company_id = 1 and remark in(select distinct pkg_id from ad_plan_relation where company_id = 1 and product_id in(54,56,86,89,90,91,92));

-- 不包含自然流量部分
INSERT IGNORE INTO ad_pkg (pkg_name, cp_app_id, cp_pkg_id, company_id, os_type, status, remark, create_time, update_time)
SELECT concat('demo-package-', t2.pkg_id) as pkg_name, t2.cp_app_id, t2.cp_pkg_id, t1.company_id, t2.os_type, t2.status, t1.pkg_id, t2.create_time, t2.update_time FROM ad_plan_relation t1
INNER JOIN ad_pkg t2 ON t1.pkg_id = t2.pkg_id
WHERE t1.company_id = 1 and t1.product_id in(54,56,86,89,90,91,92);

-- 包含自然流量部分
INSERT INTO ad_pkg (pkg_name, cp_app_id, cp_pkg_id, company_id, os_type, status, remark, create_time, update_time)
select t1.pkg_name, t1.cp_app_id, t1.cp_pkg_id, 1 as company_id, t1.os_type, t1.status, t1.pkg_id as remark, t1.create_time, t1.update_time 
from ad_pkg t1 
join ad_pkg_stat_day t2 on t1.pkg_id = t2.pkg_id
join ac_product t3 on t1.cp_app_id = t3.app_id
where t1.cp_pkg_id = 'nature'and t3.product_id in(52,81,82,83,84,85,190)
on duplicate key update pkg_name = values(pkg_name), remark = values(remark);

	
-- ad_pkg_relation 获取产品对应包的关联关系数据
delete from ad_pkg_relation where company_id = 1 and product_id in(54,56,86,89,90,91,92);

INSERT IGNORE INTO ad_pkg_relation (company_id, os_type, product_id, pkg_id, plan_id, create_time, update_time)
SELECT t2.company_id, t1.os_type, (case t1.product_id when 52 then 54 when 81 then 56 when 82 then 86 when 83 then 89 when 84 then 90 when 85 then 91 else 92 end) as product_id, t2.pkg_id, t1.plan_id, t1.create_time, t1.update_time 
FROM ad_pkg_relation t1 join ad_pkg t2 on t1.pkg_id = t2.remark 
where t2.company_id = 1 and t1.product_id in(52,81,82,83,84,85,190) and t2.remark not in('channel','tracking');


-- ad_pkg_stat_day
delete from ad_pkg_stat_day where company_id = 1 and pkg_id in (
	select distinct t1.pkg_id from ad_pkg t1 join ad_plan_relation t2 on t1.remark = t2.pkg_id where t1.company_id = 1 and t2.product_id in(54,56,86,89,90,91,92)
);

-- 非自然流量部分
INSERT IGNORE INTO ad_pkg_stat_day (stat_date, pkg_id, company_id, os_type, active_num, register_num, total_pay_times, total_pay_num, total_pay_amount, new_pay_times, new_pay_num, new_pay_amount, login_num, download_times, retain_d2, retain_d3, retain_d5, retain_d7, retain_d14, retain_d30, update_time)
SELECT t2.stat_date, t1.pkg_id, t1.company_id, t2.os_type, CEIL(t2.register_num*1.05) as active_num, t2.register_num, t2.total_pay_times, t2.total_pay_num, t2.total_pay_amount, t2.new_pay_times, t2.new_pay_num, t2.new_pay_amount, t2.login_num, t2.download_times, t2.retain_d2, t2.retain_d3, t2.retain_d5, t2.retain_d7, t2.retain_d14, t2.retain_d30, t2.update_time FROM ad_pkg t1
INNER JOIN ad_pkg_stat_day t2 ON t1.remark = t2.pkg_id
WHERE t1.company_id = 1 and t2.stat_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH) 
	and t2.pkg_id in(select distinct pkg_id from ad_plan_relation where company_id = 1 and product_id in(54,56,86,89,90,91,92))
	and t1.remark not in('channel', 'tracking');

-- 自然流量部分	
INSERT IGNORE INTO ad_pkg_stat_day (stat_date, pkg_id, company_id, os_type, active_num, register_num, total_pay_times, total_pay_num, total_pay_amount, new_pay_times, new_pay_num, new_pay_amount, login_num, download_times, retain_d2, retain_d3, retain_d5, retain_d7, retain_d14, retain_d30, update_time)
SELECT t2.stat_date, t1.pkg_id, t1.company_id, t2.os_type, CEIL(t2.register_num*1.05) as active_num, t2.register_num, t2.total_pay_times, t2.total_pay_num, t2.total_pay_amount, t2.new_pay_times, t2.new_pay_num, t2.new_pay_amount, t2.login_num, t2.download_times, t2.retain_d2, t2.retain_d3, t2.retain_d5, t2.retain_d7, t2.retain_d14, t2.retain_d30, t2.update_time FROM ad_pkg t1
INNER JOIN ad_pkg_stat_day t2 ON t1.remark = t2.pkg_id
WHERE t1.company_id = 1 and t2.stat_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH) 
	and t2.company_id = 14
	and t1.remark not in('channel', 'tracking')
	and t1.cp_pkg_id = 'nature';
	
-- ad_pkg_stat_realtime
delete from ad_pkg_stat_realtime where company_id = 1 and pkg_id in (
	select distinct t1.pkg_id from ad_pkg t1 join ad_plan_relation t2 on t1.remark = t2.pkg_id where t1.company_id = 1 and t2.product_id in(54,56,86,89,90,91,92)
);

INSERT IGNORE INTO ad_pkg_stat_realtime (stat_time, pkg_id, company_id, os_type, active_num, register_num, total_pay_times, total_pay_num, total_pay_amount, download_times, update_time)
SELECT t2.stat_time, t1.pkg_id, t1.company_id, t2.os_type, t2.active_num, CEIL(t2.register_num*1.05) as register_num, t2.total_pay_times, t2.total_pay_num, t2.total_pay_amount, t2.download_times, now() FROM ad_pkg t1
INNER JOIN ad_pkg_stat_realtime t2 ON t1.remark = t2.pkg_id
WHERE t1.company_id = 1 and t2.stat_time >= DATE_ADD(CURDATE(), INTERVAL -2 DAY)
	and t2.pkg_id in(select distinct pkg_id from ad_plan_relation where company_id = 1 and product_id in(54,56,86,89,90,91,92))
	and t1.remark not in('channel', 'tracking');


-- ad_pay_stat
delete from ad_pay_stat where company_id = 1 and package_id in (
	select distinct t1.page_id from ad_page t1 join ad_plan_relation t2 on t1.remark = t2.page_id where t1.company_id = 1 and t2.product_id in(54,56,86,89,90,91,92)
);

INSERT IGNORE INTO ad_pay_stat (company_id, os_type, platform_id, package_id, register_date, pay_date, pay_stat_type, pay_count, pay_num, pay_amount, share_rate, create_time, update_time) 
SELECT t1.company_id, t2.os_type, t2.platform_id, t1.pkg_id, t2.register_date, t2.pay_date, t2.pay_stat_type, t2.pay_count, t2.pay_num, t2.pay_amount, t2.share_rate, t2.create_time, t2.update_time FROM ad_pkg t1
INNER JOIN ad_pay_stat t2 ON t1.remark = t2.package_id
WHERE t1.company_id = 1 and t2.company_id = 81 and t2.register_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH)
	and t2.package_id in(select distinct page_id from ad_plan_relation where company_id = 1 and product_id in(54,56,86,89,90,91,92))
	and t1.remark not in('channel', 'tracking');



-- ad_payback_day
delete from ad_payback_day where company_id = 1 and package_id in (
	select distinct t1.page_id from ad_page t1 join ad_plan_relation t2 on t1.remark = t2.page_id where t1.company_id = 1 and t2.product_id in(54,56,86,89,90,91,92)
);

INSERT IGNORE INTO ad_payback_day (company_id, os_type, platform_id, package_id, register_date, amount_d1, amount_d3, amount_d7, amount_d15, amount_d30, amount_d60, amount_d90, create_time, update_time)
SELECT t1.company_id, t2.os_type, t2.platform_id, t1.pkg_id, t2.register_date, t2.amount_d1, t2.amount_d3, t2.amount_d7, t2.amount_d15, t2.amount_d30, t2.amount_d60, t2.amount_d90, t2.create_time, t2.update_time FROM ad_pkg t1
INNER JOIN ad_payback_day t2 ON t1.remark = t2.package_id
WHERE t1.company_id = 1 and t2.company_id = 81 and t2.register_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH)
	and t2.package_id in(select distinct page_id from ad_plan_relation where company_id = 1 and product_id in(54,56,86,89,90,91,92))
	and t1.remark not in('channel', 'tracking');

-- ad_payback_month
delete from ad_payback_month where company_id = 1 and package_id in (
	select distinct t1.page_id from ad_page t1 join ad_plan_relation t2 on t1.remark = t2.page_id where t1.company_id = 1 and t2.product_id in(54,56,86,89,90,91,92)
);

INSERT IGNORE INTO ad_payback_month (company_id, os_type, platform_id, package_id, register_date, amount_m1, amount_m2, amount_m3, amount_m4, amount_m5, amount_m6, amount_m7, create_time, update_time) 
SELECT t1.company_id, t2.os_type, t2.platform_id, t1.pkg_id, t2.register_date, t2.amount_m1, t2.amount_m2, t2.amount_m3, t2.amount_m4, t2.amount_m5, t2.amount_m6, t2.amount_m7, t2.create_time, t2.update_time FROM ad_pkg t1
INNER JOIN ad_payback_month t2 on t1.remark = t2.package_id
WHERE t1.company_id = 1 and t2.company_id = 81 and t2.register_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH)
	and t2.package_id in(select distinct page_id from ad_plan_relation where company_id = 1 and product_id in(54,56,86,89,90,91,92))
	and t1.remark not in('channel', 'tracking');



-- 最后再将关联关系表的落地页ID和包ID更新为最新的自增ID
UPDATE ad_plan_relation t1
INNER JOIN ad_page t2 on t1.page_id = t2.remark
INNER JOIN ad_pkg t3 on t1.pkg_id = t3.remark
SET t1.page_id = t2.page_id, t1.pkg_id = t3.pkg_id
WHERE t1.company_id = 1 and t2.company_id = 1 and t3.company_id = 1 and t1.product_id in(54,56,86,89,90,91,92) 
and t2.remark not in('channel','tracking') and t3.remark not in('channel','tracking');

-- 最后再将按天计划统计表的落地页ID和包ID更新为最新的自增ID
-- 为了使得后续更新ad_plan_stat_day表在做关联的时候效率更高，加上page_id和pkg_id索引
ALTER TABLE `ad_plan_stat_day` ADD INDEX page_id ( `page_id` ) ;
ALTER TABLE `ad_plan_stat_day` ADD INDEX pkg_id ( `pkg_id` ) ;

UPDATE ad_plan_stat_day t1
INNER JOIN ad_page t2 on t1.page_id = t2.remark
INNER JOIN ad_pkg t3 on t1.pkg_id = t3.remark
SET t1.page_id = t2.page_id, t1.pkg_id = t3.pkg_id
WHERE t1.company_id = 1 and t2.company_id = 1 and t3.company_id = 1 
and t1.stat_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH)  
and t1.medium_account_id in(select medium_account_id from ac_medium_account where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492))
and t2.remark not in('channel','tracking') and t3.remark not in('channel','tracking');

-- 最后再将实时计划统计表的落地页ID和包ID更新为最新的自增ID
ALTER TABLE `ad_plan_stat_realtime` ADD INDEX page_id ( `page_id` ) ;

UPDATE ad_plan_stat_realtime t1
INNER JOIN ad_page t2 on t1.page_id = t2.remark
SET t1.page_id = t2.page_id
WHERE t1.company_id = 1 and t2.company_id = 1 
and t1.stat_time >= DATE_ADD(CURDATE(), INTERVAL -2 DAY) 
and t1.medium_account_id in(select medium_account_id from ac_medium_account where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492))
and t2.remark not in('channel','tracking');

ALTER TABLE `ad_plan_stat_realtime` ADD INDEX pkg_id ( `pkg_id` ) ;

UPDATE ad_plan_stat_realtime t1
INNER JOIN ad_pkg t3 on t1.pkg_id = t3.remark
SET t1.pkg_id = t3.pkg_id
WHERE t1.company_id = 1 and t3.company_id = 1 
and t1.stat_time >= DATE_ADD(CURDATE(), INTERVAL -2 DAY)
and t1.medium_account_id in(select medium_account_id from ac_medium_account where remark in (279,337,338,295,290,375,493,371,345,363,364,491,432,383,397,438,488,489,492))
and t3.remark not in('channel','tracking');

ALTER TABLE `ad_plan_stat_day` DROP INDEX page_id;
ALTER TABLE `ad_plan_stat_day` DROP INDEX pkg_id;
ALTER TABLE `ad_plan_stat_realtime` DROP INDEX page_id;
ALTER TABLE `ad_plan_stat_realtime` DROP INDEX pkg_id;

