-- ad_plan_stat_day 
update ad_plan_stat_day set stat_date = DATE_ADD(stat_date, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by stat_date desc;
-- ad_plan_stat_realtime
update ad_plan_stat_realtime set stat_time = DATE_ADD(stat_time, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by stat_time desc;
-- ad_page_stat_day
update ad_page_stat_day set stat_date = DATE_ADD(stat_date, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by stat_date desc;
-- ad_page_stat_realtime
update ad_page_stat_realtime set stat_time = DATE_ADD(stat_time, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by stat_time desc;
-- ad_pkg_stat_day
update ad_pkg_stat_day set stat_date = DATE_ADD(stat_date, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by stat_date desc;
-- ad_pkg_stat_realtime
update ad_pkg_stat_realtime set stat_time = DATE_ADD(stat_time, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by stat_time desc;
-- ad_tracking_day
update ad_tracking_day set stat_date = DATE_ADD(stat_date, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by stat_date desc;
-- ad_tracking_realtime
-- update ad_tracking_realtime set stat_time = DATE_ADD(stat_time, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by stat_time desc;
-- ad_pay_stat
update ad_pay_stat set register_date = DATE_ADD(register_date, INTERVAL 1 DAY), pay_date = DATE_ADD(pay_date, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by register_date desc;
-- ad_payback_day
update ad_payback_day set register_date = DATE_ADD(register_date, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by register_date desc;
-- ad_payback_month
update ad_payback_month set register_date = DATE_ADD(register_date, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by register_date desc;

-- ac_medium_account_stat_day 20170704
update ac_medium_account_stat_day set stat_date = DATE_ADD(stat_date, INTERVAL 1 DAY), update_time = now() where company_id = 1 order by stat_date desc;