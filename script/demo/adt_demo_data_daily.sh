#!/bin/sh

source ~/.bash_profile

date=`date +%Y-%m-%d`

echo "***************************************${date}**********************************"
mysql_deadt='mysql -udc_crawler -pcrawler -hmysql7 -P3306 -A --default-character-set=utf8'

$mysql_deadt -e 'use de_adt; source /usr/local/htdocs/adtserver/adt-scripts/adt_demo_data_daily.sql;'
