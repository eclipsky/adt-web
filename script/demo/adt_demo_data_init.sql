-- 表数据梳理：
-- ac_medium_account	-- 筛选优点下的两个媒体账号
-- ac_medium_discount_rate	--根据媒体账号获取对应媒体折扣率
-- ad_plan	-- 获取优点两个指定媒体账号对应的所有计划
-- ad_plan_stat_day	--获取优点两个指定媒体账号对应的最近2个月计划统计数据
-- ad_plan_stat_realtime	--获取优点两个指定媒体账号对应的最近1周计划实时统计数据
-- ad_plan_relation -- 获取如上计划对应的关联规则
-- ad_page	--获取优点下所有落地页
-- ad_page_stat_day	-- 获取有点下最近2个月所有落地页按天统计数据
-- ad_page_stat_realtime	-- 获取最近1周所有落地页实时统计数据
-- ad_pkg	-- 获取优点所有包数据
-- ad_pkg_stat_day	--获取优点最近2个月所有包统计数据
-- ad_pkg_stat_realtime	--获取优点最近1周所有包实时统计数据
-- ad_tracking_day	-- 获取优点最近2个月所有tracking统计数据
-- ad_tracking_realtime	-- 获取优点最近1周所有tracking实时统计数据
-- ad_pay_stat
-- ad_payback_day	--获取优点最近2个月的数据
-- ad_payback_month		-- 获取优点最近两个月的数据



-- 在DataEye企业账号下预先建好产品，得到产品ID

-- 智慧推 '3311706053', '3049706606', '617558031','1873146397', '2330268629'
-- ac_medium_account 用remark字段保留旧的medium_account_id
delete from ac_medium_account where company_id = 1;

INSERT INTO ac_medium_account (company_id, product_id, medium_id, medium_account, password, medium_account_alias, system_account_name_list, login_status, cookie, status, skey, remark, create_time, update_time)
select 1 as company_id, 7 as product_id, medium_id, medium_account, password, medium_account_alias, '' system_account_name_list, login_status, cookie, 1 as status, skey, medium_account_id as remark, now(), now() from ac_medium_account where company_id = 2 and medium_account in ('3452416711', '2642615624', '3466907167', '2983646882', '3249910362', 'jiayou1@126.com', 'UC信息流-佳游');

-- ac_medium_discount_rate 用新的媒体账号remark字段保留旧的medium_account_id与媒体折扣率表的旧的medium_account_id关联，得到新的medium_account_id
INSERT INTO ac_medium_discount_rate (medium_account_id, discount_rate, effect_date, invalid_date, remark, create_time, update_time) 
SELECT t1.medium_account_id, t2.discount_rate, t2.effect_date, t2.invalid_date, t1.remark, now(), now() FROM ac_medium_account t1
inner join ac_medium_discount_rate t2 on t1.remark = t2.medium_account_id
where t1.company_id = 1 and t1.medium_account in ('3452416711', '2642615624', '3466907167', '2983646882', '3249910362', 'jiayou1@126.com', 'UC信息流-佳游');

-- ad_plan 用remark字段保留旧的plan_id
delete from ad_plan where company_id = 1;

INSERT INTO ad_plan (plan_name, medium_id, medium_account_id, company_id, status, remark, create_time, update_time)
SELECT t2.plan_name, t2.medium_id, t1.medium_account_id, 1 as company_id, t2.status, t2.plan_id, now(), now() FROM ac_medium_account t1
INNER JOIN ad_plan t2 ON t1.remark = t2.medium_account_id
WHERE t1.company_id = 1 and t2.company_id = 2;

-- ad_plan_stat_day 
delete from ad_plan_stat_day where company_id = 1;

INSERT INTO ad_plan_stat_day (stat_date, plan_id, medium_id, medium_account_id, company_id, page_id, pkg_id, ad_id, cost, show_num, click_num, bid, discount_rate, update_time) 
SELECT t2.stat_date, t1.plan_id, t2.medium_id, t1.medium_account_id, t1.company_id, t2.page_id, t2.pkg_id, t2.ad_id, t2.cost, t2.show_num, t2.click_num, t2.bid, t2.discount_rate, now() FROM ad_plan t1
INNER JOIN ad_plan_stat_day t2 ON t1.remark = t2.plan_id
WHERE t1.company_id = 1 and t2.company_id = 2 and t2.cost > 100 and t2.page_id > 0 and t2.stat_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH);

-- ad_plan_stat_realtime
delete from ad_plan_stat_realtime where company_id = 1;

INSERT INTO ad_plan_stat_realtime (stat_time, plan_id, medium_id, medium_account_id, company_id, page_id, pkg_id, ad_id, cost, show_num, click_num, bid, discount_rate, update_time) 
SELECT t2.stat_time, t1.plan_id, t2.medium_id, t1.medium_account_id, t1.company_id, t2.page_id, t2.pkg_id, t2.ad_id, t2.cost, t2.show_num, t2.click_num, t2.bid, t2.discount_rate, now() FROM ad_plan t1
INNER JOIN ad_plan_stat_realtime t2 ON t1.remark = t2.plan_id
WHERE t1.company_id = 1 and t2.company_id = 2 and t2.stat_time >= DATE_ADD(CURDATE(), INTERVAL -1 DAY);


-- ad_plan_relation
delete from ad_plan_relation where company_id = 1;

INSERT INTO ad_plan_relation (company_id, product_id, plan_id, page_id, pkg_id, remark, create_time, update_time)
SELECT t1.company_id, 7 as product_id, t1.plan_id, t2.page_id, t2.pkg_id, t1.remark, now(), now() FROM ad_plan t1
INNER JOIN ad_plan_relation t2 ON t1.remark = t2.plan_id 
WHERE t1.company_id = 1 and t2.company_id = 2;


-- ad_page 用remark字段保留旧的page_id
delete from ad_page where company_id = 1;

INSERT INTO ad_page (page_name, h5_app_id, h5_page_id, company_id, status, remark, create_time, update_time)
SELECT t2.page_name, t2.h5_app_id, t2.h5_page_id, t1.company_id, t2.status, t1.page_id, now(), now() FROM ad_plan_relation t1 
INNER JOIN ad_page t2 ON t1.page_id = t2.page_id 
WHERE t1.company_id = 1 and t1.product_id = 7 and t2.company_id = 2;

-- ad_page_stat_day
delete from ad_page_stat_day where company_id = 1;

INSERT INTO ad_page_stat_day (stat_date, page_id, company_id, pv, uv, download_times, update_time)
SELECT t2.stat_date, t1.page_id, t1.company_id, t2.pv, t2.uv, t2.download_times, now() FROM ad_page t1
INNER JOIN ad_page_stat_day t2 ON t1.remark = t2.page_id
WHERE t1.company_id = 1 and t2.company_id = 2 and t2.stat_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH);

-- ad_page_stat_realtime
delete from ad_page_stat_realtime where company_id = 1;

INSERT INTO ad_page_stat_realtime (stat_time, page_id, company_id, pv, uv, download_times, update_time)
SELECT t2.stat_time, t1.page_id, t1.company_id, t2.pv, t2.uv, t2.download_times, now() FROM ad_page t1
INNER JOIN ad_page_stat_realtime t2 ON t1.remark = t2.page_id
WHERE t1.company_id = 1 and t2.company_id = 2 and t2.stat_time >= DATE_ADD(CURDATE(), INTERVAL -1 DAY);

-- ad_pkg 用remark字段保留旧的pkg_id
delete from ad_pkg where company_id = 1;

INSERT INTO ad_pkg (pkg_name, cp_app_id, cp_pkg_id, company_id, status, remark, create_time, update_time)
SELECT t2.pkg_name, t2.cp_app_id, t2.cp_pkg_id, t1.company_id, t2.status, t1.pkg_id, now(), now() FROM ad_plan_relation t1
INNER JOIN ad_pkg t2 ON t1.pkg_id = t2.pkg_id
WHERE t1.company_id = 1 and t1.product_id = 7 and t2.company_id = 2;

-- ad_pkg_stat_day
delete from ad_pkg_stat_day where company_id = 1;

INSERT INTO ad_pkg_stat_day (stat_date, pkg_id, company_id, active_num, register_num, total_pay_times, total_pay_num, total_pay_amount, new_pay_times, new_pay_num, new_pay_amount, login_num, download_times, retain_d2, retain_d3, retain_d5, retain_d7, retain_d14, retain_d30, update_time)
SELECT t2.stat_date, t1.pkg_id, t1.company_id, t2.active_num, t2.register_num, t2.total_pay_times, t2.total_pay_num, t2.total_pay_amount, t2.new_pay_times, t2.new_pay_num, t2.new_pay_amount, t2.login_num, t2.download_times, t2.retain_d2, t2.retain_d3, t2.retain_d5, t2.retain_d7, t2.retain_d14, t2.retain_d30, now() FROM ad_pkg t1
INNER JOIN ad_pkg_stat_day t2 ON t1.remark = t2.pkg_id
WHERE t1.company_id = 1 and t2.company_id = 2 and t2.stat_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH);

-- ad_pkg_stat_realtime
delete from ad_pkg_stat_realtime where company_id = 1;

INSERT INTO ad_pkg_stat_realtime (stat_time, pkg_id, company_id, active_num, register_num, total_pay_times, total_pay_num, total_pay_amount, download_times, update_time)
SELECT t2.stat_time, t1.pkg_id, t1.company_id, t2.active_num, t2.register_num, t2.total_pay_times, t2.total_pay_num, t2.total_pay_amount, t2.download_times, now() FROM ad_pkg t1
INNER JOIN ad_pkg_stat_realtime t2 ON t1.remark = t2.pkg_id
WHERE t1.company_id = 1 and t2.company_id = 2 and t2.stat_time >= DATE_ADD(CURDATE(), INTERVAL -1 DAY);

-- ad_tracking_day
delete from ad_tracking_day where company_id = 1;

INSERT INTO ad_tracking_day (stat_date, company_id, page_id, active_num, total_click_num, unique_click_num, daily_unique_click_num, update_time)
SELECT t2.stat_date, t1.company_id, t1.page_id, t2.active_num, t2.total_click_num, t2.unique_click_num, t2.daily_unique_click_num, now() FROM ad_page t1
INNER JOIN ad_tracking_day t2 ON t1.remark = t2.page_id
WHERE t1.company_id = 1 and t2.company_id = 2 and t2.stat_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH);

-- 更新激活数比注册数大1.05倍，向上取整
INSERT INTO ad_tracking_day (stat_date, company_id, page_id, active_num, total_click_num, unique_click_num, daily_unique_click_num, update_time)
SELECT t1.stat_date, t1.company_id, t1.page_id, CEIL(t2.register_num*1.05) as active_num, IFNULL(t3.total_click_num,0) total_click_num, IFNULL(t3.unique_click_num,0) unique_click_num, IFNULL(t3.daily_unique_click_num,0) daily_unique_click_num, now()
FROM ad_plan_stat_day t1 
LEFT JOIN ad_pkg_stat_day t2 ON t1.pkg_id = t2.pkg_id AND t1.stat_date = t2.stat_date 
LEFT JOIN ad_tracking_day t3 ON t1.page_id = t3.page_id AND t1.stat_date = t3.stat_date 
WHERE t1.company_id = 1 AND t1.page_id > 0 AND t2.register_num is not null
ON DUPLICATE KEY UPDATE active_num = VALUES(active_num), update_time = VALUES(update_time);


-- ad_tracking_realtime
-- INSERT INTO ad_tracking_realtime (stat_time, company_id, page_id, active_num, total_click_num, unique_click_num, daily_unique_click_num, update_time)
-- SELECT t2.stat_time, t1.company_id, t1.page_id, t2.active_num, t2.total_click_num, t2.unique_click_num, t2.daily_unique_click_num, now() FROM ad_page t1
-- INNER JOIN ad_tracking_realtime t2 ON t1.remark = t2.page_id
-- WHERE t1.company_id = 1 and t2.company_id = 2 and t2.stat_time >= DATE_ADD(CURDATE(), INTERVAL -1 DAY);

-- ad_pay_stat
delete from ad_pay_stat where company_id = 1;

INSERT INTO ad_pay_stat (company_id, platform_id, package_id, register_date, pay_date, pay_stat_type, pay_count, pay_num, pay_amount, share_rate, create_time, update_time) 
SELECT t1.company_id, t2.platform_id, t1.pkg_id, t2.register_date, t2.pay_date, t2.pay_stat_type, t2.pay_count, t2.pay_num, t2.pay_amount, t2.share_rate, now(), now() FROM ad_pkg t1
INNER JOIN ad_pay_stat t2 ON t1.remark = t2.package_id
WHERE t1.company_id = 1 and t2.company_id = 2 and t2.register_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH);


-- ad_payback_day
delete from ad_payback_day where company_id = 1;

INSERT INTO ad_payback_day (company_id, platform_id, package_id, register_date, amount_d1, amount_d3, amount_d7, amount_d15, amount_d30, amount_d60, amount_d90, create_time, update_time)
SELECT t1.company_id, t2.platform_id, t1.pkg_id, t2.register_date, t2.amount_d1, t2.amount_d3, t2.amount_d7, t2.amount_d15, t2.amount_d30, t2.amount_d60, t2.amount_d90, now(), now() FROM ad_pkg t1
INNER JOIN ad_payback_day t2 ON t1.remark = t2.package_id
WHERE t1.company_id = 1 and t2.company_id = 2 and t2.register_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH);

-- ad_payback_month
delete from ad_payback_month where company_id = 1;

INSERT INTO ad_payback_month (company_id, platform_id, package_id, register_date, amount_m1, amount_m2, amount_m3, amount_m4, amount_m5, amount_m6, amount_m7, create_time, update_time) 
SELECT t1.company_id, t2.platform_id, t1.pkg_id, t2.register_date, t2.amount_m1, t2.amount_m2, t2.amount_m3, t2.amount_m4, t2.amount_m5, t2.amount_m6, t2.amount_m7, now(), now() FROM ad_pkg t1
INNER JOIN ad_payback_month t2 on t1.remark = t2.package_id
WHERE t1.company_id = 1 and t2.company_id = 2 and t2.register_date >= DATE_ADD(CURDATE(), INTERVAL -2 MONTH);



-- 最后再将关联关系表的落地页ID和包ID更新为最新的自增ID
UPDATE ad_plan_relation t1
INNER JOIN ad_page t2 on t1.page_id = t2.remark
INNER JOIN ad_pkg t3 on t1.pkg_id = t3.remark
SET t1.page_id = t2.page_id, t1.pkg_id = t3.pkg_id
WHERE t1.company_id = 1 and t2.company_id = 1 and t3.company_id = 1;

-- 最后再将按天计划统计表的落地页ID和包ID更新为最新的自增ID
UPDATE ad_plan_stat_day t1
INNER JOIN ad_page t2 on t1.page_id = t2.remark
INNER JOIN ad_pkg t3 on t1.pkg_id = t3.remark
SET t1.page_id = t2.page_id, t1.pkg_id = t3.pkg_id
WHERE t1.company_id = 1 and t2.company_id = 1 and t3.company_id = 1;

-- 最后再将实时计划统计表的落地页ID和包ID更新为最新的自增ID
UPDATE ad_plan_stat_realtime t1
INNER JOIN ad_page t2 on t1.page_id = t2.remark
SET t1.page_id = t2.page_id
WHERE t1.company_id = 1 and t2.company_id = 1;

UPDATE ad_plan_stat_realtime t1
INNER JOIN ad_pkg t3 on t1.pkg_id = t3.remark
SET t1.pkg_id = t3.pkg_id
WHERE t1.company_id = 1 and t3.company_id = 1;


-- ADT-Demo产品
UPDATE ad_pkg SET pkg_name = CONCAT('ADT-Demo-Package-',pkg_id) WHERE company_id = 1;
UPDATE ad_page SET page_name = CONCAT('adt-demo_page_',page_id,'.html') WHERE company_id = 1;
UPDATE ad_plan SET plan_name = CONCAT('T-',plan_name) WHERE company_id = 1;
UPDATE ac_medium_account SET medium_account = CONCAT('T-',medium_account), status = 1 WHERE company_id = 1;


-- 账号余额
insert ignore into ac_medium_account_stat_day (stat_date, medium_id, medium_account_id, company_id, balance, cost_today, update_time) 
select stat_date, medium_id, 101, 1, balance, cost_today, update_time  from ac_medium_account_stat_day t where medium_account_id = 5;

insert ignore into ac_medium_account_stat_day (stat_date, medium_id, medium_account_id, company_id, balance, cost_today, update_time) 
select stat_date, medium_id, 102, 1, balance, cost_today, update_time  from ac_medium_account_stat_day t where medium_account_id = 16;

insert ignore into ac_medium_account_stat_day (stat_date, medium_id, medium_account_id, company_id, balance, cost_today, update_time) 
select stat_date, medium_id, 103, 1, balance, cost_today, update_time  from ac_medium_account_stat_day t where medium_account_id = 20;

insert ignore into ac_medium_account_stat_day (stat_date, medium_id, medium_account_id, company_id, balance, cost_today, update_time) 
select stat_date, medium_id, 104, 1, balance, cost_today, update_time  from ac_medium_account_stat_day t where medium_account_id = 21;

insert ignore into ac_medium_account_stat_day (stat_date, medium_id, medium_account_id, company_id, balance, cost_today, update_time) 
select stat_date, medium_id, 105, 1, balance, cost_today, update_time  from ac_medium_account_stat_day t where medium_account_id = 24;

insert ignore into ac_medium_account_stat_day (stat_date, medium_id, medium_account_id, company_id, balance, cost_today, update_time) 
select stat_date, medium_id, 106, 1, balance, cost_today, update_time  from ac_medium_account_stat_day t where medium_account_id = 26;

insert ignore into ac_medium_account_stat_day (stat_date, medium_id, medium_account_id, company_id, balance, cost_today, update_time) 
select stat_date, medium_id, 107, 1, balance, cost_today, update_time  from ac_medium_account_stat_day t where medium_account_id = 30;