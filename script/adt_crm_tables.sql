DROP TABLE IF EXISTS ac_company;
CREATE TABLE IF NOT EXISTS ac_company
(
    company_id INT NOT NULL AUTO_INCREMENT COMMENT '公司ID',
    company_name VARCHAR(200) NOT NULL COMMENT '公司名称',
	uid INT NOT NULL COMMENT '统一登录用户ID',
	user_name VARCHAR(200) COMMENT '统一登录用户名称',
	email VARCHAR(60) NOT NULL COMMENT '邮箱，登录账号（统一登录用户）',
	tel VARCHAR(20) COMMENT '电话',
	qq VARCHAR(15) COMMENT 'QQ',
    status TINYINT(4) DEFAULT '0' NOT NULL COMMENT '状态：0-正常，1-待商务审核，2-暂停，3-拒绝，4-待运营审核，5-冻结，6-停用',
	biz_handler_id INT DEFAULT '0' NOT NULL COMMENT '商务处理人ID',
	oper_handler_id INT DEFAULT '0' NOT NULL COMMENT '运营处理人ID',
	pay_type TINYINT(4) DEFAULT '0' NOT NULL COMMENT '付费类型：0-试用，1-付费',
	access_sdk TINYINT(4) DEFAULT '0' NOT NULL COMMENT '接入SDK：0-是，1-否',
	expire_remind_days TINYINT(4) DEFAULT '0' NOT NULL COMMENT '到期提醒：0-不提醒，1-提前7天，2-提前14天，3-提前30天',
	business_view VARCHAR(300) COMMENT '商务审批意见',
	operator_view VARCHAR(300) COMMENT '运营审批意见',
	effect_date DATE COMMENT '生效日期',
    invalid_date DATE COMMENT '失效日期',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (company_id),
    CONSTRAINT uk UNIQUE (email)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业信息表';


DROP TABLE IF EXISTS crm_manager_account;
CREATE TABLE IF NOT EXISTS crm_manager_account
(
    account_id INT NOT NULL AUTO_INCREMENT COMMENT '账号ID',
    email VARCHAR(60) NOT NULL COMMENT '邮箱(登录账号)',
    user_name VARCHAR(200) COMMENT '姓名',
    password VARCHAR(100) NOT NULL COMMENT '密码',
    status TINYINT(4) DEFAULT '0' NOT NULL COMMENT '状态：0-正常，1-失效',
    account_role TINYINT(4) DEFAULT '0' NOT NULL DEFAULT '0' COMMENT '账户角色:0-销售/商务，1-运营，2-其他',
    menu_permission VARCHAR(200) NOT NULL COMMENT '菜单权限',
    remark  VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (account_id),
    CONSTRAINT uk UNIQUE (email)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CRM账号表';


DROP TABLE IF EXISTS crm_menu;
CREATE TABLE IF NOT EXISTS crm_menu 
(
    menu_id VARCHAR(8) NOT NULL COMMENT '菜单ID',
    menu_name VARCHAR(200) NOT NULL COMMENT '菜单名称',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (menu_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CRM菜单表';


DROP TABLE IF EXISTS crm_menu_interface_mapping;
CREATE TABLE IF NOT EXISTS crm_menu_interface_mapping 
(
    mapping_id INT NOT NULL AUTO_INCREMENT COMMENT '映射ID',
    menu_id VARCHAR(8) NOT NULL COMMENT '菜单ID',
    interface_name VARCHAR(200) NOT NULL COMMENT '接口名称',
    interface_url VARCHAR(200) NOT NULL COMMENT '接口URL',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (mapping_id),
    CONSTRAINT interface_url UNIQUE (menu_id, interface_url)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CRM菜单接口映射表';

INSERT INTO crm_menu(menu_id, menu_name, create_time, update_time) VALUES (1, '客户审核', now(), now());
INSERT INTO crm_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('public', '登录', '/user/login.do', now(), now());
INSERT INTO crm_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '注销', '/user/logout.do', now(), now());
INSERT INTO crm_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '修改系统账号密码', '/ad/systemaccount/modifyPassword.do', now(), now());
INSERT INTO crm_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '系统账号密码重置', '/ad/systemaccount/resetPassword.do', now(), now());
INSERT INTO crm_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询CRM系统账号', '/crm/systemaccount/queryForSelector.do', now(), now());
INSERT INTO crm_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '查询客户审核列表信息', '/crm/customeraudit/query.do', now(), now());
INSERT INTO crm_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '对待审核客户信息进行审核', '/crm/customeraudit/audit.do', now(), now());
INSERT INTO crm_manager_account (email, user_name, password, status, account_role, menu_permission, remark, create_time, update_time) VALUES ('kasey@dataeye.com', 'Kasey', 'e99a18c428cb38d5f260853678922e03', 0, 0, '1', null, now(), now());
INSERT INTO crm_manager_account (email, user_name, password, status, account_role, menu_permission, remark, create_time, update_time) VALUES ('hedy@dataeye.com', '董晓华', 'e99a18c428cb38d5f260853678922e03', 0, 0, '1', null, now(), now());
INSERT INTO crm_manager_account (email, user_name, password, status, account_role, menu_permission, remark, create_time, update_time) VALUES ('mengying@dataeye.com', '陈梦营', 'e99a18c428cb38d5f260853678922e03', 0, 1, '1', null, now(), now());
INSERT INTO crm_manager_account (email, user_name, password, status, account_role, menu_permission, remark, create_time, update_time) VALUES ('mikehuang@dataeye.com', '黄华基', 'e99a18c428cb38d5f260853678922e03', 0, 2, '1', null, now(), now());
INSERT INTO crm_manager_account (email, user_name, password, status, account_role, menu_permission, remark, create_time, update_time) VALUES ('luzhuyou@dataeye.com', '卢柱有', 'e99a18c428cb38d5f260853678922e03', 0, 0, '1', null, now(), now());
INSERT INTO crm_manager_account (email, user_name, password, status, account_role, menu_permission, remark, create_time, update_time) VALUES ('xiongshan@dataeye.com', '熊珊', 'e99a18c428cb38d5f260853678922e03', 0, 1, '1', null, now(), now());
