CREATE TABLE `ac_channel` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT '渠道id',
  `channel_id` varchar(33) DEFAULT NULL COMMENT '渠道id',
  `show_name` varchar(120) DEFAULT NULL COMMENT '渠道名称',
  `cb_url` varchar(6144) DEFAULT NULL COMMENT '渠道url',
  `template_params` varchar(3072) DEFAULT NULL COMMENT '模版参数',
  `click_params` varchar(1536) DEFAULT NULL COMMENT '点击参数',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `available` int(1) DEFAULT NULL COMMENT '是否可用，1-可用',
  `callback_all` int(1) DEFAULT NULL COMMENT '渠道名称',
  `medium_id` int(1) DEFAULT NULL COMMENT '媒体ID，ac_medium中的medium_id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Tracking渠道表';

CREATE TABLE `ac_medium_custom` (
  `medium_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '媒体ID',
  `medium_name` varchar(40) NOT NULL COMMENT '媒体名称',
  `company_id` int(11) NOT NULL COMMENT '公司ID',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态：0-正常，1-失效',
  `remark` varchar(300) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`medium_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8 COMMENT='自定义媒体表';

INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) VALUES ('2', '创建自定义媒体', '/ad/medium/add-custom.do', null, now(), now());

ALTER TABLE ad_plan_stat_day MODIFY COLUMN cost INT DEFAULT '0' COMMENT '花费（单位：分）';
ALTER TABLE ad_plan_stat_day MODIFY COLUMN show_num INT DEFAULT '0' COMMENT '曝光数';
ALTER TABLE ad_plan_stat_day MODIFY COLUMN click_num INT DEFAULT '0' COMMENT '点击数';

ALTER TABLE ad_plan_stat_realtime MODIFY COLUMN cost INT DEFAULT '0' COMMENT '花费（单位：分）';
ALTER TABLE ad_plan_stat_realtime MODIFY COLUMN show_num INT DEFAULT '0' COMMENT '曝光数';
ALTER TABLE ad_plan_stat_realtime MODIFY COLUMN click_num INT DEFAULT '0' COMMENT '点击数';


delete from ac_medium_tracking_mapping where medium_id in (6);

-- 渠道初始化
insert ignore into ac_channel
select t1.*, t2.medium_id from channel t1 
  left join (select distinct tracking_medium_id, medium_id, tracking_access_type from ac_medium_tracking_mapping) t2
    on t1.channel_id = t2.tracking_medium_id;
