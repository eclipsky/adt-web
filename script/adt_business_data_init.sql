set names utf8;

-- 初始化投放计划表
truncate table ad_plan;
INSERT INTO ad_plan (plan_id, plan_name, medium_id, medium_account_id, company_id, status, create_time, update_time)
select t1.id, t1.planName, t1.medium, t2.accountID, 2, t1.visible, now(), now() from dc_ad_plan t1
inner join (
  select id, planID, accountID from dc_ad_report_day 
  where id in(select max(id) as id from dc_ad_report_day where totalPay > 0 or exposureNum > 0 group by planID)
)t2 on t1.id = t2.planID;


-- 初始化落地页表
truncate table ad_page;
INSERT INTO ad_page (page_id, page_name, company_id, status, create_time, update_time)
select id, pageUrl, 2, visible, now(), now() from dc_ad_land_url 
where visible = 0;
-- 删除无效记录
delete from ad_page where page_name like  '%10086%';

-- 初始化包表
truncate table ad_pkg;
INSERT INTO ad_pkg (pkg_id, pkg_name, cp_app_id, cp_pkg_id, company_id, status, create_time, update_time)
select package_id, package_name, 1, package_id, 2, visible, now(), now() from ad_package;


-- 初始化计划统计表(按天)
truncate table ad_plan_stat_day;
INSERT INTO ad_plan_stat_day (stat_date, plan_id, medium_id, medium_account_id, company_id, page_id, pkg_id, ad_id, cost, show_num, click_num, discount_rate, update_time) 
SELECT from_unixtime(t1.reportTime, '%Y-%m-%d'), t1.planID, t1.medium, t1.accountID, 2, IFNULL(t2.landpageID,0) AS landpageID, IFNULL(t2.channelID, 0) AS channelID, t1.adID, t1.totalPay * 100, t1.exposureNum, t1.clickNum, 0 AS discount_rate, from_unixtime(t1.reportTime) 
FROM (
	SELECT reportTime, medium, planID, adID ,accountID, SUM(totalPay) AS totalPay, SUM(exposureNum) AS exposureNum, SUM(clickNum) AS clickNum
	FROM dc_ad_report_day T
	WHERE (totalPay > 0 OR exposureNum > 0 )
	GROUP BY reportTime, medium, planID, adID, accountID
)t1 
LEFT JOIN dc_ad_association t2 ON t1.planID = t2.planID AND t1.reportTime = t2.reportTime;


-- 初始化落地页统计表(按天)
truncate table ad_page_stat_day;
INSERT INTO ad_page_stat_day (stat_date, page_id, company_id, pv, uv, update_time)
SELECT from_unixtime(reportTime, '%Y-%m-%d'), urlID, 2, pv, uv, from_unixtime(reportTime) FROM dc_ad_land_page;


-- 初始化包统计表(按天)
truncate table ad_pkg_stat_day;
INSERT INTO ad_pkg_stat_day (stat_date, pkg_id, company_id, active_num, register_num, total_pay_times, total_pay_num, total_pay_amount, new_pay_times, new_pay_num, new_pay_amount, login_num, download_times, retain_d2, retain_d3, retain_d5, retain_d7, retain_d14, retain_d30, update_time)
SELECT stat_date, package_id, 2, IFNULL(active_num,0), register_num, total_pay_times, total_pay_num, total_pay_amount * 100, 0, new_pay_num, new_pay_amount * 100, login_num, download_times, retain_d2, retain_d3, retain_d5, retain_d7, retain_d14, retain_d30, update_time FROM ad_delivery_stat;


-- 初始化计划关联关系表(计划，产品，落地页，包关联)
truncate table ad_plan_relation;
INSERT INTO ad_plan_relation (company_id, product_id, plan_id, page_id, pkg_id, create_time, update_time)
SELECT 2, 1, planID, landpageID, channelID, from_unixtime(createTime), from_unixtime(updateTime) 
FROM(
	SELECT planID, landpageID, channelID,  createTime, MAX(updateTime) AS updateTime FROM dc_ad_association_range 
	WHERE visible = 1 AND endTime > unix_timestamp(now())
	group by planID, landpageID, channelID 
)t;


-- 初始化Tracking统计表(按天)
truncate table ad_tracking_day;
INSERT INTO ad_tracking_day (stat_date, company_id, page_id, active_num, total_click_num, unique_click_num, daily_unique_click_num, update_time)
SELECT from_unixtime(t1.reportTime, '%Y-%m-%d'), 2, t2.id, t1.activations, t1.clicks, t1.unique_clicks, t1.daily_unique_clicks, from_unixtime(t1.reportTime) FROM dc_ad_tracking_day t1
INNER JOIN dc_ad_land_url t2 ON t1.pageUrl = t2.pageUrl;


-- 初始化计划告警配置表
truncate table ad_plan_alert_conf;
INSERT INTO ad_plan_alert_conf (id, company_id, type, plan_id, consume, cpa, ctr, download_rate, email_alert, cost_operator, cpa_operator, ctr_operator, download_rate_operator) 
SELECT id, 2, 0, planID, consume, costPerRegistration, ctr, downloadRate, emailAlert, 1, 1, 0, 0 FROM dc_ad_plan_alert_conf;


-- 初始化更新CP付费统计表
update ad_pay_stat set company_id=2, pay_amount=pay_amount*100;

-- 初始化更新日回本表
update ad_payback_day set company_id=2, amount_d1=amount_d1*100, amount_d3=amount_d3*100, amount_d7=amount_d7*100, amount_d15=amount_d15*100, amount_d30=amount_d30*100, amount_d60=amount_d60*100, amount_d90=amount_d90*100;

-- 初始化更新月回本表
update ad_payback_month set company_id=2, amount_m1=amount_m1*100, amount_m2=amount_m2*100, amount_m3=amount_m3*100, amount_m4=amount_m4*100, amount_m5=amount_m5*100, amount_m6=amount_m6*100, amount_m7=amount_m7*100;

-- 初始化更新CP付费统计表、日回本表、月回本表 表结构
alter table ad_pay_stat modify column pay_amount DECIMAL(20,0) DEFAULT '0' NOT NULL COMMENT '付费金额(按分)';
alter table ad_payback_day modify column amount_d1 DECIMAL(20,0) DEFAULT '0' NOT NULL COMMENT '首日回本金额(按分)';
alter table ad_payback_day modify column amount_d3 DECIMAL(20,0) COMMENT '3日回本金额(按分)';
alter table ad_payback_day modify column amount_d7 DECIMAL(20,0) COMMENT '7日回本金额(按分)';
alter table ad_payback_day modify column amount_d15 DECIMAL(20,0) COMMENT '15日回本金额(按分)';
alter table ad_payback_day modify column amount_d30 DECIMAL(20,0) COMMENT '30日回本金额(按分)';
alter table ad_payback_day modify column amount_d60 DECIMAL(20,0) COMMENT '60日回本金额(按分)';
alter table ad_payback_day modify column amount_d90 DECIMAL(20,0) COMMENT '90日回本金额(按分)';
alter table ad_payback_month modify column amount_m1 DECIMAL(20,0) COMMENT '当月回本金额(按分)';
alter table ad_payback_month modify column amount_m2 DECIMAL(20,0) COMMENT '次月回本金额(按分)';
alter table ad_payback_month modify column amount_m3 DECIMAL(20,0) COMMENT '3个月回本金额(按分)';
alter table ad_payback_month modify column amount_m4 DECIMAL(20,0) COMMENT '4个月回本金额(按分)';
alter table ad_payback_month modify column amount_m5 DECIMAL(20,0) COMMENT '5个月回本金额(按分)';
alter table ad_payback_month modify column amount_m6 DECIMAL(20,0) COMMENT '6个月回本金额(按分)';
alter table ad_payback_month modify column amount_m7 DECIMAL(20,0) COMMENT '7个月回本金额(按分)';
alter table ad_payback_month modify column register_date DATE NOT NULL COMMENT '注册日期';

-- 更新落地页、包、计划统计表（实时）数据
update ad_page_stat_realtime set company_id = 2;
update ad_pkg_stat_realtime set company_id = 2;
update ad_plan_stat_realtime set company_id = 2;

-- 更新计划统计表（实时）数据
INSERT INTO ad_plan_stat_realtime (stat_time, plan_id, medium_id, medium_account_id, company_id, page_id, pkg_id, ad_id, cost, show_num, click_num, bid, discount_rate, update_time) 
SELECT t1.stat_time, t1.plan_id, t1.medium_id, t1.medium_account_id, t1.company_id, 
IFNULL(t2.landpageID,0) AS page_id, IFNULL(t2.channelID,0) AS pkg_id, t1.ad_id, t1.cost, t1.show_num, t1.click_num, t1.bid, t1.discount_rate, t1.update_time 
FROM ad_plan_stat_realtime t1
left join dc_ad_association t2 on t1.plan_id = t2.planID and DATE_FORMAT(t1.stat_time, '%Y-%m-%d') = from_unixtime(t2.reportTime, '%Y-%m-%d')
ON DUPLICATE KEY UPDATE page_id=VALUES(page_id), pkg_id=VALUES(pkg_id);

