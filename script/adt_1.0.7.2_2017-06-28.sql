create table IF NOT EXISTS ad_operation_log(
    op_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'ID',
    company_id int(11) NOT NULL COMMENT '公司ID',
    plan_id INT NOT NULL COMMENT '计划/策略ID,对应dc_ad_plan',
    op_time DATETIME NOT NULL COMMENT '操作时间',
    account_id INT  NOT NULL COMMENT 'ADT账号ID',
    op_code  int NOT NULL COMMENT '操作名称编码：1:修改出价; 2:切换开关',
    current_value  VARCHAR(32) NOT NULL COMMENT '改动后的值',
    previous_value  VARCHAR(32) NOT NULL COMMENT '改动前的值',
    result tinyint(1) NOT NULL COMMENT '结果:1:成功;0:失败',
    message VARCHAR(64) COMMENT '消息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志';

INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-1', '获取操作日志列表', '/ad/realtimedelivery/op-log.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-1', '获取指标涨跌显示偏好', '/ad/real-time/ups-and-downs-preference.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-1', '更新指标涨跌显示偏好', '/ad/real-time/ups-and-downs-preference/update.do', current_timestamp(), current_timestamp());

CREATE TABLE `ad_ups_and_downs_preference` (
   `company_id` int(11) NOT NULL COMMENT '公司ID',
   `account_id` int(11) NOT NULL COMMENT '账户ID',
   `minutes` int NOT NULL COMMENT '分钟数',
   `indicator_visibility` varchar(256) NOT NULL COMMENT '涨跌可见性',
   `create_time` datetime NOT NULL COMMENT '创建时间' DEFAULT CURRENT_TIMESTAMP,
   `update_time` datetime NOT NULL COMMENT '更新时间'DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   UNIQUE KEY `uk` (`company_id`,`account_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='指标涨跌显示偏好表';

 insert into ad_ups_and_downs_preference(company_id, account_id, minutes,indicator_visibility,create_time,update_time) select company_id, account_id, 10, '1-1:0,1-8:0,2-4:0,3-3:0,3-6:0',current_timestamp(),current_timestamp() from ac_system_account;

-- insert into ac_menu(menu_id,menu_name,create_time,update_time) values('3-4','自然流量',current_timestamp(),current_timestamp());
-- INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-4', '自然流量', '/ad/real-time/natural-flow.do', current_timestamp(), current_timestamp());
-- DODO update menu permission

insert into ac_indicator(indicator_id,indicator_key,indicator_name,selected,editable,create_time,update_time) values('3-7','activationRegistrationRate','激活注册率',1,1,current_timestamp(),current_timestamp());
insert into ac_indicator(indicator_id,indicator_key,indicator_name,selected,editable,create_time,update_time) values('5-8','newly_increased_pay_rate','新增用户付费率',1,1,current_timestamp(),current_timestamp());
insert into ac_indicator(indicator_id,indicator_key,indicator_name,selected,editable,create_time,update_time) values('5-9','first_day_ltv','首日LTV',1,1,current_timestamp(),current_timestamp());
update ac_indicator set indicator_name = '点击注册率' where indicator_id = '3-5';