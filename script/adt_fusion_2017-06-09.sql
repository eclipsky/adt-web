DROP TABLE IF EXISTS ac_indicator;
CREATE TABLE IF NOT EXISTS ac_indicator
(
    indicator_id VARCHAR(8) NOT NULL COMMENT '指标ID',
    indicator_key VARCHAR(200) NOT NULL COMMENT '指标Key',
    indicator_name VARCHAR(200) NOT NULL COMMENT '指标名称',
    selected INT NOT NULL DEFAULT '0' COMMENT '选择情况:0-不选择，1-选择',
    editable INT NOT NULL DEFAULT '0' COMMENT '编辑情况:0-不可编辑，1-可编辑',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (indicator_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='指标字典表';

alter table ac_system_account modify column account_role TINYINT DEFAULT '0' NOT NULL COMMENT '账户角色:0-优化师，1-优化师组长，2-企业账户，3-开发工程师，4-代理';
alter table ac_system_account add column indicator_permission VARCHAR(200) NOT NULL NULL DEFAULT '' COMMENT '指标权限' after menu_permission;


CREATE TABLE IF NOT EXISTS ac_indicator_habit 
(
    company_id INT NOT NULL COMMENT '公司ID',
    account_id INT NOT NULL COMMENT '账户ID',
    menu_id VARCHAR(8) NOT NULL COMMENT '菜单ID',
    indicator_habit VARCHAR(200) NOT NULL COMMENT '指标习惯',	-- 1:0,2-1:1
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    CONSTRAINT uk UNIQUE (company_id, account_id, menu_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='指标习惯表';

INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1', 'medium', '媒体', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1-1', 'cost', '消耗', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1-10', 'cpc', 'CPC', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1-2', 'plan_switch', '投放开关', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1-3', 'plan_bid', '出价', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1-4', 'show_num', '曝光', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1-5', 'click_num', '点击数', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1-6', 'unique_click_num', '排重点击数', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1-7', 'daily_unique_click_num', '按天排重点击数', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1-8', 'ctr', 'CTR', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('1-9', 'cpm', 'CPM', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('2', 'page', '落地页', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('2-1', 'uv', '到达数', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('2-2', 'uv_rate', '到达率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('2-3', 'download_times', '下载数', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('2-4', 'download_rate', '下载率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('3', 'active_register', '激活注册', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('3-1', 'active_num', '激活数', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('3-2', 'active_rate', '激活率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('3-3', 'active_cpa', '激活CPA', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('3-4', 'register_num', '注册数', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('3-5', 'register_rate', '注册率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('3-6', 'register_cpa', '注册CPA', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('4', 'retain', '留存率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('4-1', 'retain_d1', '次日留存率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('4-2', 'retain_d3', '三日留存率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('4-3', 'retain_d7', '七日留存率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('4-4', 'retain_d30', '30日留存率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('5', 'pay', '充值', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('5-1', 'new_pay_num', '新增充值人数', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('5-2', 'new_pay_amount', '新增充值金额', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('5-3', 'new_payback_rate', '首日回本率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('5-4', 'total_pay_amount', '总充值', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('5-5', 'total_pay_num', '总充值用户数', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('5-6', 'cumulative_amount', '累计充值', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('5-7', 'pay_rate', '付费率', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('6', 'dau', 'DAU', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('6-1', 'dau', 'DAU', 1, 1, null, now(), now());

DROP TABLE IF EXISTS ad_campaign;
CREATE TABLE IF NOT EXISTS ad_campaign
(
    campaign_id VARCHAR(7) NOT NULL COMMENT '活动ID（短链活动号）',
    company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',    
    system_account_id INT NOT NULL COMMENT 'ADT账号ID',
    product_id INT DEFAULT '0' NOT NULL COMMENT '产品ID',
    medium_id SMALLINT DEFAULT '0' NOT NULL COMMENT '媒体ID',
    medium_account_id INT DEFAULT '0' NOT NULL COMMENT '媒体帐号ID',
    os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
    app_treasure TINYINT DEFAULT '0' NOT NULL COMMENT '是否上应用宝:0-否，1-是',
    remark VARCHAR(128) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (campaign_id)
)
ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Tracking广告活动表';

INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) VALUES ('7-1', '查询渠道参数', '/ad/campaign/queryChannelParams.do', null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) VALUES ('7-1', '查询推广计划', '/ad/campaign/query.do', null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) VALUES ('7-1', '下拉框查询推广计划', '/ad/campaign/queryForSelector.do', null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) VALUES ('7-1', '预创建推广链接', '/ad/campaign/createCampaignBefore.do', null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) VALUES ('7-1', '创建推广活动', '/ad/campaign/add.do', null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) VALUES ('7-1', '更新推广计划', '/ad/campaign/modify.do', null, now(), now());

INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-1', '实时操盘指标偏好', '/ad/real-time/indicator-preference.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-1', '更新实时操盘指标偏好', '/ad/real-time/indicator-preference/update.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('4-1', '投放日报指标偏好', '/ad/delivery/indicator-preference.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('4-1', '更新投放日报指标偏好', '/ad/delivery/indicator-preference/update.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '指标列表', '/ad/systemaccount/indicators.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '查询系统账号信息', '/ad/systemaccount/getAccount.do', current_timestamp(), current_timestamp());
-- 初始化指标权限与指标偏好
update ac_system_account set indicator_permission = '1-1,1-2,1-3,1-4,1-5,1-6,1-7,1-8,1-10,2-1,2-2,2-3,2-4,3-1,3-2,3-3,3-4,3-5,3-6,4-1,4-2,4-3,4-4,5-1,5-2,5-3,5-4,5-5,5-6,5-7,6-1';
insert into ac_indicator_habit(company_id, account_id, menu_id,indicator_habit,create_time,update_time) select company_id, account_id, '3-1', '1-2:1,1-3:1,1-1:1,1-4:0,1-5:1,1-8:1,2-1:0,2-2:1,2-3:0,2-4:1,3-1:0,3-2:0,3-3:0,3-4:1,3-5:1,3-6:1,1-10:0',current_timestamp(),current_timestamp() from ac_system_account;
insert into ac_indicator_habit(company_id, account_id, menu_id,indicator_habit,create_time,update_time) select company_id, account_id, '4-1', '1-1:1,1-4:0,1-5:1,1-6:0,1-7:0,1-8:1,1-10:0,2-1:0,2-2:1,2-3:0,2-4:1,3-1:0,3-2:0,3-3:0,3-4:1,3-5:1,3-6:1,4-1:1,4-2:1,4-3:1,4-4:1,5-1:1,5-2:1,5-3:1,5-4:1,5-5:0,5-6:0,5-7:1,6-1:1',current_timestamp(),current_timestamp() from ac_system_account;

INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('4-2', '实时趋势图授权指标列表', '/ad/delivery/trend/indicators.do', current_timestamp(), current_timestamp());

INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '查询组长候选人列表', '/ad/systemaccount/group-leader-candidates.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('2', '查询负责人或关注者候选人列表', '/ad/systemaccount/manager-follower-candidates.do', current_timestamp(), current_timestamp());