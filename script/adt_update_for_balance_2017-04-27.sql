INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-2', '账户余额实时', '/ad/balance/list.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-2', '获取账户余额告警配置', '/ad/balance/alert/get.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-2', '保存账户余额告警配置', '/ad/balance/alert/save.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-2', '账户余额趋势图', '/ad/balance/trend.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-2', '获取负责人管理的媒体账号', '/ad/manager/medium-accounts.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-2', '获取负责人管理的媒体', '/ad/manager/mediums.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('4-2', '投放对比分析按媒体账号统计趋势图', '/ad/delivery/trend/medium-account.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('4-2', '投放对比分析按计划统计趋势图', '/ad/delivery/trend/plan.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('4-2', '投放对比分析计划列表', '/ad/delivery/plan/list.do', current_timestamp(), current_timestamp());
insert into ac_menu(menu_id,menu_name,create_time,update_time) values('4-2','对比分析',current_timestamp(),current_timestamp());
insert into ac_menu(menu_id,menu_name,create_time,update_time) values('3-1','实时操盘',current_timestamp(),current_timestamp());
insert into ac_menu(menu_id,menu_name,create_time,update_time) values('3-2','账户余额',current_timestamp(),current_timestamp());
update ac_system_account set menu_permission = '3-1,3-2,4-1,5,6,7' where menu_permission = '3,4-1,5,6,7';
update ac_menu_interface_mapping set menu_id = '3-1' where menu_id = '3';
create table ad_balance_alert_conf(
   `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT '主键',
   `company_id` int(11) NOT NULL DEFAULT '0' COMMENT '公司ID',
   `email_alert` tinyint(4) NOT NULL DEFAULT '0' COMMENT '立即邮件通知:0-否, 1-是',
   `account_id` int(11) NOT NULL COMMENT '负责人系统账户ID',
    UNIQUE KEY `uk_account_id` (`account_id`)
) COMMENT '账号余额告警配置';

create table ad_balance_alert_log(
   `email` varchar(64) NOT NULL COMMENT '电子邮箱地址',
   `medium_account_id_crc` BIGINT NOT NULL COMMENT '媒体ID CRC编码',
   `update_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    UNIQUE KEY `uk_email_medium_account_id` (`email`,`medium_account_id_crc`)
) COMMENT '账号余额告警日志';
