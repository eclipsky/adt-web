CREATE TABLE `api_tx_campaign` (
  `campaign_name` varchar(100) NOT NULL COMMENT '推广计划名称',
  `campaign_id` int(11) NOT NULL COMMENT '推广计划ID(媒体平台返回的id)',
  `campaign_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '推广计划类型：1-普通展示广告',
  `product_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '推广计划目标：0-Others，1-iOS， 2-Android',
  `daily_budget` int(11) DEFAULT NULL COMMENT '日消耗限额（单位：分,最小值 5000，最大值 400000000,每次修改幅度不能低于 5,000 分）',
  `configured_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '推广计划状态：1-正常，0-失效',
  `speed_mode` tinyint(4) NOT NULL DEFAULT '1' COMMENT '投放速度模式：1-加速投放，2-标准投放',
  `company_id` int(11) NOT NULL DEFAULT '0' COMMENT '公司ID',
  `system_account_id` int(11) NOT NULL COMMENT 'ADT账号ID',
  `medium_account_id` int(11) NOT NULL COMMENT '媒体帐号ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  UNIQUE KEY `id` (`campaign_id`,`campaign_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告推广计划表';
--广告组表
CREATE TABLE `api_tx_ad_group` (
  `adgroup_id` int(11) NOT NULL COMMENT '广告组ID',
  `adgroup_name` varchar(100) NOT NULL COMMENT '广告组名称',
  `company_id` int(11) NOT NULL DEFAULT '0' COMMENT '公司ID',
  `system_account_id` int(11) NOT NULL COMMENT 'ADT账号ID',
  `campaign_id` int(11) NOT NULL COMMENT '推广计划ID(媒体平台返回的id)',
  `medium_id` int(11) NOT NULL COMMENT '媒体ID',
  `product_id` int(11) NOT NULL COMMENT '产品ID',
  `medium_account_id` int(11) NOT NULL COMMENT '媒体帐号ID',
  `targeting_id` int(11) DEFAULT NULL COMMENT '定向ID',
  `adcreative_template_id` int(11) DEFAULT NULL COMMENT '创意规格 id',
  `site` varchar(100) NOT NULL COMMENT '广告创意站点',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  UNIQUE KEY `uk` (`adgroup_id`,`campaign_id`,`medium_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推广广告组表';
--广告创意表
CREATE TABLE `api_tx_ad_creative` (
  `adgroup_id` int(11) NOT NULL COMMENT '广告组ID',
  `ads_id` int(11) NOT NULL COMMENT '广告ID',
  `adcreative_id` int(11) NOT NULL COMMENT '广告创意ID',
  `medium_id` int(11) NOT NULL COMMENT '媒体ID',
  `medium_account_id` int(11) NOT NULL COMMENT '媒体帐号ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  UNIQUE KEY `uk` (`adgroup_id`,`ads_id`,`adcreative_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告创意表';
--广告创意规格模板表
CREATE TABLE `api_tx_ad_creative_template` (
  `id` smallint(6) NOT NULL COMMENT '广告创意id',
  `name` varchar(100) NOT NULL COMMENT '广告创意名称',
  `style` varchar(100) NOT NULL COMMENT '广告创意形式',
  `description` varchar(100) NOT NULL COMMENT '广告创意描述',
  `encoded_id` varchar(40) NOT NULL,
  `size` varchar(40) NOT NULL COMMENT '广告创意尺寸',
  `site` varchar(100) NOT NULL COMMENT '广告创意站点',
  `product_type` varchar(100) NOT NULL COMMENT '商品类型',
  `has_priv` varchar(40) DEFAULT NULL COMMENT '创意是否可用',
  `cost_type` varchar(40) NOT NULL DEFAULT '' COMMENT '出价类型 1-CPC;2--CPA,4-CPM',
  `position` text NOT NULL COMMENT '广告版位',
  `element` text NOT NULL COMMENT '创意元素',
  `available` int(1) DEFAULT NULL COMMENT '是否可用，1-可用',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  UNIQUE KEY `id` (`id`,`site`,`product_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告创意规格表';
--广点通授权账号表
CREATE TABLE `api_tx_account_auth` (
  `account_id` int(11) NOT NULL COMMENT '广告主账号id',
  `medium_account_id` int(11) NOT NULL COMMENT '媒体账户id',
  `login_name` varchar(40) NOT NULL COMMENT '登陆QQ账号',
  `access_token` varchar(225) NOT NULL COMMENT '请求认证token',
  `access_token_expires_in` bigint(20) NOT NULL COMMENT '请求认证token有效期',
  `last_update_time_for_access_token` datetime NOT NULL COMMENT 'access_token最后更新时间',
  `refresh_token` varchar(225) NOT NULL COMMENT '刷新token',
  `refresh_token_expires_in` bigint(20) NOT NULL COMMENT '刷新token有效期',
  `last_update_time_for_refresh_token` datetime NOT NULL COMMENT 'refresh_token最后更新时间',
  `scope_list` varchar(225) DEFAULT '' COMMENT '已授权菜单',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='GDT授权账户表';
--定向表
CREATE TABLE `api_tx_ad_targeting` (
  `targeting_id` int(11) NOT NULL COMMENT '定向ID',
  `medium_account_id` int(11) NOT NULL COMMENT '媒体账号ID',
  `targeting_name` varchar(100) NOT NULL COMMENT '定向名称',
  `is_public` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否为公共定向包（0-否；1-是）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  UNIQUE KEY `uk` (`targeting_id`,`medium_account_id`,`targeting_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定向表';
--推广应用明细表
CREATE TABLE `api_tx_app_product` (
  `product_refs_id` varchar(20) NOT NULL COMMENT '推广应用id',
  `app_id` VARCHAR(64) COMMENT '关联TrackingAppID',
  `medium_account_id` int(11) NOT NULL COMMENT '媒体账号id',
  `product_name` varchar(100) NOT NULL COMMENT '推广应用名称',
  `product_type` varchar(50) NOT NULL COMMENT '推广产品类型',
  `product_url` text COMMENT '推广应用图标URL',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  UNIQUE KEY `uk` (`product_refs_id`,`medium_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推广应用明细表';
INSERT INTO ac_menu(menu_id, menu_name, create_time, update_time) VALUES ('7-1', '广告投放', now(), now());
--接口映射
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '查询推广计划列表（广告投放）', '/ad/amPlanGroup/query.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '创建推广计划列表（广告投放）', '/ad/amPlanGroup/add.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '更新推广计划列表（广告投放）', '/ad/amPlanGroup/modify.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '删除推广计划列表（广告投放）', '/ad/amPlanGroup/delete.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '下拉框查询推广计划列表（广告投放）', '/ad/amPlanGroup/queryForSelect.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '查询广告列表（广告投放）', '/ad/amAdvertisementGroup/query.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '创建广告（广告投放）', '/ad/amAdvertisementGroup/addAdGroups.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '编辑广告（广告投放）', '/ad/amAdvertisementGroup/modifyAdGroups.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '删除广告（广告投放）', '/ad/amAdvertisementGroup/deleteAdGroups.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '下拉框查询广告列表（广告投放）', '/ad/amAdvertisementGroup/queryForSelect.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '查询广告版位列表（广告投放）', '/ad/amAdcreativeTemplate/query.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '查询广告创意列表（广告投放）', '/ad/amAdcreativeInfo/query.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '添加图片或视频文件（广告投放）', '/ad/amFiles/add.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '下拉框查询定向列表（广告投放）', '/ad/amTargetings/queryForSelect.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('7-1', '编辑广告组开关和出价', '/ad/amAdvertisementGroup/modifyAdGroupsBid.do', 0,null, now(), now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission,create_time,update_time) values('7-1','新增定向包','/ad/amTargetings/addTargeting.do',0,now(),now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission,create_time,update_time) values('7-1','更新定向包','/ad/amTargetings/modifyTargeting.do',0,now(),now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission,create_time,update_time) values('7-1','删除定向包','/ad/amTargetings/deleteTargeting.do',0,now(),now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission,create_time,update_time) values('7-1','查询定向','/ad/amTargetings/queryTargeting.do',0,now(),now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission,create_time,update_time) values('7-1','查询可用定向标签(地域,商业兴趣,app行为)','/ad/amTargetings/queryTargetingTags.do',0,now(),now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission,create_time,update_time) values('7-1','添加关键词文件','/ad/amTargetings/addkwFile.do',0,now(),now());
insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time) values('7-1','推广计划名称校验','/ad/amPlanGroup/checkName.do',0,now(),now());
insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time) values('7-1','广告组名称校验','/ad/amAdvertisementGroup/checkName.do',0,now(),now());
insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time) values('7-1','定向包名称校验','/ad/amTargetings/checkName.do',0,now(),now());
insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time) values('7-1','获取推广标的物应用详情','/ad/amProduct/queryProduct.do',0,now(),now());
update ac_system_account set menu_permission = concat(menu_permission,',7-1') where account_role = 2 and menu_permission not like  '%7-1%';

     

--alter table api_tx_ad_group add column site VARCHAR(100)  NOT NULL COMMENT '广告创意站点' after adcreative_template_id;

--alter table t_adcreative_template add column available INT(1)  DEFAULT '1' COMMENT '是否可用，1-可用' after element;



--2018-01-08
--补全版位
 update api_tx_ad_creative_template set available=1 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS') and has_priv='111' and site='SITE_SET_MOBILE_INNER' and name ='QQ空间' and style like '1000×560随心互动';
 update api_tx_ad_creative_template set available=1 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS') and has_priv='111' and site='SITE_SET_MOBILE_INNER' and name ='浏览器及管家' and style like '72×72单图(文)';
 update api_tx_ad_creative_template set available=1 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS') and has_priv='111' and site='SITE_SET_MOBILE_INNER' and name ='浏览器及手腾网' and style like '240×180多图(文)';
 update api_tx_ad_creative_template set available=1 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS') and has_priv='111' and site='SITE_SET_MOBILE_INNER' and name ='腾讯视频' and style like '960×274单图(文)';
 update api_tx_ad_creative_template set available=1 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS') and has_priv='111' and site='SITE_SET_MOBILE_INNER' and name ='腾讯新闻' and style like '230×152多图(文)';
 update api_tx_ad_creative_template set available=1 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS') and has_priv='111' and site='SITE_SET_MOBILE_INNER' and name ='腾讯新闻' and style like '230×152单图(文)';
 update api_tx_ad_creative_template set available=1 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS') and has_priv='111' and site='SITE_SET_MOBILE_INNER' and name ='移动Banner' and style like '72×72单图(文)';
 update api_tx_ad_creative_template set available=1 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS') and has_priv='111' and site='SITE_SET_MOBILE_INNER' and name ='移动插屏' and style like '72×72单图(文)'; 
 
 
--alter table ac_medium_account add column  daily_budget INT COMMENT '日消耗限额（单位：分,最小值 5000，最大值 400000000,每次修改幅度不能低于 5,000 分）' after page_type;
alter table api_tx_ad_group add column  daily_budget INT COMMENT '日消耗限额（单位：分,最小值 5000，最大值 400000000,每次修改幅度不能低于 5,000 分）' after adgroup_name;
alter table api_tx_ad_group add column  status TINYINT DEFAULT '1' NOT NULL COMMENT '客户设置的状态：1-正常，0-失效，即广告开关' after daily_budget;


 update api_tx_ad_creative_template
set has_priv=111
 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS')
-- and has_priv='111' and site='SITE_SET_MOBILE_INNER'
 and name ='QQ空间' and style like '1000×560随心互动';
 
  update api_tx_ad_creative_template
 set available=1,has_priv='111'
 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS')
 and name ='QQ空间' and style like '1000×560单图(文)' and description='好友动态消息流及优选移动广告位';
 
  
 update api_tx_ad_creative_template
 set available=1,has_priv='111',cost_type='[1]'
 where product_type in('PRODUCT_TYPE_APP_ANDROID_OPEN_PLATFORM','PRODUCT_TYPE_APP_IOS')
  and name ='腾讯视频' and style like '960×540单图(文)' and description='首页专栏、热点视频流';
 