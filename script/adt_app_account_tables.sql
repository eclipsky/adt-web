set names utf8;

drop table if exists ac_h5;
drop table if exists ac_h5_account;
drop table if exists ac_h5_app;
drop table if exists ac_cp;
drop table if exists ac_cp_account;
drop table if exists ac_cp_app;
drop table if exists ac_tracking_app;
drop table if exists ac_tracking_account;

-- H5平台表：
CREATE TABLE IF NOT EXISTS ac_h5(
  h5_id INT NOT NULL AUTO_INCREMENT COMMENT 'H5平台ID，自增主键',
  h5_name VARCHAR(64) NOT NULL COMMENT 'H5平台名称',
  homepage VARCHAR(128) NOT NULL COMMENT '主页',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (h5_id),
  CONSTRAINT uk UNIQUE (h5_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'H5平台表';
-- H5账户表：


CREATE TABLE IF NOT EXISTS ac_h5_account(
  h5_account_id INT NOT NULL AUTO_INCREMENT COMMENT 'H5账户ID，自增主键',
  h5_id INT NOT NULL DEFAULT 0 COMMENT 'H5平台ID',
  h5_account VARCHAR(64) NOT NULL COMMENT 'H5账户',
  password VARCHAR(100) NOT NULL COMMENT '密码',
  company_id INT NOT NULL COMMENT '公司ID',
  login_status TINYINT(4) NOT NULL DEFAULT '1' COMMENT '登录状态:0-状态正常; 1-状态异常，请重新登录',
  cookie text COMMENT 'cookie字符串',
  status TINYINT(4) NOT NULL DEFAULT '0' COMMENT '状态：0-正常，1-失效',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (h5_account_id),
  CONSTRAINT uk UNIQUE (company_id, h5_id, h5_account_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'H5账户表';

-- H5应用表：
CREATE TABLE IF NOT EXISTS ac_h5_app(
  h5_app_id INT NOT NULL AUTO_INCREMENT COMMENT 'H5应用ID，自增主键',
  h5_app_name VARCHAR(64) NOT NULL COMMENT 'H5应用名',
  h5_app_code VARCHAR(64) NOT NULL COMMENT 'H5应用编号（平台分配的编号）',
  h5_account_id INT NOT NULL COMMENT 'H5账户ID',
  company_id INT NOT NULL COMMENT '公司ID',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (h5_app_id),
  CONSTRAINT uk UNIQUE (company_id, h5_account_id, h5_app_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'H5应用表';

-- 初始化H5平台表
insert into ac_h5(h5_id, h5_name, homepage, create_time, update_time) values
(5 ,'MTA-腾讯移动统计', 'http://mta.qq.com', now(), now());
-- 初始化H5账户表
insert into ac_h5_account(h5_account_id, h5_id, h5_account, password, company_id, create_time, update_time) values 
(12, 5, '2330268629', 'y5H037o4WgH30rOmpg8gsw==', 2, now(), now());
-- 初始化H5应用表
insert into ac_h5_app(h5_app_id, h5_app_name, h5_app_code, h5_account_id, company_id, create_time, update_time) values
(1, '塔王之王', '500312391', 12, 2, now(), now());

-- CP平台表：
CREATE TABLE IF NOT EXISTS ac_cp(
  cp_id INT NOT NULL AUTO_INCREMENT COMMENT 'CP平台ID，自增主键',
  cp_name VARCHAR(64) NOT NULL COMMENT 'CP平台名称',
  homepage VARCHAR(128) NOT NULL COMMENT '主页',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (cp_id),
  CONSTRAINT uk UNIQUE (cp_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'CP平台表';

-- CP账户表：
CREATE TABLE IF NOT EXISTS ac_cp_account(
  cp_account_id INT NOT NULL AUTO_INCREMENT COMMENT 'CP账户ID，自增主键',
  cp_id INT NOT NULL DEFAULT 0 COMMENT 'CP平台ID',
  cp_account VARCHAR(64) NOT NULL COMMENT 'CP账户',
  password VARCHAR(100) NOT NULL COMMENT '密码',
  company_id INT NOT NULL COMMENT '公司ID',
  login_status TINYINT(4) NOT NULL DEFAULT '1' COMMENT '登录状态:0-状态正常; 1-状态异常，请重新登录',
  cookie text COMMENT 'cookie字符串',
  status TINYINT(4) NOT NULL DEFAULT '0' COMMENT '状态：0-正常，1-失效',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (cp_account_id),
  CONSTRAINT uk UNIQUE (company_id, cp_id, cp_account_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'CP账户表';

-- CP应用表：
CREATE TABLE IF NOT EXISTS ac_cp_app(
  cp_app_id INT NOT NULL AUTO_INCREMENT COMMENT 'CP应用ID，自增主键',
  cp_app_name VARCHAR(64) NOT NULL COMMENT 'CP应用名',
  cp_app_code VARCHAR(64) NOT NULL COMMENT 'CP应用编号（平台分配的编号）',
  cp_account_id INT NOT NULL COMMENT 'CP账户ID',
  company_id INT NOT NULL COMMENT '公司ID',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (cp_app_id),
  CONSTRAINT uk UNIQUE (company_id, cp_account_id, cp_app_code)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT 'CP应用表';

-- 初始化CP平台表
insert into ac_cp(cp_id, cp_name, homepage, create_time, update_time) values
(1 ,'旗乐', 'http://twzw.center.gzyouai.com/qile/index.html', now(), now());
-- 初始化CP账户表
insert into ac_cp_account(cp_account_id, cp_id, cp_account, password, company_id, create_time, update_time) values 
(1, 1, 'longyu', 'yOSbYvzGBoVWICbXvFTpfg==', 2, now(), now());
-- 初始化CP应用表
insert into ac_cp_app(cp_app_id, cp_app_name, cp_app_code, cp_account_id, company_id, create_time, update_time) values
(1, '龙宇-塔王之王', 'longyu-twzw', 1, 2, now(), now());


-- Tracking应用表：
CREATE TABLE IF NOT EXISTS ac_tracking_app(
  app_id VARCHAR(64) NOT NULL COMMENT 'Tracking应用ID（平台分配的编号）',
  app_name VARCHAR(64) NOT NULL COMMENT 'Tracking应用名',
  company_id INT NOT NULL COMMENT '公司ID',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (app_id),
  CONSTRAINT uk UNIQUE (company_id, app_name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT 'Tracking应用表';


-- Tracking账号表：
CREATE TABLE IF NOT EXISTS ac_tracking_account(
  company_id INT NOT NULL COMMENT '公司ID',
  tracking_account VARCHAR(64) NOT NULL COMMENT 'Tracking账号：统一登录userName，即登录tracking的账号',
  init_start_date DATE NOT NULL COMMENT '激活日期初始化值',
  status TINYINT(4) NOT NULL DEFAULT '0' COMMENT '状态：0-正常，1-失效',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (company_id),
  CONSTRAINT uk UNIQUE (tracking_account)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT 'Tracking账号表';

-- 初始化Tracking账号表
INSERT INTO ac_tracking_account (company_id, tracking_account, init_start_date, status, create_time, update_time) 
VALUES (3, 'dataeye@huiyaohuyu.com', '2017-03-15', 0, now(), now());