insert into ac_menu(menu_id,menu_name,create_time,update_time) values('5-3','LTV分析',current_timestamp(),current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('5-3', 'LTV分析', '/ad/payback/ltv/daily.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('5-3', 'LTV趋势图', '/ad/payback/ltv/trend.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('5-3', 'LTV趋势图', '/ad/payback/ltv/daily/download.do', current_timestamp(), current_timestamp());

-- LTV
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('5-10', 'ltv_7days', '7日LTV', 1, 1, null, now(), now());
INSERT INTO ac_indicator (indicator_id, indicator_key, indicator_name, selected, editable, remark, create_time, update_time) VALUES ('5-11', 'ltv_30days', '30日LTV', 1, 1, null, now(), now());
UPDATE `de_adt`.`ac_indicator` SET `indicator_key`='ltv_1day' WHERE `indicator_id`='5-9';

-- 指标显示偏好
alter table ac_indicator_habit modify `indicator_habit` varchar(256) NOT NULL COMMENT '指标习惯';

-- 更新已存在账号的菜单权限
update ac_system_account set menu_permission = '1,2,3-1,3-2,4-1,4-2,5-1,5-3,6,8-1,8-2' where menu_permission = '1,2,3-1,3-2,4-1,4-2,5,6,8-1,8-2';
update ac_system_account set menu_permission = '3-1,3-2,4-1,4-2,4-3,5-1,5-3,6,7-1,7-2' where menu_permission = '3-1,3-2,4-1,4-2,4-3,5,6,7-1,7-2';
update ac_system_account set menu_permission = '3-1,3-2,4-3,4-1,4-2,5-1,5-3,6,7-1,7-2' where menu_permission = '3-1,3-2,4-3,4-1,4-2,5,6,7-1,7-2';
update ac_system_account set menu_permission = '4-1,3-1,5-1,5-3' where menu_permission = '4-1,3-1,5';
update ac_system_account set menu_permission = '1,2,3-1,3-2,4-1,4-2,5-1,5-3,6,7-1,8-1,8-2' where menu_permission = '1,2,3-1,3-2,4-1,4-2,5,6,7-1,8-1,8-2';
update ac_system_account set menu_permission = '3-1,3-2,4-1,4-2,4-3,5-1,5-3,6,7-2,7-1' where menu_permission = '3-1,3-2,4-1,4-2,4-3,5,6,7-2,7-1';
update ac_system_account set menu_permission = '5-1,5-3,4-1,3-2,3-1,7-1,7-2' where menu_permission = '5,4-1,3-2,3-1,7-1,7-2';
update ac_system_account set menu_permission = '4-1,4-3,5-1,5-3,6,3-1,3-2,7-1,7-2' where menu_permission = '4-1,4-3,5,6,3-1,3-2,7-1,7-2';
update ac_system_account set menu_permission = '3-1,3-2,4-1,4-2,4-3,5-1,5-3' where menu_permission = '3-1,3-2,4-1,4-2,4-3,5';
update ac_system_account set menu_permission = '5-1,5-3,3-1,3-2,4-3,4-1,4-2,7-1,7-2,6' where menu_permission = '5,3-1,3-2,4-3,4-1,4-2,7-1,7-2,6';
update ac_system_account set menu_permission = '3-1,5-1,5-3,4-3,4-1' where menu_permission = '3-1,5,4-3,4-1';

-- 更新回本分析菜单ID
update ac_menu_interface_mapping set menu_id = '5-1' where menu_id = '5';