-- add by luzhuyou 2017.09.11
-- 负责人和关注人记录在媒体账号表
alter table ac_medium_account add column follower_accounts VARCHAR(500) COMMENT '关注人账号(多个以逗号分隔)' after login_status;
alter table ac_medium_account add column responsible_accounts VARCHAR(500) COMMENT '负责人账号(多个以逗号分隔)' after login_status; 

-- 在菜单接口映射表中添加字段
alter table ac_menu_interface_mapping add column data_permission TINYINT NOT NULL COMMENT '数据权限:0-不限制，1-负责人&关注人权限，2-负责人权限' after interface_url;

-- 新增表结构
create table if not exists ac_dev_authorized_product
(
    account_id INT NOT NULL COMMENT '账户ID',
    company_id INT NOT NULL COMMENT '公司ID',
    product_ids VARCHAR(100) NOT NULL DEFAULT '' COMMENT '产品（如果多个，以逗号分隔）',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (account_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='开发工程师被授权产品表';

create table ac_menu_interface_mapping_20170911 as select * from ac_menu_interface_mapping;
Delete from ac_menu_interface_mapping where menu_id='7-2' and interface_url = '/ad/mediumaccount/modify.do';

-- 系统管理开发需要的相关sql
-- 1、ac_menu表的修改
update ac_menu set menu_id='1-1',menu_name = '媒体账号管理'  where menu_id='7-2';
update ac_menu set menu_id='1-2',menu_name = '产品管理'  where menu_id='2';
update ac_menu set menu_id='1-3',menu_name = '子账号管理'  where menu_id='1';
update ac_menu set menu_id='2',menu_name = '监测链接'  where menu_id='7-1';
INSERT INTO ac_menu(menu_id, menu_name, create_time, update_time) VALUES (1, '系统管理', now(), now());
-- 2.ac_menu_interface_mapping修改
-- update ac_menu_interface_mapping set menu_id = '1-1' where interface_url='/ad/mediumaccount/query.do';
-- update ac_menu_interface_mapping set menu_id='common' where interface_url ='/ad/mediumaccount/queryMediumAccountForOptimizerSelector.do';
-- update ac_menu_interface_mapping set menu_id='common' where interface_url ='/ad/medium/queryMediumMappingForSelector.do';

-- 新增加的接口
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('common', '下拉框查询媒体分组和媒体帐号列表（投放分配）', '/ad/mediumaccount/queryMediumAccountGroupForSelector.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('common', '下拉框查询负责的产品', '/ad/product/queryProductForSelectorByResponsible.do', 0,null, now(), now());

-- update ac_menu_interface_mapping set menu_id='1-1' where menu_id='7-2';
-- update ac_menu_interface_mapping set menu_id='1-2' where menu_id='2';
-- update ac_menu_interface_mapping set menu_id='1-3' where menu_id='1';
-- update ac_menu_interface_mapping set menu_id='2' where menu_id='7-1';

-- 修改系统帐号的菜单权限
update ac_system_account set menu_permission = concat('flag',menu_permission) where menu_permission  like  '1,%';
update ac_system_account set menu_permission = replace(menu_permission,'flag1,','1-3,') where  menu_permission  like  'flag1,%';
update ac_system_account set menu_permission = replace(menu_permission,',2,',',1-2,') where menu_permission  like '%,2,%';
update ac_system_account set menu_permission = replace(menu_permission,'7-1','2') where menu_permission  like '%7-1%' ;
update ac_system_account set menu_permission = replace(menu_permission,'7-2','1-1') where menu_permission  like  '%7-2%';
update ac_system_account set menu_permission = concat('1-1,',menu_permission) where account_role = 2 and menu_permission not like  '%1-1%';
update ac_system_account set menu_permission = replace(menu_permission,',5,',',5-1,5-2,') where menu_permission  like  '%,5,%';

-- select * from ac_medium_account where follower_accounts like '%,' ;
update ac_medium_account set follower_accounts=CONCAT(follower_accounts,'0123456Dflag') where follower_accounts like '%,' ;
update ac_medium_account set follower_accounts=replace(follower_accounts,',0123456Dflag','') where follower_accounts like '%,0123456Dflag' ;


-- add by luzhuyou 2017.09.11 
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '1-1' where interface_url = '/ad/mediumaccount/queryBySystemAccount.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '1-1' where interface_url = '/ad/mediumaccount/query.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-1' where interface_url = '/ad/mediumaccount/add.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-1' where interface_url = '/ad/mediumaccount/modify.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-1' where interface_url = '/ad/product/modifyRefApp.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-1' where interface_url = '/ad/systemaccount/manager-follower-candidates.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-1' where interface_url = '/ad/medium/add-custom.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-2' where interface_url = '/ad/product/add.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-2' where interface_url = '/ad/product/modify.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-2' where interface_url = '/ad/product/queryAppForSelector.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/queryAccount.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/addAccount.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/modifyAccount.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/deleteAccount.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/resetPassword.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/queryGroup.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/queryGroupAccount.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/addGroup.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/modifyGroup.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/deleteGroup.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/deleteGroupMember.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/indicators.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/getAccount.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '1-3' where interface_url = '/ad/systemaccount/group-leader-candidates.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '3-1' where interface_url = '/ad/realtimedelivery/queryDelivery.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '3-1' where interface_url = '/ad/realtimedelivery/trend.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '3-1' where interface_url = '/ad/realtimedelivery/indicators.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '3-1' where interface_url = '/ad/realtimedelivery/period.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '3-1' where interface_url = '/ad/realtimedelivery/confAlert.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '3-1' where interface_url = '/ad/realtimedelivery/getAlert.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '3-1' where interface_url = '/ad/realtimedelivery/confAlertEmail.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '3-1' where interface_url = '/ad/realtimedelivery/bid.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '3-1' where interface_url = '/ad/realtimedelivery/plan-switch.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '3-1' where interface_url = '/ad/real-time/indicator-preference.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '3-1' where interface_url = '/ad/real-time/indicator-preference/update.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '3-1' where interface_url = '/ad/realtimedelivery/op-log.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '3-1' where interface_url = '/ad/real-time/ups-and-downs-preference.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '3-1' where interface_url = '/ad/real-time/ups-and-downs-preference/update.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '3-2' where interface_url = '/ad/balance/list.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '3-2' where interface_url = '/ad/balance/alert/get.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '3-2' where interface_url = '/ad/balance/alert/save.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '3-2' where interface_url = '/ad/balance/trend.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '3-2' where interface_url = '/ad/manager/medium-accounts.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '3-2' where interface_url = '/ad/manager/mediums.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '4-1' where interface_url = '/ad/report/adReport.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '4-1' where interface_url = '/ad/report/downloadTable.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '4-1' where interface_url = '/ad/delivery/indicator-preference.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '4-1' where interface_url = '/ad/delivery/indicator-preference/update.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '4-2' where interface_url = '/ad/delivery/trend/medium-account.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '4-2' where interface_url = '/ad/delivery/trend/plan.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '4-2' where interface_url = '/ad/delivery/trend/indicators.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/total.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/cost/product.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/cost/medium.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/download/product.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/download/medium.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/activation/product.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/activation/medium.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/registration/product.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/registration/medium.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/dau/product.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-3' where interface_url = '/ad/delivery/key-indicator/dau/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-1' where interface_url = '/ad/payback/daily.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-1' where interface_url = '/ad/payback/daily/download.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-1' where interface_url = '/ad/payback/daily/trend.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-1' where interface_url = '/ad/payback/monthly.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-1' where interface_url = '/ad/payback/monthly/download.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '6' where interface_url = '/ad/association/query.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '6' where interface_url = '/ad/association/modify.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '6' where interface_url = '/ad/association/query-his.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '6' where interface_url = '/ad/association/modify-his.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '2' where interface_url = '/ad/campaign/queryChannelParams.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '2' where interface_url = '/ad/campaign/query.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '2' where interface_url = '/ad/campaign/queryForSelector.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '2' where interface_url = '/ad/campaign/createCampaignBefore.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '2' where interface_url = '/ad/campaign/add.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '2' where interface_url = '/ad/campaign/modify.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '2' where interface_url = '/ad/campaign/delete.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = 'common' where interface_url = '/ad/mediumaccount/queryMediumAccountForOptimizerSelector.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/medium/queryMediumMappingForSelector.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/overall.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cost/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/recharge/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/paybackrate/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/balance/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cpa/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/dau/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cost/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/recharge/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/paybackrate/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/balance/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cpa/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/dau/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cost/os.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/recharge/os.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/paybackrate/os.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cpa/os.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/dau/os.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cost/trend/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/recharge/trend/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/balance/trend/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cpa/trend/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/dau/trend/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cost/trend/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/recharge/trend/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/balance/trend/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cpa/trend/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/dau/trend/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cost/trend/os.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/recharge/trend/os.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/cpa/trend/os.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-1' where interface_url = '/ad/result/dau/trend/os.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '8-2' where interface_url = '/ad/result/contrastive-analysis.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '9' where interface_url = '/ad/product/sdk-validate.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/user/logout.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/systemaccount/modifyPassword.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = 'common' where interface_url = '/ad/plan/queryPlanForSelector.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = 'common' where interface_url = '/ad/product/queryProductForSelector.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/medium/queryMediumForSelector.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = 'common' where interface_url = '/ad/mediumaccount/queryMediumAccountForSelector.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/systemaccount/queryAccountForSelector.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/pkg/queryPkgForSelector.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/page/queryPageForSelector.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/product/query.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/mediumaccount/optimizers.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = 'common' where interface_url = '/ad/result/contrastive-analysis/optimizers.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/result/contrastive-analysis/products.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/systemaccount/optimizers.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = 'common' where interface_url = '/ad/delivery/plan/list.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/productcategory/query.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/medium/queryMediumForCrawlerPage.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/medium/queryMediumForSelectorById.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'public' where interface_url = '/user/login.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'public' where interface_url = '/ad/cache/refresh.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'public' where interface_url = '/update/params.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'public' where interface_url = '/ad/association/sync.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'public' where interface_url = '/tools/aesEncrypt.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'public' where interface_url = '/tools/aesDecrypt.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'public' where interface_url = '/tools/md5.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'public' where interface_url = '/tools/md5Base64.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'public' where interface_url = '/uni/reg.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'public' where interface_url = '/uni/reg-tracking-user.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-1' where interface_url = '/ad/medium-data/list.do';
Update ac_menu_interface_mapping set data_permission = 2, menu_id = '4-1' where interface_url = '/ad/medium-data/update.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '4-2' where interface_url = '/ad/delivery/trend/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '4-2' where interface_url = '/ad/delivery/trend/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-2' where interface_url = '/ad/payback-contrastive/plan.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-2' where interface_url = '/ad/payback-contrastive/medium-account.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-2' where interface_url = '/ad/payback-contrastive/medium.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-2' where interface_url = '/ad/payback-contrastive/product.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-3' where interface_url = '/ad/payback/ltv/daily.do';
Update ac_menu_interface_mapping set data_permission = 1, menu_id = '5-3' where interface_url = '/ad/payback/ltv/trend.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = '5-3' where interface_url = '/ad/payback/ltv/daily/download.do';
Update ac_menu_interface_mapping set data_permission = 0, menu_id = 'common' where interface_url = '/ad/mediumaccount/queryMediumAccountGroupForSelector.do';

---  ac_menu_interface_mapping   interface_url = '/ad/mediumaccount/modify.do' 数据有两条，需要删除一条，
