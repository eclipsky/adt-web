-- 菜单表
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('8', '效果看板', null, now(), now());
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('8-1', '核心指标', null, now(), now());
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('8-2', '对比分析', null, now(), now());

-- 效果看板菜单
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '查询某个媒体账号某个产品的优化师ADT账号列表', '/ad/mediumaccount/optimizers.do', now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common','关联表中优化师列表','/ad/result/contrastive-analysis/optimizers.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common','关联表中的产品列表','/ad/result/contrastive-analysis/products.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','获取核心指标通体概况','/ad/result/overall.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按媒体分组统计消耗占比','/ad/result/cost/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按媒体分组统计充值占比','/ad/result/recharge/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按媒体分组统计回本率','/ad/result/paybackrate/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按媒体分组统计余额','/ad/result/balance/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按媒体分组统计CPA','/ad/result/cpa/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按媒体分组统计DAU','/ad/result/dau/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按产品分组统计消耗占比','/ad/result/cost/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按产品分组统计充值占比','/ad/result/recharge/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按产品分组统计回本率','/ad/result/paybackrate/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按产品分组统计余额','/ad/result/balance/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按产品分组统计CPA','/ad/result/cpa/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按产品分组统计DAU','/ad/result/dau/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按平台分组统计消耗占比','/ad/result/cost/os.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按平台分组统计充值占比','/ad/result/recharge/os.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按平台分组统计回本率','/ad/result/paybackrate/os.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按平台分组统计CPA','/ad/result/cpa/os.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','按平台分组统计DAU','/ad/result/dau/os.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','消耗日趋势图-分媒体','/ad/result/cost/trend/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','充值日趋势图-分媒体','/ad/result/recharge/trend/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','余额日趋势图-分媒体','/ad/result/balance/trend/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','CPA日趋势图-分媒体','/ad/result/cpa/trend/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','DAU日趋势图-分媒体','/ad/result/dau/trend/medium.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','消耗日趋势图-分产品','/ad/result/cost/trend/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','充值日趋势图-分产品','/ad/result/recharge/trend/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','余额日趋势图-分产品','/ad/result/balance/trend/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','CPA日趋势图-分产品','/ad/result/cpa/trend/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','DAU日趋势图-分产品','/ad/result/dau/trend/product.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','消耗日趋势图-分平台','/ad/result/cost/trend/os.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','充值日趋势图-分平台','/ad/result/recharge/trend/os.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','CPA日趋势图-分平台','/ad/result/cpa/trend/os.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-1','DAU日趋势图-分平台','/ad/result/dau/trend/os.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('8-2','对比分析趋势图','/ad/result/contrastive-analysis.do',now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3-2', '账户余额', '/ad/balance/list.do', now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '优化师ADT账号列表', '/ad/systemaccount/optimizers.do', now(), now());

create table ac_medium_account_20170427 as select * from ac_medium_account;
alter table ac_medium_account drop column system_account_name_list;
alter table ac_medium_account drop column optimizer_system_account;
alter table ad_plan_relation add column optimizer_system_account VARCHAR(60) COMMENT '(负责人)优化师ADT账号' after pkg_id;

-- 更新媒体账号关联关系
update ac_medium_account_relation set optimizer_system_account = system_account_name_list;

-- 更新企业账号权限
update ac_system_account set menu_permission = '1,2,5,6,8-1' where account_role = 2 and account_alias like '%企业账号';
update ac_system_account set menu_permission = '1,2,3,4-1,5,6,7,8-1' where account_role = 2 and account_alias like 'DE%' and menu_permission = '1,2,3,4-1,5,6,7';



--2018-02-22
alter table ad_campaign add column status TINYINT DEFAULT '0' NOT NULL COMMENT '状态：0-正常，1-失效' after app_treasure;
insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time) values('2','监测链接下载当前表格','/ad/campaign/downloadTable.do',0,now(),now());
insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time) values('2','批量更新监测链接','/ad/campaign/batch/modify.do',0,now(),now());
insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time) values('2','批量删除推广计划','/ad/campaign/batch/delete.do',0,now(),now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, data_permission, remark, create_time, update_time) VALUES ('6', '批量更新关联规则', '/ad/association/batchModify.do', 0, null, now(), now());


 