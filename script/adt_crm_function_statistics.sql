update crm_menu set menu_id='2-1', menu_name='活跃统计' where menu_id='2';
INSERT INTO crm_menu(menu_id, menu_name, create_time, update_time) VALUES ('2', '统计分析', now(), now());
INSERT INTO crm_menu(menu_id, menu_name, create_time, update_time) VALUES ('2-2', '功能点击', now(), now());
update crm_menu_interface_mapping set menu_id='2-1'  where menu_id='2';

CREATE TABLE `ad_event` (
   `event_id` smallint(6) NOT NULL COMMENT '事件ID,事件英文名称',
   `event_name` varchar(32) NOT NULL COMMENT '事件显示名称',
   `event_group` smallint(6) NOT NULL COMMENT '事件分组:1:按钮,2:页面访问',
   `menu_id` varchar(8) NOT NULL COMMENT '菜单ID',
   `remark` varchar(255) DEFAULT NULL COMMENT '备注',
   `priority` smallint(6) DEFAULT '0' COMMENT '优先级',
   PRIMARY KEY (`event_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ADT统计-事件';

 CREATE TABLE `ad_event_detail` (
   `account_id` int(11) NOT NULL COMMENT '登录账号ID',
   `stat_date` date NOT NULL COMMENT '事件发生日期',
   `stat_time` time NOT NULL COMMENT '事件发生时间',
   `event_id` smallint(6) DEFAULT NULL COMMENT '事件ID',
   `menu_id` varchar(8) NOT NULL COMMENT '菜单ID'
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ADT统计-事件详细';

 
 INSERT INTO `ad_event` VALUES (1, '媒体账号管理', 2, '1-1', '媒体账号管理（主账号的）', 2);
INSERT INTO `ad_event` VALUES (2, '新增', 2, '1-1', '新增媒体账号', 1);
INSERT INTO `ad_event` VALUES (3, '编辑', 2, '1-1', '编辑媒体账号', 1);
INSERT INTO `ad_event` VALUES (4, '产品管理', 2, '1-2', '产品', 2);
INSERT INTO `ad_event` VALUES (5, '新增', 2, '1-2', '新增产品', 1);
INSERT INTO `ad_event` VALUES (6, '编辑', 2, '1-2', '编辑产品', 1);
INSERT INTO `ad_event` VALUES (7, '子账号管理', 2, '1-3', '子账号', 2);
INSERT INTO `ad_event` VALUES (8, '新增', 2, '1-3', '新增子账号', 1);
INSERT INTO `ad_event` VALUES (9, '编辑', 2, '1-3', '编辑子账号', 1);
INSERT INTO `ad_event` VALUES (10, '自然流量', 1, '8-1', '核心指标自然流量', 0);
INSERT INTO `ad_event` VALUES (11, '累计消耗', 2, '8-1', '核心指标累计消耗', 1);
INSERT INTO `ad_event` VALUES (12, '累计充值', 2, '8-1', '核心指标累计充值', 1);
INSERT INTO `ad_event` VALUES (13, '回本率', 2, '8-1', '核心指标回本率', 1);
INSERT INTO `ad_event` VALUES (14, '账户余额', 2, '8-1', '核心指标媒体账户余额', 1);
INSERT INTO `ad_event` VALUES (15, 'CPA', 2, '8-1', '核心指标cpa', 1);
INSERT INTO `ad_event` VALUES (16, 'DAU', 2, '8-1', '核心指标dau', 1);
INSERT INTO `ad_event` VALUES (17, '表格视图', 1, '8-1', '核心指标表格视图', 0);
INSERT INTO `ad_event` VALUES (18, '表格视图', 1, '8-2', '子账号分析表格视图', 0);
INSERT INTO `ad_event` VALUES (19, '指标说明', 1, '3-1', '实时操盘指标说明', 0);
INSERT INTO `ad_event` VALUES (20, '全屏', 1, '3-1', '全屏', 0);
INSERT INTO `ad_event` VALUES (21, '计划开关', 1, '3-1', '计划开关', 0);
INSERT INTO `ad_event` VALUES (22, '出价', 1, '3-1', '出价', 0);
INSERT INTO `ad_event` VALUES (23, '编辑媒体数据', 2, '3-1', '实时编辑媒体数据', 0);
INSERT INTO `ad_event` VALUES (24, '自然流量', 1, '3-1', '实时过滤自然流量', 0);
INSERT INTO `ad_event` VALUES (25, '过滤媒体数据为空', 1, '3-1', '实时过滤媒体数据为空的计划', 0);
INSERT INTO `ad_event` VALUES (26, '指标控制器', 1, '3-1', '实时指标控制器', 0);
INSERT INTO `ad_event` VALUES (27, '涨跌幅', 1, '3-1', '涨跌幅', 0);
INSERT INTO `ad_event` VALUES (28, '批量告警', 1, '3-1', '批量告警', 0);
INSERT INTO `ad_event` VALUES (29, '日志', 2, '3-1', '实时日志', 0);
INSERT INTO `ad_event` VALUES (30, '告警', 1, '3-1', '实时告警', 0);
INSERT INTO `ad_event` VALUES (31, '跳转媒体', 1, '3-1', '跳转媒体', 0);
INSERT INTO `ad_event` VALUES (32, '趋势图', 2, '3-2', '实时余额趋势图', 0);
INSERT INTO `ad_event` VALUES (33, '自然流量', 1, '4-3', '关键指标自然流量', 0);
INSERT INTO `ad_event` VALUES (34, '累计消耗', 2, '4-3', '关键指标累计消耗', 1);
INSERT INTO `ad_event` VALUES (35, '累计下载', 2, '4-3', '关键指标累计下载', 1);
INSERT INTO `ad_event` VALUES (36, '累计激活', 2, '4-3', '关键指标累计激活', 1);
INSERT INTO `ad_event` VALUES (37, '累计注册', 2, '4-3', '关键指标累计注册', 1);
INSERT INTO `ad_event` VALUES (38, 'DAU', 2, '4-3', '关键指标dau', 1);
INSERT INTO `ad_event` VALUES (39, '指标下钻', 1, '4-3', '关键指标下钻（图表详情）', 0);
INSERT INTO `ad_event` VALUES (40, '表格视图', 1, '4-3', '关键指标表格视图', 0);
INSERT INTO `ad_event` VALUES (41, '指标说明', 1, '4-1', '投放日报指标说明', 0);
INSERT INTO `ad_event` VALUES (42, '按期间汇总', 1, '4-1', '日报按期间汇总', 0);
INSERT INTO `ad_event` VALUES (43, '媒体标签页', 2, '4-1', '日报媒体标签页', 0);
INSERT INTO `ad_event` VALUES (44, '过滤消耗为0', 1, '4-1', '日报过滤消耗为0的计划', 0);
INSERT INTO `ad_event` VALUES (45, '自然流量', 1, '4-1', '日报过滤自然流量', 0);
INSERT INTO `ad_event` VALUES (46, '过滤未关联', 1, '4-1', '日报过滤未关联的计划', 0);
INSERT INTO `ad_event` VALUES (47, '过滤媒体数据为空', 1, '4-1', '日报过滤媒体数据为空的计划', 0);
INSERT INTO `ad_event` VALUES (48, '导出报表', 1, '4-1', '日报导出报表', 0);
INSERT INTO `ad_event` VALUES (49, '媒体账号标签页', 2, '4-2', '活动对比媒体账号tab', 0);
INSERT INTO `ad_event` VALUES (50, '媒体标签页', 2, '4-2', '活动对比媒体tab', 0);
INSERT INTO `ad_event` VALUES (51, '产品标签页', 2, '4-2', '活动对比产品tab', 0);
INSERT INTO `ad_event` VALUES (52, '10分钟间隔', 1, '4-2', '活动对比10分钟间隔', 0);
INSERT INTO `ad_event` VALUES (53, '20分钟间隔', 1, '4-2', '活动对比20分钟间隔', 0);
INSERT INTO `ad_event` VALUES (54, '30分钟间隔', 1, '4-2', '活动对比30分钟间隔', 0);
INSERT INTO `ad_event` VALUES (55, '1小时间隔', 1, '4-2', '活动对比1小时间隔', 0);
INSERT INTO `ad_event` VALUES (56, '按周', 1, '4-2', '活动对比按周', 0);
INSERT INTO `ad_event` VALUES (57, '按月', 1, '4-2', '活动对比按月', 0);
INSERT INTO `ad_event` VALUES (58, '表格视图', 1, '4-2', '活动对比表格视图', 0);
INSERT INTO `ad_event` VALUES (59, '指标说明', 1, '5-1', '回本日报指标说明', 0);
INSERT INTO `ad_event` VALUES (60, '按月', 1, '5-1', '回本日报按月', 0);
INSERT INTO `ad_event` VALUES (61, '导出报表', 1, '5-1', '回本日报导出报表', 0);
INSERT INTO `ad_event` VALUES (62, '趋势图', 1, '5-1', '回本日报趋势图', 0);
INSERT INTO `ad_event` VALUES (63, '媒体账号标签页', 2, '5-2', '回本对比媒体账号tab', 0);
INSERT INTO `ad_event` VALUES (64, '媒体标签页', 2, '5-2', '回本对比媒体tab', 0);
INSERT INTO `ad_event` VALUES (65, '产品标签页', 2, '5-2', '回本对比产品tab', 0);
INSERT INTO `ad_event` VALUES (66, '按周', 1, '5-2', '回本对比按周', 0);
INSERT INTO `ad_event` VALUES (67, '按月', 1, '5-2', '回本对比按月', 0);
INSERT INTO `ad_event` VALUES (68, '表格视图', 1, '5-2', '回本对比表格视图', 0);
INSERT INTO `ad_event` VALUES (69, '指标说明', 1, '5-3', 'ltv指标说明', 0);
INSERT INTO `ad_event` VALUES (70, '自然流量', 1, '5-3', 'ltv过滤自然流量', 0);
INSERT INTO `ad_event` VALUES (71, '导出报表', 1, '5-3', 'ltv导出报表', 0);
INSERT INTO `ad_event` VALUES (72, '趋势图', 1, '5-3', 'ltv趋势图', 0);
INSERT INTO `ad_event` VALUES (73, '未绑定计划', 1, '6', '关联规则未绑定计划', 0);
INSERT INTO `ad_event` VALUES (74, '编辑', 1, '6', '编辑关联规则', 0);
INSERT INTO `ad_event` VALUES (75, '关联历史', 2, '6', '关联规则历史', 0);
INSERT INTO `ad_event` VALUES (76, '关联历史更新', 1, '6', '关联规则历史更新', 0);
INSERT INTO `ad_event` VALUES (78, '新增', 2, '2', '新增监测链接', 0);
INSERT INTO `ad_event` VALUES (79, '编辑', 2, '2', '编辑监测链接', 0);
INSERT INTO `ad_event` VALUES (80, '保存', 1, '2', '新增修改监测链接保存', 0);
INSERT INTO `ad_event` VALUES (81, '复制监测链接', 1, '2', '复制监测链接', 0);
INSERT INTO `ad_event` VALUES (82, '复制计划名称', 1, '2', '复制计划名称', 0);
INSERT INTO `ad_event` VALUES (83, '编辑计划名称', 1, '2', '编辑计划名称', 0);
INSERT INTO `ad_event` VALUES (84, '复制下载地址', 1, '2', '复制下载地址', 0);
INSERT INTO `ad_event` VALUES (85, '编辑下载地址', 1, '2', '编辑下载地址', 0);
INSERT INTO `ad_event` VALUES (86, '删除监测链接', 1, '2', '删除监测链接', 0);
INSERT INTO `ad_event` VALUES (87, '落地页信息', 1, '2', '落地页信息', 0);
INSERT INTO `ad_event` VALUES (88, '复制以上信息', 1, '2', '复制落地页弹窗信息', 0);
INSERT INTO `ad_event` VALUES (89, '编辑媒体账号', 1, '7-2', '编辑媒体账号（子账号的媒体账号管理，菜单id是1-1，但埋点统计要求企业账号和子账号的媒体账号管理分开统计，两个菜单id一样，目前用旧的媒体账号管理菜单id 7-2来记录）', 0);
INSERT INTO `ad_event` VALUES (90, '操作手册', 2, '100', '操作手册', 0);
INSERT INTO `ad_event` VALUES (91, '编辑媒体数据', 2, '4-1', '日报编辑媒体数据', 0);
INSERT INTO `ad_event` VALUES (92, '查看', 1, '1-1', '查看媒体账号表格展开', 0);
INSERT INTO `ad_event` VALUES (93, '查看', 1, '1-2', '查看产品表格展开', 0);
INSERT INTO `ad_event` VALUES (94, '查看', 1, '1-3', '查看子账号管理表格展开', 0);
INSERT INTO `ad_event` VALUES (95, '核心指标', 2, '8-1', '核心指标', 2);
INSERT INTO `ad_event` VALUES (96, '子账号分析', 2, '8-2', '子账号分析', 2);
INSERT INTO `ad_event` VALUES (97, '实时操盘', 2, '3-1', '实时操盘', 2);
INSERT INTO `ad_event` VALUES (98, '账户余额', 2, '3-2', '账户余额', 2);
INSERT INTO `ad_event` VALUES (99, '关键指标', 2, '4-3', '关键指标', 2);
INSERT INTO `ad_event` VALUES (100, '投放日报', 2, '4-1', '投放日报', 2);
INSERT INTO `ad_event` VALUES (101, '活动对比', 2, '4-2', '活动对比', 2);
INSERT INTO `ad_event` VALUES (102, '回本日报', 2, '5-1', '回本日报', 2);
INSERT INTO `ad_event` VALUES (103, '回本对比', 2, '5-2', '回本对比', 2);
INSERT INTO `ad_event` VALUES (104, 'LTV分析', 2, '5-3', 'LTV分析', 2);
INSERT INTO `ad_event` VALUES (105, '关联规则', 2, '6', '关联规则', 2);
INSERT INTO `ad_event` VALUES (106, '监测链接', 2, '2', '监测链接', 2);
INSERT INTO `ad_event` VALUES (107, '账号管理', 2, '7-2', '账号管理（子账号的）', 2);
INSERT INTO `ad_event` VALUES (108, '趋势图', 2, '3-1', '实时趋势图', 0);

