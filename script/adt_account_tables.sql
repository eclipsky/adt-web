set names utf8;

drop table if exists ac_company;
drop table if exists ac_system_account;
drop table if exists ac_system_account_group;
drop table if exists ac_product;
drop table if exists ac_cp_share_rate;
drop table if exists ac_medium;
drop table if exists ac_medium_account;
drop table if exists ac_medium_discount_rate;
drop table if exists ac_menu;
drop table if exists ac_menu_interface_mapping;
drop table if exists ac_account_login_info;
drop table if exists ac_mail_history;

CREATE TABLE IF NOT EXISTS ac_company
(
    company_id INT NOT NULL AUTO_INCREMENT COMMENT '公司ID',
    company_name VARCHAR(200) NOT NULL COMMENT '公司名称',
    email VARCHAR(60) NOT NULL COMMENT '邮箱，登录账号',
    status TINYINT(4) DEFAULT '0' NOT NULL COMMENT '状态：0-正常，1-冻结，2-停用',
    remark  VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (company_id),
    CONSTRAINT email UNIQUE (email)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业信息表';
    

CREATE TABLE IF NOT EXISTS ac_system_account
(
    account_id INT NOT NULL AUTO_INCREMENT COMMENT '账户ID',
    company_id INT NOT NULL COMMENT '公司ID',
    account_name VARCHAR(60) NOT NULL COMMENT '账号名称(邮箱，登录账号)',
    account_alias VARCHAR(200) COMMENT '账号别名',
    password VARCHAR(100) NOT NULL COMMENT '密码',
    status TINYINT(4) DEFAULT '0' NOT NULL COMMENT '状态：0-正常，1-失效',
    account_role TINYINT(4) DEFAULT '0' NOT NULL DEFAULT '0' COMMENT '账户角色:0-普通系统账户，1-组长，2-企业账户',
	group_id INT DEFAULT '-1' NOT NULL COMMENT '分组ID(默认-1：未分组)',
    menu_permission VARCHAR(200) NOT NULL COMMENT '菜单权限',
    mobile VARCHAR(20) COMMENT '手机',
    remark  VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (account_id),
    CONSTRAINT company_id UNIQUE (company_id, account_name)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统账户表';


CREATE TABLE IF NOT EXISTS ac_system_account_group (
  group_id INT NOT NULL AUTO_INCREMENT COMMENT '分组ID',
  company_id INT NOT NULL COMMENT '公司ID',
  group_name VARCHAR(200) NOT NULL COMMENT '分组名称',
  leader_account VARCHAR(60) COMMENT '组长账号',
  member_accounts_md5 VARCHAR(30) COMMENT '组员账号DM5校验码，用于判断组员是否有变更，如有变更，更新具体组员账号',
  remark  VARCHAR(300) COMMENT '备注',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (group_id),
  UNIQUE KEY company_id (company_id, group_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统账户分组表';


CREATE TABLE IF NOT EXISTS ac_product (
  product_id INT NOT NULL AUTO_INCREMENT COMMENT '产品ID',
  company_id INT NOT NULL COMMENT '公司ID',
  product_name VARCHAR(200) NOT NULL COMMENT '产品名称',
  status TINYINT(4) DEFAULT '0' NOT NULL COMMENT '状态：0-正常，1-失效',
  remark  VARCHAR(300) COMMENT '备注',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (product_id),
  UNIQUE KEY company_id (company_id, product_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品表';



CREATE TABLE IF NOT EXISTS ac_cp_share_rate
(
    share_rate_id INT NOT NULL AUTO_INCREMENT COMMENT '分成率ID',
    company_id INT NOT NULL COMMENT '公司ID',
    product_id INT NOT NULL COMMENT '产品ID',
    share_rate DECIMAL(3,2) DEFAULT '1.00' NOT NULL COMMENT '分成率',
    effect_date DATE COMMENT '生效日期',
    invalid_date DATE COMMENT '失效日期',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (share_rate_id),
    CONSTRAINT product_id UNIQUE (product_id, company_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CP分成率表';


CREATE TABLE IF NOT EXISTS ac_medium
(
    medium_id INT NOT NULL AUTO_INCREMENT COMMENT '媒体ID',
    medium_name VARCHAR(40) NOT NULL COMMENT '媒体名称',
    medium_homepage VARCHAR(255) NOT NULL COMMENT '媒体主页',
    remark  VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (medium_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='媒体表';


CREATE TABLE IF NOT EXISTS ac_medium_account
(
    medium_account_id INT NOT NULL AUTO_INCREMENT COMMENT '媒体账户ID',
    company_id INT NOT NULL COMMENT '公司ID',
    product_id INT NOT NULL COMMENT '产品ID',
    medium_id INT NOT NULL COMMENT '媒体ID，对应媒体表ID',
    medium_account VARCHAR(200) NOT NULL COMMENT '媒体账号',
    password VARCHAR(100) NOT NULL COMMENT '密码',
    medium_account_alias VARCHAR(200) COMMENT '媒体账号别名',
    system_account_name_list VARCHAR(300) COMMENT '系统账号名称列表(多个以逗号分隔)',
    login_status TINYINT(4) NOT NULL DEFAULT '1' COMMENT '登录状态:0-状态正常; 1-状态异常，请重新登录',
	cookie text COMMENT 'cookie字符串',
    status TINYINT(4) DEFAULT '0' NOT NULL COMMENT '状态：0-正常，1-失效',
	skey VARCHAR(200) DEFAULT '' COMMENT '爬虫所需标识',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (medium_account_id),
    CONSTRAINT product_id UNIQUE (product_id, medium_id, medium_account)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='媒体账户表';


CREATE TABLE IF NOT EXISTS ac_medium_account_relation
(
    relation_id INT NOT NULL AUTO_INCREMENT COMMENT '媒体账号关联关系ID',
    company_id INT NOT NULL COMMENT '公司ID',
    product_id INT NOT NULL COMMENT '产品ID',
    medium_id INT NOT NULL COMMENT '媒体ID，对应媒体表ID',
    medium_account_id INT NOT NULL COMMENT '媒体账户ID',
    system_account_name_list VARCHAR(300) COMMENT '关注者ADT账号名称列表(多个以逗号分隔)',
    optimizer_system_account VARCHAR(300) COMMENT '优化师ADT账号名称列表(多个以逗号分隔)',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (relation_id),
    CONSTRAINT product_id UNIQUE (company_id, product_id, medium_account_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='媒体账号关联关系表';


CREATE TABLE IF NOT EXISTS ac_medium_discount_rate
(
    discount_rate_id INT NOT NULL AUTO_INCREMENT COMMENT '折扣率ID',
    medium_account_id INT NOT NULL COMMENT '媒体账户ID',
    discount_rate DECIMAL(3,2) DEFAULT '0' NOT NULL COMMENT '折扣率',
    effect_date DATE COMMENT '生效日期',
    invalid_date DATE COMMENT '失效日期',
    remark  VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (discount_rate_id),
    CONSTRAINT medium_account_id UNIQUE (medium_account_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='媒体折扣率表';


CREATE TABLE IF NOT EXISTS ac_menu
(
    menu_id VARCHAR(8) NOT NULL COMMENT '菜单ID',
    menu_name VARCHAR(200) NOT NULL COMMENT '菜单名称',
    remark  VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (menu_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';


CREATE TABLE IF NOT EXISTS ac_menu_interface_mapping
(
    mapping_id INT NOT NULL AUTO_INCREMENT COMMENT '映射ID',
    menu_id VARCHAR(8) NOT NULL COMMENT '菜单ID',
    interface_name VARCHAR(200) NOT NULL COMMENT '接口名称',
    interface_url VARCHAR(200) NOT NULL COMMENT '接口URL',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (mapping_id),
    CONSTRAINT interface_url UNIQUE (menu_id, interface_url)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单接口映射表';


CREATE TABLE IF NOT EXISTS ac_account_login_info
(
    login_id BIGINT NOT NULL AUTO_INCREMENT COMMENT '登录ID',
    account_id INT NOT NULL COMMENT '账户ID',
    login_ip VARCHAR(40) NOT NULL COMMENT '登录IP',
    status_code INT NOT NULL COMMENT '状态码',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    PRIMARY KEY (login_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统账号登录信息表';


CREATE TABLE IF NOT EXISTS ac_mail_history
(
    id BIGINT NOT NULL AUTO_INCREMENT COMMENT 'ID',
	mail_type TINYINT(4) DEFAULT '0' NOT NULL COMMENT '邮件类型：0-创建系统账号，1-密码重置，2-告警邮件',
    sender VARCHAR(60) NOT NULL COMMENT '发件人',
    recipients VARCHAR(300) NOT NULL COMMENT '收件人',
    send_status TINYINT(4) DEFAULT '0' NOT NULL COMMENT '发送状态：0-成功，1-失败',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    PRIMARY KEY (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮件发送历史表';
