set names utf8;

drop table if exists ad_plan;
drop table if exists ad_page;
drop table if exists ad_pkg;
-- drop table if exists ad_plan_stat_realtime;
drop table if exists ad_plan_stat_day;
-- drop table if exists ad_page_stat_realtime;
drop table if exists ad_page_stat_day;
-- drop table if exists ad_pkg_stat_realtime;
drop table if exists ad_pkg_stat_day;
drop table if exists ad_plan_relation;
drop table if exists ad_tracking_realtime;
drop table if exists ad_tracking_day;
drop table if exists ad_plan_alert_conf;
-- drop table if exists ad_pay_stat;
-- drop table if exists ad_payback_day;
-- drop table if exists ad_payback_month;

-- 投放计划表
-- 替换旧表：dc_ad_plan
CREATE TABLE IF NOT EXISTS ad_plan (
  plan_id int NOT NULL AUTO_INCREMENT COMMENT '计划ID，自增主键',
  plan_name varchar(50) NOT NULL DEFAULT '' COMMENT '计划名称',
  medium_id smallint  NOT NULL DEFAULT '0' COMMENT '媒体ID',
  medium_account_id int  NOT NULL DEFAULT '0' COMMENT '媒体帐号ID',
  company_id int NOT NULL DEFAULT '0' COMMENT '公司ID',
  os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
  status tinyint NOT NULL DEFAULT '0' COMMENT '0：正常，1：删除',
  remark varchar(128) COMMENT '备注',
  create_time datetime NOT NULL COMMENT  '创建时间',
  update_time datetime COMMENT  '更新时间',
  PRIMARY KEY (plan_id),
  UNIQUE KEY uk (company_id, medium_id, medium_account_id, plan_name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='投放计划表';


-- 落地页表
-- 替换旧表：dc_ad_land_url
CREATE TABLE IF NOT EXISTS ad_page (
  page_id int NOT NULL AUTO_INCREMENT COMMENT '页面ID，自增主键',
  page_name varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '页面名称',
  h5_app_id int NOT NULL DEFAULT '0' COMMENT 'H5应用ID（或tracking应用ID）',
  h5_page_id int NOT NULL DEFAULT '0' COMMENT 'H5平台页面ID（或tracking活动号ID）',
  company_id int NOT NULL DEFAULT '0' COMMENT '公司ID',
  os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
  status tinyint NOT NULL DEFAULT '0' COMMENT '0：正常，1：删除',
  remark varchar(128) COMMENT '备注（如果为tracking表示从tracking接入数据）',
  create_time datetime NOT NULL COMMENT  '创建时间',
  update_time datetime COMMENT  '更新时间',
  PRIMARY KEY (page_id),
  UNIQUE KEY uk (company_id, page_name, h5_app_id, h5_page_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='落地页表';


-- 包表
-- 替换旧表：ad_package
CREATE TABLE IF NOT EXISTS ad_pkg (
  pkg_id int NOT NULL AUTO_INCREMENT COMMENT '包ID，自增主键',
  pkg_name varchar(50) NOT NULL DEFAULT '' COMMENT '包名称',
  cp_app_id varchar(64) NOT NULL DEFAULT '0' COMMENT 'CP应用ID（或tracking应用ID）',
  cp_pkg_id varchar(20) NOT NULL DEFAULT '0' COMMENT 'CP包ID（或tracking活动号ID）',
  company_id int NOT NULL DEFAULT '0' COMMENT '公司ID',
  os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
  status tinyint NOT NULL DEFAULT '0' COMMENT '0：正常，1：删除',
  remark varchar(128) COMMENT '备注（如果为tracking表示从tracking接入数据）',
  create_time datetime NOT NULL COMMENT  '创建时间',
  update_time datetime COMMENT  '更新时间',
  PRIMARY KEY (pkg_id),
  UNIQUE KEY uk (company_id, pkg_name, cp_app_id, cp_pkg_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='包表';


-- 计划统计表(实时)，记录每次爬虫/接口数据
CREATE TABLE IF NOT EXISTS ad_plan_stat_realtime (
	stat_time DATETIME NOT NULL COMMENT '统计时间',
	plan_id INT DEFAULT '0' NOT NULL COMMENT '计划/策略ID,对应dc_ad_plan',
	medium_id SMALLINT DEFAULT '0' NOT NULL COMMENT '平台',
	medium_account_id INT(20) DEFAULT '0' NOT NULL COMMENT '媒体帐号ID',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	page_id INT DEFAULT '0' NOT NULL COMMENT '关联页面ID',
	pkg_id INT DEFAULT '0' NOT NULL COMMENT '关联包ID',
	ad_id VARCHAR(30) NOT NULL COMMENT '广告ID',
	cost INT DEFAULT '0' NOT NULL COMMENT '花费（单位：分）',
	show_num INT DEFAULT '0' NOT NULL COMMENT '曝光数',
	click_num INT DEFAULT '0' NOT NULL COMMENT '点击数',
	bid INT DEFAULT '0' NOT NULL COMMENT '出价（单位：分）',
	discount_rate DECIMAL(3,2) DEFAULT '0' COMMENT '折扣率',
	update_time DATETIME NOT NULL COMMENT '更新时间',
	CONSTRAINT uk UNIQUE (stat_time, company_id, medium_id, plan_id, ad_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='计划统计表(实时)，记录每次爬虫/接口数据';


-- 计划统计表(按天)
-- 替换旧表：dc_ad_report_day
CREATE TABLE IF NOT EXISTS ad_plan_stat_day (
	stat_date DATE NOT NULL COMMENT '统计日期',
	plan_id INT DEFAULT '0' NOT NULL COMMENT '计划/策略ID,对应dc_ad_plan',
	medium_id SMALLINT DEFAULT '0' NOT NULL COMMENT '平台',
	medium_account_id INT(20) DEFAULT '0' NOT NULL COMMENT '媒体帐号ID',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	page_id INT DEFAULT '0' NOT NULL COMMENT '关联页面ID',
	pkg_id INT DEFAULT '0' NOT NULL COMMENT '关联包ID',
	ad_id VARCHAR(30) NOT NULL COMMENT '广告ID',
	cost INT DEFAULT '0' NOT NULL COMMENT '花费（单位：分）',
	show_num INT DEFAULT '0' NOT NULL COMMENT '曝光数',
	click_num INT DEFAULT '0' NOT NULL COMMENT '点击数',
	bid INT DEFAULT '0' NOT NULL COMMENT '出价（单位：分）',
	discount_rate DECIMAL(3,2) DEFAULT '0' COMMENT '折扣率',
	update_time DATETIME NOT NULL COMMENT '更新时间',
	CONSTRAINT uk UNIQUE (stat_date, company_id, medium_id, plan_id, ad_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='计划统计表(按天)';



-- 落地页统计表(实时)，记录每次爬虫/接口数据
CREATE TABLE IF NOT EXISTS ad_page_stat_realtime (
	stat_time DATETIME NOT NULL COMMENT '统计时间',
	page_id INT DEFAULT '0' NOT NULL COMMENT '页面ID',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	pv INT DEFAULT '0' NOT NULL COMMENT 'pv',
	uv INT DEFAULT '0' NOT NULL COMMENT 'uv',
	download_times INT DEFAULT '0' NOT NULL COMMENT '落地页中包下载次数',
	update_time DATETIME NOT NULL COMMENT '更新时间',
	CONSTRAINT uk UNIQUE (stat_time, company_id, page_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='落地页统计表(实时)，记录每次爬虫/接口数据';


-- 落地页统计表(按天)
-- 替换旧表：dc_ad_land_page
CREATE TABLE IF NOT EXISTS ad_page_stat_day (
	stat_date DATE NOT NULL COMMENT '统计日期',
	page_id INT DEFAULT '0' NOT NULL COMMENT '页面ID',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	pv INT DEFAULT '0' NOT NULL COMMENT 'pv',
	uv INT DEFAULT '0' NOT NULL COMMENT 'uv',
	download_times INT DEFAULT '0' NOT NULL COMMENT '落地页中包下载次数',
	update_time DATETIME NOT NULL COMMENT '更新时间',
	CONSTRAINT uk UNIQUE (stat_date, company_id, page_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='落地页统计表(按天)';



-- 包统计表(实时)，记录每次爬虫/接口数据
CREATE TABLE IF NOT EXISTS ad_pkg_stat_realtime (
	stat_time DATETIME NOT NULL COMMENT '统计时间',
	pkg_id INT NOT NULL COMMENT '包ID',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	active_num INT DEFAULT '0' NOT NULL COMMENT '激活数',
	register_num INT DEFAULT '0' NOT NULL COMMENT '注册数',
	total_pay_times INT DEFAULT '0' COMMENT '总付费次数',
	total_pay_num INT DEFAULT '0' NOT NULL COMMENT '总付费人数',
	total_pay_amount INT DEFAULT '0' NOT NULL COMMENT '总付费金额（按分）',
	new_pay_times INT COMMENT '新增付费次数',
	new_pay_num INT COMMENT '新增付费人数',
	new_pay_amount INT COMMENT '新增付费金额（按分）',
	login_num INT COMMENT '登录人数(即DAU)',
	download_times INT DEFAULT '0' NOT NULL COMMENT '下载次数(从MTA获取)',
	retain_d2 INT COMMENT '次日留存',
	retain_d3 INT COMMENT '3日留存',
	retain_d5 INT COMMENT '5日留存',
	retain_d7 INT COMMENT '7日留存',
	retain_d14 INT COMMENT '14日留存',
	retain_d30 INT COMMENT '30日留存',
	update_time DATETIME NOT NULL COMMENT '更新时间',
	CONSTRAINT uk UNIQUE (stat_time, company_id, pkg_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='包统计表(实时)，记录每次爬虫/接口数据';


-- 包统计表(按天)
-- 替换旧表：ad_delivery_stat
CREATE TABLE IF NOT EXISTS ad_pkg_stat_day (
	stat_date DATE NOT NULL COMMENT '统计日期',
	pkg_id INT NOT NULL COMMENT '包ID',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	active_num INT DEFAULT '0' NOT NULL COMMENT '激活数',
	register_num INT DEFAULT '0' NOT NULL COMMENT '注册数',
	total_pay_times INT DEFAULT '0' COMMENT '总付费次数',
	total_pay_num INT DEFAULT '0' NOT NULL COMMENT '总付费人数',
	total_pay_amount INT DEFAULT '0' NOT NULL COMMENT '总付费金额（按分）',
	new_pay_times INT COMMENT '新增付费次数',
	new_pay_num INT COMMENT '新增付费人数',
	new_pay_amount INT COMMENT '新增付费金额（按分）',
	login_num INT COMMENT '登录人数(即DAU)',
	download_times INT DEFAULT '0' NOT NULL COMMENT '下载次数(从MTA获取)',
	retain_d2 INT COMMENT '次日留存',
	retain_d3 INT COMMENT '3日留存',
	retain_d5 INT COMMENT '5日留存',
	retain_d7 INT COMMENT '7日留存',
	retain_d14 INT COMMENT '14日留存',
	retain_d30 INT COMMENT '30日留存',
	update_time DATETIME NOT NULL COMMENT '更新时间',
	CONSTRAINT uk UNIQUE (stat_date, company_id, pkg_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='包统计表(按天)';



-- 计划关联关系表(计划，产品，落地页，包关联)
-- 替换旧表：dc_ad_association
CREATE TABLE IF NOT EXISTS ad_plan_relation (
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	product_id INT DEFAULT '0' NOT NULL COMMENT '产品ID',
	plan_id INT DEFAULT '0' NOT NULL COMMENT '计划/策略ID,对应ad_plan',
	page_id INT DEFAULT '0' NOT NULL COMMENT '页面ID',
	pkg_id INT DEFAULT '0' NOT NULL COMMENT '包ID',
	optimizer_system_account varchar(60) COMMENT '优化师ADT账号',
	remark VARCHAR(128) COMMENT '备注',
	create_time DATETIME NOT NULL COMMENT  '创建时间',
	update_time DATETIME COMMENT  '更新时间',
	CONSTRAINT uk UNIQUE (company_id, product_id, plan_id, page_id, pkg_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='计划关联关系表(计划，产品，落地页，包关联)';



-- Tracking统计表(实时)
CREATE TABLE IF NOT EXISTS ad_tracking_realtime
(
	stat_time DATETIME NOT NULL COMMENT '统计时间',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	page_id INT DEFAULT '0' NOT NULL COMMENT '页面ID',
	active_num INT DEFAULT '0' NOT NULL COMMENT '激活数',
	total_click_num INT DEFAULT '0' NOT NULL COMMENT '点击总数',
	unique_click_num INT DEFAULT '0' NOT NULL COMMENT '排重点击数',
	daily_unique_click_num INT DEFAULT '0' NOT NULL COMMENT '按天排重点击数',
	update_time DATETIME NOT NULL COMMENT '更新时间',
	CONSTRAINT uk UNIQUE (stat_time, company_id, page_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tracking统计表(实时)';

-- Tracking统计表(按天)
CREATE TABLE IF NOT EXISTS ad_tracking_day
(
	stat_date DATE NOT NULL COMMENT '统计日期',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	page_id INT DEFAULT '0' NOT NULL COMMENT '页面ID',
	active_num INT DEFAULT '0' NOT NULL COMMENT '激活数',
	total_click_num INT DEFAULT '0' NOT NULL COMMENT '点击总数',
	unique_click_num INT DEFAULT '0' NOT NULL COMMENT '排重点击数',
	daily_unique_click_num INT DEFAULT '0' NOT NULL COMMENT '按天排重点击数',
	update_time DATETIME NOT NULL COMMENT '更新时间',
	CONSTRAINT uk UNIQUE (stat_date, company_id, page_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tracking统计表(按天)';


-- 计划告警配置表
CREATE TABLE IF NOT EXISTS ad_plan_alert_conf
(
	id BIGINT NOT NULL AUTO_INCREMENT COMMENT '主键',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	type TINYINT DEFAULT '0' NOT NULL COMMENT '告警类型：0-普通告警, 1-高级告警',
	plan_id INT DEFAULT '0' NOT NULL COMMENT '计划/策略ID,对应ad_plan',
	consume DECIMAL(20,2) COMMENT '消耗值(元)',
	cpa DECIMAL(20,4) COMMENT 'CPA',
	ctr DECIMAL(20,4) COMMENT 'CTR',
	download_rate DECIMAL(20,4) COMMENT '下载率',
	email_alert TINYINT DEFAULT '0' NOT NULL COMMENT '立即邮件通知:0-否, 1-是',
	cost_per_download DECIMAL(20, 4) COMMENT '下载单价(元)',
	period TINYINT COMMENT '时段：1-小时、2-半小时、3-20分钟、4-10分钟',
	cost_operator TINYINT DEFAULT '0' COMMENT '消耗操作符：0-小于等于, 1-大于等于',
	cpa_operator TINYINT DEFAULT '0' COMMENT 'CPA操作符：0-小于等于, 1-大于等于',
	ctr_operator TINYINT DEFAULT '0' COMMENT 'CTR操作符：0-小于等于, 1-大于等于',
	download_rate_operator TINYINT DEFAULT '0' COMMENT '下载率操作符：0-小于等于, 1-大于等于',
	cost_per_download_operator TINYINT DEFAULT '0' COMMENT '下载单价操作符：0-小于等于, 1-大于等于',
	PRIMARY KEY (id),
	CONSTRAINT uk UNIQUE (company_id, plan_id, type)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='计划告警配置表';


-- 付费统计表，对应CP回本数据接口获取数据
CREATE TABLE IF NOT EXISTS ad_pay_stat
(
	id bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	platform_id TINYINT NOT NULL COMMENT '平台ID，对应dc_ad_medium表的medium字段',
	package_id bigint NOT NULL COMMENT '包ID',
	register_date DATE NOT NULL COMMENT '注册日期',
	pay_date DATE NOT NULL COMMENT '付费日期，如果付费统计类型为月统计，则付费日期为该月1号',
	pay_stat_type TINYINT NOT NULL COMMENT '付费统计类型：1-天统计, 2-月统计',
	pay_count bigint DEFAULT '0' NOT NULL COMMENT '付费人数',
	pay_num bigint DEFAULT '0' NOT NULL COMMENT '付费次数',
	pay_amount DECIMAL DEFAULT '0' NOT NULL COMMENT '付费金额(按分)',
	share_rate DECIMAL(3,2) DEFAULT '1.00' NOT NULL COMMENT '分成率',
	create_time DATETIME NOT NULL COMMENT '创建时间',
	update_time DATETIME NOT NULL COMMENT '修改时间',
	PRIMARY KEY (id),
	CONSTRAINT uk UNIQUE (company_id, pay_stat_type, register_date, pay_date, package_id, platform_id)
)
ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='付费统计表';


-- 日回本表:基于付费统计表（ad_pay_stat）计算回本数据
CREATE TABLE IF NOT EXISTS ad_payback_day 
(
	id bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	platform_id TINYINT NOT NULL COMMENT '平台ID，对应dc_ad_medium表的medium字段',
	package_id bigint NOT NULL COMMENT '包ID',
	register_date DATE NOT NULL COMMENT '注册日期',
	amount_d1 DECIMAL DEFAULT '0' NOT NULL COMMENT '首日回本金额(按分)',
	amount_d3 DECIMAL COMMENT '3日回本金额(按分)',
	amount_d7 DECIMAL COMMENT '7日回本金额(按分)',
	amount_d15 DECIMAL COMMENT '15日回本金额(按分)',
	amount_d30 DECIMAL COMMENT '30日回本金额(按分)',
	amount_d60 DECIMAL COMMENT '60日回本金额(按分)',
	amount_d90 DECIMAL COMMENT '90日回本金额(按分)',
	create_time DATETIME NOT NULL COMMENT '创建时间',
	update_time DATETIME NOT NULL COMMENT '修改时间',
	PRIMARY KEY (id),
	CONSTRAINT uk UNIQUE (company_id, register_date, package_id, platform_id)
)
ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='日回本表';


-- 月回本表:基于付费统计表（ad_pay_stat）计算回本数据
CREATE TABLE IF NOT EXISTS ad_payback_month 
(
	id bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
	company_id INT DEFAULT '0' NOT NULL COMMENT '公司ID',
	os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android',
	platform_id TINYINT NOT NULL COMMENT '平台ID，对应dc_ad_medium表的medium字段',
	package_id bigint NOT NULL COMMENT '包ID',
	register_date DATE NOT NULL COMMENT '注册日期',
	amount_m1 DECIMAL COMMENT '当月回本金额(按分)',
	amount_m2 DECIMAL COMMENT '次月回本金额(按分)',
	amount_m3 DECIMAL COMMENT '3个月回本金额(按分)',
	amount_m4 DECIMAL COMMENT '4个月回本金额(按分)',
	amount_m5 DECIMAL COMMENT '5个月回本金额(按分)',
	amount_m6 DECIMAL COMMENT '6个月回本金额(按分)',
	amount_m7 DECIMAL COMMENT '7个月回本金额(按分)',
	create_time DATETIME NOT NULL COMMENT '创建时间',
	update_time DATETIME NOT NULL COMMENT '修改时间',
	PRIMARY KEY (id),
	CONSTRAINT uk UNIQUE (company_id, register_date, package_id, platform_id)
)
ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='月回本表';
