INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('4-2', '活动对比按媒体分组统计趋势图', '/ad/delivery/trend/medium.do', current_timestamp(), current_timestamp());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('4-2', '活动对比按产品分组统计趋势图', '/ad/delivery/trend/product.do', current_timestamp(), current_timestamp());

------回本分析需要执行的sql

 
insert into ac_menu(menu_id,menu_name,create_time,update_time) values('5-2','回本对比',current_timestamp(),current_timestamp());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) values ('5-2', '按推广计划分别获取回本的趋势图', '/ad/payback-contrastive/plan.do', null, now(), now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) values ('5-2', '按媒体帐号分别获取回本的趋势图', '/ad/payback-contrastive/medium-account.do', null, now(), now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) values ('5-2', '按媒体分别获取回本的趋势图', '/ad/payback-contrastive/medium.do', null, now(), now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) values ('5-2', '按产品分别获取回本的趋势图', '/ad/payback-contrastive/product.do', null, now(), now());


update ac_system_account set menu_permission = concat(menu_permission,',5-2') where menu_permission like '%,5-1%' and status=0;