CREATE TABLE IF NOT EXISTS ac_company_tracking
(
    company_id INT NOT NULL AUTO_INCREMENT COMMENT '公司ID',
    company_name VARCHAR(200) NOT NULL COMMENT '公司名称',
    uid INT NOT NULL COMMENT '统一登录用户ID',
    user_name VARCHAR(200) COMMENT '统一登录用户名称',
    email VARCHAR(60) NOT NULL COMMENT '邮箱，登录账号（统一登录用户）',
    tel VARCHAR(20) COMMENT '电话',
    qq VARCHAR(15) COMMENT 'QQ',
    status TINYINT DEFAULT '0' NOT NULL COMMENT '状态：0-正常，1-待商务审核，2-暂停，3-拒绝，4-待运营审核，5-冻结，6-停用',
    biz_handler_id INT DEFAULT '0' NOT NULL COMMENT '商务处理人ID',
    oper_handler_id INT DEFAULT '0' NOT NULL COMMENT '运营处理人ID',
    pay_type TINYINT DEFAULT '0' NOT NULL COMMENT '付费类型：0-试用，1-付费',
    access_sdk TINYINT DEFAULT '0' NOT NULL COMMENT '接入SDK：0-是，1-否',
    click_package_type INT DEFAULT '1' NOT NULL COMMENT '套餐点击类型,0-限制点击,1-不限制点击',
    click_package_count INT DEFAULT '0' NOT NULL COMMENT '套餐点击次数，当限制点击时，该值必须大于0',
    expire_remind_days TINYINT DEFAULT '0' NOT NULL COMMENT '到期提醒：0-不提醒，1-提前7天，2-提前14天，3-提前30天',
    business_view VARCHAR(300) COMMENT '商务审批意见',
    operator_view VARCHAR(300) COMMENT '运营审批意见',
    effect_date DATE COMMENT '生效日期',
    invalid_date DATE COMMENT '失效日期',
    remark VARCHAR(300) COMMENT '备注',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    PRIMARY KEY (company_id),
    CONSTRAINT uk UNIQUE (email)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tracking主账号注册信息表';


alter table ac_tracking_account drop index uk;
alter table ac_tracking_account add CONSTRAINT uk UNIQUE (company_id, tracking_account);