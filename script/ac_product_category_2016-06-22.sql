--创建产品分类表
CREATE TABLE ac_product_category (category_id int NOT NULL AUTO_INCREMENT COMMENT '分类ID', category_name varchar(200) NOT NULL COMMENT '分类名称', 
parent_id int NOT NULL COMMENT '父级id', PRIMARY KEY (category_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品类型表';
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (1, '应用', 0);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (2, '游戏', 0);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (3, '音乐', 1);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (4, '角色扮演-RPG', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (5, '动作-ACT', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (6, '社交', 1);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (7, '冒险-AVG', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (8, '格斗-FTG', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (9, '泥巴-MUD', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (10, '射击-STG', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (11, '策略-SLG', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (12, '模拟-SIM', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (13, '养成-EDU', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (14, '多人在线战术竞技游戏-MOBA', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (15, '卡牌类-CAG', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (16, '体育竞技-SPG', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (17, '竞速-RCG', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (18, '音乐-MUG', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (19, '棋牌桌游-TAB', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (20, '休闲益智-PUZ', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (21, '其他', 2);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (22, '动作类角色扮演-ARPG', 4);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (23, '回合制角色扮演-TRPG', 4);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (24, '策略类角色扮演-SRPG', 4);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (25, '放置类角色扮演-IRPG', 4);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (26, '第一人称射击-FPS', 10);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (27, '第三人称射击-TPS', 10);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (28, '平台、卷轴式射击', 10);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (29, '即时战略类-RTS', 11);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (30, '回合制策略-TBS', 11);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (31, '策略类塔防-TD', 11);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (32, '集换式卡牌-TCG/CCG', 15);
INSERT INTO ac_product_category (category_id, category_name, parent_id) VALUES (33, 'RPG卡牌-CRPG', 15);

--在产品表中添加字段
alter table ac_product add column os_type TINYINT DEFAULT '0' NOT NULL COMMENT '系统类型：0-Others，1-iOS， 2-Android' after app_id;
alter table ac_product add column parent_category_Id TINYINT DEFAULT '0' NOT NULL COMMENT '一级产品分类id' after os_type;
alter table ac_product add column children_category_Id TINYINT DEFAULT '0' NOT NULL COMMENT '二级产品分类id' after parent_category_Id;

--修改菜单接口权限
update ac_menu_interface_mapping set menu_id='common' where mapping_id =34

--2017-08-10(其他媒体投放迭代开发的相关sql)
ALTER TABLE ad_campaign DROP COLUMN tracking_access_type   
ALTER TABLE ad_campaign modify COLUMN medium_id int DEFAULT '0' NOT NULL COMMENT '媒体id';
alter table ad_campaign add column channel_id INT DEFAULT '0' NOT NULL COMMENT '渠道id' after medium_id;
ALTER TABLE ad_plan modify COLUMN  medium_id int DEFAULT '0' NOT NULL COMMENT '媒体ID';
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) VALUES ('7-1', '根据产品id，查询媒体列表和对应的tracking渠道列表 ', '/ad/medium/queryMediumMappingForSelector.do', null, now(), now());

ALTER TABLE ad_plan_stat_realtime  modify COLUMN  medium_id int DEFAULT '0' NOT NULL COMMENT '平台';
ALTER TABLE ad_plan_stat_day modify COLUMN   medium_id int DEFAULT '0' NOT NULL COMMENT '平台';

--ad_campaign 初始化channel_id
update ad_campaign set channel_id=1017  where medium_id=1;
update ad_campaign set channel_id=1016  where medium_id=2;
update ad_campaign set channel_id=1032  where medium_id=4;
update ad_campaign set channel_id=1053  where medium_id=10;
update ad_campaign set channel_id=1001  where medium_id=13;
update ad_campaign set channel_id=1157  where medium_id=14;
update ad_campaign set channel_id=1052  where medium_id=24;
--智汇推
update ad_campaign set channel_id=1167  where medium_id=3 and tracking_access_type=1;
update ad_campaign set channel_id=1021  where medium_id=3 and tracking_access_type=0;

--2017-08-10  对比分析相关sql start
 --修改菜单权限
 update ac_system_account set menu_permission = replace(menu_permission,',5',',5-1') where menu_permission like '%,5%'
 select* from ac_system_account where menu_permission like '%,5%'
--修改菜单接口权限
select * from ac_menu_interface_mapping where menu_id='5-1'
update ac_menu_interface_mapping set menu_id='5-1' where menu_id='5'

insert into ac_menu(menu_id,menu_name,create_time,update_time) values('5-1','回本日报',current_timestamp(),current_timestamp());
insert into ac_menu(menu_id,menu_name,create_time,update_time) values('5-2','回本对比',current_timestamp(),current_timestamp());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) values ('5-2', '按推广计划分别获取回本的趋势图', '/ad/payback-contrastive/plan.do', null, now(), now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) values ('5-2', '按媒体帐号分别获取回本的趋势图', '/ad/payback-contrastive/medium-account.do', null, now(), now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) values ('5-2', '按媒体分别获取回本的趋势图', '/ad/payback-contrastive/medium.do', null, now(), now());
insert into ac_menu_interface_mapping (menu_id, interface_name, interface_url, remark, create_time, update_time) values ('5-2', '按产品分别获取回本的趋势图', '/ad/payback-contrastive/product.do', null, now(), now());



--2017-11-01  adt优化相关sql

insert into `channel` 
     (`id`, `channel_id`, `show_name`,  `cb_url`, `template_params`, `click_params`, `create_time`, `update_time`, `available`, `callback_all`) 
values('1175','1175','阿里汇川-转化工具',NULL,NULL,'[{\"platform\":2,\"clickSource\":2,\"uriParam\":\"imei_md5={IMEI_SUM}&mac_md5={MAC_SUM}&click_time={TS}&click_ip={IP}&callback_url={CALLBACK}\"},{\"platform\":1,\"clickSource\":2,\"uriParam\":\"idfa_md5={IDFA_SUM}&mac_md5={MAC_SUM}&click_time={TS}&click_ip={IP}&callback_url={CALLBACK}\"}]','1503642248','1','1','0');
insert into `ac_channel` 
     (`id`, `channel_id`, `show_name`,  `cb_url`, `template_params`, `click_params`, `create_time`, `update_time`, `available`, `callback_all`, `medium_id`) 
values('1175','1175','阿里汇川-转化工具',NULL,NULL,'[{\"platform\":2,\"clickSource\":2,\"uriParam\":\"imei_md5={IMEI_SUM}&mac_md5={MAC_SUM}&click_time={TS}&click_ip={IP}&callback_url={CALLBACK}\"},{\"platform\":1,\"clickSource\":2,\"uriParam\":\"idfa_md5={IDFA_SUM}&mac_md5={MAC_SUM}&click_time={TS}&click_ip={IP}&callback_url={CALLBACK}\"}]','1503642248','1','1','0','9');


INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('6', '根据短链ID查询包信息', '/ad/pkg/queryPkgByCpPkgId.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('9', '查询测试设备列表', '/ad/debugDevice/query.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('9', '新增测试设备', '/ad/debugDevice/add.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('9', '删除测试设备', '/ad/debugDevice/delete.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('9', '查询实时日志', '/ad/debugSdkData/listDebugLog.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('9', '查询测试统计日志', '/ad/debugSdkData/listDebugSummary.do', 0,null, now(), now());
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url,data_permission, remark, create_time, update_time) VALUES ('9', '清除测试日志', '/ad/debugSdkData/deleteDebugData.do', 0,null, now(), now());

