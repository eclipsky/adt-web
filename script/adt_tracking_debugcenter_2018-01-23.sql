---‘SDK接入’菜单id调整相关sql
update ac_menu set menu_id='9-3' where menu_id='9';  
update ac_menu_interface_mapping set menu_id='9-3' where menu_id='9';  
update ac_system_account set menu_permission = '9-3'  where account_role=3 and menu_permission='9'; 


INSERT INTO `ac_menu` (`menu_id`, `menu_name`, `create_time`, `update_time`) VALUES ('9', '调试中心', now(), now());
INSERT INTO `ac_menu` (`menu_id`, `menu_name`, `create_time`, `update_time`) VALUES ('9-1', '实用工具', now(), now());


insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time)
values('9-1','清除调试设备信息','/ad/debugCenter/clearDebugDevice.do',0,now(),now());

insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time)
values('9-1','调试设备归因查询','/ad/debugCenter/debugDeviceMatch.do',0,now(),now());


-- update 拓展工具-媒体联调测试设备 at 2018年2月1日16:16:33
INSERT INTO `ac_menu` (`menu_id`, `menu_name`, `create_time`, `update_time`) VALUES ('9-2', '媒体联调', now(), now());

insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time)
values('9-2','查询媒体联调设备列表','/ad/debugCenter/queryChannelDebugDevice.do',0,now(),now());

insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time)
values('9-2','新增媒体联调测试设备','/ad/debugCenter/addChannelDebugDevice.do',0,now(),now());

insert into ac_menu_interface_mapping(menu_id,interface_name,interface_url,data_permission,create_time,update_time)
values('9-2','删除媒体联调测试设备','/ad/debugCenter/deleteChannelDebugDevice.do',0,now(),now());