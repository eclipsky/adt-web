set names utf8;

-- 初始化菜单表
truncate table ac_menu;
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('1', 'ADT账号管理', null, '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('2', '产品管理', null, '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('3', '实时分析', null, '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('4', '投放分析', null, '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('4-1', '投放日报', null, '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('5', '回本分析', null, '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('6', '关联规则', null, '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu (menu_id, menu_name, remark, create_time, update_time) VALUES ('7', '媒体账号管理', null, '2017-01-01 00:00:00', '2017-01-01 00:00:00');

-- 初始化菜单接口映射表
truncate table ac_menu_interface_mapping;
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('public', '登录', '/user/login.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('public', '更新ApplicationContext缓存数据', '/ad/cache/refresh.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('public', '更新实时投放参数', '/update/params.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('public', '同步更新关联规则数据', '/ad/association/sync.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('public', 'AES Encrypt', '/tools/aesEncrypt.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('public', 'AES Decrypt', '/tools/aesDecrypt.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('public', 'MD5 Encode', '/tools/md5.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('public', 'MD5 Base64 Encode', '/tools/md5Base64.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '注销', '/user/logout.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '修改系统账号密码', '/ad/systemaccount/modifyPassword.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询投放计划(旧接口)', '/plan/namelist.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询投放计划', '/ad/plan/queryPlanForSelector.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询媒体(旧接口)', '/medium/namelist.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询媒体账号(旧接口)', '/ad/report/accounts.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询产品', '/ad/product/queryProductForSelector.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询媒体', '/ad/medium/queryMediumForSelector.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询媒体账号', '/ad/mediumaccount/queryMediumAccountForSelector.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询系统账号', '/ad/systemaccount/queryAccountForSelector.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询落地页(旧接口)', '/landpage/namelist.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询投放包(旧接口)', '/datapkg/namelist.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询投放包', '/ad/pkg/queryPkgForSelector.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('common', '下拉框查询落地页', '/ad/page/queryPageForSelector.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
	-- 账号管理菜单
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '查询系统账号列表信息', '/ad/systemaccount/queryAccount.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '新增系统账号', '/ad/systemaccount/addAccount.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '修改系统账号', '/ad/systemaccount/modifyAccount.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '删除系统账号', '/ad/systemaccount/deleteAccount.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '系统账号密码重置', '/ad/systemaccount/resetPassword.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '获取分组信息', '/ad/systemaccount/queryGroup.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '查询分组系统账户列表', '/ad/systemaccount/queryGroupAccount.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '创建新分组', '/ad/systemaccount/addGroup.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '修改分组', '/ad/systemaccount/modifyGroup.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '解散分组', '/ad/systemaccount/deleteGroup.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('1', '删除分组某个组员', '/ad/systemaccount/deleteGroupMember.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
	-- 产品管理菜单
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('2', '查询产品列表信息', '/ad/product/query.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('2', '新增产品信息', '/ad/product/add.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('2', '修改产品信息', '/ad/product/modify.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('2', '查询媒体账号列表信息', '/ad/mediumaccount/query.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('2', '新增媒体账号', '/ad/mediumaccount/add.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('2', '修改媒体账号', '/ad/mediumaccount/modify.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('2', '获取所有媒体账号(旧接口)', '/ad/report/accounts.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
	-- 实时分析菜单
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3', '实时分析-数据查询', '/ad/realtimedelivery/queryDelivery.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3', '实时分析-数据趋势图', '/ad/realtimedelivery/trend.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3', '实时分析-数据趋势图-指标列表', '/ad/realtimedelivery/indicators.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3', '实时分析-数据趋势图-周期列表', '/ad/realtimedelivery/period.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3', '设置告警配置', '/ad/realtimedelivery/confAlert.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3', '查询告警配置', '/ad/realtimedelivery/getAlert.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('3', '告警邮件设置', '/ad/realtimedelivery/confAlertEmail.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
	-- 投放分析-投放日报菜单
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('4-1', '投放日报数据查询', '/ad/report/adReport.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('4-1', '投放日报下载当前表格', '/ad/report/downloadTable.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
	-- 回本分析菜单
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('5', '日回本数据查询', '/ad/payback/daily.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('5', '下载回本日报', '/ad/payback/daily/download.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('5', '日回本率趋势', '/ad/payback/daily/trend.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('5', '月回本数据查询', '/ad/payback/monthly.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('5', '下载回本月报', '/ad/payback/monthly/download.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
	-- 关联规则菜单
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('6', '查询关联规则列表(旧接口)', '/ad/association/list.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('6', '新增关联规则(旧接口)', '/ad/association/create.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('6', '修改关联规则(旧接口)', '/ad/association/edit.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('6', '删除关联规则(旧接口)', '/ad/association/del.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('6', '查询关联规则列表', '/ad/association/query.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('6', '修改关联规则', '/ad/association/modify.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
	-- 媒体账号管理菜单
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('7', '基于系统账号查询媒体账号列表', '/ad/mediumaccount/queryBySystemAccount.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_menu_interface_mapping (menu_id, interface_name, interface_url, create_time, update_time) VALUES ('7', '修改媒体账号', '/ad/mediumaccount/modify.do', '2017-01-01 00:00:00', '2017-01-01 00:00:00');

-- 初始化公司表
truncate table ac_company;
INSERT INTO ac_company (company_name, email, status, remark, create_time, update_time) VALUES ('慧动创想', 'adt@dataeye.com', 0, null, now(), now());
INSERT INTO ac_company (company_name, email, status, remark, create_time, update_time) VALUES ('优点创想', 'youdiancx@dataeye.com', 0, null, now(), now());


-- 初始化系统账户表
truncate table ac_system_account;
INSERT INTO ac_system_account (company_id, account_name, account_alias, password, status, account_role, group_id, menu_permission, mobile, remark, create_time, update_time) 
	-- DE企业账号
SELECT 1 as company_id, UserID, UserName, Password, case(Status) when 2 then 0 else 1 end, 2 as account_role, -1 as group_id, '1,2,3,4-1,5,6', Tel,  Ext, from_unixtime(CreateTime), from_unixtime(CreateTime) FROM dc_user 
where Permission = 1 and (CompanyName='慧动创想' or CompanyName='慧动-测试账号')
union all 
	-- 优点系统账号
SELECT 2 as company_id, UserID, UserName, Password, case(Status) when 2 then 0 else 1 end, 0 as account_role, -1 as group_id, '3,4-1,5,6,7', Tel,  Ext, from_unixtime(CreateTime), from_unixtime(CreateTime) FROM dc_user 
where CompanyName='优点'
union all 
	-- DE系统账号(转入优点以查看数据)
SELECT 2 as company_id, UserID, UserName, Password, case(Status) when 2 then 0 else 1 end, 0 as account_role, -1 as group_id, '3,4-1,5,6,7', Tel,  Ext, from_unixtime(CreateTime), from_unixtime(CreateTime) FROM dc_user 
where Permission = 0 and (CompanyName='慧动创想' or CompanyName='慧动-测试账号')
;

INSERT INTO ac_system_account (company_id, account_name, account_alias, password, status, account_role, group_id, menu_permission, mobile, remark, create_time, update_time) VALUES (2, 'youdiancx@dataeye.com', '优点企业账号', 'c3649271c4f39f090440c4fdf9a83f05', 0, 2, -1, '1,2,3,4-1,5,6', '', '', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_system_account (company_id, account_name, account_alias, password, status, account_role, group_id, menu_permission, mobile, remark, create_time, update_time) VALUES (2, 'mikehuang@dataeye.com', '黄华基', 'e99a18c428cb38d5f260853678922e03', 0, 0, -1, '3,4-1,5,6,7', '', '', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_system_account (company_id, account_name, account_alias, password, status, account_role, group_id, menu_permission, mobile, remark, create_time, update_time) VALUES (2, 'jennyli@dataeye.com', '李金华', 'e99a18c428cb38d5f260853678922e03', 0, 0, -1, '3,4-1,5,6,7', '', '', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_system_account (company_id, account_name, account_alias, password, status, account_role, group_id, menu_permission, mobile, remark, create_time, update_time) VALUES (2, 'wangzifei@dataeye.com', '王耔霏', 'e99a18c428cb38d5f260853678922e03', 0, 0, -1, '3,4-1,5,6,7', '', '', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_system_account (company_id, account_name, account_alias, password, status, account_role, group_id, menu_permission, mobile, remark, create_time, update_time) VALUES (2, 'liuyuhao@dataeye.com', '刘禹豪', 'e99a18c428cb38d5f260853678922e03', 0, 0, -1, '3,4-1,5,6,7', '', '', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_system_account (company_id, account_name, account_alias, password, status, account_role, group_id, menu_permission, mobile, remark, create_time, update_time) VALUES (2, 'yustarwang@digitcube.net', '汪祥斌', 'e99a18c428cb38d5f260853678922e03', 0, 2, -1, '1,2,3,4-1,5,6', '', '', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_system_account (company_id, account_name, account_alias, password, status, account_role, group_id, menu_permission, mobile, remark, create_time, update_time) VALUES (2, 'youdian@dataeye.com', '优点专用', 'f088d6463e3512523b63539c1a97b725', 0, 0, -1, '3,4-1,5,6,7', '', '', '2017-01-01 00:00:00', '2017-01-01 00:00:00');
INSERT INTO ac_system_account (company_id, account_name, account_alias, password, status, account_role, group_id, menu_permission, mobile, remark, create_time, update_time) VALUES (2, 'de@youdiancx.com', 'DataEye专用', 'f088d6463e3512523b63539c1a97b725', 0, 0, -1, '3,4-1,5,6,7', '', '', '2017-01-01 00:00:00', '2017-01-01 00:00:00');


update ac_medium_account set system_account_name_list = 'mengying@dataeye.com,jennyli@dataeye.com,wangzifei@dataeye.com,liuyuhao@dataeye.com,youdian@dataeye.com'
where company_id = 2 and medium_account in('1403135186','1561484429','2330268629','3301108347','3551802415','3557286180','617558031','2804297188','1873146397','kasey@dataeye.com','seonzhang@dataeye.com');

update ac_medium_account set system_account_name_list = '4005150@qq.com,2055460477@qq.com,2241443474@qq.com,1485893475@qq.com,1179152549@qq.com,de@youdiancx.com'
where company_id = 2 and medium_account not in('1403135186','1561484429','2330268629','3301108347','3551802415','3557286180','617558031','2804297188','1873146397','kasey@dataeye.com','seonzhang@dataeye.com');



-- 初始化媒体表
truncate table ac_medium;
INSERT INTO ac_medium (medium_id, medium_name, medium_homepage, remark, create_time, update_time)
SELECT medium, name, homePage, null, from_unixtime(CreateTime), from_unixtime(CreateTime) FROM dc_ad_medium
WHERE name != 'MTA' AND name != '旗乐';

-- 初始化产品表
truncate table ac_product;
INSERT INTO ac_product (product_id, company_id, product_name, status, remark, create_time, update_time) 
VALUES (1, 2, '塔王之王', 0, null, '2017-01-01 00:00:00', '2017-01-01 00:00:00');

-- 初始化CP分成率表
truncate table ac_cp_share_rate;
INSERT INTO ac_cp_share_rate (product_id, company_id, share_rate, effect_date, invalid_date, create_time, update_time) 
VALUES (1, 2, 1, '2017-01-01 00:00:00', '2099-12-31 00:00:00', '2017-01-01 00:00:00', '2017-01-01 00:00:00');


-- 初始化媒体账户表
truncate table ac_medium_account;

-- 初始化媒体折扣率表
truncate table ac_medium_discount_rate;
