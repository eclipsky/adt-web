insert into ac_account_login_stat(stat_date, company_id, adt_account_num, medium_account_num, login_times, uni_login_times, login_account_list, create_time, update_time)
select date_format(t1.create_time, '%Y-%m-%d') as stat_date, t2.company_id, max(t2.adt_account_num) as adt_account_num, max(t2.medium_account_num) as medium_account_num,
	count(1) as total_login_times, count(distinct T1.account_id) as uni_login_times, group_concat(distinct t2.account_name) login_account_list, now(), now()
from ac_account_login_info t1 
join (
	select i1.company_id, concat(i2.account_name, '(', i2.account_alias,')') as account_name, i2.account_id, if(i3.adt_account_num is null, 0, i3.adt_account_num) adt_account_num, if(i4.medium_account_num is null, 0, i4.medium_account_num) medium_account_num
	from ac_company i1 
	join ac_system_account i2 on i1.company_id = i2.company_id
	left join (
		select company_id, count(account_id) as adt_account_num from ac_system_account where account_role != 2 and status = 0 group by company_id
	) i3 on i1.company_id = i3.company_id
	left join (
	       select company_id , count(medium_account_id) as medium_account_num from ac_medium_account where status = 0 group by company_id
	) i4 on i1.company_id = i4.company_id
	where i2.account_role != 2 and i1.status = 0
) t2 on t1.account_id = t2.account_id 
where date_format(t1.create_time, '%Y-%m-%d') = DATE_SUB(CURDATE(),INTERVAL 1 DAY)
group by company_id, stat_date
on duplicate key update adt_account_num = values(adt_account_num), medium_account_num = values(medium_account_num), uni_login_times = values(uni_login_times), login_account_list = values(login_account_list), update_time = now();