-- 记录公司当前状态
-- 统计（所有&有效）：ADT账号数、媒体账号数
-- 统计（所有&排重）：企业账号登录次数、ADT账号登录次数
insert into ac_account_active_stat(stat_date, company_id, adt_account_num, effect_adt_account_num, medium_account_num,
effect_medium_account_num, main_total_login_times, main_uni_login_times, adt_total_login_times, adt_uni_login_times, login_account_list, create_time, update_time)
select date_sub(curdate(),interval 1 day) as stat_date, t0.company_id, 
IFNULL(t1.adt_account_num, 0)adt_account_num, ifnull(t1.effect_adt_account_num, 0) effect_adt_account_num, 
ifnull(t2.medium_account_num, 0) medium_account_num, ifnull(t2.effect_medium_account_num, 0) effect_medium_account_num, 
if(t3.main_total_login_times is null, 0, t3.main_total_login_times) as main_total_login_times, 
if(t3.main_uni_login_times is null, 0, t3.main_uni_login_times) as main_uni_login_times, 
if(t3.adt_total_login_times is null, 0, t3.adt_total_login_times) as adt_total_login_times, 
if(t3.adt_uni_login_times is null, 0, t3.adt_uni_login_times) as adt_uni_login_times, t3.login_account_list, now() as create_time, now() as update_time
from ac_company t0
left join (
  -- 统计（所有&有效）：ADT账号数
  select t1.company_id, count(1) adt_account_num, count(if(t1.status = 0, 1, null)) effect_adt_account_num from ac_system_account t1
  where t1.account_role != 2 and t1.create_time < curdate()
  group by t1.company_id
) t1 on t0.company_id = t1.company_id 
left join (
  -- 统计（所有&有效）媒体账号数
  select company_id, count(1) medium_account_num, count(if(status = 0, 1, null)) effect_medium_account_num from ac_medium_account 
  where create_time < curdate()
  group by company_id
) t2 on t0.company_id = t2.company_id
left join (
  -- 统计（所有&排重）：企业账号登录次数、ADT账号登录次数
  select i1.company_id, 
   count(if(i2.account_role = 2, 1, null)) as main_total_login_times,
   count(distinct if(i2.account_role = 2, i1.account_id, null)) as main_uni_login_times,
   count(if(i2.account_role != 2, 1, null)) as adt_total_login_times, 
   count(distinct if(i2.account_role != 2, i1.account_id, null)) as adt_uni_login_times, 
   group_concat(distinct concat(i2.account_name, '(', i2.account_alias,')')) login_account_list
  from ac_account_login_info i1 
  join ac_system_account i2 on i1.account_id = i2.account_id 
  where i1.create_time >= date_sub(curdate(),interval 1 day) and i1.create_time < curdate() 
  group by i1.company_id
) t3 on t0.company_id = t3.company_id
where t0.status = 0
on duplicate key update adt_account_num = values(adt_account_num), effect_adt_account_num = values(effect_adt_account_num), 
medium_account_num = values(medium_account_num), effect_medium_account_num = values(effect_medium_account_num), 
main_total_login_times = values(main_total_login_times), main_uni_login_times = values(main_uni_login_times), 
adt_total_login_times = values(adt_total_login_times), adt_uni_login_times = values(adt_uni_login_times), 
login_account_list = values(login_account_list), update_time = now();
