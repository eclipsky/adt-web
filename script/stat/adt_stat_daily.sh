#!/bin/sh

source ~/.bash_profile

date=`date +%Y-%m-%d`

echo "****${date}****"
mysql_deadt='mysql -udc_crawler -pcrawler -hmysql7 -P3306 -A --default-character-set=utf8'

$mysql_deadt -e 'use de_adt; source /usr/local/htdocs/adtserver/adt-scripts/stat/adt_login_stat_daily.sql;'

$mysql_deadt -e 'use de_adt; source /usr/local/htdocs/adtserver/adt-scripts/stat/adt_active_stat_daily.sql;'

echo "****!Done!****"
echo ""
