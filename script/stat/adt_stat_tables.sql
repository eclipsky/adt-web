drop table if exists ac_account_login_stat;
CREATE TABLE IF NOT EXISTS ac_account_login_stat
(
	stat_date DATE NOT NULL COMMENT '统计日期',
	company_id INT NOT NULL COMMENT '公司ID',
	account_num bigint(21) DEFAULT '0' NOT NULL COMMENT 'ADT账号数',
	login_times bigint(21) DEFAULT '0' NOT NULL COMMENT 'ADT账号总登录次数',
	uni_login_times bigint(21) DEFAULT '0' NOT NULL COMMENT 'ADT账号排重登录次数',
	login_account_list text COMMENT '登录账号列表',
	create_time DATETIME NOT NULL COMMENT '创建时间',
	update_time DATETIME NOT NULL COMMENT '更新时间',
	CONSTRAINT uk UNIQUE (stat_date, company_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '账号登录统计表';

drop table if exists ac_account_active_stat;
CREATE TABLE IF NOT EXISTS ac_account_active_stat
(
    stat_date DATE NOT NULL COMMENT '统计日期',
    company_id INT NOT NULL COMMENT '公司ID',
    adt_account_num INT DEFAULT '0' NOT NULL COMMENT 'ADT账号数',
    effect_adt_account_num INT DEFAULT '0' NOT NULL COMMENT '有效ADT账号数',
    medium_account_num INT DEFAULT '0' NOT NULL COMMENT '媒体账号数',
    effect_medium_account_num INT DEFAULT '0' NOT NULL COMMENT '媒体账号数',
    main_total_login_times INT DEFAULT '0' NOT NULL COMMENT '企业账号总登录次数',
    main_uni_login_times INT DEFAULT '0' NOT NULL COMMENT '企业账号排重登录次数',
    adt_total_login_times INT DEFAULT '0' NOT NULL COMMENT 'ADT账号总登录次数',
    adt_uni_login_times INT DEFAULT '0' NOT NULL COMMENT 'ADT账号排重登录次数',
    login_account_list text COMMENT '登录账号列表',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '更新时间',
    CONSTRAINT uk UNIQUE (stat_date, company_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账号活跃统计表';